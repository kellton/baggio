<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Classe de funcoes gerais utilizadas em toda aplicacao.
 * 
 * PHP version 5.6
 * 
 * @category   Helper
 * @package    application/helpers
 * @version    Release: v1.0
 */ 

use Carbon\Carbon;

/* ********** FUNCOES DE SESSAO ********** */
/*
 * USUARIO_SESSAO_ADM
 * 
 * Retorna todas as informacoes
 * do usuário adm logado.
 *
 * @return  array Array com o objeto de usuario.
*/
function usuario_sessao_adm() {
    $CI =& get_instance();
    return $CI->session->userdata('usuario');
}

/*
 * USUARIO_SESSAO_CLIENTE
 *
 * Retorna todas as informacoes
 * do usuário cliente logado
 *
 * @return  array Array com o objeto de cliente.
*/
function usuario_sessao_cliente() {
    $CI =& get_instance();
    return $CI->session->userdata('cliente');
}

/*
 * IS_USUARIO_ADM
 *
 * Verifica se o usuario em
 * sessao eh administrador.
 * 
 * @return  boolean True, se o usuario for admin.
*/
function is_usuario_adm(){
    $CI =& get_instance();
    $usuario = $CI->session->userdata('usuario');
    return $usuario && $usuario['tipo_usuario']==1;
}

/*
 * IS_USUARIO_CLI
 *
 * Verifica se o usuario em
 * sessao eh cliente.
 * 
 * @return  boolean True, se o usuario for cliente.
*/
function is_usuario_cli(){
    $CI = & get_instance();
    $usuario = $CI->session->userdata('cliente');
    return $usuario && $usuario['tipo_usuario']!=1;
}

/*
 * SALVAR_SESSAO_BAIRROS
 * 
 * Salva os bairros escolhidos em 
 * uma sessao temporaria para uso
 * na busca de imoveis. Necessaria 
 * pois a busca carrega os bairros 
 * por funcao asincrona.
 *
 * @param   array Bairros a serem carregados na sessao.
*/
function salvar_sessao_bairros($bairros=array()) {
    $CI =& get_instance();

    $CI->session->set_tempdata('bairros_selecionados', 
        json_encode($bairros), 10
    );
}

/*
 * RECUPERAR_SESSAO_BAIRROS
 * 
 * Recupera o array de bairros 
 * da sessao setada pela funcao 
 * SALVAR_SESSAO_BAIRROS.
 *
 * @return  array Array de bairros guardados na sessao.
*/
function recuperar_sessao_bairros() {
    $CI =& get_instance();

    $bairros = $CI->session->tempdata('bairros_selecionados');

    if (isset($bairros) && !empty($bairros)) {
        return json_decode($bairros);
    }
    return array();
}

/* ********** FUNCOES DE FORMATACAO ********** */
/*
 * FORMATAR_DATA_SQL
 *
 * Formata uma data passada por
 * parametro em uma data SQL para banco.
 *
 * @param   string data.
 *
 * @return  string data formatada.
*/
function formatar_data_sql($data="") {
    $campos = explode("/", $data);

    if (count($campos) == 3) {
        return date("Y-m-d", strtotime($campos[2].'/'.$campos[1].'/'.$campos[0]));
    }
    return $data;
}

/*
 * FORMATAR_SQL_TO_DATA
 *
 * @param   string  data.
 * @param   boolean booleano que indica se inclui as horas.
 * 
 * @return  date    Objeto date formatado.
*/
function formatar_sql_to_data($data_sql="", $sem_hora=FALSE) {
    if ($sem_hora) {
        return date("d-m-Y", strtotime($data_sql));    
    }
    return date("d-m-Y H:i", strtotime($data_sql));
}

/*
 * FORMATAR_DATA
 *
 * Formata uma data passada por
 * parametro em uma data no padrao brasi-
 * leiro. DD/MM/YYYY
 *
 * @param   string str
 *
 * @return  string
*/
function formatar_data($str="") {
    return date('d/m/Y', strtotime($str));
}


/*
 * FORMATAR_SQL_TO_ANGARIACAO
 *
 * @param   
 *
 * @return  string
*/
function formatar_sql_to_angariacao($str="") {
    $dias = array("domingo", "segunda", "terça", "quarta", "quinta", "sexta", "sábado");

    return date('d/m/Y', strtotime($str)) . " - " . ucfirst( $dias[ intval( date("w", strtotime($str)) ) ] );
}

/*
 * FORMATAR_FLOAT_SQL
 * 
 * @param   string
 *
 * @return  string
*/
function formatar_float_sql($numero="") {
    $ponto_e_virgula = array(".", ",");
    $vazio_e_ponto = array("", ".");

    return str_replace($ponto_e_virgula, $vazio_e_ponto, $numero);
}

/* 
 * FORMATAR_MOEDA
 * 
 * @param   
 *
 * @return  string
*/
function formatar_moeda($string) {
    $CI = & get_instance();

    return number_format( floatval( $string ), 2, ',', '.');
}

/* 
 * FORMATAR_ERRO_BAIRRO
 * 
 * @param   
 *
 * @return  array
*/
function formatar_erro_bairro($bairro) {
    $formatados = array();
    // SEM ACENTO E MINUSCULA
    array_push($formatados, strtolower(remover_acentos($bairro)));
    // MINUSCULA COM ESPACO
    array_push($formatados, strtolower(remover_acentos($bairro)) . " ");
    if (tem_acentos($bairro)) {
        // ACENTUADA E MINUSCULA
        array_push($formatados, lower_acentuada($bairro));
        // ACENTUADA E MINUSCULA COM ESPACO
        array_push($formatados, lower_acentuada($bairro) . " ");
    }
    return $formatados;
}

/* ********** FUNCOES DE DATA ********** */
/*
 * FROM_SQL_TO_CARBON
 *
 * @param  
 *  
 * @return  string
*/
function from_sql_to_carbon($data_sql) {
    $partes_data_sql = explode(" ", $data_sql);

    if (count($partes_data_sql) == 2) {
        $data = $partes_data_sql[0];
        $hora = $partes_data_sql[1];
        $partes_data = explode("-", $data);
        $partes_hora = explode(":", $hora);

        return Carbon::create(
            $partes_data[0],$partes_data[1],$partes_data[2],
            $partes_hora[0],$partes_hora[1],$partes_hora[2]);
    }

    return array(
        date('Y'),date('m'),date('d'),date('H'),date('i'),date('s'));
}


/*
 * DATA_HOJE_PASTA
 *
 * Retorna a data em String no
 * formato de pasta. YYYY-MM-DD_HH-MM
 *
 * @param  
 *  
 * @return  string
*/
function data_hoje_pasta() {
    return date("Y-m-d_H-i");
}

/*
 * DATA_HOJE_SQL
 *
 * Retorna a data em String no
 * formato de banco. YYYY-MM-DD HH:MM:SS
 *
 * @param 
 *   
 * @return  string
*/
function data_hoje_sql($com_horario=TRUE) {
    if ($com_horario) return date("Y-m-d H:i:s");
    return date("Y-m-d");
}

/*
 * DATA_HOJE
 * 
 * @param   
 *
 * @return  string
*/
function data_hoje($com_horario=FALSE) {
    if ($com_horario) return date("d/m/Y H:i:s");
    return date("d/m/Y");
}

/*
 * DIA_SEMANA
 * 
 * Retorna o dia da semana de
 * hoje.
 * 
 * @param   boolean $ufc Modifica os dias para UpperCase na primeira letra
 * 
 * @return  string
*/
function dia_semana($ucf=TRUE) {
    //0 for Sunday, 6 for Saturday
    $dias = array("domingo", "segunda", "terça", "quarta", "quinta", "sexta", "sábado");

    if ($ucf) {
        return ucfirst( $dias[ intval(date("w")) ] );
    }
    return $dias[ intval(date("w")) ];
}

/*
 * VALIDAR_DATA
 *
 * Valida uma data passada por
 * parametro no formato brasileiro.
 *
 * @param   
 *
 * @return  boolean
*/
function validar_data($data="") {
    $partes_data = explode('/', $data);
    if (count($partes_data) != 3) {
        return FALSE;
    }
    $dia = intval($partes_data[0]);
    $mes = intval($partes_data[1]);
    $ano = intval($partes_data[2]);

    return checkdate($mes, $dia, $ano);
}


/* ********** FUNCOES DE FRETE ********** */
/*
 * CALCULA_PESO_CAIXAS
 *
 * Calcula o frete a partir do
 * carrinho de compras
 * com os parametros passados.
 *
 * @param   
 *
 * @return  float
*/

function calcula_peso_caixas($cep_destino){
    $CI = & get_instance();
    $maior_altura=$maior_largura=$maior_comprimento=$cm_cub=$peso=0;
    foreach ($CI->cart->contents() as $item) {
        if($item['altura'] > $maior_altura){ $maior_altura = $item['altura'];}
        if($item['largura'] > $maior_largura){ $maior_largura = $item['largura'];}
        if($item['comprimento'] > $maior_comprimento){ $maior_comprimento= $item['comprimento'];}
        $cm_cub = $cm_cub + ((($item['altura']/10)*($item['largura']/10)*($item['comprimento']/10))/100) * $item['qty'];
        $peso = $peso + ($item['peso']*$item['qty']);
    }
    $maiores_dimensoes = array('alt'=>$maior_altura,'lar'=>$maior_largura,'comp'=>$maior_comprimento);
    arsort($maiores_dimensoes);
    foreach ($maiores_dimensoes as $chave => $valor) {
        $caixa[] = $valor;
    }
    $dimensao1 = $caixa[0];
    $dimensao2 = $caixa[1];
    $dimensao3 = 1;
    $caixas = 1;
    while (((($dimensao1/10)*($dimensao2/10)*($dimensao3/10))/100)<$cm_cub){
        $dimensao3++;
        if($dimensao3 % 1000 == 0){
            $caixas++;
        }
    }
    if($caixas>1){
        $dimensao3 = $dimensao3 - (($caixas-1)*1000);
    }
    $preco_correio = 0;
    if($caixas==1){
        $preco_correio = frete_correios($cep_destino, ($dimensao1/10), ($dimensao2/10), ($dimensao3/10), ($peso/1000));
    }
    else{
        if($caixas>1){
            $peso = ($peso/$caixas);
            for($i=$caixas;$i>0;$i--){
                if($i>1){
                    $preco_correio += frete_correios($cep_destino, ($dimensao1/10), ($dimensao2/10), ($dimensao3/10), ($peso/1000));
                }
            }
        }
        else{
            $preco_correio += frete_correios($cep_destino, ($dimensao1/10), ($dimensao2/10), ($dimensao3/10), ($peso/1000));
        }
    }
    return $preco_correio;
}

/*
 * FRETE_CORREIOS
 *
 * Calcula o frete a partir das
 * configuracoes contidas no arquivo
 * APPLICATION/CONFIG/CONSTANTS.PHP, e com
 * os parametros passados.
 *
 * @param   string  $comprimento
 * @param   string  $altura
 * @param   string  $largura
 * @param   string  $peso
 * 
 * @return  float
*/
function frete_correios($cep_destino,$comprimento,$altura,$largura,$peso) {
    if($altura<2){$altura=2;}  
    if($largura<11){$largura=11;}  
    if($comprimento<16){$comprimento=16;}
    $data['nCdEmpresa'] = '';
    $data['sDsSenha'] = '';
    $data['sCepOrigem'] = CONS_CEP_ORIGEM;
    $data['sCepDestino'] = $cep_destino;
    $data['nVlPeso'] = $peso;
    $data['ncdFormato'] = '1';
    $data['nVlComprimento'] = $comprimento;
    $data['nVlAltura'] = $altura;
    $data['nVlLargura'] = $largura;
    $data['nVlDiametro'] = '0';
    $data['sCdMaoPropria'] = 's';
    $data['nVlValorDeclarado'] = '0';
    $data['sCdAvisoRecebimento'] = 'n';
    $data['StrRetorno'] = 'xml';
    $data['nCdServico'] = CONS_SERVICO_ENTREGA;
    $data = http_build_query($data);
    $url = 'http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx';
    $curl = curl_init($url.'?'.$data);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    $result = curl_exec($curl);
    $result = simplexml_load_string($result);
    foreach ($result->cServico as $row) {
        if($row->Erro == 0){
            return $row->Valor;
        }
        else{
            echo '<pre>';
            print_r($row);
        }
    }
}

/* ********** FUNCOES DE VIEW ********** */
/*
 * PAGINAR
 *
 * Realizar a construcao do elemento de 
 * paginacao em codigo HTML, utilizando a
 * funcionalidade do CodeIgniter.
 *
 * @param   string      $url_paginacao URL utilizada para os links das paginas.
 * @param   integer     $qtd_por_pg    Quantidade de itens por pagina.
 * @param   integer     $total_resultados Quantidade total dos itens.
 * @param   array       $lista_itens   Array com a lista dos itens.
 * 
 * @return  html
*/
function paginar($url_paginacao='', $qtd_por_pg=0, $total_resultados=0, $lista_itens=array()) {
    $CI = & get_instance();

    /* TAG DE PAGINACAO */
    $config['full_tag_open']    = "<nav aria-label='Paginação'><ul class='pagination justify-content-center'>";
    $config['full_tag_close']   = "</ul></nav>";
    /* TAG DE NUMERACAO */
    $config['num_tag_open']     = "<li class='page-item'>";
    $config['num_tag_close']    = "</li>";
    /* TAG DE PAGINA ATUAL */
    $config['cur_tag_open']     = "<li class='page-item active'><a class='page-link'>";
    $config['cur_tag_close']    = "</a></li>";
    /* TAG DE PROXIMO */
    $config['next_tag_open']    = "<li class='page-item'>";
    $config['next_tagl_close']  = "</li>";
    /* TAG DE ANTERIOR */
    $config['prev_tag_open']    = "<li class='page-item'>";
    $config['prev_tagl_close']  = "</li>";
    /* TAG DE PRIMERIA PAGINA */
    $config['first_tag_open']   = "<li class='page-item'>";
    $config['first_tagl_close'] = "</li>";
    /* TAG DE ULTIMA PAGINA */
    $config['last_tag_open']    = "<li class='page-item'>";
    $config['last_tagl_close']  = "</li>";
    /* TEXTO LINK DO PROXIMO */
    $config['next_link']    = "&raquo;";
    /* TEXTO LINK DO ATNTERIOR */
    $config['prev_link']    = "&laquo;";

    /* ATRIBUTOS */
    $config['attributes'] = array('class' => 'page-link');
    /* URL DA PAGINACAO */
    $config['base_url'] = base_url($url_paginacao);
    /* TOTAL DE RESULTADOS */
    $config['total_rows'] = $total_resultados;
    /* TOTAL DE ITENS POR PAGINA */
    $config['per_page'] = $qtd_por_pg;
    /* SEGMENTO DA URI QUE TEM A PAGINACAO */
    $config['uri_segment'] = calcular_uri_paginacao($url_paginacao);

    $CI->pagination->initialize($config);

    return $CI->pagination->create_links();
}

/*
 * CALCULAR_URI_PAGINACAO
 * 
 * @param   
 *
 * @return  integer
*/
function calcular_uri_paginacao($url) {
    $suburls = explode("/", $url);
    return count($suburls) + 1;
}

/*
 * TABELAR
 *
 * Realiza a construcao do elemento de 
 * tabela em codigo HTML, utilizando a
 * funcionalidade do CodeIgniter.
 *
 * @param   array      $lista Array com os itens.
 * @param   array      $cabecalho Array com as strings do cabecalho.
 * @param   string     $classe_tabela Classe da tabela.
 * @param   string     $classe_cabecalho Classe do cabecalho.
 * @param   string     $classe_linha Classe das linhas.
 * @param   string     $classe_celula Classe das celulas.
 * @param   string     $html_sem_resultados String para quando nao houver resultados.
 *
 * @return  html
*/
function tabelar($lista=array(), $cabecalho=array(), $classe_tabela="", $classe_cabecalho="", $classe_linha="", $classe_celula="", $html_sem_resultados="") {
    $html_table = "";
    $CI = & get_instance();

    $table_template = array(
        "table_open"          => "<table class='table table-bordered table-striped table-hover $classe_tabela'>",

        "heading_row_start"   => "<tr>",
        "heading_row_end"     => "</tr>",
        "heading_cell_start"  => "<th class='$classe_cabecalho'>",
        "heading_cell_end"    => "</th>",

        "row_start"           => "<tr class='$classe_linha'>",
        "row_end"             => "</tr>",
        "cell_start"          => "<td class='$classe_celula'>",
        "cell_end"            => "</td>",

        "row_alt_start"       => "<tr class='$classe_linha'>",
        "row_alt_end"         => "</tr>",
        "cell_alt_start"      => "<td class='$classe_celula'>",
        "cell_alt_end"        => "</td>",

        "table_close"         => "</table>"
    );
    $CI->table->set_template($table_template);

    if (!empty($cabecalho)) {
        $CI->table->set_heading($cabecalho);
    }

    if (!empty($lista)) {
        $html_table = $CI->table->generate($lista);
    } else {
        $html_table = $CI->table->generate(array());
        $html_table .=  $html_sem_resultados;
    }
    return $html_table;
}

/*
 * LISTAR_DROPDOWN
 * 
 * @param   
*/
function listar_dropdown($lista, $tem_valor_zero=TRUE, $valor_zero="Selecione", $excluir_primeiro=FALSE) {
    $itens = array();

    if ($tem_valor_zero) {
        $itens[NULL] = $valor_zero;
    }

    foreach ($lista as $indice => $item) {
        if ($excluir_primeiro && $indice == 0) {
            continue;
        }
        $itens[current($item)] = next($item);
    }
    return $itens;
}

/*
 * CARREGAR_JS
 *
 * Retorna o array com a lista 
 * de caminhos da arquivo JS a 
 * serem carregados no header.
 * 
 * @param   string  indicacao da pagina a carregar.
 * 
 * @return  array   lista de string com os caminhos dos arquivos.
*/
function carregar_js($page="todos") {
    $script_urls = array();
    switch ($page) {
        case "geral":
            $script_urls = array(
                "publico/js/libs/jquery-slim.min.js",
                "publico/js/libs/jquery-3.2.1.min.js",
                "publico/js/libs/jquery-ui/jquery-ui.min.js",
                "publico/js/libs/popper.min.js",
                "publico/js/libs/bootstrap.min.js",
                "publico/js/baggio-config.js",
                "publico/js/baggio-constants.js",
                "publico/js/libs/jquery-mask/jquery.mask.js",
                "publico/js/libs/easy-autocomplete/jquery.easy-autocomplete.min.js",
                "publico/js/libs/slider/lightslider.js",
                "publico/js/libs/tether/tether.min.js",
                "publico/js/libs/bootstrap-confirmation.min.js",
                "publico/js/libs/slick-1.8.0/slick/slick.min.js",
                "publico/lightbox/ekko-lightbox.min.js",
                "publico/js/libs/fontawesome/fontawesome-all.min.js",
                // "publico/js/libs/chosen/chosen.jquery.min.js",
                // "publico/js/libs/select2/select2.min.js",
                "publico/js/libs/select2/select2.js",
                "publico/js/slick.js",
                //"publico/js/geral.min.js",
                "publico/js/geral.js",
                "publico/js/mascaras.js",
                //"publico/js/adm/busca.min.js",
                "publico/js/adm/baggio-admin.js",
                "publico/js/adm/busca.js",
                //"publico/js/autocomplete.min.js",
                "publico/js/autocomplete.js",
                "publico/js/adm/validacao_formularios.js",
                "publico/lightbox/acao.js"
            );
            break;
        case "busca":

            break;
        default:
            $script_urls = array(
                "publico/js/libs/jquery-slim.min.js",
                "publico/js/libs/jquery-3.2.1.min.js",
                "publico/js/libs/jquery-ui/jquery-ui.min.js",
                "publico/js/libs/popper.min.js",
                "publico/js/libs/bootstrap.min.js",
                "publico/js/baggio-config.js",
                "publico/js/baggio-constants.js",
                "publico/js/libs/jquery-mask/jquery.mask.js",
                "publico/js/libs/easy-autocomplete/jquery.easy-autocomplete.min.js",
                "publico/js/libs/slider/lightslider.js",
                "publico/js/libs/tether/tether.min.js",
                "publico/js/libs/bootstrap-confirmation.min.js",
                "publico/js/libs/slick-1.8.0/slick/slick.min.js",
                "publico/js/slick.js",
                "publico/lightbox/ekko-lightbox.min.js",
                "publico/js/libs/fontawesome/fontawesome-all.min.js",
                // "publico/js/libs/chosen/chosen.jquery.min.js",
                // "publico/js/libs/select2/select2.min.js",
                "publico/js/libs/select2/select2.js",
                //"publico/js/geral.min.js",
                "publico/js/adm/baggio-admin.js",
                "publico/js/geral.js",
                "publico/js/mascaras.js",
                //"publico/js/datepicker.min.js",
                "publico/js/datepicker.js",
                //"publico/js/togvis.min.js",
                "publico/js/togvis.js",
                "publico/js/adm/tratamento_fichas.js",
                //"publico/js/adm/modais.min.js",
                "publico/js/adm/modais.js",
                //"publico/js/adm/busca.min.js",
                "publico/js/adm/busca.js",
                //"publico/js/autocomplete.min.js",
                "publico/js/autocomplete.js",
                "publico/js/adm/validacao_formularios.js",
                "publico/lightbox/acao.js"
            );
        break;
    }
    return $script_urls;
}

/*
 * CARREGAR_CSS
 * 
 * Retorna o array com a lista 
 * de caminhos da arquivo CSS a 
 * serem carregados no header.
 * 
 * @param   string  indicacao da pagina a carregar.
 * 
 * @return  array   lista de string com os caminhos dos arquivos.
*/
function carregar_css($page="todos") {
    $css_urls = array();
    switch ($page) {
        case "geral":
            $css_urls = array(
                /* Fontes */
                "publico/fonts/hansom/hansom.css",
                "publico/fonts/antenna/antenna.css",
                /* Bootstrap */
                "publico/css/libs/bootstrap/css/bootstrap.css",
                /* Jquery UI */
                "publico/css/libs/jquery-ui/jquery-ui.min.css",
                /* Glyphicons */
                // "publico/fonts/font-awesome-4.7.0/css/font-awesome.min.css",
                "publico/css/libs/fontawesome/fontawesome-all.min.css",
                /* Easy Autocomplete */
                "publico/css/libs/easy-autocomplete/easy-autocomplete.min.css",
                /* Light Slider */
                "publico/css/libs/slider/lightslider.css",
                /* Carrosel */
                "publico/js/libs/slick-1.8.0/slick/slick.css",
                "publico/js/libs/slick-1.8.0/slick/slick-theme.css",
                /* Chosen */
                // "publico/js/libs/chosen/docsupport/stylesheet.css",
                // "publico/js/libs/chosen/chosen.min.css",
                "publico/css/libs/select2/select2.min.css",
                /* Geral */
                "publico/css/geral.css",
                /* Header */
                "publico/css/template/header.css",
                /* Menu Mobile */
                "publico/css/template/menu.css",
                /* Footer */
                "publico/css/template/footer.css",
                /* Busca */
                "publico/css/api/buscas.css",
                /* Sizes */
                "publico/css/sizes/extra-small.css",
                "publico/css/sizes/small.css",
                "publico/css/sizes/medium.css",
                "publico/css/sizes/large.css",
                "publico/css/sizes/extra-large.css"
            );
            break;
        case "busca":

            break;
        default:
            $css_urls = array(
                /* Fontes */
                "publico/fonts/hansom/hansom.css",
                "publico/fonts/antenna/antenna.css",
                /* Bootstrap */
                "publico/css/libs/bootstrap/css/bootstrap.css",
                /* Jquery UI */
                "publico/css/libs/jquery-ui/jquery-ui.min.css",
                /* Glyphicons */
                // "publico/fonts/font-awesome-4.7.0/css/font-awesome.min.css",
                "publico/css/libs/fontawesome/fontawesome-all.min.css",
                /* Lightbox */
                "publico/lightbox/ekko-lightbox.css",
                /* Easy Autocomplete */
                "publico/css/libs/easy-autocomplete/easy-autocomplete.min.css",
                /* Light Slider */
                "publico/css/libs/slider/lightslider.css",
                /* Carrosel */
                "publico/js/libs/slick-1.8.0/slick/slick.css",
                "publico/js/libs/slick-1.8.0/slick/slick-theme.css",
                /* Chosen */
                // "publico/js/libs/chosen/docsupport/stylesheet.css",
                // "publico/js/libs/chosen/chosen.min.css",
                "publico/css/libs/select2/select2.min.css",
                /* Geral */
                "publico/css/geral.css",
                /* Header */
                "publico/css/template/header.css",
                /* Menu Mobile */
                "publico/css/template/menu.css",
                /* Footer */
                "publico/css/template/footer.css",
                /* Adm Cadastro Imoveis */
                "publico/css/adm/cadastro-imoveis.css",
                "publico/css/adm/fichas-cadastrais.css",
                /* Modais do Adm */
                "publico/css/adm/modais.css",
                /* Adm Abas */
                "publico/css/adm/abas.css",
                /* Adm Clientes */
                "publico/css/adm/adm-clientes.css",
                /* Busca */
                "publico/css/api/buscas.css",
                /* Sizes */
                "publico/css/sizes/extra-small.css",
                "publico/css/sizes/small.css",
                "publico/css/sizes/medium.css",
                "publico/css/sizes/large.css",
                "publico/css/sizes/extra-large.css"
            );
        break;
    }
    return $css_urls;
}

/*
 * POST_CHECKBOX
 * 
 * Verifica se o campo de checkbox
 * esta checked e retorna a string
 * necessaria para check.
 * 
 * @param   
 *
 * @return  string
*/
function post_checkbox() {
    $CI = & get_instance();
    $campos = array();
    $qtd_campos = func_num_args();

    for ($i = 0; $i < $qtd_campos; $i++) {
        array_push($campos, func_get_arg($i));
    }

    foreach ($campos as $indice => $campo) {
        if ( $CI->input->post($campo) == "1" || $CI->input->post($campo) == "on" || $campo === "1") {
            return "checked='checked'";
        }
    }
    return "";
}

/*
 * POST_TEXT_AREA
 * 
 * @param   
 *
 * @return  string
*/
function post_text_area() {
    $CI = & get_instance();
    $campos = array();
    $qtd_campos = func_num_args();

    for ($i = 0; $i < $qtd_campos; $i++) {
        array_push($campos, func_get_arg($i));
    }

    foreach ($campos as $indice => $campo) {
        if ( $CI->input->post($campo) == "1" || $CI->input->post($campo) == "on" || $campo === "1") {
            return "checked='checked'";
        }
    }
    return "";
}

/* ********** FUNCOES DE TEXTO ********** */
/*
 * REMOVER_ACENTOS
 *
 * @param   
 *
 * @return  string
*/
function remover_acentos($variavel) {
    $string = strval($variavel);
    $acentuadas = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ', 'Ά', 'ά', 'Έ', 'έ', 'Ό', 'ό', 'Ώ', 'ώ', 'Ί', 'ί', 'ϊ', 'ΐ', 'Ύ', 'ύ', 'ϋ', 'ΰ', 'Ή', 'ή');
    $nao_acentuadas = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o', 'Α', 'α', 'Ε', 'ε', 'Ο', 'ο', 'Ω', 'ω', 'Ι', 'ι', 'ι', 'ι', 'Υ', 'υ', 'υ', 'υ', 'Η', 'η');
    return str_replace($acentuadas, $nao_acentuadas, $string);
}

/*
 * TEM_ACENTOS
 *
 * @param   
 *
 * @return  string
*/
function tem_acentos($string) {
    $acentuadas = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ', 'Ά', 'ά', 'Έ', 'έ', 'Ό', 'ό', 'Ώ', 'ώ', 'Ί', 'ί', 'ϊ', 'ΐ', 'Ύ', 'ύ', 'ϋ', 'ΰ', 'Ή', 'ή');

    foreach ($acentuadas as $acentuada) {
        if (strpos($string, $acentuada) !== FALSE) {
            return TRUE;
        }
    }
    return FALSE;
}

/*
 * REMOVER_ESPACOS_EXTERNOS
 *
 * @param   
 * 
 * @return  string
*/
function remover_espacos_externos($variavel) {
    $string = strval($variavel);

    return ltrim( rtrim($string) );
}

/*
 * UCWORDS_ACENTUADA
 *
 * @param   
 * 
 * @return  string
*/
function ucwords_acentuada($variavel, $encode=CONS_BAGGIO_XML_ENCODE) {
    $string = strval($variavel);

    return mb_convert_case($string, MB_CASE_TITLE, $encode);
}


/*
 * LOWER_ACENTUADA
 *
 * @param   
 * 
 * @return  string
*/
function lower_acentuada($variavel, $encode=CONS_BAGGIO_XML_ENCODE) {
    $string = strval($variavel);

    return mb_convert_case($string, MB_CASE_LOWER, $encode);
}

/* ********** FUNCOES DE ARQUIVOS ********** */
/*
 * UPLOAD_FOTO_IMOVEL
 *
 * @param  
 * 
 * @return  boolean
*/
function upload_foto_imovel($nome="", $tipo="", $temp="", $erro="", $tamanho="", $caminho=FCPATH) {
    return upload_arquivo(
        $nome, $tipo, $temp, $erro, $tamanho, $caminho, 
        CONS_BAGGIO_FOTOS_TIPOS, 
        CONS_BAGGIO_FOTOS_MAX_SIZE, 
        CONS_BAGGIO_FOTOS_PERMISSAO
    );
}

/*
 * UPLOAD_ARQUIVO
 *
 * @param  
 * 
 * @return  boolean
*/
function upload_arquivo(
    $nome="", $tipo="", $temp="", $erro="", $tamanho="", $caminho=FCPATH,  
    $tipos_permitidos="jpg|png|pdf", $max_size=10000, $permissao=0755) 
{
    if(!is_dir($caminho)) {
        mkdir($caminho, $permissao, TRUE);
    }

    $CI = & get_instance();

    $config['file_name']            = $nome;
    $config['file_ext_tolower']     = TRUE;
    $config['upload_path']          = $caminho;
    $config['allowed_types']        = $tipos_permitidos;
    $config['max_size']             = $max_size;

    $CI->load->library('upload', $config);

    $CI->upload->initialize($config);

    $_FILES["arquivo"]['name']      = $nome;
    $_FILES["arquivo"]['type']      = $tipo;
    $_FILES["arquivo"]['tmp_name']  = $temp;
    $_FILES["arquivo"]['error']     = $erro;
    $_FILES["arquivo"]['size']      = $tamanho;

    if(!$CI->upload->do_upload("arquivo")) {
        to_debug($CI->upload->display_errors());

        //log_message('error', "HELPER - UPLOAD ARQUIVO - " . $CI->upload->display_errors());

        return FALSE;
    }

    $CI->upload->data();

    return TRUE;
}

/* ********** FUNCOES DE CI ********** */
/*
 * VALUES_ONLY_FROM_RESULTARRAY
 *
 * Retorna um array apenas de 
 * valores da coluna especificada
 * por parametro a partir de 
 * um resultarray.
 *
 * @param   
 *
 * @return  boolean
*/
function values_only_from_resultarray($column="", $resultarray=array()) {
    $values_only = array();

    foreach ($resultarray as $linha) {
        foreach ($linha as $coluna => $valor) {
            if ($coluna === $column) {
                array_push($values_only, $valor);
            }
        }
    }
    return $values_only;
}


/* ********** OUTRAS FUNCOES ********** */
/*
 * IS_CLIENT
 *
 * Verifica que tipo de cliente
 * esta fazendo a requisicao ao servidor.
 * Bot, Browser, Mobile.
 *
 * @param   
 *
 * @return  boolean
*/
function is_client($tipo=NULL) {
    $user_agent = strtolower($_SERVER['HTTP_USER_AGENT']);
    if ($tipo == 'bot') {
        if (preg_match("/googlebot|adsbot|yahooseeker|yahoobot|msnbot|watchmouse|pingdom\.com|feedfetcher-google/", $user_agent)) {
            return TRUE;
        }
    }
    else if ($tipo == 'browser') {
        if (preg_match("/mozilla\/|opera\//", $user_agent)) {
            return TRUE;
        }
    }
    else if ($tipo == 'mobile') {
        if (preg_match("/phone|iphone|itouch|ipod|symbian|android|htc_|htc-|palmos|blackberry|opera mini|iemobile|windows ce|nokia|fennec|hiptop|kindle|mot |mot-|webos\/|samsung|sonyericsson|^sie-|nintendo/", $user_agent)) {
                return TRUE;
        }
        else if (preg_match("/mobile|pda;|avantgo|eudoraweb|minimo|netfront|brew|teleca|lg;|lge |wap;| wap /", $user_agent)) {
                return TRUE;
        }
    }
    return FALSE;
}

/*
 * TO_DEBUG
 *
 * Metodo para debugar varia-
 * veis passadas por parametro.
 *
 * @param   variable    $variable
 * @param   boolean     $to_die
*/
function to_debug($variable=NULL, $to_die=TRUE) {
    echo '<pre>';
    var_dump($variable);
    echo '<br>';

    if ($to_die) {
        die();
    }
}

/*
 * TO_DEBUG_R
 *
 * Metodo para debugar varia-
 * veis passadas por parametro.
 *
 * @param   variable    $variable
 * @param   boolean     $to_die
*/

function to_debug_r($variable=NULL, $to_die=TRUE) {
    echo '<pre>';
    print_r($variable);
    echo '<br>';

    if ($to_die) {
        die();
    }
}

/*

 * TO_DEBUG_SCREEN
 *
 * Metodo para debugar varia-
 * veis passadas por parametro.
 *
 * @param   variable    $variable
 * @param   boolean     $to_die
*/

function to_debug_screen($variable=NULL) {
    echo '<div style="position:absolute;z-index:11000;background:white;opacity:0.75;">';
    echo '<pre>';
    var_dump($variable);
    echo '<br>';
    echo '</div>';
}

/* End of file baggio_helper.php */
