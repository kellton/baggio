<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Classe de funcoes de email da aplicacao.
 * 
 * PHP version 5.6
 * 
 * @category   Helper
 * @package    application/helpers
 * @version    Release: v1.0
 */ 
/* 
    ****************************************************************************
                        FUNCOES DE EMAIL
    ****************************************************************************
*/
/*
 * ENVIAR_PARA_CLIENTE
 *
 * 
 *
 * @param   
 * 
 * @return  boolean
*/
function enviar_para_cliente($assunto="", $emails_cliente=array(), $mensagem="") {
    $CI =& get_instance();

    $CI->load->library('myphpmailer');

    foreach ($emails_cliente as $indice => $email) {
        if (!$CI->myphpmailer->send_from_locaweb_api($assunto, $email, $mensagem)) {
            return FALSE;
        }
    }
    return TRUE;
}

/*
 * ENVIAR_PARA_BAGGIO
 *
 * @param   
 * 
 * @return  boolean
*/
function enviar_para_baggio($assunto="", $email_cliente="", $nome_cliente="Cliente", $mensagem="") {
    $CI =& get_instance();

    $CI->load->library('myphpmailer');

    return $CI->myphpmailer->send_email(
        $assunto, $email_cliente, $nome_cliente,
        CONS_CODEMAIL_USER_NAME, CONS_CODEMAIL_PASSWORD,
        array(CONS_CODEMAIL_FROM), $mensagem
    );
}

/*
 * ENVIAR_PARA_BAGGIO_CONTATO
 *
 * @param   
 * 
 * @return  boolean
*/
function enviar_para_baggio_contato($assunto="", $email_cliente="", $nome_cliente="Cliente", $mensagem="", $emails=array()) {
    $CI =& get_instance();

    $CI->load->library('myphpmailer');

    foreach ($emails as $indice => $email) {
        if (!$CI->myphpmailer->send_from_locaweb_api($assunto, $email, $mensagem)) {
            return FALSE;
        }
    }
    return TRUE;
}

/*
 * ENVIAR_EMAIL
 *
 * @param   
 * 
 * @return  boolean
*/
function enviar_email($assunto="", $emails=array(), $mensagem="") {
    $CI =& get_instance();

    $CI->load->library('myphpmailer');

    foreach ($emails as $indice => $email) {
        if (!$CI->myphpmailer->send_from_locaweb_api($assunto, $email, $mensagem)) {
            return FALSE;
        }
    }
    return TRUE;
}

/*
 * ENVIAR_HOTMAIL
 *
 * @param   
 * 
 * @return  boolean
*/
function enviar_hotmail($assunto="", $emails=array(), $mensagem="") {
    $CI =& get_instance();

    $CI->load->library('myphpmailer');

    return $CI->myphpmailer->send_from_hotmail($assunto, $emails, $mensagem);
}

/*
 * ENVIAR_LOCAWEB
 *
 * @param   
 * 
 * @return  boolean
*/
function enviar_locaweb($assunto="", $emails=array(), $mensagem="") {
    $CI =& get_instance();

    $CI->load->library('myphpmailer');

    foreach ($emails as $indice => $email) {
        if (!$CI->myphpmailer->send_from_locaweb_api($assunto, $email, $mensagem)) {
            return FALSE;
        }
    }
    return TRUE;
}

/*
 * ENVIAR_EMAIL_FICHAS
 * ---------------------------------------
 * DESCRICAO : 
 * ---------------------------------------
 * PARAMETROS : 
 * ---------------------------------------
 * RETORNO : [BOOLEAN]
*/
function enviar_email_fichas($assunto="", $emails=array(), $mensagem="") {
    $CI =& get_instance();

    $CI->load->library('myphpmailer');

   foreach ($emails as $indice => $email) {
        if (!$CI->myphpmailer->send_from_locaweb_api($assunto, $email, $mensagem)) {
            return FALSE;
        }
    }
    return TRUE;
}

/* End of file baggio_email_helper.php */