<?php
// requires module php5-curl
$x_auth_token = '794c863c996485f3e93fbcfa6ee577c2';
$api_url = 'https://api.smtplw.com.br/v1/messages';

$from = "webmaster@baggioimoveis.com.br";
$to = array("felipe@voyo.com.br");
$subject = "Teste API HTML";
$body = <<<EOT
    <p>
      teste
    </p>
    <p>
      Teste 2 com <strong>bold</strong>
    </p>
EOT;

$headers = array(
  "x-auth-token: $x_auth_token",
  "Content-type: application/json"
);

$data_string = array(
  'from'    => $from,
  'to'      => $to,
  'subject' => $subject,
  'body'    => $body,
  'headers' => array('Content-type' => 'text/html')
);

$ch = curl_init($api_url);

curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_HEADER, true);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data_string));
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

$response = curl_exec($ch);
$curlInfo = curl_getinfo($ch);
curl_close($ch);

echo '<pre>';
var_dump($curlInfo);
echo '</pre>';

switch($curlInfo['http_code']) {
  case '201':
    $status = 'OK';
    if (preg_match('@^Location: (.*)$@m', $response, $matches)) {
      $location = trim($matches[1]);
    }
    // Add other actions here, if necessary.
    break;
  default:
    $status = "Error: $curlInfo[http_code]";
    break;
}

echo "\nStatus: {$status}\n";

if (isset($location)) {
  echo "\nLocation: {$location}\n\n";
}

?>