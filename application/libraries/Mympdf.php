<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include("./vendor/mpdf/mpdf/mpdf.php");

class Mympdf
{
  public function generate($html,$nome_arquivo)
  {
    define('DOMPDF_ENABLE_AUTOLOAD', false);
    require_once("./vendor/autoload.php");
    $mpdf = new mPDF('utf-8','A4','','','15','15','32','35');
    $mpdf->SetHTMLHeader('<img style="width: 23%; margin-top:-2%;" src="' . base_url() . 'publico/imagens/baggio-logo-30-anos.png"/><hr>');
    $mpdf->setFooter('{PAGENO}');
    //$mpdf->SetHTMLFooter('<hr><img style="width: 110%;margin-top:-2%;" src="' . base_url() . 'publico/imagens/baggio-logo-30-anos.png"/>');
    $mpdf->WriteHTML($html);
    $mpdf->Output($nome_arquivo.'.pdf','I');
  }
}