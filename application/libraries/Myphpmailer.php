<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require_once("./vendor/autoload.php");

class Myphpmailer {
    /*
     * SEND_EMAIL
     *
     * @param   
     * 
     * @return  array
    */
    public function send_email($subject="", $email_from="", $name_from="", $user="", $pass="", $emails_to=array(), $message="") {
        
        $CI =& get_instance();

        try {
            if (empty($message) || empty($emails_to) || empty($name_from)) {
                return FALSE;
            }

            $mail = new PHPMailer(true);

            // $mail->SMTPDebug = 2;               // Enable verbose debug output
            $mail->isSMTP();                    // Set mailer to use SMTP
            $mail->Host = CONS_CODEMAIL_HOST;   // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;             // Enable SMTP authentication
            $mail->Username = $user;  // SMTP username
            $mail->Password = $pass;   // SMTP password
            $mail->SMTPSecure = CONS_CODEMAIL_SMTP_SECURE;  // Enable TLS encryption, `ssl` also accepted
            $mail->Port = CONS_CODEMAIL_PORT;   // TCP port to connect to
            $mail->CharSet = 'UTF-8';
            
            $mail->setFrom($user, $name_from);

            foreach ($emails_to as $index => $email_to) {
                $mail->addAddress($email_to);
            }

            $mail->addReplyTo($email_from, $name_from);
            $mail->AddEmbeddedImage(CONS_CODEMAIL_IMG_PATH, 'imagem_baggio');

            $header = "";
            $header .= "<div style='text-align:left;margin-top:20px;margin-bottom:20px;'>";
            $header .= "<a href='" . base_url() . "'>";
            $header .= "<img style='width:200px;' src='cid:imagem_baggio' border='0'/>";
            $header .= "</a>";
            $header .= "<hr style='margin: 15px 0 5px 0;height:6px;border: 0;background:#183459;'/>";
            $header .= "</div>";

            $mail->isHTML(true);    // Set email format to HTML
            $mail->Subject = $subject;
            $mail->Body    = $header . $message;
            $mail->AltBody = $message;

            $mail->send();

            return true;
        } catch (Exception $e) {
            // to_debug($mail->ErrorInfo);

            log_message('error', 'ERRO PHPMAILER - ' . $mail->ErrorInfo);    
        }
        $CI->session->set_flashdata('param_msg_erro_cabecalho', 'Erro ao Enviar E-mail');
        $CI->session->set_flashdata('param_msg_erro', 'Ocorreu um erro ao tentar enviar e-mail(s). Verifique se todas as informações do(s) e-mail(s) estavam corretas!');

        redirect(base_url());
    }

    /*
     * SEND_FROM_HOTMAIL
     *
     * @param   
     * 
     * @return  array
    */
    public function send_from_hotmail($subject="", $emails_to=array(), $message="") {
        
        $CI =& get_instance();

        try {
            if (empty($message) || empty($emails_to)) {
                return FALSE;
            }

            $mail = new PHPMailer(true);

            // $mail->SMTPDebug = 2;               // Enable verbose debug output
            $mail->isSMTP();                    // Set mailer to use SMTP
            $mail->Host = CONS_HOTMAIL_HOST;   // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;             // Enable SMTP authentication
            $mail->Username = CONS_HOTMAIL_USER_NAME;  // SMTP username
            $mail->Password = CONS_HOTMAIL_PASSWORD;   // SMTP password
            $mail->SMTPSecure = CONS_HOTMAIL_SMTP_SECURE;  // Enable TLS encryption, `ssl` also accepted
            $mail->Port = CONS_HOTMAIL_PORT;   // TCP port to connect to
            $mail->CharSet = 'UTF-8';
            
            $mail->setFrom(CONS_HOTMAIL_FROM, CONS_HOTMAIL_FROM_NAME);

            foreach ($emails_to as $index => $email_to) {
                $mail->addAddress($email_to);
            }

            $mail->addReplyTo(CONS_HOTMAIL_FROM, CONS_HOTMAIL_FROM_NAME);
            $mail->AddEmbeddedImage(CONS_CODEMAIL_IMG_PATH, 'imagem_baggio');

            $header = "";
            $header .= "<div style='text-align:left;margin-top:20px;margin-bottom:20px;'>";
            $header .= "<a href='" . base_url() . "'>";
            $header .= "<img style='width:200px;' src='cid:imagem_baggio' border='0'/>";
            $header .= "</a>";
            $header .= "<hr style='margin: 15px 0 5px 0;height:6px;border: 0;background:#183459;'/>";
            $header .= "</div>";

            $mail->isHTML(true);    // Set email format to HTML
            $mail->Subject = $subject;
            $mail->Body    = $header . $message;
            $mail->AltBody = $message;

            $mail->send();

            return true;
        } catch (Exception $e) {
            // to_debug($mail->ErrorInfo);

            log_message('error', 'ERRO PHPMAILER - ' . $mail->ErrorInfo);    
        }
        $CI->session->set_flashdata('param_msg_erro_cabecalho', 'Erro ao Enviar Hotmail');
        $CI->session->set_flashdata('param_msg_erro', 'Ocorreu um erro ao tentar enviar e-mail(s). Verifique se todas as informações do(s) e-mail(s) estavam corretas!');

        redirect(base_url());
    }

    /*
     * SEND_FROM_LOCAWEB
     *
     * @param   
     * 
     * @return  array
    */
    public function send_from_locaweb($subject="", $emails_to=array(), $message="") {
        
        $CI =& get_instance();

        try {
            if (empty($message) || empty($emails_to)) {
                return FALSE;
            }

            $mail = new PHPMailer(true);

            // $mail->SMTPDebug = 2;               // Enable verbose debug output
            $mail->isSMTP();                    // Set mailer to use SMTP
            $mail->Host = CONS_LOCAWEB_HOST;   // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;             // Enable SMTP authentication
            $mail->Username = CONS_LOCAWEB_USER_NAME;  // SMTP username
            $mail->Password = CONS_LOCAWEB_PASSWORD;   // SMTP password
            $mail->SMTPSecure = CONS_LOCAWEB_SMTP_SECURE;  // Enable TLS encryption, `ssl` also accepted
            $mail->Port = CONS_LOCAWEB_PORT;   // TCP port to connect to
            $mail->CharSet = 'UTF-8';
            
            $mail->setFrom(CONS_LOCAWEB_FROM, CONS_LOCAWEB_FROM_NAME);

            foreach ($emails_to as $index => $email_to) {
                $mail->addAddress($email_to);
            }

            $mail->addReplyTo(CONS_LOCAWEB_FROM, CONS_LOCAWEB_FROM_NAME);
            $mail->AddEmbeddedImage(CONS_CODEMAIL_IMG_PATH, 'imagem_baggio');

            $header = "";
            $header .= "<div style='text-align:left;margin-top:20px;margin-bottom:20px;'>";
            $header .= "<a href='" . base_url() . "'>";
            $header .= "<img style='width:200px;' src='cid:imagem_baggio' border='0'/>";
            $header .= "</a>";
            $header .= "<hr style='margin: 15px 0 5px 0;height:6px;border: 0;background:#183459;'/>";
            $header .= "</div>";

            $mail->isHTML(true);    // Set email format to HTML
            $mail->Subject = $subject;
            $mail->Body    = $header . $message;
            $mail->AltBody = $message;

            $mail->send();

            return true;
        } catch (Exception $e) {
            to_debug($mail->ErrorInfo);

            log_message('error', 'ERRO PHPMAILER - ' . $mail->ErrorInfo);    
        }
        $CI->session->set_flashdata('param_msg_erro_cabecalho', 'Erro ao Enviar Hotmail');
        $CI->session->set_flashdata('param_msg_erro', 'Ocorreu um erro ao tentar enviar e-mail(s). Verifique se todas as informações do(s) e-mail(s) estavam corretas!');

        redirect(base_url());
    }

    /*
     * SEND_FROM_LOCAWEB_API
     *
     * @param   
     * 
     * @return  boolean
    */
    function send_from_locaweb_api($assunto="", $email="", $mensagem="") {
        try {
            if (empty($mensagem) || empty($email)) {
                return FALSE;
            }

            $headers = array(
              "x-auth-token: " . CONS_API_LW_X_AUTH,
              "Content-type: application/json"
            );

            $mail_header = "";
            $mail_header .= "<div style='text-align:left;margin-top:20px;margin-bottom:20px;'>";
            $mail_header .= "<a href='" . base_url() . "'>";
            $mail_header .= "<img style='width:200px;' src='cid:imagem_baggio' border='0'/>";
            $mail_header .= "</a>";
            $mail_header .= "<hr style='margin: 15px 0 5px 0;height:6px;border: 0;background:#183459;'/>";
            $mail_header .= "</div>";

            $data_string = array(
              'from'    => CONS_API_LW_FROM,
              'to'      => $email,
              'subject' => $assunto,
              'body'    => $mail_header . $mensagem,
              'headers' => array('Content-type' => 'text/html')
            );

            $ch = curl_init(CONS_API_LW_URL);
            
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HEADER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data_string));
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            
            $response = curl_exec($ch);
            $cur_info = curl_getinfo($ch);
            curl_close($ch);

            if ($cur_info['http_code'] == "201") {
                return true;
            }
        } catch (Exception $e) {
            // to_debug($e);

            log_message('error', 'ERRO LOCAWEB HELPER - ' . $e);    
        }
        // $CI->session->set_flashdata('param_msg_erro_cabecalho', 'Erro ao Enviar E-mail');
        // $CI->session->set_flashdata('param_msg_erro', 'Ocorreu um erro ao tentar enviar e-mail(s). Verifique se todas as informações do(s) e-mail(s) estavam corretas!');

        // redirect(base_url());

        return FALSE;
    }

}