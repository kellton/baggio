<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h3 class="espacamento-titulo-pagina">
            Comparação de Imóveis à Venda
        </h3>

        <hr>

        <div class="caixa-conteudo row">
            <?php if($this->input->cookie('baggio_comparar') != NULL) { ?>
            <?php   foreach ($imoveis as $indice => $imovel) { ?>
            <?php       if($imovel['id_tipo_negociacao'] == 1) { ?>
            <?php           $aux ='venda'; ?>
            <?php           $preco = $imovel['preco_venda']; ?>
            <?php           $cor = 'titulo-imovel-visualizar-venda'; ?>
            <?php           $separador = "vermelho"; ?>
            <?php           $titulo = $imovel['nome_bairro']; ?>
            <?php           $tipos_imovel = $imovel['tipos_imovel']; ?>
            <?php           if (!empty($tipos_imovel)) { ?>
            <?php               $tipo_principal = $tipos_imovel[0]; ?>
            <?php               if ($tipo_principal['id_sub_tipo_imovel'] != 1) { ?>
            <?php                   $tipo_imovel = $tipos_imovel[0]; ?>
            <?php                   $titulo = $tipo_imovel['subtipo_imovel'] 
                                    . ' para uso ' 
                                    . $tipo_imovel['tipo_imovel'] 
                                    . ' no ' 
                                    . $imovel['nome_bairro']; ?>
            <?php               } ?>
            <?php           } ?>

                <div class="caixa-cardboard <?php echo $aux; ?> col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <h3 class="<?php echo $cor; ?>">
                        <?php echo $titulo; ?>
                    </h3>

                    <p>
                        <small>Referência <?php echo $imovel['referencia']; ?></small>
                    </p>

                    <div class="cardboard">
                        <a class="link-imovel" 
                         href="<?php echo base_url('imovel/visualizar-imovel/') . $imovel['id_imovel']; ?>">
                            <div class="cardboard-informacoes">
                                <img class="img-fluid" 
                                 src="<?php echo $imovel['link_imagem'] ?>"/>
                                
                                <hr>
                                
                                <div class="cardboard-acoes row">
                                    <div class="acao-ignorar col-12 text-center">
                                        <a href="<?php echo base_url('imovel/remover-comparacao-venda/') . $imovel['id_imovel']; ?>" 
                                         data-toggle="tooltip" data-trigger="hover" data-html="true"  
                                         title="Remover Imóvel da Lista de Comparação">
                                            <i class="fa fa-ban" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </div>

                                <hr>

                                <div class="inf-propriedade">
                                    <p class="<?php echo $cor; ?> sub-titulo">
                                        Características
                                    </p>
                                    
                                    <hr class="separador <?php echo $separador; ?>">
                                    
                                    <div class="row">
                                        <div class="col cor-preta">
                                           <p>*Quarto(s):<b>&nbsp;<?php echo $imovel['qtd_quartos']; ?></b></p>
                                           <p>*Suíte(s):<b>&nbsp;<?php echo $imovel['qtd_suites']; ?></b></p>
                                           <p>*Banheiro(s):<b>&nbsp;<?php echo $imovel['qtd_banheiros']; ?></b></p>
                                           <p>*Vaga(s):<b>&nbsp;<?php echo $imovel['qtd_vagas_garagem']; ?></b></p>
                                           <p>*Área Útil:<b>&nbsp;<?php echo number_format($imovel['area_util'], 2).' m²'; ?></b></p>
                                           <p>*Área Total:<b>&nbsp;<?php echo number_format($imovel['area_total'], 2).' m²'; ?></b></p>
                                       </div>
                                    </div>

                                    <p class="<?php echo $cor; ?> sub-titulo">
                                        Condições de Negócio
                                    </p>

                                    <hr class="separador <?php echo $separador; ?>">

                                    <div class="row">
                                        <div class="col">
                                            <p class="cor-preta">Valor líquido para pagamento pontual:</p>
                                            <h3 class="<?php echo $cor; ?>">R$:&nbsp;<b><?php echo number_format($preco, 2, ',', '.'); ?></b> /mês</h3>
                                            <p class="cor-preta"><small>*valores sujeitos a alterações sem aviso prévio.</small></p>
                                            <label class="cor-preta"><b>Valores adicionais:</b></label>
                                            <div class="row cor-preta">
                                                <div class="col">
                                                    <label>Condomínio: R$ <?php echo number_format($imovel['valor_condominio'],2,',','.'); ?></label>
                                                </div>
                                                <div class="col">
                                                    <label>IPTU: R$ <?php echo number_format($imovel['valor_iptu'],2,',','.'); ?></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            <?php } ?>
            <?php } ?>
            <?php } ?>
        </div>
    </div>
</div>
