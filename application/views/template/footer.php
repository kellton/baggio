	</div>
	<div class="container-fluid caixa-footer background-azul-padrao">
		<div class="container">
			<div class="row">
				<div class="caixa-enderecos col-12">
					<div class="row">
						<div class="caixa-endereco col-6 col-md-4 text-left">
							<label class="fonte-peq-media">Vila Izabel</label>
							<a href="tel:4130777707" 
							 class="fonte-pequena link-telefone">
								<i class="fa fa-phone" aria-hidden="true"></i>
								&nbsp;&nbsp;(41) 3077-7707
							</a>
							<a href="https://goo.gl/maps/mCnF2BGn93r" target="_blank"
							 class="fonte-pequena link-telefone">
								<i class="fa fa-map-marker" aria-hidden="true"></i>
								&nbsp;&nbsp;Rua Prof. Ulisses Vieira, 894
							</a>
						</div>

						<div class="caixa-endereco col-6 col-md-4 text-left">
							<label class="fonte-peq-media">Campo Comprido</label>
							<a href="tel:4130152727" 
							 class="fonte-pequena link-telefone">
								<i class="fa fa-phone" aria-hidden="true"></i>
								&nbsp;&nbsp;(41) 3015-2727
							</a>
							<a href="https://goo.gl/maps/KwWdaxRuw792" target="_blank" 
							 class="fonte-pequena link-telefone">
								<i class="fa fa-map-marker" aria-hidden="true"></i>
								&nbsp;&nbsp;Rua Eduardo Sprada, 3936
							</a>
						</div>

						<div class="d-md-none col-12">
							<hr class="separador padrao cinza"/>
						</div>

						<!-- XS SM -->
						<div class="d-md-none col-12 form-group">
							<div class="row">
								<div class="col-12">
									<label class="fonte-peq-media">Horário de Funcionamento</label>
								</div>
							</div>
							<div class="row">
								<div class="col-12">
									<label class="fonte-pequena">
										<i class="far fa-clock" aria-hidden="true"></i>
										&nbsp;&nbsp;De segunda a sexta das 08:00 às 18:00
									</label>
								</div>
								<div class="col-12">
									<label class="fonte-pequena">
										<i class="far fa-clock" aria-hidden="true"></i>
										&nbsp;&nbsp;Sábados das 09:00 às 12:00
									</label>
								</div>
							</div>
						</div>
						<!-- MD LG XL -->
						<div class="d-none d-md-block col-md-4">
							<div class="row d-none d-lg-block">
								<div class="col-12">
									<label class="fonte-peq-media">Horário de Funcionamento</label>
								</div>
							</div>
							<div class="row">
								<div class="col-12">
									<label class="fonte-pequena">
										<i class="far fa-clock" aria-hidden="true"></i>
										&nbsp;&nbsp;De segunda a sexta das 08:00h às 18:00h
									</label>
								</div>
								<div class="col-12">
									<label class="fonte-pequena">
										<i class="far fa-clock" aria-hidden="true"></i>
										&nbsp;&nbsp;Sábados das 09:00h às 12:00h
									</label>
								</div>
							</div>
						</div>
					</div>

					<div class="row d-md-none">
						<div class="col text-left">
							<div class="row">
								<div class="col">
									<div class="row">
										<div class="col text-left">
											<a href="https://api.whatsapp.com/send?phone=5541998219449"
											 class="fonte-pequena link-whatsapp">
												<i class="fa fa-whatsapp" aria-hidden="true"></i>
												<b>+55 41 99821-9449</b>
											</a>
										</div>
									</div>
									<div class="row">
										<div class="col fonte-pequena posicao-menos-5">
											Locação
										</div>
									</div>
								</div>
								<div class="col">
									<div class="row">
										<div class="col text-left">
											<a href="https://api.whatsapp.com/send?phone=5541996770057"
											 class="fonte-pequena link-whatsapp">
												<i class="fa fa-whatsapp" aria-hidden="true"></i>
												<b>+55 41 99677-0057</b>
											</a>
										</div>
									</div>
									<div class="row">
										<div class="col fonte-pequena posicao-menos-5">
										Vendas
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row no-gutters">
			<div class="col">
				<hr class="separador padrao cinza sem-top sem-bottom"/>
			</div>
		</div>

		<div class="container caixa-redes-sociais d-print-none">
			<div class="row">
				<div class="col background-azul-padrao text-center">
					<a href="//www.facebook.com/BaggioImoveis.com.br" target="_blank"
					title="Compartilhe também o nosso Facebook">
						<i class="fab fa-facebook" aria-hidden="true"></i>
					</a>
					<a href="//twitter.com/Baggioimoveis" target="_blank"
					title="Comente também o nosso Twitter">
						<i class="fab fa-twitter-square" aria-hidden="true"></i>
					</a>
					<a href="//instagram.com/baggioimoveis" target="_blank"
					title="Curta também o nosso Instagram">
						<i class="fab fa-instagram" aria-hidden="true"></i>
					</a>
					<a href="http://www.baggioimoveis.com.br/blog" target="_blank">
						<i class="fa fa-rss-square" aria-hidden="true"></i>
					</a>
				</div>
			</div>
		</div>
	</div>

	<div class="container-fluid caixa-parceiros d-print-none">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-4 offset-sm-2 offset-md-3 offset-lg-4">
					<div class="row">
						<div class="col-4 text-center">
							<img src="<?php echo base_url('publico/imagens/cvi-logo.png')?>" width="100%">
						</div>
						<div class="col-4 text-center">
							<img src="<?php echo base_url('publico/imagens/rede-imoveis-logo.png')?>" width="100%">
						</div>
						<div class="col-4 text-center">
							<img src="<?php echo base_url('publico/imagens/rede-solidaria-logo.png')?>" width="100%">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div>
		<!--<script src="<?php echo base_url('publico/js/libs/jquery-slim.min.js')?>"></script>-->
		<!--<script src="<?php echo base_url('publico/js/libs/jquery-3.2.1.min.js')?>"></script>-->
		<!--<script src="<?php echo base_url('publico/js/libs/jquery-ui/jquery-ui.min.js')?>"></script>-->
		<!--<script src="<?php echo base_url('publico/js/libs/popper.min.js')?>"></script>-->
		<!--<script src="<?php echo base_url('publico/js/libs/bootstrap.min.js')?>"></script>-->
		<!--<script src="<?php echo base_url('publico/js/libs/jquery-mask/jquery.mask.js')?>"></script>-->
		<!--<script src="<?php echo base_url('publico/js/libs/easy-autocomplete/jquery.easy-autocomplete.min.js')?>"></script>-->
		<!--<script src="<?php echo base_url('publico/js/libs/slider/lightslider.js')?>"></script>-->
		<!--<script src="<?php echo base_url('publico/js/libs/tether/tether.min.js')?>"></script>-->
		<!--<script src="<?php echo base_url('publico/js/libs/bootstrap-confirmation.min.js')?>"></script>-->

		<!-- <script src="<?php echo base_url('publico/js/geral.min.js')?>"></script> -->
		<!--<script src="<?php echo base_url('publico/js/geral.js')?>"></script>-->
		<!--<script src="<?php echo base_url('publico/js/mascaras.js')?>"></script>-->
		<!-- <script src="<?php echo base_url('publico/js/datepicker.min.js')?>"></script> -->
		<!--<script src="<?php echo base_url('publico/js/datepicker.js')?>"></script>-->
		<!-- <script src="<?php echo base_url('publico/js/togvis.min.js')?>"></script> -->
		<!--<script src="<?php echo base_url('publico/js/togvis.js')?>"></script>-->
		<!-- <script src="<?php echo base_url('publico/js/adm/modais.min.js')?>"></script> -->
		<!--<script src="<?php echo base_url('publico/js/adm/modais.js')?>"></script>-->
		<!-- <script src="<?php echo base_url('publico/js/adm/busca.min.js')?>"></script> -->
		<!--<script src="<?php echo base_url('publico/js/adm/busca.js')?>"></script>-->
		<!-- <script src="<?php echo base_url('publico/js/autocomplete.min.js')?>"></script> -->
		<!--<script src="<?php echo base_url('publico/js/autocomplete.js')?>"></script>-->
		<!--<script src="<?php echo base_url('publico/js/adm/validacao_formularios.js')?>"></script>-->
		<!--<script src="<?php echo base_url('publico/js/libs/slick-1.8.0/slick/slick.min.js')?>"></script>-->
		<!--<script src="<?php echo base_url('publico/js/slick.js')?>"></script>-->
		<!--<script src="<?php echo base_url('publico/lightbox/ekko-lightbox.min.js')?>"></script>-->
		<!--<script src="<?php echo base_url('publico/lightbox/acao.js')?>"></script>-->
	</div>
    <div class="row d-none d-lg-flex">
        <a href="#" onclick="window.open('https://www.baggioimoveis.com.br/livezilla/chat.php', 'Pagina', 'STATUS=NO, TOOLBAR=NO, LOCATION=NO, DIRECTORIES=NO, RESISABLE=NO, SCROLLBARS=YES, TOP=10, LEFT=10, WIDTH=770, HEIGHT=400');">
          <div class="botao-chat-fixo">
            Atendimento Online
          </div>
        </a>
    </div>

</body>
</html>