<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container alerta-transparente">
    <?php if(!empty($this->session->flashdata('param_msg'))) { ?>
    <div class="alert alert-info alert-dismissible fade show" 
     role="alert" <?php echo ( $this->session->flashdata('param_msg_sem_tempo') ? "" : "alert-autoclose='3'" ); ?>>
        <div class="row">
            <div class="col-10">
                <label><?php echo $this->session->flashdata('param_msg'); ?></label>
            </div>
            <div class="col-2">
                <button type="button" class="close" data-dismiss="alert" 
                 aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    </div>
    <?php } ?>

    <?php if (!empty($this->session->flashdata('param_msg_sucesso'))) { ?>
    <div class="alert alert-success alert-dismissible fade show" 
     role="alert" <?php echo ( $this->session->flashdata('param_msg_sem_tempo') ? "" : "alert-autoclose='3'" ); ?>>
        <div class="row">
            <?php if (!empty($this->session->flashdata('param_msg_sucesso_cabecalho'))) { ?>
            <div class="col-10">
                <h4 class="alert-heading">
                    <?php echo $this->session->flashdata('param_msg_sucesso_cabecalho'); ?>
                </h4>
            </div>
            <div class="col-2">
                <button type="button" class="close" data-dismiss="alert" 
                 aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="col-12">
                <label><?php echo $this->session->flashdata('param_msg_sucesso'); ?></label>
            </div>
            <?php } else { ?>
            <div class="col-10">
                <label><?php echo $this->session->flashdata('param_msg_sucesso'); ?></label>
            </div>
            <div class="col-2">
                <button type="button" class="close" data-dismiss="alert" 
                 aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php } ?>
        </div>
    </div>
    <?php } ?>

    <?php if (!empty($this->session->flashdata('param_msg_erro'))) { ?>
    <div class="alert alert-danger alert-dismissible fade show" 
     role="alert" <?php echo ( $this->session->flashdata('param_msg_sem_tempo') ? "" : "alert-autoclose='3'" ); ?>>
        <div class="row">
            <?php if (!empty($this->session->flashdata('param_msg_erro_cabecalho'))) { ?>
            <div class="col-10">
                <h4 class="alert-heading">
                    <?php echo $this->session->flashdata('param_msg_erro_cabecalho'); ?>
                </h4>
            </div>
            <div class="col-2">
                <button type="button" class="close" data-dismiss="alert" 
                 aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="col-12">
                <label><?php echo $this->session->flashdata('param_msg_erro'); ?></label>
            </div>
            <?php } else { ?>
            <div class="col-10">
                <label><?php echo $this->session->flashdata('param_msg_erro'); ?></label>
            </div>
            <div class="col-2">
                <button type="button" class="close" data-dismiss="alert" 
                 aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php } ?>
        </div>
    </div>
    <?php } ?>
</div>
