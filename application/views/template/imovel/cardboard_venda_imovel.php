<?php if (isset($imovel)) { ?>
<?php $tipos_imovel = $imovel["tipos_imovel"]; ?>
<?php $valor = ( $imovel['preco_venda'] > 0 ) ? $imovel['preco_venda'] : $imovel['valor_negociacao']; ?>
<div class="caixa-cardboard venda">
	<div class="cardboard">
		<a class="link-imovel" 
		 href="<?php echo base_url('imovel/visualizar-imovel/') . $imovel['id_imovel']; ?>"  target="_blank">
			<div class="cardboard-informacoes">
				<?php if (array_key_exists('link_imagem', $imovel)) { ?>
					<div class="caixa-imagem-imovel" 
					 style="background-image:url(<?php echo $imovel['link_imagem'] ?>);">
					</div>
			    <?php } else { ?>
			    	<div class="caixa-imagem-imovel" 
					 style="background-image:url('<?php echo base_url("publico/imagens/img-padrao-apartamento.jpg"); ?>');">
					</div>
			   	<?php } ?>
				
				<div class="inf-propriedade">
					<div class="row inf-titulo">
						<div class="text-center">
							<?php foreach ($tipos_imovel as $indice => $tipo) { ?>
							<?php 	if ($tipo['id_sub_tipo_imovel'] != 1) { ?>
							<?php 		echo $tipo['subtipo_imovel']; ?>
							<?php 		echo ($indice > 0 ? " | " : ""); ?>
							<?php 	} ?>
							<?php } ?>
							<?php echo 'R$ ' . number_format( $valor, 2, ",", "."); ?>
						</div>
					</div>

					<div class="inf-endereco text-left form-group">
                   		<small>
                   			<span>Ref:&nbsp;</span>
                   			<?php echo $imovel['referencia']; ?>
                   		</small>
                   	</div>

					<div class="inf-principais text-justify">
						<?php foreach ($tipos_imovel as $indice => $tipo) { ?>
						<?php 	echo $tipo['tipo_imovel']; ?>
						<?php 	echo ($indice > 0 ? " | " : ""); ?>
						<?php 	if ($tipo['id_tipo_imovel'] == 1) { ?>
						<?php 		break; ?>
						<?php 	} ?>
						<?php } ?>
						<?php if (!empty($tipos_imovel)) { ?>
							&bull;
                        <?php } ?>
						<?php echo number_format( $imovel['area_total'], 2) .'m²'; ?>
					</div>
					
                    <div class="inf-endereco text-justify">
                    	<?php echo $imovel['bairro']; ?>
                    	,
                    	<?php echo $imovel['cidade']; ?>
                   	</div>

					<div class="inf-extras row">
						<div class="numero-banheiros col-4 text-center" title="Banheiros">
							<i class="fa fa-bath" aria-hidden="true"></i>
							<?php echo $imovel['qtd_banheiros']; ?>
						</div>
						<div class="vagas-garagem col-4 text-center" title="Vagas de garagem">
							<i class="fa fa-car" aria-hidden="true"></i>
							<?php echo $imovel['qtd_vagas']; ?>
						</div>
						<div class="numero-quartos col-4 text-center" title="Quartos">
							<i class="fa fa-bed" aria-hidden="true"></i>
							<?php echo $imovel['qtd_quartos']; ?>
						</div>
					</div>
				</div>
			</div>
		</a>
		<hr>
		<div class="cardboard-acoes row">
			<div class="acao-favoritar col-6 text-center">
				<a data-toggle="tooltip" data-trigger="hover" data-html="true"  
	     		 title="Adicionar aos favoritos." id-imovel="<?php echo $imovel['id_imovel']; ?>" onclick="favoritar(this);">
					<i class="fa fa-heart" aria-hidden="true"></i>
				</a>
			</div>
			<!-- <div class="acao-comparar col-4 text-center">
				<a data-toggle="tooltip" data-trigger="hover" data-html="true"  
	     		 title="Disponível em breve." style="cursor: not-allowed;"
				 href="<?php echo base_url('imovel/comparar-venda/' . $imovel['id_imovel']); ?>">
					<i class="fa fa-window-restore" aria-hidden="true"></i>
				</a>
			</div> -->
			<!--<div class="acao-ignorar col-6 text-center">
				<a data-toggle="tooltip" data-trigger="hover" data-html="true"  
	     		 title="Disponível em breve." onclick="return false;" style="cursor: not-allowed;"
				 href="#">
					<i class="fa fa-times-circle" aria-hidden="true"></i>
				</a>
			</div>-->
		</div>
	</div>
</div>
<?php } ?>
