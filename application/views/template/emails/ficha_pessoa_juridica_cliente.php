<div class="corpo-email-envio">
    Olá,
    <p><?php echo ucwords(strtolower($nome_cliente)); ?>,</p>
    <p>O Consultor(a) <?php echo $nome_corretor['nome_usuario']; ?> recebeu o envio da sua Ficha Pessoa Jurídica.</p>
    <p>Para acessar sua área no site da Baggio Imóveis, <a href="<?php echo base_url('cli-juridico/visualizar-ficha-cadastro')?>">clique aqui.</a></p>
    <p>Qualquer dúvida ou comentário, responda este e-mail.</p>
    <p>
        Baggio Imóveis
        <br>
        Rua Eduardo Sprada, 3936
        <br>
        (41) 3015-2727
        <br>
        facebook.com/BaggioImoveis.com.br
    </p>
</div>