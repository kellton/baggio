<div class="corpo-email-envio">
	Olá,

	<p>
		<?php echo ucwords(strtolower($nome_remetente)); ?> está te enviando o link do imóvel: <a href="<?php echo $url_imovel; ?>">Click aqui pra ver</a>.
	</p>
	<p>
		Mensagem que <?php echo ucwords(strtolower($nome_remetente)); ?> deixou:

		<p>
			<?php echo $mensagem; ?>
		</p>
	</p>
	<p>
		Permanecemos à disposição,
	</p>
	<p>
		Baggio Imóveis
	</p>
	<p>
		Rua Eduardo Sprada, 3936 | Campo Comprido | 41 3015-2727
		<br>
		Rua Prof. Ulisses Vieira, 894 | Vila Izabel | 41 3077-7707
		<br>
		WhatsApp Locações: 41 99821-9449
		<br>
		WhatsApp e Plantão de Vendas: 41 99677-0057
		<br>
		facebook.com/BaggioImoveis.com.br
	</p>
</div>

<style type="text/css">
	
</style>