<?php if (isset($breadcrumbs)) { ?>
<div class="container-fluid d-print-none">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
        <?php foreach ($breadcrumbs as $indice => $breadcrumb) { ?>
        <?php   $ultimo = ($indice + 1 == count($breadcrumbs) ); ?>
                <!-- HOME -->
                <?php if ($breadcrumb == "home") { ?>
                <?php   if ($ultimo) { ?>
                    <li class="breadcrumb-item active">Início</li>
                <?php   } else { ?>
                    <li class="breadcrumb-item">
                        <a href="<?php echo base_url(); ?>">
                            Início
                        </a>
                    </li>
                <?php   } ?>
                <?php } ?>
                <!-- BUSCA -->
                <?php if ($breadcrumb == "busca") { ?>
                <?php   if ($ultimo) { ?>
                    <li class="breadcrumb-item active">Busca</li>
                <?php   } else { ?>
                    <li class="breadcrumb-item">
                        <a href="<?php echo base_url('buscar/sessao/imoveis'); ?>">
                            Busca
                        </a>
                    </li>
                <?php   } ?>
                <?php } ?>
                <!-- VISUALIZAR IMOVEL -->
                <?php if ($breadcrumb == "visualizar-imovel") { ?>
                <?php   $uri = explode("/", $this->uri->uri_string()); ?>
                <?php   $id_imovel = $this->uri->segment(count($uri) + 1); ?>
                <?php   if ($ultimo) { ?>
                    <li class="breadcrumb-item active">Visualizar Imóvel</li>
                <?php   } else { ?>
                    <li class="breadcrumb-item">
                        <a href="<?php echo base_url('imovel/visualizar-imovel/'); ?>">
                            Visualizar Imóvel
                        </a>
                    </li>
                <?php   } ?>
                <?php } ?>
        <?php } ?>
        </ol>
    </nav>
</div>
<?php } ?>
