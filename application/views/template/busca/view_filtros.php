<div class="row">
	<div class="col">
		<div class="row">
			<!-- FAIXA DE PRECO -->
			<div class="col-12 col-md-6 col-xl-3 text-center">
				<div class="row">
					<div class="col">
						<i class="fas fa-dollar-sign" aria-hidden="true"></i>
						<label>Faixa de Preço - R$</label>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-6">
						<select class="form-control" 
						 filter-change="verificar-maior-preco" 
						 name="param_menor_preco"></select>
					</div>
					<div class="col-6">
						<select class="form-control" 
						 filter-change="verificar-menor-preco" 
						 name="param_maior_preco"></select>
					</div>
				</div>
			</div>
			<!-- AREA -->
			<div class="col-12 col-md-6 col-xl-3 text-center">
				<div class="row">
					<div class="col">
						<label>Área - m²</label>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-6">
						<select class="form-control" 
						 filter-change="verificar-maior-area" 
						 name="param_menor_area">
							<option value="0" selected="selected">
								0
							</option>
							<option value="50">50</option>
							<option value="100">100</option>
							<option value="200">200</option>
							<option value="500">500</option>
							<option value="700">700</option>
							<option value="1000">1000</option>
							<option value="1500">1500</option>
							<option value="2000">2000</option>
							<option value="2500">2500</option>
							<option value="3000">3000</option>
						</select>
					</div>
					<div class="col-6">
						<select class="form-control" 
						 filter-change="verificar-menor-area" 
						 name="param_maior_area">
						 	<option value="50">50</option>
							<option value="100">100</option>
							<option value="200">200</option>
							<option value="500">500</option>
							<option value="700">700</option>
							<option value="1000">1000</option>
							<option value="1500">1500</option>
							<option value="2000">2000</option>
							<option value="2500">2500</option>
							<option value="3000">3000</option>
							<option value="3500+" selected="">
								3500+
							</option>
						</select>
					</div>
				</div>
			</div>
			<!-- QUARTOS -->
			<div class="col-12 col-sm-4 col-xl-2 text-center">
				<div class="row">
					<div class="col">
						<i class="fa fa-bed" aria-hidden="true"></i>
						<label>Quartos</label>
					</div>
				</div>
				<div class="row form-group no-gutters">
					<div class="col-3">
						<button type="button" class="btn-sm btn-info comportamento-botao" 
		                 title="Diminuir quantidade de quartos"
		                 filter-click="diminuir-quartos">
		                    <i class="fa fa-minus" aria-hidden="true"></i>
		                </button>
		            </div>
		            <div class="col-6 caixa-filtro-number">
		                <input type="number" class="form-control text-center"
						 min="0" max="20" value="0" step="1"
						 name="param_qtd_quartos">
					</div>
					<div class="col-3">
		                <button type="button" class="btn-sm btn-info comportamento-botao" 
		                 title="Aumentar quantidade de quartos"
		                 filter-click="aumentar-quartos">
		                    <i class="fa fa-plus" aria-hidden="true"></i>
		                </button>
					</div>
				</div>
			</div>
			<!-- SUITES -->
			<div class="col-12 col-sm-4 col-xl-2 text-center">
				<div class="row">
					<div class="col">
						<i class="fa fa-shower" aria-hidden="true"></i>
						<label>Banheiros</label>
					</div>
				</div>
				<div class="row form-group no-gutters">
					<div class="col-3">
						<button type="button" class="btn-sm btn-info comportamento-botao" 
		                 title="Diminuir quantidade de banheiros"
		                 filter-click="diminuir-banheiros">
		                    <i class="fa fa-minus" aria-hidden="true"></i>
		                </button>
		            </div>
		            <div class="col-6 caixa-filtro-number">
		                <input type="number" class="form-control text-center"
						 min="0" max="20" value="0" step="1"
						 name="param_qtd_banheiros">
					</div>
					<div class="col-3">
		                <button type="button" class="btn-sm btn-info comportamento-botao" 
		                 title="Aumentar quantidade de banheiros"
		                 filter-click="aumentar-banheiros">
		                    <i class="fa fa-plus" aria-hidden="true"></i>
		                </button>
					</div>
				</div>
			</div>
			<!-- VAGAS -->
			<div class="col-12 col-sm-4 col-xl-2 text-center">
				<div class="row">
					<div class="col">
						<i class="fa fa-car" aria-hidden="true"></i>
						<label>Vagas</label>
					</div>
				</div>
				<div class="row form-group no-gutters">
					<div class="col-3">
						<button type="button" class="btn-sm btn-info comportamento-botao" 
		                 title="Diminuir quantidade de vagas"
		                 filter-click="diminuir-vagas">
		                    <i class="fa fa-minus" aria-hidden="true"></i>
		                </button>
		            </div>
		            <div class="col-6 caixa-filtro-number">
		                <input type="number" class="form-control text-center"
						 min="0" max="20" value="0" step="1"
						 name="param_qtd_vagas">
					</div>
					<div class="col-3">
		                <button type="button" class="btn-sm btn-info comportamento-botao" 
		                 title="Aumentar quantidade de vagas"
		                 filter-click="aumentar-vagas">
		                    <i class="fa fa-plus" aria-hidden="true"></i>
		                </button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
