
<?php $esconder = isset($esconder_tudo) && $esconder_tudo; ?>
<?php $tipo_busca = ( isset($param_tipo_busca) ? $param_tipo_busca : "1" ); ?>

<?php if (!$esconder) { ?>
<!-- ESCONDE BUSCA -->
<div class="row text-right espaco-top-2 espaco-bottom-2 d-block d-lg-none">
	<div class="col-12">
		<button id="elemento-botao-toggle-busca" type="button"  
		 class="btn btn-info comportamento-botao rounded-circle" 
		 aria-pressed="false" autocomplete="off">
			<!-- XS SM MD -->
			<label id="elemento-icon-toggle-mostrar-busca">
				<i class="fa fa-search-plus" aria-hidden="true"></i>
			</label>
			<label id="elemento-icon-toggle-esconder-busca">
				<i class="fa fa-search-minus" aria-hidden="true"></i>
			</label>
	    </button>
	</div>
</div>
<?php } ?>

<div <?php echo ( $esconder ? '' : "id='elemento-conteudo-toggle-busca'" ); ?> 
 class="row caixa-busca-input 
 <?php echo ( $esconder ? '' : 'caixa-busca-topo'); ?>">
	<div class="col">
		<form id="elemento-form-busca"
		 action="<?php echo base_url('buscar/imoveis'); ?>" method="POST" accept-charset="utf-8">
			<input id="elemento-busca-tipo" type="hidden" 
			 name="param_tipo_busca" value="<?php echo $tipo_busca; ?>"/>
			<input id="elemento-busca-inicio" type="hidden" 
			 name="param_inicio" value="0"/>

			<div class="row caixa-busca-conteudo">
				<div class="col-12 col-xl-10 form-group">
					<div class="tab-content" id="pills-tabContent">
						<div id="pills-conteudo-bairro" 
						 class="tab-pane fade show active"  
						 role="tabpanel" aria-labelledby="pills-busca-bairro">
							<?php echo $view_aba_bairro; ?>
						</div>
						<div id="pills-conteudo-referencia" 
						 class="tab-pane fade"  
						 role="tabpanel" aria-labelledby="pills-busca-referencia">
							<?php echo $view_aba_referencia; ?>
						</div>
						<div id="pills-conteudo-condominio" 
						 class="tab-pane fade" 
						 role="tabpanel" aria-labelledby="pills-busca-condominio">
							<?php echo $view_aba_condominio; ?>
						</div>
					</div>
				</div>
				<div class="col-12 offset-lg-3 col-lg-6 offset-xl-0 col-xl-2 form-group">
					<label for="elemento-busca-botao-buscar">&nbsp;</label>
					<button id="elemento-busca-botao-buscar"
					 type="<?php echo ($esconder ? 'submit' : 'button'); ?>" 
					 class="btn btn-info btn-block comportamento-botao"
					 data-toggle="tooltip" data-trigger="hover" data-html="true"  
	     		 	 title="Clique para realizar a busca de imóveis">
					 <i class="fa fa-search" aria-hidden="true"></i>
					 Buscar
					</button>
				</div>
			</div>
			<div class="row">
				<div class="col-12 offset-xl-5 col-xl-7 caixa-busca-pills">
					<?php echo $view_abas; ?>
				</div>
			</div>

			<?php if (!$esconder) { ?>
			<!-- FILTROS -->
			<div id="elemento-conteudo-toggle-filtros" class="row">
				<div class="col-12 caixa-busca-filtros">
					<?php echo $view_filtros; ?>
				</div>
			</div>
			<?php } ?>
		</form>

		<?php if (!$esconder) { ?>
		<!-- BOTAO MOSTRAR/ESCONDER FILTROS -->
		<div class="row caixa-busca-botao-filtros">
			<div class="col form-group">
				<button type="button" id="elemento-botao-toggle-filtros" 
				 class="btn btn-info comportamento-botao" 
				 data-toggle="tooltip" data-trigger="hover" data-html="true"  
				 	 title="Mostrar/Esconder Filtros"
				 data-toggle="button" aria-pressed="false" 
				 autocomplete="off">
					<span>Filtros</span>
					<!-- XS SM MD LG -->
					<i id="elemento-icon-toggle-mostrar-filtros" 
					 class="fa fa-arrow-down" 
					 aria-hidden="true"></i>
					<!-- XL -->
					<i id="elemento-icon-toggle-esconder-filtros" 
					 class="fa fa-arrow-up" 
					 aria-hidden="true"></i>
			    </button>
			</div>
		</div>
		<?php } ?>
	</div>
</div>
