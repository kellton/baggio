<!-- VERIFICA O TIPO DE NEGOCIACAO -->
<?php $active_locacao = ( isset($param_negociacao) && $param_negociacao == '2' ) ? "active" : ""; ?>
<?php $checked_locacao = ( isset($param_negociacao) && $param_negociacao == '2' ) ? "checked" : ""; ?>
<?php $active_venda = ( empty($active_locacao) ) ? "active" : ""; ?>
<?php $checked_venda = ( empty($active_locacao) ) ? "checked" : ""; ?>
<?php $selected_imoveis = (isset($param_tipos)) ? $param_tipos : $this->input->post("param_tipos"); ?>
<?php $selected_cidade = (isset($param_cidade)) ? $param_cidade : $this->input->post("param_cidade"); ?>

<div class="row caixa-busca-dropdown">
	<!-- XS SM MD LG -->
	<div class="col-12 form-group d-block d-xl-none text-center">
		<div id="radio-tipo-negociacao" 
		 class="btn-group" data-toggle="buttons">
			<label class="btn btn-info 
			 <?php echo $active_venda; ?> comportamento-botao">
				<input type="radio" name="param_negociacao" 
				 value="1" autocomplete="off" 
				 filter-change="verificar-dropdown-precos"
				 class="custom-control-input"
				 <?php echo $checked_venda; ?>>
				 Comprar
			</label>
			<label class="btn btn-info 
			 <?php echo $active_locacao; ?> comportamento-botao">
				<input type="radio" name="param_negociacao" 
				 value="2" autocomplete="off" 
				 filter-change="verificar-dropdown-precos"
				 class="custom-control-input"
				 <?php echo $checked_locacao; ?>>
				 &nbsp;&nbsp;Alugar&nbsp;&nbsp;
			</label>
		</div>
	</div>
	<!-- XL -->
	<div class="col-xl-3 form-group d-none d-xl-block text-left">
		<label for="radio-tipo-negociacao">Seu objetivo?</label>
		<div id="radio-tipo-negociacao" 
		 class="btn-group" data-toggle="buttons">
			<label class="btn btn-info 
			 <?php echo $active_venda; ?> comportamento-botao">
				<input type="radio" name="param_negociacao" 
				 value="1" autocomplete="off" 
				 filter-change="verificar-dropdown-precos"
				 class="custom-control-input"
				 <?php echo $checked_venda; ?>>
				 Comprar
			</label>
			<label class="btn btn-info 
			 <?php echo $active_locacao; ?> comportamento-botao">
				<input type="radio" name="param_negociacao" 
				 value="2" autocomplete="off" 
				 filter-change="verificar-dropdown-precos"
				 class="custom-control-input"
				 <?php echo $checked_locacao; ?>>
				 &nbsp;&nbsp;Alugar&nbsp;&nbsp;
			</label>
		</div>
	</div>
	<!-- TIPOS DE IMOVEL -->
	<div class="col-12 col-lg-4 col-xl-3 form-group">
		<div class="row">
			<div class="col">
				<label for="dropdown-bairro-tipos-imoveis">
					Que tipos de imóveis?
				</label>
				<select id="dropdown-bairro-tipos-imoveis"  
				 name="param_tipos[]" multiple>
				 	<option>Selecionar Todos</option>
				 	<optgroup label="Imóveis Comerciais">
				 	<?php foreach ($subtipos_imoveis as $indice => $subtipo) { ?>
					<?php 	if ($subtipo["id_tipo_imovel"] == 3) { ?>
				 	<?php 		$selected = ( in_array($subtipo['id_sub_tipo_imovel'], $selected_imoveis) ) ? "selected" : ""; ?>
					    <option value="<?php echo $subtipo['id_sub_tipo_imovel']; ?>"
					     <?php echo $selected; ?>>
					    	<?php echo $subtipo["subtipo_imovel"]; ?>
					    </option>
					<?php 	} ?>
					<?php } ?>
				 	</optgroup>
				 	<optgroup label="Imóveis Residenciais">
				 	<?php foreach ($subtipos_imoveis as $indice => $subtipo) { ?>
					<?php 	if ($subtipo["id_tipo_imovel"] == 2) { ?>
					<?php 		$selected = ( in_array($subtipo['id_sub_tipo_imovel'], $selected_imoveis) ) ? "selected" : ""; ?>
					    <option value="<?php echo $subtipo['id_sub_tipo_imovel']; ?>"
					     <?php echo $selected; ?>>
					    	<?php echo $subtipo["subtipo_imovel"]; ?>
					    </option>
					<?php 	} ?>
					<?php } ?>
				 	</optgroup>
				</select>
			</div>
		</div>
	</div>
	<!-- CIDADE -->
	<div class="col-12 col-lg-4 col-xl-3 form-group">
		<div class="row">
			<div class="col">
				<label for="dropdown-bairro-cidades">
					Em qual cidade?
				</label>
				<select id="dropdown-bairro-cidades" name="param_cidade">
			 	<?php foreach ($cidades as $indice => $cidade) { ?>
			 	<?php 	if (!$selected_cidade) { ?>
			 	<?php 		$selected = ( $cidade["nome_cidade"] == "Curitiba" ) ? "selected" : ""; ?>
			 	<?php 	} else { ?>
			 	<?php 		$selected = ( $cidade["id_cidade"] == $selected_cidade ) ? "selected" : ""; ?>
			 	<?php 	} ?>
				    <option value="<?php echo $cidade['id_cidade']; ?>"
				     <?php echo $selected; ?>>
				    	<?php echo $cidade["nome_cidade"]; ?>
				    </option>
				<?php } ?>
				</select>
			</div>
		</div>
	</div>
	<!-- BAIRRO -->
	<div class="col-12 col-lg-4 col-xl-3 form-group">
		<div class="row">
			<div class="col">
				<label for="dropdown-bairro-bairros-cidade">
					E quais bairros?
				</label>
				<select id="dropdown-bairro-bairros-cidade" 
				 name="param_bairros[]" multiple>
				 	<option class="group-all" value="0">
				 		Selecionar Todos
				 	</option>
				</select>
			</div>
		</div>
	</div>
</div>
