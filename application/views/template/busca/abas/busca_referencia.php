<?php $default_value = (isset($param_referencia) ? $param_referencia : ""); ?>
<div class="row">
	<div class="col-12 form-group">
		<label for="text-busca-referencia">Qual a referência do imóvel?</label>
		<input id="text-busca-referencia" 
		 type="text" class="form-control full-width" 
		 name="param_referencia" 
		 value="<?php echo set_value('param_referencia', $default_value); ?>"
		 placeholder="Digite a referência de um imóvel..." 
		 submit-enter/>
	</div>
</div>
