<ul class="nav nav-pills mb-3 float-right" id="pills-tab" role="tablist">
	<li class="nav-item">
		<a class="nav-link active" muda-tipo-busca="1" 
		 id="pills-busca-bairro" data-toggle="pill" 
		 href="#pills-conteudo-bairro" role="tab" 
		 aria-controls="pills-conteudo-bairro" 
		 aria-selected="true">
			Por Bairro
		</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" muda-tipo-busca="2"
		 id="pills-busca-referencia" data-toggle="pill" 
		 href="#pills-conteudo-referencia" role="tab" 
		 aria-controls="pills-conteudo-referencia" 
		 aria-selected="false">
			Por Código de Referência
		</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" muda-tipo-busca="3"
		 id="pills-busca-condominio" data-toggle="pill" 
		 href="#pills-conteudo-condominio" role="tab" 
		 aria-controls="pills-conteudo-condominio" 
		 aria-selected="false">
			Por Nome de Condomínio
		</a>
	</li>
</ul>
