<?php $default_value = (isset($param_condominio) ? $param_condominio : ""); ?>
<?php $selected_tipos_condominios = $this->input->post("param_tipos_condominios"); ?>

<div class="row">
	<div class="col-sm-12 col-lg-4 form-group">
		<!-- SELECT TIPO IMOVEIS -->
		<label for="dropdown-condominio-tipos-imoveis">
			Que tipos de imóveis?
		</label>
		<select id="dropdown-condominio-tipos-imoveis"
		 name="param_tipos_condominios[]" multiple>
		 	<option class="group-all" value="0">
		 		Selecionar Todos
		 	</option>
		 	<optgroup label="Imóveis Comerciais">
		 	<?php foreach ($subtipos_imoveis as $indice => $subtipo) { ?>
		 	<?php 	if ($subtipo["id_tipo_imovel"] == 3) { ?>
			<?php 		$selected = ( in_array($subtipo['id_sub_tipo_imovel'], $selected_tipos_condominios) ) ? "selected" : ""; ?>
			    <option value="<?php echo $subtipo['id_sub_tipo_imovel']; ?>"
			     <?php echo $selected; ?>>
			    	<?php echo $subtipo["subtipo_imovel"]; ?>
			    </option>
			<?php 	} ?>
			<?php } ?>
		 	</optgroup>
		 	<optgroup label="Imóveis Residenciais">
		 	<?php foreach ($subtipos_imoveis as $indice => $subtipo) { ?>
			<?php 	if ($subtipo["id_tipo_imovel"] == 2) { ?>
			<?php 		$selected = ( in_array($subtipo['id_sub_tipo_imovel'], $selected_tipos_condominios) ) ? "selected" : ""; ?>
			    <option value="<?php echo $subtipo['id_sub_tipo_imovel']; ?>"
			     <?php echo $selected; ?>>
			    	<?php echo $subtipo["subtipo_imovel"]; ?>
			    </option>
			<?php 	} ?>
			<?php } ?>
		 	</optgroup>
		</select>
	</div>
	<!-- INPUT BUSCA CONDOMINIO -->
	<div class="col-sm-12 col-lg-8 form-group">
		<label for="text-busca-condominio">Em que condomínio?</label>
		<input id="text-busca-condominio" 
		 type="text" class="form-control full-width" 
		 name="param_condominio" 
		 value="<?php echo set_value('param_condominio', $default_value); ?>"
		 placeholder="Digite o nome de um condomínio..." 
		 submit-busca/>
	</div>
</div>
