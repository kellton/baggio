<?php 
   /* $URI = preg_replace('/\/$|\/baggio\//i', '', $_SERVER['REQUEST_URI']);
    $inicio = empty($URI);
    $servicos = strpos($URI, 'servicos') !== FALSE;
    $admin = strpos($URI, 'adm') !== FALSE;
    $parceiros = $URI == 'parceiros';
    $portifolio = $URI == 'portifolio';
    $contato = $URI == 'contato';*/
?>

<!-- APENAS PARA IMPRESSAO -->
<div class="container-fluid d-none d-print-block">
    <br>
    <div class="row form-group">
        <div class="col">
            <img class="w-25" src="<?php echo base_url('publico/imagens/baggio-logo-30-anos.png'); ?>">
        </div>
    </div>
</div>

<div id="elemento-menu" class="d-print-none">
    <nav class="navbar  navbar-expand-md bg-faded navbar-light col-xs-12 col-sm-12 col-md-10 col-lg-10 offset-md-1 offset-lg-1">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Brand -->
        <a class="navbar-brand" href="<?php echo base_url(); ?>">
            <img width="160px" src="<?php echo base_url('publico/imagens/baggio-logo-30-anos.png'); ?>">
        </a>

        <div class="collapse navbar-collapse text-right" id="collapsibleNavbar">
            <!-- Links -->
            <ul class="navbar-nav">
                <!-- Dropdown -->
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url(); ?>">
                        Início
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                        Conheça a Baggio
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="<?php echo base_url('sobre-nos'); ?>">
                            Sobre Nós
                        </a>
                        <a class="dropdown-item" href="https://www.baggioimoveis.com.br/blog/">Nosso Blog</a>
                    </div>
                </li>

                <!-- Dropdown -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                        Aluguel
                    </a>
                    <div class="dropdown-menu">
                         <?php if(!(usuario_sessao_adm() || usuario_sessao_cliente())){ ?>
                            <a class="dropdown-item" href="<?php echo base_url('cadastro')?>"><span> Cadastre-se</span></a>
                        <?php }?>
                            <a class="dropdown-item" href="<?php echo base_url('adminstre-meu-imovel')?>">Administre Meu Imóvel</a>
                        <a class="dropdown-item" href="<?php echo base_url('documentacao-necessaria')?>">Documentação Necessária</a>
                        <?php if(usuario_sessao_cliente()) {
                            if(usuario_sessao_cliente()['cliente']==="Pessoa Física"){ $aux='cli'; ?>
                                <a class="dropdown-item muda-cor_menu-logado" href="<?php echo base_url('cli/ficha-cadastro');?>">Fichas Cadastrais</a>
                        <?php }else{?>
                            <a class="dropdown-item muda-cor_menu-logado" href="<?php echo base_url('cli-juridico/adicionar-ficha');?>">Fichas Cadastrais</a>
                        <?php }}?>
                        <?php if(usuario_sessao_cliente()) {
                            if(usuario_sessao_cliente()['cliente']==="Pessoa Física") $aux='cli';
                            else{ $aux='cli-juridico';}?>
                        <a class="dropdown-item muda-cor_menu-logado" href="<?php echo base_url($aux.'/documentacao');?>">Envio da Documentação</a>
                        <?php }?>
                        <a class="dropdown-item" href="<?php echo base_url('agendar-visita')?>">Agendar Visita</a>
                        <a class="dropdown-item" target="_blank" href="http://www.bluestarsul.com.br/baggioimoveis">Imprimir Boleto</a>
                    </div>
                </li>

                <!-- Dropdown -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                        Venda
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="<?php echo base_url('venda-meu-imovel')?>">Venda Meu Imóvel</a>
                        <a class="dropdown-item" href="<?php echo base_url('agendar-visita')?>">Agendar Visita</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url('contato'); ?>">
                        Contato
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url('trabalhe-conosco'); ?>">
                        Trabalhe Conosco
                    </a>
                </li>
            </ul>
        </div>
    </nav>
</div>

<!-- menu mobile -->
<div class="menu-mobile-container d-lg-none text-center d-print-none">
    <div class="row">
        <div class="col-item-mobile text-center">
            <a class="item-mobile" href="<?php echo base_url(); ?>">
                <i class="material-icons">home</i>
            </a>
        </div>
        <div class="col-item-mobile text-center">
            <a class="item-mobile" href="<?php echo base_url("favoritos"); ?>">
                <i class="material-icons">favorite</i>
            </a>
        </div>
        <div class="col-item-mobile text-center">
            <a class="item-mobile" href="<?php echo base_url("buscar/sessao/imoveis"); ?>">
                <i class="material-icons">search</i>
            </a>
        </div>
        <div class="col-item-mobile text-center">
            <a class="item-mobile" href="<?php echo base_url("contato"); ?>">
                <i class="material-icons">phone</i>
            </a>
        </div>
        <div class="col-item-mobile text-center">
            <a class="item-mobile" href="<?php echo base_url("login"); ?>">
                <i class="material-icons">person</i>
            </a>
        </div>
    </div>
</div>