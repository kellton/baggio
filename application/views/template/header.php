<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <title>Baggio Imóveis</title>

    <!-- Google Glyph Icons -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <!-- <link rel="icon" href="<?php echo base_url('publico/imagens/fiveicon.ico'); ?>" type="image/x-icon"> -->
    <link rel="icon" type="image/png" size="192x192" href="<?php echo base_url('publico/imagens/favicon-96x96.png'); ?>" type="image/x-icon">
    
   <!-- Google Analytics -->
	<script type="text/javascript">
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-36960680-1']);
		_gaq.push(['_trackPageview']);

		(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();
	</script>

	<!-- Google AdWords -->
	<!-- <div class="hidden">
            <script type="text/javascript">
                    /* <![CDATA[ */
                    var google_conversion_id = 970494603;
                    var google_custom_params = window.google_tag_params;
                    var google_remarketing_only = true;
                    /* ]]> */
            </script>
	</div> -->

    <!-- STYLES -->
    <?php if (isset($styles)) { ?>
    <?php   foreach ($styles as $style) { ?>
    <?php echo link_tag($style); ?>
    <?php   } ?>
    <?php } ?>
    <!-- SCRIPTS -->
    <?php if (isset($scripts)) { ?>
    <?php   foreach ($scripts as $script) { ?>
    <?php     echo "<script src='" . base_url($script) . "'></script>"; ?>
    <?php   } ?>
    <?php } ?>
  </head>
  <body>
  <div class="content">
    <div id="elemento-menu-topo" class="hidden-sm-down container-fluid d-print-none">
      <div class="header background-azul-padrao row">
        <div class="col">
          <!-- XS SM MD -->
          <!--<div class="row d-lg-none">
              <div class="text-center login col">
                <?php if(!(usuario_sessao_adm() || usuario_sessao_cliente())) { ?>
                  <a href="<?php echo base_url('login')?>">
                    <span>Login</span>
                  </a>
                <?php } ?>
                <?php if((usuario_sessao_adm() || usuario_sessao_cliente())) { ?>
                  <a href="<?php echo base_url('logout')?>">
                    <span>Sair</span>
                  </a>
                <?php } ?>
              </div>
          </div>-->
          <!-- LG XL -->
          <div class="row d-none d-lg-flex">
            <!-- TEXTO ESQUERDA -->
            <div class="fonte-pequena text-left login offset-1 col-4">
              <div class="row">
                <div class="col">
                 <?php if(!(usuario_sessao_adm() || usuario_sessao_cliente())){ ?>
                    <a class="btn btn-info btn-sm" href="<?php echo base_url('login')?>">
                      <span><i class="fas fa-sign-in-alt" aria-hidden="true"></i> Login </span>
                    </a>
                    <a class="btn btn-info btn-sm" href="<?php echo base_url('favoritos')?>">
                      <span><i class="fa fa-heart" aria-hidden="true"></i> Favoritos </span>
                    </a>
                  <?php } ?>
                  <?php if((usuario_sessao_adm() || usuario_sessao_cliente())){ ?>
                      <a class="btn btn-info btn-sm" href="<?php echo base_url('gerenciador')?>">
                        <span><i class="fa fa-user" aria-hidden="true"></i> Minha Área</span>
                      </a>
                      <a class="btn btn-info btn-sm" href="<?php echo base_url('favoritos')?>">
                        <span><i class="fa fa-heart" aria-hidden="true"></i> Favoritos </span>
                      </a>
                      <a class="btn btn-danger btn-sm"  href="<?php echo base_url('logout')?>">
                        <span><i class="fas fa-sign-out-alt" aria-hidden="true"></i> Sair</span>
                      </a>
                  <?php } ?>
                </div>
              </div>
            </div>
            <!-- TEXTO DIREITA -->
            <div class="col-7">
              <div class="row">
                <div class="offset-3 col-4">
                  <div class="row">
                    <div class="col text-center">
                      <a href="#" onclick="window.open('https://api.whatsapp.com/send?phone=5541998219449', 
                                  'Pagina', 'STATUS=NO, TOOLBAR=NO, LOCATION=NO, DIRECTORIES=NO, RESISABLE=NO, \n\
                                    SCROLLBARS=YES, TOP=10, LEFT=10, WIDTH=770, HEIGHT=400');"
                       class="fonte-pequena link-whatsapp">
                        <i class="fab fa-whatsapp" aria-hidden="true"></i>
                        <b>+55 41 99821-9449</b>
                      </a>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col fonte-pequena text-center posicao-menos-5">
                      Locação
                    </div>
                  </div>
                </div>
                <div class="col-4">
                  <div class="row">
                    <div class="col text-center">
                        <a href="#" onclick="window.open('https://api.whatsapp.com/send?phone=5541996770057', 
                                  'Pagina', 'STATUS=NO, TOOLBAR=NO, LOCATION=NO, DIRECTORIES=NO, RESISABLE=NO, \n\
                                    SCROLLBARS=YES, TOP=10, LEFT=10, WIDTH=770, HEIGHT=400');"
                       class="fonte-pequena link-whatsapp">
                        <i class="fab fa-whatsapp" aria-hidden="true"></i>
                        <b>+55 41 99677-0057</b>
                      </a>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col fonte-pequena text-center posicao-menos-5">
                      Vendas
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
