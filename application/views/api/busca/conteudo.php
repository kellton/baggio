<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container-fluid">
	<div class="row caixa-busca-conteudo">
		<div class="col-12 offset-xl-1 col-xl-10">
			<!-- ***** CARREGADOR DA BUSCA ***** -->
			<div class="row" loader-container="elemento-loader-imoveis">
				<div class="col-12 text-center caixa-busca-carregador">
					<img class="img-fluid"
					 src="<?php echo base_url('publico/imagens/gif-loader.gif'); ?>"/>
					<h4>Carregando resultado...</h4>
				</div>
			</div>
			<!-- ***** SEM RESULTADO DA BUSCA ***** -->
			<div id="elemento-sem-resultados" class="row align-items-center">
				<div class="col text-center">
					<i class="far fa-frown fa-4x"></i>
					<br>
					<label class="fonte-maior">
						Ops! Não foi possível achar nenhum resultado da sua busca.
					</label>
					<br>
					<label class="fonte-menor">
						Ajuste o filtro e refaça a busca para obter melhores resultados.
					</label>
				</div>
			</div>
			<!-- ***** CONTEUDO DA BUSCA ***** -->
			<div class="row espaco-top-medio container-resultado-imoveis"
			 loader-content="elemento-loader-imoveis" 
			 loader-url="<?php echo base_url('async/buscar/imoveis'); ?>">
			</div>
			<!-- ***** SUGESTÕES DE IMOVEIS ***** -->
			<div id="container-sugestoes-imoveis" class="row espaco-top-medio">
				
			</div>
		</div>
	</div>
</div>
