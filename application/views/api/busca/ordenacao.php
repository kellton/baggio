<div class="container-fluid">
	<!-- ***** QUANTIDADE DE REGISTROS & ORDENACAO ***** -->
	<div class="row caixa-busca-ordenacao preenchimento-top-1-porcento">
		<div class="col-12 offset-lg-1 col-lg-4">
			<label id="search-quantidade-resultado-imoveis" 
			 class="cor-azul fonte-media">
			</label>
		</div>
		<div class="col-12 offset-lg-3 col-lg-3 offset-xl-4 col-xl-2">
			<div class="form-group">
				<select class="form-control" loader-order="elemento-loader-order"
				 name="param_ordenacao" filter-order-change>
					<option value="0">Ordenação</option>
					<!-- <option value="1">Mais Recentes</option> -->
					<option value="2">Menor Valor</option>
					<option value="3">Maior Valor</option>
					<option value="4">Menor Área</option>
					<option value="5">Maior Área</option>
				</select>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-12 offset-lg-1 text-left">
			<div class="row">
				<div class="col-3 col-sm-2 col-md-1">
					<div class="copiar-pesquisa text-center" title="Copiar Link do Resultado" copiar-busca>
						<span>
							<i class="fa fa-copy"></i>
						</span>
					</div>
				</div>
				<div class="col-6 col-sm-3 col-md-3 col-lg-2 alert-box success">
					Link Copiado!
				</div>
				<div class="col-1">
					<input class="temp_textarea" id="temp_textarea"/>
				</div>
			</div>
		</div>
	</div>
</div>