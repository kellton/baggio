<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container">
    <div class="row">
        <div class="col">
            <?php echo validation_errors(
                '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>','</div>'); ?>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col">
            <div class="espaco-top-grande caixa">
                <form action="<?php echo base_url("adm/validacao")?>" method="POST" accept-charset="utf-8">
                    <div class="form-group">
                        <input class="form-control" name="email" placeholder="E-mail" value="<?php echo set_value('email')?>">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="senha" id="inputSenha" placeholder="Senha">
                    </div>
                    <button type="submit" class="btn btn-default">Entrar</button>
                </form>
            </div>
        </div>
    </div>
</div>