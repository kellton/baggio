<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h3 class="espacamento-titulo-pagina">Alteração de Senha</h3><hr>
        <?php if($this->session->flashdata('mensagem')) { ?>
        <?php   echo '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$this->session->flashdata('mensagem').'</div>'; ?>
        <?php }?>
        <p class="text-center">Para recuperar a sua senha será necessário inserir o seu email no campo abaixo.</p>
        <?php echo validation_errors('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>','</div>'); ?>
        <form action="<?php echo base_url('validacao-alteracao-senha?token='.$_GET['token']) ?>" method="POST" accept-charset="utf-8">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="senha">Senha:<span class="texto-campo-obrigatorio">*</span></label>
                        <input type="password" class="form-control" name="senha" placeholder="Senha" value="<?php echo set_value('email')?>" required>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="r_senha">Repetir Senha:<span class="texto-campo-obrigatorio">*</span></label>
                        <input type="password" class="form-control" name="r_senha" placeholder="Repetir Senha" value="<?php echo set_value('r_senha')?>" required>
                    </div>
                </div>
            </div>
            <hr>
            <button type="submit" class="btn btn-primary">Alterar Senha</button>
        </form>
    </div>
</div>