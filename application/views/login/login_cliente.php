<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-6">
            <?php echo validation_errors(
                '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>','</div>'); ?>
        <?php if($this->session->flashdata('mensagem')) { ?>
        <?php   echo '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$this->session->flashdata('mensagem').'</div>'; ?>
        <?php }?>
        </div>
    </div>
    <div class="row justify-content-center">
       <div class="col-xs-12 col-sm-12 col-md-8 col-lg-6">
            <div class="espaco-top-grande caixa">
                <h3><i class="fa fa-user"></i> Identificação</h3>
                <form action="<?php echo base_url("login/validacao")?>" method="POST" accept-charset="utf-8">
                    <div class="form-group">
                        <input class="form-control" name="email" placeholder="E-mail" value="<?php echo set_value('email')?>">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="senha" id="inputSenha" placeholder="Senha">
                    </div>
                    <div class="row justify-content-end">
                        <a href="<?php echo base_url('esqueci-senha') ?>" class="btn btn-link">Esqueceu a senha?</a>
                    </div>
                    <button type="submit" class="btn btn-default">Entrar</button>
                </form>
            </div>
        </div>
    </div>
</div>