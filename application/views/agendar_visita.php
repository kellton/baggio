<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h3 class="espacamento-titulo-pagina">Agendar Visita</h3><hr>
        <?php 
        if($this->session->flashdata('mensagem')){
            echo '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$this->session->flashdata('mensagem').'</div>';
        }?>
        <p class="text-center">Preencha os campos abaixo, que a Baggio Imóveis entrará em contato o mais breve possível.</p>
        <p class="text-center"><b>Todos os campos com (*) são de preenchimento obrigatório</b></p>
        <?php echo validation_errors('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>','</div>'); ?>
        <form action="<?php echo base_url('validacao-agendamento') ?>" method="POST" accept-charset="utf-8">
            <div class="form-group">
                <label for="nome">Nome:*</label>
                <input type="text" class="form-control" name="nome" placeholder="Nome" value="<?php echo set_value('nome')?>" required>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="email">E-mail:*</label>
                        <input type="email" class="form-control" name="email" placeholder="E-mail" value="<?php echo set_value('email')?>" required>
                    </div>
                </div>
                <div class="col"> 
                    <div class="form-group">
                        <label>Telefone:*</label>
                        <input type="text" class="form-control" name="telefone" mascara="tel" placeholder="Telefone" value="<?php echo set_value('telefone')?>" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col"> 
                    <div class="form-group">
                        <label>URL do Imovel: (Opcional)</label>
                        <input type="text" class="form-control" name="url" placeholder="URL do Imovel: (Opcional)" value="<?php echo set_value('url')?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="mensagem">Observações:*</label>
                <textarea rows="5" type="text" class="form-control" name="mensagem" required><?php echo set_value('mensagem')?></textarea>
            </div>
            <div class="row">
                <div class="col">
                    <?php echo $widget; ?>
                    <?php echo $script; ?>
                </div>
            </div>
            <hr>
            <button type="submit" class="btn btn-primary">Enviar Agendamento</button>
        </form>
    </div>
</div>
