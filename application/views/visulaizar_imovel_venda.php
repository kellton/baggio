<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $titulo = $query['nome_bairro']; ?>
<?php $subtipo_imovel = ""; ?>
<?php if (!empty($tipos_imovel)) { ?>
<?php   $tipo_principal = $tipos_imovel[0]; ?>
<?php   $subtipo_imovel = $tipo_principal['subtipo_imovel']; ?>
<?php   if ($tipo_principal['id_sub_tipo_imovel'] != 1) { ?>
<?php       $titulo = $tipo_principal['subtipo_imovel'] . ' para uso ' . $tipo_principal['tipo_imovel']  . ' no ' . $query['nome_bairro']; ?>
<?php   } ?>
<?php } ?>
<?php $valor_venda = (floatval($query["preco_locacao"]) > 0) ? number_format(floatval($query['preco_locacao']), 2, ',', '.') : number_format(floatval($query['valor_negociacao']), 2, ',', '.'); ?>

<!-- CONFIGURAÇÕES DO FACEBOOK -->
<!--<meta property="og:title"              content="<?php echo $titulo; ?>" />
<meta property="og:description"        content="<?php echo $query['logradouro']. ', '. $query['numero']. ' - '. $query['nome_bairro']. ' - '. $query['nome_cidade']; ?>" />
<meta property="og:image"              content="<?php echo $imagens[0]['link_imagem']; ?>" />-->

<!-- PAGINA DE IMPRESSAO -->
<div class="container-fluid d-none d-print-block">
    <!-- TITULO -->
    <div class="row">
        <div class="col">
            <br>
            <br>
            <h3><?php echo $titulo; ?></h3>
            <h5>
                <i class="fa fa-map-marker" aria-hidden="true"></i>
                <?php echo $query['logradouro']
                    . ', ' 
                    . $query['numero'] 
                    . ' - ' 
                    . $query['nome_bairro'] 
                    . ' - ' 
                    . $query['nome_cidade']; ?>
            </h5>
        </div>
    </div>
    <br>
    <!-- IMAGEM PRINCIPAL -->
    <div class="row">
        <div class="col text-center">
            <br>
            <img src="<?php echo $imagens[0]['link_imagem']; ?>" />
        </div>
    </div>

    <div class="page-break"></div>
    <br>
    <hr/>
    <!-- INFORMACOES PRINCIPAIS -->
    <div class="row">
        <div class="col">
            <h4>Informações principais</h4>
            <?php if (!empty($subtipo_imovel)) { ?>
                <p><i class="fa fa-home" aria-hidden="true"></i>&nbsp;<?php echo $subtipo_imovel; ?></p>
            <?php } ?>
            <p><i class="fa fa-money" aria-hidden="true"></i>&nbsp;Valor de Venda: R$ <?php echo $valor_venda; ?></p>
            <?php if($query['valor_condominio'] != 0) { ?>
                <p><i class="fa fa-money" aria-hidden="true"></i>&nbsp;Valor do Condomínio: R$ <?php echo number_format( $query['valor_condominio'], 2, ',', '.'); ?></p>
            <?php } ?>
            <?php if($query['valor_iptu'] != 0) { ?>
                <p><i class="fa fa-money" aria-hidden="true"></i>&nbsp;IPTU: R$ <?php echo number_format( $query['valor_iptu'], 2, ',', '.'); ?></p>
            <?php } ?>
            <?php if($query['area_util'] != 0) { ?>
                <p>
                    <i class="fa fa-th-large" aria-hidden="true"></i>
                    &nbsp;Área Útil:&nbsp;
                    <?php echo number_format( $query['area_util'], 2) .' m²'; ?>
                </p>
            <?php }?>
            <?php if($query['area_total'] != 0) { ?>
                <p>
                    <i class="fa fa-th-large" aria-hidden="true"></i>
                    &nbsp;Área Total:&nbsp;
                    <?php echo number_format( $query['area_total'], 2) .' m²'; ?>
                </p>
            <?php }?>
            <?php if($query['qtd_quartos'] != 0) { ?>
                <p>
                    <i class="fa fa-bed" aria-hidden="true"></i>
                    &nbsp;<?php echo $query['qtd_quartos']; ?>&nbsp;Quarto(s)
                </p>
            <?php } ?>
            <?php if($query['qtd_suites'] != 0) { ?>
                <p>
                    <i class="fa fa-shower" aria-hidden="true"></i>
                    <i class="fa fa-bed" aria-hidden="true"></i>
                    &nbsp;<?php echo $query['qtd_suites']; ?>&nbsp;Suite(s)
                </p>
             <?php } ?>
            <?php if($query['qtd_banheiros'] != 0) { ?>
                <p>
                    <i class="fa fa-bath" aria-hidden="true"></i>
                    &nbsp;<?php echo $query['qtd_banheiros']; ?>&nbsp;Banheiro(s)
                </p>
            <?php } ?>
             <?php if($query['qtd_vagas_garagem'] != 0) { ?>
                <p>
                    <i class="fa fa-car" aria-hidden="true"></i>
                    &nbsp;<?php echo $query['qtd_vagas_garagem']; ?>&nbsp;Vaga(s)
                </p>
             <?php } ?>
             <?php if($query['idade_imovel'] != 0) { ?>
                <p>
                    <i class="fa fa-calendar" aria-hidden="true"></i>
                    &nbsp;Idade do Imóvel:&nbsp;<?php echo $query['idade_imovel']; ?> 
                </p>
             <?php } ?>
        </div>
    </div>
    <br>
    <!-- DESCRICAO -->
    <div class="row">
        <div class="col">
            <h4>Descrição</h4>
            <p>
                <h5>Referência&nbsp;<?php echo $query['referencia']; ?></h5>
            </p>
            <p class="text-justify">
                <?php echo $query['descricao']; ?>
            </p>
        </div>
    </div>

    <div class="page-break"></div>
    <br>
    <hr/>
    <!-- LOCALIZACAO -->
    <div class="row">
        <div class="col">
            <h4>Localização</h4>
            <h6>
                <i class="fa fa-map-marker" aria-hidden="true"></i>
                <?php echo $query['logradouro'] 
                    . ', '
                    . $query['numero']
                    . ' - '
                    . $query['nome_bairro']
                    . ' - '
                    . $query['nome_cidade']; ?>
            </h6>
        </div>
    </div>
    
    <hr/>
    <!-- VALORES -->
    <div class="row">
        <div class="col">
            <h4>Valor de Venda</h4>
            <h5><b>R$ <?php echo $valor_venda; ?></b></h5>
            <hr>
            <div class="row">
                <?php if($query['area_util'] != 0) { ?>
                    <div class="col text-center">
                        <i class="fa fa-th-large fa-2x" aria-hidden="true"></i>
                        <br>
                        <span><small>Área Útil</small></span>
                        <br> 
                        <label>
                            <b><?php echo number_format( $query['area_util'], 2).' m²'; ?></b>
                        </label>
                    </div>
                <?php } ?>
                <?php if($query['qtd_quartos'] != 0) { ?>
                    <div class="col text-center">
                        <i class="fa fa-bed fa-2x" aria-hidden="true"></i>
                        <br>
                        <span><small>Quartos</small></span>
                        <br> 
                        <label>
                            <b><?php echo $query['qtd_quartos']; ?></b>
                        </label>
                    </div>
                <?php } ?>
                <?php if($query['qtd_banheiros'] != 0) { ?>
                    <div class="col text-center">
                        <i class="fa fa-bath fa-2x" aria-hidden="true"></i>
                        <br>
                        <span><small>Banheiros</small></span>
                        <br> 
                        <label>
                            <b><?php echo $query['qtd_banheiros']; ?></b>
                        </label>
                    </div>
                <?php } ?>
                <?php if($query['qtd_vagas_garagem'] != 0) { ?>
                    <div class="col text-center">
                        <i class="fa fa-car fa-2x" aria-hidden="true"></i>
                        <br>
                        <span><small>Vagas</small></span>
                        <br> 
                        <label>
                            <b><?php echo $query['qtd_vagas_garagem']; ?></b>
                        </label>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <br>
</div>

<!-- PAGINA DE IMOVEL -->
<div class="container-fluid d-print-none">
    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 offset-md-1 offset-lg-1 espacamento-titulo-pagina">
        <?php if($this->session->flashdata('mensagem')) { ?>
        <?php   echo '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$this->session->flashdata('mensagem').'</div>'; ?>
        <?php }?>
         <?php echo validation_errors('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>','</div>'); ?>
        

        <div class="row">
            <div class="col-md-8 col-lg-8 col-xs-12 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-8">
                                <h6 class="card-title">
                                    <?php echo $titulo; ?>
                                </h6>
                                <small class="card-text">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                    <?php echo $query['logradouro']
                                        . ', ' 
                                        . $query['numero'] 
                                        . ' - ' 
                                        . $query['nome_bairro'] 
                                        . ' - ' 
                                        . $query['nome_cidade']; ?>
                                </small>
                            </div>

                            <div class="offset-2 col-2">
                                <!--<div class="cardboard-acoes row form-group">
                                    <div class="acao-favoritar col-6 text-center">
                                        <a data-toggle="tooltip" data-trigger="hover" 
                                         data-html="true" 
                                         title="Gostei (Favoritando este imóvel ficará na sua lista de favoritos)" 
                                         id-imovel="<?php echo $query['id_imovel']; ?>" 
                                         onclick="favoritar(this);" 
                                         data-original-title="Adicionar aos favoritos.">
                                            <i class="fa fa-heart" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                    <div class="acao-ignorar col-6 text-center">
                                        <a data-toggle="tooltip" data-trigger="hover" 
                                         data-html="true" 
                                         title="Não gostei (O imóvel vai ter menor prioridade em suas buscas)" 
                                         style="cursor: not-allowed;" 
                                         href="#" data-original-title="Disponível em breve.">
                                            <i class="fa fa-times-circle" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </div>-->
                            </div>
                        </div>
                    </div>

                    <div class="card-footer"></div>

                    <div class="galeria-fotos form-group">
                        <ul id="gallery-fotos-imovel">
                            <?php foreach ($imagens as $value) { ?>
                            <li class="slide-gallery-fotos-imovel" data-thumb="<?php echo $value['link_miniatura'] ?>">
                                <a data-toggle="lightbox" data-gallery="example-gallery" href="<?php echo $value['link_imagem'] ?>">
                                    <img src="<?php echo $value['link_imagem'] ?>" />
                                </a>
                            </li>
                             <?php } ?>
                        </ul>
                    </div>
                </div>

                <?php if($query['video_youtube'] !== "") { ?>
                <?php   $newlink = str_replace("watch?v=", "embed/", $query['video_youtube']); ?>
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" 
                         src="<?php echo $newlink ?>" allowfullscreen>
                        </iframe>
                    </div>
                <?php } ?>

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <button data-toggle="modal" data-target="#agendarVisitaImovel"
                         class="btn btn-agendar-venda btn-lg btn-block detalhes-venda">
                            <i class="fa fa-calendar" aria-hidden="true"></i>
                            <span>
                                 Agendar Visita
                            </span>
                        </button>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <button data-toggle="modal" data-target="#maisIformacoes"
                         class="btn btn-agendar-venda btn-lg btn-block detalhes-venda">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                            <i class="fa fa-info" aria-hidden="true"></i>
                            <span>
                                Mais Informações
                            </span>
                        </button>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title text-secondary">Valor de Venda</h5>
                        <h4 class="card-text">
                            <b>R$ <?php echo $valor_venda; ?></b>
                        </h4>
                        <hr>
                        <div class="row">
                            <?php if($query['area_util'] != 0) { ?>
                                <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 text-center">
                                    <i class="fa fa-th-large fa-2x" aria-hidden="true"></i>
                                    <br>
                                    <span><small>Área Útil</small></span>
                                    <br> 
                                    <label>
                                        <b><?php echo number_format( $query['area_util'], 2).' m²'; ?></b>
                                    </label>
                                </div>
                            <?php } ?>
                            <?php if($query['qtd_quartos'] != 0) { ?>
                                <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 text-center">
                                    <i class="fa fa-bed fa-2x" aria-hidden="true"></i>
                                    <br>
                                    <span><small>Quartos</small></span>
                                    <br> 
                                    <label>
                                        <b><?php echo $query['qtd_quartos']; ?></b>
                                    </label>
                                </div>
                            <?php } ?>
                            <?php if($query['qtd_banheiros'] != 0) { ?>
                                <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 text-center">
                                    <i class="fa fa-bath fa-2x" aria-hidden="true"></i>
                                    <br>
                                    <span><small>Banheiros</small></span>
                                    <br> 
                                    <label>
                                        <b><?php echo $query['qtd_banheiros']; ?></b>
                                    </label>
                                </div>
                            <?php } ?>
                            <?php if($query['qtd_vagas_garagem'] != 0) { ?>
                                <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 text-center">
                                    <i class="fa fa-car fa-2x" aria-hidden="true"></i>
                                    <br>
                                    <span><small>Vagas</small></span>
                                    <br> 
                                    <label>
                                        <b><?php echo $query['qtd_vagas_garagem']; ?></b>
                                    </label>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="card espacamento-titulo-pagina">
                    <div class="card-body">
                        <button onclick="imprimirPagina()" class="btn btn-lg btn-block detalhes-venda">
                            <i class="fa fa-print" aria-hidden="true"></i>
                            <span>
                                Imprimir Ficha
                            </span>
                        </button>
                        <button data-toggle="modal" data-target="#enviarImovel" class="btn btn-lg btn-block detalhes-venda">
                            <i class="fa fa-envelope-o" aria-hidden="true"></i>
                        <span >
                            Enviar Por E-mail 
                        </span>
                        </button>
                    </div>
                </div>
            </div>
        </div>



        <hr>



        <div class="row">
            <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <h6 class="card-title">
                            Informações principais
                        </h6>
                        <?php if (!empty($subtipo_imovel)) { ?>
                            <p><i class="fa fa-home" aria-hidden="true"></i>&nbsp;<?php echo $subtipo_imovel; ?></p>
                        <?php } ?>
                        <p><i class="fa fa-money" aria-hidden="true"></i>&nbsp;Valor de Venda: R$ <?php echo $valor_venda; ?></p>
                        <?php if($query['valor_condominio'] != 0) { ?>
                            <p><i class="fa fa-money" aria-hidden="true"></i>&nbsp;Valor do Condomínio: R$ <?php echo number_format( $query['valor_condominio'], 2, ',', '.'); ?></p>
                        <?php } ?>
                        <?php if($query['valor_iptu'] != 0) { ?>
                            <p><i class="fa fa-money" aria-hidden="true"></i>&nbsp;IPTU: R$ <?php echo number_format( $query['valor_iptu'], 2, ',', '.'); ?></p>
                        <?php } ?>
                        <?php if($query['area_util'] != 0) { ?>
                            <p>
                                <i class="fa fa-th-large" aria-hidden="true"></i>
                                &nbsp;Área Útil:&nbsp;
                                <?php echo number_format( $query['area_util'], 2) .' m²'; ?>
                            </p>
                        <?php }?>
                        <?php if($query['area_total'] != 0) { ?>
                            <p>
                                <i class="fa fa-th-large" aria-hidden="true"></i>
                                &nbsp;Área Total:&nbsp;
                                <?php echo number_format( $query['area_total'], 2) .' m²'; ?>
                            </p>
                        <?php }?>
                        <?php if($query['qtd_quartos'] != 0) { ?>
                            <p>
                                <i class="fa fa-bed" aria-hidden="true"></i>
                                &nbsp;<?php echo $query['qtd_quartos']; ?>&nbsp;Quarto(s)
                            </p>
                        <?php } ?>
                        <?php if($query['qtd_suites'] != 0) { ?>
                            <p>
                                <i class="fa fa-shower" aria-hidden="true"></i>
                                <i class="fa fa-bed" aria-hidden="true"></i>
                                &nbsp;<?php echo $query['qtd_suites']; ?>&nbsp;Suite(s)
                            </p>
                         <?php } ?>
                        <?php if($query['qtd_banheiros'] != 0) { ?>
                            <p>
                                <i class="fa fa-bath" aria-hidden="true"></i>
                                &nbsp;<?php echo $query['qtd_banheiros']; ?>&nbsp;Banheiro(s)
                            </p>
                        <?php } ?>
                         <?php if($query['qtd_vagas_garagem'] != 0) { ?>
                            <p>
                                <i class="fa fa-car" aria-hidden="true"></i>
                                &nbsp;<?php echo $query['qtd_vagas_garagem']; ?>&nbsp;Vaga(s)
                            </p>
                         <?php } ?>
                         <?php if($query['idade_imovel'] != 0) { ?>
                            <p>
                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                &nbsp;Idade do Imóvel:&nbsp;<?php echo $query['idade_imovel']; ?> 
                            </p>
                         <?php } ?>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <h6 class="card-title">Descrição</h6>
                        <p>
                            <small>Referência&nbsp;<?php echo $query['referencia'] ?></small>
                        </p>
                        <p class="card-text text-justify">
                            <?php echo $query['descricao']?>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <hr>

        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12" >
                <div class="card">
                    <div class="card-body">
                        <h6 class="card-title">Localização</h6>
                        <small class="card-text">
                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                            <?php echo $query['logradouro'] 
                                . ', '
                                . $query['numero']
                                . ' - '
                                . $query['nome_bairro']
                                . ' - '
                                . $query['nome_cidade']; ?>
                        </small>
                    </div>
                    <!-- XS SM -->
                        <div id="map-small" class="d-block d-md-none form-group" style="width: 100%; height: 250px; margin-top:1%;"></div>
                        <!-- MD LG XL -->
                        <div id="map-big" class="d-none d-md-block form-group" style="width: 100%; height: 400px; margin-top:1%;"></div>
                </div>
            </div>
        </div>         

        <?php if(!empty($imoveis_sugeridos)) { ?>

            <hr>
            <h4 class="fonte-grande">
                Imóveis Semelhantes
            </h4>
            <div class="caixa-conteudo row justify-content-center">
                <?php   foreach ($imoveis_sugeridos as $imovel) { ?>
                    <div class="col-12 col-md-6 col-lg-4 col-xl-3">
                        <?php echo $imovel; ?>
                    </div>
                <?php   } ?>
            </div>

        <?php } ?>

    </div>
</div>

<script>
      function initMap() {
        var uluru = {
            lat: <?php echo $query['latitude']; ?>, 
            lng: <?php echo $query['longitude']; ?>
        };
        var image = '<?php echo base_url('publico/imagens/seta.png'); ?>';

        var mapSmall = new google.maps.Map(document.getElementById('map-small'), {
          zoom: 17,
          center: uluru,
        });
        var mapBig = new google.maps.Map(document.getElementById('map-big'), {
          zoom: 17,
          center: uluru,
        });

        new google.maps.Marker({
          position: uluru,
          map: mapBig,
          icon: image
        });

        new google.maps.Marker({
          position: uluru,
          map: mapSmall,
          icon: image
        });
      }
</script>

<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBWk4ydBLK0coABrX0Ni4KH9o1uvBCVgqk&callback=initMap">
</script>
