<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h3 class="espacamento-titulo-pagina">Contato</h3><hr>
        <?php if($this->session->flashdata('mensagem')) { ?>
        <?php   echo '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$this->session->flashdata('mensagem').'</div>'; ?>
        <?php }?>
        <p class="text-center">Você pode contatar-nos on-line. Preencha os campos abaixo que em breve retornaremos sua mensagem.</p>
        <p class="text-center">Ou envie uma mensagem diretamente para <b class="cor-laranja">contato@baggioimoveis.com.br</b>.</p>
        <p class="text-center"><b>Todos os campos com (<span class="texto-campo-obrigatorio">*</span>) são de preenchimento obrigatório</b></p>
        <?php echo validation_errors('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>','</div>'); ?>

        <form action="<?php echo base_url('validacao-contato') ?>" method="POST" accept-charset="utf-8">
            <div class="form-group">
                <label for="nome">Nome:<span class="texto-campo-obrigatorio">*</span></label>
                <input type="text" class="form-control" name="nome" placeholder="Nome" value="<?php echo set_value('nome')?>" required>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="email">E-mail:<span class="texto-campo-obrigatorio">*</span></label>
                        <input type="email" class="form-control" name="email" placeholder="E-mail" value="<?php echo set_value('email')?>" required>
                    </div>
                </div>
                <div class="col"> 
                    <div class="form-group">
                        <label>Telefone:<span class="texto-campo-obrigatorio">*</span></label>
                        <input type="text" class="form-control" name="telefone" mascara="tel" placeholder="Telefone" value="<?php echo set_value('telefone')?>" required>
                    </div>
                </div>
            </div>
          
            <div class="form-group">
            <?php 
                $tipo['']='Selecione o Assunto';
                $tipo['Quero alugar um imóvel']='Quero alugar um imóvel';
                $tipo['Quero comprar um imóvel']='Quero comprar um imóvel';
                $tipo['Angariação área de Vendas']='Angariação área de Vendas';
                $tipo['Angariação área de Locações']='Angariação área de Locações';
                $tipo['Proprietários']='Proprietários';
                $tipo['Financeiro']='Financeiro';
                $tipo['Inquilinos']='Inquilinos';
                $tipo['Manutenção']='Manutenção';
                $tipo['Rescisão']='Rescisão';
                $tipo['Renovação']='Renovação';
                $tipo['Marketing e Comunicação']='Marketing e Comunicação';
                $tipo['Outros']='Outros';

                ?>
              <label for="assunto">Assunto:</label>
             <?php echo form_dropdown('assunto', $tipo, set_value('assunto'), 'class="form-control"') ?>
            </div>
            <div class="form-group">
                <label for="mensagem">Mensagem:<span class="texto-campo-obrigatorio">*</span></label>
                <textarea rows="5" type="text" class="form-control" name="mensagem" required><?php echo set_value('mensagem')?></textarea>
            </div>
            <div class="row">
                <div class="col">
                    <?php echo $widget; ?>
                    <?php echo $script; ?>
                </div>
            </div>
            <hr>
            <button type="submit" class="btn btn-primary">Enviar Mensagem</button>
        </form>
    </div>
</div>
