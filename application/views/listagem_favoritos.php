<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h3 class="espacamento-titulo-pagina">
            Listagem de Imóveis Favoritos
        </h3>

        <hr>

        <div class="caixa-conteudo row">
            <?php if ($this->input->cookie('baggio_favoritar') != NULL) { ?>
            <?php   foreach ($imoveis as $imovel) { ?>
            <div class="col-12 col-md-6 col-lg-4 col-xl-3">
            <?php       echo $imovel; ?>
            </div>
            <?php   } ?>
            <?php } ?>
        </div>
    </div>
</div>
