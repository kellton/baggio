<div class="modal fade" id="maisIformacoes" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pedir Mais Inforações do Imóvel</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
     <form action="<?php echo base_url("imovel/validacao-mais-informacoes/".$query['id_imovel'])?>" method="POST" accept-charset="utf-8">
        <div class="modal-body">
            <p class="text-center"><b>Todos os campos com (<span class="texto-campo-obrigatorio">*</span>) são de preenchimento obrigatório</b></p>
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <label>Nome:*</label>
                        <input type="text" class="form-control" name="nome" placeholder="Nome" value="<?php echo set_value('nome')?>" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <div class="form-group">
                        <label>Telefone:*</label>
                        <input type="text" class="form-control" mascara="tel" name="telefone" placeholder="Telefone" value="<?php echo set_value('telefone')?>" required>
                    </div>
                </div>
                <div class="col-6">
                   <div class="form-group">
                        <label>E-mail:*</label>
                        <input type="email" class="form-control" name="email" placeholder="E-mail" value="<?php echo set_value('email')?>" required>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label>Mensagem:*</label>
                <textarea type="text" class="form-control" name="mensagem" rows="5" placeholder="Mensagem" required><?php echo set_value('mensagem')?></textarea>
            </div>
            <input hidden name="url_imovel" value="<?php echo base_url('imovel/visualizar-imovel/' . $query['id_imovel']); ?>"/>
            <div class="row">
                <div class="col">
                    <?php echo $widget; ?>
                    <?php echo $script; ?>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            <button type="submit" class="btn btn-primary">Enviar</button>
        </div>
    </form>
    </div>
  </div>
</div>