<div class="modal fade" id="enviarImovel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Enviar Imóvel por E-mail</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
     <form action="<?php echo base_url("imovel/validacao-envio-imovel-email/".$query['id_imovel'])?>" method="POST" accept-charset="utf-8">
        <div class="modal-body">
            <p class="text-center"><b>Todos os campos com (<span class="texto-campo-obrigatorio">*</span>) são de preenchimento obrigatório</b></p>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label>Seu Nome:*</label>
                        <input type="text" class="form-control" name="nome_remetente" placeholder="Seu Nome" value="<?php echo set_value('nome_remetente')?>" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <div class="form-group">
                        <label>Nome do Destinatário:*</label>
                        <input type="text" class="form-control" name="nome_destinatario" placeholder="Nome do Destinatário" value="<?php echo set_value('nome_destinatario')?>" required>
                    </div>
                </div>
                <div class="col-6">
                   <div class="form-group">
                        <label>E-mail do Destinatário:*</label>
                        <input type="email" class="form-control" name="email_destinatario" placeholder="E-mail do Destinatário" value="<?php echo set_value('email_destinatario')?>" required>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label>Mensagem:*</label>
                <textarea type="text" class="form-control" name="mensagem" rows="5" placeholder="Mensagem" required><?php echo set_value('mensagem')?></textarea>
            </div>
            <input hidden name="url_imovel" value="<?php echo base_url('imovel/visualizar-imovel/' . $query['id_imovel']); ?>"/>
            <div class="row">
                <div class="col">
                    <?php echo $widget; ?>
                    <?php echo $script; ?>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            <button type="submit" class="btn btn-primary">Enviar</button>
        </div>
    </form>
    </div>
  </div>
</div>