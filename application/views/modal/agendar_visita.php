<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

    <div class="modal fade" id="agendarVisitaImovel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Agendar Visita</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form action="<?php echo base_url('validacao-agendamento-imovel/'.$query['id_imovel']) ?>" method="POST" accept-charset="utf-8">
        <div class="modal-body">
                <div class="form-group">
                    <label for="nome">Nome:*</label>
                    <input type="text" class="form-control" name="nome" placeholder="Nome" value="<?php echo set_value('nome')?>" required>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="email">E-mail:*</label>
                            <input type="email" class="form-control" name="email" placeholder="E-mail" value="<?php echo set_value('email')?>" required>
                        </div>
                    </div>
                    <div class="col"> 
                        <div class="form-group">
                            <label>Telefone:*</label>
                            <input type="text" class="form-control" name="telefone" mascara="tel" placeholder="Telefone" value="<?php echo set_value('telefone')?>" required>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="mensagem">Observações:*</label>
                    <textarea rows="5" type="text" class="form-control" name="mensagem" required><?php echo set_value('mensagem')?></textarea>
                </div>
                <div class="row">
                    <div class="col">
                        <?php echo $widget; ?>
                        <?php echo $script; ?>
                    </div>
                </div>
                <hr>
                 <input hidden name="url" value="<?php echo base_url('imovel/visualizar-imovel/' . $query['id_imovel']); ?>"/>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            <button type="submit" class="btn btn-primary">Enviar Agendamento</button>
        </div>
        </form>
    </div>
  </div>
</div>
        
