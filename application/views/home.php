<div class="container-fluid">
	<div id="elemento-container-busca-inicial" 
	 class="row align-items-center caixa-busca-imoveis">
		<div class="centro-busca-imoveis col-12 offset-xl-1 col-xl-10">
            <?php echo $view_busca; ?>
		</div>
	</div>
</div>

<div class="sugestoes-imoveis">
	<!--<div class="ancora-sugestoes col-12 text-center">
		<a href="#sugestoes-imoveis" ><i class="fa fa-arrow-circle-down" aria-hidden="true"></i></a>
	</div>
    <a name="sugestoes-imoveis"></a>-->
	<div class="container">
		<div class="row">
			<div class="text-center offset-1 col-10 cor-branca">
				<label class="fonte-peq-media w-100">Sugestões para você</label>
				<label class="fonte-peq-media w-100">
					A Baggio Imóveis está sempre atenta ao que seus clientes procuram.
					</br>
					Confira os imóveis que separamos para você.
				</label>
			</div>
		</div>
	</div>
</div>

<div class="caixa-central container">
    <div class="caixa-primeira-apresentacao row">
        <div class="col-12 col-xl-6">
            <div class="caixa-titulo row">
                <div class="col-10 offset-1">
                    <h4>
                        <i class="far fa-bookmark" aria-hidden="true"></i> Locação
                    </h4>
                </div>
            </div>
            <div class="caixa-conteudo row">
                <div class="caixa-subtitulo col">
                    <div class="row">
                        <div class="col caixa-carousel">
                            <div class="carousel-imoveis">
                                <?php foreach ($imoveis_aluguel as $imovel) { ?>                                   
                                    <div class="slide-imovel">                                   
                                        <?php echo $imovel; ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-xl-6">
            <div class="caixa-titulo row">
                <div class="col-10 offset-1">
                    <h4>
                        <i class="fa fa-key" aria-hidden="true"></i> Venda
                    </h4>
                </div>
            </div>
             <div class="caixa-conteudo row">
                <div class="caixa-subtitulo col">
                    <div class="row">
                        <div class="col caixa-carousel">
                            <div class="carousel-imoveis">
                                <?php foreach ($imoveis_venda as $imovel) { ?>
                                    <div class="slide-imovel">                                   
                                        <?php echo $imovel; ?>
                                    </div>
                                <?php }?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>