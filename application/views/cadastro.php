<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h3 class="espacamento-titulo-pagina">Cadastro Para o Envio de Fichas de Cadastro e Documentação</h3><hr>
        <p class="text-center">Para uma melhor experiência no envio das suas fichas e documentos para locação, faça seu cadastro e encontre uma área exclusiva.</p>
        <p class="text-center">Lembre que esta é uma área especial para quem está em processo de locar um imóvel, caso você precise nos contatar para quaisquer outras dúvidas, 
            utilize a área de CONTATO clicando <a href="<?php echo base_url('contato')?>">aqui</a>.</p>
        <p class="text-center"><b>Todos os campos com (*) são de preenchimento obrigatório</b></p>
         <?php 
        if($this->session->flashdata('mensagem')){
            echo '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$this->session->flashdata('mensagem').'</div>';
        }?>
        <?php echo validation_errors('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>','</div>'); ?>
        <form action="<?php echo base_url('validacao-cadastro') ?>" method="POST" accept-charset="utf-8" id="needs-validation" novalidate>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="nome_usuario">Nome:*</label>
                        <input type="text" class="form-control" name="nome_cliente" id="nome_usuario" placeholder="Nome" value="<?php echo set_value('nome_cliente')?>" required>
                    </div>
                    <div class="invalid-feedback">
                        Insira o Seu Nome
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="email">E-mail:*</label>
                        <input type="email" class="form-control" name="email" id="email" placeholder="E-mail" value="<?php echo set_value('email')?>" required>
                    </div>
                    <div class="invalid-feedback">
                        Insira o Seu E-mail
                    </div>
                </div>
            </div>
             <div class="row">
                <div class="col-6">
                    <div class="form-group">
                        <label for="telefone">Telefone:</label>
                        <input type="text" class="form-control" name="telefone" mascara="cel" placeholder="Telefone" value="<?php echo set_value('telefone')?>">
                    </div>
                </div>
             </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label>Selecione o Tipo de Cliente</label>
                        <select name="tipo_cliente" class="form-control" togvis-select required>
                           <option value="" selected="selected">Selecione Um Tipo de Cliente</option>
                           <option value="Pessoa Fisica" >Pessoa Física</option>
                           <option value="Pessoa Jurídica" >Pessoa Jurídica</option>
                        </select>
                        <div class="invalid-feedback">
                            Insira o Tipo de Cliente
                        </div>
                    </div>
                </div>
                <?php 
                    $array_corretores['']='Selecione um Consultor(a)';
                    foreach ($corretores as $value) {
                        $array_corretores[$value->id_usuario]=$value->nome_usuario;
                    }
                ?>
                <div class="col">
                    <div class="form-group">
                      <label for="id_corretor">Consultor(a):</label>
                     <?php echo form_dropdown('id_corretor', $array_corretores, set_value('id_corretor'), 'class="form-control" required') ?>
                        <div class="invalid-feedback">
                            Insira o Seu Consultor(a)
                        </div>
                    </div>
                 </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="senha">Senha:</label>
                        <input type="password" class="form-control" name="senha" id="senha" placeholder="Senha" value="<?php echo set_value('senha')?>" required>
                        <div class="invalid-feedback">
                            Insira a Senha
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                      <label for="r_senha">Repetir Senha:</label>
                      <input type="password" class="form-control" name="r_senha" id="senha" placeholder="Repetir Senha" value="<?php echo set_value('r_senha')?>" required>
                        <div class="invalid-feedback">
                            Repita a Senha
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <?php echo $widget; ?>
                    <?php echo $script; ?>
                </div>
            </div>
            <hr>
            <button type="submit" class="btn btn-primary">Cadastrar</button>
        </form>
    </div>
</div>
