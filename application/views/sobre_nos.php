<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container">
    <h3 class="espacamento-titulo-pagina">Sobre Nós</h3><hr>
	<div class="row">
		<div class="col">
			<p>Imóveis para viver, investir e crescer.</p>
		</div>
	</div>
	<div class="row form-group">
		<div class="col text-justify">
			Uma empresa do setor imobiliário tem que ter o compromisso de buscar as melhores opções para seus clientes, gerar resultados e atuar com qualidade e rapidez. Compromisso que a Baggio Imóveis tem com cada um de seus clientes.

Seja residencial ou comercial, para locar, comprar ou vender, a Baggio Imóveis encontra o imóvel certo para que você possa investir com segurança ou simplesmente viver a vida. Descubra aqui um pouco mais sobre a Baggio e os motivos que fazem dela a melhor escolha.

		</div>
	</div>

	<div class="row form-group">
		<div class="col">
			<b>História</b>
		</div>
	</div>
	<div class="row form-group">
		<div class="col text-justify">
			A Baggio Imóveis foi fundada em 1987, como empresa familiar, a partir da identificação de oportunidades de atuação no setor imobiliário na região oeste de Curitiba.

O tempo passou e de lá para cá muita coisa mudou: a Baggio Imóveis cresceu e buscou a profissionalização para, mais que identificar oportunidades, criá-las. Hoje, a empresa é uma das dez imobiliárias mais lembradas do mercado e atua em toda a capital paranaense, com destaque para a mesma região oeste, onde nasceu. São mais de 50 colaboradores que prestam o atendimento de qualidade a proprietários, investidores e locatários.

A empresa é associada à Câmara de Valores Imobiliários do Paraná e ao SECOVI. Conta ainda com o Selo Platina do PEQI – Programa Excelência da Qualidade Imobiliária – concedido pelo SECOVI a empresas do segmento que investem em pessoas para buscar excelência e inovação: um reconhecimento que foi dado a poucas imobiliárias de Curitiba. A Baggio Imóveis integra também a Rede Imóveis, que reúne imobiliárias curitibanas para potencializar a atuação de cada empresa e criar mais oportunidades para os clientes.
		</div>
	</div>
	<div class="row form-group">
		<div class="col">
			<b>Missão</b>
		</div>
	</div>
	<div class="row form-group">
		<div class="col text-justify">
			“Garantir a excelência na prestação de serviços imobiliários, de forma segura e confiável, auxiliando o cliente na busca do melhor negócio e oferecendo aos colaboradores perspectivas de um futuro melhor.”
		</div>
	</div>
	<div class="row form-group">
		<div class="col">
			<b>Visão</b>
		</div>
	</div>
	<div class="row form-group">
		<div class="col">
			“Ser reconhecida como referência de qualidade nos serviços prestados no mercado imobiliário“.
		</div>
	</div>
	<div class="row form-group">
		<div class="col">
			<b>Valores</b>
		</div>
	</div>
	<div class="row form-group">
		<div class="col text-justify">
			Os valores da Baggio Imóveis são compromissos que a empresa assume perante o mercado para obter excelência todos os dias e atender às expectativas de seus clientes.

Melhoria Contínua
Proatividade
Transparência
Dignidade
Honestidade
 

Informação ao cliente: Valor aproximado dos impostos (17,70%) conforme fonte ibpt.
		</div>
	</div>
</div>
