<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="form-group row bottom-formulario-20">
	<a name="cadastro-imovel-fotos"></a>
	<!-- ********** FOTOS DO IMOVEL ********** -->
	<div class="col">
		<legend>Fotos do Imóvel</legend>
	</div>
</div>

<div class="form-group row bottom-formulario-20">
	<!-- ********** FOTOS ********** -->
	<?php if (!isset($param_fotos)) { ?>
    	<div id="elemento-campo-foto" class="offset-2 col-8 bottom-formulario-16">
			<input type="file" name="param_fotos[]">
		</div>
	<?php } else if (count($param_fotos) > 0) { ?>
    <?php   foreach ($param_fotos as $param_foto) { ?>
    			<div id="elemento-campo-foto" class="offset-2 col-8 bottom-formulario-16">
					<input type="file" name="param_fotos[]" 
					 value="<?php echo $param_foto; ?>">
				</div>
	<?php   } ?>
	<?php } ?>
	<div class="col-2">
        <button type="button" class="btn btn-primary" href="#"
         title="Adicionar nova foto" clonar-elemento="elemento-campo-foto">
            <i class="fa fa-plus" aria-hidden="true"></i>
        </button>
    </div>
</div>

<div class="row">
  <div class="offset-2 col-10">
    <hr class="separador padrao cinza"/>
  </div>
</div>
