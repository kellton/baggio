<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="form-group row bottom-formulario-20">
    <a name="cadastro-imovel-informacoes-complementares"></a>
    <!-- ********** INFORMACOES COMPLEMENTARES ********** -->
    <div class="col">
        <legend>Informações Complementares</legend>
    </div>
</div>

<div class="form-group row bottom-formulario-0">
    <!-- ********** ABAS ********** -->
    <div class="col">
        <nav>
            <div class="nav nav-tabs" role="tablist">
                <a class="nav-item nav-link active" 
                 id="nav-tab-mais-informacoes" data-toggle="tab" 
                 href="#nav-content-mais-informacoes" role="tab" 
                 aria-controls="nav-content-mais-informacoes" aria-selected="true">
                    Mais Informações
                </a>
                <a class="nav-item nav-link" 
                 id="nav-tab-especificacoes" data-toggle="tab" 
                 href="#nav-content-especificacoes" role="tab" 
                 aria-controls="nav-content-especificacoes" aria-selected="false">
                    Especificações
                </a>
                <a class="nav-item nav-link" 
                 id="nav-tab-infraestrutura" data-toggle="tab" 
                 href="#nav-content-infraestrutura" role="tab" 
                 aria-controls="nav-content-infraestrutura" aria-selected="false">
                    Infraestrutura
                </a>
                <a class="nav-item nav-link" 
                 id="nav-tab-valores-venda" data-toggle="tab" 
                 href="#nav-content-valores-venda" role="tab" 
                 aria-controls="nav-content-valores-venda" aria-selected="false">
                    Valores de Venda
                </a>
                <a class="nav-item nav-link" 
                 id="nav-tab-valores-locacao" data-toggle="tab" 
                 href="#nav-content-valores-locacao" role="tab" 
                 aria-controls="nav-content-valores-locacao" aria-selected="false">
                    Valores de Locação
                </a>
                <a class="nav-item nav-link" 
                 id="nav-tab-chaves" data-toggle="tab" 
                 href="#nav-content-chaves" role="tab" 
                 aria-controls="nav-content-chaves" aria-selected="false">
                    Chaves
                </a>
            </div>
        </nav>
    </div>
</div>

<div class="tab-content form-group row bottom-formulario-20">
<!-- ********** CONTEUDOS - MAIS INFORMACOES ********** -->
    <div id="nav-content-mais-informacoes" 
     class="col tab-pane fade show active" 
     role="tabpanel" aria-labelledby="nav-tab-mais-informacoes">
        <?php echo $tab_mais_informacoes; ?>
    </div>
<!-- ********** CONTEUDOS - ESPECIFICACOES ********** -->
    <div id="nav-content-especificacoes" 
     class="col tab-pane fade" 
     role="tabpanel" aria-labelledby="nav-tab-especificacoes">
        <?php echo $tab_especificacoes; ?>
    </div>
<!-- ********** CONTEUDOS - INFRAESTRUTURA ********** -->
    <div id="nav-content-infraestrutura" 
     class="col tab-pane fade" 
     role="tabpanel" aria-labelledby="nav-tab-infraestrutura">
        <?php echo $tab_infraestrutura; ?>
    </div>
<!-- ********** CONTEUDOS - VALORES DE VENDA ********** -->
    <div id="nav-content-valores-venda" 
     class="col tab-pane fade" 
     role="tabpanel" aria-labelledby="nav-tab-valores-venda">
        <?php echo $tab_valores_venda; ?>
    </div>
<!-- ********** CONTEUDOS - VALORES DE LOCACAO ********** -->
    <div id="nav-content-valores-locacao" 
     class="col tab-pane fade" 
     role="tabpanel" aria-labelledby="nav-tab-valores-locacao">
        <?php echo $tab_valores_locacao; ?>
    </div>
<!-- ********** CONTEUDOS - CHAVES ********** -->
    <div id="nav-content-chaves" 
     class="col tab-pane fade" 
     role="tabpanel" aria-labelledby="nav-tab-chaves">
       <?php echo $tab_chaves; ?>
    </div>
</div>
