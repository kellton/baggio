<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="form-group row bottom-formulario-20">
  <a name="cadastro-imovel-negociacao"></a>
  <!-- ********** NEGOCIACAO ********** -->
  <div class="col">
    <legend>
      Informações da Negociação
      <span class="texto-campo-publico fonte-padrao" 
       title="Todos os campos dessa seção poderão ser exibidos publicamente">
        <i class="fa fa-users" aria-hidden="true"></i>
      </span>
    </legend>
  </div>
</div>

<div class="form-group row bottom-formulario-0">
  <div class="col-12 col-sm-12 col-md-3 col-lg-3">
    <legend>
      Negociação <span class="texto-campo-obrigatorio">*</span>
    </legend>
  </div>
  <div class="offset-1 col-11 col-sm-11 offset-md-0 col-md-2 col-lg-2">
    <?php echo form_dropdown('param_id_tipo_negociacao', 
        $tipo_negociacoes, 
        set_value('param_id_tipo_negociacao', $imovel['id_tipo_negociacao']), 
        'class="custom-select w-100" required'); 
    ?>
    <div class="invalid-tooltip">Escolha um tipo de negociação</div>
  </div>
  <!-- ********** LOJA ********** -->
  <div class="col-12 col-sm-12 col-md-2 col-lg-2">
    <legend>
      Loja <span class="texto-campo-obrigatorio">*</span>
    </legend>
  </div>
  <div class="offset-1 col-11 col-sm-11 offset-md-0 col-md-2 col-lg-2">
    <?php echo form_dropdown('param_id_loja', 
        $lojas, 
        set_value('param_id_loja', $imovel['id_loja']), 
        'class="custom-select w-100" required'); 
    ?>
    <div class="invalid-tooltip">Escolha uma loja</div>
  </div>
</div>

<div class="row">
  <div class="offset-2 col-10">
    <hr class="separador padrao cinza"/>
  </div>
</div>

<div class="form-group row bottom-formulario-0">
  <!-- ********** TIPO DE IMOVEL ********** -->
  <div class="col-12 col-sm-12 col-md-3 col-lg-3">
    <legend>
      Tipo de Imóvel <span class="texto-campo-obrigatorio">*</span>
    </legend>
  </div>
  <div class="offset-1 col-11 col-sm-11 offset-md-0 col-md-3 col-lg-3">
    <?php echo form_dropdown('param_id_subtipo_imovel', 
        $subtipo_imoveis, 
        set_value('param_id_subtipo_imovel', $imovel['id_sub_tipo']), 
        'class="custom-select w-100" required');
    ?>
    <div class="invalid-tooltip">Escolha uma tipo de imóvel</div>
  </div>
  <!-- ********** SUBTIPO DE IMOVEL ********** -->
  <!-- <div class="offset-1 col-11 col-sm-11 offset-md-2 col-md-2 col-lg-2">
    <?php foreach ($tipo_imoveis as $tipo) { ?>
      <div class="form-check">
        <label class="form-check-label">
          <input type="radio" class="form-check-input" 
           name="param_id_tipo_imovel" 
           value="<?php echo $tipo['id_tipo_imovel']; ?>"
           <?php if ($tipo['id_tipo_imovel'] === '1') echo " checked"; ?>>
            <?php echo $tipo['nome_tipo']; ?>
        </label>
      </div>
    <?php } ?>
  </div> -->
</div>

<div class="row">
  <div class="offset-2 col-10">
    <hr class="separador padrao cinza"/>
  </div>
</div>
