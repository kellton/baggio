<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container-fluid">
    <div class="row">
        <div class="col">
            <?php echo validation_errors(
                '<div class="row"><div class="col"><div class="alert alert-danger alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>','</div></div></div>'); ?>
       </div>
   </div>
</div>

<div class="container">
    <div class="row bottom-formulario-16">
        <div class="col">
            <div class="row">
                <div class="offset-md-2 col-6 col-md-5">
                    <span class="texto-campo-obrigatorio"
                     title="Campos de preenchimento obrigatório">
                        * Campos obrigatórios
                    </span>
                </div>
                <div class="col-6 col-md-5">
                    <span class="texto-campo-publico">
                        <i class="fa fa-users" aria-hidden="true" 
                         title="Campos que poderão ser exibidos publicamente"></i> 
                         - Campos que poderão ser exibidos publicamente
                    </span>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2 caixa-form-cad-imovel-menu-lateral d-sm-none d-md-block">
            <ul class="nav flex-column sticky-top">
                <li class="nav-item">
                    <a rolagem="cadastro-imovel-negociacao" class="nav-link active" 
                     href="<?php echo base_url('adm/imoveis/adicionar-imovel/#cadastro-imovel-negociocao'); ?>">
                        Negociação
                    </a>
                </li>
                <li class="nav-item">
                    <a rolagem="cadastro-imovel-informacoes-basicas" class="nav-link" 
                     href="<?php echo base_url('adm/imoveis/adicionar-imovel/#cadastro-imovel-informacoes-basicas'); ?>">
                        Informações Básicas
                    </a>
                </li>
                <li class="nav-item">
                    <a rolagem="cadastro-imovel-veiculacao" class="nav-link"
                     href="<?php echo base_url('adm/imoveis/adicionar-imovel/#cadastro-imovel-veiculacao'); ?>">
                        Veiculação
                    </a>
                </li>
                <?php if (!$imovel['xml_atualizavel']) { ?>
                <li class="nav-item">
                    <a rolagem="cadastro-imovel-fotos" class="nav-link" 
                     href="<?php echo base_url('adm/imoveis/adicionar-imovel/#cadastro-imovel-fotos'); ?>">
                        Fotos do Imóvel
                    </a>
                </li>
                <?php } ?>
                <li class="nav-item">
                    <a rolagem="cadastro-imovel-informacoes-complementares" class="nav-link"
                     href="<?php echo base_url('adm/imoveis/adicionar-imovel/#cadastro-imovel-informacoes-complementares'); ?>">
                        Informações Complementares
                    </a>
                </li>
                <!-- <li class="nav-item">
                    <a rolagem="cadastro-imovel-material" class="nav-link"
                     href="<?php echo base_url('adm/imoveis/adicionar-imovel/#cadastro-imovel-material'); ?>">
                        Material de Apoio
                    </a>
                </li> -->
                <li class="nav-item">
                    <a rolagem="cadastro-imovel-dados-gerenciamento" class="nav-link"
                     href="<?php echo base_url('adm/imoveis/adicionar-imovel/#cadastro-imovel-dados-gerenciamento'); ?>">
                        Dados de Gerenciamento
                    </a>
                </li>
            </ul>
        </div>

        <div class="col-12 col-md-10">
          <form id="needs-validation" action="<?php echo base_url('adm/imoveis/atualizar-imovel/' . $imovel['id_imovel']); ?>" 
            method="POST" accept-charset="utf-8" enctype="multipart/form-data">
