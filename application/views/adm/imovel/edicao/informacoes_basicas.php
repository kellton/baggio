<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $valor_negociacao = number_format(floatval($imovel['valor_negociacao']), 2, ',', '.'); ?>

<div class="form-group row bottom-formulario-20">
  <a name="cadastro-imovel-informacoes-basicas"></a>
  <!-- ********** INFORMACOES BASICAS ********** -->
  <div class="col">
    <legend>
      Informações Básicas
      <span class="texto-campo-publico fonte-padrao" 
       title="Todos os campos dessa seção poderão ser exibidos publicamente">
        <i class="fa fa-users" aria-hidden="true"></i>
        - Exceto "Comissão"
      </span>
    </legend>
  </div>
</div>

<div class="form-group row bottom-formulario-20">
  <!-- ********** TITULO ********** -->
  <label for="text-titulo" class="col-2 col-form-label">
    Título <span class="texto-campo-obrigatorio">*</span>
  </label>
  <div class="col-10">
    <input class="form-control" type="text" name="param_titulo" 
     value="<?php echo set_value('param_titulo', $imovel['titulo']); ?>" 
     id="text-titulo" placeholder="Título do Anúncio do Imóvel">
  </div>
</div>

<div class="form-group row bottom-formulario-20">
  <!-- ********** CIDADE ********** -->
  <label for="drop-cidade" class="col-2 col-form-label">
    Cidade <span class="texto-campo-obrigatorio">*</span>
  </label>
  <div class="col-4">
    <?php echo form_dropdown('param_id_cidade', 
        $cidades, 
        set_value('param_id_cidade', $imovel['id_cidade']),
        array(
          "endereco-auto" => "cidade",
          "class" => "form-control"
        )); 
    ?>
  </div>
  <!-- ********** REFERENCIA ********** -->
  <label for="text-referencia" class="offset-1 col-2 col-form-label">
    Referência
  </label>
  <div class="col-3">
    <input class="form-control" type="text" name="param_referencia" 
     value="<?php echo set_value('param_referencia', $imovel['referencia']); ?>" 
     id="text-referencia" placeholder="Referência do Imóvel">
  </div>
</div>

<div class="form-group row bottom-formulario-20">
  <!-- ********** CEP ********** -->
  <label for="text-cep" class="col-2 col-form-label">
    CEP
  </label>
  <div class="col-5">
    <input class="form-control" type="text" mascara="cep" name="param_cep" 
     value="<?php echo set_value('param_cep', $imovel['cep']); ?>" 
     id="text-cep" placeholder="CEP do Imóvel" endereco-auto="keyup">
  </div>
  <div class="col-5">
    <a class="btn btn-primary btn-sm" href="#" 
     title="Obter endereço automático" endereco-auto="click">
      <i class="fa fa-crosshairs fa-2x" aria-hidden="true"></i>
    </a>
  </div>
</div>

<div class="form-group row bottom-formulario-20">
  <!-- ********** ENDERECO ********** -->
  <label for="text-endereco" class="col-2 col-form-label">
    Endereço
  </label>
  <div class="col-10">
    <input class="form-control" type="text" endereco-auto="logradouro" 
     value="<?php echo set_value('param_endereco_logradouro', $imovel['logradouro']); ?>" 
     id="text-endereco" name="param_endereco_logradouro" 
     placeholder="Logradouro do Imóvel" endereco-auto="keyup">
  </div>
</div>

<div class="form-group row bottom-formulario-20">
  <!-- ********** NUMERO ********** -->
  <label for="text-numero" class="col-2 col-form-label">
    Número
  </label>
  <div class="col-3">
    <input class="form-control" type="number" min="1" name="param_endereco_numero" 
     value="<?php echo set_value('param_endereco_numero', $imovel['numero']); ?>" 
     id="text-numero" placeholder="Número do Imóvel" endereco-auto="keyup">
  </div>
  <!-- ********** COMPLEMENTO ********** -->
  <label for="text-complemento" class="col-2 col-form-label">
    Complemento
  </label>
  <div class="col-5">
    <input class="form-control" type="text" endereco-auto="complemento" 
     value="<?php echo set_value('param_endereco_complemento', $imovel['complemento']); ?>" 
     id="text-complemento" name="param_endereco_complemento" 
     placeholder="Complemento do Endereço" endereco-auto="keyup">
  </div>
</div>

<div class="form-group row bottom-formulario-20">
  <!-- ********** LATITUDE ********** -->
  <label for="text-latitude-auto" class="col-2 col-form-label">
    Latitude
  </label>
  <div class="col-3">
    <input class="form-control" type="text" name="param_endereco_lat" 
     value="<?php echo set_value('param_endereco_lat', $imovel['latitude']); ?>" 
     id="text-latitude-auto" placeholder="Latitude">
  </div>
  <!-- ********** LONGITUDE ********** -->
  <label for="text-longitude-auto" class="col-2 col-form-label">
    Longitude
  </label>
  <div class="col-3">
    <input class="form-control" type="text" name="param_endereco_lng" 
     value="<?php echo set_value('param_endereco_lng', $imovel['longitude']); ?>" 
     id="text-longitude-auto" placeholder="Longitude">
  </div>
</div>

<div class="form-group row bottom-formulario-20">
  <!-- ********** BAIRRO ********** -->
  <label for="text-bairro" class="col-2 col-form-label">
    Bairro
  </label>
  <div class="col-4">
    <input class="form-control" type="text" endereco-auto="bairro"
     value="<?php echo set_value('param_endereco_bairro', $imovel['nome_bairro']); ?>" 
     id="text-bairro" name="param_endereco_bairro" 
     placeholder="Bairro do Endereço">
  </div>
</div>

<div class="form-group row bottom-formulario-20">
  <!-- ********** VALOR ********** -->
  <label for="text-valor" class="col-2 col-form-label">
    Valor (R$) <span class="texto-campo-obrigatorio">*</span>
  </label>
  <div class="col-4">
    <input id="text-valor" class="form-control" type="text" 
     value="<?php echo set_value('param_valor_negociacao', $valor_negociacao); ?>"
     name="param_valor_negociacao" placeholder="Valor da Negociação"
     mascara="moeda">
  </div>
  <!-- ********** COMISSAO ********** -->
  <label for="text-comissao" class="col-2 col-form-label">
    Comissão
  </label>
  <div class="col-4">
    <?php echo form_dropdown('param_id_comissao', 
        $comissoes, 
        set_value('param_id_comissao', $imovel['id_comissao']), 
        'class="form-control"'); 
    ?>
  </div>
</div>

<div class="row">
  <div class="offset-2 col-10">
    <hr class="separador padrao cinza"/>
  </div>
</div>
