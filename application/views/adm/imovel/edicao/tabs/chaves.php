<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $param_descricao_chave = $this->input->post("param_descricao_chave"); ?>

<div class="form-group row bottom-formulario-20">
    <div class="col">
        <div class="row bottom-formulario-20">
            <?php if (isset($param_descricao_chave)) { ?>
            <?php   $incluiu_botao = FALSE; ?>
            <?php   foreach ($param_descricao_chave as $param_chave) { ?>
                        <div id="elemento-campo-chave" class="col-10 bottom-formulario-16">
                            <input class="form-control" type="text" name="param_descricao_chave[]" 
                             placeholder="Ex: Chave principal; Chave reserva; Chave do portão;"
                             id="text-descricao-chave" value="<?php echo $param_chave; ?>"/>
                        </div>
            <?php       if (!$incluiu_botao) { ?>
            <?php           $incluiu_botao = TRUE; ?>
                        <div class="col-2">
                            <button type="button" class="btn btn-primary" href="#"
                             title="Adicionar nova chave" clonar-elemento="elemento-campo-chave">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                            </button>
                        </div>
            <?php       } ?>
            <?php   } ?>
            <?php } elseif (!empty($imovel['chaves'])) { ?>
            <?php   $incluiu_botao = FALSE; ?>
            <?php   foreach ($imovel['chaves'] as $chave) { ?>
                        <div id="elemento-campo-chave" class="col-10 bottom-formulario-16">
                            <input class="form-control" type="text" name="param_descricao_chave[]" 
                             placeholder="Ex: Chave principal; Chave reserva; Chave do portão;"
                             id="text-descricao-chave" value="<?php echo $chave['descricao_chave']; ?>"/>
                        </div>
            <?php       if (!$incluiu_botao) { ?>
            <?php           $incluiu_botao = TRUE; ?>
                        <div class="col-2">
                            <button type="button" class="btn btn-primary" href="#"
                             title="Adicionar nova chave" clonar-elemento="elemento-campo-chave">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                            </button>
                        </div>
            <?php       } ?>
            <?php   } ?>
            <?php } else { ?>
                <div id="elemento-campo-chave" class="col-10 bottom-formulario-16">
                    <input class="form-control" type="text" name="param_descricao_chave[]" 
                     placeholder="Ex: Chave principal; Chave reserva; Chave do portão;"
                     id="text-descricao-chave"/>
                </div>
                <div class="col-2">
                    <button type="button" class="btn btn-primary" href="#"
                     title="Adicionar nova chave" clonar-elemento="elemento-campo-chave">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                    </button>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
