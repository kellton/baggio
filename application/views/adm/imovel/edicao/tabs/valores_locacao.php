<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $valor_bruto = number_format(floatval($imovel['valor_bruto']), 2, ',', '.'); ?>
<?php $bonificacao = number_format(floatval($imovel['bonificacao']), 2, ',', '.'); ?>
<?php $valor_iptu = number_format(floatval($imovel['valor_iptu']), 2, ',', '.'); ?>
<?php $outros_valores = number_format(floatval($imovel['outros_valores']), 2, ',', '.'); ?>
<?php $valor_seguro_incendio = number_format(floatval($imovel['valor_seguro_incendio']), 2, ',', '.'); ?>
<?php $valor_seguro_fianca = number_format(floatval($imovel['valor_seguro_fianca']), 2, ',', '.'); ?>

<div class="form-group row bottom-formulario-20">
    <!-- ********** VALOR BRUTO ********** -->
    <label for="number-valor-bruto" class="col-2 col-form-label">
        Valor Bruto
    </label>
    <div class="col-4">
        <input class="form-control" type="text" 
         value="<?php echo set_value('param_valor_bruto', $valor_bruto); ?>" 
         name="param_valor_bruto" placeholder="00.00"
         id="number-valor-bruto" mascara="moeda">
    </div>
    <!-- ********** BONIFICACAO ********** -->
    <label for="number-bonificacao" class="col-2 col-form-label">
        Bonificação
    </label>
    <div class="col-4">
        <input class="form-control" type="text" 
         value="<?php echo set_value('param_bonificacao', $bonificacao); ?>" 
         name="param_bonificacao" placeholder="00.00"
         id="number-bonificacao" mascara="moeda">
    </div>
</div>

<div class="form-group row bottom-formulario-20">
    <!-- ********** VALOR IPTU ********** -->
    <label for="number-valor-iptu" class="col-2 col-form-label">
        Valor IPTU
    </label>
    <div class="col-4">
        <input class="form-control" type="text" 
         value="<?php echo set_value('param_valor_iptu', $valor_iptu); ?>" 
         name="param_valor_iptu" placeholder="00.00"
         id="number-valor-iptu" mascara="moeda">
    </div>
    <!-- ********** OUTROS VALORES ********** -->
    <label for="number-outros-valores" class="col-2 col-form-label">
        Outros Valores
    </label>
    <div class="col-4">
        <input class="form-control" type="text" 
         value="<?php echo set_value('param_outros_valores', $outros_valores); ?>" 
         name="param_outros_valores" placeholder="00.00"
         id="number-outros-valores" mascara="moeda">
    </div>
</div>

<div class="form-group row bottom-formulario-20">
    <!-- ********** PRAZO CONTRATO ********** -->
    <label for="text-prazo-contrato" class="col-2 col-form-label">
        Prazo de Contrato
    </label>
    <div class="col-4">
        <input class="form-control" type="text" name="param_prazo_contrato" 
         value="<?php echo set_value('param_prazo_contrato', $imovel['prazo_contrato']); ?>" 
         id="text-prazo-contrato">
    </div>
    <!-- ********** REAJUSTE ********** -->
    <label for="text-reajuste" class="col-2 col-form-label">
        Reajuste
    </label>
    <div class="col-4">
        <input class="form-control" type="text" name="param_reajuste" 
         value="<?php echo set_value('param_reajuste', $imovel['reajuste']); ?>" 
         id="text-reajuste">
    </div>
</div>

<div class="form-group row bottom-formulario-20">
    <!-- ********** INDICE REAJUSTE ********** -->
    <label for="text-indice-reajuste" class="col-2 col-form-label">
        Índice Reajuste
    </label>
    <div class="col-4">
        <input class="form-control" type="text" name="param_indice_reajuste" 
         value="<?php echo set_value('param_indice_reajuste', $imovel['indice_reajuste']); ?>" 
         id="text-indice-reajuste">
    </div>
    <!-- ********** SEGURO INCENDIO ********** -->
    <label for="number-seguro-incendio" class="col-2 col-form-label">
        Seguro Incêndio
    </label>
    <div class="col-4">
        <input class="form-control" type="text" 
         value="<?php echo set_value('param_seguro_incendio', $valor_seguro_incendio); ?>" 
         name="param_seguro_incendio" placeholder="00.00"
         id="number-seguro-incendio" mascara="moeda">
    </div>
</div>

<div class="form-group row bottom-formulario-20">
    <!-- ********** SEGURO FIANCA ********** -->
    <label for="text-seguro-fianca" class="col-2 col-form-label">
        Seguro Fiança
    </label>
    <div class="col-4">
        <input class="form-control" type="text" name="param_seguro_fianca" 
         value="<?php echo set_value('param_seguro_fianca', $valor_seguro_fianca); ?>" 
         id="text-seguro-fianca">
    </div>
    <!-- ********** FCI ********** -->
    <label for="text-fci" class="col-2 col-form-label">
        FCI
    </label>
    <div class="col-4">
        <input class="form-control" type="text" name="param_fci" 
         value="<?php echo set_value('param_fci', $imovel['fci']); ?>" 
         id="text-fci">
    </div>
</div>
