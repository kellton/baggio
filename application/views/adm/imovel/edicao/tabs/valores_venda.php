<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $valor_financiamento = number_format(floatval($imovel['valor_financiamento']), 2, ',', '.'); ?>
<?php $valor_poupanca = number_format(floatval($imovel['valor_poupanca']), 2, ',', '.'); ?>
<?php $valor_prestacao = number_format(floatval($imovel['valor_prestacao']), 2, ',', '.'); ?>
<?php $saldo_devedor = number_format(floatval($imovel['saldo_devedor']), 2, ',', '.'); ?>

<div class="form-group row bottom-formulario-20">
    <!-- ********** VALOR FINANCIAMENTO ********** -->
    <label for="number-valor-financiamento" class="col-2 col-form-label">
        Valor do Financiamento
    </label>
    <div class="col-4">
        <input class="form-control" type="text"
         value="<?php echo set_value('param_valor_financiamento', $valor_financiamento); ?>" 
         name="param_valor_financiamento" placeholder="00.00"
         id="number-valor-financiamento" mascara="moeda">
    </div>
    <!-- ********** VALOR POUPANCA ********** -->
    <label for="number-valor-poupanca" class="col-2 col-form-label">
        Valor do Poupança
    </label>
    <div class="col-4">
        <input class="form-control" type="text"
         value="<?php echo set_value('param_valor_poupanca', $valor_poupanca); ?>" 
         name="param_valor_poupanca" placeholder="00.00"
         id="number-valor-poupanca" mascara="moeda">
    </div>
</div>

<div class="form-group row bottom-formulario-20">
    <!-- ********** AGENTE FINANCEIRO ********** -->
    <label for="text-agente-financeiro" class="col-2 col-form-label">
        Agente Financeiro
    </label>
    <div class="col-4">
        <input class="form-control" type="text" name="param_agente_financeiro" 
         value="<?php echo set_value('param_agente_financeiro', $imovel['agente_financeiro']); ?>" 
         id="text-agente-financeiro">
    </div>
    <!-- ********** VALOR PRESTACAO ********** -->
    <label for="number-valor-prestacao" class="col-2 col-form-label">
        Valor da Prestação
    </label>
    <div class="col-4">
        <input class="form-control" type="text" 
         value="<?php echo set_value('param_valor_prestacao', $valor_prestacao); ?>" 
         name="param_valor_prestacao" placeholder="00.00"
         id="number-valor-prestacao" mascara="moeda">
    </div>
</div>

<div class="form-group row bottom-formulario-20">
    <!-- ********** SALDO DEVEDOR ********** -->
    <label for="number-saldo-devedor" class="col-2 col-form-label">
        Saldo Devedor
    </label>
    <div class="col-4">
        <input class="form-control" type="text" 
         value="<?php echo set_value('param_saldo_devedor', $saldo_devedor); ?>" 
         name="param_saldo_devedor" placeholder="00.00"
         id="number-saldo-devedor" mascara="moeda">
    </div>
    <!-- ********** PRAZO ********** -->
    <label for="text-prazo" class="col-2 col-form-label">
        Prazo (Meses)
    </label>
    <div class="col-4">
        <input class="form-control" type="text" name="param_prazo" 
         value="<?php echo set_value('param_prazo', $imovel['prazo']); ?>" 
         id="text-prazo">
    </div>
</div>

<div class="form-group row bottom-formulario-20">
    <!-- ********** NUMERO PRESTACOES ********** -->
    <label for="number-numero-prestacoes" class="col-2 col-form-label">
        Número de Prestações
    </label>
    <div class="col-4">
        <input class="form-control" type="number" min="0" step="1" 
         value="<?php echo set_value('param_numero_prestacoes', $imovel['numero_prestacoes']); ?>" 
         name="param_numero_prestacoes" placeholder="0"
         id="number-numero-prestacoes">
    </div>
    <!-- ********** NUMERO PRESTACOES PAGAS ********** -->
    <label for="number-numero-prestacoes-pagas" class="col-2 col-form-label">
        Número de Prestações Pagas
    </label>
    <div class="col-4">
        <input class="form-control" type="number" min="0" step="1" 
         value="<?php echo set_value('param_numero_prestacoes_pagas', $imovel['numero_prestacoes_pagas']); ?>" 
         name="param_numero_prestacoes_pagas" placeholder="0"
         id="number-numero-prestacoes-pagas">
    </div>
</div>
