<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="form-group row bottom-formulario-20">
	<a name="cadastro-imovel-material"></a>
	<!-- ********** MATERIAIS DE APOIO ********** -->
	<div class="col">
		<legend>Materiais de Apoio</legend>
	</div>
</div>

<div class="form-group row bottom-formulario-20">
	<!-- ********** MATERIAIS ********** -->
	<?php if (!isset($param_materiais)) { ?>
    	<div id="elemento-campo-material" class="offset-2 col-8 bottom-formulario-16">
    		<input type="file" name="param_materiais[]">
		</div>
	<?php } else if (count($param_materiais) > 0) { ?>
    <?php   foreach ($param_materiais as $param_material) { ?>
    			<div id="elemento-campo-material" class="offset-2 col-8 bottom-formulario-16">
					<input type="file" name="param_materiais[]" 
					 value="<?php echo $param_material; ?>">
				</div>
	<?php   } ?>
	<?php } ?>
	<div class="col-2">
        <button type="button" class="btn btn-primary" href="#"
         title="Adicionar nova foto" clonar-elemento="elemento-campo-material">
            <i class="fa fa-plus" aria-hidden="true"></i>
        </button>
    </div>
</div>

<div class="row">
  <div class="offset-2 col-10">
    <hr class="separador padrao cinza"/>
  </div>
</div>
