<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="form-group row bottom-formulario-20">
	<a name="cadastro-imovel-veiculacao"></a>
	<!-- ********** VEICULAÇÃO ********** -->
	<div class="col">
		<legend>Veiculação</legend>
	</div>
</div>

<div class="form-group row bottom-formulario-20">
	<!-- ********** PRIVACIDADE ********** -->
	<label for="drop-privacidade" class="col-2 col-form-label">
		Privacidade
	</label>
	<div class="col-4">
		<?php echo form_dropdown('param_id_privacidade', 
			$privacidades, 
			set_value('param_id_privacidade', $imovel['id_privacidade']), 
			'class="form-control"'); 
		?>
	</div>
</div>

<div class="form-group row bottom-formulario-20">
	<!-- ********** DATA DE ANGARIACAO ********** -->
	<label for="date-angariacao" class="col-2 col-form-label">
		Angariação até <span class="texto-campo-obrigatorio">*</span>
	</label>
	<div class="col-4">
		<div class="input-group">
			<input type="text" class="form-control campo-formulario-branco" 
			 id="date-angariacao" readonly datepicker="input"
			 placeholder="<?php echo $hoje; ?>" name="param_data_angariacao" 
			 value="<?php echo set_value('param_data_angariacao', formatar_sql_to_angariacao($imovel['data_angariacao']) ); ?>">
			<div class="input-group-addon comportamento-botao">
				<i class="fa fa-calendar" aria-hidden="true"></i>
			</div>
		</div>
	</div>	
</div>

<div class="form-group row bottom-formulario-20">
	<!-- ********** DESTAQUES ********** -->
	<label for="date-angariacao" class="col-2 col-form-label">
		Destaques
	</label>
	<div class="col-10">
		<div class="form-check form-check-inline">
			<label class="form-check-label">
				<input class="form-check-input" type="checkbox" 
				 name="param_destaque_viva" 
				 <?php echo post_checkbox('param_destaque_viva', $imovel['viva_real']); ?>>Viva Real
			</label>
		</div>
		<div class="form-check form-check-inline">
			<label class="form-check-label">
				<input class="form-check-input" type="checkbox" 
				 name="param_destaque_imovelweb"
				 <?php echo post_checkbox('param_destaque_imovelweb', $imovel['imovel_web']); ?>>ImovelWeb
			</label>
		</div>
		<div class="form-check form-check-inline">
			<label class="form-check-label">
				<input class="form-check-input" type="checkbox" 
				 name="param_destaque_zap"
				 <?php echo post_checkbox('param_destaque_zap', $imovel['zap']); ?>>ZAP
			</label>
		</div>
		<div class="form-check form-check-inline">
			<label class="form-check-label">
				<input class="form-check-input" type="checkbox" 
				 name="param_destaque_zap_super"
				 <?php echo post_checkbox('param_destaque_zap_super', $imovel['zap_super']); ?>>ZAP (SUPER)
			</label>
		</div>
		<div class="form-check form-check-inline">
			<label class="form-check-label">
				<input class="form-check-input" type="checkbox" 
				 name="param_destaque_enkontra"
				 <?php echo post_checkbox('param_destaque_enkontra', $imovel['enkontra']); ?>>Enkontra
			</label>
		</div>
	</div>
</div>

<div class="form-group row bottom-formulario-20">
	<!-- ********** NAO INCLUIR NA INTEGRACAO ********** -->
	<label for="date-angariacao" class="col-2 col-form-label">
		Não Incluir na Integração
	</label>
	<div class="col-6">
		<div class="form-check form-check-inline">
			<label class="form-check-label">
				<input class="form-check-input" type="checkbox" 
				 name="param_nao_imovelweb" 
				 <?php echo post_checkbox('param_nao_imovelweb', $imovel['nao_imovel_web']); ?>>ImovelWeb
			</label>
		</div>
		<div class="form-check form-check-inline">
			<label class="form-check-label">
				<input class="form-check-input" type="checkbox" 
				 name="param_nao_zap" 
				 <?php echo post_checkbox('param_nao_zap', $imovel['nao_zap']); ?>>ZAP
			</label>
		</div>
	</div>
</div>

<div class="form-group row bottom-formulario-20">
  <!-- ********** VIDEO DO YOUTUBE ********** -->
  <label for="text-video-youtube" class="col-2 col-form-label">
    Vídeo do Youtube
    <span class="texto-campo-publico fonte-padrao" 
	 title="Este campo poderá ser exibidos publicamente">
		<i class="fa fa-users" aria-hidden="true"></i>
	</span>
  </label>
  <div class="col-6">
    <input class="form-control" type="text" name="param_url_youtube" 
     value="<?php echo set_value('param_url_youtube', $imovel['video_youtube']); ?>" 
     id="text-video-youtube" placeholder="Link de vídeo do Youtube">
  </div>
</div>

<div class="row">
  <div class="offset-2 col-10">
    <hr class="separador padrao cinza"/>
  </div>
</div>
