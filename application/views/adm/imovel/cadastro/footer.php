<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

			<div class="form-group row text-center">
				<div class="col">
					<button type="submit" href="#" 
					 title="Finalizar cadatro de imóvel"
					 class="btn btn-primary btn-lg comportamento-botao">
	                    Finalizar Cadastro
	                </button>
				</div>
			</div>
		<!-- FIM DO FORMULARIO DE CADASTRO -->
		</form>
	<!-- FIM DA COL -->
    </div>
  <!-- FIM DA ROW -->
  </div>
<!-- FIM DO CONTAINER -->
</div>