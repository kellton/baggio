<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $param_fotos = $this->input->post("param_fotos"); ?>

<div class="form-group row bottom-formulario-20">
	<a name="cadastro-imovel-fotos"></a>
	<!-- ********** FOTOS DO IMOVEL ********** -->
	<div class="col">
		<legend>
			Fotos do Imóvel
			<span class="texto-campo-publico fonte-padrao" 
			 title="Todos os campos dessa seção poderão ser exibidos publicamente">
				<i class="fa fa-users" aria-hidden="true"></i>
			</span>
		</legend>
	</div>
</div>

<div class="form-group row bottom-formulario-20">
	<!-- ********** FOTOS ********** -->
	<?php if (!isset($param_fotos)) { ?>
    	<div id="elemento-campo-foto" class="offset-2 col-8 bottom-formulario-16">
			<input type="file" class="form-control-file" 
			 name="param_fotos[]">
		</div>
		<div class="col-2">
	        <button type="button" class="btn btn-primary" href="#"
	         title="Adicionar nova foto" clonar-elemento="elemento-campo-foto">
	            <i class="fa fa-plus" aria-hidden="true"></i>
	        </button>
	    </div>
	<?php } else { ?>
	<?php  	$incluiu_botao = FALSE;	?>
    <?php   foreach ($param_fotos as $param_foto) { ?>
    			<div id="elemento-campo-foto" class="offset-2 col-8 bottom-formulario-16">
					<input type="file" class="form-control-file" 
					 name="param_fotos[]">
				</div>
	<?php 		if (!$incluiu_botao) { ?>
	<?php 			$incluiu_botao = TRUE; ?>
				<div class="col-2">
			        <button type="button" class="btn btn-primary" href="#"
			         title="Adicionar nova foto" clonar-elemento="elemento-campo-foto">
			            <i class="fa fa-plus" aria-hidden="true"></i>
			        </button>
			    </div>
	<?php 		} ?>
	<?php   } ?>
	<?php } ?>
</div>

<div class="row">
  <div class="offset-2 col-10">
    <hr class="separador padrao cinza"/>
  </div>
</div>
