<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $valor_condominio = formatar_moeda( set_value('param_valor_condominio') ); ?>

<div class="form-group row bottom-formulario-20">
    <!-- ********** PUBLICO ********** -->
    <div class="col-12 text-center bottom-formulario-20">
        <legend>
            <span class="texto-campo-publico fonte-padrao" 
             title="Todos os campos dessa seção poderão ser exibidos publicamente">
                <i class="fa fa-users" aria-hidden="true"></i>
                - Exceto "Ocupação", "Informações Adicionais" e "Detalhes da Negociação"
            </span>
        </legend>
    </div>
    <!-- ********** OCUPACAO ********** -->
    <label for="drop-ocupacao" class="col-2 col-form-label">
        Ocupação
    </label>
    <div class="col-4">
        <?php echo form_dropdown('param_id_tipo_ocupacao', 
            $tipo_ocupacoes, 
            set_value('param_id_tipo_ocupacao'), 
            'class="form-control"'); 
        ?>
    </div>
    <!-- ********** VAGO EM ********** -->
    <label for="text-vago-em" class="col-2 col-form-label">
        Vago em
    </label>
    <div class="col-4">
        <input class="form-control" type="text" mascara="data" 
         value="<?php echo set_value('param_vago_em'); ?>" id="text-vago-em"
         name="param_vago_em" placeholder="Ex: 01/01/2020">
    </div>
</div>

<div class="form-group row bottom-formulario-20">
    <!-- ********** EDIFICIO ********** -->
    <label for="text-edificio" class="col-2 col-form-label">
        Edifício
    </label>
    <div class="col-4">
        <input class="form-control" type="text" name="param_edificio" 
         value="<?php echo set_value('param_edificio'); ?>" id="text-edificio"
         placeholder="Nome do edifício">
    </div>
    <!-- ********** ANDAR ********** -->
    <label for="drop-andar" class="col-2 col-form-label">
        Andar
    </label>
    <div class="col-4">
        <?php echo form_dropdown('param_id_tipo_andar', 
            $tipo_andares, 
            set_value('param_id_tipo_andar'), 
            'class="form-control"'); 
        ?>
    </div>
</div>
<div class="form-group row bottom-formulario-20">
    <!-- ********** IDADE DO IMOVEL ********** -->
    <label for="text-idade" class="col-2 col-form-label">
        Idade do Imóvel
    </label>
    <div class="col-4">
        <input class="form-control" type="text" name="param_idade_imovel" 
         value="<?php echo set_value('param_idade_imovel'); ?>" id="text-idade"
         placeholder="Ex: 10 anos">
    </div>
    <!-- ********** FACE APARTAMENTO ********** -->
    <!-- <label for="drop-face-apartamento" class="col-2 col-form-label">
        Posição do Apartamento
    </label>
    <div class="col-4">
        <?php echo form_dropdown('param_id_tipo_face_apartamento', 
            $tipo_faces, 
            set_value('param_id_tipo_face_apartamento'), 
            'class="form-control"'); 
        ?>
    </div> -->
</div>
<div class="form-group row bottom-formulario-20">
    <!-- ********** CONDOMINIO ********** -->
    <label for="text-condominio" class="col-2 col-form-label">
        Condomínio
    </label>
    <div class="col-4">
        <input class="form-control" type="text" name="param_condominio" 
         value="<?php echo set_value('param_condominio'); ?>" id="text-condominio"
         placeholder="Nome do condomínio">
    </div>
    <!-- ********** CONDOMINIO FECHADO ********** -->
    <div class="offset-2 col-4">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_condominio_fechado"
                 <?php echo post_checkbox('param_condominio_fechado'); ?>>Condomínio Fechado
            </label>
        </div>
    </div>
</div>
<div class="form-group row bottom-formulario-20">
    <!-- ********** VALOR CONDOMINIO ********** -->
    <label for="text-valor-condominio" class="col-2 col-form-label">
        Valor do Condomínio
    </label>
    <div class="col-4">
        <input id="text-valor-condominio" class="form-control" 
         type="text" name="param_valor_condominio" 
         value="<?php echo set_value('param_valor_condominio', $valor_condominio); ?>" 
         placeholder="Ex: 584.00" mascara="moeda">
    </div>
    <!-- ********** COBERTURA ********** -->
    <div class="offset-2 col-4">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_cobertura"
                 <?php echo post_checkbox('param_cobertura'); ?>>Cobertura
            </label>
        </div>
    </div>
</div>
<div class="form-group row bottom-formulario-20">
    <!-- ********** ENTRE RUAS ********** -->
    <label for="text-entre-ruas" class="col-2 col-form-label">
        Entre ruas
    </label>
    <div class="col-6">
        <input class="form-control" type="text" name="param_entre_ruas" 
         value="<?php echo set_value('param_entre_ruas'); ?>" id="text-entre-ruas" 
         placeholder="Ex: Entre as ruas A e B">
    </div>
</div>

<div class="row">
    <div class="col-12">
        <hr class="separador padrao cinza"/>
    </div>
</div>

<div class="form-group row bottom-formulario-20">
    <!-- ********** INFORMACOES ADICIONAIS ********** -->
    <label for="text-informacoes-adicionais" class="col-2 col-form-label">
        Informações Adicionais
    </label>
    <div class="col-10">
        <textarea class="form-control" id="text-informacoes-adicionais" 
         rows="5" name="param_informacoes_adicionais" 
         value="<?php echo set_value('param_informacoes_adicionais'); ?>"></textarea>
    </div>
</div>

<div class="form-group row">
    <!-- ********** DETALHES DA NEGOCIACAO ********** -->
    <label for="text-detalhes-negociacao" class="col-2 col-form-label">
        Detalhes da Negociação
    </label>
    <div class="col-10">
        <textarea class="form-control" id="text-detalhes-negociacao" 
         rows="5" name="param_detalhes_negociacao" 
         value="<?php echo set_value('param_detalhes_negociacao'); ?>"></textarea>
    </div>
</div>
