<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $area_terreno = formatar_moeda( set_value('param_area_terreno') ); ?>

<div class="form-group row bottom-formulario-20">
    <!-- ********** PUBLICO ********** -->
    <div class="col-12 text-center bottom-formulario-20">
        <legend>
            <span class="texto-campo-publico fonte-padrao" 
             title="Todos os campos dessa seção poderão ser exibidos publicamente">
                <i class="fa fa-users" aria-hidden="true"></i>
                - Exceto "Zoneamento"
            </span>
        </legend>
    </div>
    <!-- **********ZONEAMENTO ********** -->
    <label for="drop-zoneamento" class="col-2 col-form-label">
        Zoneamento
    </label>
    <div class="col-4">
        <?php echo form_dropdown('param_id_tipo_zoneamento', 
            $tipo_zoneamentos, 
            set_value('param_id_tipo_zoneamento'), 
            'class="form-control"'); 
        ?>
    </div>
    <!-- ********** FACE DO TERRENO ********** -->
    <label for="drop-face-terreno" class="col-2 col-form-label">
        Face do Terreno
    </label>
    <div class="col-4">
        <?php echo form_dropdown('param_id_tipo_face_terreno', 
            $tipo_faces, 
            set_value('param_id_tipo_face_terreno'), 
            'class="form-control"'); 
        ?>
    </div>
</div>

<div class="form-group row bottom-formulario-20">
    <!-- ********** AREA DO TERRENO ********** -->
    <label for="number-area-terreno" class="col-2 col-form-label">
        Área do Terreno
    </label>
    <div class="col-4">
        <input class="form-control" type="text"  
         value="<?php echo set_value('area_terreno'); ?>" id="number-area-terreno"
         name="param_area_terreno" placeholder="252.04"
         mascara="area">
    </div>
    <!-- ********** PAVIMENTACAO ********** -->
    <label for="text-pavimentacao" class="col-2 col-form-label">
        Pavimentação
    </label>
    <div class="col-4">
        <input class="form-control" type="text" name="param_pavimentacao" 
         value="<?php echo set_value('param_pavimentacao'); ?>" 
         id="text-pavimentacao">
    </div>
</div>

<div class="form-group row bottom-formulario-20">
    <!-- ********** PE DIREITO ********** -->
    <label for="text-pe-direito" class="col-2 col-form-label">
        Pé Direito
    </label>
    <div class="col-4">
        <input class="form-control" type="text" name="param_pe_direito" 
         value="<?php echo set_value('param_pe_direito'); ?>" 
         id="text-pe-direito">
    </div>
    <!-- ********** MEDIDAS TERRENO ********** -->
    <label for="text-medidas-terreno" class="col-2 col-form-label">
        Medidas do Terreno
    </label>
    <div class="col-4">
        <input class="form-control" type="text" name="param_medidas_terreno" 
         value="<?php echo set_value('param_medidas_terreno'); ?>" 
         id="text-medidas-terreno">
    </div>
</div>

<div class="form-group row bottom-formulario-20">
    <!-- ********** LOTE ********** -->
    <label for="text-lote" class="col-2 col-form-label">
        Lote
    </label>
    <div class="col-4">
        <input class="form-control" type="text" name="param_lote" 
         value="<?php echo set_value('param_lote'); ?>" 
         id="text-lote">
    </div>
    <!-- ********** QUADRA ********** -->
    <label for="text-quadra" class="col-2 col-form-label">
        Quadra
    </label>
    <div class="col-4">
        <input class="form-control" type="text" name="param_quadra" 
         value="<?php echo set_value('param_quadra'); ?>" 
         id="text-quadra">
    </div>
</div>

<div class="form-group row bottom-formulario-20">
    <!-- ********** PLANTA ********** -->
    <label for="text-planta" class="col-2 col-form-label">
        Planta
    </label>
    <div class="col-4">
        <input class="form-control" type="text" name="param_planta" 
         value="<?php echo set_value('param_planta'); ?>" 
         id="text-planta">
    </div>
    <!-- ********** INDICACAO FISCAL ********** -->
    <label for="text-indicacao-fiscal" class="col-2 col-form-label">
        Indicação Fiscal
    </label>
    <div class="col-4">
        <input class="form-control" type="text" name="param_indicacao_fiscal" 
         value="<?php echo set_value('param_indicacao_fiscal'); ?>" 
         id="text-indicacao-fiscal">
    </div>
</div>

<div class="form-group row bottom-formulario-20">
    <!-- ********** TOPOGRAFIA ********** -->
    <label for="text-topografia" class="col-2 col-form-label">
        Topografia
    </label>
    <div class="col-4">
        <input class="form-control" type="text" name="param_topografia" 
         value="<?php echo set_value('param_topografia'); ?>" 
         id="text-topografia">
    </div>
    <!-- ********** REGISTRO IMOVEL ********** -->
    <label for="text-registro-imovel" class="col-2 col-form-label">
        Registro de Imóvel
    </label>
    <div class="col-4">
        <input class="form-control" type="text" name="param_registro_imovel" 
         value="<?php echo set_value('param_registro_imovel'); ?>" 
         id="text-registro-imovel">
    </div>
</div>

<div class="form-group row bottom-formulario-20">
    <!-- ********** CIRCUNSCRICAO ********** -->
    <label for="text-circunscricao" class="col-2 col-form-label">
        Circunscrição
    </label>
    <div class="col-4">
        <input class="form-control" type="text" name="param_circunscricao" 
         value="<?php echo set_value('param_circunscricao'); ?>" 
         id="text-circunscricao">
    </div>
    <!-- ********** MATERIAL ********** -->
    <label for="text-material" class="col-2 col-form-label">
        Material
    </label>
    <div class="col-4">
        <?php echo form_dropdown('param_id_tipo_material', 
            $tipo_materiais, 
            set_value('param_id_tipo_material'), 
            'class="form-control"'); 
        ?>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <hr class="separador padrao cinza"/>
    </div>
</div>

<!-- ****************************************************** -->
<!-- ******************** CHECK BOXIES ******************** -->
<!-- ****************************************************** -->
<div class="form-group row bottom-formulario-10">
    <!-- ********** AGUA ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_agua"
                 <?php echo post_checkbox('param_tem_agua'); ?>>Água
            </label>
        </div>
    </div>
    <!-- ********** ESGOTO ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_esgoto"
                 <?php echo post_checkbox('param_tem_esgoto'); ?>>Esgoto
            </label>
        </div>
    </div>
    <!-- ********** LUZ ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_luz"
                 <?php echo post_checkbox('param_tem_luz'); ?>>Luz
            </label>
        </div>
    </div>
    <!-- ********** TELEFONE ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_telefone"
                 <?php echo post_checkbox('param_tem_telefone'); ?>>Telefone
            </label>
        </div>
    </div>
</div>

<div class="form-group row bottom-formulario-10">
    <!-- ********** PLACA LOCAL ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_placa_local"
                 <?php echo post_checkbox('param_tem_placa_local'); ?>>Placa no Local
            </label>
        </div>
    </div>
    <!-- ********** FORRO ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_forro"
                 <?php echo post_checkbox('param_tem_forro'); ?>>Forro
            </label>
        </div>
    </div>
    <!-- ********** TELHADO ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_telhado"
                 <?php echo post_checkbox('param_tem_telhado'); ?>>Telhado
            </label>
        </div>
    </div>
</div>
