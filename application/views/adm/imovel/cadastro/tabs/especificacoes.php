<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="form-group row bottom-formulario-10">
    <!-- ********** PUBLICO ********** -->
    <div class="col-12 text-center bottom-formulario-20">
        <legend>
            <span class="texto-campo-publico fonte-padrao" 
             title="Todos os campos dessa seção poderão ser exibidos publicamente">
                <i class="fa fa-users" aria-hidden="true"></i>
                - Exceto "Situação Documental" e "Financiamento"
            </span>
        </legend>
    </div>
    <!-- ********** AREA DE SERVICO ********** -->
    <label for="number-area-servico" class="col-2 col-form-label">
        Área de Serviço
    </label>
    <div class="col-2">
        <input class="form-control" type="number" min="0" step="1" 
         name="param_qtd_area_servico" 
         value="<?php echo set_value('param_qtd_area_servico'); ?>" 
         id="number-area-servico" placeholder="0">
    </div>
    <!-- ********** BANHEIRO ********** -->
    <label for="number-banheiros" class="col-2 col-form-label">
        Banheiros
    </label>
    <div class="col-2">
        <input class="form-control" type="number" min="0" step="1" 
         name="param_qtd_banheiros" 
         value="<?php echo set_value('param_qtd_banheiros'); ?>" 
         id="number-banheiros" placeholder="0">
    </div>
    <!-- ********** BWC EMPREGADA ********** -->
    <label for="number-bwc-empregada" class="col-2 col-form-label">
        BWC Empregada
    </label>
    <div class="col-2">
        <input class="form-control" type="number" min="0" step="1" 
         name="param_qtd_bwc_empregada" 
         value="<?php echo set_value('param_qtd_bwc_empregada'); ?>" 
         id="number-bwc-empregada" placeholder="0">
    </div>
</div>

<div class="form-group row bottom-formulario-10">
    <!-- ********** CHURRASQUEIRAS ********** -->
    <label for="number-churrasqueiras" class="col-2 col-form-label">
        Churrasqueiras
    </label>
    <div class="col-2">
        <input class="form-control" type="number" min="0" step="1" 
         name="param_qtd_churrasqueiras" 
         value="<?php echo set_value('param_qtd_churrasqueiras'); ?>" 
         id="number-churrasqueiras" placeholder="0">
    </div>
    <!-- ********** CLOSET ********** -->
    <label for="number-closet" class="col-2 col-form-label">
        Closet
    </label>
    <div class="col-2">
        <input class="form-control" type="number" min="0" step="1" 
         name="param_qtd_closet" 
         value="<?php echo set_value('param_qtd_closet'); ?>" 
         id="number-closet" placeholder="0">
    </div>
    <!-- ********** COPA ********** -->
    <label for="number-copa" class="col-2 col-form-label">
        Copa
    </label>
    <div class="col-2">
        <input class="form-control" type="number" min="0" step="1" 
         name="param_qtd_copa" 
         value="<?php echo set_value('param_qtd_copa'); ?>" 
         id="number-copa" placeholder="0">
    </div>
</div>

<div class="form-group row bottom-formulario-10">
    <!-- ********** COZINHAS ********** -->
    <label for="number-cozinhas" class="col-2 col-form-label">
        Cozinhas
    </label>
    <div class="col-2">
        <input class="form-control" type="number" min="0" step="1" 
         name="param_qtd_cozinhas" 
         value="<?php echo set_value('param_qtd_cozinhas'); ?>" 
         id="number-cozinhas" placeholder="0">
    </div>
    <!-- ********** DEP EMPREGADA ********** -->
    <label for="number-dep-empregada" class="col-2 col-form-label">
        Dep. Empregada
    </label>
    <div class="col-2">
        <input class="form-control" type="number" min="0" step="1" 
         name="param_qtd_dep_empregada" 
         value="<?php echo set_value('param_qtd_dep_empregada'); ?>" 
         id="number-dep-empregada" placeholder="0">
    </div>
    <!-- ********** LAREIRA ********** -->
    <label for="number-lareira" class="col-2 col-form-label">
        Lareira
    </label>
    <div class="col-2">
        <input class="form-control" type="number" min="0" step="1" 
         name="param_qtd_lareira" 
         value="<?php echo set_value('param_qtd_lareira'); ?>" 
         id="number-lareira" placeholder="0">
    </div>
</div>

<div class="form-group row bottom-formulario-10">
    <!-- ********** LAVABOS ********** -->
    <label for="number-lavabos" class="col-2 col-form-label">
        Lavabos
    </label>
    <div class="col-2">
        <input class="form-control" type="number" min="0" step="1" 
         name="param_qtd_lavabos" 
         value="<?php echo set_value('param_qtd_lavabos'); ?>" 
         id="number-lavabos" placeholder="0">
    </div>
    <!-- ********** QUARTOS ********** -->
    <label for="number-quartos" class="col-2 col-form-label">
        Quartos
    </label>
    <div class="col-2">
        <input class="form-control" type="number" min="0" step="1" 
         name="param_qtd_quartos" 
         value="<?php echo set_value('param_qtd_quartos'); ?>" 
         id="number-quartos" placeholder="0">
    </div>
    <!-- ********** SACADAS ********** -->
    <label for="number-sacadas" class="col-2 col-form-label">
        Sacadas
    </label>
    <div class="col-2">
        <input class="form-control" type="number" min="0" step="1" 
         name="param_qtd_sacadas" 
         value="<?php echo set_value('param_qtd_sacadas'); ?>" 
         id="number-sacadas" placeholder="0">
    </div>
</div>

<div class="form-group row bottom-formulario-10">
    <!-- ********** SALA BIBLIOTECA ********** -->
    <label for="number-sala-biblioteca" class="col-2 col-form-label">
        Sala Biblioteca
    </label>
    <div class="col-2">
        <input class="form-control" type="number" min="0" step="1" 
         name="param_qtd_sala_biblioteca" 
         value="<?php echo set_value('param_qtd_sala_biblioteca'); ?>" 
         id="number-sala-biblioteca" placeholder="0">
    </div>
    <!-- ********** SALA ESCRITORIO ********** -->
    <label for="number-sala-escritorio" class="col-2 col-form-label">
        Sala de Escritório
    </label>
    <div class="col-2">
        <input class="form-control" type="number" min="0" step="1" 
         name="param_qtd_sala_escritorio" 
         value="<?php echo set_value('param_qtd_sala_escritorio'); ?>" 
         id="number-sala-escritorio" placeholder="0">
    </div>
    <!-- ********** SALA JANTAR ********** -->
    <label for="number-sala-jantar" class="col-2 col-form-label">
        Sala de Jantar
    </label>
    <div class="col-2">
        <input class="form-control" type="number" min="0" step="1" 
         name="param_qtd_sala_jantar" 
         value="<?php echo set_value('param_qtd_sala_jantar'); ?>" 
         id="number-sala-jantar" placeholder="0">
    </div>
</div>

<div class="form-group row bottom-formulario-10">
    <!-- ********** SALA ESTAR INTIMA ********** -->
    <label for="number-sala-estar-intima" class="col-2 col-form-label">
        Sala de Estar Íntima
    </label>
    <div class="col-2">
        <input class="form-control" type="number" min="0" step="1" 
         name="param_qtd_sala_estar_intima" 
         value="<?php echo set_value('param_qtd_sala_estar_intima'); ?>" 
         id="number-sala-estar-intima" placeholder="0">
    </div>
    <!-- ********** SALAS ********** -->
    <label for="number-salas" class="col-2 col-form-label">
        Salas
    </label>
    <div class="col-2">
        <input class="form-control" type="number" min="0" step="1" 
         name="param_qtd_salas" 
         value="<?php echo set_value('param_qtd_salas'); ?>" 
         id="number-salas" placeholder="0">
    </div>
    <!-- ********** SUITES ********** -->
    <label for="number-suites" class="col-2 col-form-label">
        Suítes
    </label>
    <div class="col-2">
        <input class="form-control" type="number" min="0" step="1" 
         name="param_qtd_suites" 
         value="<?php echo set_value('param_qtd_suites'); ?>" 
         id="number-suites" placeholder="0">
    </div>
</div>

<div class="form-group row bottom-formulario-10">
    <!-- ********** VAGAS ESTACIONAMENTO ********** -->
    <label for="number-vagas-estacionamento" class="col-2 col-form-label">
        Vagas de Estacionamento
    </label>
    <div class="col-2">
        <input class="form-control" type="number" min="0" step="1" 
         name="param_qtd_vagas_estacionamento" 
         value="<?php echo set_value('param_qtd_vagas_estacionamento'); ?>" 
         id="number-vagas-estacionamento" placeholder="0">
    </div>
    <!-- ********** DEMI SUITE ********** -->
    <label for="number-demi-suite" class="col-2 col-form-label">
        Demi Suíte
    </label>
    <div class="col-2">
        <input class="form-control" type="number" min="0" step="1" 
         name="param_qtd_demi_suite" 
         value="<?php echo set_value('param_qtd_demi_suite'); ?>" 
         id="number-demi-suite" placeholder="0">
    </div>
</div>

<div class="row">
    <div class="col-12">
        <hr class="separador padrao cinza"/>
    </div>
</div>

<!-- ****************************************************** -->
<!-- ******************** CHECK BOXIES ******************** -->
<!-- ****************************************************** -->
<div class="form-group row bottom-formulario-10">
    <!-- ********** ADEGA ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_adega"
                 <?php echo post_checkbox('param_tem_adega'); ?>>Adega
            </label>
        </div>
    </div>
    <!-- ********** ALARME ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_alarme"
                 <?php echo post_checkbox('param_tem_alarme'); ?>>Alarme
            </label>
        </div>
    </div>
    <!-- ********** AQUECIMENTO ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_aquecimento"
                 <?php echo post_checkbox('param_tem_aquecimento'); ?>>Aquecimento
            </label>
        </div>
    </div>
    <!-- ********** ATICO ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_atico"
                 <?php echo post_checkbox('param_tem_atico'); ?>>Ático
            </label>
        </div>
    </div>
</div>

<div class="form-group row bottom-formulario-10">
    <!-- ********** AQUECIMENTO SOLAR ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_aquecimento_solar"
                 <?php echo post_checkbox('param_tem_aquecimento_solar'); ?>>Aquecimento Solar
            </label>
        </div>
    </div>
    <!-- ********** AQUECIMENTO ELETRICO ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_aquecimento_eletrico"
                 <?php echo post_checkbox('param_tem_aquecimento_eletrico'); ?>>Aquecimento Elétrico
            </label>
        </div>
    </div>
    <!-- ********** BICICLETARIO ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_bicicletario"
                 <?php echo post_checkbox('param_tem_bicicletario'); ?>>Bicicletário
            </label>
        </div>
    </div>
    <!-- ********** BRINQUEDOTECA ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_brinquedoteca"
                 <?php echo post_checkbox('param_tem_brinquedoteca'); ?>>Brinquedoteca
            </label>
        </div>
    </div>
</div>

<div class="form-group row bottom-formulario-10">
    <!-- ********** CANCHA ESPORTES ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_cancha_esportes"
                 <?php echo post_checkbox('param_tem_cancha_esportes'); ?>>Cancha de Esportes
            </label>
        </div>
    </div>
    <!-- ********** CANIL ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_canil"
                 <?php echo post_checkbox('param_tem_canil'); ?>>Canil
            </label>
        </div>
    </div>
    <!-- ********** CENTRAL GAS ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_central_gas"
                 <?php echo post_checkbox('param_tem_central_gas'); ?>>Central de Gás
            </label>
        </div>
    </div>
    <!-- ********** CERCA ELETRICA ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_cerca_eletrica"
                 <?php echo post_checkbox('param_tem_cerca_eletrica'); ?>>Cerca Elétrica
            </label>
        </div>
    </div>
</div>

<div class="form-group row bottom-formulario-10">
    <!-- ********** CHURRASQUEIRA COLETIVA ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_churrasqueira_coletiva"
                 <?php echo post_checkbox('param_tem_churrasqueira_coletiva'); ?>>
                 Churrasqueira Coletiva
            </label>
        </div>
    </div>
    <!-- ********** CHURRASQUEIRA PRIVADA ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_churrasqueira_privada"
                 <?php echo post_checkbox('param_tem_churrasqueira_privada'); ?>>
                 Churrasqueira Privada
            </label>
        </div>
    </div>
    <!-- ********** CINEMA ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_cinema"
                 <?php echo post_checkbox('param_tem_cinema'); ?>>Cinema
            </label>
        </div>
    </div>
    <!-- ********** COZINHA ARMARIOS ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_cozinha_armarios"
                 <?php echo post_checkbox('param_tem_cozinha_armarios'); ?>>Cozinha com Armários
            </label>
        </div>
    </div>
</div>

<div class="form-group row bottom-formulario-10">
    <!-- ********** DEPOSITO ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_deposito"
                 <?php echo post_checkbox('param_tem_deposito'); ?>>Depósito
            </label>
        </div>
    </div>
    <!-- ********** DESPENSA ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_despensa"
                 <?php echo post_checkbox('param_tem_despensa'); ?>>Despensa
            </label>
        </div>
    </div>
    <!-- ********** DORMITORIOS ARMARIOS ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_dormitorios_armarios"
                 <?php echo post_checkbox('param_tem_dormitorios_armarios'); ?>>
                 Dormitórios com Armários
            </label>
        </div>
    </div>
    <!-- ********** FITNESS ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_fitness"
                 <?php echo post_checkbox('param_tem_fitness'); ?>>Fitness
            </label>
        </div>
    </div>
</div>

<div class="form-group row bottom-formulario-10">
    <!-- ********** GARDEN ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_garden"
                 <?php echo post_checkbox('param_tem_garden'); ?>>Garden
            </label>
        </div>
    </div>
    <!-- ********** GRADES ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_grades"
                 <?php echo post_checkbox('param_tem_grades'); ?>>Grades
            </label>
        </div>
    </div>
    <!-- ********** HIDRO ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_hidro"
                 <?php echo post_checkbox('param_tem_hidro'); ?>>Hidro
            </label>
        </div>
    </div>
    <!-- ********** INTERFONE ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_interfone"
                 <?php echo post_checkbox('param_tem_interfone'); ?>>Interfone
            </label>
        </div>
    </div>
</div>

<div class="form-group row bottom-formulario-10">
    <!-- ********** JARDIM DE INVERNO ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_jardim_inverno"
                 <?php echo post_checkbox('param_tem_jardim_inverno'); ?>>Jardim de Inverno
            </label>
        </div>
    </div>
    <!-- ********** MOBILIADO ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_mobiliado"
                 <?php echo post_checkbox('param_mobiliado'); ?>>Mobiliado
            </label>
        </div>
    </div>
    <!-- ********** PISCINA ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_piscina"
                 <?php echo post_checkbox('param_tem_piscina'); ?>>Piscina
            </label>
        </div>
    </div>
    <!-- ********** PISCINA INFANTIL ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_piscina_infantil"
                 <?php echo post_checkbox('param_tem_piscina_infantil'); ?>>Piscina Infantil
            </label>
        </div>
    </div>
</div>

<div class="form-group row bottom-formulario-10">
    <!-- ********** PISTA DE CAMINHADA ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_pista_caminhada"
                 <?php echo post_checkbox('param_tem_pista_caminhada'); ?>>Pista de Caminhada
            </label>
        </div>
    </div>
    <!-- ********** PLAYGROUND ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_playground"
                 <?php echo post_checkbox('param_tem_playground'); ?>>Playground
            </label>
        </div>
    </div>
    <!-- ********** PORTÃO ELETRÔNICO ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_portao_eletronico"
                 <?php echo post_checkbox('param_tem_portao_eletronico'); ?>>Portão Eletrônico
            </label>
        </div>
    </div>
    <!-- ********** PORTARIA 24H ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_portaria_24h"
                 <?php echo post_checkbox('param_tem_portaria_24h'); ?>>Portaria 24hrs
            </label>
        </div>
    </div>
</div>

<div class="form-group row bottom-formulario-10">
    <!-- ********** QUIOSQUE ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_quiosque"
                 <?php echo post_checkbox('param_tem_quiosque'); ?>>Quiosque
            </label>
        </div>
    </div>
    <!-- ********** SALÃO DE FESTAS ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_salao_festas"
                 <?php echo post_checkbox('param_tem_salao_festas'); ?>>Salão de Festas
            </label>
        </div>
    </div>
    <!-- ********** SALÃO DE JOGOS ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_salao_jogos"
                 <?php echo post_checkbox('param_tem_salao_jogos'); ?>>Salão de Jogos
            </label>
        </div>
    </div>
    <!-- ********** SAUNA ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_sauna"
                 <?php echo post_checkbox('param_tem_sauna'); ?>>Sauna
            </label>
        </div>
    </div>
</div>

<div class="form-group row bottom-formulario-10">
    <!-- ********** SISTEMA DE AR ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_sistema_ar"
                 <?php echo post_checkbox('param_tem_sistema_ar'); ?>>Sistema de Ar
            </label>
        </div>
    </div>
    <!-- ********** SISTEMA DE CFTV ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_sistema_cftv"
                 <?php echo post_checkbox('param_tem_sistema_cftv'); ?>>Sistema de CFTV
            </label>
        </div>
    </div>
    <!-- ********** SISTEMA DE INCÊNDIO ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_sistema_incendio"
                 <?php echo post_checkbox('param_tem_sistema_incendio'); ?>>Sistema de Incêndio
            </label>
        </div>
    </div>
    <!-- ********** SISTEMA DE SEGURANÇA ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_sistema_seguranca"
                 <?php echo post_checkbox('param_tem_sistema_seguranca'); ?>>Sistema de Segurança
            </label>
        </div>
    </div>
</div>

<div class="form-group row bottom-formulario-10">
    <!-- ********** SPA ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_spa"
                 <?php echo post_checkbox('param_tem_spa'); ?>>Spa
            </label>
        </div>
    </div>
    <!-- ********** ESPAÇO GOURMET ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_espaco_gourmet"
                 <?php echo post_checkbox('param_tem_espaco_gourmet'); ?>>Espaço Gourmet
            </label>
        </div>
    </div>
    <!-- ********** EDÍCULA ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_edicula"
                 <?php echo post_checkbox('param_tem_edicula'); ?>>Edícula
            </label>
        </div>
    </div>
    <!-- ********** QUINTAL ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_quintal"
                 <?php echo post_checkbox('param_tem_quintal'); ?>>Quintal
            </label>
        </div>
    </div>
</div>

<div class="form-group row bottom-formulario-10">
    <!-- ********** JARDIM ********** -->
    <div class="col-3">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" 
                 name="param_tem_jardim"
                 <?php echo post_checkbox('param_tem_jardim'); ?>>Jardim
            </label>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <hr class="separador padrao cinza"/>
    </div>
</div>

<!-- **************************************************** -->
<!-- ******************** FORMULARIO ******************** -->
<!-- **************************************************** -->
<div class="form-group row bottom-formulario-20">
    <!-- ********** AREA PRIVADA ********** -->
    <label for="number-area-privada" class="col-2 col-form-label">
        Área Privada
    </label>
    <div class="col-4">
        <input id="number-area-privada" class="form-control" type="text"
         value="<?php echo set_value('param_area_privada'); ?>" 
         name="param_area_privada" placeholder="235.04"
         mascara="area">
    </div>
    <!-- ********** CONSERVACAO ********** -->
    <label for="text-conservacao" class="col-2 col-form-label">
        Conservação
    </label>
    <div class="col-4">
        <input class="form-control" type="text" name="param_conservacao" 
         value="<?php echo set_value('param_conservacao'); ?>" id="text-conservacao"
         placeholder="Ex: Ótimo estado">
    </div>
</div>

<div class="form-group row bottom-formulario-20">
    <!-- ********** AREA TOTAL ********** -->
    <label for="number-area-total" class="col-2 col-form-label">
        Área Total
    </label>
    <div class="col-4">
        <input id="number-area-total" class="form-control" type="text"
         value="<?php echo set_value('param_area_total'); ?>" 
         name="param_area_total" placeholder="252.04"
         mascara="area">
    </div>
    <!-- ********** REVESTIMENTO EXTERNO ********** -->
    <label for="text-revestimento-externo" class="col-2 col-form-label">
        Revestimento Externo
    </label>
    <div class="col-4">
        <input class="form-control" type="text" name="param_revestimento_externo" 
         value="<?php echo set_value('param_revestimento_externo'); ?>" 
         id="text-revestimento-externo">
    </div>
</div>

<div class="form-group row bottom-formulario-20">
    <!-- ********** AREA AVERBADA ********** -->
    <label for="text-area-averbada" class="col-2 col-form-label">
        Área Averbada
    </label>
    <div class="col-4">
        <input class="form-control" type="text" name="param_area_averbada" 
         value="<?php echo set_value('param_area_averbada'); ?>" 
         id="text-area-averbada" mascara="area">
    </div>
    <!-- ********** ESQUADRIAS ********** -->
    <label for="text-esquadrias" class="col-2 col-form-label">
        Esquadrias
    </label>
    <div class="col-4">
        <input class="form-control" type="text" name="param_esquadrias" 
         value="<?php echo set_value('param_esquadrias'); ?>" 
         id="text-esquadrias">
    </div>
</div>

<div class="form-group row bottom-formulario-20">
    <!-- ********** CONSTRUTORA ********** -->
    <label for="text-construtora" class="col-2 col-form-label">
        Construtora
    </label>
    <div class="col-6">
        <input class="form-control" type="text" name="param_construtora" 
         value="<?php echo set_value('param_construtora'); ?>" 
         id="text-construtora">
    </div>
</div>

<div class="form-group row bottom-formulario-20">
    <!-- ********** VAGAS GARAGEM ********** -->
    <label for="number-tipo-garagem" class="col-2 col-form-label">
        Vagas de Garagem
    </label>
    <div class="col-2">
         <input class="form-control" type="number" min="0" step="1" 
         value="<?php echo set_value('param_qtd_vagas_garagem'); ?>" 
         name="param_qtd_vagas_garagem" placeholder="0"
         id="number-area-privada">
    </div>
    <div class="col-6">
        <?php echo form_dropdown('param_id_tipo_garagem', 
            $tipo_garagens, 
            set_value('param_id_tipo_garagem'), 
            'class="form-control"'); 
        ?>
    </div>
</div>

<div class="form-group row bottom-formulario-20">
    <!-- ********** LOCAL GARAGEM ********** -->
    <label for="drop-local-garagem" class="col-2 col-form-label">
        Local da Garagem
    </label>
    <div class="col-6">
        <?php echo form_dropdown('param_id_tipo_local_garagem', 
            $tipo_locais_garagem, 
            set_value('param_id_tipo_local_garagem'), 
            'class="form-control"'); 
        ?>
    </div>
</div>

<div class="form-group row bottom-formulario-20">
    <!-- ********** HORARIO DE VISITAS ********** -->
    <label for="text-horario-visitas" class="col-2 col-form-label">
        Horário de Visitas
    </label>
    <div class="col-4">
        <input class="form-control" type="text" name="param_horario_visitas" 
         value="<?php echo set_value('param_horario_visitas'); ?>" 
         id="text-horario-visitas">
    </div>
    <!-- ********** FACE DO IMOVEL ********** -->
    <label for="drop-face-imovel" class="col-2 col-form-label">
        Face do Imóvel
    </label>
    <div class="col-4">
        <?php echo form_dropdown('param_id_tipo_face_imovel', 
            $tipo_faces, 
            set_value('param_id_tipo_face_imovel'), 
            'class="form-control"'); 
        ?>
    </div>
</div>

<div class="form-group row bottom-formulario-20">
    <!-- ********** NUMERO DE PAVIMENTOS ********** -->
    <label for="number-numero-pavimentos" class="col-2 col-form-label">
        Número de Pavimentos
    </label>
    <div class="col-2">
        <input class="form-control" type="number" min="0" step="1" 
         value="<?php echo set_value('param_numero_pavimentos'); ?>" 
         name="param_numero_pavimentos" placeholder="0"
         id="number-numero-pavimentos">
    </div>
    <!-- ********** NUMERO DE ELEVADORES ********** -->
    <label for="number-numero-elevadores" class="offset-2 col-2 col-form-label">
        Número de Elevadores
    </label>
    <div class="col-2">
        <input class="form-control" type="number" min="0" step="1" 
         value="<?php echo set_value('param_numero_elevadores'); ?>" 
         name="param_numero_elevadores" placeholder="0"
         id="number-numero-elevadores">
    </div>
</div>

<div class="form-group row bottom-formulario-20">
    <!-- ********** APARTAMENTOS POR ANDAR ********** -->
    <label for="number-aptos-andar" class="col-2 col-form-label">
        Apartamentos por Andar
    </label>
    <div class="col-2">
        <input class="form-control" type="number" min="0" step="1" 
         value="<?php echo set_value('param_aptos_andar'); ?>" 
         name="param_aptos_andar" placeholder="0"
         id="number-aptos-andar">
    </div>
    <!-- ********** SITUACAO DOCUMENTAL ********** -->
    <label for="text-situacao-documental" class="offset-2 col-2 col-form-label">
        Situação Documental
    </label>
    <div class="col-4">
        <?php echo form_dropdown('param_id_tipo_situacao_documental', 
            $tipo_situacoes_documental, 
            set_value('param_id_tipo_situacao_documental'), 
            'class="form-control"'); 
        ?>
    </div>
</div>

<div class="form-group row bottom-formulario-20">
    <!-- ********** FINANCIAMENTO ********** -->
    <label for="drop-financiamento" class="col-2 col-form-label">
        Financiamento
    </label>
    <div class="col-4">
        <?php echo form_dropdown('param_id_tipo_financiamento', 
            $tipo_financiamentos, 
            set_value('param_id_tipo_financiamento'), 
            'class="form-control"'); 
        ?>
    </div>
    <!-- ********** TIPO DE PISO ********** -->
    <label for="text-tipo-piso" class="col-2 col-form-label">
        Tipo de Piso
    </label>
    <div class="col-4">
        <input class="form-control" type="text" name="param_tipo_piso" 
         value="<?php echo set_value('param_tipo_piso'); ?>" 
         id="text-tipo-piso">
    </div>
</div>

<div class="form-group row bottom-formulario-20">
    <!-- ********** AREA PRIVADA DO TERRENO ********** -->
    <label for="text-area-privada-terreno" class="col-2 col-form-label">
        Área Privada do Terreno
    </label>
    <div class="col-6">
        <input class="form-control" type="text" name="param_area_privada_terreno" 
         value="<?php echo set_value('param_area_privada_terreno'); ?>" 
         id="text-area-privada-terreno"
         mascara="area">
    </div>
</div>

<div class="row">
    <div class="col-12">
        <hr class="separador padrao cinza"/>
    </div>
</div>

<!-- **************************************************** -->
<!-- ******************** FORMULARIO ******************** -->
<!-- **************************************************** -->
<div class="form-group row bottom-formulario-20">
    <!-- ********** SERVICOS OPCIONAIS ********** -->
    <label for="text-servicos-opcionais" class="col-2 col-form-label">
        Serviços Opcionais
    </label>
    <div class="col-8">
        <textarea class="form-control" id="text-servicos-opcionais" 
         rows="5" name="param_servicos_opcionais" 
         value="<?php echo set_value('param_servicos_opcionais'); ?>"></textarea>
    </div>
</div>
