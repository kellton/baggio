<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="form-group row bottom-formulario-20">
	<a name="cadastro-imovel-dados-gerenciamento"></a>
	<!-- ********** DADOS GERENCIAIS ********** -->
	<div class="col">
		<legend>Dados Gerenciais</legend>
	</div>
</div>

<div class="form-group row bottom-formulario-20">
    <!-- ********** ANGARIADOR ********** -->
    <label for="number-corretor-angariador" class="col-2 col-form-label">
        Angariador <span class="texto-campo-obrigatorio">*</span>
    </label>
    <div class="col-6">
        <?php echo form_dropdown('param_id_corretor_angariador', 
            $corretores, 
            set_value('param_id_corretor_angariador'), 
            'class="form-control"'); 
        ?>
    </div>
</div>

<div class="form-group row bottom-formulario-20">
    <!-- ********** PROPRIETARIO ********** -->
    <label for="text-proprietario" class="col-2 col-form-label">
        Proprietário
    </label>
    <div class="col-6">
        <input class="form-control" type="text" name="param_proprietario" 
         value="<?php echo set_value('param_proprietario'); ?>" 
         placeholder="Digite para buscar pelo proprietário"
         id="text-proprietario">
    </div>
</div>

<div class="form-group row bottom-formulario-20">
    <!-- ********** ATUALIZAR ********** -->
    <label for="text-proprietario" class="col-2 col-form-label">
        Atualizar no XML
    </label>
    <div class="col-6">
        <label class="custom-control custom-checkbox dropdown-checkbox-grouper"
         data-toggle="tooltip" data-trigger="hover" data-html="true"  
         title="Caso esteja selecionada, esse imóvel será atualizado quando rodar a rotina de atualização dos XMLs">
          <input type="checkbox" class="custom-control-input"  
           name="param_xml_atualizavel" 
           <?php echo post_checkbox('param_xml_atualizavel'); ?>/>
          <span class="custom-control-indicator"></span>
          <span class="custom-control-description"></span>
        </label>
    </div>
</div>

<div class="row">
  <div class="offset-2 col-10">
    <hr class="separador padrao cinza"/>
  </div>
</div>