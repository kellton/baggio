<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container-fluid">
  <div class="row">
    <div class="col">
        <?php if (!empty($tabela_imoveis)) { echo $tabela_imoveis; } ?>
    </div>
  </div>

  <div class="row">
    <div class="col">
        <?php if (!empty($tabela_imoveis)) { echo $paginacao; } ?>
    </div>
  </div>
</div>

<!-- MODAL INFORMACOES COMPLEMENTARES -->
<div class="modal fade" id="modal-informacoes-complementares" tabindex="-1" role="dialog" aria-labelledby="modal-informacoes-complementares" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal-informacoes-complementares-label">
            Informações Complementares
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body"></div>
    </div>
  </div>
</div>

<!-- MODAL INFRAESTRUTURA -->
<div class="modal fade" id="modal-infraestrutura" tabindex="-1" role="dialog" aria-labelledby="modal-infraestrutura" aria-hidden="true" >
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal-infraestrutura-label">
            Infraestrutura
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body"></div>
    </div>
  </div>
</div>

<!-- MODAL ESPECIFICAÇÕES -->
<div class="modal fade" id="modal-especificacoes" tabindex="-1" role="dialog" aria-labelledby="modal-especificacoes" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal-especificacoes-label">
            Especificações
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body"></div>
    </div>
  </div>
</div>
 