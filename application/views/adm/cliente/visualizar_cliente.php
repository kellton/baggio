<?php
defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container-fluid">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h2>Informações Básicas</h2><hr>
        <div class="row">
            <div class="col">
                <p><b>Nome:</b> <?php echo $query['nome_cliente'] ?></p>
            </div>
            <div class="col">
                <p><b>E-mail:</b> <?php echo $query['email'] ?></p>
            </div>
            <div class="col">
                <p><b>Tipo de Cliente:</b> <?php echo $query['nome_tipo'] ?></p>
            </div>
            <div class="col">
                <p><b>Corretor:</b> <?php echo $query['nome_usuario'] ?></p>
            </div>
        </div>
        <hr>
        <h2>Dados Do Cliente</h2><hr>
        <div class="row justify-content-center">
           <?php if(!$tem_pretendente){ ?>
            <div class="col-auto">
                 <p>Cliente Ainda Não Enviou A Ficha de Cadastro</p>
            </div>
           <?php }else{?>
           <div class="row">
                <div class="col">
                    <div class="jumbotron jumbotron-fluid">
                        <div class="container">
                            <p><h2>Ficha de Cadastro do Pretendente</h2></p>
                             <p>Para visualiza-la:<button type="button" class="btn btn-link" data-toggle="modal" data-target="#fichaCadastro">Clique aqui</button></p>
                        </div>
                    </div>
                </div>
           </div>
           <?php }?>
           <?php if($segundo_pretendente){ ?>
             <div class="col-auto">
                <?php if(!$adicionou_seggundo_pretendente){?>
                    <p>Cliente Ainda Não Enviou A Ficha de Cadastro do Segundo Pretendente</p>
                 <?php }else{?>
                <div class="row">
                    <div class="col">
                        <div class="jumbotron jumbotron-fluid">
                            <div class="container">
                                <p><h2>Ficha de Cadastro do 2º Pretendente</h2></p>
                                <p>Para visualiza-la:<button type="button" class="btn btn-link" data-toggle="modal" data-target="#fichaCadastroSegundoPretendente">Clique aqui</button></p>
                            </div>
                        </div>
                    </div>
                </div>
                 <?php }?>
            </div>
           <?php }?>
            <?php if(!$tem_fiador){ ?>
            <div class="col-auto">
                 <p>Cliente Ainda Não Enviou A Ficha de Cadastro do Fiador</p>
            </div>
           <?php }else{?>
           <div class="row">
                <div class="col">
                    <div class="jumbotron jumbotron-fluid">
                        <div class="container">
                            <p><h2>Ficha de Cadastro do Fiador</h2></p>
                             <p>Para visualiza-la:<button type="button" class="btn btn-link" data-toggle="modal" data-target="#fichaCadastroFiador">Clique aqui</button></p>
                        </div>
                    </div>
                </div>
           </div>
           <?php }?>
        </div>
        <hr>
        <h2>Documentação do 1º Pretendente</h2>
        <hr>
        <div class="row justify-content-center">
            <div class="col-auto">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>RG e CPF / CNH</h2></p>
                        <?php if(!isset($tem_documentos)){ ?>
                            <p>Cliente Ainda Não Enviou Essses Documentos</p>
                        <?php } else{ ?>
                        <p>Para visualiza-los:<button type="button" class="btn btn-link" data-toggle="modal" data-target="#documentosModal">Clique aqui</button></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
            <div class="col-auto">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Comprovante de Endereço</h2></p>
                        <?php if(!isset($tem_comprovante)){ ?>
                            <p>Cliente Ainda Não Enviou Essses Documentos</p>
                        <?php } else{ ?>
                           <p>Para visualiza-los:<button type="button" class="btn btn-link" data-toggle="modal" data-target="#comprovanteEnderecoModal">Clique aqui</button></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>       
            <div class="col-auto">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Comprovação de Estado Civil</h2></p>
                        <?php if(!isset($tem_estado_civil)){ ?>
                            <p>Cliente Ainda Não Enviou Essses Documentos</p>
                        <?php } else{ ?>
                            <p>Para visualiza-los:<button type="button" class="btn btn-link" data-toggle="modal" data-target="#comprovanteCivilModal">Clique aqui</button></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
            <div class="col-auto">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Comprovação de Renda</h2></p>
                        <?php if(!isset($tem_renda)){ ?>
                            <p>Cliente Ainda Não Enviou Essses Documentos</p>
                        <?php } else{ ?>
                            <p>Para visualiza-los:<button type="button" class="btn btn-link" data-toggle="modal" data-target="#comprovanteRendaModal">Clique aqui</button></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
            <?php if(count($residentes)>1){?>
            <div class="col-auto">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Documentação dos Residentes</h2></p>
                       <?php if(!isset($tem_documetacao_residentes)){ ?>
                            <p>Cliente Ainda Não Enviou Essses Documentos</p>
                        <?php } else{ ?>
                        <p>Para visualiza-los:<button type="button" class="btn btn-link" data-toggle="modal" data-target="#visualizaComprovanteResidenteModal">Clique aqui</button></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
             <?php } ?>
            <div class="col-auto">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Outros Documentos</h2></p>
                        <?php if(!isset($tem_outros_documentos)){ ?>
                            <p>Cliente Ainda Não Enviou Essses Documentos</p>
                        <?php } else{ ?>
                            <p>Para visualiza-los:<button type="button" class="btn btn-link" data-toggle="modal" data-target="#visualizarOutrosDocumentosModal">Clique aqui</button></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
        </div>
        <?php if($segundo_pretendente){ ?>
        <hr>
        <h2>Documentação do 2º Pretendente</h2>
        <hr>
        <div class="row justify-content-center">
            <div class="col-auto">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>RG e CPF / CNH</h2></p>
                        <?php if(!isset($tem_documentos_secundario)){ ?>
                            <p>Cliente Ainda Não Enviou Essses Documentos</p>
                        <?php } else{ ?>
                            <p>Para visualiza-los:<button type="button" class="btn btn-link" data-toggle="modal" data-target="#documentosSecundarioModal">Clique aqui</button></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
            <div class="col-auto">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Comprovante de Endereço</h2></p>
                        <?php if(!isset($tem_comprovante_secundario)){ ?>
                            <p>Cliente Ainda Não Enviou Essses Documentos</p>
                        <?php } else{ ?>
                            <p>Para visualiza-los:<button type="button" class="btn btn-link" data-toggle="modal" data-target="#comprovanteEnderecoSecundarioModal">Clique aqui</button></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>       
            <div class="col-auto">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Comprovação de Estado Civil</h2></p>
                        <?php if(!isset($tem_estado_civil_secundario)){ ?>
                            <p>Cliente Ainda Não Enviou Essses Documentos</p>
                        <?php } else{ ?>
                            <p>Para visualiza-los:<button type="button" class="btn btn-link" data-toggle="modal" data-target="#comprovanteCivilSecundarioModal">Clique aqui</button></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
            <div class="col-auto">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Comprovação de Renda</h2></p>
                        <?php if(!isset($tem_renda_secundario)){ ?>
                            <p>Cliente Ainda Não Enviou Essses Documentos</p>
                        <?php } else{ ?>
                            <p>Para visualiza-los:<button type="button" class="btn btn-link" data-toggle="modal" data-target="#comprovanteRendaSecundarioModal">Clique aqui</button></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
            <div class="col-auto">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Outros Documentos</h2></p>
                        <?php if(!isset($tem_outros_documentos_secundario)){ ?>
                            <p>Cliente Ainda Não Enviou Essses Documentos</p>
                        <?php } else{ ?>
                            <p>Para visualiza-los:<button type="button" class="btn btn-link" data-toggle="modal" data-target="#visualizarOutrosDocumentosSecundarioModal">Clique aqui</button></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
            <?php 
        if($tem_conjugue['id_estado_civil']=="2" && $this->uri->segment(3)==0){?>
        <div class="col-auto">
            <div class="jumbotron jumbotron-fluid">
                <div class="container">
                    <p><h2>RG e CPF / CNH do Cônjugue</h2></p>
                    <?php if(!isset($tem_documentos_conjugue)){ ?>
                        <p>Cliente Ainda Não Enviou Essses Documentos</p>
                    <?php } else{ ?>
                        <p>Para visualiza-los:<button type="button" class="btn btn-link" data-toggle="modal" data-target="#visualizarDocumentosConjugueModal">Clique aqui</button></p>
                   <?php }
                ?>
                </div>
            </div>
        </div>
        <div class="col-auto">
            <div class="jumbotron jumbotron-fluid">
                <div class="container">
                    <p><h2>Comprovação de Renda do Conjugue</h2></p>
                    <?php if(!isset($tem_renda_conjugue)){ ?>
                        <p>Cliente Ainda Não Enviou Essses Documentos</p>
                    <?php } else{ ?>
                        <p>Para visualiza-los:<button type="button" class="btn btn-link" data-toggle="modal" data-target="#comprovanteRendaConjugueModal">Clique aqui</button></p>
                   <?php }
                ?>
                </div>
            </div>
        </div>
        <?php }?>
        </div>
        <?php }?>
        <hr>
        <h2>Documentação do Fiador</h2>
        <hr>
        <div class="row justify-content-center">
            <div class="col-auto">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Enviar RG e CPF / CNH</h2></p>
                        <?php if(!isset($tem_documentos_fiador)){ ?>
                            <p>Cliente Ainda Não Enviou Essses Documentos</p>
                        <?php } else{ ?>
                            <p>Para visualiza-los:<button type="button" class="btn btn-link" data-toggle="modal" data-target="#documentosFiadorModal">Clique aqui</button></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
            <div class="col-auto">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Comprovante de Endereço</h2></p>
                        <?php if(!isset($tem_comprovante_fiador)){ ?>
                            <p>Cliente Ainda Não Enviou Essses Documentos</p>
                        <?php } else{ ?>
                                <p>Para visualiza-los:<button type="button" class="btn btn-link" data-toggle="modal" data-target="#comprovanteEnderecoFiadorModal">Clique aqui</button></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
            <div class="col-auto">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Comprovação de Estado Civil</h2></p>
                        <?php if(!isset($tem_estado_civil_fiador)){ ?>
                            <p>Cliente Ainda Não Enviou Essses Documentos</p>
                        <?php } else{ ?>
                        <p>Para visualiza-los:<button type="button" class="btn btn-link" data-toggle="modal" data-target="#comprovanteCivilFiadorModal">Clique aqui</button></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
            <div class="col-auto">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Comprovação de Renda</h2></p>
                        <?php if(!isset($tem_renda_fiador)){ ?>
                            <p>Cliente Ainda Não Enviou Essses Documentos</p>
                        <?php } else{ ?>
                            <p>Para visualiza-los:<button type="button" class="btn btn-link" data-toggle="modal" data-target="#comprovanteRendaFiadorModal">Clique aqui</button></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
        <?php 
        if($tem_conjugue_fiador['id_estado_civil']=="2"){?>
            <div class="col-auto">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>RG e CPF / CNH do Cônjugue</h2></p>
                        <?php if(!isset($tem_documentos_conjugue_fiador)){ ?>
                            <p>Cliente Ainda Não Enviou Essses Documentos</p>
                        <?php } else{ ?>
                            <p>Para visualiza-los:<button type="button" class="btn btn-link" data-toggle="modal" data-target="#visualizarDocumentosConjugueFiadorModal">Clique aqui</button></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
             <div class="col-auto">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Comprovação de Renda do Cônjugue</h2></p>
                        <?php if(!isset($tem_renda_conjugue)){ ?>
                            <p>Cliente Ainda Não Enviou Essses Documentos</p>
                        <?php } else{ ?>
                    <p>Para visualiza-los:<button type="button" class="btn btn-link" data-toggle="modal" data-target="#comprovanteRendaConjugueFiadorModal">Clique aqui</button></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
        <?php }?>
        </div>
        <hr>
        <h2>Imóveis Vinculados</h2>
    </div>
</div>
