<?php
defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container-fluid">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <?php 
        if($this->session->flashdata('mensagem')){
            echo '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$this->session->flashdata('mensagem').'</div>';
        }?>
        
        <div class="row">
            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 text-left">
                <a class="btn btn-primary" href="<?php echo base_url('adm/clientes/adicionar-cliente');?>" role="button">Adicionar Cliente</a>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-right">
                <form method="post" action="<?php echo base_url("adm/clientes/resultados"); ?>" class="form-group row">
                    <div class="col-10">
                        <input name="busca_cliente" value="<?php echo set_value('busca_cliente');?>" class="form-control" type="search" placeholder="Pesquisar Cliente">
                    </div>
                    <div class="col-2">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i></button>
                    </div>
                </form>
            </div>
        </div>


        <div class="table-responsive">
            <?php 
                $tmpl = array (
                    'table_open'          => '<table class="table table-hover">',

                    'heading_row_start'   => '<tr>',
                    'heading_row_end'     => '</tr>',
                    'heading_cell_start'  => '<th>',
                    'heading_cell_end'    => '</th>',

                    'row_start'           => '<tr>',
                    'row_end'             => '</tr>',
                    'cell_start'          => '<td>',
                    'cell_end'            => '</td>',

                    'row_alt_start'       => '<tr>',
                    'row_alt_end'         => '</tr>',
                    'cell_alt_start'      => '<td>',
                    'cell_alt_end'        => '</td>',

                    'table_close'         => '</table>'
                );

                $this->table->set_template($tmpl);
                $this->table->set_heading('Nome','E-mail','Corretor','Tipo de Cliente','Tipo de Usuário','Status','Ações');
                foreach ($lista_usuarios as $linha) {
                    if($linha->ativo==0){
                        $ativo="Inativo";
                        $this->table->add_row($linha->nome_cliente,$linha->email,$linha->nome_usuario,$linha->tipo_cliente,$linha->nome_tipo,$ativo,anchor("adm/clientes/visualizar-cliente/$linha->id_cliente",'Visualizar', array('target'=>'_blank')).' - '.anchor("adm/clientes/editar-cliente/$linha->id_cliente",'Editar', array('target'=>'_blank'))); 
                    }
                    if($linha->ativo==1){
                        $ativo="Ativo";
                        $this->table->add_row($linha->nome_cliente,$linha->email,$linha->nome_usuario,$linha->tipo_cliente,$linha->nome_tipo,$ativo,anchor("adm/clientes/visualizar-cliente/$linha->id_cliente",'Visualizar', array('target'=>'_blank')).' - '.anchor("adm/clientes/editar-cliente/$linha->id_cliente",'Editar', array('target'=>'_blank')).' - '.anchor("adm/clientes/excluir-cliente/$linha->id_cliente",'Excluir', array('target'=>'_blank'))); 
                    }
                }
                echo $this->table->generate();
                if($paginacao) echo $paginacao;
            ?>
        </div>
    </div>
</div>