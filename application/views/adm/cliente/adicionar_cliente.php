<?php
defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container-fluid">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <?php echo validation_errors('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>','</div>'); ?>
        <form action="<?php echo base_url('adm/clientes/adicionar-cliente/validacao') ?>" method="POST" accept-charset="utf-8">
            <div class="form-group">
              <label for="nome_usuario">Nome:</label>
              <input type="text" class="form-control" name="nome_cliente" id="nome_usuario" placeholder="Nome" value="<?php echo set_value('nome_cliente')?>">
            </div>
            <div class="form-group">
              <label for="email">E-mail:</label>
              <input type="email" class="form-control" name="email" id="email" placeholder="E-mail" value="<?php echo set_value('email')?>">
            </div>
            <div class="form-group">
              <label for="telefone">Telefone:</label>
              <input type="text" class="form-control" name="telefone" mascara="cel" placeholder="Telefone" value="<?php echo set_value('telefone')?>">
            </div>
            <div class="form-group">
                <label>Selecione o Tipo de Cliente</label>
                <select name="tipo_cliente" class="form-control" togvis-select>
                   <option value="" selected="selected">Selecione Um Tipo de Cliente</option>
                   <option value="Pessoa Fisica" >Pessoa Física</option>
                   <option value="Pessoa Jurídica" >Pessoa Jurídica</option>
                </select>
            </div>
            <div class="form-group">
                <?php 
                    $tipo['']='Selecione um Tipo de Usuário';
                    $tipo[3]="Inquilino"?>
              <label for="tipo_usuario">Tipo de Usuário:</label>
             <?php echo form_dropdown('id_tipo_cliente', $tipo, set_value('id_tipo_cliente'), 'class="form-control"') ?>
            </div>
            <?php if(usuario_sessao_adm()['tipo_usuario']==4){
                 $array_corretores[usuario_sessao_adm()['id_usuario']]= usuario_sessao_adm()['nome_usuario'];
            }
            else{
                $array_corretores['']='Selecione um Colaborador';
                    foreach ($corretores as $value) {
                        $array_corretores[$value->id_usuario]=$value->nome_usuario;
                }
            }
            ?>
            <div class="form-group">
              <label for="id_corretor">Colaborador:</label>
             <?php echo form_dropdown('id_corretor', $array_corretores, set_value('id_corretor'), 'class="form-control"') ?>
            </div>
            <div class="form-group">
              <label for="senha">Senha:</label>
              <input type="password" class="form-control" name="senha" id="senha" placeholder="Senha" value="<?php echo set_value('senha')?>">
            </div>
            <div class="form-group">
              <label for="r_senha">Repetir Senha:</label>
              <input type="password" class="form-control" name="r_senha" id="senha" placeholder="Repetir Senha" value="<?php echo set_value('r_senha')?>">
            </div>
            <button type="submit" class="btn btn-primary">Adicionar Cliente</button>
        </form>
    </div>
</div>
