<div class="modal fade" id="fichaCadastroSegundoPretendente" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ficha de Cadastro 2º Pretendente</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        <div class="row">
            <div class="col-10">
                 <h2>Informações Básicas do 2º Pretendente</h2>
            </div>
            <div class="offset-1">
                <a class="btn btn-light" href="<?php echo base_url('adm/clientes/relatorio/segundo-pretendente/'.$query['id_cliente']) ?>" target="_blank"><i class="fa fa-print" aria-hidden="true"></i></a>
            </div>
        </div>
            <hr>
        <div class="row">
            <div class="col">
                <p><b>Nome Completo:</b> <?php echo $query3['nome_completo'] ?></p>
            </div>
            <div class="col">
                <p><b>Telefone Fixo:</b> <?php echo $query3['telefone'] ?></p>
            </div>
            <div class="col">
                <p><b>Telefone Celular:</b> <?php echo $query3['celular'] ?></p>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <p><b>E-mail do Pretendente:</b> <?php echo $query3['email_pretendente'] ?></p>
            </div>
            <div class="col">
                <p><b>E-mail do Morador:</b> <?php echo $query3['email_morador'] ?></p>
            </div>
            <div class="col">
                <p><b>Telefone Fixo:</b> <?php echo $query3['telefone'] ?></p>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <p><b>Data de Nascimento:</b> <?php echo formatar_data($query3['data_nascimento']) ?></p>
            </div>
            <div class="col">
                <p><b>Nacionalidade:</b> <?php echo $query3['nacionalidade'] ?></p>
            </div>
            <div class="col">
                <p><b>Estado Civil:</b> <?php echo $query3['nome_estado_civil'] ?></p>
            </div>
        </div>
        <hr>
        <h2>Documentos</h2>
        <hr>
        <div class="row">
            <div class="col">
                <p><b>CPF:</b> <?php echo $query3['cpf'] ?></p>
            </div>
            <div class="col">
                <p><b>RG:</b> <?php echo $query3['rg'] ?></p>
            </div>
            <div class="col">
                <p><b>Local de Expedição:</b> <?php echo $query3['expedicao'] ?></p>
            </div>
            <div class="col">
                <p><b>Data da Expedição do RG:</b> <?php echo formatar_data($query3['data_expedicao']) ?></p>
            </div>
        </div>
        <hr>
        <h2>Informações Profissionais</h2>
        <hr>
        <div class="row">
            <div class="col-4">
                <p><b>Profissão:</b> <?php echo $query3['profissao'] ?></p>
            </div>
            <div class="col-4">
                <p><b>Aposentado?</b> <?php if($query3['aposentado']==1){ echo 'Sim';}else{echo 'Não';} ?></p>
            </div>
        </div>
        <?php if($query3['aposentado']==0){?>
            <div class="row">
                <div class="col">
                    <p><b>Nome da Empresa que Trabalha:</b> <?php echo $query3['nome_empresa'] ?></p>
                </div>
                <div class="col">
                    <p><b>Telefone Comercial:</b> <?php echo $query3['telefone_empresa'] ?></p>
                </div>
                <div class="col">
                    <p><b>E-mail Comercial:</b> <?php echo $query3['email_comercial'] ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    <p><b>Data de Admissão:</b> <?php echo formatar_data($query3['data_admissao']) ?></p>
                </div>
                <div class="col-4">
                    <p><b>Tempo de Empresa:</b> <?php echo $query3['tempo_empresa'] ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <p><b>CEP:</b> <?php echo $query3['cep'] ?></p>
                </div>
                <div class="col">
                    <p><b>Logradouro:</b> <?php echo $query3['logradouro'] ?></p>
                </div>
                <div class="col">
                    <p><b>Número:</b> <?php echo $query3['numero'] ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    <p><b>Complemento:</b> <?php echo $query3['complemento'] ?></p>
                </div>
                <div class="col-4">
                    <p><b>Bairro:</b> <?php echo $query3['bairro'] ?></p>
                </div>
                <div class="col-4">
                    <p><b>País:</b> <?php echo $query3['nome_pais'] ?></p>
                </div>
            </div>
        <?php }?>
        <div class="row">
            <div class="col-4">
                <p><b>Salário do Pretendente:</b> <?php echo $query3['salario'] ?></p>
            </div>
            <div class="col-4">
                <p><b>Total da Renda:</b> <?php echo $query3['salario'] ?></p>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <p class="text-justify"><b>Outras Rendas:</b> <?php echo $query3['total_renda'] ?></p>
            </div>
        </div>
        <?php if($query3['id_estado_civil']==2){?>
            <hr>
            <h2>Dados do Cônjuge</h2>
            <hr>
            <div class="row">
            <div class="col">
                <p><b>Nome do Cônjugue:</b> <?php echo $query3['nome_completo_conjugue'] ?></p>
            </div>
            <div class="col">
                <p><b>Telefone Celular:</b> <?php echo $query3['celular_conjugue'] ?></p>
            </div>
            <div class="col">
                <p><b>E-mail:</b> <?php echo $query3['email_conjugue'] ?></p>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <p><b>Data de Nascimento:</b> <?php echo formatar_data($query3['data_nascimento_conjugue']) ?></p>
            </div>
            <div class="col">
                <p><b>Nacionalidade:</b> <?php echo $query3['nacionalidade_conjugue'] ?></p>
            </div>
            <div class="col">
                <p><b>CPF:</b> <?php echo $query3['cpf_conjugue'] ?></p>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <p><b>RG:</b> <?php echo $query3['rg_conjugue'] ?></p>
            </div>
            <div class="col">
                <p><b>Local de Expedição:</b> <?php echo $query3['expedicao_conjugue'] ?></p>
            </div>
            <div class="col">
                <p><b>Data da Expedição do RG:</b> <?php echo formatar_data($query3['data_expedicao_conjugue']) ?></p>
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                <p><b>Profissão:</b> <?php echo $query3['profissao_conjugue'] ?></p>
            </div>
            <div class="col-4">
                <p><b>Aposentado?</b> <?php if($query3['aposentado_conjugue']==1){ echo 'Sim';}else{echo 'Não';} ?></p>
            </div>
        </div>
        <?php if($query3['aposentado_conjugue']==0){?>
            <div class="row">
                <div class="col">
                    <p><b>Nome da Empresa que Trabalha:</b> <?php echo $query3['nome_empresa_conjugue'] ?></p>
                </div>
                <div class="col">
                    <p><b>Telefone Comercial:</b> <?php echo $query3['telefone_empresa_conjugue'] ?></p>
                </div>
                <div class="col">
                    <p><b>E-mail Comercial:</b> <?php echo $query3['email_comercial_conjugue'] ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    <p><b>Data de Admissão:</b> <?php echo formatar_data($query3['data_admissao_conjugue']) ?></p>
                </div>
                <div class="col-4">
                    <p><b>Tempo de Empresa:</b> <?php echo $query3['tempo_empresa_conjugue'] ?></p>
                </div>
            </div>
        <?php }?>
            <div class="row">
                <div class="col">
                    <p><b>Salário:</b> <?php echo $query3['salario_conjugue'] ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <p class="text-justify"><b>Outras Rendas:</b> <?php echo $query3['outras_rendas_conjugue'] ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <p class="text-justify"><b>Total da Renda:</b> <?php echo $query3['total_renda_conjugue'] ?></p>
                </div>
            </div>
        <?php }?>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
  </div>
</div>
</div>