<div class="modal fade" id="visualizarOutrosDocumentosSecundarioModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Visualizar Comprovação de Estado Civil</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body">
             <div class="row justify-content-center">
                <?php 
                $contador=1;
                foreach ($documentos_secundario as $value) {
                if($value->tipo_documento==21){ ?>
                 <div class="col-auto espacamento-comprovante-endereco">
                    <a class="btn btn-info" href="<?php echo base_url("publico/uploads/".md5($id_cliente)).'/'.$value->url_documento ?>" target="_blank">
                        Visualizar Outro Documento <?php echo $contador?></a>
                </div>
                <?php $contador++;}
                }?>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
    </div>
  </div>
</div>