<div class="modal fade" id="fichaCadastroFiador" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ficha de Cadastro do Fiador</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
       <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <div class="col-10">
                 <h2>Informações Básicas do Fiador</h2>
            </div>
            <div class="offset-1">
                <a class="btn btn-light" href="<?php echo base_url('adm/clientes/relatorio/fiador/'.$query['id_cliente']) ?>" target="_blank"><i class="fa fa-print" aria-hidden="true"></i></a>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col">
                <p><b>Nome Completo:</b> <?php echo $query4['nome_completo'] ?></p>
            </div>
            <div class="col">
                <p><b>Telefone Fixo:</b> <?php echo $query4['telefone'] ?></p>
            </div>
            <div class="col">
                <p><b>Telefone Celular:</b> <?php echo $query4['celular'] ?></p>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <p><b>E-mail Pessoal:</b> <?php echo $query4['email_pessoal'] ?></p>
            </div>
            <div class="col">
                <p><b>Data de Nascimento::</b> <?php echo formatar_data($query4['data_nascimento'])  ?></p>
            </div>
        </div>
         <div class="col">
                <p><b>Nacionalidade:</b> <?php echo $query4['nacionalidade'] ?></p>
            </div>
        <div class="row">
            <div class="col">
                <p><b>Estado Civil:</b> <?php echo $query4['nome_estado_civil'] ?></p>
            </div>
        </div>
        <hr>
        <h2>Documentos</h2>
        <hr>
        <div class="row">
            <div class="col">
                <p><b>CPF:</b> <?php echo $query4['cpf'] ?></p>
            </div>
            <div class="col">
                <p><b>RG:</b> <?php echo $query4['rg'] ?></p>
            </div>
            <div class="col">
                <p><b>Local de Expedição:</b> <?php echo $query4['expedicao'] ?></p>
            </div>
            <div class="col">
                <p><b>Data da Expedição do RG:</b> <?php echo formatar_data($query4['data_expedicao']) ?></p>
            </div>
        </div>
        <hr>
        <h2>Informações Profissionais</h2>
        <hr>
        <div class="row">
            <div class="col-4">
                <p><b>Profissão:</b> <?php echo $query4['profissao'] ?></p>
            </div>
            <div class="col-4">
                <p><b>Aposentado?</b> <?php if($query4['aposentado']==1){ echo 'Sim';}else{echo 'Não';} ?></p>
            </div>
        </div>
        <?php if($query4['aposentado']==0){?>
            <div class="row">
                <div class="col">
                    <p><b>Nome da Empresa que Trabalha:</b> <?php echo $query4['nome_empresa'] ?></p>
                </div>
                <div class="col">
                    <p><b>Telefone Comercial:</b> <?php echo $query4['telefone_empresa'] ?></p>
                </div>
                <div class="col">
                    <p><b>E-mail Comercial:</b> <?php echo $query4['email_comercial'] ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    <p><b>Data de Admissão:</b> <?php echo formatar_data($query4['data_admissao']) ?></p>
                </div>
                <div class="col-4">
                    <p><b>Tempo de Empresa:</b> <?php echo $query4['tempo_empresa'] ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <p><b>CEP:</b> <?php echo $query4['cep'] ?></p>
                </div>
                <div class="col">
                    <p><b>Logradouro:</b> <?php echo $query4['logradouro'] ?></p>
                </div>
                <div class="col">
                    <p><b>Número:</b> <?php echo $query4['numero'] ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <p><b>Complemento:</b> <?php echo $query4['complemento'] ?></p>
                </div>
                <div class="col">
                    <p><b>Bairro:</b> <?php echo $query4['bairro'] ?></p>
                </div>
                <div class="col">
                    <p><b>País:</b> <?php echo $query4['nome_pais'] ?></p>
                </div>
            </div>
        <?php }?>
        <div class="row">
            <div class="col-4">
                <p><b>Salário do Fiador:</b> <?php echo $query4['salario'] ?></p>
            </div>
            <div class="col-4">
                <p><b>Total da Renda:</b> <?php echo $query4['salario'] ?></p>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <p class="text-justify"><b>Outras Rendas:</b> <?php echo $query4['total_renda'] ?></p>
            </div>
        </div>
        <hr>
        <h2>Endereço Residencial</h2>
        <hr>
        <div class="row">
            <div class="col">
                <p><b>CEP:</b> <?php echo $query4['cep_fiador'] ?></p>
            </div>
            <div class="col">
                <p><b>Logradouro:</b> <?php echo $query4['logradouro_fiador'] ?></p>
            </div>
            <div class="col">
                <p><b>Número:</b> <?php echo $query4['numero_fiador'] ?></p>
            </div>
        </div>
        <div class="row">
            <?php if ($query4['complemento_fiador']!=''){?>
                <div class="col-4">
                    <p><b>Complemento:</b> <?php echo $query4['complemento_fiador'] ?></p>
                </div>
            <?php }?>
            <div class="col-4">
                <p><b>Bairro:</b> <?php echo $query4['bairro_fiador'] ?></p>
            </div>
            <div class="col-4">
                <p><b>País:</b> <?php echo $query4['nome_pais'] ?></p>
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                <p><b>Tempo de Residência:</b> <?php echo $query4['tempo_residencia'] ?></p>
            </div>
            <div class="col-4">
                <p><b>Residência Própria?</b> <?php if($query4['residencia_propria']==1){ echo 'Sim';}else{echo 'Não';} ?></p>
            </div>
        </div>
        <?php if($query4['id_estado_civil']==2){?>
            <hr>
            <h2>Dados do Cônjuge</h2>
            <hr>
            <div class="row">
            <div class="col">
                <p><b>Nome do Cônjugue:</b> <?php echo $query4['nome_completo_conjugue'] ?></p>
            </div>
            <div class="col">
                <p><b>Telefone Celular:</b> <?php echo $query4['celular_conjugue'] ?></p>
            </div>
            <div class="col">
                <p><b>E-mail:</b> <?php echo $query4['email_conjugue'] ?></p>
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                <p><b>Data de Nascimento:</b> <?php echo formatar_data($query4['data_nascimento_conjugue']) ?></p>
            </div>
            <div class="col-4">
                <p><b>Nacionalidade:</b> <?php echo $query4['nacionalidade_conjugue'] ?></p>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col">
                <p><b>RG:</b> <?php echo $query4['rg_conjugue'] ?></p>
            </div>
            <div class="col">
                <p><b>Local de Expedição:</b> <?php echo $query4['expedicao_conjugue'] ?></p>
            </div>
            <div class="col">
                <p><b>Data da Expedição do RG:</b> <?php echo formatar_data($query4['data_expedicao_conjugue']) ?></p>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <p><b>CPF:</b> <?php echo $query4['cpf_conjugue'] ?></p>
            </div>
            <div class="col">
                <p><b>Profissão:</b> <?php echo $query4['profissao_conjugue'] ?></p>
            </div>
            <div class="col">
                <p><b>Aposentado?</b> <?php if($query4['aposentado_conjugue']==1){ echo 'Sim';}else{echo 'Não';} ?></p>
            </div>
        </div>
        <?php if($query4['aposentado_conjugue']==0){?>
            <hr>
            <div class="row">
                <div class="col">
                    <p><b>Nome da Empresa que Trabalha:</b> <?php echo $query4['nome_empresa_conjugue'] ?></p>
                </div>
                <div class="col">
                    <p><b>Telefone Comercial:</b> <?php echo $query4['telefone_empresa_conjugue'] ?></p>
                </div>
                <div class="col">
                    <p><b>E-mail Comercial:</b> <?php echo $query4['email_comercial_conjugue'] ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    <p><b>Data de Admissão:</b> <?php echo formatar_data($query4['data_admissao_conjugue']) ?></p>
                </div>
                <div class="col-4">
                    <p><b>Tempo de Empresa:</b> <?php echo $query4['tempo_empresa_conjugue'] ?></p>
                </div>
            </div>
        <?php }?>
            <div class="row">
                <div class="col">
                    <p><b>Salário:</b> <?php echo $query4['salario_conjugue'] ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <p class="text-justify"><b>Outras Rendas:</b> <?php echo $query4['outras_rendas_conjugue'] ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <p class="text-justify"><b>Total da Renda:</b> <?php echo $query4['total_renda_conjugue'] ?></p>
                </div>
            </div>
        <?php }?>
        <hr>
        <h2>Propriedades</h2>
        <hr>
        <?php foreach ($propriedades_fiador as $value) { ?>
            <div class="row">
                <div class="col">
                    <p><b>Nº da Matícula do Imóvel:</b> <?php echo $value->matricula_imovel ?></p>
                </div>
                <div class="col">
                    <p><b>Circunscrição:</b> <?php echo $value->circunscricao ?></p>
                </div>
                <div class="col">
                    <p><b>Marca Do Veículo:</b> <?php echo $value->veiculo_marca ?></p>
                </div>
                <div class="col">
                    <p><b>Renavam:</b> <?php echo $value->renavam ?></p>
                </div>
            </div>
        <?php } ?>
        <hr>
        <h2>Referências Pessoais</h2>
        <hr>
        <?php foreach ($referencias_fiador as $value) {
            if($value->tipo_referencia==1){?>
            <div class="row">
                <div class="col">
                    <p><b>Nome:</b> <?php echo $value->nome_referencia ?></p>
                </div>
                <div class="col">
                    <p><b>Endereço:</b> <?php echo $value->endereco_referencia ?></p>
                </div>
                <div class="col">
                    <p><b>Telefone:</b> <?php echo $value->telefone_referencia ?></p>
                </div>
            </div>
        <?php }} ?>
        <hr>
        <h1>Referências Bacárias</h1>
        <hr>
         <?php foreach ($referencias_bancarias_fiador as $value) { ?>
            <div class="row">
                <div class="col">
                    <p><b>Banco:</b> <?php echo $value->banco_referencia ?></p>
                </div>
                <div class="col">
                    <p><b>Telefone da Agencia:</b> <?php echo $value->telefone_banco_referencia ?></p>
                </div>
                <div class="col">
                    <p><b>Nome da Agência:</b> <?php echo $value->nome_agencia_referencia ?></p>
                </div>
                <div class="col">
                    <p><b>Número da Conta:</b> <?php echo $value->numero_conta_referencia ?></p>
                </div>
            </div>
        <?php } ?>
        <hr>
        <h2>Informações Complementares</h2>
        <hr>
        <div class="row">
            <div class="col">
                <p class="text-justify"><b>Informações Complementares:</b> <?php echo $query4['observacoes_complementares'] ?></p>
            </div>
        </div>
       </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
    </div>
  </div>
</div>