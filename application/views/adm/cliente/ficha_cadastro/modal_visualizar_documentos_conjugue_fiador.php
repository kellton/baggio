<div class="modal fade" id="visualizarDocumentosConjugueFiadorModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Visualizar Documentos</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body">
            <div class="row justify-content-center">
                <?php foreach ($documentos_conjugue_fiador as $value) {
                if($value->tipo_documento==1){?>
                <div class="col-auto">
                    <a class="btn btn-info" href="<?php echo base_url("publico/uploads/".md5($id_cliente)).'/'.$value->url_documento ?>" target="_blank">Visualizar RG</a>
                </div>
                <?php }
                if($value->tipo_documento==2){?>
                <div class="col-auto">
                    <a class="btn btn-info" href="<?php echo base_url("publico/uploads/".md5($id_cliente)).'/'.$value->url_documento ?>" target="_blank">Visualizar CPF</a>
                </div>
                <?php }
                if($value->tipo_documento==3){?>
                <div class="col-auto">
                    <a class="btn btn-info" href="<?php echo base_url("publico/uploads/".md5($id_cliente)).'/'.$value->url_documento ?>" target="_blank">Visualizar CNH</a>
                </div>
                <?php }
                }?>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
    </div>
  </div>
</div>