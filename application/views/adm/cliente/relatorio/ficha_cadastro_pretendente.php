<div class="row">
    <div class="col-10">
         <h2>Informações Básicas do Pretendente</h2>
    </div>
    <div class="offset-1">
        <a class="btn btn-light" href="<?php echo base_url('adm/clientes/relatorio/pretendente/'.$query['id_cliente']) ?>"><i class="fa fa-print" aria-hidden="true"></i></a>
    </div>
</div>
<hr>
<div class="row">
    <div class="col">
        <p><b>Nome Completo:</b> <?php echo $query2['nome_completo'] ?></p>
    </div>
    <div class="col">
        <p><b>Telefone Fixo:</b> <?php echo $query2['telefone'] ?></p>
    </div>
    <div class="col">
        <p><b>Telefone Celular:</b> <?php echo $query2['celular'] ?></p>
    </div>
</div>
<div class="row">
    <div class="col">
        <p><b>E-mail do Pretendente:</b> <?php echo $query2['email_pretendente'] ?></p>
    </div>
    <div class="col">
        <p><b>E-mail do Morador:</b> <?php echo $query2['email_morador'] ?></p>
    </div>
    <div class="col">
        <p><b>Telefone Fixo:</b> <?php echo $query2['telefone'] ?></p>
    </div>
</div>
<div class="row">
    <div class="col">
        <p><b>Data de Nascimento:</b> <?php echo formatar_data($query2['data_nascimento']) ?></p>
    </div>
    <div class="col">
        <p><b>Nacionalidade:</b> <?php echo $query2['nacionalidade'] ?></p>
    </div>
    <div class="col">
        <p><b>Estado Civil:</b> <?php echo $query2['nome_estado_civil'] ?></p>
    </div>
</div>
<hr>
<h2>Documentos</h2>
<hr>
<div class="row">
    <div class="col">
        <p><b>CPF:</b> <?php echo $query2['cpf'] ?></p>
    </div>
    <div class="col">
        <p><b>RG:</b> <?php echo $query2['rg'] ?></p>
    </div>
    <div class="col">
        <p><b>Local de Expedição:</b> <?php echo $query2['expedicao'] ?></p>
    </div>
    <div class="col">
        <p><b>Data da Expedição do RG:</b> <?php echo formatar_data($query2['data_expedicao']) ?></p>
    </div>
</div>
<hr>
<h2>Informações Profissionais</h2>
<hr>
<div class="row">
    <div class="col-4">
        <p><b>Profissão:</b> <?php echo $query2['profissao'] ?></p>
    </div>
    <div class="col-4">
        <p><b>Aposentado?</b> <?php if($query2['aposentado']==1){ echo 'Sim';}else{echo 'Não';} ?></p>
    </div>
</div>
<?php if($query2['aposentado']==0){?>
    <div class="row">
        <div class="col">
            <p><b>Nome da Empresa que Trabalha:</b> <?php echo $query2['nome_empresa'] ?></p>
        </div>
        <div class="col">
            <p><b>Telefone Comercial:</b> <?php echo $query2['telefone_empresa'] ?></p>
        </div>
        <div class="col">
            <p><b>E-mail Comercial:</b> <?php echo $query2['email_comercial'] ?></p>
        </div>
    </div>
    <div class="row">
        <div class="col-4">
            <p><b>Data de Admissão:</b> <?php echo formatar_data($query2['data_admissao']) ?></p>
        </div>
        <div class="col-4">
            <p><b>Tempo de Empresa:</b> <?php echo $query2['tempo_empresa'] ?></p>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <p><b>CEP:</b> <?php echo $query2['cep'] ?></p>
        </div>
        <div class="col">
            <p><b>Logradouro:</b> <?php echo $query2['logradouro'] ?></p>
        </div>
        <div class="col">
            <p><b>Número:</b> <?php echo $query2['numero'] ?></p>
        </div>
    </div>
    <div class="row">
        <div class="col-4">
            <p><b>Complemento:</b> <?php echo $query2['complemento'] ?></p>
        </div>
        <div class="col-4">
            <p><b>Bairro:</b> <?php echo $query2['bairro'] ?></p>
        </div>
        <div class="col-4">
            <p><b>País:</b> <?php echo $query2['nome_pais'] ?></p>
        </div>
    </div>
<?php }?>
<div class="row">
    <div class="col-4">
        <p><b>Salário do Pretendente:</b> <?php echo $query2['salario'] ?></p>
    </div>
    <div class="col-4">
        <p><b>Total da Renda:</b> <?php echo $query2['salario'] ?></p>
    </div>
</div>
<div class="row">
    <div class="col">
        <p class="text-justify"><b>Outras Rendas:</b> <?php echo $query2['total_renda'] ?></p>
    </div>
</div>
<hr>
<h2>Endereço Principal do Pretendente</h2>
<hr>
<?php 
foreach ($enderecos as $value) {
    if($value->principal==1){ ?>
    <div class="row">
        <div class="col">
            <p><b>CEP:</b> <?php echo $value->cep_cliente ?></p>
        </div>
        <div class="col">
            <p><b>Logradouro:</b> <?php echo $value->logradouro_cliente ?></p>
        </div>
        <div class="col">
            <p><b>Número:</b> <?php echo $value->numero_cliente ?></p>
        </div>
    </div>
    <div class="row">
        <div class="col-4">
            <p><b>Bairro:</b> <?php echo $value->bairro_cliente ?></p>
        </div>
        <?php if ($value->complemento_cliente!=''){?>
            <div class="col-4">
                <p><b>Complemento:</b> <?php echo $value->complemento_cliente ?></p>
            </div>
        <?php }?>
        <div class="col-4">
            <p><b>País:</b> <?php echo $value->nome_pais ?></p>
        </div>
    </div>
<?php }} 
if(count($enderecos)==2){ ?>
<hr>
<h2>Endereço Alternativo Para Correspondências</h2>
<hr>
<?php 
foreach ($enderecos as $value) {
    if($value->principal==0){ ?>
    <div class="row">
        <div class="col">
            <p><b>CEP:</b> <?php echo $value->cep_cliente ?></p>
        </div>
        <div class="col">
            <p><b>Logradouro:</b> <?php echo $value->logradouro_cliente ?></p>
        </div>
        <div class="col">
            <p><b>Número:</b> <?php echo $value->numero_cliente ?></p>
        </div>
    </div>
    <div class="row">
        <div class="col-4">
            <p><b>Bairro:</b> <?php echo $value->bairro_cliente ?></p>
        </div>
        <?php if ($value->complemento_cliente!=''){?>
            <div class="col-4">
                <p><b>Complemento:</b> <?php echo $value->complemento_cliente ?></p>
            </div>
        <?php }?>
        <div class="col-4">
            <p><b>País:</b> <?php echo $value->nome_pais ?></p>
        </div>
    </div>
<?php }}} ?>
<hr>
<h2>Dados Adicionais do Pretendente</h2>
<hr>
<div class="row">
    <div class="col-4">
        <p><b>Tempo na Residência Atual:</b> <?php echo $query2['tempo_residencia'] ?></p>
    </div>
    <div class="col-4">
        <p><b>Residência Própria?</b> <?php if($query2['residencia_propria']==1){ echo 'Sim';}else{echo 'Não';} ?></p>
    </div>
    <?php if($query2['residencia_propria']==0){?>
        <div class="col-4">
            <p><b>Valor do Aluguel Atual:</b> <?php echo $query2['aluguel_atual'] ?></p>
        </div>
    <?php }?>
</div>
<div class="row">
    <div class="col">
        <p class="text-justify"><b>Motivo da Mundaça:</b> <?php echo $query2['motivo_mudanca'] ?></p>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <p><b>Nome do Locador ou Imobiliária Atual:</b> <?php echo $query2['imobiliaria_atual'] ?></p>
    </div>
    <div class="col-6">
        <p><b>Telefone do Locador ou Imobiliária Atual:</b> <?php echo $query2['telefone_imobiliaria_atual'] ?></p>
    </div>
</div>
<hr>
<h2>Pessoas que Irão Residir no Imóvel</h2>
<hr>
<?php foreach ($residentes as $value) { ?>
    <div class="row">
        <div class="col">
            <p><b>Nome:</b> <?php echo $value->nome_completo_residente ?></p>
        </div>
        <div class="col">
            <p><b>Parentesco:</b> <?php echo $value->parentesco_residente ?></p>
        </div>
        <div class="col">
            <p><b>Telefone:</b> <?php echo $value->telefone_residente ?></p>
        </div>
    </div>
<?php } ?>
<hr>
<h2>Propriedades</h2>
<hr>
 <?php foreach ($propriedades as $value) { ?>
    <div class="row">
        <div class="col">
            <p><b>Nº da Matícula do Imóvel:</b> <?php echo $value->matricula_imovel ?></p>
        </div>
        <div class="col">
            <p><b>Circunscrição:</b> <?php echo $value->circunscricao ?></p>
        </div>
        <div class="col">
            <p><b>Marca Do Veículo:</b> <?php echo $value->veiculo_marca ?></p>
        </div>
        <div class="col">
            <p><b>Renavam:</b> <?php echo $value->renavam ?></p>
        </div>
    </div>
<?php } ?>
<hr>
<h2>Referências Pessoais</h2>
<hr>
 <?php foreach ($referencias as $value) { ?>
    <div class="row">
        <div class="col">
            <p><b>Nome:</b> <?php echo $value->nome_referencia ?></p>
        </div>
        <div class="col">
            <p><b>Endereço:</b> <?php echo $value->endereco_referencia ?></p>
        </div>
        <div class="col">
            <p><b>Telefone:</b> <?php echo $value->telefone_referencia ?></p>
        </div>
    </div>
<?php } ?>
<hr>
<h2>Referências Bancárias</h2>
<hr>
 <?php foreach ($referencias_bancarias as $value) { ?>
    <div class="row">
        <div class="col">
            <p><b>Banco:</b> <?php echo $value->banco_referencia ?></p>
        </div>
        <div class="col">
            <p><b>Telefone da Agencia:</b> <?php echo $value->telefone_banco_referencia ?></p>
        </div>
        <div class="col">
            <p><b>Nome da Agência:</b> <?php echo $value->nome_agencia_referencia ?></p>
        </div>
        <div class="col">
            <p><b>Número da Conta:</b> <?php echo $value->numero_conta_referencia ?></p>
        </div>
    </div>
<?php } ?>
<hr>
<h2>Dados da Mãe</h2>
<hr>
<?php foreach ($pais as $value) { 
    if($value->mae==1){ ?>
        <div class="row">
            <div class="col">
                <p><b>Nome da Mãe:</b> <?php echo $value->nome ?></p>
            </div>
            <div class="col">
                <p><b>Telefone:</b> <?php echo $value->telefone ?></p>
            </div>
            <div class="col">
                <p><b>CEP:</b> <?php echo $value->cep ?></p>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <p><b>Logradouro:</b> <?php echo $value->logradouro ?></p>
            </div>
            <div class="col">
                <p><b>Número:</b> <?php echo $value->numero ?></p>
            </div>
            <div class="col">
                <p><b>Bairro:</b> <?php echo $value->bairro ?></p>
            </div>
        </div>
        <div class="row">
            <?php if($value->complemento!=''){?>
            <div class="col-4">
                <p><b>Complemento:</b> <?php echo $value->complemento ?></p>
            </div>
            <?php }?>
            <div class="col-4">
                <p><b>País:</b> <?php echo $value->nome_pais ?></p>
            </div>
        </div>
<?php }} ?>
<hr>
<h2>Dados do Pai</h2>
<hr>
<?php foreach ($pais as $value) { 
    if($value->mae==0){ ?>
        <div class="row">
            <div class="col">
                <p><b>Nome da Mãe:</b> <?php echo $value->nome ?></p>
            </div>
            <div class="col">
                <p><b>Telefone:</b> <?php echo $value->telefone ?></p>
            </div>
            <div class="col">
                <p><b>CEP:</b> <?php echo $value->cep ?></p>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <p><b>Logradouro:</b> <?php echo $value->logradouro ?></p>
            </div>
            <div class="col">
                <p><b>Número:</b> <?php echo $value->numero ?></p>
            </div>
            <div class="col">
                <p><b>Bairro:</b> <?php echo $value->bairro ?></p>
            </div>
        </div>
        <div class="row">
            <?php if($value->complemento!=''){?>
            <div class="col-4">
                <p><b>Complemento:</b> <?php echo $value->complemento ?></p>
            </div>
            <?php }?>
            <div class="col-4">
                <p><b>País:</b> <?php echo $value->nome_pais ?></p>
            </div>
        </div>
<?php }} ?>
<?php if($query2['id_estado_civil']==2){?>
    <hr>
    <h2>Dados do Cônjuge</h2>
    <hr>
    <div class="row">
    <div class="col">
        <p><b>Nome do Cônjugue:</b> <?php echo $query2['nome_completo_conjugue'] ?></p>
    </div>
    <div class="col">
        <p><b>Telefone Celular:</b> <?php echo $query2['celular_conjugue'] ?></p>
    </div>
    <div class="col">
        <p><b>E-mail:</b> <?php echo $query2['email_conjugue'] ?></p>
    </div>
</div>
<div class="row">
    <div class="col">
        <p><b>Data de Nascimento:</b> <?php echo formatar_data($query2['data_nascimento_conjugue']) ?></p>
    </div>
    <div class="col">
        <p><b>Nacionalidade:</b> <?php echo $query2['nacionalidade_conjugue'] ?></p>
    </div>
    <div class="col">
        <p><b>CPF:</b> <?php echo $query2['cpf_conjugue'] ?></p>
    </div>
</div>
<div class="row">
    <div class="col">
        <p><b>RG:</b> <?php echo $query2['rg_conjugue'] ?></p>
    </div>
    <div class="col">
        <p><b>Local de Expedição:</b> <?php echo $query2['expedicao_conjugue'] ?></p>
    </div>
    <div class="col">
        <p><b>Data da Expedição do RG:</b> <?php echo formatar_data($query2['data_expedicao_conjugue']) ?></p>
    </div>
</div>
<div class="row">
    <div class="col-4">
        <p><b>Profissão:</b> <?php echo $query2['profissao_conjugue'] ?></p>
    </div>
    <div class="col-4">
        <p><b>Aposentado?</b> <?php if($query2['aposentado_conjugue']==1){ echo 'Sim';}else{echo 'Não';} ?></p>
    </div>
</div>
<?php if($query2['aposentado_conjugue']==0){?>
    <div class="row">
        <div class="col">
            <p><b>Nome da Empresa que Trabalha:</b> <?php echo $query2['nome_empresa_conjugue'] ?></p>
        </div>
        <div class="col">
            <p><b>Telefone Comercial:</b> <?php echo $query2['telefone_empresa_conjugue'] ?></p>
        </div>
        <div class="col">
            <p><b>E-mail Comercial:</b> <?php echo $query2['email_comercial_conjugue'] ?></p>
        </div>
    </div>
    <div class="row">
        <div class="col-4">
            <p><b>Data de Admissão:</b> <?php echo formatar_data($query2['data_admissao_conjugue']) ?></p>
        </div>
        <div class="col-4">
            <p><b>Tempo de Empresa:</b> <?php echo $query2['tempo_empresa_conjugue'] ?></p>
        </div>
    </div>
<?php }?>
    <div class="row">
        <div class="col">
            <p><b>Salário:</b> <?php echo $query2['salario_conjugue'] ?></p>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <p class="text-justify"><b>Outras Rendas:</b> <?php echo $query2['outras_rendas_conjugue'] ?></p>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <p class="text-justify"><b>Total da Renda:</b> <?php echo $query2['total_renda_conjugue'] ?></p>
        </div>
    </div>
<?php }?>
<hr>
<h2>Observações Complementares</h2>
<hr>
<div class="row">
    <div class="col">
        <p class="text-justify"> <?php echo $query2['observacoes_complementares'] ?></p>
    </div>
</div>