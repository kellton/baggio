<div class="container-fluid">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="jumbotron jumbotron-fluid espacamento-titulo-pagina">
            <div class="container">
                <h2 class="display-4">Dados da empresa</h2>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col">
                <p><b>Razão Social:</b> <?php echo $query2['razao_social'] ?></p>
            </div>
            <div class="col">
                <p><b>Tipo:</b> <?php echo $query2['tipo'] ?></p>
            </div>
            <?php if($query2['tipo']=="OUTRO"){ ?>
                <div class="col">
                    <p><b>Tipo de Empresa:</b> <?php echo $query2['tipo_empresa'] ?></p>
                </div>
            <?php }?>
            <div class="col">
                <p><b>Data da Fundação:</b> <?php echo formatar_data($query2['fundacao']) ?></p>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <p><b>CNPJ:</b> <?php echo $query2['cnpj'] ?></p>
            </div>
            <div class="col">
                <p><b>Número de Registro na Junta Comercial:</b> <?php echo $query2['junta_comercial'] ?></p>
            </div>
            <div class="col">
                <p><b>Número Inscrição Estadual:</b> <?php echo $query2['inscricao_estadual'] ?></p>
            </div>
        </div>
        <div class="row">
            <div class="col-auto">
                <p><b>Capital Social da Empresa:</b> <?php echo $query2['capital_social'] ?></p>
            </div>
            <div class="col-auto">
                <p><b>Ramo de Atividade:</b> <?php echo $query2['ramo_atividade'] ?></p>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <p><b>CEP:</b> <?php echo $query2['cep'] ?></p>
            </div>
            <div class="col">
                <p><b>Logradouro:</b> <?php echo $query2['logradouro'] ?></p>
            </div>
            <div class="col">
                <p><b>Número:</b> <?php echo $query2['numero'] ?></p>
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                <p><b>Complemento:</b> <?php echo $query2['complemento'] ?></p>
            </div>
            <div class="col-4">
                <p><b>Bairro:</b> <?php echo $query2['bairro'] ?></p>
            </div>
            <div class="col-4">
                <p><b>País:</b> <?php echo $query2['nome_pais'] ?></p>
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                <p><b>E-mail Comercial:</b> <?php echo $query2['email_comercial'] ?></p>
            </div>
            <div class="col-4">
                <p><b>Telefone Comercial:</b> <?php echo $query2['telefone_comercial'] ?></p>
            </div>
        </div>
        <hr>
        <div class="jumbotron jumbotron-fluid">
            <div class="container">
                <h2 class="display-4">Dados do Sócio Representante</h2>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col">
                <p><b>Nome Completo:</b> <?php echo $query2['nome_completo'] ?></p>
            </div>
            <div class="col">
                <p><b>Telefone Fixo:</b> <?php echo $query2['telefone_fixo'] ?></p>
            </div>
            <div class="col">
                <p><b>Celular:</b> <?php echo $query2['celular'] ?></p>
            </div>
            <div class="col">
                <p><b>E-mail:</b> <?php echo $query2['email'] ?></p>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <p><b>Data de Nascimento:</b> <?php echo formatar_data($query2['data_nascimento']) ?></p>
            </div>
            <div class="col">
                <p><b>Nacionalidade:</b> <?php echo $query2['nacionalidade'] ?></p>
            </div>
            <div class="col">
                <p><b>CPF:</b> <?php echo $query2['cpf'] ?></p>
            </div>
            <div class="col">
                <p><b>RG:</b> <?php echo $query2['rg'] ?></p>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <p><b>Local de Expedição:</b> <?php echo $query2['expedicao_rg'] ?></p>
            </div>
            <div class="col">
                <p><b>Data da Expedição do RG:</b> <?php echo formatar_data($query2['data_expedicao']) ?></p>
            </div>
            <div class="col">
                <p><b>Estado Civil:</b> <?php echo $query2['nome_estado_civil'] ?></p>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <p><b>Profissão:</b> <?php echo $query2['profissao'] ?></p>
            </div>
            <div class="col">
                <p><b>Cargo Exercido:</b> <?php echo $query2['cargo'] ?></p>
            </div>
            <div class="col">
                <p><b>Salário:</b> <?php echo $query2['salario'] ?></p>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <p><b>Outras Rendas:</b> <?php echo $query2['outras_rendas'] ?></p>
            </div>
            <div class="col">
                <p><b>Renda Total:</b> <?php echo $query2['renda_total'] ?></p>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col">
                <p><b>Residência Própria?</b> <?php if($query2['residencia'] =="1"){ echo 'Sim';}else{echo 'Não';}  ?></p>
            </div>
            <div class="col">
                <p><b>CEP da Residência:</b> <?php echo $query2['cep_cliente'] ?></p>
            </div>
            <div class="col">
                <p><b>Logradouro:</b> <?php echo $query2['logradouro_cliente'] ?></p>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <p><b>Número:</b> <?php echo $query2['numero_cliente']  ?></p>
            </div>
            <div class="col">
                <p><b>Complemento:</b> <?php echo $query2['complemento_cliente'] ?></p>
            </div>
            <div class="col">
                <p><b>Bairro:</b> <?php echo $query2['bairro_cliente'] ?></p>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <p><b>País:</b> <?php echo $query2['nome_pais'] ?></p>
            </div>
        </div>
        <hr>
        <div class="jumbotron jumbotron-fluid">
            <div class="container">
                <h2 class="display-4">Dados dos Sócios ou Diretores</h2>
            </div>
        </div>
        <hr>
        <?php 
        $i=1;
        foreach ($socios as $value) { ?>
        <h3>Dados do Sócio/Diretor Nº<?php echo $i; ?></h3><hr>
            <div class="row">
                <div class="col">
                    <p><b>Nome:</b> <?php echo $value->nome_completo_diretor ?></p>
                </div>
                <div class="col">
                    <p><b>Cargo Exercido:</b> <?php echo $value->cargo_diretor ?></p>
                </div>
                <div class="col">
                    <p><b>Data de Nascimento:</b> <?php echo formatar_data($value->data_nascimento_diretor) ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <p><b>Nacionalidade:</b> <?php echo $value->nacionalidade_diretor ?></p>
                </div>
                <div class="col">
                    <p><b>CPF:</b> <?php echo $value->cpf_diretor ?></p>
                </div>
                <div class="col">
                    <p><b>RG:</b> <?php echo $value->rg_diretor ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <p><b>Estado Civil:</b> <?php echo $value->nome_estado_civil ?></p>
                </div>
                <div class="col">
                    <p><b>E-mail Pessoal:</b> <?php echo $value->email_pessoal_diretor ?></p>
                </div>
                <div class="col">
                    <p><b>E-mail Comercial:</b> <?php echo $value->email_comercial_diretor ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <p><b>Telefone:</b> <?php echo $value->telefone_diretor ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <p><b>CEP:</b> <?php echo $value->cep_diretor ?></p>
                </div>
                <div class="col">
                    <p><b>Logradouro:</b> <?php echo $value->logradouro_diretor ?></p>
                </div>
                <div class="col">
                    <p><b>Número:</b> <?php echo $value->numero_diretor ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <p><b>Complemento:</b> <?php echo $value->complemento_diretor ?></p>
                </div>
                <div class="col">
                    <p><b>Bairro:</b> <?php echo $value->bairro_diretor ?></p>
                </div>
                <div class="col">
                    <p><b>Pais:</b> <?php echo $value->nome_pais ?></p>
                </div>
            </div>
            <hr>
        <?php $i++; }?>
        <div class="jumbotron jumbotron-fluid">
            <div class="container">
                <h2 class="display-4">Propriedades da Empresa</h2>
            </div>
        </div>
        <hr>
         <?php foreach ($propriedades as $value) { ?>
            <div class="row">
                <div class="col">
                    <p><b>Nº da Matícula do Imóvel:</b> <?php echo $value->matricula_imovel ?></p>
                </div>
                <div class="col">
                    <p><b>Circunscrição:</b> <?php echo $value->circunscricao ?></p>
                </div>
                <div class="col">
                    <p><b>Marca Do Veículo:</b> <?php echo $value->veiculo_marca ?></p>
                </div>
                <div class="col">
                    <p><b>Renavam:</b> <?php echo $value->renavam ?></p>
                </div>
            </div>
        <?php } ?>
        <hr>
        <div class="jumbotron jumbotron-fluid">
            <div class="container">
                <h2 class="display-4">Referências Comerciais</h2>
            </div>
        </div>
        <hr>
         <?php foreach ($referencias as $value) { 
             if($value->tipo_referencia==2){?>
            <div class="row">
                <div class="col">
                    <p><b>Nome:</b> <?php echo $value->nome_referencia ?></p>
                </div>
                <div class="col">
                    <p><b>Endereço:</b> <?php echo $value->endereco_referencia ?></p>
                </div>
                <div class="col">
                    <p><b>Telefone:</b> <?php echo $value->telefone_referencia ?></p>
                </div>
            </div>
             <?php }} ?>
        <hr>
        <div class="jumbotron jumbotron-fluid">
            <div class="container">
                <h2 class="display-4">Referências Bacárias</h2>
            </div>
        </div>
        <hr>
         <?php foreach ($referencias_bancarias as $value) { ?>
            <div class="row">
                <div class="col">
                    <p><b>Banco:</b> <?php echo $value->banco_referencia ?></p>
                </div>
                <div class="col">
                    <p><b>Telefone da Agencia:</b> <?php echo $value->telefone_banco_referencia ?></p>
                </div>
                <div class="col">
                    <p><b>Nome da Agência:</b> <?php echo $value->nome_agencia_referencia ?></p>
                </div>
                <div class="col">
                    <p><b>Número da Conta:</b> <?php echo $value->numero_conta_referencia ?></p>
                </div>
            </div>
        <?php } ?>
        <hr>
        <div class="jumbotron jumbotron-fluid">
            <div class="container">
                <h2 class="display-4">Informações Complementares</h2>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col">
                <p class="text-justify"> <b>Informações Complementares:</b> <?php echo $query2['observacoes_complementares'] ?></p>
            </div>
        </div>
    </div>
</div>