<?php
defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container-fluid">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h2>Informações Básicas</h2><hr>
        <div class="row">
            <div class="col">
                <p><b>Nome:</b> <?php echo $query['nome_cliente'] ?></p>
            </div>
            <div class="col">
                <p><b>E-mail:</b> <?php echo $query['email'] ?></p>
            </div>
            <div class="col">
                <p><b>Tipo de Cliente:</b> <?php echo $query['nome_tipo'] ?></p>
            </div>
            <div class="col">
                <p><b>Corretor:</b> <?php echo $query['nome_usuario'] ?></p>
            </div>
        </div>
        <hr>
        <h2>Dados Do Cliente</h2><hr>
        <div class="row justify-content-center">
           <?php if(!$tem_ficha_cadastro){ ?>
            <div class="col-auto">
                 <p>Cliente Ainda Não Enviou A Ficha de Cadastro</p>
            </div>
           <?php }else{?>
           <div class="row">
                <div class="col">
                    <div class="jumbotron jumbotron-fluid">
                        <div class="container">
                            <p><h2>Ficha de Cadastro Pessoa Jurídica</h2></p>
                             <p>Para visualiza-la:<button type="button" class="btn btn-link" data-toggle="modal" data-target="#fichaCadastroJuridico">Clique aqui</button></p>
                        </div>
                    </div>
                </div>
           </div>
           <?php }?>
        </div>
        <hr>
        <h2>Documentação</h2>
        <hr>
        <div class="row justify-content-center">
            <div class="col-auto">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Contrato Social</h2></p>
                        <?php if(!isset($tem_contrato_social)){ ?>
                            <p>Cliente Ainda Não Enviou O Contrato Social</p>
                        <?php } else{ ?>
                    <p>Para visualiza-lo<button type="button" class="btn btn-link" data-toggle="modal" data-target="#visualizarContratoSocialModal">Clique aqui</button></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
            <div class="col-auto">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Últimas Alterações do Contrato Social</h2></p>
                        <?php if(!isset($tem_alteracao_contrato_social)){ ?>
                            <p>Cliente Ainda Não Enviou Essa Documetação</p>
                        <?php } else{ ?>
                            <p>Para visualiza-lo<button type="button" class="btn btn-link" data-toggle="modal" data-target="#visualizaralteracaoContratoSocial">Clique aqui</button></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
            <div class="col-auto">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Balanço</h2></p>
                        <?php if(!isset($tem_balanco)){ ?>
                            <p>Cliente Ainda Não Enviou o Balanço</p>
                        <?php } else{ ?>
                            <p>Para visualiza-lo<button type="button" class="btn btn-link" data-toggle="modal" data-target="#visualizarBalancoModal">Clique aqui</button></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
            <div class="col-auto">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Balancete</h2></p>
                        <?php if(!isset($tem_balancete)){ ?>
                            <p>Cliente Ainda Não Enviou o Balancete</p>
                        <?php } else{ ?>
                            <p>Para visualiza-lo<button type="button" class="btn btn-link" data-toggle="modal" data-target="#visualizarBalanceteModal">Clique aqui</button></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
            <div class="col-auto">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Cartão CNPJ</h2></p>
                        <?php if(!isset($tem_cartao)){ ?>
                            <p>Cliente Ainda Não Enviou o Cartão CNPJ</p>
                        <?php } else{ ?>
                                <p>Para visualiza-lo<button type="button" class="btn btn-link" data-toggle="modal" data-target="#visualizarcartaoCNPJModal">Clique aqui</button></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
            <div class="col-auto">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Declaração de Imposto de Renda</h2></p>
                        <?php if(!isset($tem_imposto_renda)){ ?>
                            <p>Cliente Ainda Não Enviou a Declaração de Impsto de Renda</p>
                        <?php } else{ ?>
                                <p>Para visualiza-lo<button type="button" class="btn btn-link" data-toggle="modal" data-target="#visualizarImpostoRendaModal">Clique aqui</button></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
            <div class="col-auto">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Certidão Negativa</h2></p>
                        <?php if(!isset($tem_certidao_negativa)){ ?>
                            <p>Cliente Ainda Não Enviou a Certidão Negativa</p>
                        <?php } else{ ?>
                            <p>Para visualiza-la<button type="button" class="btn btn-link" data-toggle="modal" data-target="#visualizarCertidaoNegativaModal">Clique aqui</button></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
            <div class="col-auto">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Outros Documentos</h2></p>
                        <?php if(!isset($tem_outros_documentos)){ ?>
                            <p>Cliente Não Enviou Outros Documentos</p>
                        <?php } else{ ?>
                                <p>Para visualiza-lo<button type="button" class="btn btn-link" data-toggle="modal" data-target="#visualizarOutrosDocumentosModal">Clique aqui</button></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <h2>Documetação dos Sócios/Diretores</h2>
        <hr>
        <div class="row justify-content-center">
            <div class="col-auto">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>RG e CPF / CNH</h2></p>
                        <?php if(!isset($tem_rg_cnh)){ ?>
                            <p>Cliente Não Enviou Esses Documentos</p>
                        <?php } else{ ?>
                    <p>Para visualiza-los<button type="button" class="btn btn-link" data-toggle="modal" data-target="#documentosModal">Clique aqui</button></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
            <div class="col-auto">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Comprovante de Endereço</h2></p>
                        <?php if(!isset($tem_comprovante_residencia)){ ?>
                            <p>Cliente Não Enviou Esses Documentos</p>
                        <?php } else{ ?>
                            <p>Para visualiza-lo<button type="button" class="btn btn-link" data-toggle="modal" data-target="#comprovanteEnderecoModal">Clique aqui</button></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <h2>Imóveis Vinculados</h2>
    </div>
</div>
