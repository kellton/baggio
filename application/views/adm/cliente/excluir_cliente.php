<?php
defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container-fluid">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <form action="<?php echo base_url('adm/clientes/excluir-cliente/validacao/'.$query['id_cliente']) ?>" method="POST" accept-charset="utf-8">
            <div class="form-group">
              <label for="nome_usuario">Nome:</label>
              <input type="text" class="form-control" name="nome_cliente" id="nome_usuario" placeholder="Nome" disabled="disabled" value="<?php echo set_value('nome_cliente',$query['nome_cliente'])?>">
            </div>
            <div class="form-group">
              <label for="email">E-mail:</label>
              <input type="email" class="form-control" name="email" id="email" placeholder="E-mail" disabled="disabled" value="<?php echo set_value('email',$query['email'])?>">
            </div>
            <div class="form-group">
                <?php 
                    $tipo['']='Selecione um Tipo de Cliente';
                    foreach ($tipo_usuario as $value) {
                        $tipo[$value->id_tipo_cliente]=$value->nome_tipo;
                }?>
              <label for="tipo_usuario">Tipo de Cliente:</label>
             <?php echo form_dropdown('id_tipo_cliente', $tipo, set_value('id_tipo_cliente',$query['id_tipo_cliente']), 'class="form-control" disabled="disabled"') ?>
            </div>
            <div class="form-group">
               <?php 
                $array_corretores['']='Selecione um Colaborador';
                    foreach ($corretores as $value) {
                        $array_corretores[$value->id_usuario]=$value->nome_usuario;
                    }
            ?>
            <label for="id_corretor">Colaborador:</label>
            <?php echo form_dropdown('id_corretor', $array_corretores, set_value('id_corretor',$query['id_corretor']), 'class="form-control" disabled="disabled"') ?>
            </div>
            <div class="form-group">
              <label for="telefone">Telefone(WhatsApp):</label>
              <input type="text" class="form-control" name="telefone" placeholder="Telefone" disabled="disabled" value="<?php echo set_value('telefone',$query['telefone'])?>">
            </div>
            <button type="submit" class="btn btn-primary">Excluir Cliente</button>
        </form>
    </div>
</div>
