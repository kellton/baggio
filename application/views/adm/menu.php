<div class="">
    <nav class="navbar navbar-expand-md bg-faded navbar-light col-12 col-sm-12 offset-md-1 offset-lg-1 col-md-10 col-lg-10">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>

        <!-- BRAND -->
        <a class="navbar-brand" href="<?php echo base_url(); ?>">
            <img width="160px" src="<?php echo base_url('publico/imagens/baggio-logo-30-anos.png'); ?>">
        </a>

        <div class="collapse navbar-collapse text-right" id="collapsibleNavbar">
            <ul class="navbar-nav">
                <!-- INICIO -->
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url('adm'); ?>">Início</a>
                </li>
                <!-- USUARIOS -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                        Usuários
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="<?php echo base_url('adm/usuarios/adicionar-usuario') ?>">Cadastrar</a>
                        <a class="dropdown-item" href="<?php echo base_url('adm/usuarios') ?>">Gerenciar</a>
                    </div>
                </li>
                <!-- CLIENTES -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                        Clientes
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="<?php echo base_url('adm/clientes/adicionar-cliente') ?>">Cadastrar</a>
                        <a class="dropdown-item" href="<?php echo base_url('adm/clientes') ?>">Gerenciar</a>
                    </div>
                </li>
                <!-- IMOVEIS -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                        Imóveis
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="<?php echo base_url('adm/imoveis/cadastrar-imovel'); ?>">
                            Cadastrar
                        </a>
                        <a class="dropdown-item" href="<?php echo base_url('adm/gerenciar/imoveis'); ?>">
                            Gerenciar
                        </a>
                        <a class="dropdown-item" href="<?php echo base_url('adm/imoveis'); ?>">
                            Visualizar
                        </a>
                    </div>
                </li>
                <!-- SISTEMA -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                        Sistema
                    </a>
                    <div class="dropdown-menu">
                        <!-- <a class="dropdown-item" href="<?php echo base_url('adm/gerenciar/auxiliador-bairros'); ?>">
                            Gerenciar Auxiliador de Bairros
                        </a> -->
                        <a class="dropdown-item" href="<?php echo base_url('adm/gerenciar/bairros'); ?>">
                            Gerenciar Bairros
                        </a>
                        <a class="dropdown-item" href="<?php echo base_url('adm/gerenciar/cidades'); ?>">
                            Gerenciar Cidades
                        </a>
                        <a class="dropdown-item" href="<?php echo base_url('adm/gerenciar/estados'); ?>">
                            Gerenciar Estados
                        </a>
                        <a class="dropdown-item" href="<?php echo base_url('adm/gerenciar/jobs'); ?>">
                            Gerenciar Tarefas de Atualização dos XMLs
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</div>



<!-- menu mobile -->
<div class="menu-mobile-container d-lg-none text-center d-print-none">
    <div class="row">
        <div class="col-item-mobile text-center">
            <a class="item-mobile" href="<?php echo base_url(); ?>">
                <i class="material-icons">home</i>
            </a>
        </div>
        <div class="col-item-mobile text-center">
            <a class="item-mobile" href="<?php echo base_url("favoritos"); ?>">
                <i class="material-icons">favorite</i>
            </a>
        </div>
        <div class="col-item-mobile text-center">
            <a class="item-mobile" href="<?php echo base_url("buscar/sessao/imoveis"); ?>">
                <i class="material-icons">search</i>
            </a>
        </div>
        <div class="col-item-mobile text-center">
            <a class="item-mobile" href="<?php echo base_url("contato"); ?>">
                <i class="material-icons">phone</i>
            </a>
        </div>
        <div class="col-item-mobile text-center">
            <a class="item-mobile" href="<?php echo base_url("login"); ?>">
                <i class="material-icons">person</i>
            </a>
        </div>
    </div>
</div>
