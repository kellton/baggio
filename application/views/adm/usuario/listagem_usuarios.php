<?php
defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container-fluid">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <?php 
        if($this->session->flashdata('mensagem')){
            echo '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$this->session->flashdata('mensagem').'</div>';
        }?>
        <a class="btn btn-primary" href="<?php echo base_url('adm/usuarios/adicionar-usuario');?>" role="button">Adicionar Usuário</a>
        <div class="table-responsive">
            <?php 
                $tmpl = array (
                    'table_open'          => '<table class="table table-hover">',

                    'heading_row_start'   => '<tr>',
                    'heading_row_end'     => '</tr>',
                    'heading_cell_start'  => '<th>',
                    'heading_cell_end'    => '</th>',

                    'row_start'           => '<tr>',
                    'row_end'             => '</tr>',
                    'cell_start'          => '<td>',
                    'cell_end'            => '</td>',

                    'row_alt_start'       => '<tr>',
                    'row_alt_end'         => '</tr>',
                    'cell_alt_start'      => '<td>',
                    'cell_alt_end'        => '</td>',

                    'table_close'         => '</table>'
                );

                $this->table->set_template($tmpl);
                $this->table->set_heading('Nome','E-mail','Telefone','Tipo de Usuário','Status','Ações');
                foreach ($lista_usuarios as $linha) {
                    if($linha->ativo==0){
                        $ativo="Inativo";
                        $this->table->add_row($linha->nome_usuario,$linha->email,$linha->telefone,$linha->nome_tipo,$ativo,anchor("adm/usuarios/editar-usuario/$linha->id_usuario",'Editar').' - '.anchor("adm/usuarios/reativar-usuario/$linha->id_usuario",'Reativar')); 
                    }
                    if($linha->ativo==1){
                        $ativo="Ativo";
                        $this->table->add_row($linha->nome_usuario,$linha->email,$linha->telefone,$linha->nome_tipo,$ativo,anchor("adm/usuarios/editar-usuario/$linha->id_usuario",'Editar').' - '.anchor("adm/usuarios/excluir-usuario/$linha->id_usuario",'Excluir')); 
                    }
                    
                }
                echo $this->table->generate();
                if($paginacao) echo $paginacao;
            ?>
        </div>
    </div>
</div>
 
