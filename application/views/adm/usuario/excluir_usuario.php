<?php
defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container-fluid">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <?php echo validation_errors('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>','</div>'); ?>
        <form action="<?php echo base_url('adm/usuarios/excluir-usuario/validacao/').$query['id_usuario'] ?>" method="POST" accept-charset="utf-8">
            <div class="form-group">
              <label for="nome_usuario">Nome:</label>
              <input type="text" class="form-control" name="nome_usuario" id="nome_usuario" placeholder="Nome" disabled="disabled" value="<?php echo set_value('nome_usuario',$query['nome_usuario'])?>">
            </div>
            <div class="form-group">
              <label for="email">E-mail:</label>
              <input type="email" class="form-control" name="email" id="email" placeholder="E-mail" disabled="disabled" value="<?php echo set_value('email',$query['email'])?>">
            </div>
            <div class="form-group">
              <label for="telefone">Telefone:</label>
              <input type="text" class="form-control" name="telefone" id="email" placeholder="Telefone" disabled="disabled" value="<?php echo set_value('telefone',$query['telefone'])?>">
            </div>
            <div class="form-group">
                <?php 
                    $tipo['']='Selecione um Tipo de Usuário';
                    foreach ($tipo_usuario as $value) {
                        $tipo[$value->id_tipo_usuario]=$value->nome_tipo;
                }?>
              <label for="id_tipo_usuario">Tipo de Usuário:</label>
             <?php echo form_dropdown('id_tipo_usuario', $tipo, set_value('id_tipo_usuario',$query['id_tipo_usuario']), 'class="form-control" disabled="disabled"') ?>
            </div>
            <button type="submit" class="btn btn-primary">Excluir Usuário</button>
        </form>
    </div>
</div>
