<div class="container">
    <div class="row">
        <div class="col-12 topo-area-adm text-center">
            <img src="<?php echo base_url("publico/imagens/area-adm/adm/banner@2x.png"); ?>">
        </div>
    </div>
    <div class="row align-items-center">
        <div class="offset-0 offset-sm-0 offset-md-2 offset-lg-2 col-4 col-sm-4 col-md-2 col-lg-2 text-center botao-area-adm">
            <a class="" href='<?php echo base_url("/adm/imoveis"); ?>' role="button">
                <label class="comportamento-botao">
                    <img src="<?php echo base_url("publico/imagens/area-adm/adm/imoveis3@2x.png"); ?>">
                    Imoveis
                </label>
            </a>
        </div>
        <div class="offset-0 offset-sm-0 offset-md-1 offset-lg-1 col-4 col-sm-4 col-md-2 col-lg-2 text-center botao-area-adm">
            <a class="" href='<?php echo base_url("/adm/clientes"); ?>' role="button">
                <label class="comportamento-botao">
                    <img src="<?php echo base_url("publico/imagens/area-adm/adm/clientes3@2x.png"); ?>">
                    Clientes
                </label>
            </a>
        </div>
        <div class="offset-0 offset-sm-0 offset-md-1 offset-lg-1 col-4 col-sm-4 col-md-2 col-lg-2 text-center botao-area-adm">
            <a class="" href='<?php echo base_url("adm/usuarios/alterar-dados/"); ?>' role="button">
                <label class="comportamento-botao">
                    <img src="<?php echo base_url("publico/imagens/area-adm/adm/meus-dados3@2x.png"); ?>">
                    Meus Dados
                </label>
            </a>
        </div>
    </div>
</div>
