<div class="container">
    <div class="row">
        <div class="col">
            <form action="<?php echo base_url('adm/adicionar/cadastrar-cidade'); ?>" method="POST" accept-charset="utf-8">
                <div class="form-group row bottom-formulario-20">
                    <!-- ********** NOME ********** -->
                    <label for="text-nome" class="col-2 col-form-label">
                        Nome <span class="texto-campo-obrigatorio">*</span>
                    </label>
                    <div class="col-10">
                        <input class="form-control" type="text" name="param_nome" 
                         value="<?php echo set_value('param_nome'); ?>" id="text-nome"
                         placeholder="Nome da cidade">
                    </div>
                </div>

                <div class="form-group row bottom-formulario-20">
                    <!-- ********** ESTADO ********** -->
                    <label for="drop-cidade" class="col-2 col-form-label">
                        Estado <span class="texto-campo-obrigatorio">*</span>
                    </label>
                    <div class="col-10">
                        <?php echo form_dropdown('param_id_estado', 
                            $estados, 
                            set_value('param_id_estado'), 
                            'class="form-control"'); 
                        ?>
                    </div>
                </div>

                <div class="form-group row texto-central">
                    <div class="col">
                        <button type="submit" href="#" 
                         title="Cadastrar cidade"
                         class="btn btn-primary btn-lg comportamento-botao">
                            Cadastrar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>