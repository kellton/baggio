<div class="container">
    <div class="row">
        <div class="col">
            <form action="<?php echo base_url('adm/adicionar/cadastrar-estado'); ?>" method="POST" accept-charset="utf-8">
                <div class="form-group row bottom-formulario-20">
                    <!-- ********** NOME ********** -->
                    <div class="col-2">
                        <label for="text-nome" class="col-2 col-form-label">
                            Nome <span class="texto-campo-obrigatorio">*</span>
                        </label>
                    </div>
                    <div class="col-10">
                        <input class="form-control" type="text" name="param_nome" 
                         value="<?php echo set_value('param_nome'); ?>" id="text-nome"
                         placeholder="Nome do estado">
                    </div>
                </div>

                <div class="form-group row bottom-formulario-20">
                    <!-- ********** SIGLA ********** -->
                    <label for="text-sigla" class="col-2 col-form-label">
                        Sigla <span class="texto-campo-obrigatorio">*</span>
                    </label>
                    <div class="col-10">
                        <input class="form-control" type="text" name="param_sigla" size="2" 
                         value="<?php echo set_value('param_sigla'); ?>" id="text-sigla"
                         placeholder="Sigla do estado">
                    </div>
                </div>

                <div class="form-group row texto-central">
                    <div class="col">
                        <button type="submit" href="#" 
                         title="Cadastrar estado"
                         class="btn btn-primary btn-lg comportamento-botao">
                            Cadastrar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>