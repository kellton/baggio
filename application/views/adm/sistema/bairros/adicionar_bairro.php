<div class="container">
    <div class="row">
        <div class="col">
            <form action="<?php echo base_url('adm/adicionar/cadastrar-bairro'); ?>" method="POST" accept-charset="utf-8">
                <div class="form-group row bottom-formulario-20">
                    <!-- ********** NOME ********** -->
                    <label for="text-nome" class="col-2 col-form-label">
                        Nome <span class="texto-campo-obrigatorio">*</span>
                    </label>
                    <div class="col-10">
                        <input class="form-control" type="text" name="param_nome" 
                         value="<?php echo set_value('param_nome'); ?>" id="text-nome"
                         placeholder="Nome do bairro">
                    </div>
                </div>

                <div class="form-group row bottom-formulario-20">
                    <!-- ********** CIDADES ********** -->
                    <label for="drop-cidade" class="col-2 col-form-label">
                        Cidade <span class="texto-campo-obrigatorio">*</span>
                    </label>
                    <div class="col-10">
                        <?php echo form_dropdown('param_id_cidade', 
                            $cidades, 
                            set_value('param_id_cidade'), 
                            'class="form-control"'); 
                        ?>
                    </div>
                </div>

                <div class="form-group row texto-central">
                    <div class="col">
                        <button type="submit" href="#" 
                         title="Cadastrar bairro"
                         class="btn btn-primary btn-lg comportamento-botao">
                            Cadastrar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>