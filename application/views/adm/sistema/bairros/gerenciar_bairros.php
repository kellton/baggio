<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container">
    <div class="row bottom-formulario-16">
        <div class="col">
            <a type='button' class='btn btn-primary comportamento-botao' 
             href='<?php echo base_url('adm/adicionar/bairro'); ?>' 
             title='Adicionar novo bairro'>
                Adicionar Novo Bairro
            </a>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="table-responsive">
                <?php if (!empty($tabela_bairros)) { echo $tabela_bairros; } ?>
            </div>
        </div>
    </div>
</div>
