<div class="container">
    <div class="row">
        <div class="col">
            <form action="<?php echo base_url('adm/adicionar/cadastrar-job'); ?>" method="POST" accept-charset="utf-8">
                <div class="row form-group">
                    <div class="col-2">
                        <label for="select-imobiliaria">Imobiliária</label>
                    </div>
                    <div class="col-6">
                        <select class="form-control" name="param_id_imobiliaria" 
                         id="label-imobiliaria">
                        <?php foreach ($imobiliarias as $imobiliaria) { ?>
                            <option value="<?php echo $imobiliaria['id_imobiliaria']; ?>">
                                <?php echo $imobiliaria["nome_imobiliaria"]; ?> 
                                (<?php echo $imobiliaria["codigo_imobiliaria"]; ?>)
                            </option>
                        <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-2">
                        <label for="number-codigo">Código</label>
                    </div>
                    <div class="col-3">
                        <input type="number" class="form-control" min="1" 
                         name="param_codigo" 
                         id="number-codigo" value="1">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-2">
                        <label for="text-horarios">Horários de atualização</label>
                    </div>
                    <div class="col-4">
                        <input type="text" class="form-control" 
                         id="text-horarios" name="param_horarios" 
                         value="01:00:00;18:00:00">
                    </div>
                </div>

                <div class="form-group row texto-central">
                    <div class="col">
                        <button type="submit" href="#" 
                         title="Cadastrar job"
                         class="btn btn-primary btn-lg comportamento-botao">
                            Cadastrar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>