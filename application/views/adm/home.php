<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container">
    <div class="row">
        <div class="col-12 topo-area-adm text-center">
            <img src="<?php echo base_url("publico/imagens/area-adm/adm/banner@2x.png"); ?>">
        </div>
    </div>
    <div class="row align-items-center">
        <div class="offset-1 offset-sm-0 offset-md-1 offset-lg-0 col-4 col-sm-4 col-md-3 col-lg-3 text-center botao-area-adm">
            <a class="" href='<?php echo base_url("/adm/usuarios"); ?>' role="button">
                <label class="comportamento-botao">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-8 col-lg-8 offset-md-2 offset-lg-2 text-center">
                            <img src="<?php echo base_url("publico/imagens/area-adm/adm/usuarios3@2x.png"); ?>">
                        </div>
                    </div>
                    Usuários
                </label>
            </a>
        </div>
        <div class="offset-2 offset-sm-0 offset-md-1 offset-lg-0 col-4 col-sm-4 col-md-3 col-lg-3 text-center botao-area-adm">
            <a class="" href='<?php echo base_url("/adm/imoveis"); ?>' role="button">
                <label class="comportamento-botao">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-8 col-lg-8 offset-md-2 offset-lg-2 text-center">
                            <img src="<?php echo base_url("publico/imagens/area-adm/adm/imoveis3@2x.png"); ?>">
                        </div>
                    </div>
                    Imoveis
                </label>
            </a>
        </div>
        <div class="offset-1 offset-sm-0 offset-md-1 offset-lg-0 col-4 col-sm-4 col-md-3 col-lg-3 text-center botao-area-adm">
            <a class="" href='<?php echo base_url("/adm/clientes"); ?>' role="button">
                <label class="comportamento-botao">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-8 col-lg-8 offset-md-2 offset-lg-2 text-center">
                            <img src="<?php echo base_url("publico/imagens/area-adm/adm/clientes3@2x.png"); ?>">
                        </div>
                    </div>
                    Clientes
                </label>
            </a>
        </div>
        <div class="offset-2 offset-sm-2 offset-md-1 offset-lg-0 col-4 col-sm-4 col-md-3 col-lg-3 text-center botao-area-adm">
            <a class="" href='<?php echo base_url("adm/gerenciar/jobs"); ?>' role="button">
                <label class="comportamento-botao">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-8 col-lg-8 offset-md-2 offset-lg-2 text-center">
                            <img src="<?php echo base_url("publico/imagens/area-adm/adm/tarefas3@2x.png"); ?>">
                        </div>
                    </div>
                    Tarefas
                </label>
            </a>
        </div>
        <div class="offset-4 offset-sm-0 offset-md-1 offset-lg-0 col-4 col-sm-4 col-md-3 col-lg-3 text-center botao-area-adm">
            <a class="" href='<?php echo base_url("adm/usuarios/alterar-dados/"); ?>' role="button">
                <label class="comportamento-botao">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-8 col-lg-8 offset-md-2 offset-lg-2 text-center">
                            <img src="<?php echo base_url("publico/imagens/area-adm/adm/meus-dados3@2x.png"); ?>">
                        </div>
                    </div>
                    Meus Dados
                </label>
            </a>
        </div>
    </div>
</div>
