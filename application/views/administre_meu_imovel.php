<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h3 class="espacamento-titulo-pagina">Administrar Meu Imóvel</h3><hr>
        <?php 
        if($this->session->flashdata('mensagem')){
            echo '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$this->session->flashdata('mensagem').'</div>';
        }?>
        <p class="text-center">A Baggio Imóveis tem a estrutura e a equipe que você precisa para alugar seu imóvel com segurança e rapidez. Preencha o cadastro com seus dados que um especialista em Avaliação Imobiliária entrará em contato com você.</p>
        <p class="text-center"><b>Todos os campos com (*) são de preenchimento obrigatório</b></p>
        <?php echo validation_errors('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>','</div>'); ?>
        <form action="<?php echo base_url('validacao-administrar-meu-imovel') ?>" method="POST" accept-charset="utf-8">
            <div class="form-group">
                <label for="nome">Nome:*</label>
                <input type="text" class="form-control" name="nome" placeholder="Nome" value="<?php echo set_value('nome')?>" required>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="email">E-mail:*</label>
                        <input type="email" class="form-control" name="email" placeholder="E-mail" value="<?php echo set_value('email')?>" required>
                    </div>
                </div>
                <div class="col"> 
                    <div class="form-group">
                        <label>Telefone:*</label>
                        <input type="text" class="form-control" name="telefone" mascara="tel" placeholder="Telefone" value="<?php echo set_value('telefone')?>" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col"> 
                    <div class="form-group">
                        <label>Endereço do Seu Imóvel:*</label>
                        <input type="text" class="form-control" name="endereco" placeholder="Endereço do Seu Imóvel" value="<?php echo set_value('endereco')?>" required>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label>Selecione o Tipo de Imóvel</label>
                        <select name="tipo_imovel" class="form-control" togvis-select>
                            <option value="" selected="selected">Selecione Um Tipo de Imóvel</option>
                            <option value="Casa">Casa</option>
                            <option value="Apartamento">Apartamento</option>
                            <option value="Comercial">Comercial</option>
                            <option value="Terreno">Terreno</option>
                            <option value="Barracão">Barracão</option>
                            <option value="Sobrado">Sobrado</option>
                            <option value="Chácara">Chácara</option>
                            <option value="Outro">Outro</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="mensagem">Valor do Aluguel Desejado:*</label>
                <input type="text" class="form-control" name="valor_aluguel" placeholder="Valor do Aluguel Desejado" value="<?php echo set_value('valor_aluguel')?>" required>
            </div>
            <div class="form-group">
                <label>Observações importantes que você tenha sobre o imóvel que queira divulgar para locação:*</label>
                <textarea rows="5" type="text" class="form-control" name="observacao" required><?php echo set_value('observacao')?></textarea>
            </div>
            <div class="row">
                <div class="col">
                    <?php echo $widget; ?>
                    <?php echo $script; ?>
                </div>
            </div>
            <hr>
            <button type="submit" class="btn btn-primary">Enviar Mensagem</button>
        </form>
    </div>
</div>
