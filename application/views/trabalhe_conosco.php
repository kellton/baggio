<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h3 class="espacamento-titulo-pagina">Trabalho Conosco</h3><hr>
        <?php  
        if($this->session->flashdata('mensagem')){
            echo '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$this->session->flashdata('mensagem').'</div>';
        }?>
        <p class="text-center">Está buscando uma oportunidade no ramo imobiliário?</p>
        <p class="text-center">Então, envie seu CV para nosso banco da dados.</p>
        <p class="text-center">Assim que surgir uma nova oportunidade entraremos em contato com você.</p>
        <p class="text-center"><b>Todos os campos com (*) são de preenchimento obrigatório</b></p>
        <?php
        if(isset($error)){
            echo '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$error.'</div>';
        }
        echo validation_errors('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>','</div>'); ?>
        <form action="<?php echo base_url('validacao-trabalhe-conosco') ?>" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
            <div class="form-group">
                <label for="nome">Nome:*</label>
                <input type="text" class="form-control" name="nome" placeholder="Nome" value="<?php echo set_value('nome')?>" required>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="email">E-mail:*</label>
                        <input type="email" class="form-control" name="email" placeholder="E-mail" value="<?php echo set_value('email')?>" required>
                    </div>
                </div>
                <div class="col"> 
                    <div class="form-group">
                        <label>Telefone/Whatsapp:*</label>
                        <input type="text" class="form-control" name="telefone" mascara="cel" placeholder="Telefone" value="<?php echo set_value('telefone')?>" required>
                    </div>
                </div>
            </div>
          
            <div class="form-group">
            <?php 
                $tipo['']='Selecione uma Área de Interesse';
                $tipo['Locações']='Locações';
                $tipo['Vendas']='Vendas';

                ?>
              <label for="interesse">Área de Interesse:*</label>
             <?php echo form_dropdown('interesse', $tipo, set_value('interesse'), 'class="form-control"') ?>
            </div>
            <div class="form-group">
                <label for="mensagem">Currículo:*</label><br>
                <input type="file" name="curriculo[]" multiple />
            </div>
            <div class="form-group">
                <label for="mensagem">Deixe Seu Recado:*</label>
                <textarea rows="5" type="text" class="form-control" name="mensagem" required><?php echo set_value('mensagem')?></textarea>
            </div>
            <div class="row">
                <div class="col">
                    <?php echo $widget; ?>
                    <?php echo $script; ?>
                </div>
            </div>
            <hr>
            <button type="submit" class="btn btn-primary">Enviar Currículo</button>
        </form>
    </div>
</div>
