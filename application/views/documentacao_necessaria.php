<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h3 class="espacamento-titulo-pagina">Documentação Necessária</h3><hr>
        <p class="text-justify">Você está alugando um imóvel com a Baggio Imóveis, então seja muito bem-vindo! Aqui você encontrará todas as informações necessárias sobre a documentação a ser levantada para os trâmites de locação de um imóvel residencial ou comercial, tanto para locatários quanto fiadores, além das opções de garantia com as quais a empresa trabalha. Este é um guia para facilitar a sua locação, ou seja, mais uma solução da Baggio Imóveis para você.</p>
        <p><b>FACILIDADES BAGGIO IMÓVEIS</b></p>
        <p class="text-justify">Você poderá enviar sua documentação digitalizada para o email do seu consultor(a) ou através da <b>área exclusiva dos clientes.</b></p>
        <p class="text-justify">Firmamos convênio com Cartórios de Registro de Imóveis do Paraná*, dessa forma, para solicitar a matrícula atualizada do imóvel do seu fiador ao invés de se dirigir ao cartório, gastar com deslocamento, e enfrentar filas, efetuando o pagamento de apenas uma taxa no ato da reserva do imóvel, já temos acesso a matrícula de forma online. Não é obrigatório efetuar esse serviço, porém, neste caso, você deverá apresentar a matricula original atualizada na retirada das chaves.</p>
        <p><i>Condições especiais para Estudantes! Consulte nossos Consultores.</i></p>
        <p class="text-justify"><small>* Consulte as Cidades disponíveis com nossos Consultores. Facilidade válida apenas para imóveis no Estado do Paraná, e mediante a consulta acima, em qualquer outra localidade, será necessário que o Pretendente providencie a matrícula atualizada. Consulte o valor com nossos Consultores.</small></p>
        <hr>
        <p><b>PRETENDE LOCAÇÃO: PESSOA FÍSICA</b></p>
        <ul>
            <li>Ficha cadastral preenchida (pelo site): <b>na área exclusiva dos clientes;</b></li>
            <li>RG e CPF do pretendente;</li>
            <li>RG e CPF do cônjuge;</li>
            <li>Comprovante de endereço atual (água, luz ou telefone);</li>
            <li>Renda superior a 3 (três) vezes o valor do aluguel (aluguel líquido + taxas);</li>
            <li>Comprovante de estado civil:</li>
            <ul>
                <li>Certidão de casamento; (quando casado sob o regime de separação total de bens, é necessário a escritura pública de pacto antenupcial;</li>
                <li>Certidão de união estável, quando aplicável;</li>
                <li>Certidão de nascimento, quando solteiro;</li>
                <li>Certidão de nascimento com averbação de emancipação, quando aplicável;</li>
            </ul>
            <li>RG e CPF(quando menor, certidão de nascimento) das pessoas que irão residir no imóvel.</li>
        </ul>
        <p><b>Atenção! Locações de imóveis comerciais acima de R$ 3.000,00 (três mil reais), necessário as certidões do(s) Pretendente(s) e do(s) Fiador(es). conforme abaixo:</b></p>
        <p><b>Protestos Concordata para falência Cível Penhoras e concordatas poderão ser solicitadas na Central de Certidões:</b></p>
        <p><b>Rua XV de Novembro, 362 – Cj 202 – Centro – Curitiba – PR – (41) 3223-8915.</b></p>
        <hr>
        <p><b>PRETENDE LOCAÇÃO: PESSOA JURÍDICA</b></p>
        <ul>
            <li>Ficha cadastral preenchida (pelo site): <b>na área exclusiva dos clientes;</b></li>
            <li>Contrato social e a última alteração;</li>
            <li>Balanços e balancetes dos 3(três) últimos anos;</li>
            <li>Recibo de entrega da ECF (Escrituração Contábil Fiscal) do último ano;</li>
            <li>Declaração de Faturamento dos últimos 12(doze) meses, assinada pelo Contador;</li>
            <li>Cartão de CNPJ;</li>
            <li>RG, CPF e Comprovante de endereço atual (água, luz ou telefone) dos sócios;</li>
            <li>Caso o(s) procurador(es) assinem o contrato de locação, anexar cópia da procuração vigente e/ou certidão da procuração, apresentar RG e CPF do(s) procurador(es);</li>
            <li>Para Empresas S.A – estatuto completo da instituição e ata da última eleição;</li>
            <li>Certidão negativa:</li>
            <ul>
                <li>FGTS:<a href="http://www.caixa.gov.br"> www.caixa.gov.br</a></li>
                <li>INSS:<a href="http://www.previdenciasocial.gov.br"> www.previdenciasocial.gov.br</a></li>
                <li>Tributos Federais e Dívida Ativa da União: <a href="http://www.receita.fazenda.gov.br">www.receita.fazenda.gov.br</a></li>
            </ul>
        </ul>
        <p><b>Não serão aceitos como Fiador(es) pessoas jurídicas e pessoas que constem no capital social da empresa em questão cuja renda provenha unicamente deste CNPJ, exceto se comprovado outra fonte de renda.</b></p>
        <p><b>Se você estiver efetuando uma locação comercial, solicite ao Consultor no momento da visita a Indicação Fiscal do imóvel para consultar se o ramo de atividade pretendido será liberado pela Prefeitura.</b></p>
        <p><b>Locações acima de R$ 3.000,00(três mil reais), necessário as certidões da Pessoa Jurídica, sócio(s) e do(s) Fiador(es) (se aplicável), conforme abaixo:</b></p>
        <p><b>Protestos; Cível; Concordata para falência; Penhoras e concordatas; Poderão ser solicitadas na Central de Certidões: Rua XV de Novembro, 362 – Cj 202 – Centro – Curitiba – PR – (41) 3223-8915.</b></p>
        <hr>
        <p><b>Como comprovar a renda de pretendente(s) e fiador(es):</b></p>
        <p><b>Assalariados</b></p>
        <ul>
            <li>3 (três) últimos contracheques;</li>
            <li>Carteira de trabalho (foto/qualificação/registro de empregador, alterações e observações);</li>
            <li>Declaração de IR com recibo de entrega (exercício anterior);</li>
            <li>Comprovar no mínimo 6 (seis) meses de carteira assinada.</li>
        </ul>
        <p><b>Empresários</b></p>
        <ul>
            <li>Contrato social / última alteração;</li>
            <li>Decore dos 3 (três) últimos meses;</li>
            <li>Declaração de IR com recibo de entrega (exercício anterior).</li>
        </ul>
        <p><b>Autônomos e Profissionais Liberais</b></p>
        <ul>
            <li>3 (três) últimos extratos bancários;</li>
            <li>Decore dos 3 (três) últimos meses;</li>
            <li>Declaração de IR com recibo de entrega (exercício anterior).</li>
        </ul>
        <p><b>Autônomos e Profissionais Liberais</b></p>
        <ul>
            <li>3 (três) últimos extratos do benefício;</li>
            <li>Declaração de IR com recibo de entrega (exercício anterior).</li>
        </ul>
        <hr>
        <p><b>TIPOS DE FIANÇA</b></p>
        <p><b>1. Fiador</b></p>
        <ul>
            <li>Ficha cadastral preenchida (pelo site):<b>na área exclusiva dos clientes;</b>;</li>
            <li>RG e CPF do fiador;</li>
            <li>RG e CPF do cônjuge;</li>
            <li>Comprovante de endereço atual (água, luz ou conta telefônica);</li>
            <li>Renda superior a 3 (três) vezes o valor do aluguel (aluguel líquido + taxas);</li>
            <li>Certidão de casamento; (quando casado sob o regime de separação total de bens, é necessário a escritura pública de pacto antenupcial);</li>
            <li>Certidão de união estável, quando aplicável;</li>
            <li>Certidão de nascimento, quando solteiro.</li>
        </ul>
        <p class="text-justify"><b>Quando se tratar de um imóvel fora da Região de Curitiba (inclusive Metropolitana), este deverá ter o valor venal de, no mínimo, R$ 200.000,00 (duzentos mil reais). A comprovação deverá ser feita através de: Carnê de IPTU ou, na matrícula atualizada do imóvel ou IR. Caso nenhum destes documentos comprovem o valor venal, é necessário uma avaliação simples de pelo menos 1 (uma) Imobiliária da região do local do imóvel.</b></p>
        <p class="text-justify"><b>Não será aceito Fiador por Procuração, a fiança através de fiador é de caráter pessoal. Deve possuir imóvel quitado na Região Sul do Brasil, livre de qualquer ônus, como: alienação, hipoteca, financiamentos, penhoras, etc. Comprovado por: Matrícula atualizada com certidão e ônus mínimo de 30 (trinta) dias (cópia), na retirada das chaves apresentar a matricula original atualizada;</b></p>
        <p class="text-justify"><b>Carnê de IPTU do ano vigente com os dados: do proprietário, valor venal do imóvel e valor do imposto.</b></p>
        <p><b>2. Seguro Fiança</b></p>
        <p>A garantia é formalizada com a contratação de uma apólice, o prêmio será pago em 12(doze) vezes mensais.</p>
        <p><b>3. Título de Capitalização</b></p>
        <p>O título ficará vinculado ao contrato de locação até a rescisão, resgatado no prazo com juros da TR. O valor do título será de no mínimo 10 (dez) vezes o valor do aluguel bruto mais taxas.</p>
        <p><b>4. Carta Fiança</b></p>
        <p>Será analisada carta fiança de bancos e empresas privadas. Consulte-nos para obter maiores informações.</p>
        <p><b>5. Caução de Imóvel</b></p>
        <p class="text-justify">Consiste em caucionar um imóvel em garantia das obrigações contratadas por meio da averbação no Registro de Imóveis, sendo que todas as despesas correrão por conta do Locatário. O imóvel caucionado deverá localizar-se na cidade de Curitiba e RMC. Somente serão aceitos caucionantes Pessoas Físicas. (Depende de Análise).</p>
        <p class="text-justify"> Devido exigências de Cartório, nesta modalidade os documentos não poderão ser enviados através do site, e  sim terão que ser entregues ao Consultor, consulte a relação de documentos no momento da reserva.</p>
        <p class="text-justify"><b>Atenção! Consulte nossos Consultores para que possamos avaliar se o imóvel que você pretende locar se encaixa neste tipo de fiança.</b></p>
        <hr>
        <p><b>FIQUE ALERTA! INFORMAÇÕES IMPORTANTES</b></p>
        <ul>
            <li class="text-justify">  A assinatura do Cônjuge do Fiador é indispensável no Contrato de Locação, no Relatório de Vistoria e nos Termos Aditivos, se houver.</li>
            <li class="text-justify">Não serão aceitos fiadores Pessoa Jurídica, salvo exceção em que conste expressamente no Contrato Social a possibilidade da empresa prestar fiança locatícia (neste caso documentação a ser apresentada é a mesma do pretendente pessoa jurídica, acrescida da matrícula atualizada do imóvel em nome da pessoa jurídica).</li>
            <li class="text-justify">Não serão aceitos PRETENDENTE(S) ou FIADOR(ES) com registro em quaisquer Órgãos de Proteção ao Crédito, como SPC, SERASA, CARTÓRIOS DE PROTESTO, etc.</li>
            <li class="text-justify">Todas as fichas devem ser preenchidas corretamente.</li>
            <li class="text-justify">A documentação solicitada pode ser encaminhada através do site. Não serão aceitos documentos: borrados, falhados, cortados, incompletos e rasurados.</li>
            <li class="text-justify">Após a análise dos documentos apresentados, poderão ser solicitados documentos adicionais e/ou originais para verificação de autenticidade.</li>
            <li class="text-justify">A falsificação ou adulteração de qualquer documento solicitado inválida o cadastro do Locatário.</li>
            <li class="text-justify">Pretendente(s) e Fiador(es) estarão sujeitos à análise do crédito e aprovação.</li>
        </ul>
    </div>
</div>
