<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="banner-background-adm d-print-none">
    <img width="100%" src="<?php echo base_url('publico/imagens/area-adm/cliente/banner@2x.png'); ?>">
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="titulo-sessao-ficha espacamento-titulo-pagina">
                <div class="container">
                    <h3 class="display-6">Informações Básicas do Pretendente</h3>
                </div>
            </div>
            
<!------------------------------------------------------------------------------------------------------------------------>

            <hr>
            <div class="row">
                <div class="col">
                    <p><b>Nome Completo:</b> <?php echo $query['nome_completo'] ?></p>
                </div>
                <div class="col">
                    <p><b>Telefone Fixo:</b> <?php echo $query['telefone'] ?></p>
                </div>
                <div class="col">
                    <p><b>Telefone Celular:</b> <?php echo $query['celular'] ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <p><b>E-mail do Pretendente:</b> <?php echo $query['email_pretendente'] ?></p>
                </div>
                <div class="col">
                    <p><b>E-mail do Morador:</b> <?php echo $query['email_morador'] ?></p>
                </div>
                <div class="col">
                    <p><b>Telefone Fixo:</b> <?php echo $query['telefone'] ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <p><b>Data de Nascimento:</b> <?php echo formatar_data($query['data_nascimento']) ?></p>
                </div>
                <div class="col">
                    <p><b>Nacionalidade:</b> <?php echo $query['nacionalidade'] ?></p>
                </div>
                <div class="col">
                    <p><b>Estado Civil:</b> <?php echo $query['nome_estado_civil'] ?></p>
                </div>
            </div>
            
<!------------------------------------------------------------------------------------------------------------------------>

            <hr>
            <div class="titulo-sessao-ficha">
                <div class="container">
                    <h3 class="display-6">Documentos</h3>
                </div>
            </div>
            
<!------------------------------------------------------------------------------------------------------------------------>

            <hr>
            <div class="row">
                <div class="col">
                    <p><b>CPF:</b> <?php echo $query['cpf'] ?></p>
                </div>
                <div class="col">
                    <p><b>RG:</b> <?php echo $query['rg'] ?></p>
                </div>
                <div class="col">
                    <p><b>Local de Expedição:</b> <?php echo $query['expedicao'] ?></p>
                </div>
                <div class="col">
                    <p><b>Data da Expedição do RG:</b> <?php echo formatar_data($query['data_expedicao']) ?></p>
                </div>
            </div>
            
<!------------------------------------------------------------------------------------------------------------------------>

            <hr>
            <div class="titulo-sessao-ficha">
                <div class="container">
                    <h3 class="display-6">Informações Profissionais</h3>
                </div>
            </div>
            
<!------------------------------------------------------------------------------------------------------------------------>

            <hr>
            <div class="row">
                <div class="col-4">
                    <p><b>Profissão:</b> <?php echo $query['profissao'] ?></p>
                </div>
                <div class="col-4">
                    <p><b>Aposentado?</b> <?php if($query['aposentado']==1){ echo 'Sim';}else{echo 'Não';} ?></p>
                </div>
            </div>
            <?php if($query['aposentado']==0){?>
                <div class="row">
                    <div class="col">
                        <p><b>Nome da Empresa que Trabalha:</b> <?php echo $query['nome_empresa'] ?></p>
                    </div>
                    <div class="col">
                        <p><b>Telefone Comercial:</b> <?php echo $query['telefone_empresa'] ?></p>
                    </div>
                    <div class="col">
                        <p><b>E-mail Comercial:</b> <?php echo $query['email_comercial'] ?></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <p><b>Data de Admissão:</b> <?php echo formatar_data($query['data_admissao']) ?></p>
                    </div>
                    <div class="col-4">
                        <p><b>Tempo de Empresa:</b> <?php echo $query['tempo_empresa'] ?></p>
                    </div>
                </div>
                
    <!------------------------------------------------------------------------------------------------------------------------>

                <hr>
                <div class="row">
                    <div class="col">
                        <p><b>CEP:</b> <?php echo $query['cep'] ?></p>
                    </div>
                    <div class="col">
                        <p><b>Logradouro:</b> <?php echo $query['logradouro'] ?></p>
                    </div>
                    <div class="col">
                        <p><b>Número:</b> <?php echo $query['numero'] ?></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <p><b>Complemento:</b> <?php echo $query['complemento'] ?></p>
                    </div>
                    <div class="col-4">
                        <p><b>Bairro:</b> <?php echo $query['bairro'] ?></p>
                    </div>
                    <div class="col-4">
                        <p><b>País:</b> <?php echo $query['nome_pais'] ?></p>
                    </div>
                </div>
            <?php }?>
            <div class="row">
                <div class="col-4">
                    <p><b>Salário do Pretendente:</b> <?php echo $query['salario'] ?></p>
                </div>
                <div class="col-4">
                    <p><b>Total da Renda:</b> <?php echo $query['salario'] ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <p class="text-justify"><b>Outras Rendas:</b> <?php echo $query['total_renda'] ?></p>
                </div>
            </div>
            
<!------------------------------------------------------------------------------------------------------------------------>

            <hr>
            <div class="titulo-sessao-ficha">
                <div class="container">
                    <h3 class="display-6">Endereço Principal do Pretendente</h3>
                </div>
            </div>
            
<!------------------------------------------------------------------------------------------------------------------------>

            <hr>
            <?php 
            foreach ($enderecos as $value) {
                if($value->principal==1){ ?>
                <div class="row">
                    <div class="col">
                        <p><b>CEP:</b> <?php echo $value->cep_cliente ?></p>
                    </div>
                    <div class="col">
                        <p><b>Logradouro:</b> <?php echo $value->logradouro_cliente ?></p>
                    </div>
                    <div class="col">
                        <p><b>Número:</b> <?php echo $value->numero_cliente ?></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <p><b>Bairro:</b> <?php echo $value->bairro_cliente ?></p>
                    </div>
                    <?php if ($value->complemento_cliente!=''){?>
                        <div class="col-4">
                            <p><b>Complemento:</b> <?php echo $value->complemento_cliente ?></p>
                        </div>
                    <?php }?>
                    <div class="col-4">
                        <p><b>País:</b> <?php echo $value->nome_pais ?></p>
                    </div>
                </div>
            <?php }} ?>
            
<!------------------------------------------------------------------------------------------------------------------------>

            <hr>
            <div class="titulo-sessao-ficha">
                <div class="container">
                    <h3 class="display-6">Dados Adicionais do Pretendente</h3>
                </div>
            </div>
            
<!------------------------------------------------------------------------------------------------------------------------>

            <hr>
            <div class="row">
                <div class="col-4">
                    <p><b>Tempo na Residência Atual:</b> <?php echo $query['tempo_residencia'] ?></p>
                </div>
                <div class="col-4">
                    <p><b>Residência Própria?</b> <?php if($query['residencia_propria']==1){ echo 'Sim';}else{echo 'Não';} ?></p>
                </div>
                <?php if($query['residencia_propria']==0){?>
                    <div class="col-4">
                        <p><b>Valor do Aluguel Atual:</b> <?php echo $query['aluguel_atual'] ?></p>
                    </div>
                <?php }?>
            </div>
            <div class="row">
                <div class="col">
                    <p class="text-justify"><b>Motivo da Mundaça:</b> <?php echo $query['motivo_mudanca'] ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <p><b>Nome do Locador ou Imobiliária Atual:</b> <?php echo $query['imobiliaria_atual'] ?></p>
                </div>
                <div class="col-6">
                    <p><b>Telefone do Locador ou Imobiliária Atual:</b> <?php echo $query['telefone_imobiliaria_atual'] ?></p>
                </div>
            </div>
            
<!------------------------------------------------------------------------------------------------------------------------>

            <hr>
            <div class="titulo-sessao-ficha">
                <div class="container">
                    <h3 class="display-6">Pessoas que Irão Residir no Imóvel</h3>
                </div>
            </div>
            
<!------------------------------------------------------------------------------------------------------------------------>

            <hr>
            <?php foreach ($residentes as $value) { ?>
                <div class="row">
                    <div class="col">
                        <p><b>Nome:</b> <?php echo $value->nome_completo_residente ?></p>
                    </div>
                    <div class="col">
                        <p><b>Parentesco:</b> <?php echo $value->parentesco_residente ?></p>
                    </div>
                    <div class="col">
                        <p><b>Telefone:</b> <?php echo $value->telefone_residente ?></p>
                    </div>
                </div>
            <?php } ?>
            
<!------------------------------------------------------------------------------------------------------------------------>

            <hr>
            <div class="titulo-sessao-ficha">
                <div class="container">
                    <h3 class="display-6">Propriedades</h3>
                </div>
            </div>
            
<!------------------------------------------------------------------------------------------------------------------------>

            <hr>
             <?php foreach ($propriedades as $value) { ?>
                <div class="row">
                    <div class="col">
                        <p><b>Nº da Matícula do Imóvel:</b> <?php echo $value->matricula_imovel ?></p>
                    </div>
                    <div class="col">
                        <p><b>Circunscrição:</b> <?php echo $value->circunscricao ?></p>
                    </div>
                    <div class="col">
                        <p><b>Marca Do Veículo:</b> <?php echo $value->veiculo_marca ?></p>
                    </div>
                    <div class="col">
                        <p><b>Renavam:</b> <?php echo $value->renavam ?></p>
                    </div>
                </div>
            <?php } ?>
            
<!------------------------------------------------------------------------------------------------------------------------>

            <hr>
            <div class="titulo-sessao-ficha">
                <div class="container">
                    <h3 class="display-6">Referências Pessoais</h3>
                </div>
            </div>
            
<!------------------------------------------------------------------------------------------------------------------------>

            <hr>
             <?php foreach ($referencias as $value) { ?>
                <div class="row">
                    <div class="col">
                        <p><b>Nome:</b> <?php echo $value->nome_referencia ?></p>
                    </div>
                    <div class="col">
                        <p><b>Endereço:</b> <?php echo $value->endereco_referencia ?></p>
                    </div>
                    <div class="col">
                        <p><b>Telefone:</b> <?php echo $value->telefone_referencia ?></p>
                    </div>
                </div>
            <?php } ?>
            
<!------------------------------------------------------------------------------------------------------------------------>

            <hr>
            <div class="titulo-sessao-ficha">
                <div class="container">
                    <h3 class="display-6">Referências Bancárias</h3>
                </div>
            </div>
            
<!------------------------------------------------------------------------------------------------------------------------>

            <hr>
             <?php foreach ($referencias_bancarias as $value) { ?>
                <div class="row">
                    <div class="col">
                        <p><b>Banco:</b> <?php echo $value->banco_referencia ?></p>
                    </div>
                    <div class="col">
                        <p><b>Telefone da Agencia:</b> <?php echo $value->telefone_banco_referencia ?></p>
                    </div>
                    <div class="col">
                        <p><b>Nome da Agência:</b> <?php echo $value->nome_agencia_referencia ?></p>
                    </div>
                    <div class="col">
                        <p><b>Número da COnta:</b> <?php echo $value->numero_conta_referencia ?></p>
                    </div>
                </div>
            <?php } ?>
            
<!------------------------------------------------------------------------------------------------------------------------>

            <hr>
            <div class="titulo-sessao-ficha">
                <div class="container">
                    <h3 class="display-6">Dados da Mãe</h3>
                </div>
            </div>
            
<!------------------------------------------------------------------------------------------------------------------------>

            <hr>
            <?php foreach ($pais as $value) { 
                if($value->mae==1){ ?>
                    <div class="row">
                        <div class="col">
                            <p><b>Nome da Mãe:</b> <?php echo $value->nome ?></p>
                        </div>
                        <div class="col">
                            <p><b>Telefone:</b> <?php echo $value->telefone ?></p>
                        </div>
                        <div class="col">
                            <p><b>CEP:</b> <?php echo $value->cep ?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <p><b>Logradouro:</b> <?php echo $value->logradouro ?></p>
                        </div>
                        <div class="col">
                            <p><b>Número:</b> <?php echo $value->numero ?></p>
                        </div>
                        <div class="col">
                            <p><b>Bairro:</b> <?php echo $value->bairro ?></p>
                        </div>
                    </div>
                    <div class="row">
                        <?php if($value->complemento!=''){?>
                        <div class="col-4">
                            <p><b>Complemento:</b> <?php echo $value->complemento ?></p>
                        </div>
                        <?php }?>
                        <div class="col-4">
                            <p><b>País:</b> <?php echo $value->nome_pais ?></p>
                        </div>
                    </div>
            <?php }} ?>
            
<!------------------------------------------------------------------------------------------------------------------------>

            <hr>
            <div class="titulo-sessao-ficha">
                <div class="container">
                    <h3 class="display-6">Dados do Pai</h3>
                </div>
            </div>
            
<!------------------------------------------------------------------------------------------------------------------------>

            <hr>
            <?php foreach ($pais as $value) { 
                if($value->mae==0){ ?>
                    <div class="row">
                        <div class="col">
                            <p><b>Nome da Mãe:</b> <?php echo $value->nome ?></p>
                        </div>
                        <div class="col">
                            <p><b>Telefone:</b> <?php echo $value->telefone ?></p>
                        </div>
                        <div class="col">
                            <p><b>CEP:</b> <?php echo $value->cep ?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <p><b>Logradouro:</b> <?php echo $value->logradouro ?></p>
                        </div>
                        <div class="col">
                            <p><b>Número:</b> <?php echo $value->numero ?></p>
                        </div>
                        <div class="col">
                            <p><b>Bairro:</b> <?php echo $value->bairro ?></p>
                        </div>
                    </div>
                    <div class="row">
                        <?php if($value->complemento!=''){?>
                        <div class="col-4">
                            <p><b>Complemento:</b> <?php echo $value->complemento ?></p>
                        </div>
                        <?php }?>
                        <div class="col-4">
                            <p><b>País:</b> <?php echo $value->nome_pais ?></p>
                        </div>
                    </div>
            <?php }} ?>
            <?php if($query['id_estado_civil']==2){?>
                
<!------------------------------------------------------------------------------------------------------------------------>

                <hr>
                <div class="titulo-sessao-ficha">
                    <div class="container">
                        <h3 class="display-6">Dados do Cônjuge</h3>
                    </div>
                </div>
                
    <!------------------------------------------------------------------------------------------------------------------------>

                <hr>
                <div class="row">
                <div class="col">
                    <p><b>Nome do Cônjugue:</b> <?php echo $query['nome_completo_conjugue'] ?></p>
                </div>
                <div class="col">
                    <p><b>Telefone Celular:</b> <?php echo $query['celular_conjugue'] ?></p>
                </div>
                <div class="col">
                    <p><b>E-mail:</b> <?php echo $query['email_conjugue'] ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <p><b>Data de Nascimento:</b> <?php echo formatar_data($query['data_nascimento_conjugue']) ?></p>
                </div>
                <div class="col">
                    <p><b>Nacionalidade:</b> <?php echo $query['nacionalidade_conjugue'] ?></p>
                </div>
                <div class="col">
                    <p><b>CPF:</b> <?php echo $query['cpf_conjugue'] ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <p><b>RG:</b> <?php echo $query['rg_conjugue'] ?></p>
                </div>
                <div class="col">
                    <p><b>Local de Expedição:</b> <?php echo $query['expedicao_conjugue'] ?></p>
                </div>
                <div class="col">
                    <p><b>Data da Expedição do RG:</b> <?php echo formatar_data($query['data_expedicao_conjugue']) ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    <p><b>Profissão:</b> <?php echo $query['profissao_conjugue'] ?></p>
                </div>
                <div class="col-4">
                    <p><b>Aposentado?</b> <?php if($query['aposentado_conjugue']==1){ echo 'Sim';}else{echo 'Não';} ?></p>
                </div>
            </div>
            <?php if($query['aposentado_conjugue']==0){?>
                
<!------------------------------------------------------------------------------------------------------------------------>

                <hr>
                <div class="row">
                    <div class="col">
                        <p><b>Nome da Empresa que Trabalha:</b> <?php echo $query['nome_empresa_conjugue'] ?></p>
                    </div>
                    <div class="col">
                        <p><b>Telefone Comercial:</b> <?php echo $query['telefone_empresa_conjugue'] ?></p>
                    </div>
                    <div class="col">
                        <p><b>E-mail Comercial:</b> <?php echo $query['email_comercial_conjugue'] ?></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <p><b>Data de Admissão:</b> <?php echo formatar_data($query['data_admissao_conjugue']) ?></p>
                    </div>
                    <div class="col-4">
                        <p><b>Tempo de Empresa:</b> <?php echo $query['tempo_empresa_conjugue'] ?></p>
                    </div>
                </div>
            <?php }?>
                <div class="row">
                    <div class="col">
                        <p><b>Salário:</b> <?php echo $query['salario_conjugue'] ?></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <p class="text-justify"><b>Outras Rendas:</b> <?php echo $query['outras_rendas_conjugue'] ?></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <p class="text-justify"><b>Total da Renda:</b> <?php echo $query['total_renda_conjugue'] ?></p>
                    </div>
                </div>
            <?php }?>
            
<!------------------------------------------------------------------------------------------------------------------------>

            <hr>
            <div class="titulo-sessao-ficha">
                <div class="container">
                    <h3 class="display-6">Informações Complementares</h3>
                </div>
            </div>
            
<!------------------------------------------------------------------------------------------------------------------------>

            <hr>
            <div class="row">
                <div class="col">
                    <p class="text-justify"> <b>Informações Complementares:</b> <?php echo $query['observacoes_complementares'] ?></p>
                </div>
            </div>
            <a href="<?php echo base_url('cli/ficha-cadastro');?>" class="btn btn-primary d-print-none">Voltar</a>
        </div>
    </div>
</div>