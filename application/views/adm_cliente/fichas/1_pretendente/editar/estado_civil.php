                    <div id="menu1" class="container tab-pane"><br>

<!---------------------------------------------------------------------------------------------------------------->

                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                <div aviso-bloco class="form-group">
                                    <label for="id_estado_civil">Estado Civil:*</label>
                                    <select name="id_estado_civil" class="form-control"  togvis-select required>
                                        <option togvis-esconder-grupo="select-divs" value="1" >Selecione o Estado Civil</option>
                                        <option togvis-esconder-grupo="select-divs" value="1" <?php if($query['id_estado_civil']=="1"){ echo 'selected="selected"';}?> >Não Informado</option>
                                        <option togvis-option="casado" togvis-outros="esconder" value="2" <?php if($query['id_estado_civil']=="2"){ echo 'selected="selected"';}?>>Casado(a)</option>
                                        <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="3" <?php if($query['id_estado_civil']=="3"){ echo 'selected="selected"';}?>>Solteiro(a)</option>
                                        <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="4" <?php if($query['id_estado_civil']=="4"){ echo 'selected="selected"';}?>>Divorciado(a)</option>
                                        <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="5" <?php if($query['id_estado_civil']=="5"){ echo 'selected="selected"';}?>>Viúvo(a)</option>
                                        <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="6" <?php if($query['id_estado_civil']=="6"){ echo 'selected="selected"';}?>>União Estável</option>
                                    </select>
                                    <div class="invalid-feedback">
                                        Insira o seu Estado Civil
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div togvis-id="casado" togvis-estado='hide' togvis-grupo="select-divs">

<!---------------------------------------------------------------------------------------------------------------->

                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                    <div aviso-bloco class="form-group">
                                        <label for="nome_completo_conjugue">Nome Completo:*</label>
                                        <input aviso-campo type="text" class="form-control" name="nome_completo_conjugue" id="nome_cliente" placeholder="Nome Completo" value="<?php echo set_value('nome_completo_conjugue',$query['nome_completo_conjugue'])?>">
                                        <small aviso-texto class="form-text text-muted">Nome Completo sem Abreviações.</small>
                                        <div class="invalid-feedback">
                                            Insira o Seu Nome
                                        </div>
                                    </div>
                                </div>

                                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                    <div aviso-bloco class="form-group">
                                        <label for="celular_conjugue">Telefone Celular (Com DDD):*</label>
                                        <input aviso-campo type="text" class="form-control" name="celular_conjugue"  mascara="cel" placeholder="Celular" value="<?php echo set_value('celular_conjugue',$query['celular_conjugue'])?>">
                                        <small aviso-texto class="form-text text-muted">
                                            Em caso de Não Aplicável, colocar (00) 0000-0000
                                        </small>
                                        <div class="invalid-feedback">
                                            Insira o Telefone Celular
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                    <div aviso-bloco class="form-group">
                                        <label for="email_conjugue">E-mail:*</label>
                                        <input aviso-campo type="text" class="form-control" name="email_conjugue" placeholder="E-mail do Conjugue" value="<?php echo set_value('email_conjugue',$query['email_conjugue'])?>">
                                        <small aviso-texto class="form-text text-muted">
                                              Digite o e-mail que você visualiza com maior frequência.
                                              Este e-mail receberá os boletos de aluguel.
                                        </small>
                                        <div class="invalid-feedback">
                                            Insira o E-mail
                                        </div>
                                    </div>
                                </div>
                            </div>

<!---------------------------------------------------------------------------------------------------------------->

                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                    <div aviso-bloco class="form-group">
                                        <label for="data_nascimento_conjugue">Data de Nascimento:*</label>
                                        <input aviso-campo type="text" class="form-control" name="data_nascimento_conjugue" mascara="data" placeholder="Data de Nascimento" value="<?php echo set_value('data_nascimento_conjugue', formatar_data($query['data_nascimento_conjugue']))?>">
                                        <div class="invalid-feedback">
                                            Insira a Data de Nascimento
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                    <div aviso-bloco class="form-group">
                                        <label for="nacionalidade_conjugue">Nacionalidade:*</label>
                                        <input aviso-campo type="text" class="form-control text-uppercase" name="nacionalidade_conjugue" placeholder="Nacionalidade" value="<?php echo set_value('nacionalidade_conjugue',$query['nacionalidade_conjugue'])?>">
                                        <div class="invalid-feedback">
                                            Insira a sua Nacionalidade
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                    <div aviso-bloco class="form-group">
                                        <label for="cpf_conjugue">CPF:*</label>
                                        <input aviso-campo type="text" class="form-control" name="cpf_conjugue" mascara="cpf" placeholder="CPF" value="<?php echo set_value('cpf_conjugue',$query['cpf_conjugue'])?>">
                                        <small aviso-texto class="form-text text-muted">
                                            CPF do pretendente. Caso seja estrangeiro, converse com
                                            o consultor ou consultura que está lhe atendendo.
                                        </small>
                                        <div class="invalid-feedback">
                                            Insira o seu CPF
                                        </div>
                                    </div>
                                </div>
                            </div>

<!---------------------------------------------------------------------------------------------------------------->

                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                    <div aviso-bloco class="form-group">
                                      <label for="rg_conjugue">RG:*</label>
                                      <input aviso-campo type="text" class="form-control" name="rg_conjugue" placeholder="RG" value="<?php echo set_value('rg_conjugue',$query['rg_conjugue'])?>">
                                        <small aviso-texto class="form-text text-muted">
                                            RG do pretendente. Caso seja estrangeiro, converse com
                                            o consultor ou consultura que está lhe atendendo.
                                        </small>
                                        <div class="invalid-feedback">
                                            Insira o seu RG
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                    <div aviso-bloco class="form-group">
                                        <label for="expedicao_conjugue">Local de Expedição:*</label>
                                        <input aviso-campo type="text" class="form-control" name="expedicao_conjugue" placeholder="Local de Expedição" value="<?php echo set_value('expedicao_conjugue',$query['expedicao_conjugue'])?>">
                                        <div class="invalid-feedback">
                                            Insira o Local de Expedição
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                    <div aviso-bloco class="form-group">
                                        <label for="data_expedicao_conjugue">Data da Expedição do RG:*</label>
                                        <input aviso-campo type="text" class="form-control" name="data_expedicao_conjugue" mascara="data" placeholder="Data da Expedição do RG" value="<?php echo set_value('data_expedicao_conjugue', formatar_data($query['data_expedicao_conjugue']))?>">
                                        <div class="invalid-feedback">
                                            Insira a Data da Expedição do RG
                                        </div>
                                    </div>
                                </div>
                            </div>

<!---------------------------------------------------------------------------------------------------------------->

                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                    <div aviso-bloco class="form-group">
                                      <label for="profissao_conjugue">Profissão:*</label>
                                      <input aviso-campo type="text" class="form-control text-uppercase" name="profissao_conjugue" placeholder="Profissão" value="<?php echo set_value('profissao_conjugue',$query['profissao_conjugue'])?>">
                                        <div class="invalid-feedback">
                                            Insira a seu Profissão
                                        </div>
                                    </div>
                                </div>
                            </div>

<!-------------------------------------------------------------------------------------------------------------------->

                        <hr>
                        
<!-------------------------------------------------------------------------------------------------------------------->

                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                    <label>Aposentado(a) ou do Lar?</label><br>
                                    <div class="custom-controls-stacked d-block my-3">
                                        <label class="custom-control custom-radio">
                                            <input id="radioStacked1" name="aposentado_conjugue" value="1" togvis-button="aposentado-conjugue" type="radio" <?php if($query['aposentado_conjugue']=="1") echo 'checked';?> >
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">Sim</span>
                                        </label>
                                        <label class="custom-control custom-radio">
                                            <input id="radioStacked2" name="aposentado_conjugue" value="0" togvis-button="nao-aposentado-conjugue" type="radio" <?php if($query['aposentado_conjugue']=="0") echo 'checked';?>>
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">Não</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            
                            <div togvis-id="nao-aposentado-conjugue" togvis-estado='hide' togvis-outros='esconder' togvis-grupo='grupo-aposentado-conjugue'>
                                
<!---------------------------------------------------------------------------------------------------------------->

                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                        <div aviso-bloco class="form-group">
                                          <label for="nome_empresa_conjugue">Nome da Empresa:*</label>
                                          <input aviso-campo type="text" class="form-control" name="nome_empresa_conjugue" placeholder="Nome da Empresa que Trabalha" value="<?php echo set_value('nome_empresa_conjugue',$query['nome_empresa_conjugue'])?>">
                                        </div>
                                    </div>

                                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                        <div aviso-bloco class="form-group">
                                            <label for="telefone_empresa_conjugue">Telefone Comercial:*</label>
                                            <input aviso-campo type="text" class="form-control" name="telefone_empresa_conjugue" mascara="tel" placeholder="Telefone Comercial (com DDD)" value="<?php echo set_value('telefone_empresa_conjugue',$query['telefone_empresa_conjugue'])?>">
                                            <small aviso-texto class="form-text text-muted">
                                                Com DDD.
                                            </small>
                                            <small aviso-texto class="form-text text-muted">
                                                Em caso de Não Aplicável, colocar (00) 0000-0000.
                                            </small>
                                        </div>
                                    </div>

                                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                        <div aviso-bloco class="form-group">
                                            <label for="email_comercial_conjugue">E-mail Comercial:*</label>
                                            <input aviso-campo type="text" class="form-control" name="email_comercial_conjugue" placeholder="E-mail Comercial" value="<?php echo set_value('email_comercial_conjugue',$query['email_comercial_conjugue'])?>">
                                            <small aviso-texto class="form-text text-muted">Caso não possua, escreva 'não aplicável'.</small>
                                        </div>
                                    </div>
                                </div>

<!---------------------------------------------------------------------------------------------------------------->

                                <div class="row">
                                    
                                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                        <div aviso-bloco class="form-group">
                                          <label for="data_admissao_conjugue">Data de Admissão:*</label>
                                          <input aviso-campo type="text" class="form-control" name="data_admissao_conjugue" mascara="data" placeholder="Data de Admissão" value="<?php echo set_value('data_admissao_conjugue', formatar_data($query['data_admissao_conjugue']))?>">
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                        <div aviso-bloco class="form-group">
                                          <label for="tempo_empresa_conjugue">Tempo de Empresa:*</label>
                                          <input aviso-campo type="text" class="form-control" name="tempo_empresa_conjugue" placeholder="Tempo de Empresa" value="<?php echo set_value('tempo_empresa_conjugue',$query['tempo_empresa_conjugue'])?>">
                                        </div>
                                    </div>
                                </div>

<!-------------------------------------------------------------------------------------------------------------------->

                        <hr>
                        
<!-------------------------------------------------------------------------------------------------------------------->

                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                        <div aviso-bloco class="form-group">
                                            <label for="salario_conjugue">Salário (em R$):*</label>
                                            <input aviso-campo class="form-control" type="text" value="<?php echo set_value('salario_conjugue',$query['salario_conjugue']); ?>" name="salario_conjugue" placeholder="Salário">
                                            <div class="invalid-feedback">
                                                Insira o Seu Salário
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                        <div aviso-bloco class="form-group">
                                            <label for="outras_rendas_conjugue">Outras Rendas (especificar):*</label>
                                            <textarea class="form-control" type="text" rows="5" name="outras_rendas_conjugue" placeholder="Outras Rendas"><?php echo set_value('outras_rendas_conjugue',$query['outras_rendas_conjugue']); ?></textarea>
                                            <small aviso-texto class="form-text text-muted">
                                                  Em caso de dúvidas, converse com o consultor que está lhe atendendo.
                                                  Caso não possua outras rendas escreva: 'não aplicável'
                                            </small>
                                            <div class="invalid-feedback">
                                                Insira Suas Outras Rendas
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                        <div aviso-bloco class="form-group">
                                            <label for="total_renda_conjugue">Total da Renda (em R$):*</label>
                                            <input aviso-campo class="form-control" type="text" value="<?php echo set_value('total_renda_conjugue',$query['total_renda_conjugue']); ?>" name="total_renda_conjugue" placeholder="Total da Renda">
                                            <div class="invalid-feedback">
                                                Insira o Total de Sua Renda
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div togvis-id="aposentado-conjugue" togvis-estado='hide' togvis-outros='esconder' togvis-grupo='grupo-aposentado-conjugue'></div>
                        </div>
                    </div>