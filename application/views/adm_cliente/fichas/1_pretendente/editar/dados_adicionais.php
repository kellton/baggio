            <div id="menu0" class="container tab-pane fade"><br>
                
<!------------------------------------------------------------------------------------------------------------------>

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="row">
                            <div class="col"> 
                                <div aviso-bloco class="form-group">
                                    <label for="cep_cliente">CEP:*</label>
                                    <input aviso-campo cep-bloco="1_pretendente" class="form-control" type="text" mascara="cep" name="cep_cliente" value="<?php echo set_value('cep_cliente',$endereco_principal['cep_cliente']); ?>" placeholder="CEP" >
                                    <div class="invalid-feedback">
                                        Insira o CEP
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="logradouro_cliente">Logradouro:*</label>
                            <input aviso-campo cep-rua="1_pretendente" class="form-control text-uppercase" type="text" value="<?php echo set_value('logradouro_cliente',$endereco_principal['logradouro_cliente']); ?>" name="logradouro_cliente" placeholder="Logradouro" >
                            <small aviso-texto class="form-text text-muted">Rua, Avenida, Estrada, Travessa, etc.</small>
                            
                            <div class="invalid-feedback">
                                Insira o Logradouro
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="numero_cliente">Número:*</label>
                            <input aviso-campo class="form-control" type="text" value="<?php echo set_value('numero_cliente',$endereco_principal['numero_cliente']); ?>" name="numero_cliente" placeholder="Número" >
                            <div class="invalid-feedback">
                                Insira o Número
                            </div>
                        </div>
                    </div>
                </div>

<!------------------------------------------------------------------------------------------------------------------>

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="complemento_cliente">Complemento:*</label>
                            <input aviso-campo class="form-control text-uppercase" type="text" value="<?php echo set_value('complemento_cliente',$endereco_principal['complemento_cliente']); ?>" name="complemento_cliente" placeholder="Complemento">
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="bairro_cliente">Bairro:*</label>
                            <input aviso-campo cep-bairro="1_pretendente" class="form-control text-uppercase" type="text" value="<?php echo set_value('bairro_cliente',$endereco_principal['bairro_cliente']); ?>" name="bairro_cliente" placeholder="Bairro" >
                            <div class="invalid-feedback">
                                Insira o Bairro
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="id_pais_cliente">País:*</label>
                            <?php 
                                $array_pais['']='Selecione o País';
                                foreach ($paises as $value) {
                                    $array_pais[$value->id_pais]=$value->nome_pais;
                                }
                            ?>
                            <?php echo form_dropdown('id_pais_cliente', $array_pais, set_value('id_pais_cliente',33), 'class="form-control" ') ?>
                            <div class="invalid-feedback">
                                Selecione um País
                            </div>
                        </div>
                    </div>
                </div>

<!------------------------------------------------------------------------------------------------------------------>

                <div class="row">
                	<div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="complemento_cliente">Cidade:*</label>
                            <input aviso-campo cep-cidade="1_pretendente" class="form-control text-uppercase" type="text" value="" name="" placeholder="Cidade">
                        </div>
                        <div class="invalid-feedback">
                            Insira a Cidade
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="bairro_cliente">Estado:*</label>
                            <input aviso-campo cep-uf="1_pretendente" class="form-control text-uppercase" type="text" value="" name="" placeholder="Estado" >
                            <div class="invalid-feedback">
                                Insira o Estado
                            </div>
                        </div>
                    </div>
                
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="tempo_residencia">Tempo na Residência Atual:*</label>
                            <input aviso-campo class="form-control" type="text" value="<?php echo set_value('tempo_residencia',$query['tempo_residencia']); ?>" name="tempo_residencia" placeholder="Tempo na Residência Atual" >
                            <div class="invalid-feedback">
                                Insira o Tempo na Residência Atual
                            </div>
                        </div>
                    </div> 
                </div>


<!-------------------------------------------------------------------------------------------------------------------->

                        <hr>
                        
<!-------------------------------------------------------------------------------------------------------------------->


                <div class="row">
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <label>Residência Própria?</label><br>
                        <div class="custom-controls-stacked d-block my-3">
                            <label class="custom-control custom-radio">
                                <input aviso-campo id="radioStacked1" name="residencia_propria" value="1" togvis-button="residencia-propria" type="radio" <?php if($query['residencia_propria']==='1') echo "checked"?> >
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description">Sim</span>
                            </label>
                            <label class="custom-control custom-radio">
                                <input aviso-campo id="radioStacked2" name="residencia_propria" value="0" togvis-button="nao-residencia-propria" type="radio" <?php if($query['residencia_propria']==='0') echo "checked"?> >
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description">Não</span>
                            </label>
                        </div>
                    </div>
                </div>

<!------------------------------------------------------------------------------------------------------------------>

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                    <div togvis-id="nao-residencia-propria" togvis-estado='hide' togvis-outros='esconder' togvis-grupo="grupo-residencia">
                            <div aviso-bloco class="form-group">
                                <label for="aluguel_atual">Valor do aluguel atual (em R$):*</label>
                                <input aviso-campo class="form-control" type="text" value="<?php echo set_value('aluguel_atual',$query['aluguel_atual']); ?>" name="aluguel_atual" placeholder="Valor do aluguel atual (em R$)">
                                <small aviso-texto class="form-text text-muted">Caso não possua, escrever 'não aplicável'</small>
                            </div>
                        </div>
                    </div>
                    <div togvis-id="residencia-propria" togvis-estado='hide' togvis-outros='esconder' togvis-grupo="grupo-residencia"></div>
                </div>


<!-------------------------------------------------------------------------------------------------------------------->

                        <hr>
                        
<!-------------------------------------------------------------------------------------------------------------------->

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="motivo_mudanca">Motivo da Mudança:*</label>
                            <input aviso-campo class="form-control" type="text" value="<?php echo set_value('motivo_mudanca',$query['motivo_mudanca']); ?>" name="motivo_mudanca" placeholder="Motivo da Mudança" >
                            <div class="invalid-feedback">
                                Insira o Motivo da Mudança
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="imobiliaria_atual">Nome do Locador ou Imobiliária Atual:*</label>
                            <input aviso-campo class="form-control" type="text" value="<?php echo set_value('imobiliaria_atual',$query['imobiliaria_atual']); ?>" name="imobiliaria_atual" placeholder="Nome do Locador ou Imobiliária Atual" >
                            <small aviso-texto class="form-text text-muted">Caso não possua, escrever 'não aplicável'</small>
                            <div class="invalid-feedback">
                                Insira o Nome do Locador ou Imobiliária Atual
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="telefone_imobiliaria_atual">Telefone do Locador ou Imobiliária Atual:*</label>
                            <input aviso-campo mascara="cel" class="form-control" type="text" value="<?php echo set_value('telefone_imobiliaria_atual',$query['imobiliaria_atual']); ?>" name="telefone_imobiliaria_atual" placeholder="Telefone do Locador ou Imobiliária Atual" >
                            <small aviso-texto class="form-text text-muted">
                                Em caso de Não Aplicável, colocar (00) 0000-0000
                            </small>
                            <div class="invalid-feedback">
                                Telefone do Locador ou Imobiliária Atual
                            </div>
                        </div>
                    </div>
                </div>

<!-------------------------------------------------------------------------------------------------------------------->

                        <hr>
                        
<!-------------------------------------------------------------------------------------------------------------------->

                <p><small>Pessoas que Irão Residir no Imóvel*</small></p>
                
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-11 col-lg-11"> 
                         <?php 
                         $contador=0;
                         foreach ($residentes as $value) { ?>
                        <div class="row" id="elemento-campo-chave">
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                <div aviso-bloco class="form-group">
                                    <label for="nome_completo_residente">Nome Completo:*</label>
                                    <input aviso-campo class="form-control" type="text" name="nome_completo_residente[]" placeholder="Nome Completo" value="<?php echo set_value("nome_completo_residente[$contador]",$value->nome_completo_residente); ?>" >
                                    <div class="invalid-feedback">
                                        Insira o Nome Completo do Residente
                                    </div>
                                    <small aviso-texto class="form-text text-muted">Para adicionar mais pessoas, Clicar no +1 ao Lado do Campo Telefone.</small>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                <div aviso-bloco class="form-group">
                                    <label for="parentesco_residente">Parentesco:*</label>
                                    <input aviso-campo class="form-control" type="text" name="parentesco_residente[]" placeholder="Parentesco" value="<?php echo set_value("parentesco_residente[$contador]",$value->parentesco_residente); ?>" >
                                    <div class="invalid-feedback">
                                        Insira o Parentesco
                                    </div>
                                    <small aviso-texto class="form-text text-muted">Para adicionar mais pessoas, Clicar no +1 ao Lado do Campo Telefone.</small>      
                
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                <div aviso-bloco class="form-group">
                                    <label for="telefone_residente">Telefone:*</label>
                                    <input aviso-campo class="form-control" type="text" mascara="cel" name="telefone_residente[]" placeholder="Telefone" value="<?php echo set_value("telefone_residente[$contador]",$value->telefone_residente); ?>" >
                                    <small aviso-texto class="form-text text-muted">
                                        Em caso de Não Aplicável, colocar (00) 0000-0000
                                    </small>
                                    <small aviso-texto class="form-text text-muted">Para adicionar mais pessoas, Clicar no +1 ao Lado do Campo Telefone.</small>      
                
                                    <div class="invalid-feedback">
                                        Insira o Telefone
                                    </div>
                                </div>
                            </div>
                        </div>
                         <?php $contador++; }?>
                    </div>
                    <div class="col-12 col-sm-12 col-md-1 col-lg-1 text-center">
                        <button type="button" class="btn btn-baggio btn-mais-um" href="#" clonar-elemento="elemento-campo-chave">
                            <b>+1</b>
                        </button>
                    </div>
                </div>
                
            </div>