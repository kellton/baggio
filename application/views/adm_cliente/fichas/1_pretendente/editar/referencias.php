            <div id="menu3" class="container tab-pane fade"><br>

                <p><small>Referências Pessoais (OBRIGATÓRIO 3 REFERÊNCIAS PESSOAIS)*</small></p>

                
<!---------------------------------------------------------------------------------------------------------------->

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="nome_referencia">Nome Completo:*</label>
                            <input aviso-campo class="form-control text-uppercase" type="text" value="<?php echo set_value('nome_referencia[0]',$referencias[0]->nome_referencia); ?>" name="nome_referencia[]" placeholder="Nome Completo" >
                            <div class="invalid-feedback">
                                Insira o Nome Completo
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="endereco_referencia">Endereço:*</label>
                            <input aviso-campo class="form-control text-uppercase" type="text" value="<?php echo set_value('endereco_referencia[0]',$referencias[0]->endereco_referencia); ?>" name="endereco_referencia[]" placeholder="Endereço" >
                            <div class="invalid-feedback">
                                Insira o Endereço
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="telefone_referencia">Telefone:*</label>
                            <input aviso-campo class="form-control" type="text" mascara="cel" value="<?php echo set_value('telefone_referencia[0]',$referencias[0]->telefone_referencia); ?>" name="telefone_referencia[]" placeholder="Telefone" >
                            <small aviso-texto class="form-text text-muted">
                                Em caso de Não Aplicável, colocar (00) 0000-0000
                            </small>
                            <div class="invalid-feedback">
                                Insira o Telefone
                            </div>
                        </div>
                    </div>
                </div>
                
<!---------------------------------------------------------------------------------------------------------------->

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="nome_referencia">Nome Completo:*</label>
                            <input aviso-campo class="form-control text-uppercase" type="text" value="<?php echo set_value('nome_referencia[1]',$referencias[1]->nome_referencia); ?>" name="nome_referencia[]" placeholder="Nome Completo" >
                            <div class="invalid-feedback">
                                Insira o Nome Completo
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="endereco_referencia">Endereço:*</label>
                            <input aviso-campo class="form-control text-uppercase" type="text" value="<?php echo set_value('endereco_referencia[1]',$referencias[1]->endereco_referencia); ?>" name="endereco_referencia[]" placeholder="Endereço" >
                            <div class="invalid-feedback">
                                Insira o Endereço
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="telefone_referencia">Telefone:*</label>
                            <input aviso-campo class="form-control" type="text" mascara="cel" value="<?php echo set_value('telefone_referencia[1]',$referencias[1]->telefone_referencia); ?>" name="telefone_referencia[]" placeholder="Telefone" >
                            <small aviso-texto class="form-text text-muted">
                                Em caso de Não Aplicável, colocar (00) 0000-0000
                            </small>
                            <div class="invalid-feedback">
                                Insira o Telefone
                            </div>
                        </div>
                    </div>
                </div>
                
<!---------------------------------------------------------------------------------------------------------------->

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="nome_referencia">Nome Completo:*</label>
                            <input aviso-campo class="form-control text-uppercase" type="text" value="<?php echo set_value('nome_referencia[2]',$referencias[2]->nome_referencia); ?>" name="nome_referencia[]" placeholder="Nome Completo" >
                            <div class="invalid-feedback">
                                Insira o Nome Completo
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="endereco_referencia">Endereço:*</label>
                            <input aviso-campo class="form-control text-uppercase" type="text" value="<?php echo set_value('endereco_referencia[2]',$referencias[2]->endereco_referencia); ?>" name="endereco_referencia[]" placeholder="Endereço" >
                            <div class="invalid-feedback">
                                Insira o Endereço
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="telefone_referencia">Telefone:*</label>
                            <input aviso-campo class="form-control" type="text" mascara="cel" value="<?php echo set_value('telefone_referencia[2]',$referencias[2]->telefone_referencia); ?>" name="telefone_referencia[]" placeholder="Telefone" >
                            <small aviso-texto class="form-text text-muted">
                                Em caso de Não Aplicável, colocar (00) 0000-0000
                            </small>
                            <div class="invalid-feedback">
                                Insira o Telefone
                            </div>
                        </div>
                    </div>
                </div>
                
                
<!---------------------------------------------------------------------------------------------------------------->

                <small><p>Referências Bancárias (OBRIGATÓRIO 2 REFERÊNCIAS BANCÁRIAS)*</p></small>

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-3 col-lg-3">
                        <div aviso-bloco class="form-group">
                            <label for="banco_referencia">Banco:*</label>
                            <input aviso-campo class="form-control text-uppercase" type="text" value="<?php echo set_value('banco_referencia[0]',$referencias_bancarias[0]->banco_referencia); ?>" name="banco_referencia[]" placeholder="Banco" >
                            <div class="invalid-feedback">
                                Insira o Nome do Banco
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-3 col-lg-3">
                        <div aviso-bloco class="form-group">
                            <label for="telefone_banco_referencia">Telefone da Agencia:*</label>
                            <input aviso-campo class="form-control" type="text" mascara="tel" value="<?php echo set_value('telefone_banco_referencia[0]',$referencias_bancarias[0]->telefone_banco_referencia); ?>" name="telefone_banco_referencia[]" placeholder="Telefone da Agencia" >
                            <small aviso-texto class="form-text text-muted">
                                Em caso de Não Aplicável, colocar (00) 0000-0000
                            </small>
                            <div class="invalid-feedback">
                                Insira o Telefone da Agencia
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-3 col-lg-3">
                        <div aviso-bloco class="form-group">
                            <label for="nome_agencia_referencia">Nome da Agência:*</label>
                            <input aviso-campo class="form-control text-uppercase" type="text"  value="<?php echo set_value('nome_agencia_referencia[0]',$referencias_bancarias[0]->nome_agencia_referencia); ?>" name="nome_agencia_referencia[]" placeholder="Nome da Agência" >
                            <!--<small aviso-texto class="form-text text-muted">
                                Em caso de Não Aplicável, colocar (00) 0000-0000
                            </small>-->
                            <div class="invalid-feedback">
                                Insira o Telefone
                            </div>
                        </div>
                    </div>
                     <div class="col-12 col-sm-12 col-md-3 col-lg-3">
                        <div aviso-bloco class="form-group">
                            <label for="numero_conta_referencia">Número da Conta:*</label>
                            <input aviso-campo class="form-control" type="text" value="<?php echo set_value('numero_conta_referencia[0]',$referencias_bancarias[0]->nome_agencia_referencia); ?>" name="numero_conta_referencia[]" placeholder="Número da Conta" >
                            <div class="invalid-feedback">
                                Insira o Número da Conta
                            </div>
                        </div>
                    </div>
                </div>
                
<!---------------------------------------------------------------------------------------------------------------->

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-3 col-lg-3">
                        <div aviso-bloco class="form-group">
                            <label for="banco_referencia">Banco:*</label>
                            <input aviso-campo class="form-control text-uppercase" type="text" value="<?php echo set_value('banco_referencia[1]',$referencias_bancarias[1]->banco_referencia); ?>" name="banco_referencia[]" placeholder="Banco" >
                            <div class="invalid-feedback">
                                Insira o Nome do Banco
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-3 col-lg-3">
                        <div aviso-bloco class="form-group">
                            <label for="telefone_banco_referencia">Telefone da Agencia:*</label>
                            <input aviso-campo class="form-control" type="text" value="<?php echo set_value('telefone_banco_referencia[1]',$referencias_bancarias[1]->telefone_banco_referencia); ?>" mascara="tel" name="telefone_banco_referencia[]" placeholder="Telefone da Agencia" >
                            <small aviso-texto class="form-text text-muted">
                                Em caso de Não Aplicável, colocar (00) 0000-0000
                            </small>
                            <div class="invalid-feedback">
                                Insira o Telefone da Agencia
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-3 col-lg-3">
                        <div aviso-bloco class="form-group">
                            <label for="nome_agencia_referencia">Nome da Agência:*</label>
                            <input aviso-campo class="form-control text-uppercase" type="text" value="<?php echo set_value('nome_agencia_referencia[1]',$referencias_bancarias[1]->nome_agencia_referencia); ?>" name="nome_agencia_referencia[]" placeholder="Nome da Agência" >
                            <!--<small aviso-texto class="form-text text-muted">
                                Em caso de Não Aplicável, colocar (00) 0000-0000
                            </small>-->
                            <div class="invalid-feedback">
                                Insira o Telefone
                            </div>
                        </div>
                    </div>
                     <div class="col-12 col-sm-12 col-md-3 col-lg-3">
                        <div aviso-bloco class="form-group">
                            <label for="numero_conta_referencia">Número da Conta:*</label>
                            <input aviso-campo class="form-control" type="text" value="<?php echo set_value('numero_conta_referencia[1]',$referencias_bancarias[1]->numero_conta_referencia); ?>" name="numero_conta_referencia[]" placeholder="Número da Conta" >
                            <div class="invalid-feedback">
                                Insira o Número da Conta
                            </div>
                        </div>
                    </div>
                </div>


<!-------------------------------------------------------------------------------------------------------------------->

                        <hr>
                        
<!-------------------------------------------------------------------------------------------------------------------->

                <div class="col">
                    <label>Sua Mãe é Falecida?</label><br>
                    <div class="custom-controls-stacked d-block my-3">
                        <label class="custom-control custom-radio">
                            <input aviso-campo id="radioStacked1" name="status_mae" value="1" togvis-button="mae-falecida" type="radio" class="" <?php if(!$mae) echo 'checked';?>>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Sim</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input aviso-campo id="radioStacked2" name="status_mae" value="0" togvis-button="nao-mae-falecida" type="radio" class="" <?php if($mae) echo 'checked';?>>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Não</span>
                        </label>
                    </div>
                </div>

<!------------------------------------------------------------------------------------------------------------------>

                <div togvis-id="nao-mae-falecida" togvis-estado='hide' togvis-outros='esconder' togvis-grupo="grupo-mae">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                            <div aviso-bloco class="form-group">
                                <label for="mae_pretendente">Nome Completo da Mãe:*</label>
                                <input aviso-campo class="form-control text-uppercase" type="text" value="<?php echo set_value('mae_pretendente',$mae['nome']); ?>" name="mae_pretendente" placeholder="Nome Completo da Mãe">
                                <div class="invalid-feedback">
                                    Insira o Nome Completo da Mãe
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                            <div aviso-bloco class="form-group">
                                <label for="telefone_mae_pretendente">Telefone:*</label>
                                <input aviso-campo class="form-control" mascara="cel" type="text" value="<?php echo set_value('telefone_mae_pretendente',$mae['telefone']); ?>" name="telefone_mae_pretendente" placeholder="Telefone">
                                <small aviso-texto class="form-text text-muted">
                                    Em caso de Não Aplicável, colocar (00) 0000-0000
                                </small>
                                <div class="invalid-feedback">
                                    Insira o Telefone
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                            <div class="row">
                                <div id="" class="col">
                                    <div aviso-bloco class="form-group">
                                        <label for="cep_mae_cliente">CEP:*</label>
                                        <input aviso-campo cep-bloco="mae" class="form-control" type="text" name="cep_mae_cliente" value="<?php echo set_value('cep_mae_cliente',$mae['cep']); ?>" placeholder="CEP">
                                        <small aviso-texto class="form-text text-muted">Caso não possua, escrever 'não aplicável'.</small>
                                        <div class="invalid-feedback">
                                            Insira o CEP
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

<!------------------------------------------------------------------------------------------------------------------>
                    
                    <div class="row">
                        
                        <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                            <div aviso-bloco class="form-group">
                                <label for="logradouro_mae_cliente">Logradouro:*</label>
                                <input aviso-campo cep-rua="mae" class="form-control text-uppercase" type="text" value="<?php echo set_value('logradouro_mae_cliente',$mae['logradouro']); ?>" name="logradouro_mae_cliente" placeholder="Logradouro">
                                <small aviso-texto class="form-text text-muted">Rua, Avenida, Estrada, Travessa, etc.</small>
                                <small aviso-texto class="form-text text-muted">Caso não possua, escrever 'não aplicável'.</small>
                                <div class="invalid-feedback">
                                    Insira o Logradouro
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                            <div aviso-bloco class="form-group">
                                <label for="numero_mae_cliente">Número:*</label>
                                <input aviso-campo class="form-control" type="text" value="<?php echo set_value('numero_mae_cliente',$mae['numero']); ?>" name="numero_mae_cliente" placeholder="Número">
                                <small aviso-texto class="form-text text-muted">Caso não possua, escrever 'não aplicável'.</small>
                                <div class="invalid-feedback">
                                    Insira o Número
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                            <div aviso-bloco class="form-group">
                                <label for="complemento_mae_cliente">Complemento:</label>
                                <input aviso-campo class="form-control text-uppercase" type="text" value="<?php echo set_value('complemento_mae_cliente',$mae['complemento']); ?>" name="complemento_mae_cliente" placeholder="Complemento">
                                <small aviso-texto class="form-text text-muted">Caso não possua, escrever 'não aplicável'.</small>
                            </div>
                        </div>

                    </div>

<!------------------------------------------------------------------------------------------------------------------>

                    <div class="row">
                        
                        <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                            <div aviso-bloco class="form-group">
                                <label for="bairro_mae_cliente">Bairro:*</label>
                                <input aviso-campo cep-bairro="mae" class="form-control text-uppercase" type="text" value="<?php echo set_value('bairro_mae_cliente',$mae['bairro']); ?>" name="bairro_mae_cliente" placeholder="Bairro">
                                <small aviso-texto class="form-text text-muted">
                                    Caso não possua, escrever 'não aplicável'.
                                </small>
                                <div class="invalid-feedback">
                                    Insira o Bairro
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-sm-12 col-md-4 col-lg-4">
	                        <div aviso-bloco class="form-group">
	                            <label for="complemento_cliente">Cidade:*</label>
	                            <input aviso-campo cep-cidade="mae" class="form-control text-uppercase" type="text" value="" name="" placeholder="Cidade">
	                        </div>
	                        <div class="invalid-feedback">
	                            Insira o Estado
	                        </div>
	                    </div>

	                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
	                        <div aviso-bloco class="form-group">
	                            <label for="bairro_cliente">Estado:*</label>
	                            <input aviso-campo cep-uf="mae" class="form-control text-uppercase" type="text" value="" name="" placeholder="Estado" >
	                            <div class="invalid-feedback">
	                                Insira o Estado
	                            </div>
	                        </div>
	                    </div>
                    </div>

                    <div class="row">
                    	<div class="col-12 col-sm-12 col-md-4 col-lg-4">
                            <div aviso-bloco class="form-group">
                                <label for="id_pais_residencia_mae">País:*</label>
                                <?php 
                                    $array_pais['']='Selecione o País';
                                    foreach ($paises as $value) {
                                        $array_pais[$value->id_pais]=$value->nome_pais;
                                    }
                                ?>
                                <?php echo form_dropdown('id_pais_residencia_mae', $array_pais, set_value('id_pais_residencia_mae',33), 'class="form-control"') ?>
                                <div class="invalid-feedback">
                                    Selecione um País
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div togvis-id="mae-falecida" togvis-estado='hide' togvis-outros='esconder' togvis-grupo="grupo-mae">
                </div>


<!-------------------------------------------------------------------------------------------------------------------->

                        <hr>
                        
<!-------------------------------------------------------------------------------------------------------------------->

                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                    <label>Seu Pai é Falecido?</label><br>
                    <div class="custom-controls-stacked d-block my-3">
                        <label class="custom-control custom-radio">
                            <input aviso-campo id="radioStacked1" name="status_pai" value="1" togvis-button="pai-falecida" type="radio" class="" <?php if(!$pai) echo 'checked';?>>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Sim</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input aviso-campo id="radioStacked2" name="status_pai" value="0" togvis-button="nao-pai-falecida" type="radio" class="" <?php if($pai) echo 'checked';?>>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Não</span>
                        </label>
                    </div>
                </div>
                <div togvis-id="nao-pai-falecida" togvis-estado='hide' togvis-outros='esconder' togvis-grupo="grupo-pai">
                    
<!-------------------------------------------------------------------------------------------------------------------->


                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                            <div aviso-bloco class="form-group">
                                <label for="pai_pretendente">Nome Completo do Pai:*</label>
                                <input aviso-campo class="form-control text-uppercase" type="text" value="<?php echo set_value('pai_pretendente',$pai['nome']); ?>" name="pai_pretendente" placeholder="Nome Completo do Pai">
                                <div class="invalid-feedback">
                                    Insira o Nome Completo do Pai
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                            <div aviso-bloco class="form-group">
                                <label for="telefone_pai_pretendente">Telefone:*</label>
                                <input aviso-campo class="form-control" mascara="cel" type="text" value="<?php echo set_value('telefone_pai_pretendente',$pai['telefone']); ?>" name="telefone_pai_pretendente" placeholder="Telefone">
                                <small aviso-texto class="form-text text-muted">
                                    Em caso de Não Aplicável, colocar (00) 0000-0000
                                </small>
                                <div class="invalid-feedback">
                                    Insira o Telefone
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                            <div id="" class="row">
                                <div id="" class="col">
                                    <div aviso-bloco class="form-group">
                                        <label for="cep_pai_cliente">CEP:*</label>
                                        <input aviso-campo cep-bloco="pai" class="form-control" type="text" name="cep_pai_cliente" value="<?php echo set_value('cep_pai_cliente',$pai['cep']); ?>" placeholder="CEP">
                                        <small aviso-texto class="form-text text-muted">
                                    Caso não possua, escrever 'não aplicável'.
                                </small>
                                        <div class="invalid-feedback">
                                            Insira o CEP
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
<!-------------------------------------------------------------------------------------------------------------------->


                    <div class="row">
                        
                        <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                            <div aviso-bloco class="form-group">
                                <label for="logradouro_pai_cliente">Logradouro:*</label>
                                <input aviso-campo cep-rua="pai" class="form-control text-uppercase" type="text" value="<?php echo set_value('logradouro_pai_cliente',$pai['logradouro']); ?>" name="logradouro_pai_cliente" placeholder="Logradouro">
                                <small aviso-texto class="form-text text-muted">Rua, Avenida, Estrada, Travessa, etc.</small>
                                <small aviso-texto class="form-text text-muted">
                                    Caso não possua, escrever 'não aplicável'.
                                </small>
                                <div class="invalid-feedback">
                                    Insira o Logradouro
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                            <div aviso-bloco class="form-group">
                                <label for="numero_pai_cliente">Número:*</label>
                                <input aviso-campo class="form-control" type="text" value="<?php echo set_value('numero_pai_cliente',$pai['numero']); ?>" name="numero_pai_cliente" placeholder="Número">
                                <small aviso-texto class="form-text text-muted">
                                    Caso não possua, escrever 'não aplicável'.
                                </small>
                                <div class="invalid-feedback">
                                    Insira o Número
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                            <div aviso-bloco class="form-group">
                                <label for="complemento_pai_cliente">Complemento:</label>
                                <input aviso-campo class="form-control text-uppercase" type="text" value="<?php echo set_value('complemento_pai_cliente',$pai['complemento']); ?>" name="complemento_pai_cliente" placeholder="Complemento">
                                <small aviso-texto class="form-text text-muted">
                                    Caso não possua, escrever 'não aplicável'.
                                </small>
                            </div>
                        </div>
                    </div>
                    
<!-------------------------------------------------------------------------------------------------------------------->


                    <div class="row">
                        
                        <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                            <div aviso-bloco class="form-group">
                                <label for="bairro_pai_cliente">Bairro:*</label>
                                <input aviso-campo cep-bairro="pai" class="form-control text-uppercase" type="text" value="<?php echo set_value('bairro_pai_cliente',$pai['bairro']); ?>" name="bairro_pai_cliente" placeholder="Bairro">
                                <small aviso-texto class="form-text text-muted">
                                    Caso não possua, escrever 'não aplicável'.
                                </small>
                                <div class="invalid-feedback">
                                    Insira o Bairro
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-sm-12 col-md-4 col-lg-4">
	                        <div aviso-bloco class="form-group">
	                            <label for="complemento_cliente">Cidade:*</label>
	                            <input aviso-campo cep-cidade="pai" class="form-control text-uppercase" type="text" value="" name="" placeholder="Cidade">
	                        </div>
	                        <div class="invalid-feedback">
	                            Insira o Estado
	                        </div>
	                    </div>

	                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
	                        <div aviso-bloco class="form-group">
	                            <label for="bairro_cliente">Estado:*</label>
	                            <input aviso-campo cep-uf="pai" class="form-control text-uppercase" type="text" value="" name="" placeholder="Estado" >
	                            <div class="invalid-feedback">
	                                Insira o Estado
	                            </div>
	                        </div>
	                    </div>
                    </div>

                    <div class="row">
                    	<div class="col-12 col-sm-12 col-md-4 col-lg-4">
                            <div aviso-bloco class="form-group">
                                <label for="id_pais_residencia_pai">País:*</label>
                                <?php echo form_dropdown('id_pais_residencia_pai', $array_pais, set_value('id_pais_residencia_pai',33), 'class="form-control"') ?>
                                <div class="invalid-feedback">
                                    Selecione um País
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div togvis-id="pai-falecida" togvis-estado='hide' togvis-outros='esconder' togvis-grupo="grupo-pai">
                </div>
            </div>