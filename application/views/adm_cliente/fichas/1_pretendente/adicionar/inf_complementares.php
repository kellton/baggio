                
                <div id="menu4" class="container tab-pane fade"><br>
                    <div class="form-check">
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class=""  name="concordo">
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">
                                OBS. No valor do aluguel, serão acrescidos mensalmente as 
                                taxas: IPTU, água, luz, seguro contra incêndio, condomínio, 
                                etc. Consulte nossos atendentes para maiores informações.
                            </span>
                        </label>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="observacoes_complementares">Observações Complementares: <p> <small>Preencher caso haja alguma observação.</small> </p></label>
                                <textarea class="form-control" rows="5" name="observacoes_complementares"></textarea>
                            </div>
                        </div>
                    </div>
                    

                    <!-------------------------------------------------------------------------------------------------->
                    
                    <div class="row">
                        <div class="col">
                            <label>Possui 2º Pretendente?*</label><br>
                            <small>O 2º pretendente pode ser considerada a pessoa que irá compor a renda com o pretendente.</small>
                            <div class="custom-controls-stacked d-block my-3">
                                <label class="custom-control custom-radio">
                                    <input id="radioStacked1" name="segundo_pretendente" value="1" togvis-button="pretendente" type="radio" class="" >
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Sim</span>
                                </label>
                                <label class="custom-control custom-radio">
                                    <input id="radioStacked2" name="segundo_pretendente" value="0" togvis-button="nao-pretendente" type="radio" class="" >
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Não</span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div togvis-id="pretendente" togvis-estado='hide' togvis-outros='esconder' togvis-grupo='grupo-pretendente'>
                        <div class="row">
                            <div class="col">
                                <label>O 2º pretendete é seu cônjuge?*</label><br>
                                <small>ATENÇÃO: A ficha de segundo pretendente ficará disponível na área "fichas cadastrais" após o envio dessa ficha ao corretor responsável.</small>
                                <div class="custom-controls-stacked d-block my-3">
                                    <label class="custom-control custom-radio">
                                        <input id="radioStacked1" name="segundo_pretendente_conjugue" value="1" type="radio" class="">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">Sim</span>
                                    </label>
                                    <label class="custom-control custom-radio">
                                        <input id="radioStacked2" name="segundo_pretendente_conjugue" value="0" type="radio" class="">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">Não</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div togvis-id="nao-pretendente" togvis-estado='hide' togvis-outros='esconder' togvis-grupo='grupo-pretendente'>
                    </div>
                </div>