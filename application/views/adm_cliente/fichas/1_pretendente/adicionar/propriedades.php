            <div id="menu2" class="container tab-pane fade"><br>
                <!--<div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <h1 class="display-4">Propriedades</h1>
                    </div>
                </div>-->
                <div class="row">
                    <div class="col"> 
                        <div class="row" id="elemento-campo-propriedade">
                            <div class="col">
                                <div aviso-bloco class="form-group">
                                    <label for="matricula_imovel">Nº da Matícula do Imóvel:*</label>
                                    <input aviso-campo class="form-control" type="text" value="<?php echo set_value('matricula_imovel[]'); ?>" name="matricula_imovel[]" placeholder="Nº da Matícula do Imóvel" >
                                    <div class="invalid-feedback">
                                        Insira o Nº da Matícula do Imóvel
                                    </div>
                                    <small aviso-texto class="form-text text-muted">Para Adicionar Mais Propriedades, clique Botão +1 ao lado do Renavam. Caso não possua, escrever 'não aplicável'.</small>
                                </div>
                            </div>
                            <div class="col">
                                <div aviso-bloco class="form-group">
                                    <label for="circunscricao">Circunscrição:*</label>
                                    <input aviso-campo class="form-control" type="text" value="<?php echo set_value('circunscricao[]'); ?>" name="circunscricao[]" placeholder="Circunscrição" >
                                    <div class="invalid-feedback">
                                        Insira Circunscrição
                                    </div>
                                    <small aviso-texto class="form-text text-muted">Para Adicionar Mais Propriedades, clique Botão +1 ao lado do Renavam. Caso não possua, escrever 'não aplicável'.</small>
                                
                                </div>
                            </div>
                            <div class="col">
                                <div aviso-bloco class="form-group">
                                    <label for="veiculo_marca">Marca Do Veículo:*</label>
                                    <input aviso-campo class="form-control" type="text" value="<?php echo set_value('veiculo_marca[]'); ?>" name="veiculo_marca[]" placeholder="Marca Do Veículo" >
                                    <div class="invalid-feedback">
                                        Insira a Marca Do Veículo
                                    </div>
                                    <small aviso-texto class="form-text text-muted">Para Adicionar Mais Propriedades, clique Botão +1 ao lado do Renavam. Caso não possua, escrever 'não aplicável'.</small>
                                
                                </div>
                            </div>
                            <div class="col">
                                <div aviso-bloco class="form-group">
                                    <label for="renavam">Renavam:*</label>
                                    <input aviso-campo class="form-control" type="text" value="<?php echo set_value('renavam[]'); ?>" name="renavam[]" placeholder="Renavam" >
                                    <div class="invalid-feedback">
                                        Insira O Renavam
                                    </div>
                                    <small aviso-texto class="form-text text-muted">Para Adicionar Mais Propriedades, clique Botão +1 ao lado do Renavam. Caso não possua, escrever 'não aplicável'.</small>
                                
                                </div>
                            </div>
                            <div class="col-1 ">
                                <button type="button" class="btn btn-baggio btn-mais-um" href="#" clonar-elemento="elemento-campo-propriedade">
                                            <b>+1</b>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>