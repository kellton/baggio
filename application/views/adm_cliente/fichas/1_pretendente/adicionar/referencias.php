            <div id="menu3" class="container tab-pane fade"><br>
    
                <p><small>Referências Pessoais (OBRIGATÓRIO 3 REFERÊNCIAS PESSOAIS)*</small></p>

                
<!-------------------------------------------------------------------------------------------------------------------->

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="nome_referencia">Nome Completo:*</label>
                            <input aviso-campo class="form-control text-uppercase" type="text" value="<?php echo set_value('nome_referencia[0]'); ?>" name="nome_referencia[]" placeholder="Nome Completo" >
                            <div class="invalid-feedback">
                                Insira o Nome Completo
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="endereco_referencia">Endereço:*</label>
                            <input aviso-campo class="form-control text-uppercase" type="text" value="<?php echo set_value('endereco_referencia[0]'); ?>" name="endereco_referencia[]" placeholder="Endereço" >
                            <div class="invalid-feedback">
                                Insira o Endereço
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="telefone_referencia">Telefone:*</label>
                            <input aviso-campo class="form-control" type="text" mascara="tel" value="<?php echo set_value('telefone_referencia[0]'); ?>" name="telefone_referencia[]" placeholder="Telefone" >
                            <small aviso-texto class="form-text text-muted">
                                Em caso de Não Aplicável, colocar (00) 0000-0000
                            </small>
                            <div class="invalid-feedback">
                                Insira o Telefone
                            </div>
                        </div>
                    </div>
                </div>
                
<!-------------------------------------------------------------------------------------------------------------------->

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="nome_referencia">Nome Completo:*</label>
                            <input aviso-campo class="form-control text-uppercase" type="text" value="<?php echo set_value('nome_referencia[1]'); ?>" name="nome_referencia[]" placeholder="Nome Completo" >
                            <div class="invalid-feedback">
                                Insira o Nome Completo
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="endereco_referencia">Endereço:*</label>
                            <input aviso-campo class="form-control text-uppercase" type="text" value="<?php echo set_value('endereco_referencia[1]'); ?>" name="endereco_referencia[]" placeholder="Endereço" >
                            <div class="invalid-feedback">
                                Insira o Endereço
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="telefone_referencia">Telefone:*</label>
                            <input aviso-campo class="form-control" type="text" mascara="tel" value="<?php echo set_value('telefone_referencia[1]'); ?>" name="telefone_referencia[]" placeholder="Telefone" >
                            <small aviso-texto class="form-text text-muted">
                                Em caso de Não Aplicável, colocar (00) 0000-0000
                            </small>
                            <div class="invalid-feedback">
                                Insira o Telefone
                            </div>
                        </div>
                    </div>
                </div>
                
<!-------------------------------------------------------------------------------------------------------------------->

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="nome_referencia">Nome Completo:*</label>
                            <input aviso-campo class="form-control text-uppercase" type="text" value="<?php echo set_value('nome_referencia[2]'); ?>" name="nome_referencia[]" placeholder="Nome Completo" >
                            <div class="invalid-feedback">
                                Insira o Nome Completo
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="endereco_referencia">Endereço:*</label>
                            <input aviso-campo class="form-control text-uppercase" type="text" value="<?php echo set_value('endereco_referencia[2]'); ?>" name="endereco_referencia[]" placeholder="Endereço" >
                            <div class="invalid-feedback">
                                Insira o Endereço
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="telefone_referencia">Telefone:*</label>
                            <input aviso-campo class="form-control" type="text" mascara="tel" value="<?php echo set_value('telefone_referencia[2]'); ?>" name="telefone_referencia[]" placeholder="Telefone" >
                            <small aviso-texto class="form-text text-muted">
                                Em caso de Não Aplicável, colocar (00) 0000-0000
                            </small>
                            <div class="invalid-feedback">
                                Insira o Telefone
                            </div>
                        </div>
                    </div>
                </div>
                <p>Referências Bancárias (OBRIGATÓRIO 2 REFERÊNCIAS BANCÁRIAS)*</p>
                
<!-------------------------------------------------------------------------------------------------------------------->

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-3 col-lg-3">
                        <div aviso-bloco class="form-group">
                            <label for="banco_referencia">Banco:*</label>
                            <input aviso-campo class="form-control text-uppercase" type="text" value="<?php echo set_value('banco_referencia[0]'); ?>" name="banco_referencia[]" placeholder="Banco" >
                            <div class="invalid-feedback">
                                Insira o Nome do Banco
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-3 col-lg-3">
                        <div aviso-bloco class="form-group">
                            <label for="telefone_banco_referencia">Telefone da Agencia:*</label>
                            <input aviso-campo class="form-control" type="text" mascara="tel" value="<?php echo set_value('telefone_banco_referencia[0]'); ?>" name="telefone_banco_referencia[]" placeholder="Telefone da Agencia" >
                            <small aviso-texto class="form-text text-muted">
                                Em caso de Não Aplicável, colocar (00) 0000-0000
                            </small>
                            <div class="invalid-feedback">
                                Insira o Telefone da Agencia
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-3 col-lg-3">
                        <div aviso-bloco class="form-group">
                            <label for="nome_agencia_referencia">Nome da Agência:*</label>
                            <input aviso-campo class="form-control text-uppercase" type="text"  value="<?php echo set_value('nome_agencia_referencia[0]'); ?>" name="nome_agencia_referencia[]" placeholder="Nome da Agência" >
                            <!--<small aviso-texto class="form-text text-muted">
                                Em caso de Não Aplicável, colocar (00) 0000-0000
                            </small>-->
                            <div class="invalid-feedback">
                                Insira o Telefone
                            </div>
                        </div>
                    </div>
                     <div class="col-12 col-sm-12 col-md-3 col-lg-3">
                        <div aviso-bloco class="form-group">
                            <label for="numero_conta_referencia">Número da Conta:*</label>
                            <input aviso-campo class="form-control" type="text" value="<?php echo set_value('numero_conta_referencia[0]'); ?>" name="numero_conta_referencia[]" placeholder="Número da Conta" >
                            <div class="invalid-feedback">
                                Insira o Número da Conta
                            </div>
                        </div>
                    </div>
                </div>
                
<!-------------------------------------------------------------------------------------------------------------------->

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-3 col-lg-3">
                        <div aviso-bloco class="form-group">
                            <label for="banco_referencia">Banco:*</label>
                            <input aviso-campo class="form-control text-uppercase" type="text" value="<?php echo set_value('banco_referencia[1]'); ?>" name="banco_referencia[]" placeholder="Banco" >
                            <div class="invalid-feedback">
                                Insira o Nome do Banco
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-3 col-lg-3">
                        <div aviso-bloco class="form-group">
                            <label for="telefone_banco_referencia">Telefone da Agencia:*</label>
                            <input aviso-campo class="form-control" type="text" value="<?php echo set_value('telefone_banco_referencia[1]'); ?>" mascara="tel" name="telefone_banco_referencia[]" placeholder="Telefone da Agencia" >
                            <small aviso-texto class="form-text text-muted">
                                Em caso de Não Aplicável, colocar (00) 0000-0000
                            </small>
                            <div class="invalid-feedback">
                                Insira o Telefone da Agencia
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-3 col-lg-3">
                        <div aviso-bloco class="form-group">
                            <label for="nome_agencia_referencia">Nome da Agência:*</label>
                            <input aviso-campo class="form-control text-uppercase" type="text" value="<?php echo set_value('nome_agencia_referencia[1]'); ?>" name="nome_agencia_referencia[]" placeholder="Nome da Agência" >
                            <!--<small aviso-texto class="form-text text-muted">
                                Em caso de Não Aplicável, colocar (00) 0000-0000
                            </small>-->
                            <div class="invalid-feedback">
                                Insira o Telefone
                            </div>
                        </div>
                    </div>
                     <div class="col-12 col-sm-12 col-md-3 col-lg-3">
                        <div aviso-bloco class="form-group">
                            <label for="numero_conta_referencia">Número da Conta:*</label>
                            <input aviso-campo class="form-control" type="text" value="<?php echo set_value('numero_conta_referencia[1]'); ?>" name="numero_conta_referencia[]" placeholder="Número da Conta" >
                            <div class="invalid-feedback">
                                Insira o Número da Conta
                            </div>
                        </div>
                    </div>
                </div>


<!-------------------------------------------------------------------------------------------------------------------->

                        <hr>
                        
<!-------------------------------------------------------------------------------------------------------------------->

            
                <div class="col">
                    <label>Sua Mãe é Falecida?</label><br>
                    <div class="custom-controls-stacked d-block my-3">
                        <label class="custom-control custom-radio">
                            <input aviso-campo id="radioStacked1" name="status_mae" value="1" togvis-button="mae-falecida" type="radio" class="" >
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Sim</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input aviso-campo id="radioStacked2" name="status_mae" value="0" togvis-button="nao-mae-falecida" type="radio" class="" >
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Não</span>
                        </label>
                    </div>
                </div> 
                <div togvis-id="nao-mae-falecida" togvis-estado='hide' togvis-outros='esconder' togvis-grupo="grupo-mae">
                    
<!-------------------------------------------------------------------------------------------------------------------->

                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                            <div aviso-bloco class="form-group">
                                <label for="mae_pretendente">Nome Completo da Mãe:*</label>
                                <input aviso-campo class="form-control text-uppercase" type="text" value="<?php echo set_value('mae_pretendente'); ?>" name="mae_pretendente" placeholder="Nome Completo da Mãe">
                                <div class="invalid-feedback">
                                    Insira o Nome Completo da Mãe
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                            <div aviso-bloco class="form-group">
                                <label for="telefone_mae_pretendente">Telefone:*</label>
                                <input aviso-campo class="form-control" mascara="cel" type="text" value="<?php echo set_value('telefone_mae_pretendente'); ?>" name="telefone_mae_pretendente" placeholder="Telefone">
                                <small aviso-texto class="form-text text-muted">
                                    Em caso de Não Aplicável, colocar (00) 0000-0000
                                </small>
                                <div class="invalid-feedback">
                                    Insira o Telefone
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                            <div id="" class="row">
                                <div id="" class="col">
                                    <div aviso-bloco class="form-group">
                                        <label for="cep_mae_cliente">CEP:*</label>
                                        <input aviso-campo cep-bloco="mae" class="form-control" type="text" name="cep_mae_cliente" value="<?php echo set_value('cep_mae_cliente'); ?>" placeholder="CEP">
                                        <div class="invalid-feedback">
                                            Insira o CEP
                                        </div>
                                        <small aviso-texto class="form-text text-muted">Caso não possua, escrever 'não aplicável'.</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
<!-------------------------------------------------------------------------------------------------------------------->

                    <div class="row">
                        
                        <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                            <div aviso-bloco class="form-group">
                                <label for="logradouro_mae_cliente">Logradouro:*</label>
                                <input aviso-campo cep-rua="mae" class="form-control text-uppercase" type="text" value="<?php echo set_value('logradouro_mae_cliente'); ?>" name="logradouro_mae_cliente" placeholder="Logradouro">
                                <div class="invalid-feedback">
                                    Insira o Logradouro
                                </div>
                                <small aviso-texto class="form-text text-muted">Rua, Avenida, Estrada, Travessa, etc.</small>
                                <small aviso-texto class="form-text text-muted">Caso não possua, escrever 'não aplicável'.</small>
                                
                            </div>
                        </div>

                        <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                            <div aviso-bloco class="form-group">
                                <label for="numero_mae_cliente">Número:*</label>
                                <input aviso-campo class="form-control" type="text" value="<?php echo set_value('numero_mae_cliente'); ?>" name="numero_mae_cliente" placeholder="Número">
                                <div class="invalid-feedback">
                                    Insira o Número
                                </div>
                                <small aviso-texto class="form-text text-muted">Caso não possua, escrever 'não aplicável'.</small>
                                
                            </div>
                        </div>

                        <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                            <div aviso-bloco class="form-group">
                                <label for="complemento_mae_cliente">Complemento:</label>
                                <input aviso-campo class="form-control text-uppercase" type="text" value="<?php echo set_value('complemento_mae_cliente'); ?>" name="complemento_mae_cliente" placeholder="Complemento">
                                <small aviso-texto class="form-text text-muted">Caso não possua, escrever 'não aplicável'.</small>
                            </div>
                        </div>
                    </div>
                    
<!-------------------------------------------------------------------------------------------------------------------->

                    <div class="row">
                        
                        <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                            <div aviso-bloco class="form-group">
                                <label for="bairro_mae_cliente">Bairro:*</label>
                                <input aviso-campo cep-bairro="mae" class="form-control text-uppercase" type="text" value="<?php echo set_value('bairro_mae_cliente'); ?>" name="bairro_mae_cliente" placeholder="Bairro">
                                <div class="invalid-feedback">
                                    Insira o Bairro
                                </div>
                                <small aviso-texto class="form-text text-muted">
                                    Caso não possua, escrever 'não aplicável'.
                                </small>
                            </div>
                        </div>

                        <div class="col-12 col-sm-12 col-md-4 col-lg-4">
	                        <div aviso-bloco class="form-group">
	                            <label for="complemento_cliente">Cidade:*</label>
	                            <input aviso-campo cep-cidade="mae" class="form-control text-uppercase" type="text" value="" name="" placeholder="Cidade">
	                        </div>
	                        <div class="invalid-feedback">
	                            Insira a Cidade
	                        </div>
	                    </div>

	                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
	                        <div aviso-bloco class="form-group">
	                            <label for="bairro_cliente">Estado:*</label>
	                            <input aviso-campo cep-uf="mae" class="form-control text-uppercase" type="text" value="" name="" placeholder="Estado" >
	                            <div class="invalid-feedback">
	                                Insira o Estado
	                            </div>
	                        </div>
	                    </div>
                    </div>

                    <div class="row">
                    	<div class="col-12 col-sm-12 col-md-4 col-lg-4">
                            <div aviso-bloco class="form-group">
                                <label for="id_pais_residencia_mae">País:*</label>
                                <?php 
                                    $array_pais['']='Selecione o País';
                                    foreach ($paises as $value) {
                                        $array_pais[$value->id_pais]=$value->nome_pais;
                                    }
                                ?>
                                <?php echo form_dropdown('id_pais_residencia_mae', $array_pais, set_value('id_pais_residencia_mae',33), 'class="form-control"') ?>
                                <div class="invalid-feedback">
                                    Selecione um País
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div togvis-id="mae-falecida" togvis-estado='hide' togvis-outros='esconder' togvis-grupo="grupo-mae">
                </div>
            

<!-------------------------------------------------------------------------------------------------------------------->

                        <hr>
                        
<!-------------------------------------------------------------------------------------------------------------------->


                <div class="col">
                    <label>Seu Pai é Falecido?</label><br>
                    <div class="custom-controls-stacked d-block my-3">
                        <label class="custom-control custom-radio">
                            <input aviso-campo id="radioStacked1" name="status_pai" value="1" togvis-button="pai-falecida" type="radio" class="" >
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Sim</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input aviso-campo id="radioStacked2" name="status_pai" value="0" togvis-button="nao-pai-falecida" type="radio" class="" >
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Não</span>
                        </label>
                    </div>
                </div>
                <div togvis-id="nao-pai-falecida" togvis-estado='hide' togvis-outros='esconder' togvis-grupo="grupo-pai">
                    
<!-------------------------------------------------------------------------------------------------------------------->


                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                            <div aviso-bloco class="form-group">
                                <label for="pai_pretendente">Nome Completo do Pai:*</label>
                                <input aviso-campo class="form-control text-uppercase" type="text" value="<?php echo set_value('pai_pretendente'); ?>" name="pai_pretendente" placeholder="Nome Completo do Pai">
                                <div class="invalid-feedback">
                                    Insira o Nome Completo da Mãe
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                            <div aviso-bloco class="form-group">
                                <label for="telefone_pai_pretendente">Telefone:*</label>
                                <input aviso-campo mascara="cel" class="form-control" type="text" value="<?php echo set_value('telefone_pai_pretendente'); ?>" name="telefone_pai_pretendente" placeholder="Telefone">
                                <small aviso-texto class="form-text text-muted">
                                    Em caso de Não Aplicável, colocar (00) 0000-0000
                                </small>
                                <div class="invalid-feedback">
                                    Insira o Telefone
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                            <div id="" class="row">
                                <div id="" class="col">
                                    <div aviso-bloco class="form-group">
                                        <label for="cep_pai_cliente">CEP:*</label>
                                        <input aviso-campo cep-bloco="pai" class="form-control" type="text" name="cep_pai_cliente" value="<?php echo set_value('cep_pai_cliente'); ?>" placeholder="CEP">
                                        <div class="invalid-feedback">
                                            Insira o CEP
                                        </div>
                                        <small aviso-texto class="form-text text-muted">
                                            Caso não possua, escrever 'não aplicável'.
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
<!-------------------------------------------------------------------------------------------------------------------->


                    <div class="row">
                        
                        <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                            <div aviso-bloco class="form-group">
                                <label for="logradouro_pai_cliente">Logradouro:*</label>
                                <input aviso-campo cep-rua="pai" class="form-control text-uppercase" type="text" value="<?php echo set_value('logradouro_pai_cliente'); ?>" name="logradouro_pai_cliente" placeholder="Logradouro">
                                <div class="invalid-feedback">
                                    Insira o Logradouro
                                </div>
                                <small aviso-texto class="form-text text-muted">
                                    Rua, Avenida, Estrada, Travessa, etc.
                                </small>
                                <small aviso-texto class="form-text text-muted">
                                    Caso não possua, escrever 'não aplicável'.
                                </small>
                            </div>
                        </div>

                        <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                            <div aviso-bloco class="form-group">
                                <label for="numero_pai_cliente">Número:*</label>
                                <input aviso-campo class="form-control" type="text" value="<?php echo set_value('numero_pai_cliente'); ?>" name="numero_pai_cliente" placeholder="Número">
                                <div class="invalid-feedback">
                                    Insira o Número
                                </div>
                                <small aviso-texto class="form-text text-muted">
                                    Caso não possua, escrever 'não aplicável'.
                                </small>
                            </div>
                        </div>

                        <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                            <div aviso-bloco class="form-group">
                                <label for="complemento_pai_cliente">Complemento:</label>
                                <input aviso-campo class="form-control text-uppercase" type="text" value="<?php echo set_value('complemento_pai_cliente'); ?>" name="complemento_pai_cliente" placeholder="Complemento">
                            </div>
                            <small aviso-texto class="form-text text-muted">
                                Caso não possua, escrever 'não aplicável'.
                            </small>
                        </div>
                    </div>
                    
<!-------------------------------------------------------------------------------------------------------------------->


                    <div class="row">
                        
                        <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                            <div aviso-bloco class="form-group">
                                <label for="bairro_pai_cliente">Bairro:*</label>
                                <input aviso-campo cep-bairro="pai" class="form-control text-uppercase" type="text" value="<?php echo set_value('bairro_pai_cliente'); ?>" name="bairro_pai_cliente" placeholder="Bairro">
                                <div class="invalid-feedback">
                                    Insira o Bairro
                                </div>
                                <small aviso-texto class="form-text text-muted">
                                    Caso não possua, escrever 'não aplicável'.
                                </small>
                            </div>
                        </div>

                        <div class="col-12 col-sm-12 col-md-4 col-lg-4">
	                        <div aviso-bloco class="form-group">
	                            <label for="complemento_cliente">Cidade:*</label>
	                            <input aviso-campo cep-cidade="pai" class="form-control text-uppercase" type="text" value="" name="" placeholder="Cidade">
	                        </div>
	                        <div class="invalid-feedback">
	                            Insira a Cidade
	                        </div>
	                    </div>

	                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
	                        <div aviso-bloco class="form-group">
	                            <label for="bairro_cliente">Estado:*</label>
	                            <input aviso-campo cep-uf="pai" class="form-control text-uppercase" type="text" value="" name="" placeholder="Estado" >
	                            <div class="invalid-feedback">
	                                Insira o Estado
	                            </div>
	                        </div>
	                    </div>
                    </div>

                    <div class="row">
                    	<div class="col-12 col-sm-12 col-md-4 col-lg-4">
                            <div aviso-bloco class="form-group">
                                <label for="id_pais_residencia_pai">País:*</label>
                                <?php 
                                    $array_pais['']='Selecione o País';
                                    foreach ($paises as $value) {
                                        $array_pais[$value->id_pais]=$value->nome_pais;
                                    }
                                ?>
                                <?php echo form_dropdown('id_pais_residencia_pai', $array_pais, set_value('id_pais_residencia_pai',33), 'class="form-control"') ?>
                                <div class="invalid-feedback">
                                    Selecione um País
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div togvis-id="pai-falecida" togvis-estado='hide' togvis-outros='esconder' togvis-grupo="grupo-pai">
                </div>
            </div>