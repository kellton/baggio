<div id="menu1" class="container tab-pane"><br>

			<div class="row">
                <div class="col">
                    <label>A Ampresa Possui Quantos Sócios ou Diretores?</label><br>
                    <div class="custom-controls-stacked d-block my-3">
                        <label class="custom-control custom-radio">
                            <input name="socios" value="1" togvis-button="socios1" type="radio" class="" <?php if($query['socios']==1) echo 'checked'; ?>>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">1</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input name="socios" value="2" togvis-button="socios2" type="radio" class="" <?php if($query['socios']==2) echo 'checked'; ?>>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">2</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input name="socios" value="3" togvis-button="socios3" type="radio" class="" <?php if($query['socios']==3) echo 'checked'; ?>>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">3</span>
                        </label>
                        <small>Máximo 3 Sócios ou Diretores para o cadastro</small>
                    </div>
                </div>
            </div>
            <div togvis-id="socios1" togvis-estado='hide' togvis-outros='esconder' togvis-grupo='grupo-socios'>
                
<!---------------------------------------------------------------------------------------------------------------------->
	<hr>
<!---------------------------------------------------------------------------------------------------------------------->

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                        <h5 class="">Dados do 1º Sócio/Diretor</h5></br>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="nome_completo_diretor">Nome Completo:*</label>
                            <input class="form-control text-uppercase" type="text" value="<?php echo set_value('nome_completo_diretor[0]',isset($socios[0]->nome_completo_diretor) ? $socios[0]->nome_completo_diretor : ''); ?>" name="nome_completo_diretor[]" placeholder="Nome Completo">
                            <div class="invalid-feedback">
                                Insira o Nome Completo
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="cargo_diretor">Cargo Exercido:*</label>
                            <input type="text" class="form-control text-uppercase" name="cargo_diretor[]" placeholder="Cargo Exercido" value="<?php echo set_value('cargo_diretor[0]',isset($socios[0]->cargo_diretor) ? $socios[0]->cargo_diretor: '')?>">
                            <div class="invalid-feedback">
                                Insira o Cargo Exercido
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="data_nascimento_diretor">Data de Nascimento:*</label>
                            <input class="form-control" type="text" mascara="data" value="<?php echo set_value('data_nascimento_diretor[0]',isset($socios[0]->data_nascimento_diretor) ? $socios[0]->data_nascimento_diretor: ''); ?>" name="data_nascimento_diretor[]" placeholder="Data de Nascimento">
                            <div class="invalid-feedback">
                                Insira a Data de Nasciimento
                            </div>
                        </div>
                    </div>
                </div>
                
<!---------------------------------------------------------------------------------------------------------------------->

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="nacionalidade_diretor">Nacionalidade*</label>
                            <input class="form-control text-uppercase" type="text" value="<?php echo set_value('nacionalidade_diretor[0]',isset($socios[0]->nacionalidade_diretor) ? $socios[0]->nacionalidade_diretor: ''); ?>" name="nacionalidade_diretor[]" placeholder="Nacionalidade">
                            <div class="invalid-feedback">
                                Insira a Nacionalidade
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="cpf_diretor">CPF:*</label>
                            <input aviso-campo class="form-control" type="text" mascara="cpf" value="<?php echo set_value('cpf_diretor[0]',isset($socios[0]->cpf_diretor) ? $socios[0]->cpf_diretor: ''); ?>" name="cpf_diretor[]" placeholder="CPF">
                            <small aviso-texto class="form-text text-muted" >Caso seja estrangeiro, converse com o consultor ou consultora que está lhe atendendo.</small>
                            <div class="invalid-feedback">
                                Insira o CPF
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="rg_diretor">RG*</label>
                            <input aviso-campo class="form-control" type="text" value="<?php echo set_value('rg_diretor[0]',isset($socios[0]->rg_diretor) ? $socios[0]->rg_diretor: ''); ?>" name="rg_diretor[]" placeholder="RG">
                            <small aviso-texto class="form-text text-muted" >Caso seja estrangeiro, converse com o consultor ou consultora que está lhe atendendo.</small>
                            <div class="invalid-feedback">
                                Insira o RG
                            </div>
                        </div>
                    </div>
                </div>
                
<!---------------------------------------------------------------------------------------------------------------------->

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="id_estado_civil_diretor">Estado Civil:*</label>
                           <select name="id_estado_civil_diretor[]" class="form-control"  togvis-select >
                                <option togvis-esconder-grupo="select-divs" value="3" >Selecione o Estado Civil</option>
                                <option togvis-esconder-grupo="select-divs" value="1" <?php echo isset($socios[0]->id_estado_civil) && $socios[0]->id_estado_civil=="1" ? 'selected="selected"': ''; ?> >Não Informado</option>
                                <option togvis-option="casado" togvis-outros="esconder" value="2" <?php echo isset($socios[0]->id_estado_civil) && $socios[0]->id_estado_civil=="2" ? 'selected="selected"': ''; ?>>Casado(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="3" <?php echo isset($socios[0]->id_estado_civil) && $socios[0]->id_estado_civil=="3" ? 'selected="selected"': ''; ?>>Solteiro(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="4" <?php echo isset($socios[0]->id_estado_civil) && $socios[0]->id_estado_civil=="4" ? 'selected="selected"': ''; ?>>Divorciado(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="5" <?php echo isset($socios[0]->id_estado_civil) && $socios[0]->id_estado_civil=="5" ? 'selected="selected"': ''; ?>>Viúvo(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="6" <?php echo isset($socios[0]->id_estado_civil) && $socios[0]->id_estado_civil=="6" ? 'selected="selected"': ''; ?>>União Estável</option>
                            </select>
                            <div class="invalid-feedback">
                                Insira o Estado Civil
                            </div>
                        </div>
                    </div>
                </div>

                <hr>
                
<!---------------------------------------------------------------------------------------------------------------------->

                <div class="row">

                	<div class="col-12 col-sm-12 col-md-4 col-lg-4">
                       <div class="form-group">
                           <label for="cep_diretor">CEP da Residência:*</label>
                           <input cep-bloco="socio11" class="form-control" type="text" mascara="cep" name="cep_diretor[]" value="<?php echo set_value('cep_diretor[0]',isset($socios[0]->cep_diretor) ? $socios[0]->cep_diretor: ''); ?>" placeholder="CEP">
                           <div class="invalid-feedback">
                               Insira o CEP
                           </div>
                       </div>
                   </div>

                   <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                       <div class="form-group">
                           <label for="logradouro_diretor">Logradouro da Residência:*</label>
                           <input cep-rua="socio11" class="form-control text-uppercase" type="text" value="<?php echo set_value('logradouro_diretor[0]',isset($socios[0]->logradouro_diretor) ? $socios[0]->logradouro_diretor: ''); ?>" name="logradouro_diretor[]" placeholder="Logradouro">
                           <div class="invalid-feedback">
                               Insira o Logradouro
                           </div>
                       </div>
                   </div>
                   
                   <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                       <div class="form-group">
                           <label for="numero_diretor">Número da Residência:*</label>
                           <input class="form-control" type="text" value="<?php echo set_value('numero_diretor[0]',isset($socios[0]->numero_diretor) ? $socios[0]->numero_diretor: ''); ?>" name="numero_diretor[]" placeholder="Número">
                           <div class="invalid-feedback">
                               Insira o Número
                           </div>
                       </div>
                   </div>
               </div>
                
<!---------------------------------------------------------------------------------------------------------------------->

                <div class="row">

                	<div class="col-12 col-sm-12 col-md-4 col-lg-4">
                       <div class="form-group">
                           <label for="complemento_diretor">Complemento da Residência:</label>
                           <input class="form-control text-uppercase" type="text" value="<?php echo set_value('complemento_diretor[0]',isset($socios[0]->complemento_diretor) ? $socios[0]->complemento_diretor: ''); ?>" name="complemento_diretor[]" placeholder="Complemento">
                       </div>
                   </div>

                   <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="bairro_diretor">Bairro da Residência:*</label>
                            <input cep-bairro="socio11" class="form-control text-uppercase" type="text" value="<?php echo set_value('bairro_diretor[0]',isset($socios[0]->bairro_diretor) ? $socios[0]->bairro_diretor: ''); ?>" name="bairro_diretor[]" placeholder="Bairro">
                            <div class="invalid-feedback">
                                Insira o Bairro
                            </div>
                        </div>
                    </div>

                	<div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="complemento_cliente">Cidade:*</label>
                            <input aviso-campo cep-cidade="socio11" class="form-control text-uppercase" type="text" value="" name="" placeholder="Cidade">
                        </div>
                        <div class="invalid-feedback">
                            Insira o Estado
                        </div>
                    </div>
                </div>
                
<!---------------------------------------------------------------------------------------------------------------------->

                <div class="row">
                	<div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="bairro_cliente">Estado:*</label>
                            <input aviso-campo cep-uf="socio11" class="form-control text-uppercase" type="text" value="" name="" placeholder="Estado" >
                            <div class="invalid-feedback">
                                Insira o Estado
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">

                        	<?php 
                            $array_pais['']='Selecione o País';
                            foreach ($paises as $value) {
                                $array_pais[$value->id_pais]=$value->nome_pais;
                            }?>
                            <label for="id_pais_diretor">País:*</label>
                            <?php echo form_dropdown('id_pais_diretor[]', $array_pais, set_value('id_pais_cliente[0]',33), 'class="form-control" ') ?>
                            <div class="invalid-feedback">
                                Selecione um País
                            </div>
                        </div>
                    </div>
                </div>

<!---------------------------------------------------------------------------------------------------------------------->
				<hr>

                <div class="row">
                	<div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="email_pessoal_diretor">E-mail Pessoal:*</label>
                            <input aviso-campo class="form-control" type="text" value="<?php echo set_value('email_pessoal_diretor[0]',isset($socios[0]->email_pessoal_diretor) ? $socios[0]->email_pessoal_diretor: ''); ?>" name="email_pessoal_diretor[]" placeholder="E-mail Comercial">
                            <small aviso-texto class="form-text text-muted" >E-mail que os boletos de aluguel serão enviados.</small>
                            <div class="invalid-feedback">
                                Insira o E-mail Comercial
                            </div>
                        </div>
                    </div>

                	<div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="email_comercial_diretor">E-mail Comercial:*</label>
                            <input aviso-campo class="form-control" type="text" value="<?php echo set_value('email_comercial_diretor[0]',isset($socios[0]->email_comercial_diretor) ? $socios[0]->email_comercial_diretor: ''); ?>" name="email_comercial_diretor[]" placeholder="E-mail Comercial">
                            <small aviso-texto class="form-text text-muted" >E-mail que os boletos de aluguel serão enviados.</small>
                            <div class="invalid-feedback">
                                Insira o E-mail Comercial
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="telefone_diretor">Telefone:*</label>
                            <input class="form-control" type="text" mascara="tel" value="<?php echo set_value('telefone_diretor[0]',isset($socios[0]->telefone_diretor) ? $socios[0]->telefone_diretor: ''); ?>" name="telefone_diretor[]" placeholder="Telefone">
                            <div class="invalid-feedback">
                                Insira o E-mail Comercial
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                
<!---------------------------------------------------------------------------------------------------------------------->
	
<!---------------------------------------------------------------------------------------------------------------------->
			<div togvis-id="socios2" togvis-estado='hide' togvis-outros='esconder' togvis-grupo='grupo-socios'>
                
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                        <h5 class="">Dados do 1º Sócio/Diretor</h5></br>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="nome_completo_diretor">Nome Completo:*</label>
                            <input class="form-control text-uppercase" type="text" value="<?php echo set_value('nome_completo_diretor[0]',isset($socios[0]->nome_completo_diretor) ? $socios[0]->nome_completo_diretor : ''); ?>" name="nome_completo_diretor[]" placeholder="Nome Completo">
                            <div class="invalid-feedback">
                                Insira o Nome Completo
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="cargo_diretor">Cargo Exercido:*</label>
                            <input type="text" class="form-control text-uppercase" name="cargo_diretor[]" placeholder="Cargo Exercido" value="<?php echo set_value('cargo_diretor[0]',isset($socios[0]->cargo_diretor) ? $socios[0]->cargo_diretor: '')?>">
                            <div class="invalid-feedback">
                                Insira o Cargo Exercido
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="data_nascimento_diretor">Data de Nascimento:*</label>
                            <input class="form-control" type="text" mascara="data" value="<?php echo set_value('data_nascimento_diretor[0]',isset($socios[0]->data_nascimento_diretor) ? $socios[0]->data_nascimento_diretor: ''); ?>" name="data_nascimento_diretor[]" placeholder="Data de Nascimento">
                            <div class="invalid-feedback">
                                Insira a Data de Nasciimento
                            </div>
                        </div>
                    </div>
                </div>
                
<!---------------------------------------------------------------------------------------------------------------------->

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="nacionalidade_diretor">Nacionalidade*</label>
                            <input class="form-control text-uppercase" type="text" value="<?php echo set_value('nacionalidade_diretor[0]',isset($socios[0]->nacionalidade_diretor) ? $socios[0]->nacionalidade_diretor: ''); ?>" name="nacionalidade_diretor[]" placeholder="Nacionalidade">
                            <div class="invalid-feedback">
                                Insira a Nacionalidade
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="cpf_diretor">CPF:*</label>
                            <input aviso-campo class="form-control" type="text" mascara="cpf" value="<?php echo set_value('cpf_diretor[0]',isset($socios[0]->cpf_diretor) ? $socios[0]->cpf_diretor: ''); ?>" name="cpf_diretor[]" placeholder="CPF">
                            <small aviso-texto class="form-text text-muted" >Caso seja estrangeiro, converse com o consultor ou consultora que está lhe atendendo.</small>
                            <div class="invalid-feedback">
                                Insira o CPF
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="rg_diretor">RG*</label>
                            <input aviso-campo class="form-control" type="text" value="<?php echo set_value('rg_diretor[0]',isset($socios[0]->rg_diretor) ? $socios[0]->rg_diretor: ''); ?>" name="rg_diretor[]" placeholder="RG">
                            <small aviso-texto class="form-text text-muted" >Caso seja estrangeiro, converse com o consultor ou consultora que está lhe atendendo.</small>
                            <div class="invalid-feedback">
                                Insira o RG
                            </div>
                        </div>
                    </div>
                </div>
                
<!---------------------------------------------------------------------------------------------------------------------->

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="id_estado_civil_diretor">Estado Civil:*</label>
                           <select name="id_estado_civil_diretor[]" class="form-control"  togvis-select >
                                <option togvis-esconder-grupo="select-divs" value="3" >Selecione o Estado Civil</option>
                                <option togvis-esconder-grupo="select-divs" value="1" <?php echo isset($socios[0]->id_estado_civil) && $socios[0]->id_estado_civil=="1" ? 'selected="selected"': ''; ?> >Não Informado</option>
                                <option togvis-option="casado" togvis-outros="esconder" value="2" <?php echo isset($socios[0]->id_estado_civil) && $socios[0]->id_estado_civil=="2" ? 'selected="selected"': ''; ?>>Casado(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="3" <?php echo isset($socios[0]->id_estado_civil) && $socios[0]->id_estado_civil=="3" ? 'selected="selected"': ''; ?>>Solteiro(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="4" <?php echo isset($socios[0]->id_estado_civil) && $socios[0]->id_estado_civil=="4" ? 'selected="selected"': ''; ?>>Divorciado(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="5" <?php echo isset($socios[0]->id_estado_civil) && $socios[0]->id_estado_civil=="5" ? 'selected="selected"': ''; ?>>Viúvo(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="6" <?php echo isset($socios[0]->id_estado_civil) && $socios[0]->id_estado_civil=="6" ? 'selected="selected"': ''; ?>>União Estável</option>
                            </select>
                            <div class="invalid-feedback">
                                Insira o Estado Civil
                            </div>
                        </div>
                    </div>
                </div>

                <hr>
                
<!---------------------------------------------------------------------------------------------------------------------->

                <div class="row">

                	<div class="col-12 col-sm-12 col-md-4 col-lg-4">
                       <div class="form-group">
                           <label for="cep_diretor">CEP da Residência:*</label>
                           <input cep-bloco="socio12" class="form-control" type="text" mascara="cep" name="cep_diretor[]" value="<?php echo set_value('cep_diretor[0]',isset($socios[0]->cep_diretor) ? $socios[0]->cep_diretor: ''); ?>" placeholder="CEP">
                           <div class="invalid-feedback">
                               Insira o CEP
                           </div>
                       </div>
                   </div>

                   <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                       <div class="form-group">
                           <label for="logradouro_diretor">Logradouro da Residência:*</label>
                           <input cep-rua="socio12" class="form-control text-uppercase" type="text" value="<?php echo set_value('logradouro_diretor[0]',isset($socios[0]->logradouro_diretor) ? $socios[0]->logradouro_diretor: ''); ?>" name="logradouro_diretor[]" placeholder="Logradouro">
                           <div class="invalid-feedback">
                               Insira o Logradouro
                           </div>
                       </div>
                   </div>
                   
                   <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                       <div class="form-group">
                           <label for="numero_diretor">Número da Residência:*</label>
                           <input class="form-control" type="text" value="<?php echo set_value('numero_diretor[0]',isset($socios[0]->numero_diretor) ? $socios[0]->numero_diretor: ''); ?>" name="numero_diretor[]" placeholder="Número">
                           <div class="invalid-feedback">
                               Insira o Número
                           </div>
                       </div>
                   </div>
               </div>
                
<!---------------------------------------------------------------------------------------------------------------------->

                <div class="row">

                	<div class="col-12 col-sm-12 col-md-4 col-lg-4">
                       <div class="form-group">
                           <label for="complemento_diretor">Complemento da Residência:</label>
                           <input class="form-control text-uppercase" type="text" value="<?php echo set_value('complemento_diretor[0]',isset($socios[0]->complemento_diretor) ? $socios[0]->complemento_diretor: ''); ?>" name="complemento_diretor[]" placeholder="Complemento">
                       </div>
                   </div>

                   <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="bairro_diretor">Bairro da Residência:*</label>
                            <input cep-bairro="socio12" class="form-control text-uppercase" type="text" value="<?php echo set_value('bairro_diretor[0]',isset($socios[0]->bairro_diretor) ? $socios[0]->bairro_diretor: ''); ?>" name="bairro_diretor[]" placeholder="Bairro">
                            <div class="invalid-feedback">
                                Insira o Bairro
                            </div>
                        </div>
                    </div>

                	<div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="complemento_cliente">Cidade:*</label>
                            <input aviso-campo cep-cidade="socio12" class="form-control text-uppercase" type="text" value="" name="" placeholder="Cidade">
                        </div>
                        <div class="invalid-feedback">
                            Insira o Estado
                        </div>
                    </div>
                </div>
                
<!---------------------------------------------------------------------------------------------------------------------->

                <div class="row">
                	<div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="bairro_cliente">Estado:*</label>
                            <input aviso-campo cep-uf="socio12" class="form-control text-uppercase" type="text" value="" name="" placeholder="Estado" >
                            <div class="invalid-feedback">
                                Insira o Estado
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">

                        	<?php 
                            $array_pais['']='Selecione o País';
                            foreach ($paises as $value) {
                                $array_pais[$value->id_pais]=$value->nome_pais;
                            }?>
                            <label for="id_pais_diretor">País:*</label>
                            <?php echo form_dropdown('id_pais_diretor[]', $array_pais, set_value('id_pais_cliente[0]',33), 'class="form-control" ') ?>
                            <div class="invalid-feedback">
                                Selecione um País
                            </div>
                        </div>
                    </div>
                </div>

<!---------------------------------------------------------------------------------------------------------------------->
				<hr>

                <div class="row">
                	<div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="email_pessoal_diretor">E-mail Pessoal:*</label>
                            <input aviso-campo class="form-control" type="text" value="<?php echo set_value('email_pessoal_diretor[0]',isset($socios[0]->email_pessoal_diretor) ? $socios[0]->email_pessoal_diretor: ''); ?>" name="email_pessoal_diretor[]" placeholder="E-mail Comercial">
                            <small aviso-texto class="form-text text-muted" >E-mail que os boletos de aluguel serão enviados.</small>
                            <div class="invalid-feedback">
                                Insira o E-mail Comercial
                            </div>
                        </div>
                    </div>

                	<div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="email_comercial_diretor">E-mail Comercial:*</label>
                            <input aviso-campo class="form-control" type="text" value="<?php echo set_value('email_comercial_diretor[0]',isset($socios[0]->email_comercial_diretor) ? $socios[0]->email_comercial_diretor: ''); ?>" name="email_comercial_diretor[]" placeholder="E-mail Comercial">
                            <small aviso-texto class="form-text text-muted" >E-mail que os boletos de aluguel serão enviados.</small>
                            <div class="invalid-feedback">
                                Insira o E-mail Comercial
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="telefone_diretor">Telefone:*</label>
                            <input class="form-control" type="text" mascara="tel" value="<?php echo set_value('telefone_diretor[0]',isset($socios[0]->telefone_diretor) ? $socios[0]->telefone_diretor: ''); ?>" name="telefone_diretor[]" placeholder="Telefone">
                            <div class="invalid-feedback">
                                Insira o E-mail Comercial
                            </div>
                        </div>
                    </div>
                </div>      
                
<!---------------------------------------------------------------------------------------------------------------------->
	<hr>
<!---------------------------------------------------------------------------------------------------------------------->
                
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                        <h5 class="">Dados do 2º Sócio/Diretor</h5></br>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="nome_completo_diretor">Nome Completo:*</label>
                            <input class="form-control text-uppercase" type="text" value="<?php echo set_value('nome_completo_diretor[1]',isset($socios[1]->nome_completo_diretor) ? $socios[1]->nome_completo_diretor : ''); ?>" name="nome_completo_diretor[]" placeholder="Nome Completo">
                            <div class="invalid-feedback">
                                Insira o Nome Completo
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="cargo_diretor">Cargo Exercido:*</label>
                            <input type="text" class="form-control text-uppercase" name="cargo_diretor[]" placeholder="Cargo Exercido" value="<?php echo set_value('cargo_diretor[1]',isset($socios[1]->cargo_diretor) ? $socios[1]->cargo_diretor: '')?>">
                            <div class="invalid-feedback">
                                Insira o Cargo Exercido
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="data_nascimento_diretor">Data de Nascimento:*</label>
                            <input class="form-control" type="text" mascara="data" value="<?php echo set_value('data_nascimento_diretor[1]',isset($socios[1]->data_nascimento_diretor) ? $socios[1]->data_nascimento_diretor: ''); ?>" name="data_nascimento_diretor[]" placeholder="Data de Nascimento">
                            <div class="invalid-feedback">
                                Insira a Data de Nasciimento
                            </div>
                        </div>
                    </div>
                </div>
                
<!---------------------------------------------------------------------------------------------------------------------->

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="nacionalidade_diretor">Nacionalidade*</label>
                            <input class="form-control text-uppercase" type="text" value="<?php echo set_value('nacionalidade_diretor[1]',isset($socios[1]->nacionalidade_diretor) ? $socios[1]->nacionalidade_diretor: ''); ?>" name="nacionalidade_diretor[]" placeholder="Nacionalidade">
                            <div class="invalid-feedback">
                                Insira a Nacionalidade
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="cpf_diretor">CPF:*</label>
                            <input aviso-campo class="form-control" type="text" mascara="cpf" value="<?php echo set_value('cpf_diretor[1]',isset($socios[1]->cpf_diretor) ? $socios[1]->cpf_diretor: ''); ?>" name="cpf_diretor[]" placeholder="CPF">
                            <small aviso-texto class="form-text text-muted" >Caso seja estrangeiro, converse com o consultor ou consultora que está lhe atendendo.</small>
                            <div class="invalid-feedback">
                                Insira o CPF
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="rg_diretor">RG*</label>
                            <input aviso-campo class="form-control" type="text" value="<?php echo set_value('rg_diretor[1]',isset($socios[1]->rg_diretor) ? $socios[1]->rg_diretor: ''); ?>" name="rg_diretor[]" placeholder="RG">
                            <small aviso-texto class="form-text text-muted" >Caso seja estrangeiro, converse com o consultor ou consultora que está lhe atendendo.</small>
                            <div class="invalid-feedback">
                                Insira o RG
                            </div>
                        </div>
                    </div>
                </div>
                
<!---------------------------------------------------------------------------------------------------------------------->

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="id_estado_civil_diretor">Estado Civil:*</label>
                           <select name="id_estado_civil_diretor[]" class="form-control"  togvis-select >
                                <option togvis-esconder-grupo="select-divs" value="3" >Selecione o Estado Civil</option>
                                <option togvis-esconder-grupo="select-divs" value="1" <?php echo isset($socios[1]->id_estado_civil) && $socios[1]->id_estado_civil=="1" ? 'selected="selected"': ''; ?> >Não Informado</option>
                                <option togvis-option="casado" togvis-outros="esconder" value="2" <?php echo isset($socios[1]->id_estado_civil) && $socios[1]->id_estado_civil=="2" ? 'selected="selected"': ''; ?>>Casado(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="3" <?php echo isset($socios[1]->id_estado_civil) && $socios[1]->id_estado_civil=="3" ? 'selected="selected"': ''; ?>>Solteiro(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="4" <?php echo isset($socios[1]->id_estado_civil) && $socios[1]->id_estado_civil=="4" ? 'selected="selected"': ''; ?>>Divorciado(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="5" <?php echo isset($socios[1]->id_estado_civil) && $socios[1]->id_estado_civil=="5" ? 'selected="selected"': ''; ?>>Viúvo(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="6" <?php echo isset($socios[1]->id_estado_civil) && $socios[1]->id_estado_civil=="6" ? 'selected="selected"': ''; ?>>União Estável</option>
                            </select>
                            <div class="invalid-feedback">
                                Insira o Estado Civil
                            </div>
                        </div>
                    </div>
                </div>

                <hr>
                
<!---------------------------------------------------------------------------------------------------------------------->

                <div class="row">

                	<div class="col-12 col-sm-12 col-md-4 col-lg-4">
                       <div class="form-group">
                           <label for="cep_diretor">CEP da Residência:*</label>
                           <input cep-bloco="socio21" class="form-control" type="text" mascara="cep" name="cep_diretor[]" value="<?php echo set_value('cep_diretor[1]',isset($socios[1]->cep_diretor) ? $socios[1]->cep_diretor: ''); ?>" placeholder="CEP">
                           <div class="invalid-feedback">
                               Insira o CEP
                           </div>
                       </div>
                   </div>

                   <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                       <div class="form-group">
                           <label for="logradouro_diretor">Logradouro da Residência:*</label>
                           <input cep-rua="socio21" class="form-control text-uppercase" type="text" value="<?php echo set_value('logradouro_diretor[1]',isset($socios[1]->logradouro_diretor) ? $socios[1]->logradouro_diretor: ''); ?>" name="logradouro_diretor[]" placeholder="Logradouro">
                           <div class="invalid-feedback">
                               Insira o Logradouro
                           </div>
                       </div>
                   </div>
                   
                   <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                       <div class="form-group">
                           <label for="numero_diretor">Número da Residência:*</label>
                           <input class="form-control" type="text" value="<?php echo set_value('numero_diretor[1]',isset($socios[1]->numero_diretor) ? $socios[1]->numero_diretor: ''); ?>" name="numero_diretor[]" placeholder="Número">
                           <div class="invalid-feedback">
                               Insira o Número
                           </div>
                       </div>
                   </div>
               </div>
                
<!---------------------------------------------------------------------------------------------------------------------->

                <div class="row">

                	<div class="col-12 col-sm-12 col-md-4 col-lg-4">
                       <div class="form-group">
                           <label for="complemento_diretor">Complemento da Residência:</label>
                           <input class="form-control text-uppercase" type="text" value="<?php echo set_value('complemento_diretor[1]',isset($socios[1]->complemento_diretor) ? $socios[1]->complemento_diretor: ''); ?>" name="complemento_diretor[]" placeholder="Complemento">
                       </div>
                   </div>

                   <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="bairro_diretor">Bairro da Residência:*</label>
                            <input cep-bairro="socio21" class="form-control text-uppercase" type="text" value="<?php echo set_value('bairro_diretor[1]',isset($socios[1]->bairro_diretor) ? $socios[1]->bairro_diretor: ''); ?>" name="bairro_diretor[]" placeholder="Bairro">
                            <div class="invalid-feedback">
                                Insira o Bairro
                            </div>
                        </div>
                    </div>

                	<div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="complemento_cliente">Cidade:*</label>
                            <input aviso-campo cep-cidade="socio21" class="form-control text-uppercase" type="text" value="" name="" placeholder="Cidade">
                        </div>
                        <div class="invalid-feedback">
                            Insira o Estado
                        </div>
                    </div>
                </div>
                
<!---------------------------------------------------------------------------------------------------------------------->

                <div class="row">
                	<div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="bairro_cliente">Estado:*</label>
                            <input aviso-campo cep-uf="socio21" class="form-control text-uppercase" type="text" value="" name="" placeholder="Estado" >
                            <div class="invalid-feedback">
                                Insira o Estado
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">

                        	<?php 
                            $array_pais['']='Selecione o País';
                            foreach ($paises as $value) {
                                $array_pais[$value->id_pais]=$value->nome_pais;
                            }?>
                            <label for="id_pais_diretor">País:*</label>
                            <?php echo form_dropdown('id_pais_diretor[]', $array_pais, set_value('id_pais_cliente[1]',33), 'class="form-control" ') ?>
                            <div class="invalid-feedback">
                                Selecione um País
                            </div>
                        </div>
                    </div>
                </div>

<!---------------------------------------------------------------------------------------------------------------------->
				<hr>

                <div class="row">
                	<div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="email_pessoal_diretor">E-mail Pessoal:*</label>
                            <input aviso-campo class="form-control" type="text" value="<?php echo set_value('email_pessoal_diretor[1]',isset($socios[1]->email_pessoal_diretor) ? $socios[1]->email_pessoal_diretor: ''); ?>" name="email_pessoal_diretor[]" placeholder="E-mail Comercial">
                            <small aviso-texto class="form-text text-muted" >E-mail que os boletos de aluguel serão enviados.</small>
                            <div class="invalid-feedback">
                                Insira o E-mail Comercial
                            </div>
                        </div>
                    </div>

                	<div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="email_comercial_diretor">E-mail Comercial:*</label>
                            <input aviso-campo class="form-control" type="text" value="<?php echo set_value('email_comercial_diretor[1]',isset($socios[1]->email_comercial_diretor) ? $socios[1]->email_comercial_diretor: ''); ?>" name="email_comercial_diretor[]" placeholder="E-mail Comercial">
                            <small aviso-texto class="form-text text-muted" >E-mail que os boletos de aluguel serão enviados.</small>
                            <div class="invalid-feedback">
                                Insira o E-mail Comercial
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="telefone_diretor">Telefone:*</label>
                            <input class="form-control" type="text" mascara="tel" value="<?php echo set_value('telefone_diretor[1]',isset($socios[1]->telefone_diretor) ? $socios[1]->telefone_diretor: ''); ?>" name="telefone_diretor[]" placeholder="Telefone">
                            <div class="invalid-feedback">
                                Insira o E-mail Comercial
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
               
<!---------------------------------------------------------------------------------------------------------------------->
	
<!---------------------------------------------------------------------------------------------------------------------->

			<div togvis-id="socios3" togvis-estado='hide'  togvis-outros='esconder' togvis-grupo='grupo-socios'>
               
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                        <h5 class="">Dados do 1º Sócio/Diretor</h5></br>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="nome_completo_diretor">Nome Completo:*</label>
                            <input class="form-control text-uppercase" type="text" value="<?php echo set_value('nome_completo_diretor[0]',isset($socios[0]->nome_completo_diretor) ? $socios[0]->nome_completo_diretor : ''); ?>" name="nome_completo_diretor[]" placeholder="Nome Completo">
                            <div class="invalid-feedback">
                                Insira o Nome Completo
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="cargo_diretor">Cargo Exercido:*</label>
                            <input type="text" class="form-control text-uppercase" name="cargo_diretor[]" placeholder="Cargo Exercido" value="<?php echo set_value('cargo_diretor[0]',isset($socios[0]->cargo_diretor) ? $socios[0]->cargo_diretor: '')?>">
                            <div class="invalid-feedback">
                                Insira o Cargo Exercido
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="data_nascimento_diretor">Data de Nascimento:*</label>
                            <input class="form-control" type="text" mascara="data" value="<?php echo set_value('data_nascimento_diretor[0]',isset($socios[0]->data_nascimento_diretor) ? $socios[0]->data_nascimento_diretor: ''); ?>" name="data_nascimento_diretor[]" placeholder="Data de Nascimento">
                            <div class="invalid-feedback">
                                Insira a Data de Nasciimento
                            </div>
                        </div>
                    </div>
                </div>
                
<!---------------------------------------------------------------------------------------------------------------------->

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="nacionalidade_diretor">Nacionalidade*</label>
                            <input class="form-control text-uppercase" type="text" value="<?php echo set_value('nacionalidade_diretor[0]',isset($socios[0]->nacionalidade_diretor) ? $socios[0]->nacionalidade_diretor: ''); ?>" name="nacionalidade_diretor[]" placeholder="Nacionalidade">
                            <div class="invalid-feedback">
                                Insira a Nacionalidade
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="cpf_diretor">CPF:*</label>
                            <input aviso-campo class="form-control" type="text" mascara="cpf" value="<?php echo set_value('cpf_diretor[0]',isset($socios[0]->cpf_diretor) ? $socios[0]->cpf_diretor: ''); ?>" name="cpf_diretor[]" placeholder="CPF">
                            <small aviso-texto class="form-text text-muted" >Caso seja estrangeiro, converse com o consultor ou consultora que está lhe atendendo.</small>
                            <div class="invalid-feedback">
                                Insira o CPF
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="rg_diretor">RG*</label>
                            <input aviso-campo class="form-control" type="text" value="<?php echo set_value('rg_diretor[0]',isset($socios[0]->rg_diretor) ? $socios[0]->rg_diretor: ''); ?>" name="rg_diretor[]" placeholder="RG">
                            <small aviso-texto class="form-text text-muted" >Caso seja estrangeiro, converse com o consultor ou consultora que está lhe atendendo.</small>
                            <div class="invalid-feedback">
                                Insira o RG
                            </div>
                        </div>
                    </div>
                </div>
                
<!---------------------------------------------------------------------------------------------------------------------->

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="id_estado_civil_diretor">Estado Civil:*</label>
                           <select name="id_estado_civil_diretor[]" class="form-control"  togvis-select >
                                <option togvis-esconder-grupo="select-divs" value="3" >Selecione o Estado Civil</option>
                                <option togvis-esconder-grupo="select-divs" value="1" <?php echo isset($socios[0]->id_estado_civil) && $socios[0]->id_estado_civil=="1" ? 'selected="selected"': ''; ?> >Não Informado</option>
                                <option togvis-option="casado" togvis-outros="esconder" value="2" <?php echo isset($socios[0]->id_estado_civil) && $socios[0]->id_estado_civil=="2" ? 'selected="selected"': ''; ?>>Casado(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="3" <?php echo isset($socios[0]->id_estado_civil) && $socios[0]->id_estado_civil=="3" ? 'selected="selected"': ''; ?>>Solteiro(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="4" <?php echo isset($socios[0]->id_estado_civil) && $socios[0]->id_estado_civil=="4" ? 'selected="selected"': ''; ?>>Divorciado(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="5" <?php echo isset($socios[0]->id_estado_civil) && $socios[0]->id_estado_civil=="5" ? 'selected="selected"': ''; ?>>Viúvo(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="6" <?php echo isset($socios[0]->id_estado_civil) && $socios[0]->id_estado_civil=="6" ? 'selected="selected"': ''; ?>>União Estável</option>
                            </select>
                            <div class="invalid-feedback">
                                Insira o Estado Civil
                            </div>
                        </div>
                    </div>
                </div>

                <hr>
                
<!---------------------------------------------------------------------------------------------------------------------->

                <div class="row">

                	<div class="col-12 col-sm-12 col-md-4 col-lg-4">
                       <div class="form-group">
                           <label for="cep_diretor">CEP da Residência:*</label>
                           <input cep-bloco="socio13" class="form-control" type="text" mascara="cep" name="cep_diretor[]" value="<?php echo set_value('cep_diretor[0]',isset($socios[0]->cep_diretor) ? $socios[0]->cep_diretor: ''); ?>" placeholder="CEP">
                           <div class="invalid-feedback">
                               Insira o CEP
                           </div>
                       </div>
                   </div>

                   <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                       <div class="form-group">
                           <label for="logradouro_diretor">Logradouro da Residência:*</label>
                           <input cep-rua="socio13" class="form-control text-uppercase" type="text" value="<?php echo set_value('logradouro_diretor[0]',isset($socios[0]->logradouro_diretor) ? $socios[0]->logradouro_diretor: ''); ?>" name="logradouro_diretor[]" placeholder="Logradouro">
                           <div class="invalid-feedback">
                               Insira o Logradouro
                           </div>
                       </div>
                   </div>
                   
                   <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                       <div class="form-group">
                           <label for="numero_diretor">Número da Residência:*</label>
                           <input class="form-control" type="text" value="<?php echo set_value('numero_diretor[0]',isset($socios[0]->numero_diretor) ? $socios[0]->numero_diretor: ''); ?>" name="numero_diretor[]" placeholder="Número">
                           <div class="invalid-feedback">
                               Insira o Número
                           </div>
                       </div>
                   </div>
               </div>
                
<!---------------------------------------------------------------------------------------------------------------------->

                <div class="row">

                	<div class="col-12 col-sm-12 col-md-4 col-lg-4">
                       <div class="form-group">
                           <label for="complemento_diretor">Complemento da Residência:</label>
                           <input class="form-control text-uppercase" type="text" value="<?php echo set_value('complemento_diretor[0]',isset($socios[0]->complemento_diretor) ? $socios[0]->complemento_diretor: ''); ?>" name="complemento_diretor[]" placeholder="Complemento">
                       </div>
                   </div>

                   <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="bairro_diretor">Bairro da Residência:*</label>
                            <input cep-bairro="socio13" class="form-control text-uppercase" type="text" value="<?php echo set_value('bairro_diretor[0]',isset($socios[0]->bairro_diretor) ? $socios[0]->bairro_diretor: ''); ?>" name="bairro_diretor[]" placeholder="Bairro">
                            <div class="invalid-feedback">
                                Insira o Bairro
                            </div>
                        </div>
                    </div>

                	<div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="complemento_cliente">Cidade:*</label>
                            <input aviso-campo cep-cidade="socio13" class="form-control text-uppercase" type="text" value="" name="" placeholder="Cidade">
                        </div>
                        <div class="invalid-feedback">
                            Insira o Estado
                        </div>
                    </div>
                </div>
                
<!---------------------------------------------------------------------------------------------------------------------->

                <div class="row">
                	<div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="bairro_cliente">Estado:*</label>
                            <input aviso-campo cep-uf="socio13" class="form-control text-uppercase" type="text" value="" name="" placeholder="Estado" >
                            <div class="invalid-feedback">
                                Insira o Estado
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">

                        	<?php 
                            $array_pais['']='Selecione o País';
                            foreach ($paises as $value) {
                                $array_pais[$value->id_pais]=$value->nome_pais;
                            }?>
                            <label for="id_pais_diretor">País:*</label>
                            <?php echo form_dropdown('id_pais_diretor[]', $array_pais, set_value('id_pais_cliente[0]',33), 'class="form-control" ') ?>
                            <div class="invalid-feedback">
                                Selecione um País
                            </div>
                        </div>
                    </div>
                </div>

<!---------------------------------------------------------------------------------------------------------------------->
				<hr>

                <div class="row">
                	<div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="email_pessoal_diretor">E-mail Pessoal:*</label>
                            <input aviso-campo class="form-control" type="text" value="<?php echo set_value('email_pessoal_diretor[0]',isset($socios[0]->email_pessoal_diretor) ? $socios[0]->email_pessoal_diretor: ''); ?>" name="email_pessoal_diretor[]" placeholder="E-mail Comercial">
                            <small aviso-texto class="form-text text-muted" >E-mail que os boletos de aluguel serão enviados.</small>
                            <div class="invalid-feedback">
                                Insira o E-mail Comercial
                            </div>
                        </div>
                    </div>

                	<div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="email_comercial_diretor">E-mail Comercial:*</label>
                            <input aviso-campo class="form-control" type="text" value="<?php echo set_value('email_comercial_diretor[0]',isset($socios[0]->email_comercial_diretor) ? $socios[0]->email_comercial_diretor: ''); ?>" name="email_comercial_diretor[]" placeholder="E-mail Comercial">
                            <small aviso-texto class="form-text text-muted" >E-mail que os boletos de aluguel serão enviados.</small>
                            <div class="invalid-feedback">
                                Insira o E-mail Comercial
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="telefone_diretor">Telefone:*</label>
                            <input class="form-control" type="text" mascara="tel" value="<?php echo set_value('telefone_diretor[0]',isset($socios[0]->telefone_diretor) ? $socios[0]->telefone_diretor: ''); ?>" name="telefone_diretor[]" placeholder="Telefone">
                            <div class="invalid-feedback">
                                Insira o E-mail Comercial
                            </div>
                        </div>
                    </div>
                </div>
                
<!---------------------------------------------------------------------------------------------------------------------->
	<hr>
<!---------------------------------------------------------------------------------------------------------------------->

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                        <h5 class="">Dados do 2º Sócio/Diretor</h5></br>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="nome_completo_diretor">Nome Completo:*</label>
                            <input class="form-control text-uppercase" type="text" value="<?php echo set_value('nome_completo_diretor[1]',isset($socios[1]->nome_completo_diretor) ? $socios[1]->nome_completo_diretor : ''); ?>" name="nome_completo_diretor[]" placeholder="Nome Completo">
                            <div class="invalid-feedback">
                                Insira o Nome Completo
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="cargo_diretor">Cargo Exercido:*</label>
                            <input type="text" class="form-control text-uppercase" name="cargo_diretor[]" placeholder="Cargo Exercido" value="<?php echo set_value('cargo_diretor[1]',isset($socios[1]->cargo_diretor) ? $socios[1]->cargo_diretor: '')?>">
                            <div class="invalid-feedback">
                                Insira o Cargo Exercido
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="data_nascimento_diretor">Data de Nascimento:*</label>
                            <input class="form-control" type="text" mascara="data" value="<?php echo set_value('data_nascimento_diretor[1]',isset($socios[1]->data_nascimento_diretor) ? $socios[1]->data_nascimento_diretor: ''); ?>" name="data_nascimento_diretor[]" placeholder="Data de Nascimento">
                            <div class="invalid-feedback">
                                Insira a Data de Nasciimento
                            </div>
                        </div>
                    </div>
                </div>
                
<!---------------------------------------------------------------------------------------------------------------------->

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="nacionalidade_diretor">Nacionalidade*</label>
                            <input class="form-control text-uppercase" type="text" value="<?php echo set_value('nacionalidade_diretor[1]',isset($socios[1]->nacionalidade_diretor) ? $socios[1]->nacionalidade_diretor: ''); ?>" name="nacionalidade_diretor[]" placeholder="Nacionalidade">
                            <div class="invalid-feedback">
                                Insira a Nacionalidade
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="cpf_diretor">CPF:*</label>
                            <input aviso-campo class="form-control" type="text" mascara="cpf" value="<?php echo set_value('cpf_diretor[1]',isset($socios[1]->cpf_diretor) ? $socios[1]->cpf_diretor: ''); ?>" name="cpf_diretor[]" placeholder="CPF">
                            <small aviso-texto class="form-text text-muted" >Caso seja estrangeiro, converse com o consultor ou consultora que está lhe atendendo.</small>
                            <div class="invalid-feedback">
                                Insira o CPF
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="rg_diretor">RG*</label>
                            <input aviso-campo class="form-control" type="text" value="<?php echo set_value('rg_diretor[1]',isset($socios[1]->rg_diretor) ? $socios[1]->rg_diretor: ''); ?>" name="rg_diretor[]" placeholder="RG">
                            <small aviso-texto class="form-text text-muted" >Caso seja estrangeiro, converse com o consultor ou consultora que está lhe atendendo.</small>
                            <div class="invalid-feedback">
                                Insira o RG
                            </div>
                        </div>
                    </div>
                </div>
                
<!---------------------------------------------------------------------------------------------------------------------->

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="id_estado_civil_diretor">Estado Civil:*</label>
                           <select name="id_estado_civil_diretor[]" class="form-control"  togvis-select >
                                <option togvis-esconder-grupo="select-divs" value="3" >Selecione o Estado Civil</option>
                                <option togvis-esconder-grupo="select-divs" value="1" <?php echo isset($socios[1]->id_estado_civil) && $socios[1]->id_estado_civil=="1" ? 'selected="selected"': ''; ?> >Não Informado</option>
                                <option togvis-option="casado" togvis-outros="esconder" value="2" <?php echo isset($socios[1]->id_estado_civil) && $socios[1]->id_estado_civil=="2" ? 'selected="selected"': ''; ?>>Casado(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="3" <?php echo isset($socios[1]->id_estado_civil) && $socios[1]->id_estado_civil=="3" ? 'selected="selected"': ''; ?>>Solteiro(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="4" <?php echo isset($socios[1]->id_estado_civil) && $socios[1]->id_estado_civil=="4" ? 'selected="selected"': ''; ?>>Divorciado(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="5" <?php echo isset($socios[1]->id_estado_civil) && $socios[1]->id_estado_civil=="5" ? 'selected="selected"': ''; ?>>Viúvo(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="6" <?php echo isset($socios[1]->id_estado_civil) && $socios[1]->id_estado_civil=="6" ? 'selected="selected"': ''; ?>>União Estável</option>
                            </select>
                            <div class="invalid-feedback">
                                Insira o Estado Civil
                            </div>
                        </div>
                    </div>
                </div>

                <hr>
                
<!---------------------------------------------------------------------------------------------------------------------->

                <div class="row">

                	<div class="col-12 col-sm-12 col-md-4 col-lg-4">
                       <div class="form-group">
                           <label for="cep_diretor">CEP da Residência:*</label>
                           <input cep-bloco="socio22" class="form-control" type="text" mascara="cep" name="cep_diretor[]" value="<?php echo set_value('cep_diretor[1]',isset($socios[1]->cep_diretor) ? $socios[1]->cep_diretor: ''); ?>" placeholder="CEP">
                           <div class="invalid-feedback">
                               Insira o CEP
                           </div>
                       </div>
                   </div>

                   <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                       <div class="form-group">
                           <label for="logradouro_diretor">Logradouro da Residência:*</label>
                           <input cep-rua="socio22" class="form-control text-uppercase" type="text" value="<?php echo set_value('logradouro_diretor[1]',isset($socios[1]->logradouro_diretor) ? $socios[1]->logradouro_diretor: ''); ?>" name="logradouro_diretor[]" placeholder="Logradouro">
                           <div class="invalid-feedback">
                               Insira o Logradouro
                           </div>
                       </div>
                   </div>
                   
                   <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                       <div class="form-group">
                           <label for="numero_diretor">Número da Residência:*</label>
                           <input class="form-control" type="text" value="<?php echo set_value('numero_diretor[1]',isset($socios[1]->numero_diretor) ? $socios[1]->numero_diretor: ''); ?>" name="numero_diretor[]" placeholder="Número">
                           <div class="invalid-feedback">
                               Insira o Número
                           </div>
                       </div>
                   </div>
               </div>
                
<!---------------------------------------------------------------------------------------------------------------------->

                <div class="row">

                	<div class="col-12 col-sm-12 col-md-4 col-lg-4">
                       <div class="form-group">
                           <label for="complemento_diretor">Complemento da Residência:</label>
                           <input class="form-control text-uppercase" type="text" value="<?php echo set_value('complemento_diretor[1]',isset($socios[1]->complemento_diretor) ? $socios[1]->complemento_diretor: ''); ?>" name="complemento_diretor[]" placeholder="Complemento">
                       </div>
                   </div>

                   <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="bairro_diretor">Bairro da Residência:*</label>
                            <input cep-bairro="socio22" class="form-control text-uppercase" type="text" value="<?php echo set_value('bairro_diretor[1]',isset($socios[1]->bairro_diretor) ? $socios[1]->bairro_diretor: ''); ?>" name="bairro_diretor[]" placeholder="Bairro">
                            <div class="invalid-feedback">
                                Insira o Bairro
                            </div>
                        </div>
                    </div>

                	<div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="complemento_cliente">Cidade:*</label>
                            <input aviso-campo cep-cidade="socio22" class="form-control text-uppercase" type="text" value="" name="" placeholder="Cidade">
                        </div>
                        <div class="invalid-feedback">
                            Insira o Estado
                        </div>
                    </div>
                </div>
                
<!---------------------------------------------------------------------------------------------------------------------->

                <div class="row">
                	<div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="bairro_cliente">Estado:*</label>
                            <input aviso-campo cep-uf="socio22" class="form-control text-uppercase" type="text" value="" name="" placeholder="Estado" >
                            <div class="invalid-feedback">
                                Insira o Estado
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">

                        	<?php 
                            $array_pais['']='Selecione o País';
                            foreach ($paises as $value) {
                                $array_pais[$value->id_pais]=$value->nome_pais;
                            }?>
                            <label for="id_pais_diretor">País:*</label>
                            <?php echo form_dropdown('id_pais_diretor[]', $array_pais, set_value('id_pais_cliente[1]',33), 'class="form-control" ') ?>
                            <div class="invalid-feedback">
                                Selecione um País
                            </div>
                        </div>
                    </div>
                </div>

<!---------------------------------------------------------------------------------------------------------------------->
				<hr>

                <div class="row">
                	<div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="email_pessoal_diretor">E-mail Pessoal:*</label>
                            <input aviso-campo class="form-control" type="text" value="<?php echo set_value('email_pessoal_diretor[1]',isset($socios[1]->email_pessoal_diretor) ? $socios[1]->email_pessoal_diretor: ''); ?>" name="email_pessoal_diretor[]" placeholder="E-mail Comercial">
                            <small aviso-texto class="form-text text-muted" >E-mail que os boletos de aluguel serão enviados.</small>
                            <div class="invalid-feedback">
                                Insira o E-mail Comercial
                            </div>
                        </div>
                    </div>

                	<div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="email_comercial_diretor">E-mail Comercial:*</label>
                            <input aviso-campo class="form-control" type="text" value="<?php echo set_value('email_comercial_diretor[1]',isset($socios[1]->email_comercial_diretor) ? $socios[1]->email_comercial_diretor: ''); ?>" name="email_comercial_diretor[]" placeholder="E-mail Comercial">
                            <small aviso-texto class="form-text text-muted" >E-mail que os boletos de aluguel serão enviados.</small>
                            <div class="invalid-feedback">
                                Insira o E-mail Comercial
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="telefone_diretor">Telefone:*</label>
                            <input class="form-control" type="text" mascara="tel" value="<?php echo set_value('telefone_diretor[1]',isset($socios[1]->telefone_diretor) ? $socios[1]->telefone_diretor: ''); ?>" name="telefone_diretor[]" placeholder="Telefone">
                            <div class="invalid-feedback">
                                Insira o E-mail Comercial
                            </div>
                        </div>
                    </div>
                </div>
                 
<!---------------------------------------------------------------------------------------------------------------------->
	<hr>
<!---------------------------------------------------------------------------------------------------------------------->

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                        <h5 class="">Dados do 3º Sócio/Diretor</h5></br>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="nome_completo_diretor">Nome Completo:*</label>
                            <input class="form-control text-uppercase" type="text" value="<?php echo set_value('nome_completo_diretor[2]',isset($socios[2]->nome_completo_diretor) ? $socios[2]->nome_completo_diretor : ''); ?>" name="nome_completo_diretor[]" placeholder="Nome Completo">
                            <div class="invalid-feedback">
                                Insira o Nome Completo
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="cargo_diretor">Cargo Exercido:*</label>
                            <input type="text" class="form-control text-uppercase" name="cargo_diretor[]" placeholder="Cargo Exercido" value="<?php echo set_value('cargo_diretor[2]',isset($socios[2]->cargo_diretor) ? $socios[2]->cargo_diretor: '')?>">
                            <div class="invalid-feedback">
                                Insira o Cargo Exercido
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="data_nascimento_diretor">Data de Nascimento:*</label>
                            <input class="form-control" type="text" mascara="data" value="<?php echo set_value('data_nascimento_diretor[2]',isset($socios[2]->data_nascimento_diretor) ? $socios[2]->data_nascimento_diretor: ''); ?>" name="data_nascimento_diretor[]" placeholder="Data de Nascimento">
                            <div class="invalid-feedback">
                                Insira a Data de Nasciimento
                            </div>
                        </div>
                    </div>
                </div>
                
<!---------------------------------------------------------------------------------------------------------------------->

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="nacionalidade_diretor">Nacionalidade*</label>
                            <input class="form-control text-uppercase" type="text" value="<?php echo set_value('nacionalidade_diretor[2]',isset($socios[2]->nacionalidade_diretor) ? $socios[2]->nacionalidade_diretor: ''); ?>" name="nacionalidade_diretor[]" placeholder="Nacionalidade">
                            <div class="invalid-feedback">
                                Insira a Nacionalidade
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="cpf_diretor">CPF:*</label>
                            <input aviso-campo class="form-control" type="text" mascara="cpf" value="<?php echo set_value('cpf_diretor[2]',isset($socios[2]->cpf_diretor) ? $socios[2]->cpf_diretor: ''); ?>" name="cpf_diretor[]" placeholder="CPF">
                            <small aviso-texto class="form-text text-muted" >Caso seja estrangeiro, converse com o consultor ou consultora que está lhe atendendo.</small>
                            <div class="invalid-feedback">
                                Insira o CPF
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="rg_diretor">RG*</label>
                            <input aviso-campo class="form-control" type="text" value="<?php echo set_value('rg_diretor[2]',isset($socios[2]->rg_diretor) ? $socios[2]->rg_diretor: ''); ?>" name="rg_diretor[]" placeholder="RG">
                            <small aviso-texto class="form-text text-muted" >Caso seja estrangeiro, converse com o consultor ou consultora que está lhe atendendo.</small>
                            <div class="invalid-feedback">
                                Insira o RG
                            </div>
                        </div>
                    </div>
                </div>
                
<!---------------------------------------------------------------------------------------------------------------------->

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="id_estado_civil_diretor">Estado Civil:*</label>
                           <select name="id_estado_civil_diretor[]" class="form-control"  togvis-select >
                                <option togvis-esconder-grupo="select-divs" value="3" >Selecione o Estado Civil</option>
                                <option togvis-esconder-grupo="select-divs" value="1" <?php echo isset($socios[2]->id_estado_civil) && $socios[2]->id_estado_civil=="1" ? 'selected="selected"': ''; ?> >Não Informado</option>
                                <option togvis-option="casado" togvis-outros="esconder" value="2" <?php echo isset($socios[2]->id_estado_civil) && $socios[2]->id_estado_civil=="2" ? 'selected="selected"': ''; ?>>Casado(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="3" <?php echo isset($socios[2]->id_estado_civil) && $socios[2]->id_estado_civil=="3" ? 'selected="selected"': ''; ?>>Solteiro(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="4" <?php echo isset($socios[2]->id_estado_civil) && $socios[2]->id_estado_civil=="4" ? 'selected="selected"': ''; ?>>Divorciado(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="5" <?php echo isset($socios[2]->id_estado_civil) && $socios[2]->id_estado_civil=="5" ? 'selected="selected"': ''; ?>>Viúvo(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="6" <?php echo isset($socios[2]->id_estado_civil) && $socios[2]->id_estado_civil=="6" ? 'selected="selected"': ''; ?>>União Estável</option>
                            </select>
                            <div class="invalid-feedback">
                                Insira o Estado Civil
                            </div>
                        </div>
                    </div>
                </div>

                <hr>
                
<!---------------------------------------------------------------------------------------------------------------------->

                <div class="row">

                	<div class="col-12 col-sm-12 col-md-4 col-lg-4">
                       <div class="form-group">
                           <label for="cep_diretor">CEP da Residência:*</label>
                           <input cep-bloco="socio31" class="form-control" type="text" mascara="cep" name="cep_diretor[]" value="<?php echo set_value('cep_diretor[2]',isset($socios[2]->cep_diretor) ? $socios[2]->cep_diretor: ''); ?>" placeholder="CEP">
                           <div class="invalid-feedback">
                               Insira o CEP
                           </div>
                       </div>
                   </div>

                   <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                       <div class="form-group">
                           <label for="logradouro_diretor">Logradouro da Residência:*</label>
                           <input cep-rua="socio31" class="form-control text-uppercase" type="text" value="<?php echo set_value('logradouro_diretor[2]',isset($socios[2]->logradouro_diretor) ? $socios[2]->logradouro_diretor: ''); ?>" name="logradouro_diretor[]" placeholder="Logradouro">
                           <div class="invalid-feedback">
                               Insira o Logradouro
                           </div>
                       </div>
                   </div>
                   
                   <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                       <div class="form-group">
                           <label for="numero_diretor">Número da Residência:*</label>
                           <input class="form-control" type="text" value="<?php echo set_value('numero_diretor[2]',isset($socios[2]->numero_diretor) ? $socios[2]->numero_diretor: ''); ?>" name="numero_diretor[]" placeholder="Número">
                           <div class="invalid-feedback">
                               Insira o Número
                           </div>
                       </div>
                   </div>
               </div>
                
<!---------------------------------------------------------------------------------------------------------------------->

                <div class="row">

                	<div class="col-12 col-sm-12 col-md-4 col-lg-4">
                       <div class="form-group">
                           <label for="complemento_diretor">Complemento da Residência:</label>
                           <input class="form-control text-uppercase" type="text" value="<?php echo set_value('complemento_diretor[2]',isset($socios[2]->complemento_diretor) ? $socios[2]->complemento_diretor: ''); ?>" name="complemento_diretor[]" placeholder="Complemento">
                       </div>
                   </div>

                   <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="bairro_diretor">Bairro da Residência:*</label>
                            <input cep-bairro="socio31" class="form-control text-uppercase" type="text" value="<?php echo set_value('bairro_diretor[2]',isset($socios[2]->bairro_diretor) ? $socios[2]->bairro_diretor: ''); ?>" name="bairro_diretor[]" placeholder="Bairro">
                            <div class="invalid-feedback">
                                Insira o Bairro
                            </div>
                        </div>
                    </div>

                	<div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="complemento_cliente">Cidade:*</label>
                            <input aviso-campo cep-cidade="socio31" class="form-control text-uppercase" type="text" value="" name="" placeholder="Cidade">
                        </div>
                        <div class="invalid-feedback">
                            Insira o Estado
                        </div>
                    </div>
                </div>
                
<!---------------------------------------------------------------------------------------------------------------------->

                <div class="row">
                	<div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="bairro_cliente">Estado:*</label>
                            <input aviso-campo cep-uf="socio31" class="form-control text-uppercase" type="text" value="" name="" placeholder="Estado" >
                            <div class="invalid-feedback">
                                Insira o Estado
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">

                        	<?php 
                            $array_pais['']='Selecione o País';
                            foreach ($paises as $value) {
                                $array_pais[$value->id_pais]=$value->nome_pais;
                            }?>
                            <label for="id_pais_diretor">País:*</label>
                            <?php echo form_dropdown('id_pais_diretor[]', $array_pais, set_value('id_pais_cliente[2]',33), 'class="form-control" ') ?>
                            <div class="invalid-feedback">
                                Selecione um País
                            </div>
                        </div>
                    </div>
                </div>

<!---------------------------------------------------------------------------------------------------------------------->
				<hr>

                <div class="row">
                	<div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="email_pessoal_diretor">E-mail Pessoal:*</label>
                            <input aviso-campo class="form-control" type="text" value="<?php echo set_value('email_pessoal_diretor[2]',isset($socios[2]->email_pessoal_diretor) ? $socios[2]->email_pessoal_diretor: ''); ?>" name="email_pessoal_diretor[]" placeholder="E-mail Comercial">
                            <small aviso-texto class="form-text text-muted" >E-mail que os boletos de aluguel serão enviados.</small>
                            <div class="invalid-feedback">
                                Insira o E-mail Comercial
                            </div>
                        </div>
                    </div>

                	<div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="email_comercial_diretor">E-mail Comercial:*</label>
                            <input aviso-campo class="form-control" type="text" value="<?php echo set_value('email_comercial_diretor[2]',isset($socios[2]->email_comercial_diretor) ? $socios[2]->email_comercial_diretor: ''); ?>" name="email_comercial_diretor[]" placeholder="E-mail Comercial">
                            <small aviso-texto class="form-text text-muted" >E-mail que os boletos de aluguel serão enviados.</small>
                            <div class="invalid-feedback">
                                Insira o E-mail Comercial
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="telefone_diretor">Telefone:*</label>
                            <input class="form-control" type="text" mascara="tel" value="<?php echo set_value('telefone_diretor[2]',isset($socios[2]->telefone_diretor) ? $socios[2]->telefone_diretor: ''); ?>" name="telefone_diretor[]" placeholder="Telefone">
                            <div class="invalid-feedback">
                                Insira o E-mail Comercial
                            </div>
                        </div>
                    </div>
                </div>
            </div>

</div>