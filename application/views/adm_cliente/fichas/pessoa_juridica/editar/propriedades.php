            <div id="menu2" class="container tab-pane fade"><br>
                <div class="row">
                     <?php 
                     $contador=0;
                     foreach ($propriedades as $value) { ?>
                         <div class="col-12 col-sm-12 col-md-11 col-lg-11"> 
                            <div class="row" id="elemento-campo-propriedade">
                                <div class="col-12 col-sm-12 col-md-3 col-lg-3">
                                    <div aviso-bloco class="form-group">
                                        <label for="matricula_imovel">Nº da Matícula do Imóvel:*</label>
                                        <input aviso-campo class="form-control" type="text" value="<?php echo set_value("matricula_imovel[$contador]",$value->matricula_imovel); ?>" name="matricula_imovel[]" placeholder="Nº da Matícula do Imóvel" >
                                        <small aviso-texto class="form-text text-muted">
                                            Para Adicionar Mais Propriedades, clique Botão +1 ao lado do Renavam. Caso não possua, escrever 'não aplicável'.
                                        </small>
                                        <div class="invalid-feedback">
                                            Insira o Nº da Matícula do Imóvel
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-3 col-lg-3">
                                    <div aviso-bloco class="form-group">
                                        <label for="circunscricao">Circunscrição:*</label>
                                        <input aviso-campo class="form-control" type="text" value="<?php echo set_value("circunscricao[$contador]",$value->circunscricao); ?>" name="circunscricao[]" placeholder="Circunscrição" >
                                        <small aviso-texto class="form-text text-muted">
                                            Para Adicionar Mais Propriedades, clique Botão +1 ao lado do Renavam. Caso não possua, escrever 'não aplicável'.
                                        </small>
                                        <div class="invalid-feedback">
                                            Insira Circunscrição
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-3 col-lg-3">
                                    <div aviso-bloco class="form-group">
                                        <label for="veiculo_marca">Marca Do Veículo:*</label>
                                        <input aviso-campo class="form-control" type="text" value="<?php echo set_value("veiculo_marca[$contador]",$value->veiculo_marca); ?>" name="veiculo_marca[]" placeholder="Marca Do Veículo" >
                                        <small aviso-texto class="form-text text-muted">
                                            Para Adicionar Mais Propriedades, clique Botão +1 ao lado do Renavam. Caso não possua, escrever 'não aplicável'.
                                        </small>
                                        <div class="invalid-feedback">
                                            Insira a Marca Do Veículo
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-3 col-lg-3">
                                    <div aviso-bloco class="form-group">
                                        <label for="renavam">Renavam:*</label>
                                        <input aviso-campo class="form-control" type="text" value="<?php echo set_value("renavam[$contador]",$value->renavam); ?>" name="renavam[]" placeholder="Renavam" >
                                        <small aviso-texto class="form-text text-muted">
                                            Para Adicionar Mais Propriedades, clique Botão +1 ao lado do Renavam. Caso não possua, escrever 'não aplicável'.
                                        </small>
                                        <div class="invalid-feedback">
                                            Insira O Renavam
                                        </div>
                                    </div>
                                </div>
                            </div>                          
                        </div>
                        <div class="col-12 col-sm-12 col-md-1 col-lg-1 text-center">
                            <button type="button" class="btn btn-baggio btn-mais-um" href="#" clonar-elemento="elemento-campo-propriedade">
                                <b>+1</b>
                            </button>
                        </div>
                    <?php $contador++; } ?>
                </div>
            </div>