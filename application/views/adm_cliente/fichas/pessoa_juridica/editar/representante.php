        <div id="menu0" class="container tab-pane"><br>        
    
            
<!-------------------------------------------------------------------------------------------------------------------------->

            <div class="row">
                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="form-group">
                        <label for="nome_completo">Nome Completo:*</label>
                        <input class="form-control text-uppercase" type="text" value="<?php echo set_value('nome_completo',$query['nome_completo']); ?>" name="nome_completo" placeholder="Nome Completo" >
                        <div class="invalid-feedback">
                            Insira o Nome Completo
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="form-group">
                        <label for="telefone_fixo">Telefone Fixo:*</label>
                        <input class="form-control" type="text" mascara="tel" value="<?php echo set_value('telefone_fixo',$query['telefone_fixo']); ?>" name="telefone_fixo" placeholder="Telefone Fixo" >
                        <div class="invalid-feedback">
                            Insira o Telefone Fixo
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="form-group">
                        <label for="celular">Celular:*</label>
                        <input class="form-control" type="text" mascara="cel" value="<?php echo set_value('celular',$query['celular']); ?>" name="celular" placeholder="Celular" >
                        <div class="invalid-feedback">
                            Insira o Celular
                        </div>
                    </div>
                </div>
            </div>
            
<!-------------------------------------------------------------------------------------------------------------------------->

            <div class="row">
                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="form-group">
                        <label for="email">E-mail:*</label>
                        <input class="form-control text-uppercase" type="text" value="<?php echo set_value('email',$query['email']); ?>" name="email" placeholder="E-mail" >
                        <div class="invalid-feedback">
                            Insira o E-mail
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="form-group">
                        <label for="email">E-mail Comercial:*</label>
                        <input class="form-control text-uppercase" type="text" value="<?php echo set_value('email_comercial_socio',$query['email_comercial_socio']); ?>" name="email_comercial_socio" placeholder="E-mail Comercial" >
                        <div class="invalid-feedback">
                            Insira o E-mail
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="form-group">
                        <label for="data_nascimento">Data de Nascimento:*</label>
                        <input class="form-control" type="text" mascara="data" value="<?php echo set_value('data_nascimento', formatar_data($query['data_nascimento'])); ?>" name="data_nascimento" placeholder="Data de Nascimento" >
                        <div class="invalid-feedback">
                            Insira a Data de Nasciimento
                        </div>
                    </div>
                </div>
            </div>
            
<!-------------------------------------------------------------------------------------------------------------------------->

            <div class="row">
                
                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="form-group">
                        <label for="nacionalidade">Nacionalidade*</label>
                        <input class="form-control text-uppercase" type="text" value="<?php echo set_value('nacionalidade',$query['nacionalidade']); ?>" name="nacionalidade" placeholder="Nacionalidade" >
                        <div class="invalid-feedback">
                            Insira a Nacionalidade
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                    <div aviso-bloco class="form-group">
                        <label for="cpf">CPF:*</label>
                        <input aviso-campo class="form-control" type="text" mascara="cpf" value="<?php echo set_value('cpf',$query['cpf']); ?>" name="cpf" placeholder="CPF" >
                        <small aviso-texto class="form-text text-muted">Caso seja estrangeiro, converse com o consultor ou consultora que está lhe atendendo.</small>
                        <div class="invalid-feedback">
                            Insira o CPF
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                    <div aviso-bloco class="form-group">
                        <label for="rg">RG*</label>
                        <input aviso-campo class="form-control" type="text" value="<?php echo set_value('rg',$query['rg']); ?>" name="rg" placeholder="RG" >
                        <small aviso-texto class="form-text text-muted">Caso seja estrangeiro, converse com o consultor ou consultora que está lhe atendendo.</small>
                        <div class="invalid-feedback">
                            Insira o RG
                        </div>
                    </div>
                </div>
            </div>
            
<!-------------------------------------------------------------------------------------------------------------------------->

            <div class="row">
                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                    <div aviso-bloco class="form-group">
                        <label for="expedicao_rg">Local de Expedição do RG:*</label>
                        <input aviso-campo class="form-control" type="text" value="<?php echo set_value('expedicao_rg',$query['expedicao_rg']); ?>" name="expedicao_rg" placeholder="Local de Expedição do RG" >
                        <small aviso-texto class="form-text text-muted">Caso seja estrangeiro, converse com o consultor ou consultora que está lhe atendendo.</small>
                        <div class="invalid-feedback">
                            Insira o Local de Expedição
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                    <div aviso-bloco class="form-group">
                        <label for="data_expedicao">Data de Expedição:*</label>
                        <input aviso-campo class="form-control" type="text" mascara="data" value="<?php echo set_value('data_expedicao', formatar_data($query['data_expedicao'])); ?>" name="data_expedicao" placeholder="Data de Expedição" >
                        <small aviso-texto class="form-text text-muted">Caso seja estrangeiro, converse com o consultor ou consultora que está lhe atendendo.</small>
                        <div class="invalid-feedback">
                            Insira oa Data de Expedição
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="form-group">
                        <label for="id_estado_civil">Estado Civil:*</label>
                        <select name="id_estado_civil" class="form-control"  togvis-select required>
                            <option togvis-esconder-grupo="select-divs" value="3" >Selecione o Estado Civil</option>
                            <option togvis-esconder-grupo="select-divs" value="1" <?php if($query['id_estado_civil']=="1"){ echo 'selected="selected"';}?> >Não Informado</option>
                            <option togvis-option="casado" togvis-outros="esconder" value="2" <?php if($query['id_estado_civil']=="2"){ echo 'selected="selected"';}?>>Casado(a)</option>
                            <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="3" <?php if($query['id_estado_civil']=="3"){ echo 'selected="selected"';}?>>Solteiro(a)</option>
                            <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="4" <?php if($query['id_estado_civil']=="4"){ echo 'selected="selected"';}?>>Divorciado(a)</option>
                            <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="5" <?php if($query['id_estado_civil']=="5"){ echo 'selected="selected"';}?>>Viúvo(a)</option>
                            <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="6" <?php if($query['id_estado_civil']=="6"){ echo 'selected="selected"';}?>>União Estável</option>
                        </select>
                        <div class="invalid-feedback">
                            Insira o seu Estado Civil
                        </div>
                    </div>
                </div>
            </div>
            
<!-------------------------------------------------------------------------------------------------------------------------->

            <div class="row">
                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="form-group">
                      <label for="profissao">Profissão:*</label>
                      <input type="text" class="form-control text-uppercase" name="profissao" placeholder="Profissão" value="<?php echo set_value('profissao',$query['profissao'])?>" >
                        <div class="invalid-feedback">
                            Insira a Profissão
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="form-group">
                        <label for="cargo">Cargo Exercido:*</label>
                        <input type="text" class="form-control text-uppercase" name="cargo" placeholder="Cargo Exercido" value="<?php echo set_value('cargo',$query['cargo'])?>" >
                        <div class="invalid-feedback">
                            Insira o Cargo Exercido
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="form-group">
                        <label for="salario">Salário (em R$):*</label>
                        <input type="text" class="form-control" name="salario" placeholder="Salário" value="<?php echo set_value('salario',$query['salario'])?>" >
                        <div class="invalid-feedback">
                            Insira o Salário
                        </div>
                    </div>
                </div>
            </div>
            
<!-------------------------------------------------------------------------------------------------------------------------->
            
            <hr>

            <div class="row">
                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                    <label>Residencia Própria?</label><br>
                    <div class="custom-controls-stacked d-block my-3">
                        <label class="custom-control custom-radio">
                            <input name="residencia" value="1" type="radio" class="" <?php if($query['residencia']==1) echo 'checked';?>>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Sim</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input name="residencia" value="0" type="radio" class="" <?php if($query['residencia']==0) echo 'checked';?>>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Não</span>
                        </label>
                    </div>
                </div>
            </div>

            
<!-------------------------------------------------------------------------------------------------------------------------->

            <div class="row">
                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="form-group">
                        <label for="cep_cliente">CEP da Residência:*</label>
                        <input cep-bloco="representante" class="form-control" type="text" mascara="cep" name="cep_cliente" value="<?php echo set_value('cep_cliente',$query['cep_cliente']); ?>" placeholder="CEP" >
                        <div class="invalid-feedback">
                            Insira o CEP
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="form-group">
                        <label for="logradouro_cliente">Logradouro da Residência:*</label>
                        <input cep-rua="representante" class="form-control text-uppercase" type="text" value="<?php echo set_value('logradouro_cliente',$query['logradouro_cliente']); ?>" name="logradouro_cliente" placeholder="Logradouro" >
                        <div class="invalid-feedback">
                            Insira o Logradouro
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="form-group">
                        <label for="numero_cliente">Número da Residência:*</label>
                        <input class="form-control" type="text" value="<?php echo set_value('numero_cliente',$query['numero_cliente']); ?>" name="numero_cliente" placeholder="Número" >
                        <div class="invalid-feedback">
                            Insira o Número
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="form-group">
                        <label for="complemento_cliente">Complemento da Residência:</label>
                        <input class="form-control text-uppercase" type="text" value="<?php echo set_value('complemento_cliente',$query['complemento_cliente']); ?>" name="complemento_cliente" placeholder="Complemento">
                    </div>
                </div>

                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                    <div aviso-bloco class="form-group">
                        <label for="complemento_cliente">Cidade:*</label>
                        <input aviso-campo cep-cidade="representante" class="form-control text-uppercase" type="text" value="" name="" placeholder="Cidade">
                    </div>
                    <div class="invalid-feedback">
                        Insira o Estado
                    </div>
                </div>

                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                    <div aviso-bloco class="form-group">
                        <label for="bairro_cliente">Estado:*</label>
                        <input aviso-campo cep-uf="representante" class="form-control text-uppercase" type="text" value="" name="" placeholder="Estado" >
                        <div class="invalid-feedback">
                            Insira o Estado
                        </div>
                    </div>
                </div>
            </div>
            
<!-------------------------------------------------------------------------------------------------------------------------->

            <div class="row">
                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="form-group">
                        <label for="bairro_cliente">Bairro da Residência:*</label>
                        <input cep-bairro="representante" class="form-control text-uppercase" type="text" value="<?php echo set_value('bairro_cliente',$query['bairro_cliente']); ?>" name="bairro_cliente" placeholder="Bairro" >
                        <div class="invalid-feedback">
                            Insira o Bairro
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="form-group">
                        <?php 
                        $array_pais['']='Selecione o País';
                        foreach ($paises as $value) {
                            $array_pais[$value->id_pais]=$value->nome_pais;
                        }?>

                        <label for="id_pais_cliente">País:*</label>
                        <?php echo form_dropdown('id_pais_cliente', $array_pais, set_value('id_pais_cliente',33), 'class="form-control" ') ?>
                        <div class="invalid-feedback">
                            Selecione um País
                        </div>
                    </div>
                </div>
            </div>
            
<!-------------------------------------------------------------------------------------------------------------------------->

            <hr>

            <div class="row">
                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                    <div aviso-bloco class="form-group">
                        <label for="outras_rendas">Outras Rendas (especificar):*</label>
                        <textarea aviso-campo class="form-control" type="text" rows="5" name="outras_rendas" placeholder="Outras Rendas (especificar)" ><?php echo set_value('outras_rendas',$query['outras_rendas']); ?></textarea>
                        <small aviso-texto class="form-text text-muted">Caso não possua, escrever 'não aplicável'.</small>
                        <div class="invalid-feedback">
                            Insira Outras Rendas
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="form-group">
                        <label for="renda_total">Renda Total (em R$):*</label>
                        <input class="form-control" type="text" value="<?php echo set_value('renda_total',$query['renda_total']); ?>" name="renda_total" placeholder="Renda Total (em R$)" >
                        <div class="invalid-feedback">
                            Insira a Renda Total
                        </div>
                    </div>
                </div>
            </div>
            
<!-------------------------------------------------------------------------------------------------------------------------->

        </div>