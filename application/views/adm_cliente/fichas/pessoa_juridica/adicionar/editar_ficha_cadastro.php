<div class="container-fluid bold-label">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="caixa-adm-cliente-central">
            <h2>Editar Ficha de Cadastro Pessoa Jurídica</h2>
        </div>
        <hr>
        
<!---------------------------------------------------------------------------------------------------------------------->

        
            
<!---------------------------------------------------------------------------------------------------------------------->

        
            
<!---------------------------------------------------------------------------------------------------------------------->

            <div class="jumbotron jumbotron-fluid">
                <div class="container">
                    <h1 class="display-4">Dados dos Sócios ou Diretores</h1>
                </div>
            </div>
            
            
<!---------------------------------------------------------------------------------------------------------------------->

            <div class="jumbotron jumbotron-fluid">
                <div class="container">
                    <h1 class="display-4">Propriedades da Empresa</h1>
                    <p class="lead">As propriedades devem ser no nome da empresa/CNPJ.</p>
                </div>
            </div>
            <p>Para Adicionar Mais Propriedades, clique Botão +1 ao lado do Renavam. Caso não possua, escrever 'não aplicável'.</p>
             <div class="row">
                <div class="col"> 
                     <?php 
                     $contador=0;
                     foreach ($propriedades as $value) { ?>
                    <div class="row" id="elemento-campo-propriedade">
                        <div class="col">
                            <div class="form-group">
                                <label for="matricula_imovel">Nº da Matícula do Imóvel:*</label>
                                <input class="form-control" type="text" value="<?php echo set_value("matricula_imovel[$contador]",$value->matricula_imovel); ?>" name="matricula_imovel[]" placeholder="Nº da Matícula do Imóvel" >
                                <div class="invalid-feedback">
                                    Insira o Nº da Matícula do Imóvel
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="circunscricao">Circunscrição:*</label>
                                <input class="form-control" type="text" value="<?php echo set_value("circunscricao[$contador]",$value->circunscricao); ?>" name="circunscricao[]" placeholder="Circunscrição" >
                                <div class="invalid-feedback">
                                    Insira Circunscrição
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="veiculo_marca">Marca Do Veículo:*</label>
                                <input class="form-control" type="text" value="<?php echo set_value("veiculo_marca[$contador]",$value->veiculo_marca); ?>" name="veiculo_marca[]" placeholder="Marca Do Veículo" >
                                <div class="invalid-feedback">
                                    Insira a Marca Do Veículo
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="renavam">Renavam:*</label>
                                <input class="form-control" type="text" value="<?php echo set_value("renavam[$contador]",$value->renavam); ?>" name="renavam[]" placeholder="Renavam" >
                                <div class="invalid-feedback">
                                    Insira O Renavam
                                </div>
                            </div>
                        </div>
                        <div class="col-1 ">
                            <button type="button" class="btn btn-primary" href="#"
                                     style="margin-top: 37%;" clonar-elemento="elemento-campo-propriedade">
                                        <i class="material-icons">exposure_plus_1</i>
                            </button>
                        </div>
                    </div>
                     <?php $contador++; } ?>
                </div>
            </div>
            <div class="row">
               <div class="col">
                   <div class="form-group">
                        <input type="submit" name="enviar" class="btn btn-success" value="Salvar"/>
                   </div>
               </div>
            </div>
             
<!---------------------------------------------------------------------------------------------------------------------->

             <div class="jumbotron jumbotron-fluid">
                <div class="container">
                    <h1 class="display-4">Referências</h1>
                    <p class="lead">Preencher abaixo as referências comerciais e bancárias determinadas abaixo.</p>
                </div>
            </div>  
            <p>Referências Comerciais (OBRIGATÓRIO 2 REFERÊNCIAS Comerciais)*</p>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="nome_referencia">Nome Completo:*</label>
                        <input class="form-control text-uppercase" type="text" value="<?php echo set_value('nome_referencia[0]',$referencias[0]->nome_referencia); ?>" name="nome_referencia[]" placeholder="Nome Completo" >
                        <div class="invalid-feedback">
                            Insira o Nome Completo
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="endereco_referencia">Endereço:*</label>
                        <input class="form-control" type="text" value="<?php echo set_value('endereco_referencia[0]',$referencias[0]->endereco_referencia); ?>" name="endereco_referencia[]" placeholder="Endereço" >
                        <div class="invalid-feedback">
                            Insira o Endereço
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="telefone_referencia">Telefone:*</label>
                        <input class="form-control" type="text" mascara="tel" value="<?php echo set_value('telefone_referencia[0]',$referencias[0]->telefone_referencia); ?>" name="telefone_referencia[]" placeholder="Telefone" >
                        <div class="invalid-feedback">
                            Insira o Telefone
                        </div>
                    </div>
                </div>
                 <input hidden value="2" name="tipo_referencia[]">
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="nome_referencia">Nome Completo:*</label>
                        <input class="form-control text-uppercase" type="text" value="<?php echo set_value('nome_referencia[1]',$referencias[1]->nome_referencia); ?>" name="nome_referencia[]" placeholder="Nome Completo" >
                        <div class="invalid-feedback">
                            Insira o Nome Completo
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="endereco_referencia">Endereço:*</label>
                        <input class="form-control" type="text" value="<?php echo set_value('endereco_referencia[1]',$referencias[1]->endereco_referencia); ?>" name="endereco_referencia[]" placeholder="Endereço" >
                        <div class="invalid-feedback">
                            Insira o Endereço
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="telefone_referencia">Telefone:*</label>
                        <input class="form-control" type="text" mascara="tel" value="<?php echo set_value('telefone_referencia[1]',$referencias[1]->telefone_referencia); ?>" name="telefone_referencia[]" placeholder="Telefone" >
                        <div class="invalid-feedback">
                            Insira o Telefone
                        </div>
                    </div>
                </div>
                 <input hidden value="2" name="tipo_referencia[]">
            </div>
             <p>Referências Bancárias (OBRIGATÓRIO 2 REFERÊNCIAS BANCÁRIAS)*</p>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="banco_referencia">Banco:*</label>
                        <input class="form-control" type="text" value="<?php echo set_value('banco_referencia[0]',$referencias_bancarias[0]->banco_referencia); ?>" name="banco_referencia[]" placeholder="Banco" >
                        <div class="invalid-feedback">
                            Insira o Nome do Banco
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="telefone_banco_referencia">Telefone da Agencia:*</label>
                        <input class="form-control" type="text" mascara="tel" value="<?php echo set_value('telefone_banco_referencia[0]',$referencias_bancarias[0]->telefone_banco_referencia); ?>" name="telefone_banco_referencia[]" placeholder="Telefone da Agencia" >
                        <div class="invalid-feedback">
                            Insira o Telefone da Agencia
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="nome_agencia_referencia">Nome da Agência:*</label>
                        <input class="form-control text-uppercase" type="text"  value="<?php echo set_value('nome_agencia_referencia[0]',$referencias_bancarias[0]->nome_agencia_referencia); ?>" name="nome_agencia_referencia[]" placeholder="Nome da Agência" >
                        <div class="invalid-feedback">
                            Insira o Telefone
                        </div>
                    </div>
                </div>
                 <div class="col">
                    <div class="form-group">
                        <label for="numero_conta_referencia">Número da Conta:*</label>
                        <input class="form-control" type="text" value="<?php echo set_value('numero_conta_referencia[0]',$referencias_bancarias[0]->nome_agencia_referencia); ?>" name="numero_conta_referencia[]" placeholder="Número da Conta" >
                        <div class="invalid-feedback">
                            Insira o Número da Conta
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="banco_referencia">Banco:*</label>
                        <input class="form-control" type="text" value="<?php echo set_value('banco_referencia[1]',$referencias_bancarias[1]->banco_referencia); ?>" name="banco_referencia[]" placeholder="Banco" >
                        <div class="invalid-feedback">
                            Insira o Nome do Banco
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="telefone_banco_referencia">Telefone da Agencia:*</label>
                        <input class="form-control" type="text" value="<?php echo set_value('telefone_banco_referencia[1]',$referencias_bancarias[1]->telefone_banco_referencia); ?>" mascara="tel" name="telefone_banco_referencia[]" placeholder="Telefone da Agencia" >
                        <div class="invalid-feedback">
                            Insira o Telefone da Agencia
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="nome_agencia_referencia">Nome da Agência:*</label>
                        <input class="form-control text-uppercase" type="text" value="<?php echo set_value('nome_agencia_referencia[1]',$referencias_bancarias[1]->nome_agencia_referencia); ?>" name="nome_agencia_referencia[]" placeholder="Nome da Agência" >
                        <div class="invalid-feedback">
                            Insira o Telefone
                        </div>
                    </div>
                </div>
                 <div class="col">
                    <div class="form-group">
                        <label for="numero_conta_referencia">Número da Conta:*</label>
                        <input class="form-control" type="text" value="<?php echo set_value('numero_conta_referencia[1]',$referencias_bancarias[1]->numero_conta_referencia); ?>" name="numero_conta_referencia[]" placeholder="Número da Conta" >
                        <div class="invalid-feedback">
                            Insira o Número da Conta
                        </div>
                    </div>
                </div>
            </div>
             <div class="row">
               <div class="col">
                   <div class="form-group">
                        <input type="submit" name="enviar" class="btn btn-success" value="Salvar"/>
                   </div>
               </div>
            </div>
            <div class="form-check">
               <label class="custom-control custom-checkbox">
                   <input type="checkbox" class="custom-control-input"  name="concordo">
                   <span class="custom-control-indicator"></span>
                   <span class="custom-control-description">
                       OBS. No valor do aluguel serão acrescidos mensalmente as
                       taxas: IPTU, água, luz, seguro contra incêndio, condomínio,
                       etc. Em caso de dúvidas, converse com o cosultor(a) que 
                       está lhe atendendo.
                   </span>
               </label>
            </div>
             <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="observacoes_complementares">Observações Complementares: <small>Preencher caso haja alguma observação.</small></label>
                        <textarea class="form-control" rows="10" name="observacoes_complementares"><?php echo set_value('observacoes_complementares',$query['observacoes_complementares']) ?></textarea>
                    </div>
                </div>
            </div>
             <input type="submit" name="enviar" class="btn btn-primary" value="Enviar Ficha para Baggio"/>
            <input type="submit" name="enviar" class="btn btn-success" value="Salvar"/>
        </form>
    </div>
</div>