            <div id="menu3" class="container tab-pane fade"><br>

                <p><small>Referências Comerciais (OBRIGATÓRIO 2 REFERÊNCIAS Comerciais)*</small></p>

                
<!---------------------------------------------------------------------------------------------------------------->

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="nome_referencia">Nome Completo:*</label>
                            <input class="form-control text-uppercase" type="text" value="" name="nome_referencia[]" placeholder="Nome Completo" >
                            <div class="invalid-feedback">
                                Insira o Nome Completo
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="endereco_referencia">Endereço:*</label>
                            <input class="form-control" type="text" value="" name="endereco_referencia[]" placeholder="Endereço" >
                            <div class="invalid-feedback">
                                Insira o Endereço
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="telefone_referencia">Telefone:*</label>
                            <input class="form-control" type="text" mascara="tel" value="" name="telefone_referencia[]" placeholder="Telefone" >
                            <div class="invalid-feedback">
                                Insira o Telefone
                            </div>
                        </div>
                    </div>
                     <input hidden value="2" name="tipo_referencia[]">
                </div>
                
<!---------------------------------------------------------------------------------------------------------------->

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="nome_referencia">Nome Completo:*</label>
                            <input class="form-control text-uppercase" type="text" value="" name="nome_referencia[]" placeholder="Nome Completo" >
                            <div class="invalid-feedback">
                                Insira o Nome Completo
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="endereco_referencia">Endereço:*</label>
                            <input class="form-control" type="text" value="" name="endereco_referencia[]" placeholder="Endereço" >
                            <div class="invalid-feedback">
                                Insira o Endereço
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="telefone_referencia">Telefone:*</label>
                            <input class="form-control" type="text" mascara="tel" value="" name="telefone_referencia[]" placeholder="Telefone" >
                            <div class="invalid-feedback">
                                Insira o Telefone
                            </div>
                        </div>
                    </div>
                     <input hidden value="2" name="tipo_referencia[]">
                </div>               
                
<!---------------------------------------------------------------------------------------------------------------->

                <small><p>Referências Bancárias (OBRIGATÓRIO 2 REFERÊNCIAS BANCÁRIAS)*</p></small>

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-3 col-lg-3">
                        <div aviso-bloco class="form-group">
                            <label for="banco_referencia">Banco:*</label>
                            <input aviso-campo class="form-control text-uppercase" type="text" value="" name="banco_referencia[]" placeholder="Banco" >
                            <div class="invalid-feedback">
                                Insira o Nome do Banco
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-3 col-lg-3">
                        <div aviso-bloco class="form-group">
                            <label for="telefone_banco_referencia">Telefone da Agencia:*</label>
                            <input aviso-campo class="form-control" type="text" mascara="tel" value="" name="telefone_banco_referencia[]" placeholder="Telefone da Agencia" >
                            <small aviso-texto class="form-text text-muted">
                                Em caso de Não Aplicável, colocar (00) 0000-0000
                            </small>
                            <div class="invalid-feedback">
                                Insira o Telefone da Agencia
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-3 col-lg-3">
                        <div aviso-bloco class="form-group">
                            <label for="nome_agencia_referencia">Nome da Agência:*</label>
                            <input aviso-campo class="form-control text-uppercase" type="text"  value="" name="nome_agencia_referencia[]" placeholder="Nome da Agência" >
                            <!--<small aviso-texto class="form-text text-muted">
                                Em caso de Não Aplicável, colocar (00) 0000-0000
                            </small>-->
                            <div class="invalid-feedback">
                                Insira o Telefone
                            </div>
                        </div>
                    </div>
                     <div class="col-12 col-sm-12 col-md-3 col-lg-3">
                        <div aviso-bloco class="form-group">
                            <label for="numero_conta_referencia">Número da Conta:*</label>
                            <input aviso-campo class="form-control" type="text" value="" name="numero_conta_referencia[]" placeholder="Número da Conta" >
                            <div class="invalid-feedback">
                                Insira o Número da Conta
                            </div>
                        </div>
                    </div>
                </div>
                
<!---------------------------------------------------------------------------------------------------------------->

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-3 col-lg-3">
                        <div aviso-bloco class="form-group">
                            <label for="banco_referencia">Banco:*</label>
                            <input aviso-campo class="form-control text-uppercase" type="text" value="" name="banco_referencia[]" placeholder="Banco" >
                            <div class="invalid-feedback">
                                Insira o Nome do Banco
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-3 col-lg-3">
                        <div aviso-bloco class="form-group">
                            <label for="telefone_banco_referencia">Telefone da Agencia:*</label>
                            <input aviso-campo class="form-control" type="text" value="" mascara="tel" name="telefone_banco_referencia[]" placeholder="Telefone da Agencia" >
                            <small aviso-texto class="form-text text-muted">
                                Em caso de Não Aplicável, colocar (00) 0000-0000
                            </small>
                            <div class="invalid-feedback">
                                Insira o Telefone da Agencia
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-3 col-lg-3">
                        <div aviso-bloco class="form-group">
                            <label for="nome_agencia_referencia">Nome da Agência:*</label>
                            <input aviso-campo class="form-control text-uppercase" type="text" value="" name="nome_agencia_referencia[]" placeholder="Nome da Agência" >
                            <!--<small aviso-texto class="form-text text-muted">
                                Em caso de Não Aplicável, colocar (00) 0000-0000
                            </small>-->
                            <div class="invalid-feedback">
                                Insira o Telefone
                            </div>
                        </div>
                    </div>
                     <div class="col-12 col-sm-12 col-md-3 col-lg-3">
                        <div aviso-bloco class="form-group">
                            <label for="numero_conta_referencia">Número da Conta:*</label>
                            <input aviso-campo class="form-control" type="text" value="" name="numero_conta_referencia[]" placeholder="Número da Conta" >
                            <div class="invalid-feedback">
                                Insira o Número da Conta
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>