                
                <div id="menu4" class="container tab-pane fade"><br>
                    <div class="form-check">
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox"  name="concordo">
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">
                                OBS. No valor do aluguel, serão acrescidos mensalmente as 
                                taxas: IPTU, água, luz, seguro contra incêndio, condomínio, 
                                etc. Consulte nossos atendentes para maiores informações.
                            </span>
                        </label>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="observacoes_complementares">Observações Complementares: <p> <small>Preencher caso haja alguma observação.</small> </p></label>
                                <textarea class="form-control" rows="5" name="observacoes_complementares"></textarea>
                            </div>
                        </div>
                    </div>
                     

                    
                </div>