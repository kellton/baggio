                    <div id="home" class="container tab-pane active"><br>
                        
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                <div aviso-bloco class="form-group">
                                    <label for="razao_social">Razão Social:*</label>
                                    <input aviso-campo type="text" class="form-control text-uppercase" name="razao_social" id="nome_cliente" placeholder="Razão Social" value="<?php echo set_value('razao_social')?>" >
                                    <small aviso-texto class="form-text text-muted">Nome da empresa conforme descrito no cartão CNPJ.</small>
                                    <div class="invalid-feedback">
                                        Insira a Razão Social
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                <label>Tipo da Empresa:* </label><br>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input name="tipo" value="LTDA" togvis-button="tipo" type="radio" class="form-check-input" required >
                                        <span class="">LTDA</span>
                                    </label>
                                </div>
                                <div class="form-check-inline">    
                                    <label class="form-check-label">
                                        <input name="tipo" value="S.A" togvis-button="tipo" type="radio" class="form-check-input" required >
                                        <span class="">S.A</span>
                                    </label>
                                </div>
                                <div class="form-check-inline">    
                                    <label class="form-check-label">
                                        <input name="tipo" value="MEI" togvis-button="tipo" type="radio" class="form-check-input" required >
                                        <span class="">MEI</span>
                                    </label>
                                </div>
                                <div class="form-check-inline">    
                                    <label class="form-check-label">
                                        <input name="tipo" value="EIRELI" togvis-button="tipo" type="radio" class="form-check-input" required >
                                        <span class="">EIRELI</span>
                                    </label>
                                </div>
                                <div class="form-check-inline">    
                                    <label class="form-check-label">
                                        <input name="tipo" value="ME" togvis-button="tipo" type="radio" class="form-check-input" required >
                                        <span class="">ME</span>
                                    </label>
                                </div>
                                <div class="form-check-inline">    
                                    <label class="form-check-label">
                                        <input name="tipo" value="OUTRO" togvis-button="tipo-outro" type="radio" class="form-check-input" required >
                                        <span class="">OUTRO</span>
                                    </label>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-4 col-lg-4" togvis-id="tipo-outro" togvis-estado='hide' togvis-outros='esconder' togvis-grupo='grupo-tipo'>
                                <div class="form-group">
                                    <label for="tipo_empresa">Tipo de Empresa:*</label>
                                    <input type="text" class="form-control" name="tipo_empresa" placeholder="Tipo de Empresa" value="<?php echo set_value('tipo_empresa')?>">
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-4 col-lg-4" togvis-id="tipo" togvis-estado='hide' togvis-outros='esconder' togvis-grupo='grupo-tipo'></div>
                        </div>

<!---------------------------------------------------------------------------------------------------------------------->

                       
                        
<!---------------------------------------------------------------------------------------------------------------------->

                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label for="fundacao">Data da Fundação:*</label>
                                    <input type="text" class="form-control" name="fundacao" mascara="data" placeholder="Data da Fundação" value="<?php echo set_value('fundacao')?>" >
                                    <div class="invalid-feedback">
                                        Insira a Data da Fundação
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label for="cnpj">CNPJ:*</label>
                                    <input type="text" class="form-control" name="cnpj" mascara="cnpj" placeholder="CNPJ" value="<?php echo set_value('cnpj')?>" >
                                    <div class="invalid-feedback">
                                        Insira o CNPJ
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label for="junta_comercial">Número de Registro na Junta Comercial:*</label>
                                    <input type="text" class="form-control" name="junta_comercial" placeholder="Número de Registro na Junta Comercial" value="<?php echo set_value('junta_comercial')?>" >
                                    <div class="invalid-feedback">
                                        Insira o Número de Registro na Junta Comercial
                                    </div>
                                </div>
                            </div>
                        </div>
                        
<!---------------------------------------------------------------------------------------------------------------------->

                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label for="inscricao_estadual">Número Inscrição Estadual:*</label>
                                    <input type="text" class="form-control" name="inscricao_estadual" placeholder="Número Inscrição Estadual" value="<?php echo set_value('inscricao_estadual')?>" >
                                    <div class="invalid-feedback">
                                        Insira o Número Inscrição Estadual
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                <div aviso-bloco class="form-group">
                                    <label for="capital_social">Capital Social da Empresa (em R$):*</label>
                                    <input aviso-campo type="text" class="form-control" name="capital_social" placeholder="Capital Social da Empresa (em R$)" value="<?php echo set_value('capital_social')?>" >
                                    <small aviso-texto class="form-text text-muted">Caso tenha dúvidas, converse com o consultor ou consultora que está lhe atendendo.</small>
                                    <div class="invalid-feedback">
                                        Insira o Capital Social da Empresa
                                    </div>
                                </div>
                            </div>

                            <div aviso-bloco class="col-12 col-sm-12 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label for="ramo_atividade">Ramo de Atividade:*</label>
                                    <input aviso-campo type="text" class="form-control text-uppercase" name="ramo_atividade" placeholder="Ramo de Atividade" value="<?php echo set_value('inscricao_estadual')?>" >
                                    <small aviso-texto class="form-text text-muted">Especifique o ramo de negócios que a empresa tem atividade atualmente.</small>
                                    <div class="invalid-feedback">
                                        Insira o Ramo de Atividade
                                    </div>
                                </div>
                            </div>
                        </div>
                        
<!---------------------------------------------------------------------------------------------------------------------->

                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label for="cep">CEP da Empresa:*</label>
                                    <input cep-bloco="empresa" class="form-control" type="text" mascara="cep" name="cep" value="<?php echo set_value('cep'); ?>" placeholder="CEP da Empresa" >
                                    <div class="invalid-feedback">
                                        Insira o CEP
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                <div aviso-bloco class="form-group">
                                    <label for="logradouro">Logradouro Atual:*</label>
                                    <input cep-rua="empresa" aviso-campo class="form-control text-uppercase" type="text" endereco-auto="logradouro" value="<?php echo set_value('logradouro'); ?>" name="logradouro" placeholder="Logradouro" >
                                    <small aviso-texto class="form-text text-muted">
                                       Rua, Avenida, Estrada, Travessa, etc.
                                    </small>

                                    <div class="invalid-feedback">
                                        Insira o Logradouro
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label for="numero">Número:*</label>
                                    <input class="form-control" type="text" value="<?php echo set_value('numero'); ?>" name="numero" placeholder="Número" >
                                    <div class="invalid-feedback">
                                        Insira o Número
                                    </div>
                                </div>
                            </div>
                        </div>
                        
<!---------------------------------------------------------------------------------------------------------------------->

                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label for="complemento">Complemento:</label>
                                    <input class="form-control text-uppercase" type="text" endereco-auto="complemento" value="<?php echo set_value('complemento'); ?>" name="complemento" placeholder="Complemento">
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label for="bairro">Bairro:*</label>
                                    <input cep-bairro="empresa" class="form-control text-uppercase" type="text" endereco-auto="bairro" value="<?php echo set_value('bairro'); ?>" name="bairro" placeholder="Bairro" >
                                    <div class="invalid-feedback">
                                        Insira o Bairro
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                <div aviso-bloco class="form-group">
                                    <label for="complemento_cliente">Cidade:*</label>
                                    <input aviso-campo cep-cidade="empresa" class="form-control text-uppercase" type="text" value="" name="" placeholder="Cidade">
                                </div>
                                <div class="invalid-feedback">
                                    Insira o Estado
                                </div>
                            </div>
                        </div>
                        
<!---------------------------------------------------------------------------------------------------------------------->

                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                <div aviso-bloco class="form-group">
                                    <label for="bairro_cliente">Estado:*</label>
                                    <input aviso-campo cep-uf="empresa" class="form-control text-uppercase" type="text" value="" name="" placeholder="Estado" >
                                    <div class="invalid-feedback">
                                        Insira o Estado
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <?php 
                                    $array_pais['']='Selecione o País';
                                    foreach ($paises as $value) {
                                        $array_pais[$value->id_pais]=$value->nome_pais;
                                    }?>
                                    <label for="id_pais">País:*</label>
                                    <?php echo form_dropdown('id_pais', $array_pais, set_value('id_pais',33), 'class="form-control" ') ?>
                                    <div class="invalid-feedback">
                                        Insira o País
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                <div aviso-bloco class="form-group">
                                    <label for="email_comercial">E-mail Comercial:*</label>
                                    <input aviso-campo class="form-control" type="text" value="<?php echo set_value('email_comercial'); ?>" name="email_comercial" placeholder="E-mail Comercial" >
                                    <small aviso-texto class="form-text text-muted">E-mail que os boletos de aluguel serão enviados.</small>
                                    <div class="invalid-feedback">
                                        Insira o E-mail Comercial
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label for="telefone_comercial">Telefone Comercial:*</label>
                                    <input class="form-control" type="text" mascara="tel" value="<?php echo set_value('telefone_comercial'); ?>" name="telefone_comercial" placeholder="Telefone Comercial" >
                                    <div class="invalid-feedback">
                                        Insira o Telefone Comercial
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>