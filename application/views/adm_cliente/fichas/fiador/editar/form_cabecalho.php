<a name="ancora-menu"></a>
<div class="banner-background-adm">
    <img width="100%" src="<?php echo base_url('publico/imagens/area-adm/cliente/background-topo.png'); ?>">
</div>

<div class="container-fluid caixa-adm-fichas">
    <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 offset-xs-1 offset-sm-1 offset-md-1 offset-lg-1 caixa-fichas-cadastrais">
        <div class="text-center topo-ficha">
            <img width="200px" src="<?php echo base_url('publico/imagens/area-adm/cliente/ficha-cadastro-icone.png'); ?>"> 
        </div>

        <div class="erros-fichas">
            <?php echo validation_errors('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>','</div>'); ?>
        </div>
        <form action="<?php echo base_url('cli/adicionar-ficha-cadastro-fiador/validacao') ?>" method="POST" accept-charset="utf-8" id="needs-validation">
            <div class="row">
                <div class="col-12 text-center">
                    <ul class="nav nav-tabs menu-fichas menu-fiador" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" numero-aba="0" data-toggle="tab" href="#home">PRETENDENTE</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" numero-aba="1" data-toggle="tab" href="#menu0">DADOS ADICIONAIS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" numero-aba="2" data-toggle="tab" href="#menu1">CÔNJUGUE</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" numero-aba="3" data-toggle="tab" href="#menu2">PROPRIEDADES</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" numero-aba="4" data-toggle="tab" href="#menu3">REFERÊNCIAS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" numero-aba="5" data-toggle="tab" href="#menu4">FINALIZAÇÃO</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="row">
                <div class="col-12 text-center aviso-campo-obrigatorio">
                    <small class="text-center">Todos os campos com (*) são de preenchimento obrigatório</small>
                </div>
                <div class="col-12 text-center aviso-campo-obrigatorio">
                    <small class="text-center">Pretendente entende-se por pessoa que vai alugar o imóvel e comprovar a renda.</small>
                </div>
            </div>

            <div class="tab-content">
