                    <div id="home" class="container tab-pane active"><br>
                        
<!-------------------------------------------------------------------------------------------------------------->

                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                <div aviso-bloco class="form-group">
                                    <label for="nome_completo">Nome Completo:*</label>
                                    <input aviso-campo type="text" class="form-control text-uppercase" name="nome_completo" id="nome_cliente" placeholder="Nome Completo" value="<?php echo set_value('nome_completo',$query['nome_completo'])?>" >
                                    <small aviso-texto class="form-text text-muted">Nome Completo sem Abreviações.</small>
                                    <div class="invalid-feedback">
                                        Insira o Seu Nome
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                <div aviso-bloco class="form-group">
                                    <label for="telefone">Telefone Fixo:*</label>
                                    <input aviso-campo type="text" class="form-control" name="telefone"  mascara="tel" placeholder="Telefone" value="<?php echo set_value('telefone',$query['telefone'])?>" >
                                    <small aviso-texto class="form-text text-muted">
                                        Com DDD.
                                    </small>
                                    <small aviso-texto class="form-text text-muted">
                                        Em caso de Não Aplicável, colocar (00) 0000-0000.
                                    </small>
                                    <div class="invalid-feedback">
                                        Insira o Telefone Fixo
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                <div aviso-bloco class="form-group">
                                    <label for="celular">Telefone Celular:*</label>
                                    <input aviso-campo type="text" class="form-control" name="celular"  mascara="cel" placeholder="Celular" value="<?php echo set_value('celular',$query['celular'])?>" >
                                    <div class="invalid-feedback">
                                        Insira o Telefone Celular
                                    </div>
                                    <small aviso-texto class="form-text text-muted">
                                        Com DDD.
                                    </small>
                                    <small aviso-texto class="form-text text-muted">
                                        Em caso de Não Aplicável, colocar (00) 0000-0000.
                                    </small>
                                </div>
                            </div>
                        </div>

<!---------------------------------------------------------------------------------------------------------------->

                        <div class="row">

                            <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                <div aviso-bloco class="form-group">
                                    <label for="data_nascimento">Data de Nascimento:*</label>
                                    <input aviso-campo type="text" class="form-control" name="data_nascimento" mascara="data" placeholder="Data de Nascimento" value="<?php echo set_value('data_nascimento', formatar_data($query['data_nascimento']))?>" >
                                    <div class="invalid-feedback">
                                        Insira a Data de Nascimento
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                <div aviso-bloco class="form-group">
                                    <label for="email_pretendente">E-mail Pessoal:*</label>
                                    <input aviso-campo type="text" class="form-control" name="email_pessoal" placeholder="E-mail do Pessoal do Fiador" value="<?php echo set_value('email_pessoal',$query['email_pessoal'])?>" >
                                    <small aviso-texto class="form-text text-muted">
                                          Digite o e-mail que você visualiza com maior frequência.<br>
                                          Este e-mail receberá os boletos de aluguel.
                                    </small>
                                    <div class="invalid-feedback">
                                        Insira o E-mail do Pretendente
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                <div aviso-bloco class="form-group">
                                    <label for="nacionalidade">Nacionalidade:*</label>
                                    <input aviso-campo type="text" class="form-control text-uppercase" name="nacionalidade" placeholder="Nacionalidade" value="<?php echo set_value('nacionalidade',$query['nacionalidade'])?>" >
                                    <div class="invalid-feedback">
                                        Insira a sua Nacionalidade
                                    </div>
                                </div>
                            </div>                            
                            
                        </div>

<!---------------------------------------------------------------------------------------------------------------->

                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                <div aviso-bloco class="form-group">
                                    <label for="cpf">CPF:*</label>
                                    <input aviso-campo type="text" class="form-control" name="cpf" mascara="cpf" placeholder="CPF" value="<?php echo set_value('cpf',$query['cpf'])?>" >
                                    <small aviso-texto class="form-text text-muted">
                                        CPF do pretendente. Caso seja estrangeiro, converse com
                                        o consultor ou consultura que está lhe atendendo.
                                    </small>
                                    <div class="invalid-feedback">
                                        Insira o seu CPF
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                <div aviso-bloco class="form-group">
                                  <label for="rg">RG:*</label>
                                  <input aviso-campo type="text" class="form-control" name="rg" placeholder="RG" value="<?php echo set_value('rg',$query['rg'])?>" >
                                    <small aviso-texto class="form-text text-muted">
                                        RG do pretendente. Caso seja estrangeiro, converse com
                                        o consultor ou consultura que está lhe atendendo.
                                    </small>
                                    <div class="invalid-feedback">
                                        Insira o seu RG
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                <div aviso-bloco class="form-group">
                                    <label for="expedicao">Local de Expedição:*</label>
                                    <input aviso-campo type="text" class="form-control" name="expedicao" placeholder="Local de Expedição" value="<?php echo set_value('expedicao',$query['expedicao'])?>" >
                                    <div class="invalid-feedback">
                                        Insira o Local de Expedição
                                    </div>
                                </div>
                            </div>
                        </div>

<!-------------------------------------------------------------------------------------------------------------------->

                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                <div aviso-bloco class="form-group">
                                    <label for="data_expedicao">Data da Expedição do RG:*</label>
                                    <input aviso-campo type="text" class="form-control" name="data_expedicao" mascara="data" placeholder="Data da Expedição do RG" value="<?php echo set_value('data_expedicao', formatar_data($query['data_expedicao']))?>" >
                                    <div class="invalid-feedback">
                                        Insira a Data da Expedição do RG
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                <div aviso-bloco class="form-group">
                                  <label for="profissao">Profissão:*</label>
                                  <input aviso-campo type="text" class="form-control text-uppercase" name="profissao" placeholder="Profissão" value="<?php echo set_value('profissao',$query['profissao'])?>" >
                                    <div class="invalid-feedback">
                                        Insira a seu Profissão
                                    </div>
                                </div>
                            </div>
                        </div>

                        
<!-------------------------------------------------------------------------------------------------------------------->

                        <hr>
                        
<!-------------------------------------------------------------------------------------------------------------------->

                        <div class="row">
                            <div class="col">
                                <label>Aposentado?</label><br>
                                <div class="custom-controls-stacked d-block my-3">
                                    <label class="custom-control custom-radio">
                                        <input id="radioStacked1" name="aposentado" value="1" togvis-button="aposentado" type="radio" <?php if($query['aposentado']==='1') echo "checked" ?>>
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">Sim</span>
                                    </label>
                                    <label class="custom-control custom-radio">
                                        <input id="radioStacked2" name="aposentado" value="0" togvis-button="nao-aposentado" type="radio" <?php if($query['aposentado']==='0') echo "checked" ?>>
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">Não</span>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div togvis-id="nao-aposentado" togvis-estado='hide' togvis-outros='esconder' togvis-grupo='grupo-aposentado'>
                            
<!-------------------------------------------------------------------------------------------------------------------->               

                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                    <div aviso-bloco class="form-group">
                                      <label for="nome_empresa">Nome da Empresa:*</label>
                                      <input aviso-campo type="text" class="form-control text-uppercase" name="nome_empresa" placeholder="Nome da Empresa que Trabalha" value="<?php echo set_value('nome_empresa',$query['nome_empresa'])?>">
                                    </div>
                                </div>

                                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                    <div aviso-bloco class="form-group">
                                      <label for="telefone_empresa">Telefone Comercial:*</label>
                                      <input aviso-campo type="text" class="form-control" name="telefone_empresa" mascara="tel" placeholder="Telefone Comercial (com DDD)" value="<?php echo set_value('telefone_empresa',$query['telefone_empresa'])?>">
                                        <small aviso-texto class="form-text text-muted">
                                           Com DDD
                                        </small>
                                        <small aviso-texto class="form-text text-muted">
                                           Em caso de Não Aplicável, colocar (00) 0000-0000
                                        </small>
                                    </div>
                                </div>

                                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                    <div aviso-bloco class="form-group">
                                        <label for="email_comercial">E-mail Comercial:*</label>
                                        <input aviso-campo type="text" class="form-control" name="email_comercial" placeholder="E-mail Comercial" value="<?php echo set_value('email_comercial',$query['telefone_empresa'],$query['telefone_empresa'])?>">
                                        <small aviso-texto class="form-text text-muted">Caso não possua, escreva 'não aplicável'</small>
                                    </div>
                                </div>
                            </div>

<!-------------------------------------------------------------------------------------------------------------------->

                            <div class="row">
                                
                                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                    <div aviso-bloco class="form-group">
                                      <label for="data_admissao">Data de Admissão:*</label>
                                      <input aviso-campo type="text" class="form-control" name="data_admissao" mascara="data" placeholder="Data de Admissão" value="<?php echo set_value('data_admissao', formatar_data($query['data_admissao']))?>">
                                    </div>
                                </div>

                                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                    <div aviso-bloco class="form-group">
                                      <label for="tempo_empresa">Tempo de Empresa:*</label>
                                      <input aviso-campo type="text" class="form-control" name="tempo_empresa" placeholder="Tempo de Empresa" value="<?php echo set_value('tempo_empresa',$query['tempo_empresa'])?>">
                                    </div>
                                </div>

                                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                    <div class="row">
                                        <div class="col">
                                            <div aviso-bloco class="form-group">
                                                <label for="cep">CEP da Empresa:*</label>
                                                <input aviso-campo cep-bloco="empresa" class="form-control" type="text" mascara="cep" name="cep" value="<?php echo set_value('cep',$query['cep']); ?>" placeholder="CEP da Empresa">
                                            </div>
                                        </div>
                                        <!--<div aviso-bloco class="form-group">
                                            <div class="col" style="margin-top: 37%;">
                                                <a class="btn btn-primary" href="#" 
                                                 title="Obter endereço automático" 
                                                 endereco-auto="click">
                                                <i class="material-icons" style="font-size:22px;">gps_fixed</i>
                                              </a>
                                            </div>
                                        </div>-->
                                    </div>
                                </div>
                            </div>

<!-------------------------------------------------------------------------------------------------------------------->

                            <div class="row">
                                
                                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                    <div aviso-bloco class="form-group">
                                        <label for="logradouro">Logradouro da Empresa:*</label>
                                        <input aviso-campo cep-rua="empresa" class="form-control text-uppercase" type="text" endereco-auto="logradouro" value="<?php echo set_value('logradouro',$query['logradouro']); ?>" name="logradouro" placeholder="Logradouro">
                                        <small aviso-texto class="form-text text-muted">
                                           Rua, Avenida, Estrada, Travessa, etc.
                                        </small>
                                    </div>
                                </div>

                                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                    <div aviso-bloco class="form-group">
                                        <label for="numero">Número (Empresa):*</label>
                                        <input aviso-campo class="form-control" type="text" value="<?php echo set_value('numero',$query['numero']); ?>" name="numero" placeholder="Número">
                                    </div>
                                </div>

                                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                    <div aviso-bloco class="form-group">
                                        <label for="complemento">Complemento (Empresa):*</label>
                                        <input aviso-campo class="form-control text-uppercase" type="text" endereco-auto="complemento" value="<?php echo set_value('complemento',$query['complemento']); ?>" name="complemento" placeholder="Complemento">
                                    </div>
                                </div>
                            </div>

<!-------------------------------------------------------------------------------------------------------------------->

                            <div class="row">
                                
                                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                    <div aviso-bloco class="form-group">
                                        <label for="bairro">Bairro da Empresa:*</label>
                                        <input aviso-campo cep-bairro="empresa" class="form-control text-uppercase" type="text" endereco-auto="bairro" value="<?php echo set_value('bairro',$query['bairro']); ?>" name="bairro" placeholder="Bairro">
                                    </div>
                                </div>

                                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
			                        <div aviso-bloco class="form-group">
			                            <label for="complemento_cliente">Cidade:*</label>
			                            <input aviso-campo cep-cidade="empresa" class="form-control text-uppercase" type="text" value="" name="" placeholder="Cidade">
			                        </div>
			                        <div class="invalid-feedback">
			                            Insira o Estado
			                        </div>
			                    </div>

			                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
			                        <div aviso-bloco class="form-group">
			                            <label for="bairro_cliente">Estado:*</label>
			                            <input aviso-campo cep-uf="empresa" class="form-control text-uppercase" type="text" value="" name="" placeholder="Estado" >
			                            <div class="invalid-feedback">
			                                Insira o Estado
			                            </div>
			                        </div>
			                    </div>
                            </div>

                            <div class="row">
                            	<div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                    <div aviso-bloco class="form-group">
                                        <?php 
                                        $array_pais['']='Selecione o País';
                                        foreach ($paises as $value) {
                                            $array_pais[$value->id_pais]=$value->nome_pais;
                                        }?>
                                        <label for="id_pais">País da Empresa:*</label>
                                        <?php echo form_dropdown('id_pais', $array_pais, set_value('id_pais',$query['id_pais']), 'class="form-control"') ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div togvis-id="aposentado" togvis-estado='hide' togvis-outros='esconder' togvis-grupo='grupo-aposentado'>
                        </div>


                        
<!-------------------------------------------------------------------------------------------------------------------->

                        <hr>
                        
<!-------------------------------------------------------------------------------------------------------------------->

                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                <div aviso-bloco class="form-group">
                                    <label for="salario">Salário do Pretendente (em R$):*</label>
                                    <input aviso-campo class="form-control" type="text" value="<?php echo set_value('salario',$query['salario']); ?>" name="salario" placeholder="Salário" >
                                    <div class="invalid-feedback">
                                        Insira o Seu Salário
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                <div aviso-bloco class="form-group">
                                    <label for="outras_rendas">Outras Rendas (especificar):*</label>
                                    <textarea class="form-control" type="text" rows="5"  name="outras_rendas" placeholder="Outras Rendas" ><?php echo set_value('outras_rendas',$query['outras_rendas']); ?></textarea>
                                    <small aviso-texto class="form-text text-muted">
                                          Em caso de dúvidas, converse com o consultor que está lhe atendendo.
                                          Caso não possua outras rendas escreva: 'não aplicável'
                                    </small>
                                    <div class="invalid-feedback">
                                        Insira Suas Outras Rendas
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                <div aviso-bloco class="form-group">
                                    <label for="total_renda">Total da Renda (em R$):*</label>
                                    <input aviso-campo class="form-control" type="text" value="<?php echo set_value('total_renda',$query['total_renda']); ?>" name="total_renda" placeholder="Total da Renda" >
                                    <div class="invalid-feedback">
                                        Insira o Total de Sua Renda
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>