            <div id="menu0" class="container tab-pane fade"><br>
                
<!------------------------------------------------------------------------------------------------------------------>

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="row">
                            <div class="col"> 
                                <div aviso-bloco class="form-group">
                                    <label for="cep_cliente">CEP:*</label>
                                    <input aviso-campo cep-bloco="fiador" class="form-control" type="text" mascara="cep" name="cep_fiador" value="<?php echo set_value('cep_fiador'); ?>" placeholder="CEP" >
                                    <div class="invalid-feedback">
                                        Insira o CEP
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="logradouro_cliente">Logradouro:*</label>
                            <input aviso-campo cep-rua="fiador" class="form-control text-uppercase" type="text" value="<?php echo set_value('logradouro_fiador'); ?>" name="logradouro_fiador" placeholder="Logradouro" >
                            <small aviso-texto class="form-text text-muted">Rua, Avenida, Estrada, Travessa, etc.</small>
                            
                            <div class="invalid-feedback">
                                Insira o Logradouro
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="numero_cliente">Número:*</label>
                            <input aviso-campo class="form-control" type="text" value="<?php echo set_value('numero_fiador'); ?>" name="numero_fiador" placeholder="Número" >
                            <div class="invalid-feedback">
                                Insira o Número
                            </div>
                        </div>
                    </div>
                </div>

<!------------------------------------------------------------------------------------------------------------------>

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="complemento_cliente">Complemento:*</label>
                            <input aviso-campo class="form-control text-uppercase" type="text" value="<?php echo set_value('complemento_fiador'); ?>" name="complemento_fiador" placeholder="Complemento">
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="bairro_cliente">Bairro:*</label>
                            <input aviso-campo cep-bairro="fiador" class="form-control text-uppercase" type="text" value="<?php echo set_value('bairro_fiador'); ?>" name="bairro_fiador" placeholder="Bairro" >
                            <div class="invalid-feedback">
                                Insira o Bairro
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="id_pais_cliente">País:*</label>
                            <?php 
                                $array_pais['']='Selecione o País';
                                foreach ($paises as $value) {
                                    $array_pais[$value->id_pais]=$value->nome_pais;
                                }
                            ?>
                            <?php echo form_dropdown('id_pais_fiador', $array_pais, set_value('id_pais_fiador',33), 'class="form-control" ') ?>
                            <div class="invalid-feedback">
                                Selecione um País
                            </div>
                        </div>
                    </div>
                </div>

<!------------------------------------------------------------------------------------------------------------------>

                <div class="row">
                	<div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="complemento_cliente">Cidade:*</label>
                            <input aviso-campo cep-cidade="fiador" class="form-control text-uppercase" type="text" value="" name="" placeholder="Cidade">
                        </div>
                        <div class="invalid-feedback">
                            Insira o Estado
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="bairro_cliente">Estado:*</label>
                            <input aviso-campo cep-uf="fiador" class="form-control text-uppercase" type="text" value="" name="" placeholder="Estado" >
                            <div class="invalid-feedback">
                                Insira o Estado
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div aviso-bloco class="form-group">
                            <label for="tempo_residencia">Tempo na Residência Atual:*</label>
                            <input aviso-campo class="form-control" type="text" value="<?php echo set_value('tempo_residencia'); ?>" name="tempo_residencia" placeholder="Tempo na Residência Atual" >
                            <div class="invalid-feedback">
                                Insira o Tempo na Residência Atual
                            </div>
                        </div>
                    </div> 
                </div>
                
<!-------------------------------------------------------------------------------------------------------------------->


                <div class="row">
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <label>Residência Própria?</label><br>
                        <div class="custom-controls-stacked d-block my-3">
                            <label class="custom-control custom-radio">
                                <input aviso-campo id="radioStacked1" name="residencia_propria" value="1" togvis-button="residencia-propria" type="radio" >
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description">Sim</span>
                            </label>
                            <label class="custom-control custom-radio">
                                <input aviso-campo id="radioStacked2" name="residencia_propria" value="0" togvis-button="nao-residencia-propria" type="radio" >
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description">Não</span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>