<?php
defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container-fluid">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="caixa-adm-cliente-central">
            <h2>Minha Documentação</h2>
        </div>
        <hr>
        <div class="alert alert-info caixa-informacoes" role="alert">
            <ul>
                <li>
                    TODAS AS INFORMAÇÕES E DOCUMENTOS INSERIDOS NESTA PÁGINA
                    SERÃO ANALISADOS EXCLUSIVAMENTE PELA BAGGIO IMÓVEIS, SENDO
                    TODA E QUALQUER INFORMAÇÃO PREENCHIDA DE CARÁTER SIGILOSO,
                    PARA PRESERVAR A SEGURANÇA DOS NOSSOS CLIENTES.
                </li> 
                <li>
                    TODOS OS DOCUMENTOS ANEXADOS DEVEM ESTAR LEGÍVEIS. ANTES DE 
                    REALIZAR O UPLOAD CERTIFIQUE-SE QUE ELE TEM A QUALIDADE 
                    NECESSÁRIA PARA LEITURA CLARA.
                </li>
                <li>
                    É PERMITIDO O ENVIO DE TODAS AS EXTENSÕES DE FOTOS 
                    (PNG, JPG, JPGE, ETC.) E TAMBÉM ARQUIVOS EM PDF. PORÉM,
                    EM CASO DE ARQUIVOS ILEGÍVEIS, SERÁ SOLICITADO NOVO ENVIO 
                    DO DOCUMENTO.
                </li>
                <li>
                    EM QUALQUER MOMENTO PODEM SER SOLICITADOS OUTROS DOCUMENTOS.
                </li>
                <li>
                    CASO VOCÊ TENHA RECEBIDO UM PEDIDO DE DOCUMENTO QUE NÃO 
                    TENHA NOS CAMPOS ABAIXO, UTILIZE O CAMPO AO FINAL INTITULADO
                    DE 'OUTROS DOCUMENTOS', ESPECIFICANDO O DOCUMENTO QUE ESTÁ 
                    ENVIADO.
                </li>
            </ul>
        </div>
        <div class="row align-items-center">
            <?php 
            if(isset($tem_dados)){ ?>
            <div class="col-6 form-group">
                <a class="btn btn-primary btn-block" href='<?php echo base_url("cli/adicionar-documentacao/1"); ?>' role="button">
                    <label class="fonte-media comportamento-botao">
                        <i class="fa fa-file-text-o" aria-hidden="true"></i>
                        Adicionar Documentação Pretendente
                    </label>
                </a>
            </div>
                <?php if(isset($tem_segundo_pretendente)){ ?>
                    <div class="col-6 form-group">
                        <a class="btn btn-primary btn-block" href='<?php echo base_url("cli/adicionar-documentacao/0"); ?>' role="button">
                            <label class="fonte-media comportamento-botao">
                                <i class="fa fa-file-text-o" aria-hidden="true"></i>
                               Adicionar Documentação 2º Pretendente
                            </label>
                        </a>
                    </div>
                <?php }?>
            <?php if(isset($verifica_fiador)){ ?>
                <div class="col-6 form-group">
                    <a class="btn btn-primary btn-block" href='<?php echo base_url("cli/adicionar-documentacao-fiador"); ?>' role="button">
                        <label class="fonte-media comportamento-botao">
                            <i class="fa fa-file-text-o" aria-hidden="true"></i>
                          Adicionar Documentação do Fiador
                        </label>
                    </a>
                </div>
            <?php
            }else{?>
            <div class="col-12 form-group">
                <p>Para o Envio da Documentação do Fiador é Necessário Primeiro Adcionar A ficha do Fiador <a href="<?php echo base_url('/cli/adicionar-ficha-cadastro-fiador') ?>">Clicando aqui</a></p>
            </div>
            <?php }} else { ?>
            <div class="col-12 form-group">
                <p>Para o Envio da Documentação é Necessário Primeiro Você Completar Seu Cadastro <a href="<?php echo base_url('cli/ficha-cadastro') ?>">Clicando aqui</a></p>
            </div>
            <?php } ?>
        </div>
    </div>
</div>

