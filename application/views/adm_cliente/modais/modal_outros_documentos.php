<div class="modal fade" id="outrosDocumentosModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Enviar Outros Documentos</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form action="<?php echo base_url("cli/validacao-envio-outros-documentos/".$this->uri->segment(3))?>" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
            <div class="modal-body">
                <label><b>É PERMITIDO O ENVIO DAS SEGUINTES EXTENSÕES DE FOTOS/DOCUMENTOS(PNG, JPG, PDF)</b></label>
                <div class="form-group">
                    <p>Envie qualquer Outro Documento </p>
                   <input type="file" name="outros_documentos[]" multiple />
                </div>
            </div>
            <input hidden name="principal" value="<?php echo $this->uri->segment(3)?>"/>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
              <button type="submit" class="btn btn-primary">Salvar</button>
            </div>
        </form>
    </div>
  </div>
</div>