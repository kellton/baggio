<div class="modal fade" id="visualizaComprovanteResidenteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Visualizar Documentação dos Residentes</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body">
            <b><p>Residentes:</p></b>
            <div class="row">
            <?php foreach ($residentes as $value) { ?>
                    <div class="col-auto">
                        <div class="form-group">
                            <?php echo $value->nome_completo_residente ?>
                        </div>
                    </div>
                <?php
                } ?>
            </div>
             <div class="row justify-content-center">
                <?php 
                $contador=1;
                foreach ($documentos_residentes as $value) {
                ?>
                 <div class="col-auto espacamento-comprovante-endereco">
                    <a class="btn btn-info" href="<?php echo base_url("publico/uploads/".md5($id_cliente)).'/'.$value->url_documento ?>" target="_blank">
                        Visualizar Documento <?php echo $contador?></a>
                </div>
                <?php $contador++;
                }?>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
    </div>
  </div>
</div>