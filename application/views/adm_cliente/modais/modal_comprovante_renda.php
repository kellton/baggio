<div class="modal fade" id="rendaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Enviar Comprovamte de Renda</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form action="<?php echo base_url("cli/validacao-envio-comprovacao-renda/".$this->uri->segment(3))?>" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
            <div class="modal-body">
                <label><b>É PERMITIDO O ENVIO DAS SEGUINTES EXTENSÕES DE FOTOS/DOCUMENTOS(PNG, JPG, PDF)</b></label>
                <div class="form-group">
                    <label>Selecione Abaixo a Categoria</label>
                    <select name="categoria" class="form-control" togvis-select>
                       <option togvis-esconder-grupo="select-divs" value="" selected="selected">Selecione Uma Categoria</option>
                       <option togvis-option="assalariado" togvis-outros="esconder" value="Assalariado" >Assalariado</option>
                       <option togvis-option="empresario" togvis-outros="esconder" value="Empresário" >Empresário</option>
                       <option togvis-option="autonomo" togvis-outros="esconder" value="Profissional Autônomo ou Profissional Liberal" >Profissional Autônomo ou Profissional Liberal</option>
                       <option togvis-option="aposentado" togvis-outros="esconder" value="Aposentado" >Aposentado</option>
                    </select>
                </div>
                <div togvis-id="assalariado" togvis-estado='hide' togvis-grupo="select-divs">
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <p><b>HOLERITE - 3 Últimos Meses:*</b></p>
                                <input type="file" name="holerite[]" multiple/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <label>Você é Funcionário Público?</label><br>
                            <div class="custom-controls-stacked d-block my-3">
                                <label class="custom-control custom-radio">
                                    <input id="radioStacked1" name="funcionario_publico" value="1" togvis-button="funcionario-publico" type="radio" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Sim</span>
                                </label>
                                <label class="custom-control custom-radio">
                                    <input id="radioStacked2" name="funcionario_publico" value="0" togvis-button="nao-funcionario-publico" type="radio" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Não</span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div togvis-id="nao-funcionario-publico" togvis-estado='hide' togvis-outros='esconder' togvis-grupo='grupo-funcionario-publico'>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <p><b>Carteira de Trabalho:*</b></p>
                                    <input type="file" name="carteira_trabalho[]" multiple/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div togvis-id="funcionario-publico" togvis-estado='hide' togvis-outros='esconder' togvis-grupo='grupo-funcionario-publico'></div>
                    <div class="row">
                        <div class="col">
                            <label>Você Declara Imposto de Renda?</label><br>
                            <div class="custom-controls-stacked d-block my-3">
                                <label class="custom-control custom-radio">
                                    <input id="radioStacked1" name="imposto_renda" value="1" togvis-button="imposto-renda" type="radio" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Sim</span>
                                </label>
                                <label class="custom-control custom-radio">
                                    <input id="radioStacked2" name="imposto_renda" value="0" togvis-button="nao-imposto-renda" type="radio" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Não</span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div togvis-id="imposto-renda" togvis-estado='hide' togvis-outros='esconder' togvis-grupo='grupo-imposto-renda'>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <p><b>Imposto de Renda Completo:*</b></p>
                                    <input type="file" name="imposto_renda_file[]" multiple/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div togvis-id="nao-imposto-renda" togvis-estado='hide' togvis-outros='esconder' togvis-grupo='grupo-imposto-renda'></div>
                </div>
                <div togvis-id="empresario" togvis-estado='hide' togvis-grupo="select-divs">
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <p><b>Contrato Social:*</b></p>
                                <input type="file" name="contrato_social[]" multiple/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <p><b>Decore Dos 3 (Três) Últimos Meses:*</b></p>
                                <input type="file" name="decore_empresario[]" multiple/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <p><b>Declaração de IRPF Com Recibo de Entrega (Exercício Anterior):*</b></p>
                                <input type="file" name="imposto_renda_empresario[]" multiple/>
                            </div>
                        </div>
                    </div>
                </div>
                <div togvis-id="autonomo" togvis-estado='hide' togvis-grupo="select-divs">
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <p><b>3 (Três) Últimos Extratos Bancários:*</b></p>
                                <input type="file" name="extrato_bancario[]" multiple/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <p><b>Decore Dos 3 (Três) Últimos Meses:*</b></p>
                                <input type="file" name="decore_autonomo[]" multiple/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <p><b>Declaração de IRPF Com Recibo de Entrega (Exercício Anterior):*</b></p>
                                <input type="file" name="imposto_renda_autonomo[]" multiple/>
                            </div>
                        </div>
                    </div>
                </div>
                <div togvis-id="aposentado" togvis-estado='hide' togvis-grupo="select-divs">
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <p><b>3 (Três) Últimos Extratos do Benefício:*</b></p>
                                <input type="file" name="extrato_beneficio[]" multiple/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <p><b>Cartão do Benefício:*</b></p>
                                <input type="file" name="cartao_beneficio[]" multiple/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <p><b>Declaração de IRPF Com Recibo de Entrega (Exercício Anterior):*</b></p>
                                <input type="file" name="imposto_renda_aposentado[]" multiple/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input hidden name="principal" value="<?php echo $this->uri->segment(3)?>"/>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
              <button type="submit" class="btn btn-primary">Salvar</button>
            </div>
        </form>
    </div>
  </div>
</div>