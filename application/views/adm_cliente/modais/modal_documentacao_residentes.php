<div class="modal fade" id="documentacaoResidentesModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Enviar Comprovante de Endereço</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form action="<?php echo base_url("cli/validacao-envio-documentacao-residente/".$this->uri->segment(3))?>" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
            <div class="modal-body">
                <label><b>É PERMITIDO O ENVIO DAS SEGUINTES EXTENSÕES DE FOTOS/DOCUMENTOS(PNG, JPG, PDF)</b></label>
                <label>Ao Lado do Nome de Cada Residente Envio a Documentação Respectiva de Cada Um:</label>
                <hr>
                <?php 
                foreach ($residentes as $value) { ?>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <b><?php echo $value->nome_completo_residente ?></b>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <input type="file" name="documentacao_residente[]" multiple/>
                            </div>
                        </div>
                    </div>
                <hr>
                <?php
                }
                ?>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
              <button type="submit" class="btn btn-primary">Salvar</button>
            </div>
        </form>
    </div>
  </div>
</div>