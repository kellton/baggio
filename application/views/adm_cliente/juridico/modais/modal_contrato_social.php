<div class="modal fade" id="contratoSocial" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Enviar Contrato Social</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form action="<?php echo base_url("cli-juridico/validacao-envio-contrato-social")?>" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
            <div class="modal-body">
                <label><b>É PERMITIDO O ENVIO DAS SEGUINTES EXTENSÕES DE FOTOS/DOCUMENTOS(PNG, JPG, PDF)</b></label>
                <div class="form-group col-md-offset-1 col-md-10">
                    <label><b>Contrato Social:</b></label>
                    <input type="file" name="documentos[]" multiple/>
                </div>
            </div>
            <input hidden name="tipo_documento" value="16"/>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
              <button type="submit" class="btn btn-primary">Salvar</button>
            </div>
        </form>
    </div>
  </div>
</div>