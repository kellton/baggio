<?php
defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container-fluid">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="caixa-adm-cliente-central">
           <h2>Ficha de Cadastro</h2>
        </div>
        <hr>
        <div class="alert alert-info caixa-informacoes" role="alert">
            <ul>
                <li>
                    TODAS AS INFORMAÇÕES E DOCUMENTOS INSERIDOS NESTA PÁGINA
                    SERÃO ANALISADOS EXCLUSIVAMENTE PELA BAGGIO IMÓVEIS, SENDO
                    TODA E QUALQUER INFORMAÇÃO PREENCHIDA DE CARÁTER SIGILOSO,
                    PARA PRESERVAR A SEGURANÇA DOS NOSSOS CLIENTES.
                </li> 
                <li>
                    TODOS OS DOCUMENTOS ANEXADOS DEVEM ESTAR LEGÍVEIS. ANTES DE 
                    REALIZAR O UPLOAD CERTIFIQUE-SE QUE ELE TEM A QUALIDADE 
                    NECESSÁRIA PARA LEITURA CLARA.
                </li>
                <li>
                    É PERMITIDO O ENVIO DE TODAS AS EXTENSÕES DE FOTOS 
                    (PNG, JPG, JPGE, ETC.) E TAMBÉM ARQUIVOS EM PDF. PORÉM,
                    EM CASO DE ARQUIVOS ILEGÍVEIS, SERÁ SOLICITADO NOVO ENVIO 
                    DO DOCUMENTO.
                </li>
                <li>
                    EM QUALQUER MOMENTO PODEM SER SOLICITADOS OUTROS DOCUMENTOS.
                </li>
                <li>
                    CASO VOCÊ TENHA RECEBIDO UM PEDIDO DE DOCUMENTO QUE NÃO 
                    TENHA NOS CAMPOS ABAIXO, UTILIZE O CAMPO AO FINAL INTITULADO
                    DE 'OUTROS DOCUMENTOS', ESPECIFICANDO O DOCUMENTO QUE ESTÁ 
                    ENVIADO.
                </li>
            </ul>
        </div>
        <div class="row justify-content-center">
            <p>Para o Envio da Documentação é Necessário Primiero Você Completar Seu Cadastro <a href="<?php echo base_url('cli-juridico/adicionar-ficha') ?>">Clicando aqui</a></p>
        </div>
    </div>
</div>
