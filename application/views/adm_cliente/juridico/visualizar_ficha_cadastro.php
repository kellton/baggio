<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="banner-background-adm d-print-none">
    <img width="100%" src="<?php echo base_url('publico/imagens/area-adm/cliente/banner@2x.png'); ?>">
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="titulo-sessao-ficha espacamento-titulo-pagina">
                <div class="container">
                    <h3 class="display-6">Dados da empresa</h3>
                </div>
            </div>
            
<!-------------------------------------------------------------------------------------------------------------------------->

            <div class="row">
                <div class="col">
                    <p><b>Razão Social:</b> <?php echo $query['razao_social'] ?></p>
                </div>

                <div class="col">
                    <p><b>Tipo:</b> <?php echo $query['tipo'] ?></p>
                </div>

                <?php if($query['tipo']=="OUTRO"){ ?>
                    <div class="col">
                        <p><b>Tipo de Empresa:</b> <?php echo $query['tipo_empresa'] ?></p>
                    </div>
                <?php }?>
            </div>
            
<!-------------------------------------------------------------------------------------------------------------------------->

            <div class="row">
                <div class="col">
                    <p><b>Data da Fundação:</b> <?php echo formatar_data($query['fundacao']) ?></p>
                </div>

                <div class="col">
                    <p><b>CNPJ:</b> <?php echo $query['cnpj'] ?></p>
                </div>

                <div class="col">
                    <p><b>Número de Registro na Junta Comercial:</b> <?php echo $query['junta_comercial'] ?></p>
                </div>
            </div>
            
<!-------------------------------------------------------------------------------------------------------------------------->

            <div class="row">
                <div class="col">
                    <p><b>Número Inscrição Estadual:</b> <?php echo $query['inscricao_estadual'] ?></p>
                </div>

                <div class="col">
                    <p><b>Capital Social da Empresa:</b> <?php echo $query['capital_social'] ?></p>
                </div>
                <div class="col">
                    <p><b>Ramo de Atividade:</b> <?php echo $query['ramo_atividade'] ?></p>
                </div>
            </div>
            
<!-------------------------------------------------------------------------------------------------------------------------->

            <div class="row">
                <div class="col">
                    <p><b>CEP:</b> <?php echo $query['cep'] ?></p>
                </div>
                <div class="col">
                    <p><b>Logradouro:</b> <?php echo $query['logradouro'] ?></p>
                </div>
                <div class="col">
                    <p><b>Número:</b> <?php echo $query['numero'] ?></p>
                </div>
            </div>
            
<!-------------------------------------------------------------------------------------------------------------------------->

            <div class="row">
                <div class="col-4">
                    <p><b>Complemento:</b> <?php echo $query['complemento'] ?></p>
                </div>
                <div class="col-4">
                    <p><b>Bairro:</b> <?php echo $query['bairro'] ?></p>
                </div>
                <div class="col-4">
                    <p><b>País:</b> <?php echo $query['nome_pais'] ?></p>
                </div>
            </div>
            
<!-------------------------------------------------------------------------------------------------------------------------->

            <div class="row">
                <div class="col-4">
                    <p><b>E-mail Comercial:</b> <?php echo $query['email_comercial'] ?></p>
                </div>
                <div class="col-4">
                    <p><b>Telefone Comercial:</b> <?php echo $query['telefone_comercial'] ?></p>
                </div>
            </div>
            <hr>
            
<!------------------------------------------------------------------------------------------------------------------------------>

            <div class="titulo-sessao-ficha espacamento-titulo-pagina">
                <div class="container">
                    <h3 class="display-6">Dados do Sócio Representante</h3>
                </div>
            </div>
            <hr>
            
<!-------------------------------------------------------------------------------------------------------------------------->

            <div class="row">
                <div class="col">
                    <p><b>Nome Completo:</b> <?php echo $query['nome_completo'] ?></p>
                </div>

                <div class="col">
                    <p><b>Telefone Fixo:</b> <?php echo $query['telefone_fixo'] ?></p>
                </div>

                <div class="col">
                    <p><b>Celular:</b> <?php echo $query['celular'] ?></p>
                </div>
            </div>
            
<!-------------------------------------------------------------------------------------------------------------------------->

            <div class="row">
                <div class="col">
                    <p><b>E-mail:</b> <?php echo $query['email'] ?></p>
                </div>

                <div class="col">
                    <p><b>Data de Nascimento:</b> <?php echo formatar_data($query['data_nascimento']) ?></p>
                </div>

                <div class="col">
                    <p><b>Nacionalidade:</b> <?php echo $query['nacionalidade'] ?></p>
                </div>
            </div>
            
<!-------------------------------------------------------------------------------------------------------------------------->

            <div class="row">

                <div class="col">
                    <p><b>CPF:</b> <?php echo $query['cpf'] ?></p>
                </div>
                
                <div class="col">
                    <p><b>RG:</b> <?php echo $query['rg'] ?></p>
                </div>

                <div class="col">
                    <p><b>Local de Expedição:</b> <?php echo $query['expedicao_rg'] ?></p>
                </div>
            </div>
            
<!-------------------------------------------------------------------------------------------------------------------------->

            <div class="row">

                <div class="col">
                    <p><b>Data da Expedição do RG:</b> <?php echo formatar_data($query['data_expedicao']) ?></p>
                </div>
                
                <div class="col">
                    <p><b>Estado Civil:</b> <?php echo $query['nome_estado_civil'] ?></p>
                </div>

                <div class="col">
                    <p><b>Profissão:</b> <?php echo $query['profissao'] ?></p>
                </div>
            </div>
            
<!-------------------------------------------------------------------------------------------------------------------------->

            <div class="row">

                <div class="col">
                    <p><b>Cargo Exercido:</b> <?php echo $query['cargo'] ?></p>
                </div>
                
                <div class="col">
                    <p><b>Salário:</b> <?php echo $query['salario'] ?></p>
                </div>

                <div class="col">
                    <p><b>Outras Rendas:</b> <?php echo $query['outras_rendas'] ?></p>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <p><b>Renda Total:</b> <?php echo $query['renda_total'] ?></p>
                </div>
            </div>

            <hr>
            
<!-------------------------------------------------------------------------------------------------------------------------->

            <div class="row">
                <div class="col">
                    <p><b>Residência Própria?</b> <?php if($query['residencia'] =="1"){ echo 'Sim';}else{echo 'Não';}  ?></p>
                </div>
                <div class="col">
                    <p><b>CEP da Residência:</b> <?php echo $query['cep_cliente'] ?></p>
                </div>
                <div class="col">
                    <p><b>Logradouro:</b> <?php echo $query['logradouro_cliente'] ?></p>
                </div>
            </div>
            
<!-------------------------------------------------------------------------------------------------------------------------->

            <div class="row">
                <div class="col">
                    <p><b>Número:</b> <?php echo $query['numero_cliente']  ?></p>
                </div>
                <div class="col">
                    <p><b>Complemento:</b> <?php echo $query['complemento_cliente'] ?></p>
                </div>
                <div class="col">
                    <p><b>Bairro:</b> <?php echo $query['bairro_cliente'] ?></p>
                </div>
            </div>
            
<!-------------------------------------------------------------------------------------------------------------------------->

            <div class="row">
                <div class="col">
                    <p><b>País:</b> <?php echo $query['nome_pais'] ?></p>
                </div>
            </div>
            <hr>
            
<!------------------------------------------------------------------------------------------------------------------------------>

            <div class="titulo-sessao-ficha espacamento-titulo-pagina">
                <div class="container">
                    <h3 class="display-6">Dados dos Sócios ou Diretores</h3>
                </div>
            </div>
            <hr>
            <?php 
            $i=1;
            foreach ($socios as $value) { ?>
            <h4>Dados do Sócio/Diretor Nº<?php echo $i; ?></h4><hr>
                
<!-------------------------------------------------------------------------------------------------------------------------->

                <div class="row">
                    <div class="col">
                        <p><b>Nome:</b> <?php echo $value->nome_completo_diretor ?></p>
                    </div>
                    <div class="col">
                        <p><b>Cargo Exercido:</b> <?php echo $value->cargo_diretor ?></p>
                    </div>
                    <div class="col">
                        <p><b>Data de Nascimento:</b> <?php echo formatar_data($value->data_nascimento_diretor) ?></p>
                    </div>
                </div>
                
    <!-------------------------------------------------------------------------------------------------------------------------->

                <div class="row">
                    <div class="col">
                        <p><b>Nacionalidade:</b> <?php echo $value->nacionalidade_diretor ?></p>
                    </div>
                    <div class="col">
                        <p><b>CPF:</b> <?php echo $value->cpf_diretor ?></p>
                    </div>
                    <div class="col">
                        <p><b>RG:</b> <?php echo $value->rg_diretor ?></p>
                    </div>
                </div>
                
    <!-------------------------------------------------------------------------------------------------------------------------->

                <div class="row">
                    <div class="col">
                        <p><b>Estado Civil:</b> <?php echo $value->nome_estado_civil ?></p>
                    </div>
                    <div class="col">
                        <p><b>E-mail Pessoal:</b> <?php echo $value->email_pessoal_diretor ?></p>
                    </div>
                    <div class="col">
                        <p><b>E-mail Comercial:</b> <?php echo $value->email_comercial_diretor ?></p>
                    </div>
                </div>
                
    <!-------------------------------------------------------------------------------------------------------------------------->

                <div class="row">
                    <div class="col">
                        <p><b>Telefone:</b> <?php echo $value->telefone_diretor ?></p>
                    </div>
                </div>
                
    <!-------------------------------------------------------------------------------------------------------------------------->

                <div class="row">
                    <div class="col">
                        <p><b>CEP:</b> <?php echo $value->cep_diretor ?></p>
                    </div>
                    <div class="col">
                        <p><b>Logradouro:</b> <?php echo $value->logradouro_diretor ?></p>
                    </div>
                    <div class="col">
                        <p><b>Número:</b> <?php echo $value->numero_diretor ?></p>
                    </div>
                </div>
                
    <!-------------------------------------------------------------------------------------------------------------------------->

                <div class="row">
                    <div class="col">
                        <p><b>Complemento:</b> <?php echo $value->complemento_diretor ?></p>
                    </div>
                    <div class="col">
                        <p><b>Bairro:</b> <?php echo $value->bairro_diretor ?></p>
                    </div>
                    <div class="col">
                        <p><b>Pais:</b> <?php echo $value->nome_pais ?></p>
                    </div>
                </div>
                <hr>
            <?php $i++; }?>
            
<!------------------------------------------------------------------------------------------------------------------------------>

            <div class="titulo-sessao-ficha espacamento-titulo-pagina">
                <div class="container">
                    <h3 class="display-6">Propriedades da Empresa</h3>
                </div>
            </div>
            <hr>
             <?php foreach ($propriedades as $value) { ?>
                
    <!-------------------------------------------------------------------------------------------------------------------------->

                <div class="row">
                    <div class="col">
                        <p><b>Nº da Matícula do Imóvel:</b> <?php echo $value->matricula_imovel ?></p>
                    </div>
                    <div class="col">
                        <p><b>Circunscrição:</b> <?php echo $value->circunscricao ?></p>
                    </div>
                    <div class="col">
                        <p><b>Marca Do Veículo:</b> <?php echo $value->veiculo_marca ?></p>
                    </div>
                    <div class="col">
                        <p><b>Renavam:</b> <?php echo $value->renavam ?></p>
                    </div>
                </div>
            <?php } ?>
            <hr>
            
<!------------------------------------------------------------------------------------------------------------------------------>

            <div class="titulo-sessao-ficha espacamento-titulo-pagina">
                <div class="container">
                    <h3 class="display-6">Referências Comerciais</h3>
                </div>
            </div>
            <hr>
             <?php foreach ($referencias as $value) { 
                 if($value->tipo_referencia==2){?>
                
        <!-------------------------------------------------------------------------------------------------------------------------->

                <div class="row">
                    <div class="col">
                        <p><b>Nome:</b> <?php echo $value->nome_referencia ?></p>
                    </div>
                    <div class="col">
                        <p><b>Endereço:</b> <?php echo $value->endereco_referencia ?></p>
                    </div>
                    <div class="col">
                        <p><b>Telefone:</b> <?php echo $value->telefone_referencia ?></p>
                    </div>
                </div>
                 <?php }} ?>
            <hr>
            
<!------------------------------------------------------------------------------------------------------------------------------>

            <div class="titulo-sessao-ficha espacamento-titulo-pagina">
                <div class="container">
                    <h3 class="display-6">Referências Bacárias</h3>
                </div>
            </div>
            <hr>
             <?php foreach ($referencias_bancarias as $value) { ?>
                
    <!-------------------------------------------------------------------------------------------------------------------------->

                <div class="row">
                    <div class="col">
                        <p><b>Banco:</b> <?php echo $value->banco_referencia ?></p>
                    </div>
                    <div class="col">
                        <p><b>Telefone da Agencia:</b> <?php echo $value->telefone_banco_referencia ?></p>
                    </div>
                    <div class="col">
                        <p><b>Nome da Agência:</b> <?php echo $value->nome_agencia_referencia ?></p>
                    </div>
                    <div class="col">
                        <p><b>Número da Conta:</b> <?php echo $value->numero_conta_referencia ?></p>
                    </div>
                </div>
            <?php } ?>
            <hr>
            
<!------------------------------------------------------------------------------------------------------------------------------>

            <div class="titulo-sessao-ficha espacamento-titulo-pagina">
                <div class="container">
                    <h3 class="display-6">Informações Complementares</h3>
                </div>
            </div>
            <hr>
            
<!-------------------------------------------------------------------------------------------------------------------------->

            <div class="row">
                <div class="col">
                    <p class="text-justify"> <b>Informações Complementares:</b> <?php echo $query['observacoes_complementares'] ?></p>
                </div>
            </div>
            <a href="<?php echo base_url('cli-juridico');?>" class="btn btn-primary d-print-none">Voltar</a>
        </div>
    </div>
</div>