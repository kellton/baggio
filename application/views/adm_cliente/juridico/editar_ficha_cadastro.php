<div class="container-fluid bold-label">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="caixa-adm-cliente-central">
            <h2>Editar Ficha de Cadastro Pessoa Jurídica</h2>
        </div>
        <hr>
        <div class="jumbotron jumbotron-fluid">
            <div class="container">
                <h1 class="display-4">Dados da empresa / Pretendente da locação</h1>
                <p class="lead">Caso tenha dúvida em relação a algum campo abaixo, favor contatar o consultor ou consultora que está lhe atendendo.</p>
            </div>
        </div>
       <?php echo validation_errors('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>','</div>'); ?>
        <form action="<?php echo base_url('cli-juridico/adicionar-ficha-cadastro/validacao') ?>" method="POST" accept-charset="utf-8" id="needs-validation">
            <small>Todos os campos com * são obrigatórios</small>
            <div class="form-group">
                <label for="razao_social">Razão Social:*</label>
                <input type="text" class="form-control text-uppercase" name="razao_social" id="nome_cliente" placeholder="Razão Social" value="<?php echo set_value('razao_social',$query['razao_social'])?>" >
                <small class="form-text text-muted">Nome da empresa conforme descrito no cartão CNPJ.</small>
                <div class="invalid-feedback">
                    Insira a Razão Social
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <label>Tipo?</label><br>
                    <div class="custom-controls-stacked d-block my-3">
                        <label class="custom-control custom-radio">
                            <input name="tipo" value="LTDA" togvis-button="tipo" type="radio" class="custom-control-input" required <?php if($query['tipo']=="LTDA") echo 'checked'; ?>>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">LTDA</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input name="tipo" value="S.A" togvis-button="tipo" type="radio" class="custom-control-input" required <?php if($query['tipo']=="S.A") echo 'checked'; ?>>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">S.A</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input name="tipo" value="MEI" togvis-button="tipo" type="radio" class="custom-control-input" required <?php if($query['tipo']=="MEI") echo 'checked'; ?>>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">MEI</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input name="tipo" value="EIRELI" togvis-button="tipo" type="radio" class="custom-control-input" required <?php if($query['tipo']=="EIRELI") echo 'checked'; ?>>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">EIRELI</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input name="tipo" value="ME" togvis-button="tipo" type="radio" class="custom-control-input" required <?php if($query['tipo']=="ME") echo 'checked'; ?>>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">ME</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input name="tipo" value="OUTRO" togvis-button="tipo-outro" type="radio" class="custom-control-input" required <?php if($query['tipo']=="OUTRO") echo 'checked'; ?>>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">OUTRO</span>
                        </label>
                    </div>
                </div>
            </div>
           <div togvis-id="tipo-outro" togvis-estado='hide' togvis-outros='esconder' togvis-grupo='grupo-tipo'>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="tipo_empresa">Tipo de Empresa:*</label>
                            <input type="text" class="form-control" name="tipo_empresa" placeholder="Tipo de Empresa" value="<?php echo set_value('tipo_empresa',$query['tipo_empresa'])?>">
                        </div>
                    </div>
                </div>
            </div>
            <div togvis-id="tipo" togvis-estado='hide' togvis-outros='esconder' togvis-grupo='grupo-tipo'></div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="fundacao">Data da Fundação:*</label>
                        <input type="text" class="form-control" name="fundacao" mascara="data" placeholder="Data da Fundação" value="<?php echo set_value('fundacao', formatar_data($query['fundacao']))?>" >
                        <div class="invalid-feedback">
                            Insira a Data da Fundação
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="cnpj">CNPJ:*</label>
                        <input type="text" class="form-control" name="cnpj" mascara="cnpj" placeholder="CNPJ" value="<?php echo set_value('cnpj',$query['cnpj'])?>" >
                        <div class="invalid-feedback">
                            Insira o CNPJ
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="junta_comercial">Número de Registro na Junta Comercial:*</label>
                        <input type="text" class="form-control" name="junta_comercial" placeholder="Número de Registro na Junta Comercial" value="<?php echo set_value('junta_comercial',$query['junta_comercial'])?>" >
                        <div class="invalid-feedback">
                            Insira o Número de Registro na Junta Comercial
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="inscricao_estadual">Número Inscrição Estadual:*</label>
                        <input type="text" class="form-control" name="inscricao_estadual" placeholder="Número Inscrição Estadual" value="<?php echo set_value('inscricao_estadual',$query['inscricao_estadual'])?>" >
                        <div class="invalid-feedback">
                            Insira o Número Inscrição Estadual
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="capital_social">Capital Social da Empresa (em RS):*</label>
                        <input type="text" class="form-control" name="capital_social" placeholder="Capital Social da Empresa (em RS)" value="<?php echo set_value('capital_social',$query['capital_social'])?>" >
                        <small class="form-text text-muted">Caso tenha dúvidas, converse com o consultor ou consultora que está lhe atendendo.</small>
                        <div class="invalid-feedback">
                            Insira o Capital Social da Empresa
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="ramo_atividade">Ramo de Atividade Para a Locação Pretendida:*</label>
                        <input type="text" class="form-control text-uppercase" name="ramo_atividade" placeholder="Ramo de Atividade" value="<?php echo set_value('inscricao_estadual',$query['inscricao_estadual'])?>" >
                        <small class="form-text text-muted">Especifique o ramo de negócios que a empresa tem atividade atualmente.</small>
                        <div class="invalid-feedback">
                            Insira o Ramo de Atividade
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="cep">CEP da Empresa:*</label>
                                <input class="form-control" type="text" mascara="cep" name="cep" value="<?php echo set_value('cep',$query['cep']); ?>" placeholder="CEP da Empresa" >
                                <div class="invalid-feedback">
                                    Insira o CEP
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col" style="margin-top: 37%;">
                                <a class="btn btn-primary" href="#" 
                                 title="Obter endereço automático"
                                 endereco-auto="click">
                                <i class="material-icons" style="font-size:22px;">gps_fixed</i>
                              </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="logradouro">Logradouro Atual (Rua, Avenida, Estrada, Travessa, etc.):*</label>
                        <input class="form-control text-uppercase" type="text" endereco-auto="logradouro" value="<?php echo set_value('logradouro',$query['logradouro']); ?>" name="logradouro" placeholder="Logradouro" >
                        <div class="invalid-feedback">
                            Insira o Logradouro
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="numero">Número:*</label>
                        <input class="form-control" type="text" value="<?php echo set_value('numero',$query['numero']); ?>" name="numero" placeholder="Número" >
                        <div class="invalid-feedback">
                            Insira o Número
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="complemento">Complemento:</label>
                        <input class="form-control text-uppercase" type="text" endereco-auto="complemento" value="<?php echo set_value('complemento',$query['complemento']); ?>" name="complemento" placeholder="Complemento">
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="bairro">Bairro:*</label>
                        <input class="form-control text-uppercase" type="text" endereco-auto="bairro" value="<?php echo set_value('bairro',$query['bairro']); ?>" name="bairro" placeholder="Bairro" >
                        <div class="invalid-feedback">
                            Insira o Bairro
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <?php 
                        $array_pais['']='Selecione o País';
                        foreach ($paises as $value) {
                            $array_pais[$value->id_pais]=$value->nome_pais;
                        }?>
                        <label for="id_pais">País:*</label>
                        <?php echo form_dropdown('id_pais', $array_pais, set_value('id_pais',33), 'class="form-control" ') ?>
                        <div class="invalid-feedback">
                            Insira o País
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="email_comercial">E-mail Comercial:*</label>
                        <input class="form-control" type="text" value="<?php echo set_value('email_comercial',$query['email_comercial']); ?>" name="email_comercial" placeholder="E-mail Comercial" >
                        <small>E-mail que os boletos de aluguel serão enviados.</small>
                        <div class="invalid-feedback">
                            Insira o E-mail Comercial
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="telefone_comercial">Telefone Comercial:*</label>
                        <input class="form-control" type="text" mascara="tel" value="<?php echo set_value('telefone_comercial',$query['telefone_comercial']); ?>" name="telefone_comercial" placeholder="Telefone Comercial" >
                        <div class="invalid-feedback">
                            Insira o Telefone Comercial
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
               <div class="col">
                   <div class="form-group">
                        <input type="submit" name="enviar" class="btn btn-success" value="Salvar"/>
                   </div>
               </div>
            </div>
            <div class="jumbotron jumbotron-fluid">
                <div class="container">
                    <h1 class="display-4">Dados do Sócio Representante</h1>
                    <p class="lead">O responsável pela locação deve estar inserido no contrato social. Para demais casos, converse com o consultor(a) que está lhe atendendo.</p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="nome_completo">Nome Completo:*</label>
                        <input class="form-control text-uppercase" type="text" value="<?php echo set_value('nome_completo',$query['nome_completo']); ?>" name="nome_completo" placeholder="Nome Completo" >
                        <div class="invalid-feedback">
                            Insira o Nome Completo
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="telefone_fixo">Telefone Fixo:*</label>
                        <input class="form-control" type="text" mascara="tel" value="<?php echo set_value('telefone_fixo',$query['telefone_fixo']); ?>" name="telefone_fixo" placeholder="Telefone Fixo" >
                        <div class="invalid-feedback">
                            Insira o Telefone Fixo
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="celular">Celular:*</label>
                        <input class="form-control" type="text" mascara="cel" value="<?php echo set_value('celular',$query['celular']); ?>" name="celular" placeholder="Celular" >
                        <div class="invalid-feedback">
                            Insira o Celular
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="email">E-mail:*</label>
                        <input class="form-control text-uppercase" type="text" value="<?php echo set_value('email',$query['email']); ?>" name="email" placeholder="E-mail" >
                        <div class="invalid-feedback">
                            Insira o E-mail
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="email">E-mail Comercial:*</label>
                        <input class="form-control text-uppercase" type="text" value="<?php echo set_value('email_comercial_socio',$query['email_comercial_socio']); ?>" name="email_comercial_socio" placeholder="E-mail Comercial" >
                        <div class="invalid-feedback">
                            Insira o E-mail
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="data_nascimento">Data de Nascimento:*</label>
                        <input class="form-control" type="text" mascara="data" value="<?php echo set_value('data_nascimento', formatar_data($query['data_nascimento'])); ?>" name="data_nascimento" placeholder="Data de Nascimento" >
                        <div class="invalid-feedback">
                            Insira a Data de Nasciimento
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="nacionalidade">Nacionalidade*</label>
                        <input class="form-control text-uppercase" type="text" value="<?php echo set_value('nacionalidade',$query['nacionalidade']); ?>" name="nacionalidade" placeholder="Nacionalidade" >
                        <div class="invalid-feedback">
                            Insira a Nacionalidade
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="cpf">CPF:*</label>
                        <input class="form-control" type="text" mascara="cpf" value="<?php echo set_value('cpf',$query['cpf']); ?>" name="cpf" placeholder="CPF" >
                        <small>Caso seja estrangeiro, converse com o consultor ou consultora que está lhe atendendo.</small>
                        <div class="invalid-feedback">
                            Insira o CPF
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="rg">RG*</label>
                        <input class="form-control" type="text" value="<?php echo set_value('rg',$query['rg']); ?>" name="rg" placeholder="RG" >
                        <small>Caso seja estrangeiro, converse com o consultor ou consultora que está lhe atendendo.</small>
                        <div class="invalid-feedback">
                            Insira o RG
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="expedicao_rg">Local de Expedição do RG:*</label>
                        <input class="form-control" type="text" value="<?php echo set_value('expedicao_rg',$query['expedicao_rg']); ?>" name="expedicao_rg" placeholder="Local de Expedição do RG" >
                        <small>Caso seja estrangeiro, converse com o consultor ou consultora que está lhe atendendo.</small>
                        <div class="invalid-feedback">
                            Insira o Local de Expedição
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="data_expedicao">Data de Expedição:*</label>
                        <input class="form-control" type="text" mascara="data" value="<?php echo set_value('data_expedicao', formatar_data($query['data_expedicao'])); ?>" name="data_expedicao" placeholder="Data de Expedição" >
                        <small>Caso seja estrangeiro, converse com o consultor ou consultora que está lhe atendendo.</small>
                        <div class="invalid-feedback">
                            Insira oa Data de Expedição
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="id_estado_civil">Estado Civil:*</label>
                        <select name="id_estado_civil" class="form-control"  togvis-select required>
                            <option togvis-esconder-grupo="select-divs" value="" >Selecione o Estado Civil</option>
                            <option togvis-esconder-grupo="select-divs" value="1" <?php if($query['id_estado_civil']=="1"){ echo 'selected="selected"';}?> >Não Informado</option>
                            <option togvis-option="casado" togvis-outros="esconder" value="2" <?php if($query['id_estado_civil']=="2"){ echo 'selected="selected"';}?>>Casado(a)</option>
                            <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="3" <?php if($query['id_estado_civil']=="3"){ echo 'selected="selected"';}?>>Solteiro(a)</option>
                            <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="4" <?php if($query['id_estado_civil']=="4"){ echo 'selected="selected"';}?>>Divorciado(a)</option>
                            <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="5" <?php if($query['id_estado_civil']=="5"){ echo 'selected="selected"';}?>>Viúvo(a)</option>
                            <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="6" <?php if($query['id_estado_civil']=="6"){ echo 'selected="selected"';}?>>União Estável</option>
                        </select>
                        <div class="invalid-feedback">
                            Insira o seu Estado Civil
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                      <label for="profissao">Profissão:*</label>
                      <input type="text" class="form-control text-uppercase" name="profissao" placeholder="Profissão" value="<?php echo set_value('profissao',$query['profissao'])?>" >
                        <div class="invalid-feedback">
                            Insira a Profissão
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="cargo">Cargo Exercido:*</label>
                        <input type="text" class="form-control text-uppercase" name="cargo" placeholder="Cargo Exercido" value="<?php echo set_value('cargo',$query['cargo'])?>" >
                        <div class="invalid-feedback">
                            Insira o Cargo Exercido
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="salario">Salário (em R$):*</label>
                        <input type="text" class="form-control" name="salario" placeholder="Salário" value="<?php echo set_value('salario',$query['salario'])?>" >
                        <div class="invalid-feedback">
                            Insira o Salário
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <label>Residencia Própria?</label><br>
                    <div class="custom-controls-stacked d-block my-3">
                        <label class="custom-control custom-radio">
                            <input name="residencia" value="1" type="radio" class="custom-control-input" <?php if($query['residencia']==1) echo 'checked';?>>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Sim</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input name="residencia" value="0" type="radio" class="custom-control-input" <?php if($query['residencia']==0) echo 'checked';?>>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Não</span>
                        </label>
                    </div>
                </div>
            </div> 
            <div class="row">
               <div class="col">
                   <div class="form-group">
                        <input type="submit" name="enviar" class="btn btn-success" value="Salvar"/>
                   </div>
               </div>
            </div>
            <hr>
             <div class="row">
                <div class="col">
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="cep_cliente">CEP da Residência:*</label>
                                <input class="form-control" type="text" mascara="cep" name="cep_cliente" value="<?php echo set_value('cep_cliente',$query['cep_cliente']); ?>" placeholder="CEP" >
                                <div class="invalid-feedback">
                                    Insira o CEP
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="logradouro_cliente">Logradouro da Residência:*</label>
                        <input class="form-control text-uppercase" type="text" value="<?php echo set_value('logradouro_cliente',$query['logradouro_cliente']); ?>" name="logradouro_cliente" placeholder="Logradouro" >
                        <div class="invalid-feedback">
                            Insira o Logradouro
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="numero_cliente">Número da Residência:*</label>
                        <input class="form-control" type="text" value="<?php echo set_value('numero_cliente',$query['numero_cliente']); ?>" name="numero_cliente" placeholder="Número" >
                        <div class="invalid-feedback">
                            Insira o Número
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="complemento_cliente">Complemento da Residência:</label>
                        <input class="form-control text-uppercase" type="text" value="<?php echo set_value('complemento_cliente',$query['complemento_cliente']); ?>" name="complemento_cliente" placeholder="Complemento">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="bairro_cliente">Bairro da Residência:*</label>
                        <input class="form-control text-uppercase" type="text" value="<?php echo set_value('bairro_cliente',$query['bairro_cliente']); ?>" name="bairro_cliente" placeholder="Bairro" >
                        <div class="invalid-feedback">
                            Insira o Bairro
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="id_pais_cliente">País:*</label>
                        <?php echo form_dropdown('id_pais_cliente', $array_pais, set_value('id_pais_cliente',33), 'class="form-control" ') ?>
                        <div class="invalid-feedback">
                            Selecione um País
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="outras_rendas">Outras Rendas (especificar):*</label>
                        <textarea class="form-control" type="text" rows="5" name="outras_rendas" placeholder="Outras Rendas (especificar)" ><?php echo set_value('outras_rendas',$query['outras_rendas']); ?></textarea>
                        <small>Caso não possua, escrever 'não aplicável'.</small>
                        <div class="invalid-feedback">
                            Insira Outras Rendas
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="renda_total">Renda Total (em R$):*</label>
                        <input class="form-control" type="text" value="<?php echo set_value('renda_total',$query['renda_total']); ?>" name="renda_total" placeholder="Renda Total (em R$)" >
                        <div class="invalid-feedback">
                            Insira a Renda Total
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
               <div class="col">
                   <div class="form-group">
                        <input type="submit" name="enviar" class="btn btn-success" value="Salvar"/>
                   </div>
               </div>
            </div>
            <div class="jumbotron jumbotron-fluid">
                <div class="container">
                    <h1 class="display-4">Dados dos Sócios ou Diretores</h1>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <label>A Ampresa Possui Quantos Sócios ou Diretores?</label><br>
                    <div class="custom-controls-stacked d-block my-3">
                        <label class="custom-control custom-radio">
                            <input name="socios" value="1" togvis-button="socios1" type="radio" class="custom-control-input" <?php if($query['socios']==1) echo 'checked'; ?>>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">1</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input name="socios" value="2" togvis-button="socios2" type="radio" class="custom-control-input" <?php if($query['socios']==2) echo 'checked'; ?>>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">2</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input name="socios" value="3" togvis-button="socios3" type="radio" class="custom-control-input" <?php if($query['socios']==3) echo 'checked'; ?>>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">3</span>
                        </label>
                        <small>Máximo 3 Sócios ou Diretores para o cadastro</small>
                    </div>
                </div>
            </div>
            <div togvis-id="socios1" togvis-estado='hide' togvis-outros='esconder' togvis-grupo='grupo-socios'>
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <h1 class="display-4">Dados do Sócio/Diretor Nº1</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="nome_completo_diretor">Nome Completo:*</label>
                            <input class="form-control text-uppercase" type="text" value="<?php echo set_value('nome_completo_diretor[0]',isset($socios[0]->nome_completo_diretor) ? $socios[0]->nome_completo_diretor : ''); ?>" name="nome_completo_diretor[]" placeholder="Nome Completo">
                            <div class="invalid-feedback">
                                Insira o Nome Completo
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="cargo_diretor">Cargo Exercido:*</label>
                            <input type="text" class="form-control text-uppercase" name="cargo_diretor[]" placeholder="Cargo Exercido" value="<?php echo set_value('cargo_diretor[0]',isset($socios[0]->cargo_diretor) ? $socios[0]->cargo_diretor: '')?>">
                            <div class="invalid-feedback">
                                Insira o Cargo Exercido
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="data_nascimento_diretor">Data de Nascimento:*</label>
                            <input class="form-control" type="text" mascara="data" value="<?php echo set_value('data_nascimento_diretor[0]',isset($socios[0]->data_nascimento_diretor) ? $socios[0]->data_nascimento_diretor: ''); ?>" name="data_nascimento_diretor[]" placeholder="Data de Nascimento">
                            <div class="invalid-feedback">
                                Insira a Data de Nasciimento
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="nacionalidade_diretor">Nacionalidade*</label>
                            <input class="form-control text-uppercase" type="text" value="<?php echo set_value('nacionalidade_diretor[0]',isset($socios[0]->nacionalidade_diretor) ? $socios[0]->nacionalidade_diretor: ''); ?>" name="nacionalidade_diretor[]" placeholder="Nacionalidade">
                            <div class="invalid-feedback">
                                Insira a Nacionalidade
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="cpf_diretor">CPF:*</label>
                            <input class="form-control" type="text" mascara="cpf" value="<?php echo set_value('cpf_diretor[0]',isset($socios[0]->cpf_diretor) ? $socios[0]->cpf_diretor: ''); ?>" name="cpf_diretor[]" placeholder="CPF">
                            <small>Caso seja estrangeiro, converse com o consultor ou consultora que está lhe atendendo.</small>
                            <div class="invalid-feedback">
                                Insira o CPF
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="rg_diretor">RG*</label>
                            <input class="form-control" type="text" value="<?php echo set_value('rg_diretor[0]',isset($socios[0]->rg_diretor) ? $socios[0]->rg_diretor: ''); ?>" name="rg_diretor[]" placeholder="RG">
                            <small>Caso seja estrangeiro, converse com o consultor ou consultora que está lhe atendendo.</small>
                            <div class="invalid-feedback">
                                Insira o RG
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="id_estado_civil_diretor">Estado Civil:*</label>
                           <select name="id_estado_civil_diretor[]" class="form-control"  togvis-select >
                                <option togvis-esconder-grupo="select-divs" value="" >Selecione o Estado Civil</option>
                                <option togvis-esconder-grupo="select-divs" value="1" <?php echo isset($socios[0]->id_estado_civil) && $socios[0]->id_estado_civil=="1" ? 'selected="selected"': ''; ?> >Não Informado</option>
                                <option togvis-option="casado" togvis-outros="esconder" value="2" <?php echo isset($socios[0]->id_estado_civil) && $socios[0]->id_estado_civil=="2" ? 'selected="selected"': ''; ?>>Casado(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="3" <?php echo isset($socios[0]->id_estado_civil) && $socios[0]->id_estado_civil=="3" ? 'selected="selected"': ''; ?>>Solteiro(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="4" <?php echo isset($socios[0]->id_estado_civil) && $socios[0]->id_estado_civil=="4" ? 'selected="selected"': ''; ?>>Divorciado(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="5" <?php echo isset($socios[0]->id_estado_civil) && $socios[0]->id_estado_civil=="5" ? 'selected="selected"': ''; ?>>Viúvo(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="6" <?php echo isset($socios[0]->id_estado_civil) && $socios[0]->id_estado_civil=="6" ? 'selected="selected"': ''; ?>>União Estável</option>
                            </select>
                            <div class="invalid-feedback">
                                Insira o Estado Civil
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                   <div class="col">
                       <div class="row">
                           <div class="col">
                               <div class="form-group">
                                   <label for="cep_diretor">CEP da Residência:*</label>
                                   <input class="form-control" type="text" mascara="cep" name="cep_diretor[]" value="<?php echo set_value('cep_diretor[0]',isset($socios[0]->cep_diretor) ? $socios[0]->cep_diretor: ''); ?>" placeholder="CEP">
                                   <div class="invalid-feedback">
                                       Insira o CEP
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
                   <div class="col">
                       <div class="form-group">
                           <label for="logradouro_diretor">Logradouro da Residência:*</label>
                           <input class="form-control text-uppercase" type="text" value="<?php echo set_value('logradouro_diretor[0]',isset($socios[0]->logradouro_diretor) ? $socios[0]->logradouro_diretor: ''); ?>" name="logradouro_diretor[]" placeholder="Logradouro">
                           <div class="invalid-feedback">
                               Insira o Logradouro
                           </div>
                       </div>
                   </div>
                   <div class="col">
                       <div class="form-group">
                           <label for="numero_diretor">Número da Residência:*</label>
                           <input class="form-control" type="text" value="<?php echo set_value('numero_diretor[0]',isset($socios[0]->numero_diretor) ? $socios[0]->numero_diretor: ''); ?>" name="numero_diretor[]" placeholder="Número">
                           <div class="invalid-feedback">
                               Insira o Número
                           </div>
                       </div>
                   </div>
                   <div class="col">
                       <div class="form-group">
                           <label for="complemento_diretor">Complemento da Residência:</label>
                           <input class="form-control text-uppercase" type="text" value="<?php echo set_value('complemento_diretor[0]',isset($socios[0]->complemento_diretor) ? $socios[0]->complemento_diretor: ''); ?>" name="complemento_diretor[]" placeholder="Complemento">
                       </div>
                   </div>
               </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="bairro_diretor">Bairro da Residência:*</label>
                            <input class="form-control text-uppercase" type="text" value="<?php echo set_value('bairro_diretor[0]',isset($socios[0]->bairro_diretor) ? $socios[0]->bairro_diretor: ''); ?>" name="bairro_diretor[]" placeholder="Bairro">
                            <div class="invalid-feedback">
                                Insira o Bairro
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="id_pais_diretor">País:*</label>
                            <?php echo form_dropdown('id_pais_diretor[]', $array_pais, set_value('id_pais_cliente[0]',33), 'class="form-control" ') ?>
                            <div class="invalid-feedback">
                                Selecione um País
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="email_pessoal_diretor">E-mail Pessoal:*</label>
                            <input class="form-control" type="text" value="<?php echo set_value('email_pessoal_diretor[0]',isset($socios[0]->email_pessoal_diretor) ? $socios[0]->email_pessoal_diretor: ''); ?>" name="email_pessoal_diretor[]" placeholder="E-mail Comercial">
                            <small>E-mail que os boletos de aluguel serão enviados.</small>
                            <div class="invalid-feedback">
                                Insira o E-mail Comercial
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="email_comercial_diretor">E-mail Comercial:*</label>
                            <input class="form-control" type="text" value="<?php echo set_value('email_comercial_diretor[0]',isset($socios[0]->email_comercial_diretor) ? $socios[0]->email_comercial_diretor: ''); ?>" name="email_comercial_diretor[]" placeholder="E-mail Comercial">
                            <small>E-mail que os boletos de aluguel serão enviados.</small>
                            <div class="invalid-feedback">
                                Insira o E-mail Comercial
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="telefone_diretor">Telefone:*</label>
                            <input class="form-control" type="text" mascara="tel" value="<?php echo set_value('telefone_diretor[0]',isset($socios[0]->telefone_diretor) ? $socios[0]->telefone_diretor: ''); ?>" name="telefone_diretor[]" placeholder="Telefone">
                            <div class="invalid-feedback">
                                Insira o E-mail Comercial
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div togvis-id="socios2" togvis-estado='hide' togvis-outros='esconder' togvis-grupo='grupo-socios'>
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <h1 class="display-4">Dados do Sócio/Diretor Nº1</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="nome_completo_diretor">Nome Completo:*</label>
                            <input class="form-control text-uppercase" type="text" value="<?php echo set_value('nome_completo_diretor[0]',isset($socios[0]->nome_completo_diretor) ? $socios[0]->nome_completo_diretor : ''); ?>" name="nome_completo_diretor[]" placeholder="Nome Completo">
                            <div class="invalid-feedback">
                                Insira o Nome Completo
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="cargo_diretor">Cargo Exercido:*</label>
                            <input type="text" class="form-control text-uppercase" name="cargo_diretor[]" placeholder="Cargo Exercido" value="<?php echo set_value('cargo_diretor[0]',isset($socios[0]->cargo_diretor) ? $socios[0]->cargo_diretor: '')?>">
                            <div class="invalid-feedback">
                                Insira o Cargo Exercido
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="data_nascimento_diretor">Data de Nascimento:*</label>
                            <input class="form-control" type="text" mascara="data" value="<?php echo set_value('data_nascimento_diretor[0]',isset($socios[0]->data_nascimento_diretor) ? $socios[0]->data_nascimento_diretor: ''); ?>" name="data_nascimento_diretor[]" placeholder="Data de Nascimento">
                            <div class="invalid-feedback">
                                Insira a Data de Nasciimento
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="nacionalidade_diretor">Nacionalidade*</label>
                            <input class="form-control text-uppercase" type="text" value="<?php echo set_value('nacionalidade_diretor[0]',isset($socios[0]->nacionalidade_diretor) ? $socios[0]->nacionalidade_diretor: ''); ?>" name="nacionalidade_diretor[]" placeholder="Nacionalidade">
                            <div class="invalid-feedback">
                                Insira a Nacionalidade
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="cpf_diretor">CPF:*</label>
                            <input class="form-control" type="text" mascara="cpf" value="<?php echo set_value('cpf_diretor[0]',isset($socios[0]->cpf_diretor) ? $socios[0]->cpf_diretor: ''); ?>" name="cpf_diretor[]" placeholder="CPF">
                            <small>Caso seja estrangeiro, converse com o consultor ou consultora que está lhe atendendo.</small>
                            <div class="invalid-feedback">
                                Insira o CPF
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="rg_diretor">RG*</label>
                            <input class="form-control" type="text" value="<?php echo set_value('rg_diretor[0]',isset($socios[0]->rg_diretor) ? $socios[0]->rg_diretor: ''); ?>" name="rg_diretor[]" placeholder="RG">
                            <small>Caso seja estrangeiro, converse com o consultor ou consultora que está lhe atendendo.</small>
                            <div class="invalid-feedback">
                                Insira o RG
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="id_estado_civil_diretor">Estado Civil:*</label>
                           <select name="id_estado_civil_diretor[]" class="form-control"  togvis-select >
                                <option togvis-esconder-grupo="select-divs" value="" >Selecione o Estado Civil</option>
                                <option togvis-esconder-grupo="select-divs" value="1" <?php echo isset($socios[0]->id_estado_civil) && $socios[0]->id_estado_civil=="1" ? 'selected="selected"': ''; ?> >Não Informado</option>
                                <option togvis-option="casado" togvis-outros="esconder" value="2" <?php echo isset($socios[0]->id_estado_civil) && $socios[0]->id_estado_civil=="2" ? 'selected="selected"': ''; ?>>Casado(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="3" <?php echo isset($socios[0]->id_estado_civil) && $socios[0]->id_estado_civil=="3" ? 'selected="selected"': ''; ?>>Solteiro(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="4" <?php echo isset($socios[0]->id_estado_civil) && $socios[0]->id_estado_civil=="4" ? 'selected="selected"': ''; ?>>Divorciado(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="5" <?php echo isset($socios[0]->id_estado_civil) && $socios[0]->id_estado_civil=="5" ? 'selected="selected"': ''; ?>>Viúvo(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="6" <?php echo isset($socios[0]->id_estado_civil) && $socios[0]->id_estado_civil=="6" ? 'selected="selected"': ''; ?>>União Estável</option>
                            </select>
                            <div class="invalid-feedback">
                                Insira o Estado Civil
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                   <div class="col">
                       <div class="row">
                           <div class="col">
                               <div class="form-group">
                                   <label for="cep_diretor">CEP da Residência:*</label>
                                   <input class="form-control" type="text" mascara="cep" name="cep_diretor[]" value="<?php echo set_value('cep_diretor[0]',isset($socios[0]->cep_diretor) ? $socios[0]->cep_diretor: ''); ?>" placeholder="CEP">
                                   <div class="invalid-feedback">
                                       Insira o CEP
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
                   <div class="col">
                       <div class="form-group">
                           <label for="logradouro_diretor">Logradouro da Residência:*</label>
                           <input class="form-control text-uppercase" type="text" value="<?php echo set_value('logradouro_diretor[0]',isset($socios[0]->logradouro_diretor) ? $socios[0]->logradouro_diretor: ''); ?>" name="logradouro_diretor[]" placeholder="Logradouro">
                           <div class="invalid-feedback">
                               Insira o Logradouro
                           </div>
                       </div>
                   </div>
                   <div class="col">
                       <div class="form-group">
                           <label for="numero_diretor">Número da Residência:*</label>
                           <input class="form-control" type="text" value="<?php echo set_value('numero_diretor[0]',isset($socios[0]->numero_diretor) ? $socios[0]->numero_diretor: ''); ?>" name="numero_diretor[]" placeholder="Número">
                           <div class="invalid-feedback">
                               Insira o Número
                           </div>
                       </div>
                   </div>
                   <div class="col">
                       <div class="form-group">
                           <label for="complemento_diretor">Complemento da Residência:</label>
                           <input class="form-control text-uppercase" type="text" value="<?php echo set_value('complemento_diretor[0]',isset($socios[0]->complemento_diretor) ? $socios[0]->complemento_diretor: ''); ?>" name="complemento_diretor[]" placeholder="Complemento">
                       </div>
                   </div>
               </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="bairro_diretor">Bairro da Residência:*</label>
                            <input class="form-control text-uppercase" type="text" value="<?php echo set_value('bairro_diretor[0]',isset($socios[0]->bairro_diretor) ? $socios[0]->bairro_diretor: ''); ?>" name="bairro_diretor[]" placeholder="Bairro">
                            <div class="invalid-feedback">
                                Insira o Bairro
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="id_pais_diretor">País:*</label>
                            <?php echo form_dropdown('id_pais_diretor[]', $array_pais, set_value('id_pais_cliente[0]',33), 'class="form-control" ') ?>
                            <div class="invalid-feedback">
                                Selecione um País
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="email_pessoal_diretor">E-mail Pessoal:*</label>
                            <input class="form-control" type="text" value="<?php echo set_value('email_pessoal_diretor[0]',isset($socios[0]->email_pessoal_diretor) ? $socios[0]->email_pessoal_diretor: ''); ?>" name="email_pessoal_diretor[]" placeholder="E-mail Comercial">
                            <small>E-mail que os boletos de aluguel serão enviados.</small>
                            <div class="invalid-feedback">
                                Insira o E-mail Comercial
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="email_comercial_diretor">E-mail Comercial:*</label>
                            <input class="form-control" type="text" value="<?php echo set_value('email_comercial_diretor[0]',isset($socios[0]->email_comercial_diretor) ? $socios[0]->email_comercial_diretor: ''); ?>" name="email_comercial_diretor[]" placeholder="E-mail Comercial">
                            <small>E-mail que os boletos de aluguel serão enviados.</small>
                            <div class="invalid-feedback">
                                Insira o E-mail Comercial
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="telefone_diretor">Telefone:*</label>
                            <input class="form-control" type="text" mascara="tel" value="<?php echo set_value('telefone_diretor[0]',isset($socios[0]->telefone_diretor) ? $socios[0]->telefone_diretor: ''); ?>" name="telefone_diretor[]" placeholder="Telefone">
                            <div class="invalid-feedback">
                                Insira o E-mail Comercial
                            </div>
                        </div>
                    </div>
                </div>
                       
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <h1 class="display-4">Dados do Sócio/Diretor Nº2</h1>
                    </div>
                </div>
                 <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="nome_completo_diretor">Nome Completo:*</label>
                            <input class="form-control text-uppercase" type="text" value="<?php echo set_value('nome_completo_diretor[1]',isset($socios[1]->nome_completo_diretor) ? $socios[1]->nome_completo_diretor: ''); ?>" name="nome_completo_diretor[]" placeholder="Nome Completo">
                            <div class="invalid-feedback">
                                Insira o Nome Completo
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="cargo_diretor">Cargo Exercido:*</label>
                            <input type="text" class="form-control text-uppercase" name="cargo_diretor[]" placeholder="Cargo Exercido" value="<?php echo set_value('cargo_diretor[1]',isset($socios[1]->cargo_diretor) ? $socios[1]->cargo_diretor: '')?>">
                            <div class="invalid-feedback">
                                Insira o Cargo Exercido
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="data_nascimento_diretor">Data de Nascimento:*</label>
                            <input class="form-control" type="text" mascara="data" value="<?php echo set_value('data_nascimento_diretor[1]',isset($socios[1]->data_nascimento_diretor) ? $socios[1]->data_nascimento_diretor: ''); ?>" name="data_nascimento_diretor[]" placeholder="Data de Nascimento">
                            <div class="invalid-feedback">
                                Insira a Data de Nasciimento
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="nacionalidade_diretor">Nacionalidade*</label>
                            <input class="form-control text-uppercase" type="text" value="<?php echo set_value('nacionalidade_diretor[1]',isset($socios[1]->nacionalidade_diretor) ? $socios[1]->nacionalidade_diretor: ''); ?>" name="nacionalidade_diretor[]" placeholder="Nacionalidade">
                            <div class="invalid-feedback">
                                Insira a Nacionalidade
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="cpf_diretor">CPF:*</label>
                            <input class="form-control" type="text" mascara="cpf" value="<?php echo set_value('cpf_diretor[1]',isset($socios[1]->cpf_diretor) ? $socios[1]->cpf_diretor: ''); ?>" name="cpf_diretor[]" placeholder="CPF">
                            <small>Caso seja estrangeiro, converse com o consultor ou consultora que está lhe atendendo.</small>
                            <div class="invalid-feedback">
                                Insira o CPF
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="rg_diretor">RG*</label>
                            <input class="form-control" type="text" value="<?php echo set_value('rg_diretor[1]',isset($socios[1]->rg_diretor) ? $socios[1]->rg_diretor: ''); ?>" name="rg_diretor[]" placeholder="RG">
                            <small>Caso seja estrangeiro, converse com o consultor ou consultora que está lhe atendendo.</small>
                            <div class="invalid-feedback">
                                Insira o RG
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="id_estado_civil_diretor">Estado Civil:*</label>
                           <select name="id_estado_civil_diretor[]" class="form-control"  togvis-select >
                                <option togvis-esconder-grupo="select-divs" value="" >Selecione o Estado Civil</option>
                                <option togvis-esconder-grupo="select-divs" value="1" <?php echo isset($socios[1]->id_estado_civil) && $socios[1]->id_estado_civil=="1" ? 'selected="selected"': ''; ?> >Não Informado</option>
                                <option togvis-option="casado" togvis-outros="esconder" value="2" <?php echo isset($socios[1]->id_estado_civil) && $socios[1]->id_estado_civil=="2" ? 'selected="selected"': ''; ?>>Casado(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="3" <?php echo isset($socios[1]->id_estado_civil) && $socios[1]->id_estado_civil=="3" ? 'selected="selected"': ''; ?>>Solteiro(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="4" <?php echo isset($socios[1]->id_estado_civil) && $socios[1]->id_estado_civil=="4" ? 'selected="selected"': ''; ?>>Divorciado(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="5" <?php echo isset($socios[1]->id_estado_civil) && $socios[1]->id_estado_civil=="5" ? 'selected="selected"': ''; ?>>Viúvo(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="6" <?php echo isset($socios[1]->id_estado_civil) && $socios[1]->id_estado_civil=="6" ? 'selected="selected"': ''; ?>>União Estável</option>
                            </select>
                            <div class="invalid-feedback">
                                Insira o Estado Civil
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                   <div class="col">
                       <div class="row">
                           <div class="col">
                               <div class="form-group">
                                   <label for="cep_diretor">CEP da Residência:*</label>
                                   <input class="form-control" type="text" mascara="cep" name="cep_diretor[]" value="<?php echo set_value('cep_diretor[1]',isset($socios[1]->cep_diretor) ? $socios[1]->cep_diretor: ''); ?>" placeholder="CEP">
                                   <div class="invalid-feedback">
                                       Insira o CEP
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
                   <div class="col">
                       <div class="form-group">
                           <label for="logradouro_diretor">Logradouro da Residência:*</label>
                           <input class="form-control text-uppercase" type="text" value="<?php echo set_value('logradouro_diretor[1]',isset($socios[1]->logradouro_diretor) ? $socios[1]->logradouro_diretor: ''); ?>" name="logradouro_diretor[]" placeholder="Logradouro">
                           <div class="invalid-feedback">
                               Insira o Logradouro
                           </div>
                       </div>
                   </div>
                   <div class="col">
                       <div class="form-group">
                           <label for="numero_diretor">Número da Residência:*</label>
                           <input class="form-control" type="text" value="<?php echo set_value('numero_diretor[1]',isset($socios[1]->numero_diretor) ? $socios[1]->numero_diretor: ''); ?>" name="numero_diretor[]" placeholder="Número">
                           <div class="invalid-feedback">
                               Insira o Número
                           </div>
                       </div>
                   </div>
                   <div class="col">
                       <div class="form-group">
                           <label for="complemento_diretor">Complemento da Residência:</label>
                           <input class="form-control text-uppercase" type="text" value="<?php echo set_value('complemento_diretor[1]',isset($socios[1]->complemento_diretor) ? $socios[1]->complemento_diretor: ''); ?>" name="complemento_diretor[]" placeholder="Complemento">
                       </div>
                   </div>
               </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="bairro_diretor">Bairro da Residência:*</label>
                            <input class="form-control text-uppercase" type="text" value="<?php echo set_value('bairro_diretor[1]',isset($socios[1]->bairro_diretor) ? $socios[1]->bairro_diretor: ''); ?>" name="bairro_diretor[]" placeholder="Bairro">
                            <div class="invalid-feedback">
                                Insira o Bairro
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="id_pais_diretor">País:*</label>
                            <?php echo form_dropdown('id_pais_diretor[]', $array_pais, set_value('id_pais_cliente[1]',33), 'class="form-control" ') ?>
                            <div class="invalid-feedback">
                                Selecione um País
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="email_pessoal_diretor">E-mail Pessoal:*</label>
                            <input class="form-control" type="text" value="<?php echo set_value('email_pessoal_diretor[1]',isset($socios[1]->email_pessoal_diretor) ? $socios[1]->email_pessoal_diretor: ''); ?>" name="email_pessoal_diretor[]" placeholder="E-mail Comercial">
                            <small>E-mail que os boletos de aluguel serão enviados.</small>
                            <div class="invalid-feedback">
                                Insira o E-mail Comercial
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="email_comercial_diretor">E-mail Comercial:*</label>
                            <input class="form-control" type="text" value="<?php echo set_value('email_comercial_diretor[1]',isset($socios[1]->email_comercial_diretor) ? $socios[1]->email_comercial_diretor: ''); ?>" name="email_comercial_diretor[]" placeholder="E-mail Comercial">
                            <small>E-mail que os boletos de aluguel serão enviados.</small>
                            <div class="invalid-feedback">
                                Insira o E-mail Comercial
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="telefone_diretor">Telefone:*</label>
                            <input class="form-control" type="text" mascara="tel" value="<?php echo set_value('telefone_diretor[1]',isset($socios[1]->telefone_diretor) ? $socios[1]->telefone_diretor: ''); ?>" name="telefone_diretor[]" placeholder="Telefone">
                            <div class="invalid-feedback">
                                Insira o E-mail Comercial
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div togvis-id="socios3" togvis-estado='hide'  togvis-outros='esconder' togvis-grupo='grupo-socios'>
               <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <h1 class="display-4">Dados do Sócio/Diretor Nº1</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="nome_completo_diretor">Nome Completo:*</label>
                            <input class="form-control text-uppercase" type="text" value="<?php echo set_value('nome_completo_diretor[0]',isset($socios[0]->nome_completo_diretor) ? $socios[0]->nome_completo_diretor : ''); ?>" name="nome_completo_diretor[]" placeholder="Nome Completo">
                            <div class="invalid-feedback">
                                Insira o Nome Completo
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="cargo_diretor">Cargo Exercido:*</label>
                            <input type="text" class="form-control text-uppercase" name="cargo_diretor[]" placeholder="Cargo Exercido" value="<?php echo set_value('cargo_diretor[0]',isset($socios[0]->cargo_diretor) ? $socios[0]->cargo_diretor: '')?>">
                            <div class="invalid-feedback">
                                Insira o Cargo Exercido
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="data_nascimento_diretor">Data de Nascimento:*</label>
                            <input class="form-control" type="text" mascara="data" value="<?php echo set_value('data_nascimento_diretor[0]',isset($socios[0]->data_nascimento_diretor) ? $socios[0]->data_nascimento_diretor: ''); ?>" name="data_nascimento_diretor[]" placeholder="Data de Nascimento">
                            <div class="invalid-feedback">
                                Insira a Data de Nasciimento
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="nacionalidade_diretor">Nacionalidade*</label>
                            <input class="form-control text-uppercase" type="text" value="<?php echo set_value('nacionalidade_diretor[0]',isset($socios[0]->nacionalidade_diretor) ? $socios[0]->nacionalidade_diretor: ''); ?>" name="nacionalidade_diretor[]" placeholder="Nacionalidade">
                            <div class="invalid-feedback">
                                Insira a Nacionalidade
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="cpf_diretor">CPF:*</label>
                            <input class="form-control" type="text" mascara="cpf" value="<?php echo set_value('cpf_diretor[0]',isset($socios[0]->cpf_diretor) ? $socios[0]->cpf_diretor: ''); ?>" name="cpf_diretor[]" placeholder="CPF">
                            <small>Caso seja estrangeiro, converse com o consultor ou consultora que está lhe atendendo.</small>
                            <div class="invalid-feedback">
                                Insira o CPF
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="rg_diretor">RG*</label>
                            <input class="form-control" type="text" value="<?php echo set_value('rg_diretor[0]',isset($socios[0]->rg_diretor) ? $socios[0]->rg_diretor: ''); ?>" name="rg_diretor[]" placeholder="RG">
                            <small>Caso seja estrangeiro, converse com o consultor ou consultora que está lhe atendendo.</small>
                            <div class="invalid-feedback">
                                Insira o RG
                            </div>
                        </div>
                    </div>
                </div>
               <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="id_estado_civil_diretor">Estado Civil:*</label>
                           <select name="id_estado_civil_diretor[]" class="form-control"  togvis-select >
                                <option togvis-esconder-grupo="select-divs" value="" >Selecione o Estado Civil</option>
                                <option togvis-esconder-grupo="select-divs" value="1" <?php echo isset($socios[0]->id_estado_civil) && $socios[0]->id_estado_civil=="1" ? 'selected="selected"': ''; ?> >Não Informado</option>
                                <option togvis-option="casado" togvis-outros="esconder" value="2" <?php echo isset($socios[0]->id_estado_civil) && $socios[0]->id_estado_civil=="2" ? 'selected="selected"': ''; ?>>Casado(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="3" <?php echo isset($socios[0]->id_estado_civil) && $socios[0]->id_estado_civil=="3" ? 'selected="selected"': ''; ?>>Solteiro(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="4" <?php echo isset($socios[0]->id_estado_civil) && $socios[0]->id_estado_civil=="4" ? 'selected="selected"': ''; ?>>Divorciado(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="5" <?php echo isset($socios[0]->id_estado_civil) && $socios[0]->id_estado_civil=="5" ? 'selected="selected"': ''; ?>>Viúvo(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="6" <?php echo isset($socios[0]->id_estado_civil) && $socios[0]->id_estado_civil=="6" ? 'selected="selected"': ''; ?>>União Estável</option>
                            </select>
                            <div class="invalid-feedback">
                                Insira o Estado Civil
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                   <div class="col">
                       <div class="row">
                           <div class="col">
                               <div class="form-group">
                                   <label for="cep_diretor">CEP da Residência:*</label>
                                   <input class="form-control" type="text" mascara="cep" name="cep_diretor[]" value="<?php echo set_value('cep_diretor[0]',isset($socios[0]->cep_diretor) ? $socios[0]->cep_diretor: ''); ?>" placeholder="CEP">
                                   <div class="invalid-feedback">
                                       Insira o CEP
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
                   <div class="col">
                       <div class="form-group">
                           <label for="logradouro_diretor">Logradouro da Residência:*</label>
                           <input class="form-control text-uppercase" type="text" value="<?php echo set_value('logradouro_diretor[0]',isset($socios[0]->logradouro_diretor) ? $socios[0]->logradouro_diretor: ''); ?>" name="logradouro_diretor[]" placeholder="Logradouro">
                           <div class="invalid-feedback">
                               Insira o Logradouro
                           </div>
                       </div>
                   </div>
                   <div class="col">
                       <div class="form-group">
                           <label for="numero_diretor">Número da Residência:*</label>
                           <input class="form-control" type="text" value="<?php echo set_value('numero_diretor[0]',isset($socios[0]->numero_diretor) ? $socios[0]->numero_diretor: ''); ?>" name="numero_diretor[]" placeholder="Número">
                           <div class="invalid-feedback">
                               Insira o Número
                           </div>
                       </div>
                   </div>
                   <div class="col">
                       <div class="form-group">
                           <label for="complemento_diretor">Complemento da Residência:</label>
                           <input class="form-control text-uppercase" type="text" value="<?php echo set_value('complemento_diretor[0]',isset($socios[0]->complemento_diretor) ? $socios[0]->complemento_diretor: ''); ?>" name="complemento_diretor[]" placeholder="Complemento">
                       </div>
                   </div>
               </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="bairro_diretor">Bairro da Residência:*</label>
                            <input class="form-control text-uppercase" type="text" value="<?php echo set_value('bairro_diretor[0]',isset($socios[0]->bairro_diretor) ? $socios[0]->bairro_diretor: ''); ?>" name="bairro_diretor[]" placeholder="Bairro">
                            <div class="invalid-feedback">
                                Insira o Bairro
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="id_pais_diretor">País:*</label>
                            <?php echo form_dropdown('id_pais_diretor[]', $array_pais, set_value('id_pais_cliente[0]',33), 'class="form-control" ') ?>
                            <div class="invalid-feedback">
                                Selecione um País
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="email_pessoal_diretor">E-mail Pessoal:*</label>
                            <input class="form-control" type="text" value="<?php echo set_value('email_pessoal_diretor[0]',isset($socios[0]->email_pessoal_diretor) ? $socios[0]->email_pessoal_diretor: ''); ?>" name="email_pessoal_diretor[]" placeholder="E-mail Comercial">
                            <small>E-mail que os boletos de aluguel serão enviados.</small>
                            <div class="invalid-feedback">
                                Insira o E-mail Pessoal
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="email_comercial_diretor">E-mail Comercial:*</label>
                            <input class="form-control" type="text" value="<?php echo set_value('email_comercial_diretor[0]',isset($socios[0]->email_comercial_diretor) ? $socios[0]->email_comercial_diretor: ''); ?>" name="email_comercial_diretor[]" placeholder="E-mail Comercial">
                            <small>E-mail que os boletos de aluguel serão enviados.</small>
                            <div class="invalid-feedback">
                                Insira o E-mail Comercial
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="telefone_diretor">Telefone:*</label>
                            <input class="form-control" type="text" mascara="tel" value="<?php echo set_value('telefone_diretor[0]',isset($socios[0]->telefone_diretor) ? $socios[0]->telefone_diretor: ''); ?>" name="telefone_diretor[]" placeholder="Telefone">
                            <div class="invalid-feedback">
                                Insira o E-mail Comercial
                            </div>
                        </div>
                    </div>
                </div>
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <h1 class="display-4">Dados do Sócio/Diretor Nº2</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="nome_completo_diretor">Nome Completo:*</label>
                            <input class="form-control text-uppercase" type="text" value="<?php echo set_value('nome_completo_diretor[1]',isset($socios[1]->nome_completo_diretor) ? $socios[1]->nome_completo_diretor: ''); ?>" name="nome_completo_diretor[]" placeholder="Nome Completo">
                            <div class="invalid-feedback">
                                Insira o Nome Completo
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="cargo_diretor">Cargo Exercido:*</label>
                            <input type="text" class="form-control text-uppercase" name="cargo_diretor[]" placeholder="Cargo Exercido" value="<?php echo set_value('cargo_diretor[1]',isset($socios[1]->cargo_diretor) ? $socios[1]->cargo_diretor: '')?>">
                            <div class="invalid-feedback">
                                Insira o Cargo Exercido
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="data_nascimento_diretor">Data de Nascimento:*</label>
                            <input class="form-control" type="text" mascara="data" value="<?php echo set_value('data_nascimento_diretor[1]',isset($socios[1]->data_nascimento_diretor) ? $socios[1]->data_nascimento_diretor: ''); ?>" name="data_nascimento_diretor[]" placeholder="Data de Nascimento">
                            <div class="invalid-feedback">
                                Insira a Data de Nasciimento
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="nacionalidade_diretor">Nacionalidade*</label>
                            <input class="form-control text-uppercase" type="text" value="<?php echo set_value('nacionalidade_diretor[1]',isset($socios[1]->nacionalidade_diretor) ? $socios[1]->nacionalidade_diretor: ''); ?>" name="nacionalidade_diretor[]" placeholder="Nacionalidade">
                            <div class="invalid-feedback">
                                Insira a Nacionalidade
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="cpf_diretor">CPF:*</label>
                            <input class="form-control" type="text" mascara="cpf" value="<?php echo set_value('cpf_diretor[1]',isset($socios[1]->cpf_diretor) ? $socios[1]->cpf_diretor: ''); ?>" name="cpf_diretor[]" placeholder="CPF">
                            <small>Caso seja estrangeiro, converse com o consultor ou consultora que está lhe atendendo.</small>
                            <div class="invalid-feedback">
                                Insira o CPF
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="rg_diretor">RG*</label>
                            <input class="form-control" type="text" value="<?php echo set_value('rg_diretor[1]',isset($socios[1]->rg_diretor) ? $socios[1]->rg_diretor: ''); ?>" name="rg_diretor[]" placeholder="RG">
                            <small>Caso seja estrangeiro, converse com o consultor ou consultora que está lhe atendendo.</small>
                            <div class="invalid-feedback">
                                Insira o RG
                            </div>
                        </div>
                    </div>
                </div>
               <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="id_estado_civil_diretor">Estado Civil:*</label>
                           <select name="id_estado_civil_diretor[]" class="form-control"  togvis-select >
                                <option togvis-esconder-grupo="select-divs" value="" >Selecione o Estado Civil</option>
                                <option togvis-esconder-grupo="select-divs" value="1" <?php echo isset($socios[1]->id_estado_civil) && $socios[1]->id_estado_civil=="1" ? 'selected="selected"': ''; ?> >Não Informado</option>
                                <option togvis-option="casado" togvis-outros="esconder" value="2" <?php echo isset($socios[1]->id_estado_civil) && $socios[1]->id_estado_civil=="2" ? 'selected="selected"': ''; ?>>Casado(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="3" <?php echo isset($socios[1]->id_estado_civil) && $socios[1]->id_estado_civil=="3" ? 'selected="selected"': ''; ?>>Solteiro(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="4" <?php echo isset($socios[1]->id_estado_civil) && $socios[1]->id_estado_civil=="4" ? 'selected="selected"': ''; ?>>Divorciado(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="5" <?php echo isset($socios[1]->id_estado_civil) && $socios[1]->id_estado_civil=="5" ? 'selected="selected"': ''; ?>>Viúvo(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="6" <?php echo isset($socios[1]->id_estado_civil) && $socios[1]->id_estado_civil=="6" ? 'selected="selected"': ''; ?>>União Estável</option>
                            </select>
                            <div class="invalid-feedback">
                                Insira o Estado Civil
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                   <div class="col">
                       <div class="row">
                           <div class="col">
                               <div class="form-group">
                                   <label for="cep_diretor">CEP da Residência:*</label>
                                   <input class="form-control" type="text" mascara="cep" name="cep_diretor[]" value="<?php echo set_value('cep_diretor[1]',isset($socios[1]->cep_diretor) ? $socios[1]->cep_diretor: ''); ?>" placeholder="CEP">
                                   <div class="invalid-feedback">
                                       Insira o CEP
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
                   <div class="col">
                       <div class="form-group">
                           <label for="logradouro_diretor">Logradouro da Residência:*</label>
                           <input class="form-control text-uppercase" type="text" value="<?php echo set_value('logradouro_diretor[1]',isset($socios[1]->logradouro_diretor) ? $socios[1]->logradouro_diretor: ''); ?>" name="logradouro_diretor[]" placeholder="Logradouro">
                           <div class="invalid-feedback">
                               Insira o Logradouro
                           </div>
                       </div>
                   </div>
                   <div class="col">
                       <div class="form-group">
                           <label for="numero_diretor">Número da Residência:*</label>
                           <input class="form-control" type="text" value="<?php echo set_value('numero_diretor[1]',isset($socios[1]->numero_diretor) ? $socios[1]->numero_diretor: ''); ?>" name="numero_diretor[]" placeholder="Número">
                           <div class="invalid-feedback">
                               Insira o Número
                           </div>
                       </div>
                   </div>
                   <div class="col">
                       <div class="form-group">
                           <label for="complemento_diretor">Complemento da Residência:</label>
                           <input class="form-control text-uppercase" type="text" value="<?php echo set_value('complemento_diretor[1]',isset($socios[1]->complemento_diretor) ? $socios[1]->complemento_diretor: ''); ?>" name="complemento_diretor[]" placeholder="Complemento">
                       </div>
                   </div>
               </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="bairro_diretor">Bairro da Residência:*</label>
                            <input class="form-control text-uppercase" type="text" value="<?php echo set_value('bairro_diretor[1]',isset($socios[1]->bairro_diretor) ? $socios[1]->bairro_diretor: ''); ?>" name="bairro_diretor[]" placeholder="Bairro">
                            <div class="invalid-feedback">
                                Insira o Bairro
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="id_pais_diretor">País:*</label>
                            <?php echo form_dropdown('id_pais_diretor[]', $array_pais, set_value('id_pais_cliente[1]',33), 'class="form-control" ') ?>
                            <div class="invalid-feedback">
                                Selecione um País
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="email_pessoal_diretor">E-mail Pessoal:*</label>
                            <input class="form-control" type="text" value="<?php echo set_value('email_pessoal_diretor[1]',isset($socios[1]->email_pessoal_diretor) ? $socios[1]->email_pessoal_diretor: ''); ?>" name="email_pessoal_diretor[]" placeholder="E-mail Comercial">
                            <small>E-mail que os boletos de aluguel serão enviados.</small>
                            <div class="invalid-feedback">
                                Insira o E-mail Comercial
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="email_comercial_diretor">E-mail Comercial:*</label>
                            <input class="form-control" type="text" value="<?php echo set_value('email_comercial_diretor[1]',isset($socios[1]->email_comercial_diretor) ? $socios[1]->email_comercial_diretor: ''); ?>" name="email_comercial_diretor[]" placeholder="E-mail Comercial">
                            <small>E-mail que os boletos de aluguel serão enviados.</small>
                            <div class="invalid-feedback">
                                Insira o E-mail Comercial
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="telefone_diretor">Telefone:*</label>
                            <input class="form-control" type="text" mascara="tel" value="<?php echo set_value('telefone_diretor[1]',isset($socios[1]->telefone_diretor) ? $socios[1]->telefone_diretor: ''); ?>" name="telefone_diretor[]" placeholder="Telefone">
                            <div class="invalid-feedback">
                                Insira o E-mail Comercial
                            </div>
                        </div>
                    </div>
                </div>
                 <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <h1 class="display-4">Dados do Sócio/Diretor Nº3</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="nome_completo_diretor">Nome Completo:*</label>
                            <input class="form-control text-uppercase" type="text" value="<?php echo set_value('nome_completo_diretor[2]',isset($socios[2]->nome_completo_diretor) ? $socios[2]->nome_completo_diretor: ''); ?>" name="nome_completo_diretor[]" placeholder="Nome Completo">
                            <div class="invalid-feedback">
                                Insira o Nome Completo
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="cargo_diretor">Cargo Exercido:*</label>
                            <input type="text" class="form-control" name="cargo_diretor[]" placeholder="Cargo Exercido" value="<?php echo set_value('cargo_diretor[2]',isset($socios[2]->cargo_diretor) ? $socios[2]->cargo_diretor: '')?>">
                            <div class="invalid-feedback">
                                Insira o Cargo Exercido
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="data_nascimento_diretor">Data de Nascimento:*</label>
                            <input class="form-control" type="text" mascara="data" value="<?php echo set_value('data_nascimento_diretor[2]',isset($socios[2]->data_nascimento_diretor) ? $socios[2]->data_nascimento_diretor: ''); ?>" name="data_nascimento_diretor[]" placeholder="Data de Nascimento">
                            <div class="invalid-feedback">
                                Insira a Data de Nasciimento
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="nacionalidade_diretor">Nacionalidade*</label>
                            <input class="form-control text-uppercase" type="text" value="<?php echo set_value('nacionalidade_diretor[2]',isset($socios[2]->nacionalidade_diretor) ? $socios[2]->nacionalidade_diretor: ''); ?>" name="nacionalidade_diretor[]" placeholder="Nacionalidade">
                            <div class="invalid-feedback">
                                Insira a Nacionalidade
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="cpf_diretor">CPF:*</label>
                            <input class="form-control" type="text" mascara="cpf" value="<?php echo set_value('cpf_diretor[2]',isset($socios[2]->cpf_diretor) ? $socios[2]->cpf_diretor: ''); ?>" name="cpf_diretor[]" placeholder="CPF">
                            <small>Caso seja estrangeiro, converse com o consultor ou consultora que está lhe atendendo.</small>
                            <div class="invalid-feedback">
                                Insira o CPF
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="rg_diretor">RG*</label>
                            <input class="form-control" type="text" value="<?php echo set_value('rg_diretor[2]',isset($socios[2]->rg_diretor) ? $socios[2]->rg_diretor: ''); ?>" name="rg_diretor[]" placeholder="RG">
                            <small>Caso seja estrangeiro, converse com o consultor ou consultora que está lhe atendendo.</small>
                            <div class="invalid-feedback">
                                Insira o RG
                            </div>
                        </div>
                    </div>
                </div>
               <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="id_estado_civil_diretor">Estado Civil:*</label>
                           <select name="id_estado_civil_diretor[]" class="form-control"  togvis-select >
                                <option togvis-esconder-grupo="select-divs" value="" >Selecione o Estado Civil</option>
                                <option togvis-esconder-grupo="select-divs" value="1" <?php echo isset($socios[2]->id_estado_civil) && $socios[2]->id_estado_civil=="1" ? 'selected="selected"': ''; ?> >Não Informado</option>
                                <option togvis-option="casado" togvis-outros="esconder" value="2" <?php echo isset($socios[2]->id_estado_civil) && $socios[2]->id_estado_civil=="2" ? 'selected="selected"': ''; ?>>Casado(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="3" <?php echo isset($socios[2]->id_estado_civil) && $socios[2]->id_estado_civil=="3" ? 'selected="selected"': ''; ?>>Solteiro(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="4" <?php echo isset($socios[2]->id_estado_civil) && $socios[2]->id_estado_civil=="4" ? 'selected="selected"': ''; ?>>Divorciado(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="5" <?php echo isset($socios[2]->id_estado_civil) && $socios[2]->id_estado_civil=="5" ? 'selected="selected"': ''; ?>>Viúvo(a)</option>
                                <option togvis-esconder-grupo="select-divs" togvis-outros="esconder" value="6" <?php echo isset($socios[2]->id_estado_civil) && $socios[2]->id_estado_civil=="6" ? 'selected="selected"': ''; ?>>União Estável</option>
                            </select>
                            <div class="invalid-feedback">
                                Insira o Estado Civil
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                   <div class="col">
                       <div class="row">
                           <div class="col">
                               <div class="form-group">
                                   <label for="cep_diretor">CEP da Residência:*</label>
                                   <input class="form-control" type="text" mascara="cep" name="cep_diretor[]" value="<?php echo set_value('cep_diretor[2]',isset($socios[2]->cep_diretor) ? $socios[2]->cep_diretor: ''); ?>" placeholder="CEP">
                                   <div class="invalid-feedback">
                                       Insira o CEP
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
                   <div class="col">
                       <div class="form-group">
                           <label for="logradouro_diretor">Logradouro da Residência:*</label>
                           <input class="form-control text-uppercase" type="text" value="<?php echo set_value('logradouro_diretor[2]',isset($socios[2]->logradouro_diretor) ? $socios[2]->logradouro_diretor: ''); ?>" name="logradouro_diretor[]" placeholder="Logradouro">
                           <div class="invalid-feedback">
                               Insira o Logradouro
                           </div>
                       </div>
                   </div>
                   <div class="col">
                       <div class="form-group">
                           <label for="numero_diretor">Número da Residência:*</label>
                           <input class="form-control" type="text" value="<?php echo set_value('numero_diretor[2]',isset($socios[2]->numero_diretor) ? $socios[2]->numero_diretor: ''); ?>" name="numero_diretor[]" placeholder="Número">
                           <div class="invalid-feedback">
                               Insira o Número
                           </div>
                       </div>
                   </div>
                   <div class="col">
                       <div class="form-group">
                           <label for="complemento_diretor">Complemento da Residência:</label>
                           <input class="form-control text-uppercase" type="text" value="<?php echo set_value('complemento_diretor[2]',isset($socios[2]->complemento_diretor) ? $socios[2]->complemento_diretor: ''); ?>" name="complemento_diretor[]" placeholder="Complemento">
                       </div>
                   </div>
               </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="bairro_diretor">Bairro da Residência:*</label>
                            <input class="form-control text-uppercase" type="text" value="<?php echo set_value('bairro_diretor[2]',isset($socios[2]->bairro_diretor) ? $socios[2]->bairro_diretor: ''); ?>" name="bairro_diretor[]" placeholder="Bairro">
                            <div class="invalid-feedback">
                                Insira o Bairro
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="id_pais_diretor">País:*</label>
                            <?php echo form_dropdown('id_pais_diretor[]', $array_pais, set_value('id_pais_cliente[2]',33), 'class="form-control" ') ?>
                            <div class="invalid-feedback">
                                Selecione um País
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="email_pessoal_diretor">E-mail Pessoal:*</label>
                            <input class="form-control" type="text" value="<?php echo set_value('email_pessoal_diretor[2]',isset($socios[2]->email_pessoal_diretor) ? $socios[2]->email_pessoal_diretor: ''); ?>" name="email_pessoal_diretor[]" placeholder="E-mail Comercial">
                            <small>E-mail que os boletos de aluguel serão enviados.</small>
                            <div class="invalid-feedback">
                                Insira o E-mail Comercial
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="email_comercial_diretor">E-mail Comercial:*</label>
                            <input class="form-control" type="text" value="<?php echo set_value('email_comercial_diretor[2]',isset($socios[2]->email_comercial_diretor) ? $socios[2]->email_comercial_diretor: ''); ?>" name="email_comercial_diretor[]" placeholder="E-mail Comercial">
                            <small>E-mail que os boletos de aluguel serão enviados.</small>
                            <div class="invalid-feedback">
                                Insira o E-mail Comercial
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="telefone_diretor">Telefone:*</label>
                            <input class="form-control" type="text" mascara="tel" value="<?php echo set_value('telefone_diretor[2]',isset($socios[2]->telefone_diretor) ? $socios[2]->telefone_diretor: ''); ?>" name="telefone_diretor[]" placeholder="Telefone">
                            <div class="invalid-feedback">
                                Insira o E-mail Comercial
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
               <div class="col">
                   <div class="form-group">
                        <input type="submit" name="enviar" class="btn btn-success" value="Salvar"/>
                   </div>
               </div>
            </div>
            <div class="jumbotron jumbotron-fluid">
                <div class="container">
                    <h1 class="display-4">Propriedades da Empresa</h1>
                    <p class="lead">As propriedades devem ser no nome da empresa/CNPJ.</p>
                </div>
            </div>
            <p>Para Adicionar Mais Propriedades, clique Botão +1 ao lado do Renavam. Caso não possua, escrever 'não aplicável'.</p>
             <div class="row">
                <div class="col"> 
                     <?php 
                     $contador=0;
                     foreach ($propriedades as $value) { ?>
                    <div class="row" id="elemento-campo-propriedade">
                        <div class="col">
                            <div class="form-group">
                                <label for="matricula_imovel">Nº da Matícula do Imóvel:*</label>
                                <input class="form-control" type="text" value="<?php echo set_value("matricula_imovel[$contador]",$value->matricula_imovel); ?>" name="matricula_imovel[]" placeholder="Nº da Matícula do Imóvel" >
                                <div class="invalid-feedback">
                                    Insira o Nº da Matícula do Imóvel
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="circunscricao">Circunscrição:*</label>
                                <input class="form-control" type="text" value="<?php echo set_value("circunscricao[$contador]",$value->circunscricao); ?>" name="circunscricao[]" placeholder="Circunscrição" >
                                <div class="invalid-feedback">
                                    Insira Circunscrição
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="veiculo_marca">Marca Do Veículo:*</label>
                                <input class="form-control" type="text" value="<?php echo set_value("veiculo_marca[$contador]",$value->veiculo_marca); ?>" name="veiculo_marca[]" placeholder="Marca Do Veículo" >
                                <div class="invalid-feedback">
                                    Insira a Marca Do Veículo
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="renavam">Renavam:*</label>
                                <input class="form-control" type="text" value="<?php echo set_value("renavam[$contador]",$value->renavam); ?>" name="renavam[]" placeholder="Renavam" >
                                <div class="invalid-feedback">
                                    Insira O Renavam
                                </div>
                            </div>
                        </div>
                        <div class="col-1 ">
                            <button type="button" class="btn btn-primary" href="#"
                                     style="margin-top: 37%;" clonar-elemento="elemento-campo-propriedade">
                                        <i class="material-icons">exposure_plus_1</i>
                            </button>
                        </div>
                    </div>
                     <?php $contador++; } ?>
                </div>
            </div>
            <div class="row">
               <div class="col">
                   <div class="form-group">
                        <input type="submit" name="enviar" class="btn btn-success" value="Salvar"/>
                   </div>
               </div>
            </div>
             <div class="jumbotron jumbotron-fluid">
                <div class="container">
                    <h1 class="display-4">Referências</h1>
                    <p class="lead">Preencher abaixo as referências comerciais e bancárias determinadas abaixo.</p>
                </div>
            </div>  
            <p>Referências Comerciais (OBRIGATÓRIO 2 REFERÊNCIAS Comerciais)*</p>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="nome_referencia">Nome Completo:*</label>
                        <input class="form-control text-uppercase" type="text" value="<?php echo set_value('nome_referencia[0]',$referencias[0]->nome_referencia); ?>" name="nome_referencia[]" placeholder="Nome Completo" >
                        <div class="invalid-feedback">
                            Insira o Nome Completo
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="endereco_referencia">Endereço:*</label>
                        <input class="form-control" type="text" value="<?php echo set_value('endereco_referencia[0]',$referencias[0]->endereco_referencia); ?>" name="endereco_referencia[]" placeholder="Endereço" >
                        <div class="invalid-feedback">
                            Insira o Endereço
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="telefone_referencia">Telefone:*</label>
                        <input class="form-control" type="text" mascara="tel" value="<?php echo set_value('telefone_referencia[0]',$referencias[0]->telefone_referencia); ?>" name="telefone_referencia[]" placeholder="Telefone" >
                        <div class="invalid-feedback">
                            Insira o Telefone
                        </div>
                    </div>
                </div>
                 <input hidden value="2" name="tipo_referencia[]">
            </div>
             <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="nome_referencia">Nome Completo:*</label>
                        <input class="form-control text-uppercase" type="text" value="<?php echo set_value('nome_referencia[1]',$referencias[1]->nome_referencia); ?>" name="nome_referencia[]" placeholder="Nome Completo" >
                        <div class="invalid-feedback">
                            Insira o Nome Completo
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="endereco_referencia">Endereço:*</label>
                        <input class="form-control" type="text" value="<?php echo set_value('endereco_referencia[1]',$referencias[1]->endereco_referencia); ?>" name="endereco_referencia[]" placeholder="Endereço" >
                        <div class="invalid-feedback">
                            Insira o Endereço
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="telefone_referencia">Telefone:*</label>
                        <input class="form-control" type="text" mascara="tel" value="<?php echo set_value('telefone_referencia[1]',$referencias[1]->telefone_referencia); ?>" name="telefone_referencia[]" placeholder="Telefone" >
                        <div class="invalid-feedback">
                            Insira o Telefone
                        </div>
                    </div>
                </div>
                 <input hidden value="2" name="tipo_referencia[]">
            </div>
             <p>Referências Bancárias (OBRIGATÓRIO 2 REFERÊNCIAS BANCÁRIAS)*</p>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="banco_referencia">Banco:*</label>
                        <input class="form-control" type="text" value="<?php echo set_value('banco_referencia[0]',$referencias_bancarias[0]->banco_referencia); ?>" name="banco_referencia[]" placeholder="Banco" >
                        <div class="invalid-feedback">
                            Insira o Nome do Banco
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="telefone_banco_referencia">Telefone da Agencia:*</label>
                        <input class="form-control" type="text" mascara="tel" value="<?php echo set_value('telefone_banco_referencia[0]',$referencias_bancarias[0]->telefone_banco_referencia); ?>" name="telefone_banco_referencia[]" placeholder="Telefone da Agencia" >
                        <div class="invalid-feedback">
                            Insira o Telefone da Agencia
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="nome_agencia_referencia">Nome da Agência:*</label>
                        <input class="form-control text-uppercase" type="text"  value="<?php echo set_value('nome_agencia_referencia[0]',$referencias_bancarias[0]->nome_agencia_referencia); ?>" name="nome_agencia_referencia[]" placeholder="Nome da Agência" >
                        <div class="invalid-feedback">
                            Insira o Telefone
                        </div>
                    </div>
                </div>
                 <div class="col">
                    <div class="form-group">
                        <label for="numero_conta_referencia">Número da Conta:*</label>
                        <input class="form-control" type="text" value="<?php echo set_value('numero_conta_referencia[0]',$referencias_bancarias[0]->nome_agencia_referencia); ?>" name="numero_conta_referencia[]" placeholder="Número da Conta" >
                        <div class="invalid-feedback">
                            Insira o Número da Conta
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="banco_referencia">Banco:*</label>
                        <input class="form-control" type="text" value="<?php echo set_value('banco_referencia[1]',$referencias_bancarias[1]->banco_referencia); ?>" name="banco_referencia[]" placeholder="Banco" >
                        <div class="invalid-feedback">
                            Insira o Nome do Banco
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="telefone_banco_referencia">Telefone da Agencia:*</label>
                        <input class="form-control" type="text" value="<?php echo set_value('telefone_banco_referencia[1]',$referencias_bancarias[1]->telefone_banco_referencia); ?>" mascara="tel" name="telefone_banco_referencia[]" placeholder="Telefone da Agencia" >
                        <div class="invalid-feedback">
                            Insira o Telefone da Agencia
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="nome_agencia_referencia">Nome da Agência:*</label>
                        <input class="form-control text-uppercase" type="text" value="<?php echo set_value('nome_agencia_referencia[1]',$referencias_bancarias[1]->nome_agencia_referencia); ?>" name="nome_agencia_referencia[]" placeholder="Nome da Agência" >
                        <div class="invalid-feedback">
                            Insira o Telefone
                        </div>
                    </div>
                </div>
                 <div class="col">
                    <div class="form-group">
                        <label for="numero_conta_referencia">Número da Conta:*</label>
                        <input class="form-control" type="text" value="<?php echo set_value('numero_conta_referencia[1]',$referencias_bancarias[1]->numero_conta_referencia); ?>" name="numero_conta_referencia[]" placeholder="Número da Conta" >
                        <div class="invalid-feedback">
                            Insira o Número da Conta
                        </div>
                    </div>
                </div>
            </div>
             <div class="row">
               <div class="col">
                   <div class="form-group">
                        <input type="submit" name="enviar" class="btn btn-success" value="Salvar"/>
                   </div>
               </div>
            </div>
            <div class="form-check">
               <label class="custom-control custom-checkbox">
                   <input type="checkbox" class="custom-control-input"  name="concordo">
                   <span class="custom-control-indicator"></span>
                   <span class="custom-control-description">
                       OBS. No valor do aluguel serão acrescidos mensalmente as
                       taxas: IPTU, água, luz, seguro contra incêndio, condomínio,
                       etc. Em caso de dúvidas, converse com o cosultor(a) que 
                       está lhe atendendo.
                   </span>
               </label>
            </div>
             <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="observacoes_complementares">Observações Complementares: <small>Preencher caso haja alguma observação.</small></label>
                        <textarea class="form-control" rows="10" name="observacoes_complementares"><?php echo set_value('observacoes_complementares',$query['observacoes_complementares']) ?></textarea>
                    </div>
                </div>
            </div>
             <input type="submit" name="enviar" class="btn btn-primary" value="Enviar Ficha para Baggio"/>
            <input type="submit" name="enviar" class="btn btn-success" value="Salvar"/>
        </form>
    </div>
</div>