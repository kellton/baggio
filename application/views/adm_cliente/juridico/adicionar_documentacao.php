<?php
defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container-fluid">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="jumbotron jumbotron-fluid espacamento-titulo-pagina">
            <h1>Cliente Pessoa Jurídica</h1>
        </div>
        <?php 
        if(isset($error)){
            echo '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$error.'</div>';
        }
        if($this->session->flashdata('mensagem')){
            echo '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$this->session->flashdata('mensagem').'</div>';
        }
        echo validation_errors('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>','</div>'); 
        ?>
        <div class="row">
            <div class="col">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Enviar Contrato Social</h2></p>
                        <?php if(!isset($tem_contrato_social)){ ?>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#contratoSocial">
                            <h4><i class="material-icons">description</i>Contrato Social</h4>
                        </button>
                        <?php } else{ ?>
                    <p>Você já enviou seus documentos</p>
                    <p>Para visualiza-los<button type="button" class="btn btn-link" data-toggle="modal" data-target="#visualizarContratoSocialModal">Clique aqui</button></p>
                    <p>Para Excluir <a href="<?php echo base_url('cli-juridico/excluir-documentacao/16') ?>">Clique aqui</a></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Enviar 3(Três) Ultimas Alterações do Contrato Social</h2></p>
                        <?php if(!isset($tem_alteracao_contrato_social)){ ?>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#alteracaoContratoSocial">
                            <h4><i class="material-icons">border_color</i> Alteração Contrato Social</h4>
                        </button>
                        <?php } else{ ?>
                    <p>Você já enviou a alteração do contrato social</p>
                    <p>Para visualiza-lo<button type="button" class="btn btn-link" data-toggle="modal" data-target="#visualizaralteracaoContratoSocial">Clique aqui</button></p>
                    <p>Para Excluir <a href="<?php echo base_url('cli-juridico/excluir-documentacao/23') ?>">Clique aqui</a></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Enviar Balanço</h2></p>
                        <?php if(!isset($tem_balanco)){ ?>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#balancoModal">
                            <h4><i class="material-icons">attach_money</i> Balanço</h4>
                        </button>
                        <?php } else{ ?>
                    <p>Você já enviou balanço</p>
                    <p>Para visualiza-lo<button type="button" class="btn btn-link" data-toggle="modal" data-target="#visualizarBalancoModal">Clique aqui</button></p>
                    <p>Para Excluir <a href="<?php echo base_url('cli-juridico/excluir-documentacao/24') ?>">Clique aqui</a></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Enviar Balancete</h2></p>
                        <?php if(!isset($tem_balancete)){ ?>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#balanceteModal">
                            <h4><i class="material-icons">monetization_on</i> Balancete</h4>
                        </button>
                        <?php } else{ ?>
                    <p>Você já enviou o balancete</p>
                    <p>Para visualiza-lo<button type="button" class="btn btn-link" data-toggle="modal" data-target="#visualizarBalanceteModal">Clique aqui</button></p>
                    <p>Para Excluir <a href="<?php echo base_url('cli-juridico/excluir-documentacao/25') ?>">Clique aqui</a></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Enviar Cartão CNPJ</h2></p>
                        <?php if(!isset($tem_cartao)){ ?>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#cartaoCNPJModal">
                            <h4><i class="material-icons">credit_card</i> Cartão</h4>
                        </button>
                        <?php } else{ ?>
                    <p>Você já enviou o cartão CNPJ</p>
                    <p>Para visualiza-lo<button type="button" class="btn btn-link" data-toggle="modal" data-target="#visualizarcartaoCNPJModal">Clique aqui</button></p>
                    <p>Para Excluir <a href="<?php echo base_url('cli-juridico/excluir-documentacao/26') ?>">Clique aqui</a></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Enviar Declaração de Imposto de Renda</h2></p>
                        <?php if(!isset($tem_imposto_renda)){ ?>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#impostoRendaModal">
                            <h4><i class="material-icons">account_balance</i> Declaração</h4>
                        </button>
                        <?php } else{ ?>
                    <p>Você já enviou a declaração de imposto de renda</p>
                    <p>Para visualiza-lo<button type="button" class="btn btn-link" data-toggle="modal" data-target="#visualizarImpostoRendaModal">Clique aqui</button></p>
                    <p>Para Excluir <a href="<?php echo base_url('cli-juridico/excluir-documentacao/15') ?>">Clique aqui</a></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Enviar Certidão Negativa</h2></p>
                        <?php if(!isset($tem_certidao_negativa)){ ?>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#certidaoNegativaModal">
                            <h4><i class="material-icons">storage</i> Certidão Negativa</h4>
                        </button>
                        <?php } else{ ?>
                    <p>Você já enviou certidão negativa</p>
                    <p>Para visualiza-lo<button type="button" class="btn btn-link" data-toggle="modal" data-target="#visualizarCertidaoNegativaModal">Clique aqui</button></p>
                    <p>Para Excluir <a href="<?php echo base_url('cli-juridico/excluir-documentacao/27') ?>">Clique aqui</a></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Outros Documentos</h2></p>
                        <?php if(!isset($tem_outros_documentos)){ ?>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#outrosDocumentosModal">
                            <h4><i class="material-icons">storage</i> Outros Documentos</h4>
                        </button>
                        <?php } else{ ?>
                    <p>Você já enviou outros documentos</p>
                    <p>Para visualiza-lo<button type="button" class="btn btn-link" data-toggle="modal" data-target="#visualizarOutrosDocumentosModal">Clique aqui</button></p>
                    <p>Para Excluir <a href="<?php echo base_url('cli-juridico/excluir-documentacao/21') ?>">Clique aqui</a></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="jumbotron jumbotron-fluid">
            <h1>Documetação dos Sócios/Diretores</h1>
        </div>
        <div class="row">
            <div class="col">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Enviar RG e CPF / CNH</h2></p>
                        <?php if(!isset($tem_rg_cnh)){ ?>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#rgCpfModal">
                            <h4><i class="material-icons">perm_identity</i>RG e CPF</h4>
                        </button>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#cnhModal">
                            <h4><i class="material-icons">directions_car</i> CNH</h4>
                        </button>
                        <?php } else{ ?>
                    <p>Você já enviou esses documentos</p>
                    <p>Para visualiza-los<button type="button" class="btn btn-link" data-toggle="modal" data-target="#documentosModal">Clique aqui</button></p>
                    <p>Para Excluir <a href="<?php echo base_url('cli-juridico/excluir-documentacao/1') ?>">Clique aqui</a></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Enviar Comprovante de Endereço</h2></p>
                        <?php if(!isset($tem_comprovante_residencia)){ ?>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#enderecoModal">
                            <h4><i class="material-icons">home</i>Água, Luz ou Telefone</h4>
                        </button>
                        <?php } else{ ?>
                    <p>Você já enviou os comprovantes de endereço</p>
                    <p>Para visualiza-lo<button type="button" class="btn btn-link" data-toggle="modal" data-target="#comprovanteEnderecoModal">Clique aqui</button></p>
                    <p>Para Excluir <a href="<?php echo base_url('cli-juridico/excluir-documentacao/4') ?>">Clique aqui</a></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>