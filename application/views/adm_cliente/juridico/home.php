<?php 
    if($this->session->flashdata('mensagem')){
        echo '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$this->session->flashdata('mensagem').'</div>';
}?>

<div class="banner-background-adm">
    <img width="100%" src="<?php echo base_url('publico/imagens/area-adm/cliente/banner@2x.png'); ?>">
</div>

<div class="container">
    
    <div class="row align-items-center">
        <div class="offset-1 offset-sm-0 offset-md-0 offset-lg-0 col-4 col-sm-3 col-md-3 col-lg-3 text-center botao-area-adm botao-area-adm-cliente">
            <a class="" href='cli-juridico/alterar-dados' role="button">
                <label class="comportamento-botao">
                    <div class="row">
                        <div class="col-10 col-sm-10 col-md-6 col-lg-6 offset-1 offset-sm-1 offset-md-3 offset-lg-3 text-center">
                            <img src="<?php echo base_url("publico/imagens/area-adm/cliente/editar-perfil3@2x.png"); ?>">
                        </div>
                    </div>
                    Meu Perfil
                </label>
            </a>
        </div>

<!-------------------------------------------------------------------------------------------------------------------------->

        <?php 
        if($tem_dados){
            if($tem_dados['enviou']==0){ ?>
                <div class="offset-2 offset-sm-0 offset-md-0 offset-lg-0 col-4 col-sm-3 col-md-3 col-lg-3 text-center botao-area-adm botao-area-adm-cliente">
                    <a class="" href='<?php echo base_url("cli-juridico/editar-ficha"); ?>' role="button">
                        <label class="comportamento-botao">
                            <div class="row">
                                <div class="col-10 col-sm-10 col-md-6 col-lg-6 offset-1 offset-sm-1 offset-md-3 offset-lg-3 text-center">
                                    <img src="<?php echo base_url("publico/imagens/area-adm/cliente/ficha-cadastro3@2x.png"); ?>">
                                </div>
                            </div>
                            Editar Ficha de Cadastro
                        </label>
                    </a>
                </div>
            <?php }
            else{ ?>
                <div class="offset-2 offset-sm-0 offset-md-0 offset-lg-0 col-4 col-sm-3 col-md-3 col-lg-3 text-center botao-area-adm botao-area-adm-cliente">
                    <a class="" href='<?php echo base_url("cli-juridico/visualizar-ficha-cadastro"); ?>' role="button">
                        <label class="comportamento-botao">
                            <div class="row">
                                <div class="col-10 col-sm-10 col-md-6 col-lg-6 offset-1 offset-sm-1 offset-md-3 offset-lg-3 text-center">
                                    <img src="<?php echo base_url("publico/imagens/area-adm/cliente/ficha-cadastro3@2x.png"); ?>">
                                </div>
                            </div>
                            Visualizar Ficha de Cadastro
                        </label>
                    </a>
                </div>
            <?php }
        }else{ ?>
            <div class="offset-2 offset-sm-0 offset-md-0 offset-lg-0 col-4 col-sm-3 col-md-3 col-lg-3 text-center botao-area-adm botao-area-adm-cliente">
                <a class="" href='<?php echo base_url("cli-juridico/adicionar-ficha"); ?>' role="button">
                    <label class="comportamento-botao">
                        <div class="row">
                            <div class="col-10 col-sm-10 col-md-6 col-lg-6 offset-1 offset-sm-1 offset-md-3 offset-lg-3 text-center">
                                <img src="<?php echo base_url("publico/imagens/area-adm/cliente/ficha-cadastro3@2x.png"); ?>">
                            </div>
                        </div>
                        Ficha de Cadastro
                    </label>
                </a>
            </div>
        <?php } ?>

<!-------------------------------------------------------------------------------------------------------------------------->

        <div class="offset-1 offset-sm-0 offset-md-0 offset-lg-0 col-4 col-sm-3 col-md-3 col-lg-3 text-center botao-area-adm botao-area-adm-cliente">
            <a class="" href='#' role="button">
                <label class="comportamento-botao">
                    <div class="row">
                        <div class="col-10 col-sm-10 col-md-6 col-lg-6 offset-1 offset-sm-1 offset-md-3 offset-lg-3 text-center">
                            <img src="<?php echo base_url("publico/imagens/area-adm/cliente/meus-imoveis3@2x.png"); ?>">
                        </div>
                    </div>
                    Meus Imóveis
                </label>
            </a>
        </div>

        <div class="offset-2 offset-sm-0 offset-md-0 offset-lg-0 col-4 col-sm-3 col-md-3 col-lg-3 text-center botao-area-adm botao-area-adm-cliente">
            <a class="" href='<?php echo base_url("cli-juridico/documentacao"); ?>' role="button">
                <label class="comportamento-botao">
                    <div class="row">
                        <div class="col-10 col-sm-10 col-md-6 col-lg-6 offset-1 offset-sm-1 offset-md-3 offset-lg-3 text-center">
                            <img src="<?php echo base_url("publico/imagens/area-adm/cliente/minha-documentacao3@2x.png"); ?>">
                        </div>
                    </div>
                    Meus <span class="d-lg-none">Doc.</span><span class="d-md-down-none">Documentos</span>
                </label>
            </a>
        </div>
    </div>
</div>
