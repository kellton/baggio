<?php
defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container-fluid">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="jumbotron jumbotron-fluid espacamento-titulo-pagina">
            <h1>Pretendente Pessoa Física</h1>
        </div>
        <?php 
        if(isset($error)){
            if($error!=="1" && $error!=="0"){
            echo '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$error.'</div>';
            }
        }
        if($this->session->flashdata('mensagem')){
            echo '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$this->session->flashdata('mensagem').'</div>';
        }
        echo validation_errors('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>','</div>'); 
        ?>
        <div class="row">
            <div class="col">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Enviar RG e CPF / CNH</h2></p>
                        <?php if(!isset($tem_documentos)){ ?>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#rgCpfModal">
                            <h4><i class="fa fa-user-o" aria-hidden="true"></i> RG e CPF</h4>
                        </button>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#cnhModal">
                            <h4><i class="fa fa-car" aria-hidden="true"></i> CNH</h4>
                        </button>
                        <?php } else{ ?>
                    <p>Você já enviou seus documentos</p>
                    <p>Para visualiza-los<button type="button" class="btn btn-link" data-toggle="modal" data-target="#documentosModal">Clique aqui</button></p>
                    <p>Para Excluir <a href="<?php echo base_url('cli/excluir-documentacao/1/'.$this->uri->segment(3)) ?>">Clique aqui</a></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Comprovante de Endereço</h2></p>
                        <?php if(!isset($tem_comprovante)){ ?>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#enderecoModal">
                            <h4><i class="fa fa-home" aria-hidden="true"></i> Água, Luz ou Telefone</h4>
                        </button>
                        <?php } else{ ?>
                    <p>Você já enviou seu comprovante de endereço</p>
                    <p>Para visualiza-lo<button type="button" class="btn btn-link" data-toggle="modal" data-target="#comprovanteEnderecoModal">Clique aqui</button></p>
                    <p>Para Excluir <a href="<?php echo base_url('cli/excluir-documentacao/4/'.$this->uri->segment(3)) ?>">Clique aqui</a></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Comprovação de Estado Civil</h2></p>
                        <?php if(!isset($tem_estado_civil)){ ?>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#estadoCivilModal">
                            <h4><i class="fa fa-users" aria-hidden="true"></i> Certidões</h4>
                        </button>
                        <?php } else{ ?>
                    <p>Você já enviou seu comprovante de estado civil</p>
                    <p>Para visualiza-lo<button type="button" class="btn btn-link" data-toggle="modal" data-target="#comprovanteCivilModal">Clique aqui</button></p>
                    <p>Para Excluir <a href="<?php echo base_url('cli/excluir-documentacao/7/'.$this->uri->segment(3)) ?>">Clique aqui</a></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Comprovação de Renda</h2></p>
                        <?php if(!isset($tem_renda)){ ?>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#rendaModal">
                            <h4><i class="fa fa-list" aria-hidden="true"></i> Comprovantes de Renda</h4>
                        </button>
                        <?php } else{ ?>
                    <p>Você já enviou seus comprovantes de renda</p>
                    <p>Para visualiza-lo<button type="button" class="btn btn-link" data-toggle="modal" data-target="#comprovanteRendaModal">Clique aqui</button></p>
                    <p>Para Excluir <a href="<?php echo base_url('cli/excluir-documentacao/13/'.$this->uri->segment(3)) ?>">Clique aqui</a></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
             <?php if($this->uri->segment(3)==1 && count($residentes)>1){?>
            <div class="col">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Documentação dos Residentes</h2></p>
                        <?php if(!isset($tem_residentes)){ ?>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#documentacaoResidentesModal">
                            <h4><i class="fa fa-user-plus" aria-hidden="true"></i> Documentos</h4>
                        </button>
                        <?php } else{ ?>
                    <p>Você já enviou a documentaçao dos residentes</p>
                    <p>Para visualiza-lo<button type="button" class="btn btn-link" data-toggle="modal" data-target="#visualizaComprovanteResidenteModal">Clique aqui</button></p>
                     <p>Para Excluir <a href="<?php echo base_url('cli/excluir-documentacao/0/'.$this->uri->segment(3)) ?>">Clique aqui</a></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
             <?php } ?>
            <div class="col">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Outros Documentos</h2></p>
                        <?php if(!isset($tem_outros_documentos)){ ?>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#outrosDocumentosModal">
                            <h4><i class="fa fa-list" aria-hidden="true"></i> Outros Documentos</h4>
                        </button>
                        <?php } else{ ?>
                    <p>Você já enviou outros documentos</p>
                    <p>Para visualiza-lo<button type="button" class="btn btn-link" data-toggle="modal" data-target="#visualizarOutrosDocumentosModal">Clique aqui</button></p>
                    <p>Para Excluir <a href="<?php echo base_url('cli/excluir-documentacao/21/'.$this->uri->segment(3)) ?>">Clique aqui</a></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
        </div>
        <?php 
        if($tem_conjugue['id_estado_civil']=="2" && $this->uri->segment(3)==0){?>
        <div class="jumbotron jumbotron-fluid espacamento-titulo-pagina">
            <h1>Dados do Cônjugue</h1>
        </div>
        <div class="row">
            <div class="col">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Enviar RG e CPF / CNH</h2></p>
                        <?php if(!isset($tem_documentos_conjugue)){ ?>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#rgCpfConjugueModal">
                            <h4><i class="fa fa-user-o" aria-hidden="true"></i> RG e CPF</h4>
                        </button>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#cnhConjugueModal">
                            <h4><i class="fa fa-car" aria-hidden="true"></i> CNH</h4>
                        </button>
                        <?php } else{ ?>
                    <p>Você já enviou seus documentos</p>
                    <p>Para visualiza-los<button type="button" class="btn btn-link" data-toggle="modal" data-target="#visualizarDocumentosConjugueModal">Clique aqui</button></p>
                    <p>Para Excluir <a href="<?php echo base_url('cli/excluir-documentacao/1/'.$this->uri->segment(3)) ?>">Clique aqui</a></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
             <div class="col">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Comprovação de Renda</h2></p>
                        <?php if(!isset($tem_renda_conjugue)){ ?>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#rendaConjugueModal">
                            <h4><i class="fa fa-list" aria-hidden="true"></i> Comprovantes de Renda</h4>
                        </button>
                        <?php } else{ ?>
                    <p>Você já enviou seus comprovantes de renda</p>
                    <p>Para visualiza-lo<button type="button" class="btn btn-link" data-toggle="modal" data-target="#comprovanteRendaConjugueModal">Clique aqui</button></p>
                    <p>Para Excluir <a href="<?php echo base_url('cli/excluir-documentacao/13/'.$this->uri->segment(3)) ?>">Clique aqui</a></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
        </div>
        <?php }?>
    </div>
</div>