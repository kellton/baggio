<?php
defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="banner-background-adm">
    <img width="100%" src="<?php echo base_url('publico/imagens/area-adm/cliente/banner@2x.png'); ?>">
</div>
      
<div class="container">
    <?php 
     if($this->session->flashdata('mensagem')){
         echo '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$this->session->flashdata('mensagem').'</div>';
     }?>
    <div class="row align-items-center">
        
<!-------------------------------------------------------------------------------------------------------------------------->

        <?php if(!$tem_pretendente){ ?>
            <div class="col-5 col-md-4 form-group">
                <div class="text-center botao-area-adm botao-area-adm-cliente">
                    <a href="<?php echo base_url('cli/adicionar-ficha-cadastro')?>">
                        <label class="comportamento-botao">
                            <div class="row">
                                <div class="col-10 col-sm-10 col-md-6 col-lg-6 offset-1 offset-sm-1 offset-md-3 offset-lg-3 text-center">
                                    <img src="<?php echo base_url("publico/imagens/area-adm/cliente/ficha-cadastro3@2x.png"); ?>">
                                </div>
                            </div>
                            <b>1º Pretendente</b>
                        </label>
                    </a>
                    
                    <a class="btn btn-link btn-sm" href='<?php echo base_url("cli/adicionar-ficha-cadastro"); ?>'><b>Preencher Ficha</b></a>
                </div>
            </div>
        <?php }else{
            if($tem_pretendente['enviou']==0){?>
                <div class="col-5 col-md-4 form-group">
                    <div class="text-center botao-area-adm botao-area-adm-cliente">
                        <a href="<?php echo base_url('cli/editar-ficha-cadastro')?>">
                            <label class="comportamento-botao">
                                <div class="row">
                                    <div class="col-10 col-sm-10 col-md-6 col-lg-6 offset-1 offset-sm-1 offset-md-3 offset-lg-3 text-center">
                                        <img src="<?php echo base_url("publico/imagens/area-adm/cliente/ficha-cadastro3@2x.png"); ?>">
                                    </div>
                                </div>
                                <b>1º Pretendente</b>
                            </label>
                        </a>
                        
                        <a class="btn btn-link btn-sm" href='<?php echo base_url("cli/editar-ficha-cadastro"); ?>'><b>Editar Ficha</b></a>
                    </div>
                </div>
            <?php } else {?>
                    <div class="col-5 col-md-4 form-group">
                        <div class="text-center botao-area-adm botao-area-adm-cliente">
                            <a href="<?php echo base_url('cli/ficha-cadastro/visualizar')?>">
                                <label class="comportamento-botao">
                                    <div class="row">
                                        <div class="col-10 col-sm-10 col-md-6 col-lg-6 offset-1 offset-sm-1 offset-md-3 offset-lg-3 text-center">
                                            <img src="<?php echo base_url("publico/imagens/area-adm/cliente/ficha-cadastro3@2x.png"); ?>">
                                        </div>
                                    </div>
                                    <b>1º Pretendente</b>
                                </label>
                            </a>

                            <a class="btn btn-link btn-sm" href="<?php echo base_url('cli/ficha-cadastro/visualizar')?>"><b>Vizualizar Ficha</b></a><br>

                            <a class="btn btn-link btn-sm" href='<?php echo base_url("cli/editar-ficha-cadastro"); ?>'><b>Editar Ficha</b></a>
                        </div>
                    </div>
        <?php }}?>

<!-------------------------------------------------------------------------------------------------------------------------->

        <?php if($segundo_pretendente){?>
            <?php if(!$adicionou_seggundo_pretendente){?>
                <div class="col-5 col-md-4 form-group">
                    <div class="text-center botao-area-adm botao-area-adm-cliente">
                        <a href="<?php echo base_url('cli/adicionar-documentacao-segundo-pretendente')?>">
                            <label class="comportamento-botao">
                                <div class="row">
                                    <div class="col-10 col-sm-10 col-md-6 col-lg-6 offset-1 offset-sm-1 offset-md-3 offset-lg-3 text-center">
                                        <img src="<?php echo base_url("publico/imagens/area-adm/cliente/ficha-cadastro3@2x.png"); ?>">
                                    </div>
                                </div>
                                <b>2º Pretendente</b>
                            </label>
                        </a>
                        
                        <a class="btn btn-link btn-sm" href='<?php echo base_url("cli/adicionar-documentacao-segundo-pretendente"); ?>'><b>Preencher Ficha</b></a>
                    </div>
                </div>
             <?php } else {

                    if($adicionou_seggundo_pretendente['enviou']==0){ ?>
                        <div class="col-5 col-md-4 form-group">
                            <div class="text-center botao-area-adm botao-area-adm-cliente">
                                <a href="<?php echo base_url('cli/editar-ficha-cadastro-segundo-pretendente')?>">
                                    <label class="comportamento-botao">
                                        <div class="row">
                                            <div class="col-10 col-sm-10 col-md-6 col-lg-6 offset-1 offset-sm-1 offset-md-3 offset-lg-3 text-center">
                                                <img src="<?php echo base_url("publico/imagens/area-adm/cliente/ficha-cadastro3@2x.png"); ?>">
                                            </div>
                                        </div>
                                        <b>2º Pretendente</b>
                                    </label>
                                </a>
                                
                                <a class="btn btn-link btn-sm" href='<?php echo base_url("cli/editar-ficha-cadastro-segundo-pretendente"); ?>'><b>Editar Ficha</b></a>
                            </div>
                        </div>
                    <?php }

                    else{ ?>
                        <div class="col-5 col-md-4 form-group">
                            <div class="text-center botao-area-adm botao-area-adm-cliente">
                                <a href="<?php echo base_url('cli/ficha-cadastro-segundo-pretendente/visualizar')?>">
                                    <label class="comportamento-botao">
                                        <div class="row">
                                            <div class="col-10 col-sm-10 col-md-6 col-lg-6 offset-1 offset-sm-1 offset-md-3 offset-lg-3 text-center">
                                                <img src="<?php echo base_url("publico/imagens/area-adm/cliente/ficha-cadastro3@2x.png"); ?>">
                                            </div>
                                        </div>
                                        <b>2º Pretendente</b>
                                    </label>
                                </a>

                                <a class="btn btn-link btn-sm" href="<?php echo base_url('cli/ficha-cadastro-segundo-pretendente/visualizar')?>"><b>Vizualizar Ficha</b></a><br>

                                <a class="btn btn-link btn-sm" href='<?php echo base_url("cli/editar-ficha-cadastro-segundo-pretendente"); ?>'><b>Editar Ficha</b></a>
                            </div>
                        </div>
            <?php }}}?>

<!-------------------------------------------------------------------------------------------------------------------------->

        <?php if(!$tem_fiador){ ?>
                <div class="col-5 col-md-4 form-group">
                    <div class="text-center botao-area-adm botao-area-adm-cliente">
                        <a href="<?php echo base_url('cli/adicionar-ficha-cadastro-fiador')?>">
                            <label class="comportamento-botao">
                                <div class="row">
                                    <div class="col-10 col-sm-10 col-md-6 col-lg-6 offset-1 offset-sm-1 offset-md-3 offset-lg-3 text-center">
                                        <img src="<?php echo base_url("publico/imagens/area-adm/cliente/ficha-cadastro3@2x.png"); ?>">
                                    </div>
                                </div>
                                <b>Fiador</b>
                            </label>
                        </a>
                        
                        <a class="btn btn-link btn-sm" href='<?php echo base_url("cli/adicionar-ficha-cadastro-fiador"); ?>'><b>Preencher Ficha</b></a>
                    </div>
                </div>
        <?php } else{
            if($tem_fiador['enviou']==0){ ?>
                        <div class="col-5 col-md-4 form-group">
                            <div class="text-center botao-area-adm botao-area-adm-cliente">
                                <a href="<?php echo base_url('cli/editar-ficha-cadastro-fiador')?>">
                                    <label class="comportamento-botao">
                                        <div class="row">
                                            <div class="col-10 col-sm-10 col-md-6 col-lg-6 offset-1 offset-sm-1 offset-md-3 offset-lg-3 text-center">
                                                <img src="<?php echo base_url("publico/imagens/area-adm/cliente/ficha-cadastro3@2x.png"); ?>">
                                            </div>
                                        </div>
                                        <b>Fiador</b>
                                    </label>
                                </a>
                                
                                <a class="btn btn-link btn-sm" href='<?php echo base_url("cli/editar-ficha-cadastro-fiador"); ?>'><b>Editar Ficha</b></a>
                            </div>
                        </div>
            <?php }else{?>
                        <div class="col-5 col-md-4 form-group">
                            <div class="text-center botao-area-adm botao-area-adm-cliente">
                                <a href="<?php echo base_url('cli/ficha-cadastro-fiador/visualizar')?>">
                                    <label class="comportamento-botao">
                                        <div class="row">
                                            <div class="col-10 col-sm-10 col-md-6 col-lg-6 offset-1 offset-sm-1 offset-md-3 offset-lg-3 text-center">
                                                <img src="<?php echo base_url("publico/imagens/area-adm/cliente/ficha-cadastro3@2x.png"); ?>">
                                            </div>
                                        </div>
                                        <b>Fiador</b>
                                    </label>
                                </a>

                                <a class="btn btn-link btn-sm" href="<?php echo base_url('cli/ficha-cadastro-fiador/visualizar')?>"><b>Vizualizar Ficha</b></a><br>

                                <a class="btn btn-link btn-sm" href='<?php echo base_url("cli/editar-ficha-cadastro-fiador"); ?>'><b>Editar Ficha</b></a>
                            </div>
                        </div>
         <?php }}?>
    </div>
</div>