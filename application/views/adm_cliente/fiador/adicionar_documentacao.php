<?php
defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container-fluid">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="jumbotron jumbotron-fluid espacamento-titulo-pagina">
            <h1>Fiador</h1>
        </div>
        <?php 
        if(isset($error)){
            echo '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$error.'</div>';
        }
        if($this->session->flashdata('mensagem')){
            echo '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$this->session->flashdata('mensagem').'</div>';
        }
        echo validation_errors('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>','</div>'); 
        ?>
        <div class="row">
            <div class="col">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Enviar RG e CPF / CNH</h2></p>
                        <?php if(!isset($tem_documentos)){ ?>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#rgCpfModal">
                            <h4><i class="material-icons">perm_identity</i>RG e CPF</h4>
                        </button>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#cnhModal">
                            <h4><i class="material-icons">directions_car</i> CNH</h4>
                        </button>
                        <?php } else{ ?>
                    <p>Você já enviou seus documentos</p>
                    <p>Para visualiza-los<button type="button" class="btn btn-link" data-toggle="modal" data-target="#documentosFiadorModal">Clique aqui</button></p>
                    <p>Para Excluir <a href="<?php echo base_url('cli/excluir-documentacao-fiador/1') ?>">Clique aqui</a></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Comprovante de Endereço</h2></p>
                        <?php if(!isset($tem_comprovante)){ ?>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#enderecoModal">
                            <h4><i class="material-icons">home</i>Água, Luz ou Telefone</h4>
                        </button>
                        <?php } else{ ?>
                    <p>Você já enviou seu comprovante de endereço</p>
                    <p>Para visualiza-lo<button type="button" class="btn btn-link" data-toggle="modal" data-target="#comprovanteEnderecoFiadorModal">Clique aqui</button></p>
                    <p>Para Excluir <a href="<?php echo base_url('cli/excluir-documentacao-fiador/4') ?>">Clique aqui</a></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Comprovação de Estado Civil</h2></p>
                        <?php if(!isset($tem_estado_civil)){ ?>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#estadoCivilModal">
                            <h4><i class="material-icons">supervisor_account</i> Certidões</h4>
                        </button>
                        <?php } else{ ?>
                    <p>Você já enviou seu comprovante de estado civil</p>
                    <p>Para visualiza-lo<button type="button" class="btn btn-link" data-toggle="modal" data-target="#comprovanteCivilFiadorModal">Clique aqui</button></p>
                    <p>Para Excluir <a href="<?php echo base_url('cli/excluir-documentacao-fiador/7') ?>">Clique aqui</a></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Comprovação de Renda</h2></p>
                        <?php if(!isset($tem_renda)){ ?>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#rendaModal">
                            <h4><i class="material-icons">storage</i> Comprovantes de Renda</h4>
                        </button>
                        <?php } else{ ?>
                    <p>Você já enviou seus comprovantes de renda</p>
                    <p>Para visualiza-lo<button type="button" class="btn btn-link" data-toggle="modal" data-target="#comprovanteRendaFiadorModal">Clique aqui</button></p>
                    <p>Para Excluir <a href="<?php echo base_url('cli/excluir-documentacao-fiador/13') ?>">Clique aqui</a></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
        </div>
        <?php 
        if($tem_conjugue['id_estado_civil']=="2"){?>
        <div class="jumbotron jumbotron-fluid espacamento-titulo-pagina">
            <h1>Dados do Cônjugue</h1>
        </div>
        <div class="row">
            <div class="col">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Enviar RG e CPF / CNH</h2></p>
                        <?php if(!isset($tem_documentos_conjugue)){ ?>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#rgCpfConjugueModal">
                            <h4><i class="material-icons">perm_identity</i>RG e CPF</h4>
                        </button>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#cnhConjugueModal">
                            <h4><i class="material-icons">directions_car</i> CNH</h4>
                        </button>
                        <?php } else{ ?>
                    <p>Você já enviou seus documentos</p>
                    <p>Para visualiza-los<button type="button" class="btn btn-link" data-toggle="modal" data-target="#visualizarDocumentosConjugueFiadorModal">Clique aqui</button></p>
                    <p>Para Excluir <a href="<?php echo base_url('cli/excluir-documentacao-fiador/50') ?>">Clique aqui</a></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
             <div class="col">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <p><h2>Comprovação de Renda</h2></p>
                        <?php if(!isset($tem_renda_conjugue)){ ?>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#rendaConjugueModal">
                            <h4><i class="material-icons">storage</i> Comprovantes de Renda</h4>
                        </button>
                        <?php } else{ ?>
                    <p>Você já enviou seus comprovantes de renda</p>
                    <p>Para visualiza-lo<button type="button" class="btn btn-link" data-toggle="modal" data-target="#comprovanteRendaConjugueFiadorModal">Clique aqui</button></p>
                    <p>Para Excluir <a href="<?php echo base_url('cli/excluir-documentacao-fiador/51') ?>">Clique aqui</a></p>
                       <?php }
                    ?>
                    </div>
                </div>
            </div>
        </div>
        <?php }?>
    </div>
</div>