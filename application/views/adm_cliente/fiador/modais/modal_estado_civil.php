<div class="modal fade" id="estadoCivilModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Enviar Comprovamte de Estado Civil</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form action="<?php echo base_url("cli/validacao-envio-estado-civil-fiador")?>" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
            <div class="modal-body">
                <label><b>É PERMITIDO O ENVIO DAS SEGUINTES EXTENSÕES DE FOTOS/DOCUMENTOS(PNG, JPG, PDF)</b></label>
                <div class="form-group col-md-offset-1 col-md-10">
                    <div class="form-group">
                        <label>Selecione o Comprovante que Você Irá Enviar</label>
                        <select name="tipo_estado_civil" class="form-control">
                           <option value="7">Solteiro (a) - CERTIDÃO DE NASCIMENTO</option>
                           <option value="8">Casado (a) - CERTIDÃO DE CASAMENTO</option>
                           <option value="9">Divorciado (a) - CERTIDÃO DE CASAMENTO COM AVERBAÇÃO</option>
                           <option value="10">Separado (a) - CERTIDÃO DE CASAMENTO COM AVERBAÇÃO</option>
                           <option value="11">União Estável - CERTIDÃO DE UNIÃO ESTÁVEL</option>
                           <option value="12">Viúvo (a) - CERTIDÃO DE CASAMENTO COM AVERBAÇÃO DE ÓBITO OU CERTIDÃO DE CASAMENTO + CERTIDÃO DE ÓBITO</option>
                        </select>
                    </div>
                    <input type="file" name="estado_civil[]" multiple />
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
              <button type="submit" class="btn btn-primary">Salvar</button>
            </div>
        </form>
    </div>
  </div>
</div>