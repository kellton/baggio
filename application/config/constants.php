<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

/*
|--------------------------------------------------------------------------
| CONSTANTES DO PROJETO
|--------------------------------------------------------------------------
*/
/* ***** SISTEMA BAGGIO ***** */
// XML ENCONDE
define('CONS_BAGGIO_XML_ENCODE', "UTF-8");
// QUANTIDADE DE IMOVEIS PARA O RESULTADO DA BUSCA
define('CONS_BAGGIO_BUSCA_QTD', 12);
// TEMPO MAXIMO DE PROCESSAMENTO DO XML
define('CONS_BAGGIO_MAX_TIME_POR_XML', 24 * 3600);
// TIMEZONE DA APLICACAO
//define('CONS_BAGGIO_SYSTEM_TIMEZONE', "America/Sao_Paulo");
define('CONS_BAGGIO_SYSTEM_TIMEZONE', "America/Fortaleza");
// URL DE UPDATE DO XML
define('CONS_BAGGIO_URL_UPDATE_XML', "http://www.redeimoveis.com.br/exportadores/voyo.php?cod=");
// TEMPO DE VIDA DO COOKIE DE FAVORITAR
define('CONS_BAGGIO_FAVORITO_COOKIE_LIFE', 10000 * 1000);
// SEGURANCA DO COOKIE DE FAVORITAR
define('CONS_BAGGIO_FAVORITO_COOKIE_SECURE', FALSE);

/* ***** FOTOS DE IMOVEIS ***** */
// TIPOS DE FOTOS
define('CONS_BAGGIO_FOTOS_TIPOS', "jpg|jpeg|png|bmp|bitmap|gif");
// PERMISSAO DA PASTA DE FOTOS DE IMOVEIS
define('CONS_BAGGIO_FOTOS_PERMISSAO', 0755);
// MAX SIZE DE FOTO DE IMOVEL - MB
define('CONS_BAGGIO_FOTOS_MAX_SIZE', 1024 * 1024);
// CAMINHO PARA OS UPLOADS DE FOTOS DE IMOVEIS
define('CONS_BAGGIO_FOTOS_CAMINHO', "publico".DIRECTORY_SEPARATOR."uploads".DIRECTORY_SEPARATOR."imagens-imoveis".DIRECTORY_SEPARATOR);

/* ***** EMAIL CODEIGNITER - GMAIL ***** */
define('CONS_CODEMAIL_CHARSET', "utf8");
define('CONS_CODEMAIL_TYPE', "html");
define('CONS_CODEMAIL_PROTOCOL', "smtp");
define('CONS_CODEMAIL_HOST', "smtp.gmail.com");
define('CONS_CODEMAIL_PORT', 465);
// define('CONS_CODEMAIL_PORT', 587);
define('CONS_CODEMAIL_SMTP_SECURE', "ssl");
// define('CONS_CODEMAIL_SMTP_SECURE', "tls");
define('CONS_CODEMAIL_USER_NAME', "baggioimoveisga@gmail.com");
define('CONS_CODEMAIL_PASSWORD', "imoveisbaggio3936");
define('CONS_CODEMAIL_FROM', "baggioimoveisga@gmail.com");
define('CONS_CODEMAIL_FROM_NAME', "Baggio");
define('CONS_CODEMAIL_IMG_PATH', FCPATH."publico".DIRECTORY_SEPARATOR."imagens".DIRECTORY_SEPARATOR."baggio-logo-30-anos.png");

/* ***** HOTMAIL CODEIGNITER ***** */
define('CONS_HOTMAIL_HOST', "smtp.live.com");
define('CONS_HOTMAIL_PORT', 587);
define('CONS_HOTMAIL_SMTP_SECURE', "tls");
define('CONS_HOTMAIL_USER_NAME', "baggioimoveis@outlook.com");
define('CONS_HOTMAIL_PASSWORD', " baggio_imoveis2018");
define('CONS_HOTMAIL_FROM', "baggioimoveis@outlook.com");
define('CONS_HOTMAIL_FROM_NAME', "Baggio");

/* ***** EMAIL CODEIGNITER - LW ***** */
define('CONS_LOCAWEB_HOST', "smtplw.com.br");
define('CONS_LOCAWEB_PORT', 587);
define('CONS_LOCAWEB_SMTP_SECURE', "tls");
define('CONS_LOCAWEB_USER_NAME', "imoveisbaggio@baggioimoveis.com.br");
define('CONS_LOCAWEB_PASSWORD', "dXFKVVNPeUExNzlw");
define('CONS_LOCAWEB_FROM', "imoveisbaggio@baggioimoveis.com.br");
define('CONS_LOCAWEB_FROM_NAME', "Baggio");

/* ***** EMAIL API LOCAWEB ***** */
define('CONS_API_LW_X_AUTH', "794c863c996485f3e93fbcfa6ee577c2");
define('CONS_API_LW_URL', "https://api.smtplw.com.br/v1/messages");
define('CONS_API_LW_FROM', "webmaster@baggioimoveis.com.br");
