<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// To use reCAPTCHA, you need to sign up for an API key pair for your site.
// link: http://www.google.com/recaptcha/admin
$config['recaptcha_site_key'] = '6Ld8YEAUAAAAAIPX9E-9NDuZOmVNa_XOm-uzAkJj';
$config['recaptcha_secret_key'] = '6Ld8YEAUAAAAAL-4adQBPWwwVLoWlOf7hzqG0RSi';

// reCAPTCHA supported 40+ languages listed here:
// https://developers.google.com/recaptcha/docs/language
$config['recaptcha_lang'] = 'pt-br';

/* End of file recaptcha.php */
/* Location: ./application/config/recaptcha.php */
