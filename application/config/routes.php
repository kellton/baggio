<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Geral';
$route['enviar-gmail/(:any)'] = 'Geral/teste_gmail/$1';
$route['enviar-hotmail/(:any)'] = 'Geral/teste_hotmail/$1';
$route['enviar-locaweb/(:any)'] = 'Geral/teste_locaweb/$1';
$route['testar-horario'] = 'Geral/teste_horario';
$route['sobre-nos'] = 'Geral/sobre_nos';
$route['contato'] = 'Geral/contato';
$route['validacao-contato'] = 'Geral/validacao_contato';
$route['cadastro'] = 'Geral/cadastro';
$route['trabalhe-conosco'] = 'Geral/trabalhe_conosco';
$route['validacao-trabalhe-conosco'] = 'Geral/validacao_trabalhe_conosco';
$route['validacao-cadastro'] = 'Geral/validacao_cadastro';
$route['agendar-visita'] = 'Geral/agendar_visita';
$route['validacao-agendamento'] = 'Geral/validacao_agendar_visita';
$route['validacao-agendamento-imovel/(:num)'] = 'Geral/validacao_agendar_visita_imovel/$1';
$route['imovel/visualizar-imovel/(:num)'] = 'Geral/visualizar_imovel/$1';
$route['imovel/(:any)'] = 'Geral/visualizar_imovel_referencia/$1';
$route['imovel/favoritar'] = 'Geral/favoritar';
$route['imovel/favoritar/(:num)'] = 'Geral/favoritar/$1';
$route['imovel/desgostar'] = 'Geral/desgostar';
$route['favoritos'] = 'Geral/listagem_imoveis_favoritos';
$route['documentacao-necessaria'] = 'Geral/documentacao_necessaria';
$route['imovel/comparar-locacao/(:num)'] = 'Geral/comparar_locacao/$1';
$route['imovel/comparar-venda/(:num)'] = 'Geral/comparar_venda/$1';
$route['imovel/remover-favorito'] = 'Geral/remover_favorito';
$route['imovel/remover-comparacao-locacao/(:num)'] = 'Geral/remover_listagem_locacao/$1';
$route['imovel/remover-comparacao-venda/(:num)'] = 'Geral/remover_listagem_venda/$1';
$route['imovel/visualizar-comparacao-locacao'] = 'Geral/listagem_comparacao_locacao';
$route['imovel/visualizar-comparacao-venda'] = 'Geral/listagem_comparacao_venda';
$route['imovel/validacao-envio-imovel-email/(:num)'] = 'Geral/validadacao_envio_imovel_email/$1';
$route['adminstre-meu-imovel'] = 'Geral/administre_meu_imovel';
$route['validacao-administrar-meu-imovel'] = 'Geral/validadacao_administre_meu_imovel';
$route['venda-meu-imovel'] = 'Geral/venda_meu_imovel';
$route['validacao-venda-meu-imovel'] = 'Geral/validacao_venda_meu_imovel';
$route['imovel/validacao-mais-informacoes/(:num)'] = 'Geral/validacao_mais_informacoes_imovel/$1';
$route['esqueci-senha'] = 'Geral/esqueci_senha';
$route['validacao-esqueci-senha'] = 'Geral/validacao_esqueci_senha';
$route['alterar-senha'] = 'Geral/alteracao_senha';
$route['validacao-alteracao-senha'] = 'Geral/validacao_alteracao_senha';

$route['adm/home'] = 'adm/HomeController';

$route['cli'] = 'adm/cliente/AreaClienteController';

$route['404_override'] = 'Geral';

$route['translate_uri_dashes'] = FALSE;

/* *************************** ROTAS DE LOGIN *********************************** */
$route['adm'] = 'LoginController';
$route['adm/validacao'] = 'LoginController/validacao';
$route['login'] = 'LoginClienteController';
$route['login/validacao'] = 'LoginClienteController/validacao';

/* ****************** ROTAS DO GERENCIADOR DE ACESSOS *************************** */
$route['logout'] = 'GerenciadorController/logout';
$route['gerenciador'] = 'GerenciadorController';

/* *************************** ROTAS DO CLIENTE ********************************* */
$route['adm/clientes'] = 'adm/ClienteController';
$route['adm/clientes/paginacao/(:any)'] = 'adm/ClienteController';
$route['adm/clientes/paginacao'] = 'adm/ClienteController';
$route['adm/clientes/resultados'] = 'adm/ClienteController/pesquisa_cliente';
$route['adm/clientes/adicionar-cliente'] = 'adm/ClienteController/adicionar_cliente';
$route['adm/clientes/adicionar-cliente/validacao'] = 'adm/ClienteController/validacao';
$route['adm/clientes/editar-cliente/(:any)'] = 'adm/ClienteController/editar_cliente/$1';
$route['adm/clientes/editar-cliente/validacao/(:any)'] = 'adm/ClienteController/validacao_editar_cliente/$1';
$route['adm/clientes/editar-cliente-juridico/validacao/(:any)'] = 'adm/ClienteController/validacao_editar_cliente_juridico/$1';
$route['adm/clientes/visualizar-cliente/(:any)'] = 'adm/ClienteController/visualizar_cliente/$1';
$route['adm/clientes/excluir-cliente/(:any)'] = 'adm/ClienteController/excluir_cliente/$1';
$route['adm/clientes/excluir-cliente/validacao/(:any)'] = 'adm/ClienteController/validacao_excluir_cliente/$1';

/* ************************** ROTAS DOS USUARIOS ********************************* */
$route['adm/usuarios'] = 'adm/UsuarioController';
$route['adm/usuarios/paginacao/(:any)'] = 'adm/UsuarioController';
$route['adm/usuarios/paginacao'] = 'adm/UsuarioController';
$route['adm/usuarios/adicionar-usuario'] = 'adm/UsuarioController/adicionar_usuario';
$route['adm/usuarios/adicionar-usuario/validacao'] = 'adm/UsuarioController/validacao';
$route['adm/usuarios/editar-usuario/(:any)'] = 'adm/UsuarioController/editar_usuario/$1';
$route['adm/usuarios/editar-usuario/validacao/(:any)'] = 'adm/UsuarioController/validacao_editar_usuario/$1';
$route['adm/usuarios/excluir-usuario/(:any)'] = 'adm/UsuarioController/excluir_usuario/$1';
$route['adm/usuarios/excluir-usuario/validacao/(:any)'] = 'adm/UsuarioController/validacao_excluir_usuario/$1';
$route['adm/usuarios/reativar-usuario/(:any)'] = 'adm/UsuarioController/validacao_reativar_usuario/$1';
$route['adm/usuarios/alterar-dados'] = 'adm/UsuarioController/alterar_dados';
$route['adm/usuarios/alterar-dados/validacao'] = 'adm/UsuarioController/validacao_alterar_meus_dados';

/* *************************** ROTAS DE IMOVEL ********************************* */
$route['adm/imoveis'] = 'adm/ImovelController';
$route['adm/imoveis/paginacao'] = 'adm/ImovelController';
$route['adm/imoveis/paginacao/(:num)'] = 'adm/ImovelController';
$route['adm/imoveis/infraestrutura/(:num)'] = 'adm/ImovelController/buscar_infraestrutura';
$route['adm/imoveis/especificacoes/(:num)'] = 'adm/ImovelController/buscar_especificacoes';
$route['adm/imoveis/informacoes-complementares/(:num)'] = 'adm/ImovelController/buscar_informacoes_complementares';
$route['adm/imoveis/cadastrar-imovel'] = 'adm/ImovelController/carregar_formulario_imovel';
$route['adm/imoveis/adicionar-imovel'] = 'adm/ImovelController/adicionar_imovel';
$route['adm/gerenciar/imoveis'] = 'adm/ImovelController/carregar_gerenciar_imoveis';
$route['adm/gerenciar/imoveis/(:num)'] = 'adm/ImovelController/carregar_gerenciar_imoveis';
$route['adm/imoveis/editar-imovel/(:num)'] = 'adm/ImovelController/carregar_editar_imovel';
$route['adm/imoveis/atualizar-imovel/(:num)'] = 'adm/ImovelController/editar_imovel';
$route['adm/excluir/imovel/(:num)'] = 'adm/ImovelController/excluir_imovel';

/* *************************** ROTAS DE OUTROS ********************************* */
$route['adm/gerenciar/auxiliador-bairros'] = 'adm/SistemaController/carregar_gerenciar_aux_bairros';
$route['adm/gerenciar/auxiliador-bairros/(:num)'] = 'adm/SistemaController/carregar_gerenciar_aux_bairros';
$route['adm/adicionar/aux-bairro'] = 'adm/SistemaController/carregar_adicionar_aux_bairro';
$route['adm/adicionar/cadastrar-aux-bairro'] = 'adm/SistemaController/cadastrar_aux_bairro';
$route['adm/editar/aux-bairro/(:num)'] = 'adm/SistemaController/editar_aux_bairro';
$route['adm/excluir/aux-bairro/(:num)'] = 'adm/SistemaController/excluir_aux_bairro';

$route['adm/gerenciar/bairros'] = 'adm/SistemaController/carregar_gerenciar_bairros';
$route['adm/adicionar/bairro'] = 'adm/SistemaController/carregar_adicionar_bairro';
$route['adm/adicionar/cadastrar-bairro'] = 'adm/SistemaController/cadastrar_bairro';
$route['adm/excluir/bairro/(:num)'] = 'adm/SistemaController/excluir_bairro';

$route['adm/gerenciar/cidades'] = 'adm/SistemaController/carregar_gerenciar_cidades';
$route['adm/adicionar/cidade'] = 'adm/SistemaController/carregar_adicionar_cidade';
$route['adm/adicionar/cadastrar-cidade'] = 'adm/SistemaController/cadastrar_cidade';
$route['adm/excluir/cidade/(:num)'] = 'adm/SistemaController/excluir_cidade';

$route['adm/gerenciar/estados'] = 'adm/SistemaController/carregar_gerenciar_estados';
$route['adm/adicionar/estado'] = 'adm/SistemaController/carregar_adicionar_estado';
$route['adm/adicionar/cadastrar-estado'] = 'adm/SistemaController/cadastrar_estado';
$route['adm/excluir/estado/(:num)'] = 'adm/SistemaController/excluir_estado';

$route['adm/gerenciar/jobs'] = 'adm/SistemaController/carregar_gerenciar_jobs';
$route['adm/adicionar/job'] = 'adm/SistemaController/carregar_adicionar_job';
$route['adm/adicionar/cadastrar-job'] = 'adm/SistemaController/cadastrar_job';
$route['adm/alternar-ativacao/job/(:num)'] = 'adm/SistemaController/alternar_ativacao_job';
$route['adm/rodar/job/(:num)'] = 'xml/XmlController/atualizar_imobiliaria';

/* ******************* ROTAS ÁREA DO CLIENTE PESSOA FÍSICA ********************* */
$route['cli/alterar-dados'] = 'adm/cliente/AreaClienteController/alterar_dados';
$route['cli/alterar-dados/validacao'] = 'adm/cliente/AreaClienteController/validacao_editar_cliente';
$route['cli/documentacao'] = 'adm/cliente/AreaClienteController/documentacao';
$route['cli/adicionar-documentacao/(:num)'] = 'adm/cliente/DocumentacaoController/adicionar_documentacao/$1';
$route['cli/validacao-envio-rg-cpf/(:num)'] = 'adm/cliente/DocumentacaoController/validacao_rg_cpf/$1';
$route['cli/validacao-envio-cnh/(:num)'] = 'adm/cliente/DocumentacaoController/validacao_cnh/$1';
$route['cli/validacao-envio-endereco/(:num)'] = 'adm/cliente/DocumentacaoController/validacao_endereco/$1';
$route['cli/validacao-envio-estado-civil/(:num)'] = 'adm/cliente/DocumentacaoController/validacao_estado_civil/$1';
$route['cli/validacao-envio-comprovacao-renda/(:num)'] = 'adm/cliente/DocumentacaoController/validacao_comprovacao_renda/$1';
$route['cli/validacao-envio-documentacao-residente/(:num)'] = 'adm/cliente/DocumentacaoController/validacao_documentacao_residente/$1';
$route['cli/validacao-envio-outros-documentos/(:num)'] = 'adm/cliente/DocumentacaoController/validacao_outros_documentos/$1';
$route['cli/validacao-envio-rg-cpf-conjugue/(:num)'] = 'adm/cliente/DocumentacaoController/validacao_rg_cpf_conjugue/$1';
$route['cli/validacao-envio-cnh-conjugue/(:num)'] = 'adm/cliente/DocumentacaoController/validacao_cnh_conjugue/$1';
$route['cli/validacao-envio-comprovacao-renda-conjugue/(:num)'] = 'adm/cliente/DocumentacaoController/validacao_renda_conjugue/$1';
$route['cli/ficha-cadastro'] = 'adm/cliente/FichaCadastroController';
$route['cli/editar-ficha-cadastro'] = 'adm/cliente/FichaCadastroController/editar_ficha_cadastro';
$route['cli/editar-ficha-cadastro-segundo-pretendente'] = 'adm/cliente/FichaCadastroController/editar_ficha_cadastro_segundo_pretendente';
$route['cli/editar-ficha-cadastro-fiador'] = 'adm/cliente/FichaCadastroController/editar_ficha_cadastro_fiador';
$route['cli/adicionar-ficha-cadastro'] = 'adm/cliente/FichaCadastroController/adicionar_ficha_cadastro';
$route['cli/adicionar-ficha-cadastro/validacao'] = 'adm/cliente/FichaCadastroController/validacao_ficha_cadastro';
$route['cli/adicionar-documentacao-segundo-pretendente'] = 'adm/cliente/FichaCadastroController/adicionar_ficha_cadastro_segundo_pretendente';
$route['cli/adicionar-ficha-cadastro-segundo-pretendente/validacao'] = 'adm/cliente/FichaCadastroController/validacao_ficha_cadastro_segundo_pretendente';
$route['cli/ficha-cadastro/visualizar'] = 'adm/cliente/AreaClienteController/visualizar_ficha_pretendente';
$route['cli/ficha-cadastro-segundo-pretendente/visualizar'] = 'adm/cliente/AreaClienteController/visualizar_ficha_segundo_pretendente';
$route['cli/adicionar-ficha-cadastro-fiador'] = 'adm/cliente/FichaCadastroController/adicionar_ficha_cadastro_fiador';
$route['cli/adicionar-ficha-cadastro-fiador/validacao'] = 'adm/cliente/FichaCadastroController/validacao_fica_cadastro_fiador';
$route['cli/ficha-cadastro-fiador/visualizar'] = 'adm/cliente/AreaClienteController/visualizar_ficha_fiador';

$route['cli/adicionar-documentacao-fiador'] = 'adm/cliente/DocumentacaoFiadorController';
$route['cli/validacao-envio-rg-cpf-fiador'] = 'adm/cliente/DocumentacaoFiadorController/validacao_rg_cpf';
$route['cli/validacao-envio-cnh-fiador'] = 'adm/cliente/DocumentacaoFiadorController/validacao_cnh';
$route['cli/validacao-envio-endereco-fiador'] = 'adm/cliente/DocumentacaoFiadorController/validacao_endereco';
$route['cli/validacao-envio-estado-civil-fiador'] = 'adm/cliente/DocumentacaoFiadorController/validacao_estado_civil';
$route['cli/validacao-envio-comprovacao-renda-fiador'] = 'adm/cliente/DocumentacaoFiadorController/validacao_comprovacao_renda';
$route['cli/validacao-envio-rg-cpf-conjugue-fiador'] = 'adm/cliente/DocumentacaoFiadorController/validacao_rg_cpf_conjugue';
$route['cli/validacao-envio-cnh-conjugue-fiador'] = 'adm/cliente/DocumentacaoFiadorController/validacao_cnh_conjugue';
$route['cli/validacao-envio-comprovacao-renda-conjugue-fiador'] = 'adm/cliente/DocumentacaoFiadorController/validacao_renda_conjugue';

$route['cli/excluir-documentacao/(:num)/(:num)'] = 'adm/cliente/DocumentacaoController/excluir_documentacao/$1/$2';
$route['cli/excluir-documentacao-fiador/(:num)'] = 'adm/cliente/DocumentacaoFiadorController/excluir_documentacao/$1';
$route['cli-juridico/excluir-documentacao/(:num)'] = 'adm/cliente/DocumentacaoJuridicoController/excluir_documentacao/$1';

/* ***************** ROTAS ÁREA DO CLIENTE PESSOA JURÍDICA ******************* */
$route['cli-juridico'] = 'adm/cliente/AreaClienteJuridicoController';
$route['cli-juridico/alterar-dados'] = 'adm/cliente/AreaClienteJuridicoController/alterar_dados';
$route['cli-juridico/alterar-dados/validacao'] = 'adm/cliente/AreaClienteJuridicoController/validacao_editar_cliente';
$route['cli-juridico/adicionar-ficha'] = 'adm/cliente/FichaCadastroJuridicoController/adicionar_ficha';
$route['cli-juridico/editar-ficha'] = 'adm/cliente/FichaCadastroJuridicoController/editar_ficha';
$route['cli-juridico/adicionar-ficha-cadastro/validacao'] = 'adm/cliente/FichaCadastroJuridicoController/validacao_ficha_cadastro';
$route['cli-juridico/visualizar-ficha-cadastro'] = 'adm/cliente/FichaCadastroJuridicoController/visualizar_ficha_cadastro';
$route['cli-juridico/documentacao'] = 'adm/cliente/DocumentacaoJuridicoController';
$route['cli-juridico/validacao-envio-contrato-social'] = 'adm/cliente/DocumentacaoJuridicoController/validacao_contrato_social';
$route['cli-juridico/validacao-envio-altercao-contrato-social'] = 'adm/cliente/DocumentacaoJuridicoController/validacao_alteracao_contrato_social';
$route['cli-juridico/validacao-envio-balanco'] = 'adm/cliente/DocumentacaoJuridicoController/validacao_balanco';
$route['cli-juridico/validacao-envio-balancete'] = 'adm/cliente/DocumentacaoJuridicoController/validacao_balancete';
$route['cli-juridico/validacao-envio-cartao-cnpj'] = 'adm/cliente/DocumentacaoJuridicoController/validacao_cartao_cnpj';
$route['cli-juridico/validacao-envio-imposto-renda'] = 'adm/cliente/DocumentacaoJuridicoController/validacao_imposto_renda';
$route['cli-juridico/validacao-envio-certidao-negativa'] = 'adm/cliente/DocumentacaoJuridicoController/validacao_certidao_negativa';
$route['cli-juridico/validacao-envio-rg-cpf'] = 'adm/cliente/DocumentacaoJuridicoController/validacao_rg_cpf';
$route['cli-juridico/validacao-envio-cnh'] = 'adm/cliente/DocumentacaoJuridicoController/validacao_cnh';
$route['cli-juridico/validacao-comprovante-endereco'] = 'adm/cliente/DocumentacaoJuridicoController/validacao_comprovante_endereco';
$route['cli-juridico/validacao-envio-outros-documentos'] = 'adm/cliente/DocumentacaoJuridicoController/validacao_outros_documentos';

/* ************************** ROTAS API ABERTA DE BUSCA ********************************* */
$route['async/buscar/imoveis'] = 'api/ApiAbertaController/async_buscar_imoveis';
$route['async/buscar/copiar'] = 'api/ApiAbertaController/async_copiar_busca';
$route['async/buscar/referencias'] = 'api/ApiAbertaController/async_buscar_referencias';
$route['async/buscar/condominios'] = 'api/ApiAbertaController/async_buscar_condominios';
$route['async/buscar/vendas'] = 'api/ApiAbertaController/async_buscar_vendas';
$route['async/buscar/locacoes'] = 'api/ApiAbertaController/async_buscar_locacoes';
$route['async/buscar/informacoes-busca'] = 'api/ApiAbertaController/async_buscar_informacoes';
$route['async/buscar/avancada'] = 'api/ApiAbertaController/async_busca_avancada';
$route['async/buscar/bairros'] = 'api/ApiAbertaController/async_buscar_bairros';

$route['buscar/sessao/imoveis'] = 'BuscaController/carregar_busca_imoveis_sessao';
$route['buscar/imoveis'] = 'BuscaController/carregar_busca_imoveis';
$route['buscar/url/imoveis/(:any)'] = 'BuscaController/carregar_busca_imoveis_url/$1';

/* ************************** ROTAS API DE BUSCA ********************************** */
$route['api/adm/busca/proprietarios'] = 'api/ApiController/buscar_proprietarios';

/* ************************** ROTAS XML ******************************************* */
//$route['xml/atualizar'] = 'xml/XmlController/atualizar';
$route['xml/verificar-atualizacoes'] = 'xml/XmlController/verificar_atualizacoes';

/* ******************* ROTAS DE GERAÇAO DE RELATÓRIOS *********************** */
$route['adm/clientes/relatorio/pretendente/(:num)'] = 'adm/RelatorioController/relatorio_ficha_pretendente_pessoa_fisica/$1';
$route['adm/clientes/relatorio/segundo-pretendente/(:num)'] = 'adm/RelatorioController/relatorio_ficha_segundo_pretendente_pessoa_fisica/$1';
$route['adm/clientes/relatorio/fiador/(:num)'] = 'adm/RelatorioController/relatorio_ficha_fiador/$1';
$route['adm/clientes/relatorio/pessoa-juridica/(:num)'] = 'adm/RelatorioController/relatorio_pessoa_juridica/$1';
 
