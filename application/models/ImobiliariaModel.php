<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * IMOBIALIARIA_MODEL
 * -------------------------------------------------------------------
 * DESCRICAO : 
 * -------------------------------------------------------------------
 * CARREGA : 
 * - N/A
 */
class ImobiliariaModel extends CI_Model {

    /* ****************************************************** */
    /* ********************** LISTAGEM ********************** */
    /* ****************************************************** */
    /*
     * LISTAR_JOBS
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function listar_jobs() {
        try {
            $this->db->from('tb_jobs_xml AS j');
            $this->db->join('tb_imobiliaria AS imo','j.id_imobiliaria=imo.id_imobiliaria');
            return $this->db->get()->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR JOBS - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /*
     * LISTAR_JOBS_ATIVOS
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function listar_jobs_ativos() {
        try {
            $this->db->from('tb_jobs_xml AS j');
            $this->db->where('j.ativo', TRUE);
            $this->db->join('tb_imobiliaria AS imo','j.id_imobiliaria=imo.id_imobiliaria');
            return $this->db->get()->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR JOBS - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /*
     * LISTAR_IMOBILIARIAS
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function listar_imobiliarias() {
        try {
            return $this->db->get('tb_imobiliaria')->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR IMOBILIARIAS - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /* ****************************************************** */
    /* *********************** BUSCAS *********************** */
    /* ****************************************************** */
    /*
     * BUSCAR_IMOBILIARIA
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function buscar_imobiliaria($id_imobiliaria=0) {
        try {
            $this->db->from('tb_imobiliaria');
            $this->db->where('id_imobiliaria', $id_imobiliaria);
            return $this->db->get()->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR IMOBILIARIA - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /*
     * BUSCAR_JOB_IMOBILIARIA
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function buscar_job_imobiliaria($codigo=0) {
        try {
            $this->db->from('tb_jobs_xml');
            $this->db->where('codigo', $codigo);
            return $this->db->get()->result_array()[0];
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR JOB IMOBILIARIA - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /* ****************************************************** */
    /* ********************** INSERCAO ********************** */
    /* ****************************************************** */
    /*
     * INSERIR_JOB
     * ---------------------------------------
     * 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [INT] - 
    */
    public function inserir_job($dados=array()) {
        try {
            if (!empty($dados)) {
                $this->db->insert("tb_jobs_xml", $dados);
                return $this->db->insert_id();
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - INSERIR JOB - ' . $e->getMessage());
            return -1;
        }
        return -1;
    }

    /* ****************************************************** */
    /* *********************** FUNCOES ********************** */
    /* ****************************************************** */
    /*
     * ATUALIZAR_JOB
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function atualizar_job($id_job=0, $date_time=NULL) {
        try {
            $this->db->set('data_ultimo_job', $date_time);
            $this->db->where('id_jobs_xml', $id_job);
            $this->db->update('tb_jobs_xml AS j');
            return TRUE;
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ATUALIZAR JOB - ' . $e->getMessage());
            return FALSE;
        }
        return FALSE;
    }

    /*
     * ESTA_ATIVO_JOB
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function esta_ativo_job($id_job=0) {
        try {
            $this->db->from('tb_jobs_xml');
            $this->db->where('id_jobs_xml', $id_job);
            $this->db->where('ativo', TRUE);
            
            return $this->db->count_all_results() > 0;
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ESTA ATIVO JOB - ' . $e->getMessage());
            return FALSE;
        }
        return FALSE;
    }

    /*
     * ATIVAR_JOB
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function ativar_job($id_job=0) {
        try {
            $this->db->set('ativo', TRUE);
            $this->db->where('id_jobs_xml', $id_job);
            $this->db->update('tb_jobs_xml AS j');
            return TRUE;
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ATIVAR JOB - ' . $e->getMessage());
            return FALSE;
        }
        return FALSE;
    }

    /*
     * DESATIVAR_JOB
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function desativar_job($id_job=0) {
        try {
            $this->db->set('ativo', FALSE);
            $this->db->where('id_jobs_xml', $id_job);
            $this->db->update('tb_jobs_xml AS j');
            return TRUE;
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - DESATIVAR JOB - ' . $e->getMessage());
            return FALSE;
        }
        return FALSE;
    }

}
