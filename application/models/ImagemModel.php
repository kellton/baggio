<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * IMAGEM_MODEL
 * -------------------------------------------------------------------
 * DESCRICAO : 
 * -------------------------------------------------------------------
 * CARREGA : 
 * - N/A
 */
class ImagemModel extends CI_Model {

    /* ****************************************************** */
    /* ********************** INSERCAO ********************** */
    /* ****************************************************** */
    /*
     * INSERIR_IMAGEM
     * ---------------------------------------
     * 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [INT] - 
    */
    public function inserir_imagem($dados=array()) {
        try {
            if (!empty($dados)) {
                $this->db->insert("tb_imagens_imovel", $dados);
                return $this->db->insert_id();
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - INSERIR IMAGEM - ' . $e->getMessage());
            return -1;
        }
        return -1;
    }

    /*
     * INSERIR_IMAGENS
     * ---------------------------------------
     * 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [INT] - 
    */
    public function inserir_imagens($dados=array()) {
        try {
            if (!empty($dados)) {
                $this->db->insert_batch("tb_imagens_imovel", $dados);
                return TRUE;
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - INSERIR IMAGEM - ' . $e->getMessage());
            return FALSE;
        }
        return FALSE;
    }

    /* ****************************************************** */
    /* ********************** EXCLUSAO ********************** */
    /* ****************************************************** */
    /*
     * EXCLUIR_IMAGENS_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function excluir_imagens_imovel($id_imovel=0) {
        try {
            $this->db->where('id_imovel', $id_imovel);
            $this->db->delete('tb_imagens_imovel');

            return TRUE;
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - EXCLUIR IMAGENS IMOVEL - ' . $e->getMessage());
            return FALSE;
        }
        return FALSE;
    }

}
