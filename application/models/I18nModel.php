<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * I18N_MODEL
 * -------------------------------------------------------------------
 * DESCRICAO : Arquivo de funcoes para traducao.
 * -------------------------------------------------------------------
 * CARREGA : 
 * - N/A
 */
class I18nModel extends CI_Model {

    protected $idioma;
    protected $array_idioma;


    /*
     * CONSTRUCT
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function __construct() {
        parent::__construct();

        $this->array_idioma = array();

        $this->carregar_idioma();
    }


    /*
     * T
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : $CHAVE [STRING]
     *              $UC_FIRST [BOOLEAN]
     *              $UC_WORDS [BOOLEAN]
     *              $UC [BOOLEAN]
     * ---------------------------------------
     * RETORNO : [STRING]
    */
    public function t($chave='', $uc_first=FALSE, $uc_words=FALSE, $uc=FALSE) {
        if (!isset($this->array_idioma) || empty($this->array_idioma)) {
            $this->carregar_idioma($idioma);
        }
        if (array_key_exists($chave, $this->array_idioma)) {
            if ($uc_first) {
                return ucfirst($this->array_idioma[$chave]);
            }
            if ($uc_words) {
                return $this->_mb_convert_case_utf8_variation($this->array_idioma[$chave], MB_CASE_TITLE);
            }
            if ($uc) {
                return $this->_mb_convert_case_utf8_variation($this->array_idioma[$chave], MB_CASE_UPPER);
            }
            return $this->array_idioma[$chave];
        }
        return $chave;
    }

    /*
     * T_BOOLEAN
     * ---------------------------------------
     * DESCRICAO : Traduz o booleano para as
     * labels "sim" ou "nao".
     * ---------------------------------------
     * PARAMETROS : $BOOLEAN [BOOLEAN]
     *              $UC_FIRST [BOOLEAN]
     * ---------------------------------------
     * RETORNO : [STRING]
    */
    public function t_boolean($boolean, $uc_first=TRUE) {
        if (isset($boolean) && $boolean) {
            return $this->t('label-sim', $uc_first);
        }
        return $this->t('label-nao', $uc_first);
    }

    /*
     * CARREGAR_IDIOMA
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : $LANG [STRING]
     * ---------------------------------------
     * RETORNO : [STRING]
    */
    public function carregar_idioma($lang='pt-br') {
        $this->idioma = $lang;
        try {
            $this->db->select( array('chave', 'valor') );
            $this->db->from('i18n');
            $this->db->where('idioma',$this->idioma);

            $lista = $this->db->get()->result_array();

            foreach ($lista as $linha) {
                $this->array_idioma[$linha['chave']] = $linha['valor'];
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - CARREGAR IDIOMA - ' . $e->getMessage());
            return array();
        }
        return array();
    }


    /*
     * MB_CONVERT_CASE_UTF8_VARIATION
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : $LANG [STRING]
     * ---------------------------------------
     * RETORNO : [STRING]
    */
    private function _mb_convert_case_utf8_variation($s, $modo=MB_CASE_LOWER) {
    $arr = preg_split("//u", $s, -1, PREG_SPLIT_NO_EMPTY);
    $result = "";
    $mode = FALSE;
    foreach ($arr as $char) {
        $res = preg_match(
            '/\\p{Mn}|\\p{Me}|\\p{Cf}|\\p{Lm}|\\p{Sk}|\\p{Lu}|\\p{Ll}|'.
            '\\p{Lt}|\\p{Sk}|\\p{Cs}/u', $char) == 1;
        if ($mode) {
            if (!$res)
                $mode = FALSE;
        }
        elseif ($res) {
            $mode = TRUE;
            $char = mb_convert_case($char, $modo, "UTF-8");
        }
        $result .= $char;
    }

    return $result;
}
}
