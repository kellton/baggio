<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * NEGOCIACAO_MODEL
 * -------------------------------------------------------------------
 * DESCRICAO : 
 * -------------------------------------------------------------------
 * CARREGA : 
 * - N/A
 */
class NegociacaoModel extends CI_Model {

    /* ****************************************************** */
    /* ********************** INSERCAO ********************** */
    /* ****************************************************** */
    /*
     * INSERIR_VALORES
     * ---------------------------------------
     * 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [INT] - 
    */
    public function inserir_valores($dados=array()) {
        try {
            if (!empty($dados)) {
                $this->db->insert("tb_valores_imovel", $dados);
                return $this->db->insert_id();
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - INSERIR VALORES - ' . $e->getMessage());
            return NULL;
        }
        return NULL;
    }

    /*
     * INSERIR_NEGOCIACAO
     * ---------------------------------------
     * 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [INT] - 
    */
    public function inserir_negociacao($dados=array()) {
        try {
            if (!empty($dados)) {
                $this->db->insert("tb_negociacao", $dados);
                return $this->db->insert_id();
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - INSERIR NEGOCIACAO - ' . $e->getMessage());
            return NULL;
        }
        return NULL;
    }

    /* ****************************************************** */
    /* ********************** EXCLUSAO ********************** */
    /* ****************************************************** */
    /*
     * EXCLUIR_NEGOCIACAO_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function excluir_negociacao_imovel($id_imovel=0) {
        $id_negociacao = -1;
        try {
            $this->db->select('id_negociacao');
            $this->db->from('tb_negociacao');
            $this->db->where('id_imovel', $id_imovel);

            $result_array = $this->db->get()->result_array();

            if (!empty($result_array)) {
                $id_negociacao = intval( $result_array[0]['id_negociacao'] );
            } 

            $this->db->where('id_imovel', $id_imovel);
            $this->db->delete('tb_negociacao');
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - EXCLUIR NEGOCIACAO IMOVEL - ' . $e->getMessage());
            return $id_negociacao;
        }
        return $id_negociacao;
    }

}
