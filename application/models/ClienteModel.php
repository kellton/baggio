<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ClienteModel extends CI_Model {
    
    /*************SELECTs NA TABELA CLIENTE E TABELAS LIGADAS******************/
    
   public function listar_usuarios($qtde=0, $inicio=0,$id_corretor=NULL, $busca=NULL){
        try {
            if($qtde > 0){
                $this->db->limit($qtde, $inicio);
            }
            $this->db->select('tb_clientes.*,tb_tipo_cliente.*,tb_usuarios.nome_usuario');
            $this->db->from('tb_clientes');
            $this->db->join('tb_tipo_cliente','tb_clientes.id_tipo_cliente=tb_tipo_cliente.id_tipo_cliente');
            $this->db->join('tb_usuarios','tb_clientes.id_corretor=tb_usuarios.id_usuario');
            if($busca!=NULL) {
                $busca = trim(mb_strtolower($busca, "UTF-8"));

                $this->db->like('TRIM(LOWER(tb_clientes.nome_cliente))', $busca);
                $this->db->or_like('TRIM(LOWER(tb_clientes.email))', $busca);
            }
            if($id_corretor!=NULL){
                echo "teste";
                $this->db->where('tb_clientes.id_corretor',$id_corretor);
            }
            return $this->db->get();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR USUARIOS - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function listar_tipo_usuario() {
        try {
            return $this->db->get('tb_tipo_cliente')->result();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR TIPO USUARIOS - ' . $e->getMessage());
            return array();
        }
        return array();
    }
        
    public function buscar_cliente($id_cliente,$id_corretor=NULL) {
        try {
            $this->db->from('tb_clientes');
            if($id_corretor!=NULL){
                $this->db->where('tb_clientes.id_corretor',$id_corretor);
            }
            $this->db->where('tb_clientes.id_cliente',$id_cliente);
            return $this->db->get()->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - PEGAR CLIENTES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function listar_estado_civil() {
        try {
            return $this->db->get('tb_estados_civis')->result();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR ESTADO CIVIL - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function listar_profissoes() {
        try {
            return $this->db->get('tb_profissoes')->result();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR PROFISSOES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function listar_perfil_cliente() {
        try {
            return $this->db->get('tb_perfis_cliente')->result();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR PERFIL CLIENTE - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function listar_corretores() {
        try {
            $this->db->where('id_tipo_usuario',4);
            return $this->db->get('tb_usuarios')->result();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR PERFIL CLIENTE - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function listar_corretores_cadastro_cliente() {
        try {
            $this->db->where('ativo',1);
            $this->db->where('id_tipo_usuario',4);
            $this->db->order_by('nome_usuario','ASC');
            return $this->db->get('tb_usuarios')->result();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR CORRETORES CADASTRO CLIENTE - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function busca_email_corretor($id_corretor) {
        try {
            $this->db->select('email');
            $this->db->where('id_usuario',$id_corretor);
            return $this->db->get('tb_usuarios')->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR EMAIL CORRETOR - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function busca_nome_corretor($id_corretor) {
        try {
            $this->db->select('nome_usuario');
            $this->db->where('id_usuario',$id_corretor);
            return $this->db->get('tb_usuarios')->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR NOME CORRETOR - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function buscar_informacoes($id_cliente,$id_corretor=NULL) {
        try {
            $this->db->select('tb_clientes.*,tb_tipo_cliente.*,tb_usuarios.nome_usuario');
            $this->db->from('tb_clientes');
            $this->db->join('tb_tipo_cliente','tb_clientes.id_tipo_cliente=tb_tipo_cliente.id_tipo_cliente','LEFT');
            $this->db->join('tb_usuarios','tb_clientes.id_corretor=tb_usuarios.id_usuario');
            $this->db->where('tb_clientes.id_cliente',$id_cliente);
            if($id_corretor!=NULL){
                $this->db->where('tb_usuarios.id_usuario',$id_corretor);
            }
            return $this->db->get()->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR INFORMACOES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function get_id_fiador($id_cliente) {
        try {
            $this->db->select('id_fiador');
            $this->db->from('tb_fiadores');
            $this->db->where('id_cliente',$id_cliente);
            return $this->db->get()->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR INFORMACOES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function verifica_se_tem_dados($id_cliente) {
        try {
            $this->db->where('id_cliente',$id_cliente);
            $this->db->where('principal',1);
            return $this->db->get('tb_pretendentes')->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR INFORMACOES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function verifica_se_tem_documemtos_rg_cnh($id_cliente,$id_pretendente) {
        try {
            $where = "(tipo_documento=1 OR tipo_documento=3)";
            $this->db->where('id_cliente',$id_cliente);
            $this->db->where('id_pretendente',$id_pretendente);
            $this->db->where($where);
            return $this->db->get('tb_documentos_pretendentes')->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR INFORMACOES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function verifica_se_tem_documemtos_rg_cnh_conjugue($id_pretendente) {
        try {
            $where = "(tipo_documento=1 OR tipo_documento=3)";
            $this->db->where('id_pretendente',$id_pretendente);
            $this->db->where($where);
            return $this->db->get('tb_documentos_conjugues')->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR INFORMACOES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function verifica_se_tem_documemtos_rg_cnh_conjugue_fiador($id_fiador) {
        try {
            $where = "(tipo_documento=1 OR tipo_documento=3)";
            $this->db->where('id_fiador',$id_fiador);
            $this->db->where($where);
            return $this->db->get('tb_documentos_conjugues_fiadores')->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR INFORMACOES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function verifica_se_tem_documemtos_rg_cnh_fiador($id_cliente) {
        try {
            $where = "(tipo_documento=1 OR tipo_documento=3)";
            $this->db->where('id_cliente',$id_cliente);
            $this->db->where($where);
            return $this->db->get('tb_documentos_fiadores')->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR INFORMACOES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function verifica_se_tem_comrprovante_residencia($id_cliente,$id_pretendente) {
        try {
            $where = "(tipo_documento=4 OR tipo_documento=5 OR tipo_documento=6)";
            $this->db->where('id_cliente',$id_cliente);
            $this->db->where('id_pretendente',$id_pretendente);
            $this->db->where($where);
            return $this->db->get('tb_documentos_pretendentes')->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR INFORMACOES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function verifica_se_tem_comrprovante_residencia_fiador($id_cliente) {
        try {
            $where = "(tipo_documento=4 OR tipo_documento=5 OR tipo_documento=6)";
            $this->db->where('id_cliente',$id_cliente);
            $this->db->where($where);
            return $this->db->get('tb_documentos_fiadores')->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR INFORMACOES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function verifica_se_tem_estado_civil($id_cliente,$id_pretendente) {
        try {
            $where = "(tipo_documento=7 OR tipo_documento=8 OR tipo_documento=9 OR tipo_documento=10"
                    . " OR tipo_documento=11 OR tipo_documento=12)";
            $this->db->where('id_cliente',$id_cliente);
            $this->db->where('id_pretendente',$id_pretendente);
            $this->db->where($where);
            return $this->db->get('tb_documentos_pretendentes')->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR INFORMACOES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function verifica_se_tem_estado_civil_fiador($id_cliente) {
        try {
            $where = "(tipo_documento=7 OR tipo_documento=8 OR tipo_documento=9 OR tipo_documento=10"
                    . " OR tipo_documento=11 OR tipo_documento=12)";
            $this->db->where('id_cliente',$id_cliente);
            $this->db->where($where);
            return $this->db->get('tb_documentos_fiadores')->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR INFORMACOES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function verifica_se_tem_renda($id_cliente,$id_pretendente) {
        try {
            $where = "(tipo_documento=13 OR tipo_documento=17 OR tipo_documento=20)";
            $this->db->where('id_cliente',$id_cliente);
            $this->db->where('id_pretendente',$id_pretendente);
            $this->db->where($where);
            return $this->db->get('tb_documentos_pretendentes')->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR INFORMACOES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function verifica_se_tem_renda_fiador($id_cliente) {
        try {
            $where = "(tipo_documento=13 OR tipo_documento=17 OR tipo_documento=20)";
            $this->db->where('id_cliente',$id_cliente);
            $this->db->where($where);
            return $this->db->get('tb_documentos_fiadores')->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR INFORMACOES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function verifica_se_tem_renda_conjugue($id_pretendente) {
        try {
            $where = "(tipo_documento=13 OR tipo_documento=17 OR tipo_documento=20 OR tipo_documento=22)";
            $this->db->where('id_pretendente',$id_pretendente);
            $this->db->where($where);
            return $this->db->get('tb_documentos_conjugues')->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR INFORMACOES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function verifica_se_tem_renda_conjugue_fiador($id_fiador) {
        try {
            $where = "(tipo_documento=13 OR tipo_documento=17 OR tipo_documento=20 OR tipo_documento=22)";
            $this->db->where('id_fiador',$id_fiador);
            $this->db->where($where);
            return $this->db->get('tb_documentos_conjugues_fiadores')->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR INFORMACOES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function verifica_se_tem_outros_documentos($id_cliente,$id_pretendente) {
        try {
            $this->db->where('id_cliente',$id_cliente);
            $this->db->where('id_pretendente',$id_pretendente);
            $this->db->where('tipo_documento',21);
            return $this->db->get('tb_documentos_pretendentes')->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR INFORMACOES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function verifica_se_tem_documentos_residente($id_cliente) {
        try {
            $this->db->where('id_cliente',$id_cliente);
            return $this->db->get('tb_documentos_residentes')->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR INFORMACOES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function verifica_se_segundo_pretendente_tem_conjugue($id_cliente) {
        try {
            $this->db->select('id_estado_civil');
            $this->db->where('id_cliente',$id_cliente);
            $this->db->where('principal',0);
            return $this->db->get('tb_pretendentes')->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR INFORMACOES - ' . $e->getMessage());
            return array();
        }
        return array();
    
        
        }
    public function verifica_se_fiador_tem_conjugue($id_cliente) {
        try {
            $this->db->select('id_estado_civil');
            $this->db->where('id_cliente',$id_cliente);
            return $this->db->get('tb_fiadores')->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR INFORMACOES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function get_documentos($id_cliente,$id_pretendente) {
        try {
            $this->db->where('tb_documentos_pretendentes.id_cliente',$id_cliente);
            $this->db->where('tb_documentos_pretendentes.id_pretendente',$id_pretendente);
            $this->db->from('tb_documentos_pretendentes');
            $this->db->join('tb_tipo_documentos','tb_documentos_pretendentes.tipo_documento=tb_tipo_documentos.id_tipo_documento');
            return $this->db->get()->result();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR INFORMACOES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function get_documentos_fiadores($id_cliente) {
        try {
            $this->db->where('tb_documentos_fiadores.id_cliente',$id_cliente);
            $this->db->from('tb_documentos_fiadores');
            $this->db->join('tb_tipo_documentos','tb_documentos_fiadores.tipo_documento=tb_tipo_documentos.id_tipo_documento');
            return $this->db->get()->result();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR INFORMACOES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function get_residentes($id_cliente) {
        try {
            $this->db->where('id_cliente',$id_cliente);
            $this->db->from('tb_pessoas_residentes');
            return $this->db->get()->result();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR INFORMACOES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function get_documentos_residentes($id_cliente) {
        try {
            $this->db->where('id_cliente',$id_cliente);
            $this->db->from('tb_documentos_residentes');
            return $this->db->get()->result();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR INFORMACOES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function get_documentos_conjugue($id_pretendente) {
        try {
            $this->db->where('id_pretendente',$id_pretendente);
            $this->db->from('tb_documentos_conjugues');
            $this->db->join('tb_tipo_documentos','tb_documentos_conjugues.tipo_documento=tb_tipo_documentos.id_tipo_documento');
            return $this->db->get()->result();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR INFORMACOES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function get_documentos_conjugue_fiador($id_fiador) {
        try {
            $this->db->where('id_fiador',$id_fiador);
            $this->db->from('tb_documentos_conjugues_fiadores');
            $this->db->join('tb_tipo_documentos','tb_documentos_conjugues_fiadores.tipo_documento=tb_tipo_documentos.id_tipo_documento');
            return $this->db->get()->result();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR INFORMACOES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function listar_paises() {
        try {
            return $this->db->get('tb_paises')->result();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR PAISES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function verifica_se_tem_pretendente($id_cliente) {
        try {
            $this->db->where('id_cliente',$id_cliente);
            $this->db->from('tb_pretendentes');
            return $this->db->get()->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR PAISES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function verifica_se_tem_fiador($id_cliente) {
        try {
            $this->db->where('id_cliente',$id_cliente);
            $this->db->from('tb_fiadores');
            return $this->db->get()->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR PAISES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function verifica_se_tem_segundo_pretendente($id_cliente) {
        try {
            $this->db->select('segundo_pretendente,segundo_pretendente_conjugue');
            $this->db->where('id_cliente',$id_cliente);
            $this->db->where('segundo_pretendente',1);
            $this->db->where('segundo_pretendente_conjugue',0);
            $this->db->where('principal',1);
            $this->db->from('tb_pretendentes');
            return $this->db->get()->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTA PRETENDENTE - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function verifica_se_adicionou_fiador($id_cliente) {
        try {
            $this->db->select('id_fiador');
            $this->db->where('id_cliente',$id_cliente);
            $this->db->from('tb_fiadores');
            return $this->db->get()->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTA PRETENDENTE - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function verifica_se_adicionou_segundo_pretendente($id_cliente) {
        try {
            $this->db->select('enviou');
            $this->db->where('id_cliente',$id_cliente);
            $this->db->where('principal',0);
            $this->db->from('tb_pretendentes');
            return $this->db->get()->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR SEGUNDO PRETENDENTE - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function pega_dados_pretendente($id_cliente) {
        try {
            $this->db->select('tb_pretendentes.*,tb_estados_civis.*,tb_dados_empresa_cliente.*,tb_paises.*,'
                    . ' tb_pretendentes.id_cliente as cliente_id, tb_pretendentes.id_pretendente as pretendente_id,'
                    . 'tb_conjugue_pretendente.*');
            $this->db->where('tb_pretendentes.id_cliente',$id_cliente);
            $this->db->where('tb_pretendentes.principal',1);
            $this->db->from('tb_pretendentes');
            $this->db->join('tb_estados_civis','tb_pretendentes.id_estado_civil=tb_estados_civis.id_estado_civil');
            $this->db->join('tb_dados_empresa_cliente','tb_pretendentes.id_cliente=tb_dados_empresa_cliente.id_cliente '
                    . 'AND tb_pretendentes.id_pretendente=tb_dados_empresa_cliente.id_pretendente','LEFT');
            $this->db->join('tb_paises','tb_dados_empresa_cliente.id_pais=tb_paises.id_pais','LEFT');
            $this->db->join('tb_conjugue_pretendente','tb_pretendentes.id_cliente=tb_conjugue_pretendente.id_cliente '
                    . 'AND tb_pretendentes.id_pretendente=tb_conjugue_pretendente.id_pretendente','LEFT');
            return $this->db->get()->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR DADOS PRETENDENTE - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function pega_dados_segundo_pretendente($id_cliente) {
        try {
            $this->db->select('tb_pretendentes.*,tb_estados_civis.*,tb_dados_empresa_cliente.*,tb_paises.*,'
                    . ' tb_pretendentes.id_cliente as cliente_id, tb_pretendentes.id_pretendente as pretendente_id,'
                    . 'tb_conjugue_pretendente.*');
            $this->db->where('tb_pretendentes.id_cliente',$id_cliente);
            $this->db->where('tb_pretendentes.principal',0);
            $this->db->from('tb_pretendentes');
            $this->db->join('tb_estados_civis','tb_pretendentes.id_estado_civil=tb_estados_civis.id_estado_civil');
            $this->db->join('tb_dados_empresa_cliente','tb_pretendentes.id_cliente=tb_dados_empresa_cliente.id_cliente '
                    . 'AND tb_pretendentes.id_pretendente=tb_dados_empresa_cliente.id_pretendente','LEFT');
            $this->db->join('tb_paises','tb_dados_empresa_cliente.id_pais=tb_paises.id_pais','LEFT');
            $this->db->join('tb_conjugue_pretendente','tb_pretendentes.id_cliente=tb_conjugue_pretendente.id_cliente '
                    . 'AND tb_pretendentes.id_pretendente=tb_conjugue_pretendente.id_pretendente','LEFT');
            return $this->db->get()->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR DADOS PRETENDENTE - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function pega_dados_fiador($id_cliente) {
        try {
            $this->db->where('tb_fiadores.id_cliente',$id_cliente);
            $this->db->from('tb_fiadores');
            $this->db->join('tb_estados_civis','tb_fiadores.id_estado_civil=tb_estados_civis.id_estado_civil');
            $this->db->join('tb_dados_empresa_fiador','tb_fiadores.id_fiador=tb_dados_empresa_fiador.id_fiador','LEFT');
            $this->db->join('tb_paises as p','tb_dados_empresa_fiador.id_pais=p.id_pais','LEFT');
            $this->db->join('tb_conjugue_fiadores','tb_fiadores.id_fiador=tb_conjugue_fiadores.id_fiador','LEFT');
            $this->db->join('tb_enderecos_fiadores','tb_fiadores.id_fiador=tb_enderecos_fiadores.id_fiador','LEFT');
            $this->db->join('tb_paises as p2','tb_enderecos_fiadores.id_pais_fiador=p2.id_pais','LEFT');
            return $this->db->get()->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR DADOS PRETENDENTE - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function pega_propriedades_fiador($id_fiador) {
        try {
            $this->db->where('id_fiador',$id_fiador);
            $this->db->from('tb_propriedades_fiadores');
            return $this->db->get()->result();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR PROPRIEDADES FIADOR - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function pega_referencias_fiador($id_fiador) {
        try {
            $this->db->where('id_fiador',$id_fiador);
            $this->db->from('tb_referencias_fiadores');
            return $this->db->get()->result();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR REFERENCIAS FIADOR - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function pega_referencias_bancarias_fiador($id_fiador) {
        try {
            $this->db->where('id_fiador',$id_fiador);
            $this->db->from('tb_bancos_referencias_fiadores');
            return $this->db->get()->result();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR REFERENCIAS BANCARIAS FIADOR - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function enderecos_pretendentes($id_cliente,$id_pretendente) {
        try {
            $this->db->where('tb_endereco_pretendente.id_cliente',$id_cliente);
            $this->db->where('tb_endereco_pretendente.id_pretendente',$id_pretendente);
            $this->db->from('tb_endereco_pretendente');
            $this->db->join('tb_paises','tb_endereco_pretendente.id_pais_cliente=tb_paises.id_pais');
            return $this->db->get()->result();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR ENDEREÇOS PRETENDENTES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function enderecos_pretendente_principal($id_cliente,$id_pretendente) {
        try {
            $this->db->where('tb_endereco_pretendente.id_cliente',$id_cliente);
            $this->db->where('tb_endereco_pretendente.id_pretendente',$id_pretendente);
            $this->db->where('tb_endereco_pretendente.principal',1);
            $this->db->from('tb_endereco_pretendente');
            $this->db->join('tb_paises','tb_endereco_pretendente.id_pais_cliente=tb_paises.id_pais');
            return $this->db->get()->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR ENDEREÇOS PRETENDENTES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function enderecos_pretendente_alternativo($id_cliente,$id_pretendente) {
        try {
            $this->db->where('tb_endereco_pretendente.id_cliente',$id_cliente);
            $this->db->where('tb_endereco_pretendente.id_pretendente',$id_pretendente);
            $this->db->where('tb_endereco_pretendente.principal',0);
            $this->db->from('tb_endereco_pretendente');
            $this->db->join('tb_paises','tb_endereco_pretendente.id_pais_cliente=tb_paises.id_pais');
            return $this->db->get()->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR ENDEREÇOS PRETENDENTES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function pegar_residentes($id_cliente) {
        try {
            $this->db->where('id_cliente',$id_cliente);
            $this->db->from('tb_pessoas_residentes');
            return $this->db->get()->result();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR RESIDENTES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function pegar_propriedades($id_cliente) {
        try {
            $this->db->where('id_cliente',$id_cliente);
            $this->db->from('tb_propriedades_clientes');
            return $this->db->get()->result();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR PROPRIEDADES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function pegar_referencias($id_cliente) {
        try {
            $this->db->where('id_cliente',$id_cliente);
            $this->db->where('tipo_referencia',NULL);
            $this->db->from('tb_referencias_clientes');
            return $this->db->get()->result();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR REFERENCIAS - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function pegar_referencias_comerciais($id_cliente) {
        try {
            $this->db->where('id_cliente',$id_cliente);
            $this->db->where('tipo_referencia',2);
            $this->db->from('tb_referencias_clientes');
            return $this->db->get()->result();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR REFERENCIAS - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function pegar_referencias_bancarias($id_cliente) {
        try {
            $this->db->where('id_cliente',$id_cliente);
            $this->db->from('tb_bancos_referencias');
            return $this->db->get()->result();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR REFERENCIAS BANCARIAS- ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function pegar_pais_pretendentes($id_cliente) {
        try {
            $this->db->where('tb_pais_clientes.id_cliente',$id_cliente);
            $this->db->from('tb_pais_clientes');
            $this->db->join('tb_paises','tb_pais_clientes.id_pais=tb_paises.id_pais');
            return $this->db->get()->result();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR PAIS PRETENDENTE - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function pegar_pai_pretendentes($id_cliente) {
        try {
            $this->db->where('id_cliente',$id_cliente);
            $this->db->where('mae',0);
            return $this->db->get('tb_pais_clientes')->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR PAIS PRETENDENTE - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function pegar_mae_pretendentes($id_cliente) {
        try {
            $this->db->where('id_cliente',$id_cliente);
            $this->db->where('mae',1);
            return $this->db->get('tb_pais_clientes')->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR PAIS PRETENDENTE - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function get_id_pretendente($id_cliente,$principal) {
         try {
            $this->db->select('id_pretendente');
            $this->db->where('id_cliente',$id_cliente);
            $this->db->where('principal',$principal);
            $this->db->from('tb_pretendentes');
            return $this->db->get()->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - PEGA ID PRETENDENTE - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function verifica_se_tem_dados_empresa_cliente($id_cliente,$id_pretendente) {
         try {
            $this->db->select('id_dados_empresa_cliente');
            $this->db->where('id_cliente',$id_cliente);
            $this->db->where('id_pretendente',$id_pretendente);
            $this->db->from('tb_dados_empresa_cliente');
            return $this->db->get()->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - PEGA ID DADOS EMPRESA CLIENTE - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function verifica_se_tem_dados_juridico($id_cliente) {
        try {
            $this->db->where('id_cliente',$id_cliente);
            return $this->db->get('tb_dados_empresa_juridico')->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR INFORMACOES - ' . $e->getMessage());
            return array();
        }
    }
    
    public function get_cliente_jurudico($id_cliente) {
        try {
            $this->db->from('tb_dados_empresa_juridico');
            $this->db->join('tb_paises','tb_dados_empresa_juridico.id_pais=tb_paises.id_pais');
            $this->db->join('tb_socios_representantes','tb_dados_empresa_juridico.id_cliente=tb_socios_representantes.id_cliente','LEFT');
            $this->db->join('tb_estados_civis','tb_socios_representantes.id_estado_civil=tb_estados_civis.id_estado_civil','LEFT');
            $this->db->where('tb_dados_empresa_juridico.id_cliente',$id_cliente);
            return $this->db->get()->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR INFORMACOES - ' . $e->getMessage());
            return array();
        }
    }
    
    public function get_socios($id_cliente) {
        try {
            $this->db->from('tb_socios');
            $this->db->join('tb_paises','tb_socios.id_pais_diretor=tb_paises.id_pais','LEFT');
            $this->db->join('tb_estados_civis','tb_socios.id_estado_civil_diretor=tb_estados_civis.id_estado_civil','LEFT');
            $this->db->where('tb_socios.id_cliente',$id_cliente);
            return $this->db->get()->result();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR INFORMACOES - ' . $e->getMessage());
            return array();
        }
    }
    
    public function get_documentos_juridico($id_cliente) {
        try {
            $this->db->where('tb_documentos_empresa_juridico.id_cliente',$id_cliente);
            $this->db->from('tb_documentos_empresa_juridico');
            $this->db->join('tb_tipo_documentos','tb_documentos_empresa_juridico.tipo_documento=tb_tipo_documentos.id_tipo_documento');
            return $this->db->get()->result();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR INFORMACOES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function get_documentos_socios($id_cliente) {
        try {
            $this->db->where('tb_documentos_socios.id_cliente',$id_cliente);
            $this->db->from('tb_documentos_socios');
            $this->db->join('tb_tipo_documentos','tb_documentos_socios.tipo_documento=tb_tipo_documentos.id_tipo_documento');
            return $this->db->get()->result();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR INFORMACOES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
     
    public function verifica_se_tem_contrato_social($id_cliente) {
        try {
            $where = "(tipo_documento=16)";
            $this->db->where('id_cliente',$id_cliente);
            $this->db->where($where);
            return $this->db->get('tb_documentos_empresa_juridico')->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR INFORMACOES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function verifica_se_tem_alteracao_contrato_social($id_cliente) {
        try {
            $where = "(tipo_documento=23)";
            $this->db->where('id_cliente',$id_cliente);
            $this->db->where($where);
            return $this->db->get('tb_documentos_empresa_juridico')->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR INFORMACOES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function verifica_se_tem_balanco($id_cliente) {
        try {
            $where = "(tipo_documento=24)";
            $this->db->where('id_cliente',$id_cliente);
            $this->db->where($where);
            return $this->db->get('tb_documentos_empresa_juridico')->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR INFORMACOES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function verifica_se_tem_balancete($id_cliente) {
        try {
            $where = "(tipo_documento=25)";
            $this->db->where('id_cliente',$id_cliente);
            $this->db->where($where);
            return $this->db->get('tb_documentos_empresa_juridico')->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR INFORMACOES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function verifica_se_tem_cartao_cnpj($id_cliente) {
        try {
            $where = "(tipo_documento=26)";
            $this->db->where('id_cliente',$id_cliente);
            $this->db->where($where);
            return $this->db->get('tb_documentos_empresa_juridico')->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR INFORMACOES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function verifica_se_tem_imposto_renda($id_cliente) {
        try {
            $where = "(tipo_documento=15)";
            $this->db->where('id_cliente',$id_cliente);
            $this->db->where($where);
            return $this->db->get('tb_documentos_empresa_juridico')->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR INFORMACOES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function verifica_se_tem_certidao_negativa($id_cliente) {
        try {
            $where = "(tipo_documento=27)";
            $this->db->where('id_cliente',$id_cliente);
            $this->db->where($where);
            return $this->db->get('tb_documentos_empresa_juridico')->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR INFORMACOES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function verifica_se_tem_outros_documentos_juridico($id_cliente) {
        try {
            $where = "(tipo_documento=21)";
            $this->db->where('id_cliente',$id_cliente);
            $this->db->where($where);
            return $this->db->get('tb_documentos_empresa_juridico')->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR INFORMACOES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function verifica_se_tem_rg_cnh($id_cliente) {
        try {
            $where = "(tipo_documento=1 OR tipo_documento=3)";
            $this->db->where('id_cliente',$id_cliente);
            $this->db->where($where);
            return $this->db->get('tb_documentos_socios')->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR INFORMACOES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function verifica_se_tem_comprovante_residencia($id_cliente) {
        try {
            $where = "(tipo_documento=4 OR tipo_documento=5 OR tipo_documento=6)";
            $this->db->where('id_cliente',$id_cliente);
            $this->db->where($where);
            return $this->db->get('tb_documentos_socios')->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR INFORMACOES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    public function verifica_email_cadastro($email) {
        try {
            $this->db->select('email');
            $this->db->where('email',$email);
            return $this->db->get('tb_clientes')->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR INFORMACOES - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    /*********************INSERÇÕES NA TABELA CLIENTE**************************/
    
    public function adicionar_cliente($dados) {
        try {
            $this->db->insert('tb_clientes',$dados);
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ADICIONAR USUARIO - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
        
    }
    
    public function adicionar_dados_cliente($dados) {
        try {
            $this->db->insert('tb_dados_clientes',$dados);
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ADICIONAR DADOS CLIENTE  - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function inserir_documentos($dados) {
        try {
            $this->db->insert('tb_documentos_pretendentes',$dados);
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ADICIONAR DOCUMENTOS CLIENTE  - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function inserir_documentos_conjugues($dados) {
        try {
            $this->db->insert('tb_documentos_conjugues',$dados);
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ADICIONAR DOCUMENTOS CLIENTE  - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function inserir_documentos_conjugues_fiador($dados) {
        try {
            $this->db->insert('tb_documentos_conjugues_fiadores',$dados);
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ADICIONAR DOCUMENTOS FIADOR  - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function inserir_documentos_fiador($dados) {
        try {
            $this->db->insert('tb_documentos_fiadores',$dados);
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ADICIONAR DOCUMENTOS FIADOR  - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function inserir_documentos_residentes($dados) {
        try {
            $this->db->insert('tb_documentos_residentes',$dados);
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ADICIONAR DOCUMENTOS CLIENTE  - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function inserir_pretendente($dados) {
        try {
            $this->db->insert('tb_pretendentes',$dados);
            return $this->db->insert_id();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ADICIONAR PRETENDENTE CLIENTE  - ' . $e->getMessage());
            return FALSE;
        }
    }
    
    public function inserir_dados_empresa_cliente($dados) {
        try {
            $this->db->insert('tb_dados_empresa_cliente',$dados);
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ADICIONAR DADOS EMPRESA CLIENTE  - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function atualiza_dados_empresa_cliente($id_dados_empresa,$dados) {
        try {
            $this->db->where('id_dados_empresa_cliente',$id_dados_empresa);
            $this->db->update('tb_dados_empresa_cliente',$dados);
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ADICIONAR DADOS EMPRESA CLIENTE  - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function inserir_dados_endereco($dados) {
        try {
            $this->db->insert('tb_endereco_pretendente',$dados);
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ADICIONAR DADOS ENDEREÇO CLIENTE  - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function atualizar_dados_endereco($id_pretendente,$principal,$dados) {
        try {
            $this->db->where('id_pretendente',$id_pretendente);
            $this->db->where('principal',$principal);
            $this->db->update('tb_endereco_pretendente',$dados);
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ATUALIZAR DADOS ENDEREÇO CLIENTE  - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function verifica_se_tem_endereco_alternativo($id_pretendente) {
        try {
            $this->db->where('id_pretendente',$id_pretendente);
            $this->db->where('principal',0);
            return $this->db->get('tb_endereco_pretendente')->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCA DADOS ENDEREÇO CLIENTE ALTERNATIVO  - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function inserir_dados_residentes($dados) {
        try {
            $this->db->insert('tb_pessoas_residentes',$dados);
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ADICIONAR DADOS RESIDENTES CLIENTE  - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function excluir_dados_residentes($id_cliente) {
        try {
            $this->db->where('id_cliente',$id_cliente);
            $this->db->delete('tb_pessoas_residentes');
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - EXCLUIR DADOS RESIDENTES CLIENTE  - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function inserir_dados_propriedades($dados) {
        try {
            $this->db->insert('tb_propriedades_clientes',$dados);
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ADICIONAR DADOS PROPRIEDADES CLIENTE  - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function excluir_dados_propriedades($id_cliente) {
        try {
            $this->db->where('id_cliente',$id_cliente);
            $this->db->delete('tb_propriedades_clientes');
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD -  EXCLUIR DADOS PROPRIEDADES CLIENTE  - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function inserir_dados_referencias($dados) {
        try {
            $this->db->insert('tb_referencias_clientes',$dados);
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ADICIONAR DADOS REFERENCIAS CLIENTE  - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function excluir_dados_referencias($id_cliente) {
        try {
            $this->db->where('id_cliente',$id_cliente);
            $this->db->where('tipo_referencia',NULL);
            $this->db->delete('tb_referencias_clientes');
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - EXCLUIR DADOS REFERENCIAS CLIENTE  - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function excluir_dados_referencias_comerciais($id_cliente) {
        try {
            $this->db->where('id_cliente',$id_cliente);
            $this->db->where('tipo_referencia',2);
            $this->db->delete('tb_referencias_clientes');
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - EXCLUIR DADOS REFERENCIAS CLIENTE  - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function inserir_dados_banco($dados) {
        try {
            $this->db->insert('tb_bancos_referencias',$dados);
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ADICIONAR DADOS BANCO CLIENTE  - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function excluir_dados_banco($id_cliente) {
        try {
            $this->db->where('id_cliente',$id_cliente);
            $this->db->delete('tb_bancos_referencias');
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - EXCLUIR DADOS BANCO CLIENTE  - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function inserir_dados_pais($dados) {
        try {
            $this->db->insert('tb_pais_clientes',$dados);
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ADICIONAR DADOS PAIS CLIENTE  - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function excluir_dados_pais($id_cliente,$tipo) {
        try {
            $this->db->where('id_cliente',$id_cliente);
            $this->db->where('mae',$tipo);
            $this->db->delete('tb_pais_clientes');
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - EXCLUIR DADOS PAIS CLIENTE  - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function inserir_dados_conjugue_pretendente($dados) {
        try {
            $this->db->insert('tb_conjugue_pretendente',$dados);
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ADICIONAR DADOS CONJUGUE PRETENDENTE  - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function excluir_dados_conjugue_pretendente($id_pretendente) {
        try {
            $this->db->where('id_pretendente',$id_pretendente);
            $this->db->delete('tb_conjugue_pretendente');
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - EXCLUIR DADOS CONJUGUE PRETENDENTE  - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function inserir_fiador($dados) {
        try {
            $this->db->insert('tb_fiadores',$dados);
            return $this->db->insert_id();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ADICIONAR DADOS FIADOR - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function atualiza_fiador($id_cliente,$dados) {
        try {
            $this->db->where('id_cliente',$id_cliente);
            $this->db->update('tb_fiadores',$dados);
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ATUALIZA DADOS FIADOR - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function inserir_dados_empresa_fiador($dados) {
        try {
            $this->db->insert('tb_dados_empresa_fiador',$dados);
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ADICIONAR DADOS EMPRESA FIADOR - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function atualiza_dados_empresa_fiador($id_fiador,$dados) {
        try {
            $this->db->where('id_fiador',$id_fiador);
            $this->db->update('tb_dados_empresa_fiador',$dados);
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ATUALIZA DADOS EMPRESA FIADOR - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function verifica_se_tem_dados_empresa_fiador($id_fiador) {
        try {
            $this->db->select('id_dados_empresa_fiador');
            $this->db->where('id_fiador',$id_fiador);
            return $this->db->get('tb_dados_empresa_fiador')->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ADICIONAR DADOS EMPRESA FIADOR - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function inserir_endereco_fiador($dados) {
        try {
            $this->db->insert('tb_enderecos_fiadores',$dados);
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ADICIONAR ENDERECO FIADOR - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function excluir_endereco_fiador($id_fiador) {
        try {
            $this->db->where('id_fiador',$id_fiador);
            $this->db->delete('tb_enderecos_fiadores');
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - EXCLUIR ENDERECO FIADOR- ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function inserir_dados_propriedades_fiador($dados) {
        try {
            $this->db->insert('tb_propriedades_fiadores',$dados);
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ADICIONAR DADOS PROPRIEDADES DO FIADOR - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function excluir_dados_propriedades_fiador($id_fiador) {
        try {
            $this->db->where('id_fiador',$id_fiador);
            $this->db->delete('tb_propriedades_fiadores');
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - EXCLUIR DADOS PROPRIEDADES DO FIADOR - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function inserir_dados_referencias_fiador($dados) {
        try {
            $this->db->insert('tb_referencias_fiadores',$dados);
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ADICIONAR DADOS REFERENCIAS DO FIADOR - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function excluir_dados_referencias_fiador($id_fiador) {
        try {
            $this->db->where('id_fiador',$id_fiador);
            $this->db->delete('tb_referencias_fiadores');
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - EXCLUIR DADOS REFERENCIAS DO FIADOR - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function inserir_dados_banco_fiador($dados) {
        try {
            $this->db->insert('tb_bancos_referencias_fiadores',$dados);
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ADICIONAR DADOS BANCOS DO FIADOR - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function excluir_dados_banco_fiador($id_fiador) {
        try {
            $this->db->where('id_fiador',$id_fiador);
            $this->db->delete('tb_bancos_referencias_fiadores');
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - EXCLUIR DADOS BANCOS DO FIADOR - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function inserir_dados_conjugue_fiador($dados) {
        try {
            $this->db->insert('tb_conjugue_fiadores',$dados);
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ADICIONAR DADOS CONJUGUE DO FIADOR - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function excluir_dados_conjugue_fiador($id_fiador) {
        try {
            $this->db->where('id_fiador',$id_fiador);
            $this->db->delete('tb_conjugue_fiadores');
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - EXCLUIR DADOS CONJUGUE DO FIADOR - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function inserir_dados_socio_representante($dados) {
        try {
            $this->db->insert('tb_socios_representantes',$dados);
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ADICIONAR DADOS SOCIO REPRESENTANDO - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function atualizar_dados_socio_representante($id_cliente,$dados) {
        try {
            $this->db->where('id_cliente',$id_cliente);
            $this->db->update('tb_socios_representantes',$dados);
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ADICIONAR DADOS SOCIO REPRESENTANDO - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function verifica_se_tem_dados_socio_representante($id_cliente) {
        try {
            $this->db->where('id_cliente',$id_cliente);
            return $this->db->get('tb_socios_representantes')->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ADICIONAR DADOS SOCIO REPRESENTANDO - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function inserir_dados_empresa_juridico($dados) {
        try {
            $this->db->insert('tb_dados_empresa_juridico',$dados);
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ADICIONAR DADOS EMPRESA JURIDICO - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function alterar_dados_empresa_juridico($id_cliente,$dados) {
        try {
            $this->db->where('id_cliente',$id_cliente);
            $this->db->update('tb_dados_empresa_juridico',$dados);
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ALTERAR DADOS EMPRESA JURIDICO - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function inserir_dados_socios($dados) {
        try {
            $this->db->insert('tb_socios',$dados);
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ADICIONAR DADOS SOCIOS - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function excluir_dados_socios($id_cliente) {
        try {
            $this->db->where('id_cliente',$id_cliente);
            $this->db->delete('tb_socios');
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - EXCLUIR DADOS SOCIOS - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function inserir_dados_propriedades_juridico($dados) {
        try {
            $this->db->insert('tb_propriedades_juridico',$dados);
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ADICIONAR DADOS PROPRIEDADES JURIDICO  - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function inserir_documentos_juridico($dados) {
        try {
            $this->db->insert('tb_documentos_empresa_juridico',$dados);
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ADICIONAR DOCUMENTOS JURIDICO  - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function inserir_documentos_socios($dados) {
        try {
            $this->db->insert('tb_documentos_socios',$dados);
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ADICIONAR DOCUMENTOS SOCIOS  - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    /*******************ATUALIZAÇÕES NA TABELA CLIENTE*************************/
    
    public function editar_cliente($dados,$id_cliente) {
        try {
            $this->db->where('id_cliente',$id_cliente);
            $this->db->update('tb_clientes',$dados);
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - EDITAR CLIENTE  - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function editar_dados_cliente($dados,$id_cliente) {
        try {
            $this->db->where('id_cliente',$id_cliente);
            $this->db->update('tb_dados_clientes',$dados);
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - EDITAR CLIENTE  - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function alterar_senha($email,$senha) {
        try {
            $dados['senha']=$senha;
            $this->db->where('email',$email);
            $this->db->update('tb_clientes',$dados);
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - EDITAR CLIENTE  - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function atualiza_pretendente($id_pretendente,$dados) {
        try {
            $this->db->where('id_pretendente',$id_pretendente);
            $this->db->update('tb_pretendentes',$dados);
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - EDITAR CLIENTE  - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    /*************EXCLUSÕES NA TABELA CLIENTE E TABELAS LIGADAS****************/
        
    public function excluir_dados_cliente($id_cliente) {
        try {
            $this->db->where('id_cliente',$id_cliente);
            $this->db->delete('tb_dados_clientes');
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - EDITAR CLIENTE  - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function excluir_documetacao($id_pretendente,$tipo) {
        try {
            $this->db->where('id_pretendente',$id_pretendente);
            $this->db->where('tipo_documento',$tipo);
            $this->db->delete('tb_documentos_pretendentes');
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - EXCLUIR DOCUMENTO CLIENTE  - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function excluir_documetacao_residentes($id_cliente) {
        try {
            $this->db->where('id_cliente',$id_cliente);
            $this->db->delete('tb_documentos_residentes');
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - EXCLUIR DOCUMENTO CLIENTE  - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function excluir_documetacao_conjugue($id_pretendente,$tipo) {
        try {
            $this->db->where('id_pretendente',$id_pretendente);
            $this->db->where('tipo_documento',$tipo);
            $this->db->delete('tb_documentos_conjugues');
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - EXCLUIR DOCUMENTO CLIENTE  - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function excluir_documetacao_fiador($id_cliente,$tipo) {
        try {
            $this->db->where('id_cliente',$id_cliente);
            $this->db->where('tipo_documento',$tipo);
            $this->db->delete('tb_documentos_fiadores');
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - EXCLUIR DOCUMENTO CLIENTE  - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function excluir_documetacao_conjugue_fiador($id_fiador,$tipo) {
        try {
            $this->db->where('id_fiador',$id_fiador);
            $this->db->where('tipo_documento',$tipo);
            $this->db->delete('tb_documentos_conjugues_fiadores');
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - EXCLUIR DOCUMENTO CLIENTE  - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function excluir_documetacao_juridico($id_cliente,$tipo) {
        try {
            $this->db->where('id_cliente',$id_cliente);
            $this->db->where('tipo_documento',$tipo);
            $this->db->delete('tb_documentos_empresa_juridico');
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - EXCLUIR DOCUMENTO CLIENTE  - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
    
    public function excluir_documetacao_juridico_socios($id_cliente,$tipo) {
        try {
            $this->db->where('id_cliente',$id_cliente);
            $this->db->where('tipo_documento',$tipo);
            $this->db->delete('tb_documentos_socios');
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - EXCLUIR DOCUMENTO CLIENTE  - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }
}
