<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * IMOVEL_MODEL
 * -------------------------------------------------------------------
 * DESCRICAO : 
 * -------------------------------------------------------------------
 * CARREGA : 
 * - N/A
 */
class ImovelModel extends CI_Model {

    /* ****************************************************** */
    /* ********************** LISTAGEM ********************** */
    /* ****************************************************** */
    /*
     * LISTAR_IMOVEIS
     * ---------------------------------------
     * DESCRICAO : Retorna a lista de imoveis
     * com as informacoes gerais. Pode-se li-
     * mitar a lista especificando o inicio
     * {inicio} e a quantidade {qtd}.
     * ---------------------------------------
     * PARAMETROS : $QTD [INT]
     *              $INICIO [INT]
     * ---------------------------------------
     * RETORNO : [ARRAY] - Lista de imoveis
    */
    public function listar_imoveis($qtd=0, $inicio=0) {
        try {
            $this->db->from('tb_imoveis AS imo');
            $this->db->join('tb_imoveis_sub_tipos AS ist','i.id_imovel=ist.id_imovel');
            $this->db->join('tb_tipo_imovel AS ti','ist.id_tipo=ti.id_tipo_imovel');
            $this->db->join('tb_sub_tipo_imovel AS sti','ist.id_sub_tipo=sti.id_sub_tipo_imovel');
            $this->db->join('tb_informacoes_complementares AS infc','imo.id_informacoes_complementares=infc.id_informacao', 'left');
            $this->db->join('tb_infraestrutura_interna AS infi','imo.id_infraestrutura_interna=infi.id_infraestrutura_interna', 'left');
            $this->db->join('tb_infraestrutura_externa AS infe','imo.id_infraestrutura_externa=infe.    id_infraestrutura_externa', 'left');
            $this->db->join('tb_especificacoes AS esp','imo.id_especificacao=esp.id_especificacao', 'left');
            if($qtd > 0) {
                $this->db->limit($qtd, $inicio);
            }
            return $this->db->get()->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR IMOVEIS - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /*
     * LISTAR_RESUMO_IMOVEIS
     * ---------------------------------------
     * DESCRICAO : Retorna a lista de imoveis
     * com as informacoes resumidas. Pode-se li-
     * mitar a lista especificando o inicio
     * {inicio} e a quantidade {qtd}.
     * ---------------------------------------
     * PARAMETROS : $QTD [INT]
     *              $INICIO [INT]
     * ---------------------------------------
     * RETORNO : [ARRAY] - Lista de imoveis
    */
    public function listar_resumo_imoveis($qtd=0, $inicio=0) {
        try {
            $this->db->select(array('i.id_imovel', 
                                    'i.id_baggio', 
                                    'sti.nome_tipo AS tipo_imovel', 
                                    'i.referencia',
                                    'i.titulo', 
                                    'i.descricao')
            );
            $this->db->from('tb_imoveis AS i');
            $this->db->join('tb_imoveis_sub_tipos AS ist','i.id_imovel=ist.id_imovel');
            $this->db->join('tb_sub_tipo_imovel AS sti','ist.id_sub_tipo=sti.id_sub_tipo_imovel');

            if($qtd > 0) {
                $this->db->limit($qtd, $inicio);
            }
            return $this->db->get()->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR RESUMO IMOVEIS - ' . $e->getMessage());
            return array();
        }
        return array();
    }
    
    /*
     * LISTAR_LOJAS
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function listar_lojas() {
        try {
            return $this->db->get('tb_lojas')->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR LOJAS - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /*
     * LISTAR_REFERENCIAS_COM_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function listar_referencias_com_imovel() {
        try {
            $this->db->where('referencia !=', NULL);
            $this->db->group_by('referencia');
            return $this->db->get('tb_imoveis')->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR REFERENCIAS - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /*
     * LISTAR_CONDOMINIOS_COM_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function listar_condominios_com_imovel() {
        try {
            $this->db->where('nome_condominio !=', NULL);
            $this->db->group_by('nome_condominio');
            return $this->db->get('tb_endereco_complemento')->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR CONDOMINIOS - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /*
     * LISTAR_SUBTIPOS_IMOVEL_CATEGORIZADO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function listar_subtipos_imovel_categorizado() {
        try {
            $this->db->select(array('ti.id_tipo_imovel', 
                                    'sti.id_sub_tipo_imovel', 
                                    'ti.nome_tipo AS tipo_imovel', 
                                    'sti.nome_tipo AS subtipo_imovel')
            );
            $this->db->from('tb_sub_tipo_imovel AS sti');
            $this->db->join('tb_tipo_imovel AS ti','ti.id_tipo_imovel=sti.id_tipo_imovel');

            $this->db->order_by('sti.nome_tipo', 'ASC');

            return $this->db->get()->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR SUBTIPOS IMOVEL CATEGORIZADO - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /* ****************************************************** */
    /* *************** CONTAGEM E VERIFICACAO *************** */
    /* ****************************************************** */
    /*
     * QUANTIFICAR_IMOVEIS
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function quantificar_imoveis() {
        try {
            return $this->db->count_all('tb_imoveis');
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - QUANTIFICAR IMOVEIS - ' . $e->getMessage());
            return 0;
        }
        return 0;
    }

    /*
     * QUANTIFICAR_IMOVEIS_BUSCA
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function quantificar_imoveis_busca(
        $id_tipo_negociacao=1, 
        $ids_tipos_imoveis=array(),
        $cidade=1, $bairros=array(), 
        $referencia=NULL, $condominio=NULL, $tipos_condominios=array(), 
        $filtro_menor_preco=0, $filtro_maior_preco=0,
        $filtro_qtd_quartos=0, $filtro_qtd_banheiros=0, $filtro_qtd_vagas=0, 
        $filtro_menor_area=0, $filtro_maior_area=0) 
    {
        return $this->_quantificar_imoveis_busca(
            $id_tipo_negociacao, $ids_tipos_imoveis,
            $cidade, $bairros, 
            $referencia, $condominio, $tipos_condominios, 
            $filtro_menor_preco, $filtro_maior_preco,
            $filtro_qtd_quartos, $filtro_qtd_banheiros, $filtro_qtd_vagas, 
            $filtro_menor_area, $filtro_maior_area);
    }

    /*
     * EXISTE_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function existe_imovel($id_imovel=0, $referencia="", $id_imobiliaria=0) {
        try {
            $this->db->from('tb_imoveis AS i');
            $this->db->where('i.id_imovel', $id_imovel);
            $this->db->where('i.id_imobiliaria', $id_imobiliaria);
            $this->db->where('i.referencia', $referencia);
            return $this->db->count_all_results() > 0;
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - EXISTE IMOVEL - ' . $e->getMessage());
            return FALSE;
        }
        return FALSE;
    }

    /*
     * ATUALIZAVEL_EXISTE_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function atualizavel_existe_imovel($id_imovel=0, $referencia="", $id_imobiliaria=0) {
        try {
            $this->db->from('tb_imoveis AS i');
            $this->db->where('i.id_imovel', $id_imovel);
            $this->db->where('i.id_imobiliaria', $id_imobiliaria);
            $this->db->where('i.referencia', $referencia);
            $this->db->where('i.xml_atualizavel', TRUE);
            return $this->db->count_all_results() > 0;
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ATUALIZAVEL EXISTE IMOVEL - ' . $e->getMessage());
            return FALSE;
        }
        return FALSE;
    }

    /* ****************************************************** */
    /* *********************** BUSCAS *********************** */
    /* ****************************************************** */
    /*
     * BUSCAR_ID_LOJA
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function buscar_id_loja($nome_loja="") {
        $id_loja = 1;
        try {
            if (!empty($nome_loja)) {
                $this->db->select('id_loja');
                $this->db->from('tb_lojas');
                $this->db->where('LOWER(nome_loja)', strtolower($nome_loja));

                $result_array = $this->db->get()->result_array();
                
                $id_loja = intval( $result_array[0]["id_loja"] );

                return $id_loja;
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR ID LOJA - ' . $e->getMessage());

            return $id_loja;
        }
        return $id_loja;
    }

    /*
     * BUSCAR_ID_TIPO_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - 
    */
    public function buscar_id_tipo_imovel($id_sub_tipo) {
        $id_tipo = 0;
        try {
            $this->db->select( array('sti.id_tipo_imovel') );
            $this->db->from('tb_sub_tipo_imovel AS sti');
            $this->db->where('sti.id_sub_tipo_imovel', $id_sub_tipo);

            $row_array = $this->db->get()->row_array();
                
            $id_tipo = intval( $row_array["id_tipo_imovel"] );

            return $id_tipo;
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR ID TIPO IMOVEL ' . $e->getMessage());

            return $id_tipo;
        }
        return $id_tipo;
    }

    /*
     * PESQUISAR_SUB_TIPO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - 
    */
    public function pesquisar_sub_tipo($id_sub_tipo) {
        try {
            $this->db->select('nome_tipo');
            $this->db->from('tb_sub_tipo_imovel');
            $this->db->where('id_sub_tipo_imovel', $id_sub_tipo);

            $row_array = $this->db->get()->row_array();

            return $row_array['nome_tipo'];
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR ID TIPO IMOVEL ' . $e->getMessage());

            return $row_array['nome_tipo'];
        }
        return $row_array['nome_tipo'];
    }

    /*
     * BUSCAR_INFORMACOES_COMPLEMENTARES
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : 
     * ---------------------------------------
     * RETORNO : [ARRAY]
    */
    public function buscar_informacoes_complementares($id_imovel=0) {
        try {
            $this->db->select( array(
                'info.id_informacao','a.nome_tipo AS acao','o.nome_tipo AS ocupacao',
                'f.nome_tipo AS finalidade','fa.nome_tipo AS face_apartamento',
                'ad.nome_tipo AS andar','info.idade_imovel',
                'info.condominio_fechado','info.cobertura','info.informacoes_adicionais',
                'info.lancamento','info.uso_tipo','info.reservado',
                'info.andar_apartamento','info.situacao','info.vago_em',
                'info.acab_benfeito')
            );
            $this->db->from('tb_imoveis AS imo');
            $this->db->join('tb_informacoes_complementares AS info','imo.id_informacoes_complementares=info.id_informacao', 'left');
            $this->db->join('tb_tipo_acao AS a','info.id_tipo_acao=a.id_tipo_acao', 'left');
            $this->db->join('tb_tipo_ocupacao AS o','info.id_tipo_ocupacao=o.id_tipo_ocupacao', 'left');
            $this->db->join('tb_tipo_finalidade AS f','info.id_tipo_finalidade=f.id_tipo_finalidade', 'left');
            $this->db->join('tb_tipo_face AS fa','info.id_face_apartamento=fa.id_tipo_face', 'left');
            $this->db->join('tb_tipo_andar AS ad','info.id_tipo_andar=ad.id_andar', 'left');
            $this->db->where('imo.id_imovel', $id_imovel);
            return $this->db->get()->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR INFORMACOES COMPLEMENTARES - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /*
     * BUSCAR_INFRAESTRUTURA
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : 
     * ---------------------------------------
     * RETORNO : [ARRAY]
    */
    public function buscar_infraestrutura($id_imovel=0) {
        try {
            $this->db->select( array(
                'infra.id_infraestrutura', 'fa.nome_tipo AS face_terreno',
                'z.nome_tipo AS zoneamento', 'm.nome_tipo AS tipo_material',
                'infra.area_terreno', 'infra.medidas_terreno',
                'infra.pavimentacao', 'infra.pe_direito',
                'infra.lote', 'infra.quadra', 'infra.planta',
                'infra.indicacao_fiscal', 'infra.topografia',
                'infra.registro_imovel', 'infra.circunscricao',
                'infra.tem_agua', 'infra.tem_esgoto', 'infra.tem_luz',
                'infra.tem_telefone', 'infra.tem_placa_local',
                'infra.tem_forro','infra.tem_telhado')
            );
            $this->db->from('tb_imoveis AS i');
            $this->db->join('tb_infraestrutura AS infra','i.id_infraestrutura=infra.id_infraestrutura', 'left');
            $this->db->join('tb_tipo_face AS fa','infra.id_face_terreno=fa.id_tipo_face', 'left');
            $this->db->join('tb_tipo_zoneamento AS z','infra.id_tipo_zoneamento=z.id_tipo_zoneamento', 'left');
            $this->db->join('tb_tipo_material AS m','infra.id_tipo_material=m.id_material', 'left');
            $this->db->where('i.id_imovel', $id_imovel);
            return $this->db->get()->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR INFRAESTRUTURA - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /*
     * BUSCAR_ESPECIFICACOES
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : 
     * ---------------------------------------
     * RETORNO : [ARRAY]
    */
    public function buscar_especificacoes($id_imovel=0) {
        try {
            $this->db->select( array(
                'e.id_especificacao', 's.nome_tipo AS situacao_documental', 
                'f.nome_tipo AS financiamento', 
                'g.nome_tipo AS garagem', 'lg.nome_tipo AS local_garagem',
                'fa.nome_tipo AS face_imovel', 'e.qtd_vagas_estacionamento',
                'e.qtd_vagas_garagem', 'e.qtd_closets', 'e.qtd_quartos',
                'e.qtd_suites', 'e.qtd_demi_suites', 'e.qtd_churrasqueiras',
                'e.qtd_copas', 'e.qtd_despensas', 'e.qtd_cozinhas',
                'e.qtd_lareiras', 'e.qtd_sacadas', 'e.qtd_salas',
                'e.qtd_salas_jantar', 'e.qtd_salas_estar', 'e.qtd_salas_estar_intima',
                'e.qtd_salas_escritorio', 'e.qtd_salas_biblioteca', 
                'e.qtd_lavabos', 'e.qtd_banheiros', 'e.qtd_areas_servico', 
                'e.qtd_dependencias_empregada', 'e.qtd_bwc_empregada',
                'e.tem_adega', 'e.tem_alarme', 'e.tem_aquecimento',
                'e.tem_atico', 'e.tem_aquecimento_solar', 'e.tem_aquecimento_eletrico',
                'e.tem_bicicletario', 'e.tem_brinquedoteca', 'e.tem_cancha_esportes',
                'e.tem_central_gas', 'e.tem_canil', 'e.tem_cerca_eletrica',
                'e.tem_churrasqueira_privada', 'e.tem_churrasqueira_coletiva',
                'e.tem_cozinha_armarios', 'e.tem_cinema',
                'e.tem_sistema_ar', 'e.tem_sistema_incendio', 
                'e.tem_sistema_cftv', 'e.tem_sistema_seguranca', 
                'e.tem_deposito', 'e.tem_despensa', 'e.tem_dormitorio_armarios',
                'e.tem_fitness', 'e.tem_garden', 'e.tem_grades', 'e.tem_hidro',
                'e.tem_interfone', 'e.tem_jardim_inverno', 'e.tem_piscina',
                'e.tem_piscina_infantil', 'e.esta_mobiliado', 'e.tem_pista_caminhada',
                'e.tem_playground', 'e.tem_portao_eletronico', 'e.tem_portaria_vinte_quatro',
                'e.tem_quiosque', 'e.tem_salao_festas', 'e.tem_salao_jogos', 'e.tem_sauna',
                'e.tem_spa', 'e.tem_espaco_gourmet', 'e.tem_edicula', 'e.tem_quintal',
                'e.tem_jardim', 'e.conservacao', 'e.area_privada', 
                'e.area_total', 'e.area_averbada', 'e.area_privada_terreno',
                'e.esquadrias', 'e.revestimento_externo', 'e.construtora',
                'e.horarios_visita', 'e.qtd_elevadores', 'e.qtd_apartamentos_andar',
                'e.numero_pavimentos', 'e.tipo_piso', 'e.condicoes_negocio',
                'e.status_imovel', 'e.servicos_opcionais')
            );
            $this->db->from('tb_imoveis AS i');
            $this->db->join('tb_especificacoes AS e','i.id_especificacoes=e.id_especificacao', 'left');
            $this->db->join('tb_tipo_situacao_documental AS s','e.id_situacao_documental=s.id_situacao_documental', 'left');
            $this->db->join('tb_tipo_financiamento AS f','e.id_tipo_financiamento=f.id_financiamento', 'left');
            $this->db->join('tb_tipo_garagem AS g','e.id_tipo_garagem=g.id_garagem', 'left');
            $this->db->join('tb_tipo_local_garagem AS lg','e.id_local_garagem=lg.id_local_garagem', 'left');
            $this->db->join('tb_tipo_face AS fa','e.id_face_imovel=fa.id_tipo_face', 'left');
            $this->db->where('i.id_imovel', $id_imovel);
            return $this->db->get()->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR ESPECIFICACOES - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /*
     * BUSCAR_IMAGENS
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : 
     * ---------------------------------------
     * RETORNO : [ARRAY]
    */
    public function buscar_imagens($id_imovel=0) {
        try {
            // $this->db->select(array(
            //     'ch.id_chave','ch.id_cliente AS id_chave_cliente',
            //     'ch.id_loja AS id_chave_loja',
            //     'ch.id_usuario AS id_chave_usuario',
            //     'ch.descricao AS descricao_chave'
            // ));
            // $this->db->from('tb_chaves AS ch');

            // $this->db->where('i.id_imovel', $id_imovel);

            // return $this->db->get()->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR IMAGENS - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /*
     * BUSCAR_CHAVES
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : 
     * ---------------------------------------
     * RETORNO : [ARRAY]
    */
    public function buscar_chaves($id_imovel=0) {
        try {
            $this->db->select(array(
                'ch.id_chave','ch.id_cliente AS id_chave_cliente',
                'ch.id_loja AS id_chave_loja',
                'ch.id_usuario AS id_chave_usuario',
                'ch.descricao AS descricao_chave'
            ));
            $this->db->from('tb_chaves AS ch');
            $this->db->where('ch.id_imovel', $id_imovel);

            return $this->db->get()->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR CHAVES - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /*
     * BUSCAR_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : 
     * ---------------------------------------
     * RETORNO : [ARRAY]
    */
    public function buscar_imovel($id_imovel=0) {
        try {
            $this->db->select(array(
                'i.id_imovel','i.id_baggio','i.id_imobiliaria',
                'i.id_especificacoes','i.id_informacoes_complementares',
                'i.id_infraestrutura','i.id_veiculacao',
                'i.id_informacoes_xml','i.referencia',
                'i.titulo','i.descricao','i.observacao',
                'i.alugado','i.vendido',
                'i.xml_atualizavel',
                // ENDERECO
                'e.id_endereco','e.id_cidade','e.id_bairro',
                'e.id_complemento','e.cep','e.logradouro','e.numero',
                // IMOBILIARIA
                'imo.referencia AS referencia_imobiliaria',
                'imo.nome_imobiliaria','imo.codigo_imobiliaria',
                // SUBTIPOS DO IMOVEL
                'ist.id_sub_tipo','ist.id_tipo',
                // NOMES DO TIPO E SUBTIPO
                'ti.nome_tipo AS tipo_imovel',
                'sti.nome_tipo AS subtipo_imovel',
                // ESPECIFICACOES
                'espc.id_situacao_documental','espc.id_tipo_financiamento',
                'espc.id_tipo_garagem','espc.id_local_garagem',
                'espc.id_face_imovel','espc.qtd_vagas_estacionamento',
                'espc.qtd_vagas_garagem','espc.qtd_closets','espc.qtd_quartos',
                'espc.qtd_suites','espc.qtd_demi_suites','espc.qtd_churrasqueiras',
                'espc.qtd_copas','espc.qtd_despensas','espc.qtd_cozinhas',
                'espc.qtd_lareiras','espc.qtd_sacadas','espc.qtd_salas',
                'espc.qtd_salas_jantar','espc.qtd_salas_estar',
                'espc.qtd_salas_estar_intima','espc.qtd_salas_escritorio',
                'espc.qtd_salas_biblioteca','espc.qtd_lavabos',
                'espc.qtd_banheiros','espc.qtd_areas_servico',
                'espc.qtd_dependencias_empregada','espc.qtd_bwc_empregada',
                'espc.tem_adega','espc.tem_alarme','espc.tem_aquecimento',
                'espc.tem_atico','espc.tem_aquecimento_solar',
                'espc.tem_aquecimento_eletrico','espc.tem_bicicletario',
                'espc.tem_brinquedoteca','espc.tem_cancha_esportes',
                'espc.tem_central_gas','espc.tem_canil','espc.tem_cerca_eletrica',
                'espc.tem_churrasqueira_privada','espc.tem_churrasqueira_coletiva',
                'espc.tem_cozinha_armarios','espc.tem_cinema','espc.tem_sistema_ar',
                'espc.tem_sistema_incendio','espc.tem_sistema_cftv',
                'espc.tem_sistema_seguranca','espc.tem_deposito','espc.tem_despensa',
                'espc.tem_dormitorio_armarios','espc.tem_fitness','espc.tem_garden',
                'espc.tem_grades','espc.tem_hidro','espc.tem_interfone',
                'espc.tem_jardim_inverno','espc.tem_piscina',
                'espc.tem_piscina_infantil','espc.esta_mobiliado',
                'espc.tem_pista_caminhada','espc.tem_playground',
                'espc.tem_portao_eletronico','espc.tem_portaria_vinte_quatro',
                'espc.tem_quiosque','espc.tem_salao_festas','espc.tem_salao_jogos',
                'espc.tem_sauna','espc.tem_spa','espc.tem_espaco_gourmet',
                'espc.tem_edicula','espc.tem_quintal','espc.tem_jardim',
                'espc.conservacao','espc.area_privada','espc.area_total',
                'espc.area_averbada','espc.area_privada_terreno',
                'espc.area_util','espc.esquadrias','espc.revestimento_externo',
                'espc.construtora','espc.horarios_visita','espc.qtd_elevadores',
                'espc.qtd_apartamentos_andar','espc.numero_pavimentos',
                'espc.tipo_piso','espc.condicoes_negocio','espc.status_imovel',
                'espc.servicos_opcionais',
                // INFORMACOES COMPLEMENTARES
                'info.id_tipo_acao','info.id_tipo_ocupacao',
                'info.id_tipo_finalidade','info.id_face_apartamento',
                'info.id_tipo_andar','info.idade_imovel',
                'info.condominio_fechado','info.cobertura',
                'info.informacoes_adicionais','info.lancamento',
                'info.uso_tipo','info.reservado','info.andar_apartamento',
                'info.situacao','info.vago_em','info.acab_benfeito',
                // INFRAESTRUTURA
                'infra.id_face_terreno','infra.id_tipo_zoneamento',
                'infra.id_tipo_material','infra.area_terreno',
                'infra.medidas_terreno','infra.pavimentacao',
                'infra.pe_direito','infra.lote','infra.quadra','infra.planta',
                'infra.indicacao_fiscal','infra.topografia',
                'infra.registro_imovel','infra.circunscricao','infra.tem_agua',
                'infra.tem_esgoto','infra.tem_luz','infra.tem_telefone',
                'infra.tem_placa_local','infra.tem_forro','infra.tem_telhado',
                // VEICULACAO
                'vei.id_corretor AS corretor_veiculacao',
                'vei.id_privacidade','vei.data_angariacao','vei.viva_real',
                'vei.imovel_web','vei.zap','vei.zap_super','vei.enkontra',
                'vei.nao_imovel_web','vei.nao_zap','vei.video_youtube',
                // INFORMACOES XML
                'x.id_tipo_xml_status','x.data_insercao','x.data_atualizacao',
                'x.data_edicao','x.ultimas_modificacoes',
                // NEGOCIACAO
                'n.id_negociacao','n.id_corretor','n.id_proprietario',
                'n.id_cliente','n.id_tipo_negociacao','n.id_loja',
                'n.id_valores_imovel','n.id_comissao','n.detalhes_negociacao',
                // TIPO DE NEGOCIACAO
                'tn.nome_tipo AS tipo_negociacao',
                // VALORES
                'v.preco_venda','v.preco_locacao','v.valor_condominio',
                'v.valor_aluguel','v.valor_liquido','v.valor_negociacao',
                'v.valor_financiamento','v.valor_poupanca','v.valor_prestacao',
                'v.agente_financeiro','v.saldo_devedor','v.prazo',
                'v.numero_prestacoes_pagas','v.numero_prestacoes',
                'v.valor_bruto','v.bonificacao','v.valor_iptu',
                'v.outros_valores','v.prazo_contrato','v.reajuste',
                'v.indice_reajuste','v.valor_seguro_incendio','v.fci',
                'v.valor_seguro_fianca',
                // CIDADE
                'c.id_estado','c.nome_cidade',
                // BAIRRO
                'b.nome_bairro',
                // LAT LONG
                'l.latitude','l.longitude',
                // COMPLEMENTO
                'com.descricao AS complemento',
                'com.bloco','com.sala','com.nome_edificio',
                'com.nome_condominio','com.imediacoes','com.entre_ruas'
            ));
            $this->db->from('tb_imoveis AS i');
            $this->db->join('tb_enderecos AS e','i.id_imovel=e.id_imovel');
            $this->db->join('tb_imobiliaria AS imo','i.id_imobiliaria=imo.id_imobiliaria');
            $this->db->join('tb_imoveis_sub_tipos AS ist','i.id_imovel=ist.id_imovel');
            $this->db->join('tb_tipo_imovel AS ti','ist.id_tipo=ti.id_tipo_imovel');
            $this->db->join('tb_sub_tipo_imovel AS sti','ist.id_sub_tipo=sti.id_sub_tipo_imovel');
            $this->db->join('tb_especificacoes AS espc','i.id_especificacoes=espc.id_especificacao');
            $this->db->join('tb_informacoes_complementares AS info','i.id_informacoes_complementares=info.id_informacao');
            $this->db->join('tb_infraestrutura AS infra','i.id_infraestrutura=infra.id_infraestrutura');
            $this->db->join('tb_veiculacao AS vei','i.id_veiculacao=vei.id_veiculacao');
            $this->db->join('tb_informacoes_xml_imovel AS x','i.id_informacoes_xml=x.id_informacoes_xml', 'left');
            $this->db->join('tb_negociacao AS n','n.id_imovel=i.id_imovel');
            $this->db->join('tb_tipo_negociacao AS tn','n.id_tipo_negociacao=tn.id_tipo_negociacao');
            $this->db->join('tb_valores_imovel AS v','v.id_imovel=i.id_imovel');
            $this->db->join('tb_cidades AS c','e.id_cidade=c.id_cidade');
            $this->db->join('tb_bairros AS b','e.id_bairro=b.id_bairro', 'left');
            $this->db->join('tb_lat_long AS l','e.id_lat_long=l.id_lat_long', 'left');
            $this->db->join('tb_endereco_complemento AS com','e.id_complemento=com.id_complemento', 'left');

            $this->db->where('i.id_imovel', $id_imovel);

            return $this->db->get()->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR IMOVEL - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /*
     * BUSCAR_IMOVEIS_FILTROS
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : 
     * ---------------------------------------
     * RETORNO : [ARRAY]
    */
    public function buscar_imoveis_filtros(
        $id_tipo_negociacao=0, 
        $ids_tipos_imoveis=array(),

        $qtd=0, $inicio=0,

        $cidade=1, $bairros=array(), 
        $referencia=NULL, $condominio=NULL, $tipos_condominios=array(), 

        $filtro_ordenacao=NULL, 

        $filtro_menor_preco=0, $filtro_maior_preco=0,
        $filtro_qtd_quartos=0, $filtro_qtd_banheiros=0, $filtro_qtd_vagas=0, 
        $filtro_menor_area=0, $filtro_maior_area=0,

        $ids_nao_gostei=array()) 
    {
        return $this->_buscar_imoveis_filtrado(
            $id_tipo_negociacao, $ids_tipos_imoveis,
            $qtd, $inicio,
            $cidade, $bairros, 
            $referencia, $condominio, $tipos_condominios, 
            $filtro_ordenacao, 
            $filtro_menor_preco, $filtro_maior_preco,
            $filtro_qtd_quartos, $filtro_qtd_banheiros, $filtro_qtd_vagas, 
            $filtro_menor_area, $filtro_maior_area,
            $ids_nao_gostei);
    }

    /*
     * BUSCAR_IMOVEL_CARD
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - 
    */
    public function buscar_imovel_card($id_imovel=0) {
        return $this->_buscar_imovel_card_info($id_imovel);
    }

    /*
     * BUSCA_IMOVEIS_ALUGUEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - 
    */
    public function busca_imoveis_aluguel(
            $limete_resultados=6,
            $nome_bairro="",
            $id_imovel="",
            $tipo="",
            $preco=0,
            $quartos=0,
            $imobiliaria=""
        ) {
        try {
            $this->db->select( array( 
                'i.id_imovel', 'ist.id_sub_tipo', 'n.id_tipo_negociacao',
                'v.preco_locacao', 'v.valor_negociacao',
                'espc.area_total', 'espc.qtd_banheiros', 
                'espc.qtd_vagas_garagem As qtd_vagas', 'espc.qtd_quartos', 
                'espc.qtd_suites', 
                'b.nome_bairro AS bairro', 
                'c.nome_cidade AS cidade', 
                'img.link_imagem','i.referencia')
            );

            $this->db->from('tb_imoveis AS i');
            $this->db->join('tb_negociacao AS n','i.id_imovel=n.id_imovel');
            $this->db->join('tb_imoveis_sub_tipos AS ist','i.id_imovel=ist.id_imovel');
            $this->db->join('tb_especificacoes AS espc','i.id_especificacoes=espc.id_especificacao');
            $this->db->join('tb_enderecos AS e','i.id_imovel=e.id_imovel');
            $this->db->join('tb_imagens_imovel AS img','i.id_imovel=img.id_imovel');
            $this->db->join('tb_bairros AS b','e.id_bairro=b.id_bairro');
            $this->db->join('tb_cidades AS c','e.id_cidade=c.id_cidade');
            $this->db->join('tb_valores_imovel AS v','n.id_valores_imovel=v.id_valor_imovel');
            $this->db->where('n.id_tipo_negociacao', 2);
            if($imobiliaria!="") $this->db->where('i.id_imobiliaria', 1);
            if($nome_bairro!="") $this->db->where_in('b.nome_bairro', $nome_bairro);
            if($tipo!="") $this->db->where('ist.id_sub_tipo', $tipo);
            if($preco>0) {
                $this->db->where('v.valor_negociacao <', (int)($preco+$preco*0.4));
                $this->db->where('v.valor_negociacao >', (int)($preco-$preco*0.4));
            }
            if($preco<0) {
                $preco = (((int)($preco))*-1)-1;
                $this->db->where('v.valor_negociacao >', $preco);
            }
            if($quartos!=0) $this->db->where('espc.qtd_quartos >', $quartos-1);
            if($id_imovel!="") $this->db->where('i.id_imovel !=', $id_imovel);
            $this->db->group_start();
            $this->db->where('img.principal', TRUE);
            $this->db->or_where('img.principal', NULL);
            $this->db->group_end();
            $this->db->order_by('id_imovel', 'RANDOM');

            $this->db->limit($limete_resultados);

            return $this->db->get()->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCA IMOVEIS HOME ALUGUEL ' . $e->getMessage());

            return array();
        }
        return array();
    }
    
    /*
     * BUSCA_IMOVEIS_VENDA
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - 
    */
    public function busca_imoveis_venda(
            $limete_resultados=6,
            $nome_bairro="",
            $id_imovel="",
            $tipo="",
            $preco=0,
            $quartos=0,
            $imobiliaria=""
        ) {
        try {
            $this->db->select( array(
                'i.id_imovel','ist.id_sub_tipo', 'n.id_tipo_negociacao',  
                'v.preco_venda', 'v.valor_negociacao',
                'espc.area_total', 'espc.qtd_banheiros', 
                'espc.qtd_vagas_garagem As qtd_vagas', 'espc.qtd_quartos', 
                'espc.qtd_suites', 'b.nome_bairro AS bairro', 'c.nome_cidade AS cidade', 
                'img.link_imagem','i.referencia')
            );
            $this->db->from('tb_imoveis AS i');
            $this->db->join('tb_negociacao AS n','i.id_imovel=n.id_imovel');
            $this->db->join('tb_imoveis_sub_tipos AS ist','i.id_imovel=ist.id_imovel');
            $this->db->join('tb_tipo_imovel AS ti','ist.id_tipo=ti.id_tipo_imovel');
            $this->db->join('tb_sub_tipo_imovel AS sti','ist.id_sub_tipo=sti.id_sub_tipo_imovel');
            $this->db->join('tb_especificacoes AS espc','i.id_especificacoes=espc.id_especificacao');
            $this->db->join('tb_enderecos AS e','i.id_imovel=e.id_imovel');
            $this->db->join('tb_imagens_imovel AS img','i.id_imovel=img.id_imovel');
            $this->db->join('tb_bairros AS b','e.id_bairro=b.id_bairro');
            $this->db->join('tb_cidades AS c','e.id_cidade=c.id_cidade');
            $this->db->join('tb_valores_imovel AS v','n.id_valores_imovel=v.id_valor_imovel');
            $this->db->where('n.id_tipo_negociacao',1);
            if($imobiliaria!="") $this->db->where('i.id_imobiliaria', 1);
            if($nome_bairro!="") $this->db->where_in('LOWER(b.nome_bairro)', $nome_bairro);
            if($tipo!="") $this->db->where('ist.id_sub_tipo', $tipo);
            if($preco>0) {
                $this->db->where('v.preco_venda <', (int)($preco+$preco*0.2));
                $this->db->where('v.preco_venda >', (int)($preco-$preco*0.2));
            }
            if($preco<0) {
                $preco = (((int)($preco))*-1)-1;
                $this->db->where('v.preco_venda >', $preco);
            }
            if($quartos!=0) $this->db->where('espc.qtd_quartos >', $quartos-1);
            if($id_imovel!="") $this->db->where('i.id_imovel !=', $id_imovel);
            $this->db->where('img.principal', TRUE);

            $this->db->order_by('id_imovel', 'RANDOM');

            $this->db->limit($limete_resultados);

            return $this->db->get()->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCA IMOVEIS HOME ALUGUEL ' . $e->getMessage());

            return array();
        }
        return array();
    }

    /*
     * BUSCAR_TODOS_TIPOS_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - 
    */
    public function buscar_todos_tipos_imovel($id_imovel) {
        try {
            $this->db->select( array(
                'ti.id_tipo_imovel', 'ti.nome_tipo AS tipo_imovel',
                'sti.id_sub_tipo_imovel', 'sti.nome_tipo AS subtipo_imovel')
            );
            $this->db->from('tb_imoveis_sub_tipos AS ist');
            $this->db->join('tb_tipo_imovel AS ti','ist.id_tipo=ti.id_tipo_imovel');
            $this->db->join('tb_sub_tipo_imovel AS sti','ist.id_sub_tipo=sti.id_sub_tipo_imovel');
            $this->db->where('ist.id_imovel', $id_imovel);
            $this->db->group_by('ist.id_tipo');
            $this->db->order_by('ist.id_tipo', 'ASC');

            return $this->db->get()->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR TODOS TIPOS IMOVEL ' . $e->getMessage());

            return array();
        }
        return array();
    }

    /*
     * BUSCAR_IMAGEM_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - 
    */
    public function buscar_imagem_imovel($id_imovel=0, $ordem=0, $principal=TRUE) {
        try {
            $this->db->select( array(
                'img.link_imagem', 'img.link_miniatura')
            );
            $this->db->from('tb_imagens_imovel AS img');
            $this->db->where('img.id_imovel', $id_imovel);
            if ($principal) {
                $this->db->where('img.principal', $principal);
            }
            if ($ordem > 0) {
                $this->db->where('img.ordem', $ordem);
            }

            return $this->db->get()->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR IMAGEM IMOVEL ' . $e->getMessage());

            return array();
        }
        return array();
    }
    
    /*
     * BUSCAR_TODAS_IMAGEM_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - 
    */
    public function buscar_todas_imagem_imovel($id_imovel) {
        try {
            $this->db->select( array(
                'img.link_imagem', 'img.link_miniatura')
            );
            $this->db->from('tb_imagens_imovel AS img');
            $this->db->where('id_imovel', $id_imovel);
            $this->db->order_by('ordem', 'ASC');
            return $this->db->get()->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR TODAS IMAGEM IMOVEL ' . $e->getMessage());

            return array();
        }
        return array();
    }

    /*
     * BUSCAR_MENOR_MAIOR_PRECO_VENDA
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - 
    */
    public function buscar_menor_maior_preco_venda() {
        try {
            $this->db->select( array(
                'MIN(preco_venda) AS menor', 'MAX(preco_venda) AS maior')
            );
            $this->db->from('tb_valores_imovel');
            $this->db->where('preco_venda >', 0);
            return $this->db->get()->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR MENOR MAIOR PRECO VENDA ' . $e->getMessage());

            return array();
        }
        return array();
    }

    /*
     * BUSCAR_MENOR_MAIOR_PRECO_LOCACAO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - 
    */
    public function buscar_menor_maior_preco_locacao() {
        try {
            $this->db->select( array(
                'MIN(preco_locacao) AS menor', 'MAX(preco_locacao) AS maior')
            );
            $this->db->from('tb_valores_imovel');
            $this->db->where('preco_locacao >', 0);
            return $this->db->get()->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR MENOR MAIOR PRECO LOCACAO ' . $e->getMessage());

            return array();
        }
        return array();
    }

    /*
     * BUSCAR_MENOR_MAIOR_AREA_VENDA
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - 
    */
    public function buscar_menor_maior_area_venda() {
        try {
            $this->db->select( array(
                'MIN(area_util) AS menor', 'MAX(area_util) AS maior')
            );
            $this->db->from('tb_especificacoes AS e');
            $this->db->join('tb_imoveis AS i','i.id_especificacoes=e.id_especificacao');
            $this->db->join('tb_negociacao AS n','n.id_imovel=i.id_imovel');
            $this->db->where('area_util >', 0);
            $this->db->where('id_tipo_negociacao', 1);
            return $this->db->get()->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR MENOR MAIOR AREA VENDA ' . $e->getMessage());

            return array();
        }
        return array();
    }

    /*
     * BUSCAR_MENOR_MAIOR_AREA_LOCACAO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - 
    */
    public function buscar_menor_maior_area_locacao() {
        try {
            $this->db->select( array(
                'MIN(area_util) AS menor', 'MAX(area_util) AS maior')
            );
            $this->db->from('tb_especificacoes AS e');
            $this->db->join('tb_imoveis AS i','i.id_especificacoes=e.id_especificacao');
            $this->db->join('tb_negociacao AS n','n.id_imovel=i.id_imovel');
            $this->db->where('area_util >', 0);
            $this->db->where('id_tipo_negociacao', 2);
            return $this->db->get()->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR MENOR MAIOR AREA LOCACAO ' . $e->getMessage());

            return array();
        }
        return array();
    }
    
    /*
     * BUSCAR TODAS INFORMACOES IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - 
    */
    public function buscar_informacao_imovel_visualizar($id_imovel) {
        try {
            $this->db->select( array( 
                'i.id_imovel', 'i.descricao', 'ist.id_sub_tipo',
                'bai.nome_bairro', 'i.referencia', 
                'e.qtd_banheiros', 'e.qtd_vagas_garagem', 
                'e.qtd_quartos', 'e.qtd_suites', 
                'e.area_total', 'e.area_util', 
                'end.logradouro', 'end.numero', 'c.nome_cidade', 
                'v.valor_negociacao', 'v.preco_venda', 'v.preco_locacao', 
                'v.valor_condominio', 'v.valor_iptu', 
                'tb_lat_long.latitude', 'tb_lat_long.longitude', 
                'n.id_tipo_negociacao', 'vi.video_youtube', 
                'ic.idade_imovel')
            );
            $this->db->from('tb_imoveis AS i');
            $this->db->join('tb_enderecos AS end','i.id_imovel=end.id_imovel');
            $this->db->join('tb_valores_imovel AS v','i.id_imovel=v.id_imovel');
            $this->db->join('tb_imoveis_sub_tipos AS ist','i.id_imovel=ist.id_imovel', 'left');
            $this->db->join('tb_especificacoes AS e','i.id_especificacoes=e.id_especificacao');
            $this->db->join('tb_veiculacao AS vi','vi.id_veiculacao=i.id_veiculacao');
            $this->db->join('tb_bairros AS bai','bai.id_bairro=end.id_bairro', 'left');
            $this->db->join('tb_cidades AS c','end.id_cidade=c.id_cidade');
            $this->db->join('tb_lat_long','tb_lat_long.id_lat_long=end.id_lat_long', 'left');
            $this->db->join('tb_negociacao AS n','n.id_imovel=i.id_imovel');
            $this->db->join('tb_informacoes_complementares AS ic','i.id_informacoes_complementares=ic.id_informacao');
            $this->db->where('i.id_imovel', $id_imovel);

            return $this->db->get()->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR INFORMACAO IMOVEL VISUALIZAR ' . $e->getMessage());

            return array();
        }
        return array();
    }

    /*
     * BUSCA_IDS_IMOVEIS_NAO_GOSTEI
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - 
    */
    public function busca_ids_imoveis_nao_gostei($id_ou_cookie=NULL) {
        try {
            if ($id_ou_cookie != NULL) {
                $this->db->select( array('nao.id_imovel') );
                $this->db->from('tb_imoveis_nao_gostei AS nao');
                $this->db->where('nao.id_cliente', $id_ou_cookie);
                $this->db->or_where('nao.cookie', $id_ou_cookie);

                return $this->db->get()->result_array();
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCA IDS IMOVEIS NAO GOSTEI ' . $e->getMessage());

            return array();
        }
        return array();
    }

    /*
     * BUSCA_ID_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - 
    */
    public function buscar_id_imovel($tipo, $bairro, $referencia) {
        try {
            $this->db->select('tb_imoveis.id_imovel');
            $this->db->from('tb_imoveis');
            $this->db->join('tb_imoveis_sub_tipos', "tb_imoveis_sub_tipos.id_imovel=tb_imoveis.id_imovel");
            $this->db->join('tb_sub_tipo_imovel', "tb_sub_tipo_imovel.id_sub_tipo_imovel=tb_imoveis_sub_tipos.id_sub_tipo");
            $this->db->join('tb_enderecos', "tb_enderecos.id_imovel=tb_imoveis.id_imovel");
            $this->db->join('tb_bairros', "tb_enderecos.id_bairro=tb_bairros.id_bairro");
            $this->db->like('REPLACE(nome_tipo, "/", "_")', $tipo);
            $this->db->like('REPLACE(REPLACE(LOWER(nome_bairro), "/", "_"), "-", "_")', $bairro);
            $this->db->like('REPLACE(referencia, ".", "")', $referencia);

            /*echo "<pre>";
            print_r($this->db->get()->result_array());
            die();*/

            return $this->db->get()->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCA IDS IMOVEIS VISUALIZAR ' . $e->getMessage());

            return array();
        }
        return array();
    }

    /* ****************************************************** */
    /* ********************** INSERCAO ********************** */
    /* ****************************************************** */
    /*
     * INSERIR_ESPECIFICACOES
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [INT] - 
    */
    public function inserir_especificacoes($dados=array()) {
        try {
            if (!empty($dados)) {
                $this->db->insert("tb_especificacoes", $dados);
                return $this->db->insert_id();
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - INSERIR ESPECIFICACOES - ' . $e->getMessage());

            return -1;
        }
        return -1;
    }

    /*
     * INSERIR_INFORMACOES_COMPLEMENTARES
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [INT] - 
    */
    public function inserir_informacoes_complementares($dados=array()) {
        try {
            if (!empty($dados)) {
                $this->db->insert("tb_informacoes_complementares", $dados);
                return $this->db->insert_id();
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - INSERIR INFORMACOES COMPLEMENTARES - ' . $e->getMessage());

            return -1;
        }
        return -1;
    }

    /*
     * INSERIR_INFRAESTRUTURA
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [INT] - 
    */
    public function inserir_infraestrutura($dados=array()) {
        try {
            if (!empty($dados)) {
                $this->db->insert("tb_infraestrutura", $dados);
                return $this->db->insert_id();
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - INSERIR INFRAESTRUTURA - ' . $e->getMessage());

            return -1;
        }
        return -1;
    }

    /*
     * INSERIR_VEICULACAO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [INT] - 
    */
    public function inserir_veiculacao($dados=array()) {
        try {
            if (!empty($dados)) {
                $this->db->insert("tb_veiculacao", $dados);
                return $this->db->insert_id();
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - INSERIR VEICULACAO - ' . $e->getMessage());

            return -1;
        }
        return -1;
    }

    /*
     * INSERIR_IMOVEL
     * ---------------------------------------
     * DESCRICAO : Retorna o ID do imovel in-
     * serido.
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [INT] - ID do novo imovel
    */
    public function inserir_imovel($dados=array(), $atualizar_id=FALSE) {
        try {
            if (!empty($dados)) {
                $id_imovel = -1;

                if (!array_key_exists("id_imovel", $dados)) {
                    $id_imovel = $this->_gerar_id_imovel();

                    $dados["id_imovel"] = $id_imovel;
                } else {
                    $id_imovel = $dados["id_imovel"];
                }

                if (!array_key_exists("id_baggio", $dados)) {
                    $dados["id_baggio"] = $this->_gerar_id_baggio();
                }

                $this->db->insert("tb_imoveis", $dados);

                if ($atualizar_id) {
                    $this->_atualizar_ultimo_id_imovel();
                }
                return $id_imovel;
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - INSERIR IMOVEL - ' . $e->getMessage());

            return -1;
        }
        return -1;
    }

    /*
     * INSERIR_CHAVE
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [INT] - 
    */
    public function inserir_chave($dados=array()) {
        try {
            if (!empty($dados)) {
                $this->db->insert("tb_chaves", $dados);
                return TRUE;
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - INSERIR CHAVE - ' . $e->getMessage());

            return FALSE;
        }
        return FALSE;
    }

    /*
     * INSERIR_CHAVES
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [INT] - 
    */
    public function inserir_chaves($dados=array()) {
        try {
            if (!empty($dados)) {
                $this->db->insert_batch("tb_chaves", $dados);
                return TRUE;
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - INSERIR CHAVES - ' . $e->getMessage());

            return FALSE;
        }
        return FALSE;
    }

    /*
     * INSERIR_IMOVEL_SUB_TIPO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [INT] - 
    */
    public function inserir_imovel_sub_tipo($dados=array()) {
        try {
            if (!empty($dados)) {
                $this->db->insert("tb_imoveis_sub_tipos", $dados);
                return TRUE;
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - INSERIR IMOVEL SUB TIPO - ' . $e->getMessage());

            return FALSE;
        }
        return FALSE;
    }

    /*
     * INSERIR_IMOVEL_SUB_TIPOS
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [INT] - 
    */
    public function inserir_imovel_sub_tipos($dados=array()) {
        try {
            if (!empty($dados)) {
                $this->db->insert_batch("tb_imoveis_sub_tipos", $dados);
                return TRUE;
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - INSERIR IMOVEL SUB TIPOS - ' . $e->getMessage());

            return FALSE;
        }
        return FALSE;
    }

    /*
     * INSERIR_IMOVEL_NAO_GOSTEI
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - 
    */
    public function inserir_imovel_nao_gostei($id_imovel=NULL) {
        try {
            if ($id_imovel != NULL) {
                $this->db->select( array('nao.id_imovel') );
                $this->db->from('tb_imoveis_nao_gostei AS nao');
                $this->db->where('nao.id_cliente', $id_ou_cookie);
                $this->db->or_where('nao.cookie', $id_ou_cookie);

                return $this->db->get()->result_array();
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCA IDS IMOVEIS NAO GOSTEI ' . $e->getMessage());

            return array();
        }
        return array();
    }

    /* ****************************************************** */
    /* ******************** ATUALIZACAO ********************* */
    /* ****************************************************** */
    /*
     * ATUALIZAR_ESPECIFICACOES
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [INT] - 
    */
    public function atualizar_especificacoes($id_imovel=NULL, $dados=array()) {
        try {
            if ($id_imovel != NULL && !empty($dados)) {
                $this->db->select( array('i.id_especificacoes') );
                $this->db->from('tb_imoveis AS i');
                $this->db->where('i.id_imovel', $id_imovel);

                $id_especificacao = values_only_from_resultarray('id_especificacoes', $this->db->get()->result_array());

                $this->db->where('id_especificacao', $id_especificacao[0]);

                if ($this->db->update('tb_especificacoes', $dados)) {
                    return $id_especificacao[0];
                }
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ATUALIZAR ESPECIFICACOES - ' . $e->getMessage());

            return NULL;
        }
        return NULL;
    }

    /*
     * ATUALIZAR_INFORMACOES_COMPLEMENTARES
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [INT] - 
    */
    public function atualizar_informacoes_complementares($id_imovel=NULL, $dados=array()) {
        try {
            if ($id_imovel != NULL && !empty($dados)) {
                $this->db->select( array('i.id_informacoes_complementares') );
                $this->db->from('tb_imoveis AS i');
                $this->db->where('i.id_imovel', $id_imovel);

                $id_informacoes_complementares = values_only_from_resultarray('id_informacoes_complementares', $this->db->get()->result_array());

                $this->db->where('id_informacao', $id_informacoes_complementares[0]);

                if ($this->db->update("tb_informacoes_complementares", $dados)) {
                    return $id_informacoes_complementares[0];
                }
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ATUALIZAR INFORMACOES COMPLEMENTARES - ' . $e->getMessage());

            return NULL;
        }
        return NULL;
    }

    /*
     * ATUALIZAR_INFRAESTRUTURA
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [INT] - 
    */
    public function atualizar_infraestrutura($id_imovel=NULL, $dados=array()) {
        try {
            if ($id_imovel != NULL && !empty($dados)) {
                $this->db->select( array('i.id_infraestrutura') );
                $this->db->from('tb_imoveis AS i');
                $this->db->where('i.id_imovel', $id_imovel);

                $id_infraestrutura = values_only_from_resultarray('id_infraestrutura', $this->db->get()->result_array());

                $this->db->where('id_infraestrutura', $id_infraestrutura[0]);

                if ($this->db->update("tb_infraestrutura", $dados)) {
                    return $id_infraestrutura[0];
                }
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ATUALIZAR INFRAESTRUTURA - ' . $e->getMessage());

            return NULL;
        }
        return NULL;
    }

    /*
     * ATUALIZAR_VEICULACAO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [INT] - 
    */
    public function atualizar_veiculacao($id_imovel=NULL, $dados=array()) {
        try {
            if ($id_imovel != NULL && !empty($dados)) {
                $this->db->select( array('i.id_veiculacao') );
                $this->db->from('tb_imoveis AS i');
                $this->db->where('i.id_imovel', $id_imovel);

                $id_veiculacao = values_only_from_resultarray('id_veiculacao', $this->db->get()->result_array());

                $this->db->where('id_veiculacao', $id_veiculacao[0]);

                return $this->db->update("tb_veiculacao", $dados);
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ATUALIZAR VEICULACAO - ' . $e->getMessage());
        }
        return FALSE;
    }

    /*
     * ATUALIZAR_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [INT] - ID do novo imovel
    */
    public function atualizar_imovel($id_imovel=NULL, $dados=array()) {
        try {
            if ($id_imovel != NULL && !empty($dados)) {
                $dados["id_imovel"] = $id_imovel;

                $this->db->where("id_imovel", $id_imovel);

                return $this->db->update('tb_imoveis', $dados);
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ATUALIZAR IMOVEL - ' . $e->getMessage());
        }
        return FALSE;
    }

    /*
     * ATUALIZAR_IMOVEL_SUB_TIPO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [INT] - 
    */
    public function atualizar_imovel_sub_tipo($id_imovel=NULL, $dados=array()) {
        try {
            if ($id_imovel != NULL && !empty($dados)) {
                $this->db->where('id_imovel', $id_imovel);

                return $this->db->update("tb_imoveis_sub_tipos", $dados);
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ATUALIZAR IMOVEL SUB TIPO - ' . $e->getMessage());
        }
        return FALSE;
    }

    /* ****************************************************** */
    /* ********************** EXCLUSAO ********************** */
    /* ****************************************************** */
    /*
     * EXCLUIR_VALORES_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function excluir_valores_imovel($id_imovel=0) {
        $id_valor_imovel = -1;
        try {
            $this->db->select('id_valor_imovel');
            $this->db->from('tb_valores_imovel');
            $this->db->where('id_imovel', $id_imovel);

            $result_array = $this->db->get()->result_array();

            if (!empty($result_array)) {
                $id_valor_imovel = intval( $result_array[0]['id_valor_imovel'] );
            } 

            $this->db->where('id_imovel', $id_imovel);
            $this->db->delete('tb_valores_imovel');
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - EXCLUIR VALORES IMOVEL - ' . $e->getMessage());
            return $id_valor_imovel;
        }
        return $id_valor_imovel;
    }

    /*
     * EXCLUIR_CHAVES_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function excluir_chaves_imovel($id_imovel=0) {
        try {
            $this->db->where('id_imovel', $id_imovel);
            $this->db->delete('tb_chaves');

            return TRUE;
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - EXCLUIR CHAVES IMOVEL - ' . $e->getMessage());
            return FALSE;
        }
        return FALSE;
    }

    /*
     * EXCLUIR_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function excluir_imovel($id_imovel=0, $referencia="", $id_imobiliaria=0) {
        try {
            $this->db->where('id_imovel', $id_imovel);
            $this->db->where('referencia', $referencia);
            $this->db->where('id_imobiliaria', $id_imobiliaria);
            $this->db->delete('tb_imoveis');

            return TRUE;
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - EXCLUIR IMOVEL - ' . $e->getMessage());
            return FALSE;
        }
        return FALSE;
    }

    /*
     * EXCLUIR_ESPECIFICACOES_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function excluir_especificacoes_imovel($id_imovel=0) {
        try {
            $this->db->query(
                "DELETE `espc` 
                    FROM    `tb_especificacoes` AS `espc` 
                    JOIN    `tb_imoveis` AS `i`
                    WHERE   `espc`.`id_especificacao` = `i`.`id_especificacoes`
                    AND     `i`.`id_imovel` = $id_imovel;
            ");

            return TRUE;
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - EXCLUIR ESPECIFICACOES IMOVEL - ' . $e->getMessage());
            return FALSE;
        }
        return FALSE;
    }

    /*
     * EXCLUIR_INFORMACOES_COMPLEMENTARES_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function excluir_informacoes_complementares_imovel($id_imovel=0) {
        try {
            $this->db->query(
                "DELETE `info` 
                    FROM    `tb_informacoes_complementares` AS `info` 
                    JOIN    `tb_imoveis` AS `i`
                    WHERE   `info`.`id_informacao` = `i`.`id_informacoes_complementares`
                    AND     `i`.`id_imovel` = $id_imovel;
            ");

            return TRUE;
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - EXCLUIR INFORMACOES COMPLEMENTARES IMOVEL - ' . $e->getMessage());
            return FALSE;
        }
        return FALSE;
    }

    /*
     * EXCLUIR_INFRAESTRUTURA_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function excluir_infraestrutura_imovel($id_imovel=0) {
        try {
            $this->db->query(
                "DELETE `infra` 
                    FROM    `tb_infraestrutura` AS `infra` 
                    JOIN    `tb_imoveis` AS `i`
                    WHERE   `infra`.`id_infraestrutura` = `i`.`id_infraestrutura`
                    AND     `i`.`id_imovel` = $id_imovel;
            ");

            return TRUE;
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - EXCLUIR INFRAESTRUTURA IMOVEL - ' . $e->getMessage());
            return FALSE;
        }
        return FALSE;
    }

    /*
     * EXCLUIR_VEICULACAO_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function excluir_veiculacao_imovel($id_imovel=0) {
        try {
            $this->db->query(
                "DELETE `v` 
                    FROM    `tb_veiculacao` AS `v` 
                    JOIN    `tb_imoveis` AS `i`
                    WHERE   `v`.`id_veiculacao` = `i`.`id_veiculacao`
                    AND     `i`.`id_imovel` = $id_imovel;
            ");

            return TRUE;
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - EXCLUIR VEICULACAO IMOVEL - ' . $e->getMessage());
            return FALSE;
        }
        return FALSE;
    }

    /*
     * EXCLUIR_INFORMACOES_XML_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function excluir_informacoes_xml_imovel($id_imovel=0) {
        try {
            $this->db->query(
                "DELETE `x` 
                    FROM    `tb_informacoes_xml_imovel` AS `x` 
                    JOIN    `tb_imoveis` AS `i`
                    WHERE   `x`.`id_informacoes_xml` = `i`.`id_informacoes_xml`
                    AND     `i`.`id_imovel` = $id_imovel;
            ");

            return TRUE;
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - EXCLUIR INFORMACOES XML IMOVEL - ' . $e->getMessage());
            return FALSE;
        }
        return FALSE;
    }

    /*
     * EXCLUIR_IMOVEIS_IMOBILIARIA
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function excluir_imoveis_imobiliaria($id_imobiliaria=0, $zerar_ids=FALSE) {
        try {
            $this->db->trans_start();

            /* ***** SELECIONAR TODOS OS IMOVEIS ***** */
            $this->db->select('i.id_imovel')
            ->from('tb_imoveis AS i')
            ->join('tb_imobiliaria AS imo','i.id_imobiliaria=imo.id_imobiliaria')
            ->where("imo.id_imobiliaria = $id_imobiliaria", NULL, FALSE)
            ->where('i.xml_atualizavel', TRUE);

            $select_ids_imoveis =  $this->db->get_compiled_select();

            /* ***** DELETAR MATERIAL DE APOIO ***** */
            $this->db->select('n.id_negociacao')
            ->from('tb_negociacao AS n')
            ->where("n.id_imovel IN ($select_ids_imoveis)", NULL, FALSE);

            $select_ids_negociacoes =  $this->db->get_compiled_select();

            $this->db->where("id_negociacao IN ($select_ids_negociacoes)", NULL, FALSE)
            ->delete('tb_material_apoio');

            if ($zerar_ids) {
                $this->db->query("ALTER TABLE `tb_material_apoio` AUTO_INCREMENT=1;");
            }

            /* ***** DELETAR NEGOCIACOES ***** */
            $this->db->where("id_imovel IN ($select_ids_imoveis)", NULL, FALSE)
            ->delete('tb_negociacao');

            if ($zerar_ids) {
                $this->db->query("ALTER TABLE `tb_negociacao` AUTO_INCREMENT=1;");
            }

            /* ***** DELETAR VALORES ***** */
            $this->db->where("id_imovel IN ($select_ids_imoveis)", NULL, FALSE)
            ->delete('tb_valores_imovel');

            if ($zerar_ids) {
                $this->db->query("ALTER TABLE `tb_valores_imovel` AUTO_INCREMENT=1;");
            }

            /* ***** DELETAR LAT LONG ***** */
            $this->db->select('e.id_lat_long')
            ->from('tb_enderecos AS e')
            ->where("e.id_imovel IN ($select_ids_imoveis)", NULL, FALSE);

            $select_ids_lats_longs =  $this->db->get_compiled_select();

            $this->db->where("id_lat_long IN ($select_ids_lats_longs)", NULL, FALSE)
            ->delete('tb_lat_long');

            if ($zerar_ids) {
                $this->db->query("ALTER TABLE `tb_lat_long` AUTO_INCREMENT=1;");
            }

            /* ***** DELETAR COMPLEMENTOS ***** */
            $this->db->select('e.id_complemento')
            ->from('tb_enderecos AS e')
            ->where("e.id_imovel IN ($select_ids_imoveis)", NULL, FALSE);

            $select_ids_complementos =  $this->db->get_compiled_select();

            $this->db->where("id_complemento IN ($select_ids_complementos)", NULL, FALSE)
            ->delete('tb_endereco_complemento');

            if ($zerar_ids) {
                $this->db->query("ALTER TABLE `tb_endereco_complemento` AUTO_INCREMENT=1;");
            }

            /* ***** DELETAR CIDADES ***** */
            // $this->db->select('e.id_cidade')
            // ->from('tb_enderecos AS e')
            // ->where("e.id_imovel IN ($select_ids_imoveis)", NULL, FALSE);

            // $select_ids_cidades =  $this->db->get_compiled_select();

            // $this->db->where("id_cidade IN ($select_ids_cidades)", NULL, FALSE)
            // ->delete('tb_cidades');

            // if ($zerar_ids) {
            //     $this->db->query("ALTER TABLE `tb_cidades` AUTO_INCREMENT=1;");
            // }

            /* ***** DELETAR BAIRROS ***** */
            // $this->db->select('e.id_bairro')
            // ->from('tb_enderecos AS e')
            // ->where("e.id_imovel IN ($select_ids_imoveis)", NULL, FALSE);

            // $select_ids_bairros =  $this->db->get_compiled_select();

            // $this->db->where("id_bairro IN ($select_ids_bairros)", NULL, FALSE)
            // ->delete('tb_bairros');

            // if ($zerar_ids) {
            //     $this->db->query("ALTER TABLE `tb_bairros` AUTO_INCREMENT=1;");
            // }

            /* ***** DELETAR ENDERECOS ***** */
            $this->db->where("id_imovel IN ($select_ids_imoveis)", NULL, FALSE)
            ->delete('tb_enderecos');

            if ($zerar_ids) {
                $this->db->query("ALTER TABLE `tb_enderecos` AUTO_INCREMENT=1;");
            }

            /* ***** DELETAR CHAVES ***** */
            $this->db->where("id_imovel IN ($select_ids_imoveis)", NULL, FALSE)
            ->delete('tb_chaves');

            $this->db->where("id_imovel", NULL)->delete('tb_chaves');

            if ($zerar_ids) {
                $this->db->query("ALTER TABLE `tb_chaves` AUTO_INCREMENT=1;");
            }

            /* ***** DELETAR IMAGENS ***** */
            /* TODO : Necessário deletar todas a imagens dos imoveis que foram removidos */

            $this->db->where("id_imovel IN ($select_ids_imoveis)", NULL, FALSE)
            ->delete('tb_imagens_imovel');

            $this->db->where("id_imovel", NULL)->delete('tb_imagens_imovel');

            if ($zerar_ids) {
                $this->db->query("ALTER TABLE `tb_imagens_imovel` AUTO_INCREMENT=1;");
            }

            /* ***** DELETAR ESPECIFICACOES ***** */
            $this->db->select('i.id_especificacoes')
            ->from('tb_imoveis AS i')
            ->join('tb_imobiliaria AS imo','i.id_imobiliaria=imo.id_imobiliaria')
            ->where("imo.id_imobiliaria = $id_imobiliaria", NULL, FALSE);

            $select_ids_especificacoes =  $this->db->get_compiled_select();

            $this->db->where("id_especificacao IN ($select_ids_especificacoes)", NULL, FALSE)
            ->delete('tb_especificacoes');

            if ($zerar_ids) {
                $this->db->query("ALTER TABLE `tb_especificacoes` AUTO_INCREMENT=1;");
            }

            /* ***** DELETAR INFORMACOES COMPLEMENTARES ***** */
            $this->db->select('i.id_informacoes_complementares')
            ->from('tb_imoveis AS i')
            ->join('tb_imobiliaria AS imo','i.id_imobiliaria=imo.id_imobiliaria')
            ->where("imo.id_imobiliaria = $id_imobiliaria", NULL, FALSE);

            $select_ids_informacoes_complementares =  $this->db->get_compiled_select();

            $this->db->where("id_informacao IN ($select_ids_informacoes_complementares)", NULL, FALSE)
            ->delete('tb_informacoes_complementares');

            if ($zerar_ids) {
                $this->db->query("ALTER TABLE `tb_informacoes_complementares` AUTO_INCREMENT=1;");
            }

            /* ***** DELETAR INFRAESTRUTURA ***** */
            $this->db->select('i.id_infraestrutura')
            ->from('tb_imoveis AS i')
            ->join('tb_imobiliaria AS imo','i.id_imobiliaria=imo.id_imobiliaria')
            ->where("imo.id_imobiliaria = $id_imobiliaria", NULL, FALSE);

            $select_ids_infraestruturas =  $this->db->get_compiled_select();

            $this->db->where("id_infraestrutura IN ($select_ids_infraestruturas)", NULL, FALSE)
            ->delete('tb_infraestrutura');

            if ($zerar_ids) {
                $this->db->query("ALTER TABLE `tb_infraestrutura` AUTO_INCREMENT=1;");
            }

            /* ***** DELETAR VEICULACAO ***** */
            $this->db->select('i.id_veiculacao')
            ->from('tb_imoveis AS i')
            ->join('tb_imobiliaria AS imo','i.id_imobiliaria=imo.id_imobiliaria')
            ->where("imo.id_imobiliaria = $id_imobiliaria", NULL, FALSE);

            $select_ids_veiculacoes =  $this->db->get_compiled_select();

            $this->db->where("id_veiculacao IN ($select_ids_veiculacoes)", NULL, FALSE)
            ->delete('tb_veiculacao');

            if ($zerar_ids) {
                $this->db->query("ALTER TABLE `tb_veiculacao` AUTO_INCREMENT=1;");
            }

            /* ***** DELETAR INFORMACOES XML ***** */
            $this->db->select('i.id_informacoes_xml')
            ->from('tb_imoveis AS i')
            ->join('tb_imobiliaria AS imo','i.id_imobiliaria=imo.id_imobiliaria')
            ->where("imo.id_imobiliaria = $id_imobiliaria", NULL, FALSE);

            $select_ids_informacoes_xml =  $this->db->get_compiled_select();

            $this->db->where("id_informacoes_xml IN ($select_ids_informacoes_xml)", NULL, FALSE)
            ->delete('tb_informacoes_xml_imovel');

            if ($zerar_ids) {
                $this->db->query("ALTER TABLE `tb_informacoes_xml_imovel` AUTO_INCREMENT=1;");
            }

            /* ***** DELETAR IMOVEIS SUBTIPOS ***** */
            $this->db->where("id_imovel IN ($select_ids_imoveis)", NULL, FALSE)
            ->delete('tb_imoveis_sub_tipos');

            if ($zerar_ids) {
                $this->db->query("ALTER TABLE `tb_imoveis_sub_tipos` AUTO_INCREMENT=1;");
            }

            /* ***** DELETAR IMOVEIS ***** */
            $this->db->where('id_imobiliaria', $id_imobiliaria);
            $this->db->delete('tb_imoveis');

            if ($zerar_ids) {
                $this->db->query("ALTER TABLE `tb_imoveis` AUTO_INCREMENT=1;");
            }

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();

                return TRUE;
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - EXCLUIR IMOVEIS IMOBILIARIA - ' . $e->getMessage());
            return FALSE;
        }
        return FALSE;
    }

    /* ****************************************************** */
    /* ********************** PRIVATES ********************** */
    /* ****************************************************** */
    /*
     * _ATUALIZAR_ULTIMO_ID_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [INT] - 
    */
    private function _atualizar_ultimo_id_imovel() {
        try {
            $this->db->select('i.id_imovel');
            $this->db->from('tb_imoveis AS i');
            $this->db->order_by('i.id_imovel', 'DESC');

            $result_array = $this->db->get()->result_array();

            $novo_id_imovel = intval($result_array[0]["id_imovel"]) + 1;

            $this->db->query("ALTER TABLE tb_imoveis AUTO_INCREMENT = $novo_id_imovel");
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ATUALIZAR ULTIMO ID IMOVEL - PRIVATE - ' . $e->getMessage());
        }
    }

    /*
     * _GERAR_ID_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [INT] - 
    */
    private function _gerar_id_imovel() {
        try {
            $this->db->select('i.id_imovel');
            $this->db->from('tb_imoveis AS i');
            $this->db->order_by('i.id_imovel', 'DESC');

            $result_array = $this->db->get()->result_array();

            if (!empty($result_array)) {
                $novo_id_imovel = intval($result_array[0]["id_imovel"]) + 1;
            } else {
                $novo_id_imovel = 1;
            }
            return $novo_id_imovel;
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - GERAR ID IMOVEL - PRIVATE - ' . $e->getMessage());

            return -1;
        }
        return -1;
    }

    /*
     * _GERAR_ID_BAGGIO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [INT] - 
    */
    private function _gerar_id_baggio() {
        try {
            $this->db->select('i.id_baggio');
            $this->db->from('tb_imoveis AS i');
            $this->db->order_by('i.id_baggio', 'DESC');

            $result_array = $this->db->get()->result_array();

            if (!empty($result_array)) {
                $novo_id_baggio = intval($result_array[0]["id_baggio"]) + 1;
            } else {
                $novo_id_baggio = 1;
            }
            return $novo_id_baggio;
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - GERAR ID BAGGIO - PRIVATE - ' . $e->getMessage());

            return -1;
        }
        return -1;
    }

    /*
     * _BUSCAR_IMOVEL_CARD_INFO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - 
    */
    private function _buscar_imovel_card_info($id_imovel=0) {
        try {
            $this->db->select( array( 
                'i.id_imovel', 'i.referencia', 'n.id_tipo_negociacao',
                'v.preco_locacao', 'v.preco_venda', 'v.valor_negociacao',
                'espc.area_total', 'espc.qtd_banheiros', 
                'espc.qtd_vagas_garagem As qtd_vagas', 'espc.qtd_quartos',  
                'b.nome_bairro AS bairro', 
                'c.nome_cidade AS cidade')
            );
            $this->db->from('tb_imoveis AS i');
            $this->db->join('tb_negociacao AS n','i.id_imovel=n.id_imovel');
            $this->db->join('tb_especificacoes AS espc','i.id_especificacoes=espc.id_especificacao');
            $this->db->join('tb_enderecos AS e','i.id_imovel=e.id_imovel');
            $this->db->join('tb_bairros AS b','e.id_bairro=b.id_bairro');
            $this->db->join('tb_cidades AS c','e.id_cidade=c.id_cidade');
            $this->db->join('tb_valores_imovel AS v','n.id_valores_imovel=v.id_valor_imovel');
            $this->db->where('i.id_imovel', $id_imovel);

            return $this->db->get()->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCA IMOVEL CARD INFO ' . $e->getMessage());

            return array();
        }
        return array();
    }

    /*
     * _BUSCAR_IMOVEIS_FILTRADO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [INT] - 
    */
    private function _buscar_imoveis_filtrado(
        $id_tipo_negociacao=0, $ids_tipos_imoveis=array(),

        $qtd=0, $inicio=0,

        $cidade=1, $bairros=array(), 
        $referencia=NULL, $condominio=NULL, $tipos_condominios=array(), 

        $filtro_ordenacao=NULL, 

        $filtro_menor_preco=0, $filtro_maior_preco=0,
        $filtro_qtd_quartos=0, $filtro_qtd_banheiros=0, $filtro_qtd_vagas=0, 
        $filtro_menor_area=0, $filtro_maior_area=0,
        $ids_nao_gostei=array(),
        $prioridade_baggio=TRUE) 
    {
        try {
            $this->db->select( array(
                'i.id_imovel', 'i.referencia', 'i.titulo', 
                'i.descricao', 'i.observacao',
                'imo.referencia AS referencia_imobiliaria', 
                'imo.nome_imobiliaria',
                'espc.area_total AS area_total', 
                'espc.qtd_banheiros AS qtd_banheiros',
                'espc.qtd_vagas_garagem AS qtd_vagas', 
                'espc.qtd_quartos AS qtd_quartos', 
                'v.valor_condominio AS valor_condominio',
                'v.valor_aluguel AS valor_aluguel', 
                'v.valor_bruto AS valor_venda',
                'v.valor_negociacao AS valor_negociacao', 
                'v.preco_venda', 'v.preco_locacao', 
                'b.nome_bairro AS bairro', 'c.nome_cidade AS cidade',
                'n.id_tipo_negociacao AS id_tipo_negociacao',
                'tn.nome_tipo AS tipo_negociacao')
            );
            $this->db->from('tb_imoveis AS i');
            $this->db->join('tb_imobiliaria AS imo','i.id_imobiliaria=imo.id_imobiliaria');
            $this->db->join('tb_enderecos AS e','i.id_imovel=e.id_imovel');
            $this->db->join('tb_imoveis_sub_tipos AS ist','i.id_imovel=ist.id_imovel', 'left');
            $this->db->join('tb_especificacoes AS espc','i.id_especificacoes=espc.id_especificacao', 'left');
            $this->db->join('tb_negociacao AS n','i.id_imovel=n.id_imovel');
            $this->db->join('tb_tipo_negociacao AS tn','n.id_tipo_negociacao=tn.id_tipo_negociacao');
            $this->db->join('tb_valores_imovel AS v','i.id_imovel=v.id_imovel');
            $this->db->join('tb_cidades AS c','e.id_cidade=c.id_cidade');
            $this->db->join('tb_bairros AS b','e.id_bairro=b.id_bairro', 'left');
            $this->db->join('tb_endereco_complemento AS com','e.id_complemento=com.id_complemento', 'left');
            /* TIPO DE NEGOCIACAO E IMOBILIARIA */
            if ($id_tipo_negociacao != 0) {
                $this->db->where('n.id_tipo_negociacao', $id_tipo_negociacao);
            }
            /* SUB TIPOS DE IMOVEIS */
            if (!empty($ids_tipos_imoveis)) {
                $this->db->where_in('ist.id_sub_tipo', $ids_tipos_imoveis);
            }
            /* CIDADE */
            if ($cidade != NULL) {
                $this->db->where('c.id_cidade', $cidade);
            }
            /* BAIRROS */
            if (!empty($bairros)) {
                $this->db->where_in('b.id_bairro', $bairros);
            }
            /* REFERENCIA */
            if ($referencia != NULL) {
                $this->db->like(
                    'REPLACE(LOWER(i.referencia), ".","")', 
                    str_replace('.', '', strtolower($referencia)) 
                );
            }
            /* CONDOMINIO */
            if ($condominio != NULL) {
                $this->db->like('LOWER(com.nome_condominio)', strtolower($condominio));

                if (!empty($tipos_condominios)) {
                    $this->db->where_in('ist.id_sub_tipo', $tipos_condominios);
                }
            }
            /* FILTROS - PRECO */
            if ($filtro_menor_preco > 0) {
                $filtro_menor_preco = str_replace(".", "", substr( $filtro_menor_preco, 0, -3 ));

                if ($filtro_maior_preco > 0) {
                    $this->db->where("(0 + v.valor_negociacao) > (0 + $filtro_menor_preco)", NULL, FALSE);
                } else {
                    $this->db->where("(0 + v.valor_negociacao) >= (0 + $filtro_menor_preco)", NULL, FALSE);
                }
            }
            if ($filtro_maior_preco > 0) {
                $filtro_maior_preco = str_replace(".", "", substr( $filtro_maior_preco, 0, -3 ));

                $this->db->where("(0 + v.valor_negociacao) <= (0 + $filtro_maior_preco)", NULL, FALSE);
            }
            /* FILTROS - QUARTOS */
            if ($filtro_qtd_quartos > 0) {
                $this->db->group_start();
                $this->db->where('espc.qtd_quartos >=', $filtro_qtd_quartos);
                $this->db->where('espc.qtd_suites >=', $filtro_qtd_quartos);
                $this->db->group_end();
            }
            /* FILTROS - BANHEIROS */
            if ($filtro_qtd_banheiros > 0) {
                $this->db->where('espc.qtd_banheiros >=', $filtro_qtd_banheiros);
            }
            /* FILTROS - VAGAS */
            if ($filtro_qtd_vagas > 0) {
                $this->db->where('espc.qtd_vagas_garagem >=', $filtro_qtd_vagas);
            }
            /* FILTROS - AREA */
            if ($filtro_menor_area > 0) {
                $this->db->where('espc.area_total >=', $filtro_menor_area);
            }
            if ($filtro_maior_area > 0) {
                $this->db->where('espc.area_total <=', $filtro_maior_area);
            }
            // NAO GOSTEI
            if (!empty($ids_nao_gostei)) {
                $this->db->where_not_in('i.id_imovel', $ids_nao_gostei);
            }
            // ORDENACAO
            if ($prioridade_baggio) {
                $this->db->order_by("FIELD(`i`.`id_imobiliaria`, 1) DESC", NULL, FALSE);
            }

            if ($filtro_ordenacao != NULL) {
                switch ($filtro_ordenacao) {
                    case 1:
                    break;
                    /* POR MENOR VALOR */
                    case 2:

                        $this->db->order_by("CAST(`v`.`valor_negociacao` AS DECIMAL(10,2)) ASC", NULL, FALSE);
                    break;
                    /* POR MAIOR VALOR */
                    case 3:
                        $this->db->order_by("CAST(`v`.`valor_negociacao` AS DECIMAL(10,2)) DESC", NULL, FALSE);
                    break;
                    /* POR MENOR AREA */
                    case 4:
                        $this->db->order_by("CAST(`espc`.`area_total` AS DECIMAL(10,2)) ASC", NULL, FALSE);
                    break;
                    /* POR MAIOR AREA */
                    case 5:
                        $this->db->order_by("CAST(`espc`.`area_total` AS DECIMAL(10,2)) DESC", NULL, FALSE);
                    break;
                    default:
                        // $this->db->order_by('i.id_imovel', 'RANDOM');
                        $this->db->order_by('i.referencia', 'ASC');
                    break;
                }
            }
            // QUANTIFICADOR
            if($qtd > 0) {
                $this->db->limit($qtd, $inicio);
            }

            $this->db->group_by('i.id_imovel');

            return $this->db->get()->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR IMOVEIS FILTRADO - ' . $e->getMessage());
            
            return array();
        }
        return array();
    }

    /*
     * _QUANTIFICAR_IMOVEIS_BUSCA
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    private function _quantificar_imoveis_busca(
        $id_tipo_negociacao=0, 
        $ids_tipos_imoveis=array(),
        $cidade=1, $bairros=array(), 
        $referencia=NULL, $condominio=NULL, $tipos_condominios=array(), 
        $filtro_menor_preco=0, $filtro_maior_preco=0,
        $filtro_qtd_quartos=0, $filtro_qtd_banheiros=0, $filtro_qtd_vagas=0, 
        $filtro_menor_area=0, $filtro_maior_area=0,
        $ids_nao_gostei=array(),
        $prioridade_baggio=TRUE) 
    {
        try {
            $this->db->select( array(
                'i.id_imovel', 'i.referencia', 'i.titulo', 
                'i.descricao', 'i.observacao',
                'imo.referencia AS referencia_imobiliaria', 
                'imo.nome_imobiliaria',
                'espc.area_total AS area_total', 
                'espc.qtd_banheiros AS qtd_banheiros',
                'espc.qtd_vagas_garagem AS qtd_vagas', 
                'espc.qtd_quartos AS qtd_quartos', 
                'v.valor_condominio AS valor_condominio',
                'v.valor_aluguel AS valor_aluguel', 
                'v.valor_bruto AS valor_venda',
                'v.valor_negociacao AS valor_negociacao', 
                'v.preco_venda', 'v.preco_locacao', 
                'b.nome_bairro AS bairro', 'c.nome_cidade AS cidade',
                'n.id_tipo_negociacao AS id_tipo_negociacao',
                'tn.nome_tipo AS tipo_negociacao')
            );
            $this->db->from('tb_imoveis AS i');
            $this->db->join('tb_imobiliaria AS imo','i.id_imobiliaria=imo.id_imobiliaria');
            $this->db->join('tb_enderecos AS e','i.id_imovel=e.id_imovel');
            $this->db->join('tb_imoveis_sub_tipos AS ist','i.id_imovel=ist.id_imovel', 'left');
            $this->db->join('tb_especificacoes AS espc','i.id_especificacoes=espc.id_especificacao', 'left');
            $this->db->join('tb_negociacao AS n','i.id_imovel=n.id_imovel');
            $this->db->join('tb_tipo_negociacao AS tn','n.id_tipo_negociacao=tn.id_tipo_negociacao');
            $this->db->join('tb_valores_imovel AS v','i.id_imovel=v.id_imovel');
            $this->db->join('tb_cidades AS c','e.id_cidade=c.id_cidade');
            $this->db->join('tb_bairros AS b','e.id_bairro=b.id_bairro', 'left');
            $this->db->join('tb_endereco_complemento AS com','e.id_complemento=com.id_complemento', 'left');
            /* TIPO DE NEGOCIACAO E IMOBILIARIA */
            if ($id_tipo_negociacao != 0) {
                $this->db->where('n.id_tipo_negociacao', $id_tipo_negociacao);
            }
            /* SUB TIPOS DE IMOVEIS */
            if (!empty($ids_tipos_imoveis)) {
                $this->db->where_in('ist.id_sub_tipo', $ids_tipos_imoveis);
            }
            /* CIDADE */
            if ($cidade != NULL) {
                $this->db->where('c.id_cidade', $cidade);
            }
            /* BAIRROS */
            if (!empty($bairros)) {
                $this->db->where_in('b.id_bairro', $bairros);
            }
            /* REFERENCIA */
            if ($referencia != NULL) {
                $this->db->like(
                    'REPLACE(LOWER(i.referencia), ".","")', 
                    str_replace('.', '', strtolower($referencia)) 
                );
            }
            /* CONDOMINIO */
            if ($condominio != NULL) {
                $this->db->like('LOWER(com.nome_condominio)', strtolower($condominio));

                if (!empty($tipos_condominios)) {
                    $this->db->where_in('ist.id_sub_tipo', $tipos_condominios);
                }
            }
            /* FILTROS - PRECO */
            if ($filtro_menor_preco > 0) {
                $filtro_menor_preco = str_replace(".", "", substr( $filtro_menor_preco, 0, -3 ));

                if ($filtro_maior_preco > 0) {
                    $this->db->where("(0 + v.valor_negociacao) > (0 + $filtro_menor_preco)", NULL, FALSE);
                } else {
                    $this->db->where("(0 + v.valor_negociacao) >= (0 + $filtro_menor_preco)", NULL, FALSE);
                }
            }
            if ($filtro_maior_preco > 0) {
                $filtro_maior_preco = str_replace(".", "", substr( $filtro_maior_preco, 0, -3 ));

                $this->db->where("(0 + v.valor_negociacao) <= (0 + $filtro_maior_preco)", NULL, FALSE);
            }
            /* FILTROS - QUARTOS */
            if ($filtro_qtd_quartos > 0) {
                $this->db->group_start();
                $this->db->where('espc.qtd_quartos >=', $filtro_qtd_quartos);
                $this->db->where('espc.qtd_suites >=', $filtro_qtd_quartos);
                $this->db->group_end();
            }
            /* FILTROS - BANHEIROS */
            if ($filtro_qtd_banheiros > 0) {
                $this->db->where('espc.qtd_banheiros >=', $filtro_qtd_banheiros);
            }
            /* FILTROS - VAGAS */
            if ($filtro_qtd_vagas > 0) {
                $this->db->where('espc.qtd_vagas_garagem >=', $filtro_qtd_vagas);
            }
            /* FILTROS - AREA */
            if ($filtro_menor_area > 0) {
                $this->db->where('espc.area_total >=', $filtro_menor_area);
            }
            if ($filtro_maior_area > 0) {
                $this->db->where('espc.area_total <=', $filtro_maior_area);
            }
            // NAO GOSTEI
            if (!empty($ids_nao_gostei)) {
                $this->db->where_not_in('i.id_imovel', $ids_nao_gostei);
            }

            $this->db->group_by('i.id_imovel');

            return $this->db->count_all_results();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - QUANTIFICAR IMOVEIS BUSCA - ' . $e->getMessage());
            
            return array();
        }
        return array();
    }

}
