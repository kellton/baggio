<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * MATERIAL_APOIO_MODEL
 * -------------------------------------------------------------------
 * DESCRICAO : 
 * -------------------------------------------------------------------
 * CARREGA : 
 * - N/A
 */
class MaterialApoioModel extends CI_Model {

    /* ****************************************************** */
    /* ********************** INSERCAO ********************** */
    /* ****************************************************** */
    /*
     * INSERIR_MATERIAL
     * ---------------------------------------
     * 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [INT] - 
    */
    public function inserir_material($dados=array()) {
        try {
            if (!empty($dados)) {
                $this->db->insert("tb_material_apoio", $dados);
                return TRUE;
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - INSERIR MATERIAL - ' . $e->getMessage());
            return FALSE;
        }
        return FALSE;
    }

    /*
     * INSERIR_MATERIAIS
     * ---------------------------------------
     * 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [INT] - 
    */
    public function inserir_materiais($dados=array()) {
        try {
            if (!empty($dados)) {
                $this->db->insert_batch("tb_material_apoio", $dados);
                return TRUE;
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - INSERIR MATERIAIS - ' . $e->getMessage());
            return FALSE;
        }
        return FALSE;
    }

    /* ****************************************************** */
    /* ********************** EXCLUSAO ********************** */
    /* ****************************************************** */
    /*
     * EXCLUIR_MATERIAL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function excluir_material($id_negociacao=0) {
        try {
            $this->db->where('id_negociacao', $id_negociacao);
            $this->db->delete('tb_material_apoio');

            return TRUE;
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - EXCLUIR MATERIAL - ' . $e->getMessage());
            return FALSE;
        }
        return FALSE;
    }

    /*
     * EXCLUIR_MATERIAL_APOIO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function excluir_material_apoio($id_imovel=0) {
        try {
            $this->db->query(
                "DELETE 'ma' 
                    FROM    'tb_material_apoio' AS 'ma' 
                    JOIN    'tb_negociacao' AS 'n'
                    WHERE   'ma'.'id_negociacao' = 'n'.'id_negociacao'
                    AND     'n'.'id_imovel' = $id_imovel;
            ");
            return TRUE;
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - EXCLUIR MATERIAL APOIO - ' . $e->getMessage());
            return FALSE;
        }
        return FALSE;
    }

}
