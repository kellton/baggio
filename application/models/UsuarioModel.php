<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * USUARIO_MODEL
 * -------------------------------------------------------------------
 * DESCRICAO : 
 * -------------------------------------------------------------------
 * CARREGA : 
 * - N/A
 */
class UsuarioModel extends CI_Model {
    
    /* ****************************************************** */
    /* ********************** LISTAGEM ********************** */
    /* ****************************************************** */
    /*
     * LISTAR_USUARIOS
     * ---------------------------------------
     * DESCRICAO : SELECTs NA TABELA USUARIO 
     * E TABELAS LIGADAS
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function listar_usuarios($qtde=0, $inicio=0) {
        try {
            if($qtde > 0) {
                $this->db->limit($qtde, $inicio);
            }
            $this->db->from('tb_usuarios');
            $this->db->join('tb_tipo_usuario','tb_usuarios.id_tipo_usuario=tb_tipo_usuario.id_tipo_usuario');
            return $this->db->get();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR USUARIOS - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /*
     * LISTAR_TIPO_USUARIO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function listar_tipo_usuario() {
        try {
            return $this->db->get('tb_tipo_usuario')->result();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR TIPO USUARIOS - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /*
     * LISTAR_LOJAS
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function listar_lojas() {
        try {
            return $this->db->get('tb_lojas')->result();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR LOJAS - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /*
     * LISTAR_CORRETORES
     * ---------------------------------------
     * DESCRICAO : SELECTs NA TABELA USUARIO 
     * E TABELAS LIGADAS
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function listar_corretores() {
        try {
            $this->db->from('tb_usuarios AS u');
            $this->db->join('tb_tipo_usuario AS tu','u.id_tipo_usuario=tu.id_tipo_usuario', 'left');
            $this->db->where('u.id_tipo_usuario', 4);
            return $this->db->get()->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR CORRETORES - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /*
     * LISTAR_CORRETORES_NOME
     * ---------------------------------------
     * DESCRICAO : SELECTs NA TABELA USUARIO 
     * E TABELAS LIGADAS
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function listar_corretores_nome() {
        try {
            $this->db->select('u.id_usuario, u.nome_usuario');
            $this->db->from('tb_usuarios AS u');
            $this->db->join('tb_tipo_usuario AS tu','u.id_tipo_usuario=tu.id_tipo_usuario', 'left');
            $this->db->where('u.id_tipo_usuario', 4);
            return $this->db->get()->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR CORRETORES NOME - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /*
     * LISTAR_PROPRIETARIOS
     * ---------------------------------------
     * DESCRICAO : SELECTs NA TABELA USUARIO 
     * E TABELAS LIGADAS
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function listar_proprietarios() {
        try {
            $this->db->from('tb_clientes AS c');
            $this->db->join('tb_tipo_cliente AS tc','c.id_tipo_cliente=tc.id_tipo_cliente', 'left');
            $this->db->join('tb_usuarios AS u','c.id_corretor=u.id_usuario', 'left');
            $this->db->where('c.id_tipo_cliente', 1);
            return $this->db->get()->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR PROPRIETARIOS - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /*
     * LISTAR_PROPRIETARIOS_NOME
     * ---------------------------------------
     * DESCRICAO : SELECTs NA TABELA USUARIO 
     * E TABELAS LIGADAS
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function listar_proprietarios_nome($como_objeto=FALSE) {
        try {
            $this->db->select('c.id_cliente, c.nome_cliente');
            $this->db->from('tb_clientes AS c');
            $this->db->join('tb_tipo_cliente AS tc','c.id_tipo_cliente=tc.id_tipo_cliente', 'left');
            $this->db->join('tb_usuarios AS u','c.id_corretor=u.id_usuario', 'left');
            $this->db->where('c.id_tipo_cliente', 1);
            if ($como_objeto) {
                return $this->db->get()->result();
            }
            return $this->db->get()->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR PROPRIETARIOS NOME - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /* **************************************************** */
    /* ********************** BUSCAS ********************** */
    /* **************************************************** */
    /*
     * BUSCAR_USUARIO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function buscar_usuario($id_usuario) {
        try {
            $this->db->from('tb_usuarios');
            $this->db->where('id_usuario',$id_usuario);
            return $this->db->get()->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - PEGAR USUARIO - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /* ******************************************************* */
    /* ********************** INSERCOES ********************** */
    /* ******************************************************* */
    /*
     * ADICIONAR_USUARIO
     * ---------------------------------------
     * DESCRICAO : INSERÇÕES NA TABELA USUARIO
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function adicionar_usuario($dados) {
        try {
            $this->db->insert('tb_usuarios',$dados);
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - ADICIONAR USUARIO - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }

    /*
     * EDITAR_USUARIO
     * ---------------------------------------
     * DESCRICAO : ATUALIZAÇÕES NA TABELA 
     * USUARIO
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function editar_usuario($dados,$id_usuario) {
        try {
            $this->db->where('id_usuario',$id_usuario);
            $this->db->update('tb_usuarios',$dados);
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - EDITAR USUARIO  - ' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }

    /* ******************************************************* */
    /* ********************** EXCLUSOES ********************** */
    /* ******************************************************* */
    
}
