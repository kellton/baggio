<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * ENDERECO_MODEL
 * -------------------------------------------------------------------
 * DESCRICAO : 
 * -------------------------------------------------------------------
 * CARREGA : 
 * - N/A
 */
class EnderecoModel extends CI_Model {

    /* ****************************************************** */
    /* ********************** LISTAGEM ********************** */
    /* ****************************************************** */
    /*
     * LISTAR_ENDERECOS_IMOVEL
     * ---------------------------------------
     * DESCRICAO : Retorna a lista de enderecos
     * de um imovel de id {id_imovel}.
     * ---------------------------------------
     * PARAMETROS : $ID_IMOVEL [INT]
     * ---------------------------------------
     * RETORNO : [ARRAY] - Lista de enderecos
    */
    public function listar_enderecos_imovel($id_imovel=0) {
        try {
            $this->db->select(
                'end.id_endereco','end.id_imovel','c.nome_cidade AS cidade',
                'e.sigla_estado AS estado','b.nome_bairro AS bairro',
                'co.descricao AS complemento','co.numero_apartamento AS numero',
                'co.bloco','co.sala','co.nome_edificio AS edificio',
                'co.nome_condominio AS condominio','co.imediacoes',
                'co.entre_ruas','l.latitude AS lat','l.longitude AS lng',
                'end.cep','end.logradouro','end.numero'
            );
            $this->db->from('tb_enderecos AS end');
            $this->db->join('tb_cidades AS c','end.id_cidade=c.id_cidade', 'left');
            $this->db->join('tb_estados AS e','c.id_estado=e.id_estado', 'left');
            $this->db->join('tb_bairros AS b','end.id_bairro=b.id_bairro', 'left');
            $this->db->join('tb_complementos AS co','end.id_complemento=co.id_complemento', 'left');
            $this->db->join('tb_lat_long AS l','end.id_lat_long=l.id_lat_long', 'left');
            $this->db->where('id_imovel', $id_imovel);
            return $this->db->get();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR ENDERECOS IMOVEL - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /*
     * LISTAR_AUX_BAIRROS
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function listar_aux_bairros() {
        try {
            $this->db->from('tb_aux_bairros AS aux');
            $this->db->join('tb_bairros AS b', 'aux.id_bairro=b.id_bairro', 'left');
            $this->db->join('tb_cidades AS c', 'aux.id_cidade=c.id_cidade', 'left');
            return $this->db->get()->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR AUX BAIRROS - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /*
     * PAGINAR_AUX_BAIRROS
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function paginar_aux_bairros($qtd=10, $inicio=0) {
        try {
            $this->db->from('tb_aux_bairros AS aux');
            $this->db->join('tb_bairros AS b', 'aux.id_bairro=b.id_bairro', 'left');
            $this->db->join('tb_cidades AS c', 'aux.id_cidade=c.id_cidade', 'left');

            if($qtd > 0) {
                $this->db->limit($qtd, $inicio);
            }
            return $this->db->get()->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - PAGINAR AUX BAIRROS - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /*
     * LISTAR_BAIRROS_ID
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function listar_bairros_id() {
        try {
            $this->db->select( array(
                'b.id_bairro', 
                'CONCAT(b.nome_bairro, " - ", c.nome_cidade)'
            ));
            $this->db->from('tb_bairros AS b');
            $this->db->join('tb_cidades AS c', 'b.id_cidade=c.id_cidade');

            $this->db->order_by('b.nome_bairro', 'ASC');

            return $this->db->get()->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR BAIRROS ID - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /*
     * LISTAR_BAIRROS
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function listar_bairros() {
        try {
            $this->db->from('tb_bairros AS b');
            $this->db->join('tb_cidades AS c','b.id_cidade=c.id_cidade', 'left');
            return $this->db->get()->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR BAIRROS - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /*
     * LISTAR_BAIRROS_COM_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function listar_bairros_com_imovel() {
        try {
            $this->db->select('b.*');
            $this->db->from('tb_enderecos AS e');
            $this->db->join('tb_bairros AS b', 'e.id_bairro=b.id_bairro');
            $this->db->where('e.id_imovel !=', NULL);
            $this->db->group_by('b.nome_bairro');
            
            return $this->db->get()->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR BAIRROS COM IMOVEL - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /*
     * LISTAR_CIDADES
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function listar_cidades() {
        try {
            $this->db->from('tb_cidades AS c');
            $this->db->join('tb_estados AS e','c.id_estado=e.id_estado');
            return $this->db->get()->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR CIDADES - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /*
     * LISTAR_CIDADES_COM_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function listar_cidades_com_imovel() {
        try {
            $this->db->select('c.*');
            $this->db->from('tb_enderecos AS e');
            $this->db->join('tb_cidades AS c', 'e.id_cidade=c.id_cidade');
            $this->db->where('e.id_imovel !=', NULL);
            $this->db->group_by('c.nome_cidade');

            return $this->db->get()->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR CIDADES COM IMOVEL - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /*
     * LISTAR_CIDADES_COM_IMOVEL_E_BAIRRO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function listar_cidades_com_imovel_e_bairro() {
        try {
            $this->db->select('c.*');
            $this->db->from('tb_enderecos AS e');
            $this->db->join('tb_cidades AS c', 'e.id_cidade=c.id_cidade');
            $this->db->join('tb_bairros AS b', 'e.id_bairro=b.id_bairro');
            $this->db->where('e.id_imovel !=', NULL);
            $this->db->group_by('c.nome_cidade');

            return $this->db->get()->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR CIDADES COM IMOVEL E BAIRRO - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /*
     * LISTAR_CIDADES_ESTADO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function listar_cidades_estado() {
        try {
            $this->db->select('c.id_cidade, CONCAT(c.nome_cidade, " - ", e.sigla_estado) As nome_cidade', FALSE);
            $this->db->from('tb_cidades AS c');
            $this->db->join('tb_estados AS e','c.id_estado=e.id_estado');

            $this->db->order_by('c.nome_cidade', 'ASC');

            return $this->db->get()->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR CIDADES ESTADO - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /*
     * LISTAR_ESTADOS
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function listar_estados() {
        try {
            $this->db->from('tb_estados');
            return $this->db->get()->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR ESTADO - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /*
     * LISTAR_ESTADOS_SIGLA
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function listar_estados_SIGLA() {
        try {
            $this->db->select('e.id_estado, CONCAT(e.nome_estado, " - ", e.sigla_estado)');
            $this->db->from('tb_estados AS e');
            
            $this->db->order_by('e.sigla_estado', 'ASC');

            return $this->db->get()->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR ESTADO SIGLA - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /* ****************************************************** */
    /* *********************** BUSCAS *********************** */
    /* ****************************************************** */
    /*
     * BUSCAR_ID_BAIRRO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function buscar_id_bairro($bairro="") {
        $id_bairro = NULL;
        try {
            $this->db->select('b.id_bairro');
            $this->db->from('tb_bairros AS b');
            $this->db->like('LOWER(b.nome_bairro)', strtolower($bairro));
            
            $row_array = $this->db->get()->row_array();

            if (!empty($row_array)) {
                $id_bairro = intval( $row_array["id_bairro"] );
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR ID BAIRRO - ' . $e->getMessage());
            return $id_bairro;
        }
        return $id_bairro;
    }

    /*
     * BUSCAR_ID_CIDADE_BAIRRO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function buscar_id_cidade_bairro($id_bairro=0) {
        $id_cidade = NULL;
        try {
            $this->db->select('b.id_cidade');
            $this->db->from('tb_bairros AS b');
            $this->db->where('b.id_bairro', $id_bairro);
            
            $row_array = $this->db->get()->row_array();

            if (!empty($row_array)) {
                $id_cidade = intval( $row_array["id_cidade"] );
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR ID CIDADE BAIRRO - ' . $e->getMessage());
            return $id_cidade;
        }
        return $id_cidade;
    }

    /*
     * BUSCAR_AUX_BAIRRO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function buscar_aux_bairro($bairro="", $id_cidade=NULL) {
        try {
            $this->db->select( array(
                'aux.id_bairro AS id_bairro',
                'aux.id_aux_bairro AS id_aux_bairro'
            ));
            $this->db->from('tb_aux_bairros AS aux');
            $this->db->like('aux.nome_aux', lower_acentuada($bairro));

            if ($id_cidade != NULL) {
                $this->db->where('aux.id_cidade', $id_cidade);
            }
            return $this->db->get()->row_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR AUX BAIRRO - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /*
     * BUSCAR_NOME_AUX_BAIRRO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function buscar_nome_aux_bairro($id_aux_bairro=NULL) {
        try {
            $this->db->select('aux.nome_aux');
            $this->db->from('tb_aux_bairros AS aux');
            $this->db->where('aux.id_aux_bairro', $id_aux_bairro);

            $row_array = $this->db->get()->row_array();

            if (!empty($row_array)) {
               return $row_array["nome_aux"];
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR NOME AUX BAIRRO - ' . $e->getMessage());
            return "";
        }
        return "";
    }

    /*
     * BUSCAR_ID_CIDADE
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function buscar_id_cidade($cidade="", $id_estado=-1) {
        $id_cidade = NULL;
        try {
            $this->db->select('c.id_cidade');
            $this->db->from('tb_cidades AS c');
            $this->db->like('LOWER(c.nome_cidade)', strtolower($cidade));
            $this->db->where('c.id_estado', $id_estado);
            
            $row_array = $this->db->get()->row_array();

            if (!empty($row_array)) {
                $id_cidade = intval( $row_array["id_cidade"] );
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR ID CIDADE - ' . $e->getMessage());
            return $id_cidade;
        }
        return $id_cidade;
    }

    /*
     * BUSCAR_ID_ESTADO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function buscar_id_estado($sigla="") {
        $id_estado = NULL;
        try {
            $this->db->select('e.id_estado');
            $this->db->from('tb_estados AS e');
            $this->db->like('LOWER(e.sigla_estado)', strtolower($sigla));
            
            $row_array = $this->db->get()->row_array();

            if (!empty($row_array)) {
                $id_estado = intval( $row_array["id_estado"] );
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR ID ESTADO - ' . $e->getMessage());
            return $id_estado;
        }
        return $id_estado;
    }

    /*
     * BUSCAR_BAIRROS_POR_CIDADE
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function buscar_bairros_por_cidade($id_cidade=NULL) {
        try {
            if ($id_cidade != NULL) {
                $this->db->select(array(
                    'b.id_bairro', 'b.nome_bairro'
                ));
                $this->db->from('tb_bairros AS b');
                $this->db->where('b.id_cidade', $id_cidade);
                $this->db->order_by('b.nome_bairro', 'ASC');
                
                return $this->db->get()->result_array();
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR BAIRROS POR CIDADE - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /* ****************************************************** */
    /* ********************** INSERCAO ********************** */
    /* ****************************************************** */
    /*
     * INSERIR_AUX_BAIRRO
     * ---------------------------------------
     * 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [INT] - 
    */
    public function inserir_aux_bairro($dados=array()) {
        try {
            if (!empty($dados)) {
                $this->db->insert("tb_aux_bairros", $dados);

                return $this->db->insert_id();
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - INSERIR AUX BAIRRO - ' . $e->getMessage());
            return NULL;
        }
        return NULL;
    }

    /*
     * INSERIR_AUX_BAIRROS
     * ---------------------------------------
     * 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [INT] - 
    */
    public function inserir_aux_bairros($dados=array()) {
        $ids_inseridos = array();
        try {
            if (!empty($dados)) {
                foreach ($dados as $dado) {
                    array_push(
                        $ids_inseridos, 
                        $this->inserir_aux_bairro($dado)
                    );
                }
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - INSERIR AUX BAIRROS - ' . $e->getMessage());
            return $ids_inseridos;
        }
        return $ids_inseridos;
    }

    /*
     * INSERIR_BAIRRO
     * ---------------------------------------
     * 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [INT] - 
    */
    public function inserir_bairro($dados=array()) {
        try {
            if (!empty($dados)) {
                $this->db->insert("tb_bairros", $dados);

                return $this->db->insert_id();
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - INSERIR BAIRRO - ' . $e->getMessage());
            return NULL;
        }
        return NULL;
    }

    /*
     * INSERIR_CIDADE
     * ---------------------------------------
     * 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [INT] -
    */
    public function inserir_cidade($dados=array()) {
        try {
            if (!empty($dados)) {
                $this->db->insert("tb_cidades", $dados);
                return TRUE;
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - INSERIR CIDADE - ' . $e->getMessage());
            return FALSE;
        }
        return FALSE;
    }

    /*
     * INSERIR_ESTADO
     * ---------------------------------------
     * 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [INT] -
    */
    public function inserir_estado($dados=array()) {
        try {
            if (!empty($dados)) {
                $this->db->insert("tb_estados", $dados);
                return TRUE;
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - INSERIR ESTADO - ' . $e->getMessage());
            return FALSE;
        }
        return FALSE;
    }

    /*
     * INSERIR_COMPLEMENTO
     * ---------------------------------------
     * 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [INT] - 
    */
    public function inserir_complemento($dados=array()) {
        try {
            if (!empty($dados)) {
                $this->db->insert("tb_endereco_complemento", $dados);
                return $this->db->insert_id();
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - INSERIR COMPLEMENTO - ' . $e->getMessage());
            return NULL;
        }
        return NULL;
    }

    /*
     * INSERIR_LAT_LONG
     * ---------------------------------------
     * 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [INT] -
    */
    public function inserir_lat_long($dados=array()) {
        try {
            if (!empty($dados)) {
                $this->db->insert("tb_lat_long", $dados);
                return $this->db->insert_id();
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - INSERIR LAT LONG - ' . $e->getMessage());
            return NULL;
        }
        return NULL;
    }

    /* ****************************************************** */
    /* ********************** EXCLUSAO ********************** */
    /* ****************************************************** */
    /*
     * EXCLUIR_AUX_BAIRRO
     * ---------------------------------------
     * 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [INT] - 
    */
    public function excluir_aux_bairro($id_aux_bairro=0) {
        try {
            $this->db->where("id_aux_bairro", $id_aux_bairro);
            $this->db->delete("tb_aux_bairros");
            return TRUE;
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - EXCLUIR AUX BAIRRO - ' . $e->getMessage());
            return FALSE;
        }
        return FALSE;
    }

    /*
     * EXCLUIR_BAIRRO
     * ---------------------------------------
     * 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [INT] - 
    */
    public function excluir_bairro($id_bairro=0) {
        try {
            $this->db->where("id_bairro", $id_bairro);
            $this->db->delete("tb_bairros");
            return TRUE;
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - EXCLUIR BAIRRO - ' . $e->getMessage());
            return FALSE;
        }
        return FALSE;
    }

    /*
     * EXCLUIR_CIDADE
     * ---------------------------------------
     * 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [INT] - 
    */
    public function excluir_cidade($id_cidade=0) {
        try {
            $this->db->where("id_cidade", $id_cidade);
            $this->db->delete("tb_cidades");
            return TRUE;
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - EXCLUIR CIDADE - ' . $e->getMessage());
            return FALSE;
        }
        return FALSE;
    }

    /*
     * EXCLUIR_ESTADO
     * ---------------------------------------
     * 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [INT] - 
    */
    public function excluir_estado($id_estado=0) {
        try {
            $this->db->where("id_estado", $id_estado);
            $this->db->delete("tb_estados");
            return TRUE;
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - EXCLUIR ESTADO - ' . $e->getMessage());
            return FALSE;
        }
        return FALSE;
    }

    /*
     * EXCLUIR_LAT_LONG_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function excluir_lat_long_imovel($id_imovel=0) {
        try {
            $this->db->query(
                "DELETE `l` 
                    FROM    `tb_lat_long` AS `l` 
                    JOIN    `tb_enderecos` AS `e`
                    WHERE   `l`.`id_lat_long` = `e`.`id_lat_long`
                    AND     `e`.`id_imovel` = $id_imovel;
            ");

            return TRUE;
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - EXCLUIR LAT LONG IMOVEL - ' . $e->getMessage());
            return FALSE;
        }
        return FALSE;
    }

    /*
     * EXCLUIR_COMPLEMENTO_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function excluir_complemento_imovel($id_imovel=0) {
        try {
            $this->db->query(
                "DELETE `c` 
                    FROM    `tb_endereco_complemento` AS `c` 
                    JOIN    `tb_enderecos` AS `e`
                    WHERE   `c`.`id_complemento` = `e`.`id_complemento`
                    AND     `e`.`id_imovel` = $id_imovel;
            ");

            return TRUE;
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - EXCLUIR COMPLEMENTO IMOVEL - ' . $e->getMessage());
            return FALSE;
        }
        return FALSE;
    }

    /*
     * EXCLUIR_ENDERECO_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function excluir_endereco_imovel($id_imovel=0) {
        try {
            $this->db->where('id_imovel', $id_imovel);
            $this->db->delete('tb_enderecos');

            return TRUE;
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - EXCLUIR ENDERECO IMOVEL - ' . $e->getMessage());
            return FALSE;
        }
        return FALSE;
    }

    /*
     * QUANTIFICAR_AUX_BAIRROS
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function quantificar_aux_bairros() {
        try {
            return $this->db->count_all('tb_aux_bairros');
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - QUANTIFICAR AUX BAIRROS - ' . $e->getMessage());
            return 0;
        }
        return 0;
    }

}
