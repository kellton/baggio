<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * TIPO_MODEL
 * -------------------------------------------------------------------
 * DESCRICAO : 
 * -------------------------------------------------------------------
 * CARREGA : 
 * - N/A
 */
class TipoModel extends CI_Model {

    /* ****************************************************** */
    /* ********************** LISTAGEM ********************** */
    /* ****************************************************** */
    /*
     * LISTAR_TIPO_NEGOCIACOES
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function listar_tipo_negociacoes() {
        try {
            return $this->db->get('tb_tipo_negociacao')->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR TIPO NEGOCIACOES - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /*
     * LISTAR_TIPO_IMOVEIS
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function listar_tipo_imoveis() {
        try {
            return $this->db->get('tb_tipo_imovel')->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR TIPO IMOVEIS - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /*
     * LISTAR_SUBTIPO_IMOVEIS
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function listar_subtipo_imoveis() {
        try {
            return $this->db->get('tb_sub_tipo_imovel')->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR SUBTIPO IMOVEIS - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /*
     * LISTAR_DROPDOWN_SUBTIPO_IMOVEIS
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function listar_dropdown_subtipo_imoveis() {
        try {
            $this->db->select( array("id_sub_tipo_imovel", "nome_tipo") );
            return $this->db->get('tb_sub_tipo_imovel')->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR DROPDOWN SUBTIPO IMOVEIS - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /*
     * LISTAR_COMISSOES
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function listar_comissoes() {
        try {
            return $this->db->get('tb_tipo_comissao')->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR COMISSOES - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /*
     * LISTAR_PRIVACIDADES
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function listar_privacidades() {
        try {
            return $this->db->get('tb_tipo_privacidade')->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR PRIVACIDADES - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /*
     * LISTAR_TIPO_OCUPACOES
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function listar_tipo_ocupacoes() {
        try {
            return $this->db->get('tb_tipo_ocupacao')->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR TIPO OCUPACOES - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /*
     * LISTAR_TIPO_ANDARES
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function listar_tipo_andares() {
        try {
            return $this->db->get('tb_tipo_andar')->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR TIPO ANDARES - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /*
     * LISTAR_TIPO_FACES
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function listar_tipo_faces() {
        try {
            return $this->db->get('tb_tipo_face')->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR TIPO FACES - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /*
     * LISTAR_TIPO_GARAGENS
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function listar_tipo_garagens() {
        try {
            return $this->db->get('tb_tipo_garagem')->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR TIPO GARAGENS - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /*
     * LISTAR_TIPO_LOCAIS_GARAGEM
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function listar_tipo_locais_garagem() {
        try {
            return $this->db->get('tb_tipo_local_garagem')->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR TIPO LOCAIS GARAGEM - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /*
     * LISTAR_TIPO_SITUACAO_DOCUMENTAL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function listar_tipo_situacao_documental() {
        try {
            return $this->db->get('tb_tipo_situacao_documental')->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR TIPO SITUACAO DOCUMENTAL - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /*
     * LISTAR_TIPO_FINANCIAMENTOS
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function listar_tipo_financiamentos() {
        try {
            return $this->db->get('tb_tipo_financiamento')->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR TIPO FINANCIAMENTOS - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /*
     * LISTAR_TIPO_ZONEAMENTOS
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function listar_tipo_zoneamentos() {
        try {
            return $this->db->get('tb_tipo_zoneamento')->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR TIPO ZONEAMENTOS - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /*
     * LISTAR_TIPO_MATERIAIS
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function listar_tipo_materiais() {
        try {
            return $this->db->get('tb_tipo_material')->result_array();
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - LISTAR TIPO MATERIAIS - ' . $e->getMessage());
            return array();
        }
        return array();
    }

    /* ****************************************************** */
    /* *********************** BUSCAR *********************** */
    /* ****************************************************** */
    /*
     * BUSCAR_IMOVEL_SUB_TIPO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function buscar_imovel_sub_tipo($nome_tipo="") {
        try {
            if (!empty($nome_tipo)) {
                $this->db->from('tb_sub_tipo_imovel AS sti');
                $this->db->like('LOWER(nome_tipo)', strtolower($nome_tipo));

                return $this->db->get()->result_array();
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR IMOVEL SUB TIPO - ' . $e->getMessage());

            return array();
        }
        return array();
    }

    /*
     * BUSCAR_IMOVEL_SUB_TIPOS
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function buscar_imovel_sub_tipos($nomes_tipos=array(), $id_uso_tipo=1) {
        try {
            if (!empty($nomes_tipos)) {
                $this->db->from('tb_sub_tipo_imovel AS sti');
                if ($id_uso_tipo != 1) {
                    $this->db->where('sti.id_tipo_imovel', $id_uso_tipo);
                }

                $this->db->group_start();
                foreach ($nomes_tipos as $indice => $nome_tipo) {
                    if ($indice == 0) {
                        $this->db->like('LOWER(sti.nome_tipo)', lower_acentuada($nome_tipo));
                    } else {
                        $this->db->or_like('LOWER(sti.nome_tipo)', lower_acentuada($nome_tipo));
                    }
                }
                $this->db->group_end();
                return $this->db->get()->result_array();
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR IMOVEL SUB TIPOS - ' . $e->getMessage());

            return array();
        }
        return array();
    }

    /*
     * BUSCAR_ID_TIPO_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function buscar_id_tipo_imovel($nome_tipo="") {
        $id_tipo = 0;
        try {
            if (!empty($nome_tipo)) {
                $this->db->select("id_tipo_imovel");
                $this->db->from("tb_tipo_imovel");
                $this->db->like('LOWER(nome_tipo)', lower_acentuada($nome_tipo), 'after');

                $row_array = $this->db->get()->row_array();

                if (!empty($row_array)) {
                    $id_tipo = intval( $row_array["id_tipo_imovel"] );
                }
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR ID TIPO IMOVEL - PRIVATE - ' . $e->getMessage());

            return $id_tipo;
        }
        return $id_tipo;
    }

    /*
     * BUSCAR_ID_SUB_TIPO_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function buscar_id_sub_tipo_imovel($nome_tipo="") {
        $id_sub_tipo_imovel = 3;
        try {
            if (!empty($nome_tipo)) {
                $this->db->select('id_sub_tipo_imovel');
                $this->db->from('tb_sub_tipo_imovel AS sti');
                $this->db->like('LOWER(nome_tipo)', strtolower($nome_tipo));

                $result_array = $this->db->get()->result_array();
                
                if (!empty($result_array)) {
                    $id_sub_tipo_imovel = intval( $result_array[0]["id_sub_tipo_imovel"] );
                }
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR ID SUB TIPO IMOVEL - ' . $e->getMessage());

            return $id_sub_tipo_imovel;
        }
        return $id_sub_tipo_imovel;
    }

    /*
     * BUSCAR_ID_SITUACAO_DOCUMENTAL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function buscar_id_situacao_documental($nome_tipo="") {
        return $this->_buscar_id_tipo($nome_tipo, "tb_tipo_situacao_documental", "id_situacao_documental", 1);
    }

    /*
     * BUSCAR_ID_TIPO_GARAGEM
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function buscar_id_tipo_garagem($nome_tipo="") {
        return $this->_buscar_id_tipo($nome_tipo, "tb_tipo_garagem", "id_garagem", 1);
    }

    /*
     * BUSCAR_ID_TIPO_LOCAL_GARAGEM
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function buscar_id_tipo_local_garagem($nome_tipo="") {
        return $this->_buscar_id_tipo($nome_tipo, "tb_tipo_local_garagem", "id_local_garagem", 1);
    }

    /*
     * BUSCAR_ID_TIPO_FACE
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function buscar_id_tipo_face($nome_tipo="") {
        return $this->_buscar_id_tipo($nome_tipo, "tb_tipo_face", "id_tipo_face", 1);
    }

    /*
     * BUSCAR_ID_TIPO_ACAO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function buscar_id_tipo_acao($nome_tipo="") {
        return $this->_buscar_id_tipo($nome_tipo, "tb_tipo_acao", "id_tipo_acao", 1);
    }

    /*
     * BUSCAR_ID_TIPO_OCUPACAO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function buscar_id_tipo_ocupacao($nome_tipo="") {
        return $this->_buscar_id_tipo($nome_tipo, "tb_tipo_ocupacao", "id_tipo_ocupacao", 1);
    }

    /*
     * BUSCAR_ID_TIPO_FINALIDADE
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function buscar_id_tipo_finalidade($nome_tipo="") {
        return $this->_buscar_id_tipo($nome_tipo, "tb_tipo_finalidade", "id_tipo_finalidade", 1);
    }

    /*
     * BUSCAR_ID_TIPO_ANDAR
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function buscar_id_tipo_andar($nome_tipo="") {
        return $this->_buscar_id_tipo($nome_tipo, "tb_tipo_andar", "id_andar", 1);
    }

    /*
     * BUSCAR_ID_TIPO_ZONEAMENTO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function buscar_id_tipo_zoneamento($nome_tipo="") {
        return $this->_buscar_id_tipo($nome_tipo, "tb_tipo_zoneamento", "id_tipo_zoneamento", 1);
    }

    /*
     * BUSCAR_ID_TIPO_NEGOCIACAO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function buscar_id_tipo_negociacao($nome_tipo="") {
        $id_tipo_negociacao = 1;
        try {
            if (!empty($nome_tipo)) {
                $this->db->select('id_tipo_negociacao');
                $this->db->from('tb_tipo_negociacao AS n');
                $this->db->like('LOWER(nome_tipo)', strtolower($nome_tipo), 'after');

                $result_array = $this->db->get()->result_array();
                
                if (!empty($result_array)) {
                    $id_tipo_negociacao = intval( $result_array[0]["id_tipo_negociacao"] );
                }
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR ID TIPO NEGOCIACAO - ' . $e->getMessage());

            return $id_tipo_negociacao;
        }
        return $id_tipo_negociacao;
    }

    /* ****************************************************** */
    /* *********************** PRIVATE ********************** */
    /* ****************************************************** */
    /*
     * _BUSCAR_ID_TIPO - PRIVATE
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    private function _buscar_id_tipo($buscar_valor="", $nome_tabela="", $nome_id="", $valor_id=1) {
        $id_tipo = $valor_id;
        try {
            if (!empty($buscar_valor) && !empty($nome_tabela) && !empty($nome_id)) {
                $this->db->select($nome_id);
                $this->db->from($nome_tabela);
                $this->db->where('LOWER(nome_tipo)', strtolower($buscar_valor));

                $result_array = $this->db->get()->result_array();

                if (!empty($result_array)) {
                    $id_tipo = intval( $result_array[0][$nome_id] );
                }
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - BUSCAR ID TIPO - PRIVATE - ' . $e->getMessage());

            return $id_tipo;
        }
        return $id_tipo;
    }

}
