<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * XML_MODEL
 * -------------------------------------------------------------------
 * DESCRICAO : 
 * -------------------------------------------------------------------
 * CARREGA : 
 * - N/A
 */
class XmlModel extends CI_Model {

    /* ****************************************************** */
    /* ********************** INSERCAO ********************** */
    /* ****************************************************** */
    /*
     * INSERIR_INFORMACOES_XML
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [INT] - 
    */
    public function inserir_informacoes_xml($dados=array()) {
        try {
            if (!empty($dados)) {
                $this->db->insert("tb_informacoes_xml_imovel", $dados);
                return $this->db->insert_id();
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO SGBD - INSERIR INFORMACOES XML - ' . $e->getMessage());

            return -1;
        }
        return -1;
    }


    /* ****************************************************** */
    /* ********************** PRIVATES ********************** */
    /* ****************************************************** */

}
