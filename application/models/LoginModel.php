<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginModel extends CI_Model {
    public function buscar_usuario($email,$senha) {
        $this->db->from('tb_usuarios');
        $this->db->where('email',$email);
        $this->db->where('senha', $senha);
        $this->db->where('ativo', 1);
        return $this->db->get()->row();
    }
    
    public function buscar_usuario_cliente($email,$senha) {
        $this->db->from('tb_clientes');
        $this->db->where('email',$email);
        $this->db->where('senha', $senha);
        $this->db->where('ativo', 1);
        return $this->db->get()->row();
    }
}
