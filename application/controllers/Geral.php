﻿<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

use Carbon\Carbon;

/**
 * GERAL_CONTROLLER
 * -------------------------------------------------------------------
 * DESCRICAO : Classe controller de inicio da aplicacao.
 * -------------------------------------------------------------------
 * CARREGA : 
 * - N/A
 */
class Geral extends CI_Controller {

    /*
     * CONSTRUCT
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function __construct() {
        parent::__construct();

        //date_default_timezone_set();

        $this->load->model('TipoModel','m_tipos');
        $this->load->model('EnderecoModel','m_enderecos');
        $this->load->model('ClienteModel','m_cliente');
        $this->load->model('ImovelModel','m_imoveis');
        $this->load->library('encryption');
    }

    /*
     * INDEX
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function index() {
        $dados = array();

        $dados['imoveis_aluguel'] = $this->_preparar_cardboards(
                $this->m_imoveis->busca_imoveis_aluguel(
                    6,
                    [
                        "ecoville",
                        "mossunguê",
                        "mossungue",
                        "mossuguê",
                        "vila izabel",
                        "santa quitéria",
                        "santa quiteria",
                        "seminário",
                        "seminario"
                    ],
                    "",
                    "",
                    -1500,
                    0,
                    1
                ),
                2
            );
        $dados['imoveis_venda'] = $this->_preparar_cardboards(
                $this->m_imoveis->busca_imoveis_venda(
                    6,
                    [
                        "ecoville",
                        "mossunguê",
                        "mossungue",
                        "izabel",
                        "santa quitéria",
                        "santa quiteria",
                        "seminário",
                        "seminario"
                    ],
                    "",
                    "",
                    -500000,
                    0,
                    1
                ),
                1
            );
        $dados['view_busca'] = $this->_preparar_view_busca();

        $header["scripts"] = carregar_js("geral");
        $header["styles"] = carregar_css("geral");

        $this->load->view('template/header', $header);
        $this->load->view('template/menu');
        $this->load->view('template/mensagens');
        $this->load->view('home', $dados);
        $this->load->view('template/footer');
    }

    /*
     * VISUALIZAR_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function visualizar_imovel($id_imovel) {
        $imovel = $this->m_imoveis->buscar_informacao_imovel_visualizar($id_imovel);

        if ($imovel != NULL) {
            $sub_tipo = $this->m_imoveis->pesquisar_sub_tipo($imovel["id_sub_tipo"]);

            $referencia_array = explode("-", $imovel['referencia']);
            $sub_tipo = str_replace("/", "_", mb_strtolower($sub_tipo, "UTF-8"));
            $nome_bairro = str_replace("/", "_", str_replace(" ", "-", str_replace("-", "_", mb_strtolower($imovel["nome_bairro"], "UTF-8"))));
            $nome_cidade = str_replace(" ", "_", mb_strtolower($imovel["nome_cidade"], "UTF-8"));
            $referencia =  str_replace(".", "", $referencia_array[0]);

            header("Location: ".base_url("imovel/".$sub_tipo."-".$nome_bairro."-".$nome_cidade."-".$referencia));
            die();
        } else {
            $this->session->set_flashdata('param_msg_erro_cabecalho', 'Erro ao Visualizar Imóvel');
            $this->session->set_flashdata('param_msg_erro', 'Ocorreu um problema ao tentar visualizar este imóvel. Tente depois novamente.');

            $this->index();
        }
    }

    public function visualizar_imovel_referencia($url) {
        $url_array = explode("-", $url);
        $counter = count($url_array);

        $tipo = urldecode($url_array[0]);
        $referencia = urldecode($url_array[$counter-1]);
        $bairro = "";

        for ($i=1; $i<$counter-2; $i++) {
            $bairro = urldecode($bairro." ".$url_array[$i]);
        }

        $bairro = trim($bairro);

        $imovel_array=$this->m_imoveis->buscar_id_imovel($tipo, $bairro, $referencia);
        $id_imovel=$imovel_array['id_imovel'];

        $imovel = $this->m_imoveis->buscar_informacao_imovel_visualizar($id_imovel);
        $dados = array();

        if($imovel['id_tipo_negociacao'] == 2) {
            $dados["imoveis_sugeridos"] = $this->_preparar_cardboards( $this->m_imoveis->busca_imoveis_aluguel(3, $imovel['nome_bairro'], $id_imovel, $imovel['id_sub_tipo'], $imovel['valor_negociacao'], $imovel['qtd_quartos']), 2);
        } else {
            $dados["imoveis_sugeridos"] = $this->_preparar_cardboards( $this->m_imoveis->busca_imoveis_venda(3, $imovel['nome_bairro'], $id_imovel, $imovel['id_sub_tipo'], $imovel['valor_negociacao'], $imovel['qtd_quartos']), 1);
        }



        if(empty($imovel)) {
            redirect(base_url());
        }
        $imovel["imagens"] = $this->m_imoveis->buscar_todas_imagem_imovel($id_imovel);

        $dados['query'] = $imovel;
        
        if($dados['query']['id_tipo_negociacao'] == 2) {
            $this->_visualizar_imovel_aluguel($dados, $id_imovel);
        } else {
            $this->_visualizar_imovel_venda($dados, $id_imovel);
        }
    }
    
    /*
     * SOBRE_NOS
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function sobre_nos() {

        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $this->load->view('template/menu');
        $this->load->view('sobre_nos');
        $this->load->view('template/footer');
    }
    
    /*
     * ADMINISTRE_MEU_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function administre_meu_imovel() {
        $dados = array(
                'widget' => $this->recaptcha->getWidget(),
                'script' => $this->recaptcha->getScriptTag(),
        );
        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $this->load->view('template/menu');
        $this->load->view('administre_meu_imovel',$dados);
        $this->load->view('template/footer');
    }
    
    /*
     * VENDA_MEU_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function venda_meu_imovel() {
        $dados = array(
                'widget' => $this->recaptcha->getWidget(),
                'script' => $this->recaptcha->getScriptTag(),
        );
        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $this->load->view('template/menu');
        $this->load->view('venda_meu_imovel',$dados);
        $this->load->view('template/footer');
    }
    
    /*
     * DOCUMENTAÇÃO NECESSÁRIA
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function documentacao_necessaria() {

        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $this->load->view('template/menu');
        $this->load->view('documentacao_necessaria');
        $this->load->view('template/footer');
    }
    
    /*
     * CONTATO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function contato() {
        $dados = array(
                'widget' => $this->recaptcha->getWidget(),
                'script' => $this->recaptcha->getScriptTag(),
        );

        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $this->load->view('template/menu');
        $this->load->view('contato', $dados);
        $this->load->view('template/footer');
    }
    
    /*
     * CADASTRO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function cadastro() {
        $dados['corretores']=$this->m_cliente->listar_corretores_cadastro_cliente();
        $dados['widget'] = $this->recaptcha->getWidget();
        $dados['script'] = $this->recaptcha->getScriptTag();

        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $this->load->view('template/menu');
        $this->load->view('cadastro', $dados);
        $this->load->view('template/footer');
    }
    
    /*
     * AGENDAR VISITA
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function agendar_visita() {
        if(isset($_POST['url'])){ $dados['url']=$_POST['url']; }

        $dados['widget'] = $this->recaptcha->getWidget();
        $dados['script'] = $this->recaptcha->getScriptTag();

        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $this->load->view('template/menu');
        $this->load->view('agendar_visita', $dados);
        $this->load->view('template/footer');
    }
    
    /*
     * TRABALHE CONOSCO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function trabalhe_conosco($valor=NULL) {
        $dados['error'] = $valor;
        $dados['widget'] = $this->recaptcha->getWidget();
        $dados['script'] = $this->recaptcha->getScriptTag();

        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $this->load->view('template/menu');
        $this->load->view('trabalhe_conosco', $dados);
        $this->load->view('template/footer');
    }
    
    /*
     * ESQUECI SENHA
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function esqueci_senha() {
        $dados['widget'] = $this->recaptcha->getWidget();
        $dados['script'] = $this->recaptcha->getScriptTag();

        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $this->load->view('template/menu');
        $this->load->view('login/esqueci_senha', $dados);
        $this->load->view('template/footer');
    }
    
    /*
     * ALTERAÇÃO DE SENHA
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function alteracao_senha() {
        if($_GET['token']==''){
            redirect();
        }
        $dados['widget'] = $this->recaptcha->getWidget();
        $dados['script'] = $this->recaptcha->getScriptTag();

        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $this->load->view('template/menu');
        $this->load->view('login/alteracao_senha', $dados);
        $this->load->view('template/footer');
    }

    /*
     * FAVORITAR
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS :
     * ---------------------------------------
     * RETORNO :
    */
    public function favoritar() {
        $id_imovel = $this->input->post('id_imovel');
        $this->load->helper('cookie');

        $cookie = array();
        $value_cookie = $id_imovel;

        if($this->input->cookie('baggio_favoritar')) {
            $imoveis = $this->input->cookie('baggio_favoritar');
            $imoveis = "$imoveis;$id_imovel";

            $value_cookie = $imoveis;
        }

        $cookie = array(
            'name'   => 'baggio_favoritar',
            'value'  => $value_cookie,
            'expire' => CONS_BAGGIO_FAVORITO_COOKIE_LIFE,
            'secure' => CONS_BAGGIO_FAVORITO_COOKIE_SECURE
        );

        $this->input->set_cookie($cookie);

        header('Content-Type: application/json');

        echo json_encode($id_imovel);
    }

    /*
     * desgostar
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS :
     * ---------------------------------------
     * RETORNO :
    */
    public function desgostar() {
        $id_imovel = $this->input->post('id_imovel');

        if($this->input->cookie('baggio_favoritar')) {
            $imoveis = $this->input->cookie('baggio_favoritar');
            $imoveis = "$imoveis;$id_imovel";

            $value_cookie = $imoveis;
        }

        $this->input->set_cookie($cookie);

        header('Content-Type: application/json');

        echo json_encode($id_imovel);
    }
    
    /*
     * LISTAR IMOVEIS FAVORITOS
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS :
     * ---------------------------------------
     * RETORNO :
    */
    public function listagem_imoveis_favoritos() {
        $dados['imoveis'] = $this->_preparar_imoveis_favoritos();

        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $this->load->view('template/menu');
        $this->load->view('template/mensagens');
        $this->load->view('listagem_favoritos', $dados);
        $this->load->view('template/footer');
    }
    
    public function remover_favorito() {
        $id_imovel = $this->input->post('id_imovel');

        $array_imoveis = explode(";", $this->input->cookie('baggio_favoritar'));
        $novo=NULL;
        for($i=0;$i<count($array_imoveis);$i++){
            if($array_imoveis[$i]!=$id_imovel){
                if($novo!=NULL){
                    $novo=$novo.';'.$array_imoveis[$i];
                }
                else {
                    $novo=$array_imoveis[$i];
                }
                
            }
        }
        $cookie = array(
            'name'   => 'baggio_favoritar',
            'value'  => $novo,
            'expire' => CONS_BAGGIO_FAVORITO_COOKIE_LIFE,
            'secure' => CONS_BAGGIO_FAVORITO_COOKIE_SECURE
        );
        $this->input->set_cookie($cookie);

        header('Content-Type: application/json');

        echo json_encode($id_imovel);
    }
    
    /*
     * COMPARAR_LOCACAO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS :
     * ---------------------------------------
     * RETORNO :
    */
    public function comparar_locacao($id_imovel) {
        $this->load->helper('cookie');

        $valor_cookie = $id_imovel;

        if ($this->input->cookie('baggio_comparar')) {
            $imoveis = $this->input->cookie('baggio_comparar');
            $imoveis = $imoveis . ';' . $id_imovel;
            $valor_cookie = $imoveis;
        } else {
            $this->session->set_flashdata('param_msg_sucesso_cabecalho', 'Imóvel Comparação');
            $this->session->set_flashdata('param_msg_sucesso', 'Seu imóvel foi adicionado para comparação! Agora você pode buscá-lo mais facilmente acessando sua lista de comparação, no topo do site!');
        }

        $cookie = array(
                'name'   => 'baggio_comparar',
                'value'  => $valor_cookie,
                'expire' => CONS_BAGGIO_FAVORITO_COOKIE_LIFE,
                'secure' => CONS_BAGGIO_FAVORITO_COOKIE_SECURE
        );

        $this->input->set_cookie($cookie);

        redirect(base_url('imovel/visualizar-comparacao-locacao'));
    }

    /*
     * COMPARAR_VENDA
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS :
     * ---------------------------------------
     * RETORNO :
    */
    public function comparar_venda($id_imovel) {
        $this->load->helper('cookie');

        $valor_cookie = $id_imovel;

        if ($this->input->cookie('baggio_comparar')) {
            $imoveis = $this->input->cookie('baggio_comparar');
            $imoveis = $imoveis . ';' . $id_imovel;
            $valor_cookie = $imoveis;
        } else {
            $this->session->set_flashdata('param_msg_sucesso_cabecalho', 'Imóvel Comparação');
            $this->session->set_flashdata('param_msg_sucesso', 'Seu imóvel foi adicionado para comparação! Agora você pode buscá-lo mais facilmente acessando sua lista de comparação, no topo do site!');
        }

        $cookie = array(
                'name'   => 'baggio_comparar',
                'value'  => $valor_cookie,
                'expire' => CONS_BAGGIO_FAVORITO_COOKIE_LIFE,
                'secure' => CONS_BAGGIO_FAVORITO_COOKIE_SECURE
        );

        $this->input->set_cookie($cookie);
            
        redirect(base_url('imovel/visualizar-comparacao-venda'));
    }
    
    /*
     * LISTAGEM COMPARACAO LOCACAO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS :
     * ---------------------------------------
     * RETORNO :
    */
    public function listagem_comparacao_locacao() {
        $dados['imoveis'] = $this->_preparar_imoveis_comparacao();

        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $this->load->view('template/menu');
        $this->load->view('template/mensagens');
        $this->load->view('listagem_comparacao_aluguel', $dados);
        $this->load->view('template/footer');
    }
    
    /*
     * LISTAGEM COMPARACAO VENDA
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS :
     * ---------------------------------------
     * RETORNO :
    */
    public function listagem_comparacao_venda() {
        $dados['imoveis'] = $this->_preparar_imoveis_comparacao();

        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $this->load->view('template/menu');
        $this->load->view('template/mensagens');
        $this->load->view('listagem_comparacao_venda', $dados);
        $this->load->view('template/footer');
    }
    
    /*
     * REMOVER_LISTAGEM_LOCACAO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS :
     * ---------------------------------------
     * RETORNO :
    */
    public function remover_listagem_locacao($id_imovel) {
        $imoveis_cookie = explode(";", $this->input->cookie('baggio_comparar'));

        $nao_removidos = NULL;

        foreach ($imoveis_cookie as $cookie_id_imovel) {
            if ($cookie_id_imovel != $id_imovel) {
                if ($nao_removidos != NULL) {
                    $nao_removidos = $nao_removidos . ';' . $cookie_id_imovel;
                } else {
                    $nao_removidos = $cookie_id_imovel;
                }
                
            }
        }

        $cookie = array(
            'name'   => 'baggio_comparar',
            'value'  => $nao_removidos,
            'expire' => CONS_BAGGIO_FAVORITO_COOKIE_LIFE,
            'secure' => CONS_BAGGIO_FAVORITO_COOKIE_SECURE
        );

        $this->input->set_cookie($cookie);

        $this->session->set_flashdata('param_msg_sucesso','O imóvel foi removido da sua lista de Comparação com sucesso!');

        redirect(base_url('imovel/visualizar-comparacao-locacao'));
    }
    
    /*
     * REMOVER_LISTAGEM_VENDA
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS :
     * ---------------------------------------
     * RETORNO :
    */
    public function remover_listagem_venda($id_imovel) {
        $imoveis_cookie = explode(";", $this->input->cookie('baggio_comparar'));

        $nao_removidos = NULL;

        foreach ($imoveis_cookie as $cookie_id_imovel) {
            if ($cookie_id_imovel != $id_imovel) {
                if ($nao_removidos != NULL) {
                    $nao_removidos = $nao_removidos . ';' . $cookie_id_imovel;
                } else {
                    $nao_removidos = $cookie_id_imovel;
                }
                
            }
        }

        $cookie = array(
            'name'   => 'baggio_comparar',
            'value'  => $nao_removidos,
            'expire' => CONS_BAGGIO_FAVORITO_COOKIE_LIFE,
            'secure' => CONS_BAGGIO_FAVORITO_COOKIE_SECURE
        );

        $this->input->set_cookie($cookie);

        $this->session->set_flashdata('param_msg_sucesso','O imóvel foi removido da sua lista de Comparação com sucesso!');

        redirect(base_url('imovel/visualizar-comparacao-venda'));
    }
    
    /* ************************************************************* */
    /* ******************** FUNCOES DE VALIDACAO ******************* */
    /* ************************************************************* */
    /*
     * VALIDACAO_CONTATO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function validacao_contato() {
        $this->form_validation->set_rules('nome','Nome','required');
        $this->form_validation->set_rules('email','E-mail','required|valid_email');
        $this->form_validation->set_rules('telefone','Telefone','required');
        $this->form_validation->set_rules('assunto','Assunto','required');
        $this->form_validation->set_rules('mensagem','Mensagem','required');
        $this->form_validation->set_rules('g-recaptcha-response','Captcha','callback__validacao_recaptcha');



        if($this->form_validation->run()==FALSE){
            $this->contato();
        }
        else {
            $dados=elements(array('nome','email','telefone','assunto','mensagem'), 
                $this->input->post());
            if($dados['assunto']==='Proprietários' || $dados['assunto']==='Inquilinos' || 
                $dados['assunto']==='Rescisão' || $dados['assunto']==='Manutenção' ||
                $dados['assunto']==='Renovação'){
                $array_emails=array("eduardosprada@baggioimoveis.com.br","joaofalarz@baggioimoveis.com.br");
            }
            if($dados['assunto']==='Financeiro'){
                $array_emails=array("bruno@baggioimoveis.com.br","darlan@baggioimoveis.com.br");
            }
            if($dados['assunto']==='Quero alugar um imóvel'){
                $array_emails=array("locacaosite@baggioimoveis.com.br");
            }
            if($dados['assunto']==='Quero comprar um imóvel'){
                $array_emails=array("nathalia@baggioimoveis.com.br");
            }
            if($dados['assunto']==='Angariação área de Vendas'){
                $array_emails=array("nathalia@baggioimoveis.com.br");
            }
            if($dados['assunto']==='Angariação área de Locações'){
                $array_emails=array("junior@baggioimoveis.com.br");
            }
            if($dados['assunto']==='Marketing e Comunicação'){
                $array_emails=array("comunicacao@baggioimoveis.com.br");
            }
            if($dados['assunto']==='Outros'){
                $array_emails=array("contato@baggioimoveis.com.br");
            }
            $assunto="Baggio Imóveis - ".$dados['assunto'];
            $mensagem="<b>Nome: </b>".$dados['nome']."<br/><b>E-mail: </b>".$dados['email']."<br/><b>Mensagem:</b><br/>".$dados['mensagem'];
            if(enviar_para_baggio_contato($assunto,$dados['email'],$dados['nome'],$mensagem,$array_emails)==TRUE){
                $this->session->set_flashdata('mensagem','Mensagem Enviada com Sucesso!!!');
                redirect('contato');
            }
        }
    }
    
    /*
     * VALIDACAO_ESQUECI_SENHA
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    
    public function validacao_esqueci_senha() {
        $this->form_validation->set_rules('email','E-mail','required|valid_email|callback__validacao_email_cadastro');
        $this->form_validation->set_rules('g-recaptcha-response','Captcha','callback__validacao_recaptcha');
        if($this->form_validation->run()==FALSE){
            $this->esqueci_senha();
        }
        else {
             $mensagem="Para Alteração de sua senha <a href=".base_url('alterar-senha?token='.$this->encryption->encrypt($this->input->post('email'))).">clique aqui</a>";
            if(enviar_email('Baggio Imóveis - Recuperação de Senha',array($this->input->post('email')),$mensagem)==TRUE){
                $this->session->set_flashdata('mensagem','Verifique seu E-mail para continuar com a alteração de sua senha');
                redirect('esqueci-senha');
            }
        }
    }
    
    /*
     * VALIDACAO_ESQUECI_SENHA
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    
    public function validacao_alteracao_senha() {
        $this->form_validation->set_rules('senha','Senha','required');
        $this->form_validation->set_rules('r_senha','Repetir Senha','matches[senha]');
        if($this->form_validation->run()==FALSE){
            $this->alteracao_senha();
        }
        else {
            $this->m_cliente->alterar_senha($this->encryption->decrypt($_GET['token']),md5($this->input->post('senha')));
            $this->session->set_flashdata('mensagem','Senha Alterada com Sucesso');
            redirect('login');
        }
    }
    
    /*
     * VALIDACAO_AGENDAR_VISITA
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    
    public function validacao_agendar_visita() {
        $this->form_validation->set_rules('nome','Nome','required');
        $this->form_validation->set_rules('email','E-mail','required|valid_email');
        $this->form_validation->set_rules('telefone','Telefone','required');
        $this->form_validation->set_rules('mensagem','Mensagem','required');
        $this->form_validation->set_rules('g-recaptcha-response','Captcha','callback__validacao_recaptcha');
        if($this->form_validation->run()==FALSE){
            $this->agendar_visita();
        }
        else {
            $dados=elements(array('nome','email','telefone','mensagem','url'), $this->input->post());
             $mensagem="Nome: ".$dados['nome']."<br>E-mail: ".$dados['email'].
                    "<br>Telefone:".$dados['telefone']."<br>Imovel de Interesse: ".
                    $dados['url']."<br>Mensagem: ".$dados['mensagem'];
            if(enviar_para_baggio_contato('Baggio Imóveis - Agendamento de Visita',$dados['email'],$dados['nome'],$mensagem,array("locacaosite@baggioimoveis.com.br"))==TRUE){
                $this->session->set_flashdata('mensagem','Mensagem Enviada com Sucesso!!!');
                redirect('agendar-visita');
            }
        }
    }
    
    public function validacao_agendar_visita_imovel($id_imovel) {
        $this->form_validation->set_rules('nome','Nome','required');
        $this->form_validation->set_rules('email','E-mail','required|valid_email');
        $this->form_validation->set_rules('telefone','Telefone','required');
        $this->form_validation->set_rules('mensagem','Mensagem','required');
        $this->form_validation->set_rules('g-recaptcha-response','Captcha','callback__validacao_recaptcha');
        if($this->form_validation->run()==FALSE){
            $this->visualizar_imovel($id_imovel);
        }
        else {
            $dados=elements(array('nome','email','telefone','mensagem','url'), $this->input->post());
            $mensagem="Nome: ".$dados['nome']."<br>E-mail: ".$dados['email'].
                    "<br>Telefone:".$dados['telefone']."<br>Imovel de Interesse: ".
                    $dados['url']."<br>Mensagem: ".$dados['mensagem'];
            if(enviar_para_baggio_contato('Agendamento de Visita',$dados['email'],$dados['nome'],$mensagem,array("locacaosite@baggioimoveis.com.br"))==TRUE){
                $this->session->set_flashdata('mensagem','Mensagem Enviada com Sucesso!!!');
                redirect('imovel/visualizar-imovel/'.$id_imovel);
            }
        }
    }
    
    /*
     * VALIDACAO_CADASTRO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function validacao_cadastro() {
        $this->form_validation->set_rules('nome_cliente','Nome','required');
        $this->form_validation->set_rules('email','E-mail','required|valid_email|is_unique[tb_clientes.email]');
        $this->form_validation->set_message('is_unique','Esse E-mail Já Está Cadastrado');
        $this->form_validation->set_rules('tipo_cliente','Tipo de Cliente','required');
        $this->form_validation->set_rules('id_corretor','Corretor','required');
        $this->form_validation->set_rules('senha','Senha','trim|required');
        $this->form_validation->set_rules('r_senha','Repetir Senha','required|matches[senha]');
        $this->form_validation->set_rules('g-recaptcha-response','Captcha','callback__validacao_recaptcha');
        if($this->form_validation->run()==FALSE){
            $this->cadastro();
        }
        else {
            $dados= elements(array('nome_cliente','email','senha','id_corretor','tipo_cliente','telefone'), $this->input->post());
            $dados['senha']= md5($dados['senha']);
            $dados['ativo']=1;
            $dados['id_tipo_cliente']=3;
            $this->m_cliente->adicionar_cliente($dados);
            $this->session->set_flashdata('mensagem','Cadastro Efetuado com Sucesso!!!'
                    . ' Para Acessar Sua Área Exclusiva <a href="'.base_url('login').'"> Clique Aqui</a>');
            redirect('cadastro');
        }
    }
    
    /*
     * VALIDACAO_TRABALHE_CONOSCO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function validacao_trabalhe_conosco() {
        $this->form_validation->set_rules('nome','Nome','required');
        $this->form_validation->set_rules('email','E-mail','required|valid_email');
        $this->form_validation->set_rules('telefone','Telefone','required');
        $this->form_validation->set_rules('interesse','interesse','required');
        $this->form_validation->set_rules('mensagem','Mensagem','required');
        $this->form_validation->set_rules('g-recaptcha-response','Captcha','callback__validacao_recaptcha');
        $dados=$this->_upload_documentos();
        if($this->form_validation->run()==FALSE || isset($dados['error'])){
            $this->trabalhe_conosco($dados['error']);
        }
        else {
            $dados_trabalhe=elements(array('nome','email','telefone','interesse','mensagem'), 
                $this->input->post());
            $mensagem="Nome: ".$dados_trabalhe['nome']."<br>E-mail: ".$dados_trabalhe['email']
                    ."<br>Telefone: ".$dados_trabalhe['telefone']."<br>Interesse: ".$dados_trabalhe['interesse']."<br>"
                    . "Mensagem: ".$dados_trabalhe['mensagem']."<br>Currículo: "
                    . base_url('publico/uploads/trabalhe-conosco/'.$dados[0]['file_name']);
            if(enviar_para_baggio_contato("Trabalhe Conosco",$dados_trabalhe['email'],$dados_trabalhe['nome'],$mensagem,array("gestaodepessoas@baggioimoveis.com.br"))==TRUE){
                $mensagem2="<p>Prezado Candidato</p><p>Informamos que recebemos a sua mensagem e currículo enviados pelo nosso site,"
                        . "através da página Trabalhe Conosco.</p><p>Surgindo a oportunidade, entraremos em contato.</p>"
                        . "<p>Atenciosamente,</p><p>Área de Gestão de Pessoas.</p>";
                enviar_email("Trabalhe Conosco - Baggio Imóveis", array($dados_trabalhe['email']), $mensagem2);
                $this->session->set_flashdata('mensagem','Currículo Enviado com Sucesso!!!');
                redirect('trabalhe-conosco');
            }
        }
    }
    
    /* VALIDACAO_ENVIO_IMOVEL_EMAIL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function validadacao_envio_imovel_email($id_imovel) {
        $this->form_validation->set_rules('nome_destinatario','Nome','required');
        $this->form_validation->set_rules('email_destinatario','E-mail','required|valid_email');
        $this->form_validation->set_rules('mensagem','Mensagem','required');
        $this->form_validation->set_rules('g-recaptcha-response','Captcha','callback__validacao_recaptcha');
        if($this->form_validation->run()==FALSE){
            $this->visualizar_imovel($id_imovel);
        }
        else {
            $dados=elements(array('nome_remetente', 'nome_destinatario','email_destinatario','mensagem','url_imovel'), 
                $this->input->post());

            //$mensagem='URL do Imóvel:'.$dados['url_imovel'].'<br>'.'Mensagem:'.$dados['mensagem'];

            $mensagem = $this->load->view('template/emails/envio_imovel_email', $dados, TRUE);

            if(enviar_para_cliente('Baggio Imóveis – '.$dados['nome_remetente'].' te enviou um imóvel.',array($dados['email_destinatario']),$mensagem)==TRUE){
                $this->session->set_flashdata('mensagem','Mensagem Enviada com Sucesso!!!');
                redirect('imovel/visualizar-imovel/'.$id_imovel);
            }
        }
    }
    
    /* VALIDACAO_MAIS_INFORMAÇÔES_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function validacao_mais_informacoes_imovel($id_imovel) {
        $this->form_validation->set_rules('nome','Nome','required');
        $this->form_validation->set_rules('email','E-mail','required|valid_email');
        $this->form_validation->set_rules('telefone','Telefone','required');
        $this->form_validation->set_rules('mensagem','Mensagem','required');
        $this->form_validation->set_rules('g-recaptcha-response','Captcha','callback__validacao_recaptcha');
        if($this->form_validation->run()==FALSE){
            $this->visualizar_imovel($id_imovel);
        }
        else {
            $dados=elements(array('nome','email','mensagem','url_imovel','telefone'), 
                $this->input->post());
            $mensagem='Nome: '.$dados['nome'].'<br>E-mail: '.$dados['email'].'<br>URL do Imóvel: '.$dados['url_imovel'].'<br>'.'Telefone: '.$dados['telefone'].'<br>'.
                    'Mensagem: '.$dados['mensagem'];
            if(enviar_para_baggio_contato('Mais Informações de Imóvel',$dados['email'],$dados['nome'],$mensagem,array("locacaosite@baggioimoveis.com.br"))==TRUE){
                $this->session->set_flashdata('mensagem','Mensagem Enviada com Sucesso!!!');
                redirect('imovel/visualizar-imovel/'.$id_imovel);
            }
        }
    }
    
    /* VALIDACAO_ADMINISTRE_MEU_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function validadacao_administre_meu_imovel() {
        $this->form_validation->set_rules('nome','Nome','required');
        $this->form_validation->set_rules('email','E-mail','required|valid_email');
        $this->form_validation->set_rules('telefone','Telefone','required');
        $this->form_validation->set_rules('endereco','Endereço','required');
        $this->form_validation->set_rules('tipo_imovel','Tipo de Imóvel','required');
        $this->form_validation->set_rules('valor_aluguel','Valor do Aluguel','required');
        $this->form_validation->set_rules('observacao','Observação','required');
        $this->form_validation->set_rules('g-recaptcha-response','Captcha','callback__validacao_recaptcha');
        if($this->form_validation->run()==FALSE){
            $this->administre_meu_imovel();
        }
        else {
            $dados=elements(array('nome','email','mensagem','telefone','endereco','tipo_imovel','valor_aluguel',
                'observacao'),$this->input->post());
            $mensagem='<b>Nome:</b> '.$dados['nome'].'<br><b>E-mail:</b> '.$dados['email'].'<br>'.
                    '<b>Telefone:</b> '.$dados['telefone'].'<br>'.'<b>Endereço do Imóvel:</b> '.$dados['endereco'].'<br>'.
                    '<b>Tipo de Imóvel:</b> '.$dados['tipo_imovel'].'<br>'.'<b>Valor do Aluguel:</b> '.$dados['valor_aluguel'].
                    '<br>'.'<b>Observações:</b> '.$dados['observacao'];
            if(enviar_para_baggio_contato('Administre Meu Imóvel',$dados['email'],$dados['nome'],$mensagem,array("locacaosite@baggioimoveis.com.br"))==TRUE){
                $this->session->set_flashdata('mensagem','Mensagem Enviada com Sucesso!!!');
                redirect('adminstre-meu-imovel');
            }
        }
    }
    
    /* VALIDACAO_VENDA_MEU_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function validacao_venda_meu_imovel() {
        $this->form_validation->set_rules('nome','Nome','required');
        $this->form_validation->set_rules('email','E-mail','required|valid_email');
        $this->form_validation->set_rules('telefone','Telefone','required');
        $this->form_validation->set_rules('endereco','Endereço','required');
        $this->form_validation->set_rules('tipo_imovel','Tipo de Imóvel','required');
        $this->form_validation->set_rules('observacao','Observação','required');
        $this->form_validation->set_rules('g-recaptcha-response','Captcha','callback__validacao_recaptcha');
        if($this->form_validation->run()==FALSE){
            $this->venda_meu_imovel();
        }
        else {
            $dados=elements(array('nome','email','mensagem','telefone','endereco','tipo_imovel',
                'observacao'),$this->input->post());
            $mensagem='<b>Nome:</b> '.$dados['nome'].'<br><b>E-mail:</b> '.$dados['email'].'<br>'.
                    '<b>Telefone:</b> '.$dados['telefone'].'<br>'.'<b>Endereço do Imóvel:</b> '.$dados['endereco'].'<br>'.
                    '<b>Tipo de Imóvel:</b> '.$dados['tipo_imovel'].'<br>'.'<b>Observações:</b> '.$dados['observacao'];
            if(enviar_para_baggio_contato('Administre Meu Imóvel',$dados['email'],$dados['nome'],$mensagem,array("locacaosite@baggioimoveis.com.br"))==TRUE){
                $this->session->set_flashdata('mensagem','Mensagem Enviada com Sucesso!!!');
                redirect('venda-meu-imovel');
            }
        }
    }
    
    /*
     * VALIDACAO_RECAPTCHA
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : Bool
    */
    public function _validacao_recaptcha($captcha){
        if (!empty($captcha)) {
            $response = $this->recaptcha->verifyResponse($captcha);
            if (isset($response['success']) and $response['success'] === TRUE) {
                return TRUE;
            }
        }
        else{
            $this->form_validation->set_message('_validacao_recaptcha', 'O {field} é obtigatório');
            return FALSE;
        }
    }
    
    /*
     * VALIDACAO_EMAIL_CADASTRO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : Bool
    */
    public function _validacao_email_cadastro($email){
        $get_email= $this->m_cliente->verifica_email_cadastro($email);
        if (!empty($get_email)) {
            return TRUE;
        }
        else{
            $this->form_validation->set_message('_validacao_email_cadastro', 'O {field} não está cadastrado');
            return FALSE;
        }
    }

    /*
     * TESTE_GMAIL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : Bool
    */
    public function teste_gmail($email="") {
        if (!empty($email)) {
            if (enviar_email("Teste de Email Baggio", array($email . "@gmail.com"), "Mensagem do email")) {
                to_debug("Email enviado");
            } else {
                to_debug("Não foi enviado");
            }
        }
    }

    /*
     * TESTE_HOTMAIL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : Bool
    */
    public function teste_hotmail($email="") {
        if (!empty($email)) {
            if (enviar_hotmail("Teste de Email Baggio", array($email . "@hotmail.com"), "Mensagem do email")) {
                to_debug("Email enviado");
            } else {
                to_debug("Não foi enviado");
            }
        }
    }

    /*
     * TESTE_LOCAWEB
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : Bool
    */
    public function teste_locaweb($email="") {
        if (!empty($email)) {
            if (enviar_locaweb("Teste de Email Baggio", array($email . "@gmail.com"), "Mensagem do email")) {
                to_debug("Email enviado");
            } else {
                to_debug("Não foi enviado");
            }
        }
    }

    /*
     * TESTE_HORARIO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : Bool
    */
    // require './vendor/autoload.php';
    public function teste_horario() {
        $agora = Carbon::now();
        $data_antes = from_sql_to_carbon("2018-03-20 20:10:05");
        $data_depois = from_sql_to_carbon("2018-03-20 20:11:15");

        to_debug($data_antes->toDateTimeString(), FALSE);
        to_debug($data_depois->toDateTimeString(), FALSE);
        to_debug($data_depois->diffInSeconds($data_antes, false));
    }
    
    /* ************************************************************* */
    /* ********************** FUNCOES PRIVADAS ********************* */
    /* ************************************************************* */
    /*
     * _VISUALIZAR_IMOVEL_ALUGUEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function _visualizar_imovel_aluguel($dados, $id_imovel) {
        $dados['imagens'] = $this->m_imoveis->buscar_todas_imagem_imovel($id_imovel);
        $dados['tipos_imovel'] = $this->m_imoveis->buscar_todos_tipos_imovel($id_imovel);
        $dados['widget']=$this->recaptcha->getWidget();
        $dados['script']=$this->recaptcha->getScriptTag();

        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $dados_breadcrumbs["breadcrumbs"] = array(
            "home", "busca", "visualizar-imovel"
        );

        $this->load->view('template/header', $header);
        $this->load->view('template/menu');
        $this->load->view('template/breadcrumbs', $dados_breadcrumbs);
        $this->load->view('visulaizar_imovel_aluguel', $dados);
        $this->load->view('modal/agendar_visita');
        $this->load->view('modal/enviar_imovel_email');
        $this->load->view('modal/mais_informacoes');
        $this->load->view('template/footer');
    }

    /*
     * _VISUALIZAR_IMOVEL_VENDA
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function _visualizar_imovel_venda($dados, $id_imovel) {
        $dados['imagens'] = $this->m_imoveis->buscar_todas_imagem_imovel($id_imovel);
        $dados['tipos_imovel'] = $this->m_imoveis->buscar_todos_tipos_imovel($id_imovel);
        $dados['widget']=$this->recaptcha->getWidget();
        $dados['script']=$this->recaptcha->getScriptTag();

        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $dados_breadcrumbs["breadcrumbs"] = array(
            "home", "busca", "visualizar-imovel"
        );

        $this->load->view('template/header', $header);
        $this->load->view('template/menu');
        $this->load->view('template/breadcrumbs', $dados_breadcrumbs);
        $this->load->view('visulaizar_imovel_venda', $dados);
        $this->load->view('modal/agendar_visita');
        $this->load->view('modal/enviar_imovel_email');
        $this->load->view('modal/mais_informacoes');
        $this->load->view('template/footer');
    }

    /*
     * _PREPARAR_IMOVEIS_FAVORITOS
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    private function _preparar_imoveis_favoritos() {
        $cardboards = array();

        $imoveis_cookie = explode(";", $this->input->cookie('baggio_favoritar'));

        foreach ($imoveis_cookie as $indice => $id_imovel_cookie) {
            if (!empty($id_imovel_cookie)) {
                $dados_cardboard = array();

                $dados_imovel = $this->m_imoveis->buscar_imovel_card($id_imovel_cookie);

                $dados_imovel["tipos_imovel"] = $this->m_imoveis->buscar_todos_tipos_imovel($id_imovel_cookie);
                $imagem = $this->m_imoveis->buscar_imagem_imovel($id_imovel_cookie);
                if (!empty($imagem)) {
                    $dados_imovel["link_imagem"] = $imagem[0]["link_imagem"];
                    $dados_imovel["link_miniatura"] = $imagem[0]["link_miniatura"];
                }

                $dados_cardboard["imovel"] = $dados_imovel;

                if (array_key_exists("id_tipo_negociacao", $dados_imovel)) {
                    if ($dados_imovel["id_tipo_negociacao"] == 1) {
                        $cardboard = $this->load->view(
                            'template/imovel/cardboard_venda_imovel', 
                            $dados_cardboard, TRUE
                        );
                    } else if ($dados_imovel["id_tipo_negociacao"] == 2) {
                        $cardboard = $this->load->view(
                            'template/imovel/cardboard_locacao_imovel', 
                            $dados_cardboard, TRUE
                        );
                    }
                    array_push($cardboards, $cardboard);
                }
            }
        }
        return $cardboards;
    }

    /*
     * _PREPARAR_IMOVEIS_COMPARACAO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    private function _preparar_imoveis_comparacao() {
        $dados = array();
        $imoveis_cookie = explode(";", $this->input->cookie('baggio_comparar'));

        foreach ($imoveis_cookie as $indice => $id_imovel) {
            if(!empty($id_imovel)) {
                $dados_imovel = $this->m_imoveis->buscar_informacao_imovel_visualizar($id_imovel);

                $dados_imovel["tipos_imovel"] = $this->m_imoveis->buscar_todos_tipos_imovel($id_imovel);
                $dados_imovel["imagens"] = $this->m_imoveis->buscar_todas_imagem_imovel($id_imovel);

                $dados[$indice] = $dados_imovel;
            }
        }
        return $dados;
    }

    /*
     * _PREPARAR_CARDBOARDS
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    private function _preparar_cardboards($imoveis=array(), $tipo_negociacao=1) {
        $cardboards = array();

        foreach ($imoveis as $imovel) {
            $dados_cardboard = array();

            $imovel["tipos_imovel"] = $this->m_imoveis->buscar_todos_tipos_imovel($imovel["id_imovel"]);

            $dados_cardboard["imovel"] = $imovel;

            if ($tipo_negociacao == 1) {
                $cardboard = $this->load->view(
                    'template/imovel/cardboard_venda_imovel', 
                    $dados_cardboard, TRUE
                );
            } else if ($tipo_negociacao == 2) {
                $cardboard = $this->load->view(
                    'template/imovel/cardboard_locacao_imovel', 
                    $dados_cardboard, TRUE
                );
            }

            array_push($cardboards, $cardboard);
        }
        return $cardboards;
    }

    /*
     * _PREPARAR_VIEW_BUSCA
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    private function _preparar_view_busca() {
        $dados_abas = array();
        $dados_busca = array();
        $dados_filtros = array();
        $dados_aba_bairro = array();
        $dados_aba_condomio = array();
        $dados_aba_referencia = array();

        $dados_aba_bairro["tipos_negociacoes"] = $this->m_tipos->listar_tipo_negociacoes();
        $dados_aba_bairro["subtipos_imoveis"] = $this->m_imoveis->listar_subtipos_imovel_categorizado();
        $dados_aba_bairro["cidades"] = $this->m_enderecos->listar_cidades_com_imovel_e_bairro();
        $dados_aba_bairro["bairros"] = $this->m_enderecos->listar_bairros_com_imovel();

        $dados_aba_condomio["subtipos_imoveis"] = $this->m_imoveis->listar_subtipos_imovel_categorizado();

        $dados_busca["esconder_tudo"] = TRUE;
        $dados_busca["view_abas"] = $this->load->view('template/busca/abas/view_abas', $dados_abas, TRUE);
        $dados_busca["view_aba_bairro"] = $this->load->view('template/busca/abas/busca_bairro', $dados_aba_bairro, TRUE);
        $dados_busca["view_aba_referencia"] = $this->load->view('template/busca/abas/busca_referencia', $dados_aba_referencia, TRUE);
        $dados_busca["view_aba_condominio"] = $this->load->view('template/busca/abas/busca_condominio', $dados_aba_condomio, TRUE);

        return $this->load->view('template/busca/view_busca', $dados_busca, TRUE);
    }

    /*
     * _UPLOAD_DOCUMENTOS
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : Bool
    */
    public function _upload_documentos(){
        if(!is_dir("publico/uploads/trabalhe-conosco")){
            mkdir("publico/uploads/trabalhe-conosco", 0700);
        }
        $config['upload_path']          = 'publico/uploads/trabalhe-conosco';
        $config['allowed_types']        = 'jpg|png|pdf';
        $config['max_size']             = 100000;
        $this->load->library('upload', $config);
        $filesCountCurriculo = count($_FILES['curriculo']['name']);
        for($i = 0; $i < $filesCountCurriculo; $i++){
            $_FILES['curriculo_aux']['name'] = $_FILES['curriculo']['name'][$i];
            $_FILES['curriculo_aux']['type'] = $_FILES['curriculo']['type'][$i];
            $_FILES['curriculo_aux']['tmp_name'] = $_FILES['curriculo']['tmp_name'][$i];
            $_FILES['curriculo_aux']['error'] = $_FILES['curriculo']['error'][$i];
            $_FILES['curriculo_aux']['size'] = $_FILES['curriculo']['size'][$i];
            if(! $this->upload->do_upload('curriculo_aux')){
                $error = array('error' => $this->upload->display_errors());
                return $error;
            }
            $dados[$i] = $this->upload->data();
        }
        $dados['error']=NULL;
        return $dados;
    }

}
