<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * BUSCACONTROLLER
 * -------------------------------------------------------------------
 * DESCRICAO : 
 * -------------------------------------------------------------------
 * CARREGA : 
 * - N/A
 */
class BuscaController extends CI_Controller {

    /*
     * CONSTRUCT
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function __construct() {
        parent::__construct();

        $this->load->model('ImovelModel','m_imoveis');
        $this->load->model('EnderecoModel','m_enderecos');
        $this->load->model('TipoModel','m_tipos');
    }
    
    /*
     * INDEX
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function index() {

    }

    /*
     * CARREGAR_BUSCA_IMOVEIS
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function carregar_busca_imoveis() {
        $id_negociacao = $this->input->post("param_negociacao");
        $tipo_busca = $this->input->post("param_tipo_busca");
        $ids_tipos = $this->input->post("param_tipos");
        $id_cidade = $this->input->post("param_cidade");
        $ids_bairros = $this->input->post("param_bairros");
        $referencia = $this->input->post("param_referencia");
        $condominio = $this->input->post("param_condominio");
        $tipos_condominios = $this->input->post("param_tipos_condominios");

        $dados_painel = array();

        $dados_painel['view_busca'] = $this->_preparar_view_busca(
            $id_negociacao, $tipo_busca, 
            $ids_tipos, $id_cidade, $referencia, 
            $condominio, $tipos_condominios
        );

        salvar_sessao_bairros($ids_bairros);

        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $this->load->view('template/menu');
        $this->load->view('template/mensagens');
        $this->load->view('api/busca/painel', $dados_painel);
        $this->load->view('api/busca/ordenacao');
        $this->load->view('api/busca/conteudo');
        $this->load->view('template/footer');
    }

    /*
     * CARREGAR_BUSCA_IMOVEIS_SESSAO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function carregar_busca_imoveis_sessao() {
        $busca = $this->_recuperar_sessao_busca_imoveis();

        if (isset($busca) && !empty($busca)) {
            $id_negociacao = $busca->id_negociacao;
            $tipo_busca = $busca->id_tipo_busca;
            $ids_tipos = $busca->tipos_imoveis;
            $id_cidade = $busca->cidade;
            $ids_bairros = $busca->bairros;
            $referencia = $busca->referencia;
            $condominio = $busca->condominio;
            $tipos_condominios = $busca->tipos_condominios;
            $ordenacao = $busca->ordenacao;
            $menor_preco = $busca->menor_preco;
            $maior_preco = $busca->maior_preco;
            $qtd_quartos = $busca->qtd_quartos;
            $qtd_banheiros = $busca->qtd_banheiros;
            $qtd_vagas = $busca->qtd_vagas;
            $menor_area = $busca->menor_area;
            $maior_area = $busca->maior_area;

            $dados_painel = array();

            $dados_painel['view_busca'] = $this->_preparar_view_busca(
                $id_negociacao, $tipo_busca, 
                $ids_tipos, $id_cidade, $referencia, 
                $condominio, $tipos_condominios
            );

            salvar_sessao_bairros($ids_bairros);

            $header["scripts"] = carregar_js();
            $header["styles"] = carregar_css();

            $this->load->view('template/header', $header);
            $this->load->view('template/menu');
            $this->load->view('template/mensagens');
            $this->load->view('api/busca/painel', $dados_painel);
            $this->load->view('api/busca/ordenacao');
            $this->load->view('api/busca/conteudo');
            $this->load->view('template/footer');
        } else {
            $this->carregar_busca_imoveis();
        }
    }

    /*
     * CARREGAR_BUSCA_IMOVEIS_SESSAO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function carregar_busca_imoveis_url($json_codificado) {
        $this->load->library('session');
        $json_codificado = str_replace("_", "=", $json_codificado);
        echo $json_sessao = base64_decode($json_codificado);
        //die();
        
        $this->session->unset_userdata("params_busca");
        $this->session->set_userdata("params_busca", $json_sessao);
        
        header("Location: ".base_url('buscar/sessao/imoveis'));
    }

    /* ************************************************************* */
    /* ********************** FUNCOES PRIVADAS ********************* */
    /* ************************************************************* */
    /*
     * _PREPARAR_VIEW_BUSCA
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    private function _preparar_view_busca(
        $param_negociacao=1, $param_tipo_busca, 
        $param_tipos=array(), $param_cidade=NULL, 
        $param_referencia="", 
        $param_condominio="", $param_tipos_condominios=array()) 
    {
        $dados_abas = array();
        $dados_busca = array();
        $dados_filtros = array();
        $dados_aba_bairro = array();
        $dados_aba_condomio = array();
        $dados_aba_referencia = array();

        $dados_busca["param_tipo_busca"] = $param_tipo_busca;
        
        $dados_aba_bairro["param_negociacao"] = $param_negociacao;
        $dados_aba_bairro["tipos_negociacoes"] = $this->m_tipos->listar_tipo_negociacoes();
        $dados_aba_bairro["subtipos_imoveis"] = $this->m_imoveis->listar_subtipos_imovel_categorizado();
        $dados_aba_bairro["cidades"] = $this->m_enderecos->listar_cidades_com_imovel_e_bairro();
        $dados_aba_bairro["bairros"] = $this->m_enderecos->listar_bairros_com_imovel();
        $dados_aba_bairro["param_tipos"] = $param_tipos;
        $dados_aba_bairro["param_cidade"] = $param_cidade;

        $dados_aba_referencia["param_referencia"] = $param_referencia;

        $dados_aba_condomio["param_tipos_condominios"] = $param_tipos_condominios;
        $dados_aba_condomio["param_condominio"] = $param_condominio;

        $dados_busca["esconder_tudo"] = FALSE;
        $dados_busca["view_filtros"] = $this->load->view('template/busca/view_filtros', NULL, TRUE);
        $dados_busca["view_abas"] = $this->load->view('template/busca/abas/view_abas', $dados_abas, TRUE);
        $dados_busca["view_aba_bairro"] = $this->load->view('template/busca/abas/busca_bairro', $dados_aba_bairro, TRUE);
        $dados_busca["view_aba_referencia"] = $this->load->view('template/busca/abas/busca_referencia', $dados_aba_referencia, TRUE);
        $dados_busca["view_aba_condominio"] = $this->load->view('template/busca/abas/busca_condominio', $dados_aba_condomio, TRUE);

        return $this->load->view('template/busca/view_busca', $dados_busca, TRUE);
    }

    /*
     * _RECUPERAR_SESSAO_BUSCA_IMOVEIS
     *
     * @return  array
    */
    function _recuperar_sessao_busca_imoveis() {
        $busca_json = $this->session->userdata('params_busca');

        if (isset($busca_json) && !empty($busca_json)) {
            return json_decode($busca_json);
        }
        return array();
    }

}
