<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Controller responsável pelas operações referentes as fichas do cliente 
 * pessoa jurídica
 *
 * PHP version 7
 *
 * @category PHP
 * @author   Kellton Leitão <kelltonl10@gmail.com>
 */
class FichaCadastroJuridicoController extends CI_Controller {

     public function __construct() {
        parent::__construct();
        if(!usuario_sessao_cliente()) {
            $this->session->set_flashdata('param_msg_erro_cabecalho', 'Sem Permissão');
            $this->session->set_flashdata('param_msg_erro', 'Você não tem permissão de acessar esta área!');
            redirect(base_url());
        }
        if(usuario_sessao_cliente()['cliente']!=="Pessoa Jurídica"){
            $this->session->set_flashdata('param_msg_erro_cabecalho', 'Sem Permissão');
            $this->session->set_flashdata('param_msg_erro', 'Você não tem permissão de acessar esta área!');
            redirect(base_url());
        }
        $this->load->model('ClienteModel','cliente');
    }
    
    /**
     * Carrega o fomulário de adição das fichas
     *
     * @param 
     * @return views
     */
    public function adicionar_ficha() {
        $dados['paises']=$this->cliente->listar_paises();

        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $this->load->view('template/menu');
        
        $this->load->view('adm_cliente/fichas/pessoa_juridica/adicionar/form_cabecalho',$dados);
        $this->load->view('adm_cliente/fichas/pessoa_juridica/adicionar/empresa',$dados);
        $this->load->view('adm_cliente/fichas/pessoa_juridica/adicionar/representante',$dados);
        $this->load->view('adm_cliente/fichas/pessoa_juridica/adicionar/socios',$dados);
        $this->load->view('adm_cliente/fichas/pessoa_juridica/adicionar/propriedades',$dados);
        $this->load->view('adm_cliente/fichas/pessoa_juridica/adicionar/referencias',$dados);
        $this->load->view('adm_cliente/fichas/pessoa_juridica/adicionar/inf_complementares',$dados);
        $this->load->view('adm_cliente/fichas/pessoa_juridica/adicionar/form_rodape',$dados);

        $this->load->view('template/footer');
    }
    
    /**
     * Carrega o fomulário de edição das fichas
     *
     * @param 
     * @return views
     */
    public function editar_ficha() {
        $dados['query']= $this->cliente->get_cliente_jurudico(usuario_sessao_cliente()['id_cliente']);
        $dados['socios']= $this->cliente->get_socios(usuario_sessao_cliente()['id_cliente']);
        $dados['propriedades']=$this->cliente->pegar_propriedades(usuario_sessao_cliente()['id_cliente']);
        $dados['referencias']=$this->cliente->pegar_referencias_comerciais(usuario_sessao_cliente()['id_cliente']);
        $dados['referencias_bancarias']=$this->cliente->pegar_referencias_bancarias(usuario_sessao_cliente()['id_cliente']);
        $dados['paises']=$this->cliente->listar_paises();

        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $this->load->view('template/menu');

        $this->load->view('adm_cliente/fichas/pessoa_juridica/editar/form_cabecalho',$dados);
        $this->load->view('adm_cliente/fichas/pessoa_juridica/editar/empresa',$dados);
        $this->load->view('adm_cliente/fichas/pessoa_juridica/editar/representante',$dados);
        $this->load->view('adm_cliente/fichas/pessoa_juridica/editar/socios',$dados);
        $this->load->view('adm_cliente/fichas/pessoa_juridica/editar/propriedades',$dados);
        $this->load->view('adm_cliente/fichas/pessoa_juridica/editar/referencias',$dados);
        $this->load->view('adm_cliente/fichas/pessoa_juridica/editar/inf_complementares',$dados);
        $this->load->view('adm_cliente/fichas/pessoa_juridica/editar/form_rodape',$dados);

        $this->load->view('template/footer');
    }
    
    /**
     * Carrega o visualização da ficha de cadastro preenchida
     *
     * @param 
     * @return views
     */
    public function visualizar_ficha_cadastro() {
        $dados['query']= $this->cliente->get_cliente_jurudico(usuario_sessao_cliente()['id_cliente']);
        $dados['socios']= $this->cliente->get_socios(usuario_sessao_cliente()['id_cliente']);
        $dados['propriedades']=$this->cliente->pegar_propriedades(usuario_sessao_cliente()['id_cliente']);
        $dados['referencias']=$this->cliente->pegar_referencias_comerciais(usuario_sessao_cliente()['id_cliente']);
        $dados['referencias_bancarias']=$this->cliente->pegar_referencias_bancarias(usuario_sessao_cliente()['id_cliente']);
        
        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $this->load->view('template/menu');
        $this->load->view('adm_cliente/juridico/visualizar_ficha_cadastro',$dados);
        $this->load->view('template/footer');
    }
    
    /**
     * Faz a validação da ficha de cadastro
     *
     * @param 
     * @return views
     */
    public function validacao_ficha_cadastro() {
        if($this->input->post('enviar')==="Enviar ficha para Baggio"){
            $this->_validacao_ficha_cadastro();
            if($this->form_validation->run()==FALSE){
                $this->_inserir_dados();
                $this->editar_ficha();
            }
            else{
                $this->_inserir_dados($envio=1);
                $this->session->set_flashdata('mensagem','Ficha de Cadastro Adicionada com Sucesso!!!');
                redirect('cli-juridico');
            }
        }
        else{
            $this->_inserir_dados();
            $this->session->set_flashdata('mensagem','Ficha de Cadastro Salva com Sucesso!!!');
            redirect('cli-juridico');
        }
    }
    
    /**
     * Insere os dados do pretendente no banco de dados
     *
     * @param int $envio Define o envio ou não da ficha para a Baggio
     * @return BOOLEAN
     */
    public function _inserir_dados($enviou=0) {
        $id_cliente=usuario_sessao_cliente()['id_cliente'];
        
        $this->db->trans_start();

        /*=INSERE OS DADOS DA EMPRESA NA TABELA TB_DADOS_EMPRESA_JURIDICO=*/
        $dados=elements(array('razao_social','tipo','tipo_empresa','fundacao',
            'cnpj','junta_comercial','inscricao_estadual','capital_social',
            'ramo_atividade','cep','logradouro','numero','complemento','bairro',
            'id_pais','email_comercial','telefone_comercial','socios',
            'observacoes_complementares'), $this->input->post());
        $dados['id_cliente']=$id_cliente;
        $dados['fundacao']=formatar_data_sql($dados['fundacao']);
        $dados['enviou']=$enviou;
        if($this->cliente->verifica_se_tem_dados_juridico(usuario_sessao_cliente()['id_cliente'])){
            $this->cliente->alterar_dados_empresa_juridico($id_cliente,$dados);
        }
        else{
            $this->cliente->inserir_dados_empresa_juridico($dados);
        }
        /*===============================FIM==============================*/
        
        /*INSERE OS DADOS DO SOCIO REPRESENTANTE NA TABELA TB_SOCIOS_REPRESENTANTES*/
        $socio=elements(array('nome_completo','telefone_fixo','celular','email',
            'data_nascimento','nacionalidade','cpf','rg','expedicao_rg','data_expedicao',
            'id_estado_civil','profissao','cargo','salario','residencia','cep_cliente',
            'logradouro_cliente','numero_cliente','complemento_cliente','bairro_cliente',
            'id_pais_cliente','outras_rendas','renda_total','email_comercial_socio'), $this->input->post());
        $socio['id_cliente']=$id_cliente;
        $socio['data_nascimento']=formatar_data_sql($socio['data_nascimento']);
        $socio['data_expedicao']=formatar_data_sql($socio['data_expedicao']);
        if($this->cliente->verifica_se_tem_dados_socio_representante(usuario_sessao_cliente()['id_cliente'])){
            $this->cliente->atualizar_dados_socio_representante($id_cliente,$socio);
        }
        else{
            $this->cliente->inserir_dados_socio_representante($socio);
        }
        /*===============================FIM==============================*/

        /*==========INSERE OS DADOS DOS SOCIOS NA TABELA TB_SOCIOS========*/
        $this->cliente->excluir_dados_socios($id_cliente);
        $socio_diretor=elements(array('nome_completo_diretor','cargo_diretor',
            'data_nascimento_diretor','nacionalidade_diretor','cpf_diretor',
            'rg_diretor','id_estado_civil_diretor','cep_diretor','logradouro_diretor',
            'numero_diretor','complemento_diretor','bairro_diretor','id_pais_diretor',
            'email_pessoal_diretor','email_comercial_diretor','telefone_diretor','email_comercial_socio'), $this->input->post());
        if($dados['socios']==1){
            $aux['nome_completo_diretor']=$socio_diretor['nome_completo_diretor'][0];
            $aux['cargo_diretor']=$socio_diretor['cargo_diretor'][0];
            $aux['data_nascimento_diretor']=formatar_data_sql($socio_diretor['data_nascimento_diretor'][0]);
            $aux['nacionalidade_diretor']=$socio_diretor['nacionalidade_diretor'][0];
            $aux['cpf_diretor']=$socio_diretor['cpf_diretor'][0];
            $aux['rg_diretor']=$socio_diretor['rg_diretor'][0];
            if($socio_diretor['id_estado_civil_diretor'][0]=='') $socio_diretor['id_estado_civil_diretor'][0]=1;
            $aux['id_estado_civil_diretor']=$socio_diretor['id_estado_civil_diretor'][0];
            $aux['cep_diretor']=$socio_diretor['cep_diretor'][0];
            $aux['logradouro_diretor']=$socio_diretor['logradouro_diretor'][0];
            $aux['numero_diretor']=$socio_diretor['numero_diretor'][0];
            $aux['complemento_diretor']=$socio_diretor['complemento_diretor'][0];
            $aux['bairro_diretor']=$socio_diretor['bairro_diretor'][0];
            $aux['id_pais_diretor']=$socio_diretor['id_pais_diretor'][0];
            $aux['email_pessoal_diretor']=$socio_diretor['email_pessoal_diretor'][0];
            $aux['email_comercial_diretor']=$socio_diretor['email_comercial_diretor'][0];
            $aux['telefone_diretor']=$socio_diretor['telefone_diretor'][0];
            $aux['id_cliente']=$id_cliente;
            $this->cliente->inserir_dados_socios($aux);
        }else{
            if($dados['socios']==2){
                for($i=1;$i<=2;$i++){
                    $aux['nome_completo_diretor']=$socio_diretor['nome_completo_diretor'][$i];
                    $aux['cargo_diretor']=$socio_diretor['cargo_diretor'][$i];
                    $aux['data_nascimento_diretor']=formatar_data_sql($socio_diretor['data_nascimento_diretor'][$i]);
                    $aux['nacionalidade_diretor']=$socio_diretor['nacionalidade_diretor'][$i];
                    $aux['cpf_diretor']=$socio_diretor['cpf_diretor'][$i];
                    $aux['rg_diretor']=$socio_diretor['rg_diretor'][$i];
                    if($socio_diretor['id_estado_civil_diretor'][$i]=='') $socio_diretor['id_estado_civil_diretor'][$i]=1;
                    $aux['id_estado_civil_diretor']=$socio_diretor['id_estado_civil_diretor'][$i];
                    $aux['cep_diretor']=$socio_diretor['cep_diretor'][$i];
                    $aux['logradouro_diretor']=$socio_diretor['logradouro_diretor'][$i];
                    $aux['numero_diretor']=$socio_diretor['numero_diretor'][$i];
                    $aux['complemento_diretor']=$socio_diretor['complemento_diretor'][$i];
                    $aux['bairro_diretor']=$socio_diretor['bairro_diretor'][$i];
                    $aux['id_pais_diretor']=$socio_diretor['id_pais_diretor'][$i];
                    $aux['email_pessoal_diretor']=$socio_diretor['email_pessoal_diretor'][$i];
                    $aux['email_comercial_diretor']=$socio_diretor['email_comercial_diretor'][$i];
                    $aux['telefone_diretor']=$socio_diretor['telefone_diretor'][$i];
                    $aux['id_cliente']=$id_cliente;
                    $this->cliente->inserir_dados_socios($aux);
                }
            }
            else{
                if($dados['socios']==3){
                    for($i=3;$i<=5;$i++){
                        $aux['nome_completo_diretor']=$socio_diretor['nome_completo_diretor'][$i];
                        $aux['cargo_diretor']=$socio_diretor['cargo_diretor'][$i];
                        $aux['data_nascimento_diretor']=formatar_data_sql($socio_diretor['data_nascimento_diretor'][$i]);
                        $aux['nacionalidade_diretor']=$socio_diretor['nacionalidade_diretor'][$i];
                        $aux['cpf_diretor']=$socio_diretor['cpf_diretor'][$i];
                        $aux['rg_diretor']=$socio_diretor['rg_diretor'][$i];
                        if($socio_diretor['id_estado_civil_diretor'][$i]=='') $socio_diretor['id_estado_civil_diretor'][$i]=1;
                        $aux['id_estado_civil_diretor']=$socio_diretor['id_estado_civil_diretor'][$i];
                        $aux['cep_diretor']=$socio_diretor['cep_diretor'][$i];
                        $aux['logradouro_diretor']=$socio_diretor['logradouro_diretor'][$i];
                        $aux['numero_diretor']=$socio_diretor['numero_diretor'][$i];
                        $aux['complemento_diretor']=$socio_diretor['complemento_diretor'][$i];
                        $aux['bairro_diretor']=$socio_diretor['bairro_diretor'][$i];
                        $aux['id_pais_diretor']=$socio_diretor['id_pais_diretor'][$i];
                        $aux['email_pessoal_diretor']=$socio_diretor['email_pessoal_diretor'][$i];
                        $aux['email_comercial_diretor']=$socio_diretor['email_comercial_diretor'][$i];
                        $aux['telefone_diretor']=$socio_diretor['telefone_diretor'][$i];
                        $aux['id_cliente']=$id_cliente;
                        $this->cliente->inserir_dados_socios($aux);
                    }
                }
            }
        }
        /*===============================FIM==============================*/
        
        /*INSERE OS DADOS DAS PROPRIEDADES DO CLIENTE NA TB_PROPRIEDADES_CLIENTES*/
        $this->cliente->excluir_dados_propriedades($id_cliente);
        $dados_propriedades= elements(array('matricula_imovel','circunscricao','veiculo_marca',
                'renavam'), $this->input->post());
        $propriedades=array();
        for($i=0;$i<count($dados_propriedades['matricula_imovel']);$i++){
            $propriedades['id_cliente']=$id_cliente;
            $propriedades['matricula_imovel']=$dados_propriedades['matricula_imovel'][$i];
            $propriedades['circunscricao']=$dados_propriedades['circunscricao'][$i];
            $propriedades['veiculo_marca']=$dados_propriedades['veiculo_marca'][$i];
            $propriedades['renavam']=$dados_propriedades['renavam'][$i];
            $this->cliente->inserir_dados_propriedades($propriedades);
        }
        /*===============================FIM==============================*/

        /*INSERE OS DADOS DAS REFERENCIAS DO CLIENTE NA TB_REFERENCIAS_CLIENTES*/
        $this->cliente->excluir_dados_referencias_comerciais($id_cliente);
        $dados_referencias= elements(array('nome_referencia','endereco_referencia','telefone_referencia','tipo_referencia'), $this->input->post());
        $referencias=array();
        for($i=0;$i<count($dados_referencias['nome_referencia']);$i++){
            $referencias['id_cliente']=$id_cliente;
            $referencias['nome_referencia']=$dados_referencias['nome_referencia'][$i];
            $referencias['endereco_referencia']=$dados_referencias['endereco_referencia'][$i];
            $referencias['telefone_referencia']=$dados_referencias['telefone_referencia'][$i];
            $referencias['tipo_referencia']=$dados_referencias['tipo_referencia'][$i];
            $this->cliente->inserir_dados_referencias($referencias);
        }
        /*===============================FIM==============================*/
        
        /*INSERE OS DADOS DAS REFERENCIAS DO BANCO DO CLIENTE NA TB_BANCOS_REFERENCIAS*/
        $this->cliente->excluir_dados_banco($id_cliente);
        $dados_banco= elements(array('banco_referencia','telefone_banco_referencia','nome_agencia_referencia',
                'numero_conta_referencia'), $this->input->post());
        $banco=array();
        for($i=0;$i<count($dados_banco['banco_referencia']);$i++){
            $banco['id_cliente']=$id_cliente;
            $banco['banco_referencia']=$dados_banco['banco_referencia'][$i];
            $banco['telefone_banco_referencia']=$dados_banco['telefone_banco_referencia'][$i];
            $banco['nome_agencia_referencia']=$dados_banco['nome_agencia_referencia'][$i];
            $banco['numero_conta_referencia']=$dados_banco['numero_conta_referencia'][$i];
            $this->cliente->inserir_dados_banco($banco);
        }
        /*===============================FIM==============================*/
        
        $this->db->trans_complete();
        
        if($enviou==1){
            /*===ENVIA EMAILS PARA O CORRETOR AVISANDO DE FICHA ADICIONADA====*/     
            $email_corretor=$this->cliente->busca_email_corretor(usuario_sessao_cliente()['id_corretor']);
            $dados_email['nome_cliente']=usuario_sessao_cliente()['nome_cliente'];
            $dados_email['email_cliente']=usuario_sessao_cliente()['email'];
            $dados_email['nome_corretor']=$this->cliente->busca_nome_corretor(usuario_sessao_cliente()['id_corretor']);
            $dados_email['link']=base_url('adm/clientes/visualizar-cliente/'.usuario_sessao_cliente()['id_cliente']);
            $mensagem = $this->load->view('template/emails/ficha_pessoa_juridica_baggio', $dados_email, TRUE);
            $mensagem_cliente = $this->load->view('template/emails/ficha_pessoa_juridica_cliente', $dados_email, TRUE);
            $array_emails=array($email_corretor['email'],"juarez@baggioimoveis.com.br","andressa@baggioimoveis.com.br");
            enviar_email_fichas(usuario_sessao_cliente()['nome_cliente']." | Ficha Pessoa Jurídica", $array_emails, $mensagem);
            enviar_email_fichas("Baggio Imóveis - ".$dados_email['nome_cliente'].", recebemos sua Ficha Pessoa Jurídica",  array($dados_email['email_cliente']), $mensagem_cliente);
            /*===============================FIM==============================*/
        }
    }
    
    /**
     * Regras de Validação da ficha de cadastro
     *
     * @param 
     * @return BOOLEAN
     */
    public function _validacao_ficha_cadastro() {
        $this->form_validation->set_rules('razao_social','Razão Social','required');
        $this->form_validation->set_rules('tipo','Tipo','required');
        if($this->input->post('tipo')=="OUTRO"){
            $this->form_validation->set_rules('tipo_empresa','Tipo de Empresa','required');
        }
        $this->form_validation->set_rules('fundacao','Fundaçao','required');
        $this->form_validation->set_rules('cnpj','CNPJ','required');
        $this->form_validation->set_rules('junta_comercial','Junta Comercial','required');
        $this->form_validation->set_rules('inscricao_estadual','Inscrição Estadual','required');
        $this->form_validation->set_rules('capital_social','Capital Social','required');
        $this->form_validation->set_rules('ramo_atividade','Ramo de Atividade','required');
        $this->form_validation->set_rules('cep','CEP','required');
        $this->form_validation->set_rules('logradouro','Logradouro','required');
        $this->form_validation->set_rules('numero','Número','required');
        $this->form_validation->set_rules('bairro','Bairro','required');
        $this->form_validation->set_rules('id_pais','País','required');
        $this->form_validation->set_rules('email_comercial','E-mail Comercial','required');
        $this->form_validation->set_rules('telefone_comercial','Telefone Comercial','required');
        $this->form_validation->set_rules('nome_completo','Nome Completo','required');
        $this->form_validation->set_rules('telefone_fixo','Telefone Fixo','required');
        $this->form_validation->set_rules('celular','Celular','required');
        $this->form_validation->set_rules('email','E-mail','required');
        $this->form_validation->set_rules('data_nascimento','Data de Nascimento','required');
        $this->form_validation->set_rules('nacionalidade','Nacionalidade','required');
        $this->form_validation->set_rules('cpf','CPF','required');
        $this->form_validation->set_rules('rg','RG','required');
        $this->form_validation->set_rules('expedicao_rg','Expedição RG','required');
        $this->form_validation->set_rules('data_expedicao','Data Expedição RG','required');
        $this->form_validation->set_rules('id_estado_civil','Estado Civil','required');
        $this->form_validation->set_rules('profissao','Profissão','required');
        $this->form_validation->set_rules('cargo','Cargo','required');
        $this->form_validation->set_rules('salario','Salario','required');
        $this->form_validation->set_rules('residencia','Residência Própria','required');
        $this->form_validation->set_rules('cep_cliente','CEP do Cliente','required');
        $this->form_validation->set_rules('logradouro_cliente','Logradouro do Sócio Representante','required');
        $this->form_validation->set_rules('numero_cliente','Número Casa do Sócio Representante','required');
        $this->form_validation->set_rules('bairro_cliente','Bairro do Sócio Representante','required');
        $this->form_validation->set_rules('id_pais_cliente','País do Sócio Representante','required');
        $this->form_validation->set_rules('outras_rendas','Outras Rendas','required');
        $this->form_validation->set_rules('renda_total','Renda Total','required');
        $this->form_validation->set_rules('socios','Quantidade de Sócios','required');
//        $this->form_validation->set_rules('nome_completo_diretor[]','Nome Completo do Sócio','required');
//        $this->form_validation->set_rules('cargo_diretor[]','Cargo do Sócio','required');
//        $this->form_validation->set_rules('data_nascimento_diretor[]','Data de Nascimento do Sócio','required');
//        $this->form_validation->set_rules('nacionalidade_diretor[]','Nacionalidade do Sócio','required');
//        $this->form_validation->set_rules('cpf_diretor[]','CPF do Sócio','required');
//        $this->form_validation->set_rules('rg_diretor[]','RG do Sócio','required');
//        $this->form_validation->set_rules('id_estado_civil_diretor[]','Estado Civil do Sócio','required');
//        $this->form_validation->set_rules('cep_diretor[]','CEP do Sócio','required');
//        $this->form_validation->set_rules('logradouro_diretor[]','Logradouro do Sócio','required');
//        $this->form_validation->set_rules('numero_diretor[]','Número do Sócio','required');
//        $this->form_validation->set_rules('bairro_diretor[]','Bairro do Sócio','required');
//        $this->form_validation->set_rules('id_pais_diretor[]','País do Sócio','required');
//        $this->form_validation->set_rules('email_pessoal_diretor[]','E-mail Pessoal do Sócio','required');
//        $this->form_validation->set_rules('email_comercial_diretor[]','E-mail Comercial do Sócio','required');
//        $this->form_validation->set_rules('telefone_diretor[]','Telefone do Sócio','required');
        $this->form_validation->set_rules('matricula_imovel[]','Matrícula do Imovel','required');
        $this->form_validation->set_rules('circunscricao[]','Circunscricao','required');
        $this->form_validation->set_rules('veiculo_marca[]','Marca do Veículo','required');
        $this->form_validation->set_rules('renavam[]','Renavam','required');
        $this->form_validation->set_rules('nome_referencia[]','Nome da Referência','required');
        $this->form_validation->set_rules('endereco_referencia[]','Endereço da Referência','required');
        $this->form_validation->set_rules('telefone_referencia[]','Telefone da Referência','required');
        $this->form_validation->set_rules('banco_referencia[]','Banco','required');
        $this->form_validation->set_rules('telefone_banco_referencia[]','Telefone do Banco','required');
        $this->form_validation->set_rules('nome_agencia_referencia[]','Nome da Agência','required');
        $this->form_validation->set_rules('numero_conta_referencia[]','Número da Conta','required');
        $this->form_validation->set_rules('concordo','Concordar com os Termos','required');
    }
}
