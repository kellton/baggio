<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Controller responsável pelas operações referentes a documetação do cliente 
 * pessoa física
 *
 * PHP version 7
 *
 * @category PHP
 * @author   Kellton Leitão <kelltonl10@gmail.com>
 */
class DocumentacaoController extends CI_Controller {

     public function __construct() {
        parent::__construct();
        if(!usuario_sessao_cliente()) {
            $this->session->set_flashdata('param_msg_erro_cabecalho', 'Sem Permissão');
            $this->session->set_flashdata('param_msg_erro', 'Você não tem permissão de acessar esta área!');
            redirect(base_url());
        }
        if(usuario_sessao_cliente()['cliente']!=="Pessoa Física"){
            $this->session->set_flashdata('param_msg_erro_cabecalho', 'Sem Permissão');
            $this->session->set_flashdata('param_msg_erro', 'Você não tem permissão de acessar esta área!');
            redirect(base_url());
        }
        $this->load->model('ClienteModel','cliente');
    }
    
    /**
     * Carrega a página de visualizaçao/adição da documentação
     *
     * @param nenhum
     * @return views
     */
    public function adicionar_documentacao($valor=null) {
        if($this->uri->segment(3)!=0 && $this->uri->segment(3)!=1){
            redirect('cli/documentacao');
        }
        $id_cliente=usuario_sessao_cliente()['id_cliente'];
        $id_pretendente=$this->cliente->get_id_pretendente($id_cliente,$this->uri->segment(3));
        $dados['tem_documentos']= $this->cliente->verifica_se_tem_documemtos_rg_cnh($id_cliente,$id_pretendente['id_pretendente']);
        $dados['tem_documentos_conjugue']= $this->cliente->verifica_se_tem_documemtos_rg_cnh_conjugue($id_pretendente['id_pretendente']);
        $dados['tem_comprovante']= $this->cliente->verifica_se_tem_comrprovante_residencia($id_cliente,$id_pretendente['id_pretendente']);
        $dados['tem_estado_civil']= $this->cliente->verifica_se_tem_estado_civil($id_cliente,$id_pretendente['id_pretendente']);
        $dados['tem_renda']= $this->cliente->verifica_se_tem_renda($id_cliente,$id_pretendente['id_pretendente']);
        $dados['tem_renda_conjugue']= $this->cliente->verifica_se_tem_renda_conjugue($id_pretendente['id_pretendente']);
        $dados['tem_outros_documentos']= $this->cliente->verifica_se_tem_outros_documentos($id_cliente,$id_pretendente['id_pretendente']);
        $dados['tem_conjugue']= $this->cliente->verifica_se_segundo_pretendente_tem_conjugue($id_cliente);
        $dados['tem_residentes']=$this->cliente->verifica_se_tem_documentos_residente($id_cliente);
        $dados['documentos']=$this->cliente->get_documentos($id_cliente,$id_pretendente['id_pretendente']);
        $dados['residentes']=$this->cliente->get_residentes($id_cliente);
        $dados['documentos_residentes']=$this->cliente->get_documentos_residentes($id_cliente);
        $dados['documentos_conjugue']= $this->cliente->get_documentos_conjugue($id_pretendente['id_pretendente']);
        $dados['error']=$valor;
        $dados['id_cliente']=$id_cliente;
        
        /******************CARREGAMENTO DE MODALS E VIEWS**********************/
        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $this->load->view('template/menu');
        $this->load->view('adm_cliente/adicionar_documentacao',$dados);
        $this->load->view('adm_cliente/modais/modal_rg_cpf');
        $this->load->view('adm_cliente/modais/modal_cnh');
        $this->load->view('adm_cliente/modais/modal_documentos');
        $this->load->view('adm_cliente/modais/modal_endereco');
        $this->load->view('adm_cliente/modais/modal_estado_civil');
        $this->load->view('adm_cliente/modais/modal_visualizar_endereco');
        $this->load->view('adm_cliente/modais/modal_visualizar_estado_civil');
        $this->load->view('adm_cliente/modais/modal_comprovante_renda');
        $this->load->view('adm_cliente/modais/modal_visualizar_comprovante_renda');
        $this->load->view('adm_cliente/modais/modal_documentacao_residentes');
        $this->load->view('adm_cliente/modais/modal_visualizar_comprovante_residente');
        $this->load->view('adm_cliente/modais/modal_outros_documentos');
        $this->load->view('adm_cliente/modais/modal_visualizar_outros_documentos');
        $this->load->view('adm_cliente/modais/modal_visualizar_documentos_conjugue');
        $this->load->view('adm_cliente/modais/modal_cnh_conjugue');
        $this->load->view('adm_cliente/modais/modal_rg_cpf_conjugue');
        $this->load->view('adm_cliente/modais/modal_comprovante_renda_conjugue');
        $this->load->view('adm_cliente/modais/modal_visualizar_comprovante_renda_conjugue');
        $this->load->view('template/footer');
        /********************************FIM***********************************/
    }
    
    /**
     * Faz a validação do RG e CPF do Cliente
     *
     * @param
     * @return BOOLEAN
     */
    public function validacao_rg_cpf() {
        $dados=$this->_upload_rg_cpf(usuario_sessao_cliente()['id_cliente']);
        if(isset($dados['error'])){
            $this->adicionar_documentacao($dados['error']);
        }
        else{
            $id_pretendente=$this->cliente->get_id_pretendente(usuario_sessao_cliente()['id_cliente'], $this->input->post('principal'));
            $data_rg['id_cliente']=usuario_sessao_cliente()['id_cliente'];
            $data_rg['tipo_documento']=1;
            $data_rg['id_pretendente']=$id_pretendente['id_pretendente'];
            $data_rg['url_documento']=$dados['rg']['file_name'];
            $data_cpf['id_cliente']=usuario_sessao_cliente()['id_cliente'];
            $data_cpf['tipo_documento']=2;
            $data_cpf['id_pretendente']=$id_pretendente['id_pretendente'];
            $data_cpf['url_documento']=$dados['cpf']['file_name'];
            $this->cliente->inserir_documentos($data_rg);
            $this->cliente->inserir_documentos($data_cpf);
            $this->_enviar_emails();
            $this->session->set_flashdata('mensagem','Documentos Adicionados com Sucesso!!!');
            redirect('cli/adicionar-documentacao/'.$this->input->post('principal'));
        }
    }
    
    /**
     * Faz a validação da CNH do Cliente
     *
     * @param
     * @return BOOLEAN
     */
    public function validacao_cnh() {
        $dados=$this->_upload_cnh(usuario_sessao_cliente()['id_cliente']);
        if(isset($dados['error'])){
            $this->adicionar_documentacao($dados['error']);
        }
        else{
            $id_pretendente=$this->cliente->get_id_pretendente(usuario_sessao_cliente()['id_cliente'], $this->input->post('principal'));
            $data_cnh['id_cliente']=usuario_sessao_cliente()['id_cliente'];
            $data_cnh['tipo_documento']=3;
            $data_cnh['id_pretendente']=$id_pretendente['id_pretendente'];
            $data_cnh['url_documento']=$dados['cnh']['file_name'];
            $this->cliente->inserir_documentos($data_cnh);
            $this->_enviar_emails();
            $this->session->set_flashdata('mensagem','Documentos Adicionados com Sucesso!!!');
            redirect('cli/adicionar-documentacao/'.$this->input->post('principal'));
        }
    }
    
    /**
     * Faz a validaçao do Comprovante de endereço do Cliente
     *
     * @param
     * @return BOOLEAN
     */
    public function validacao_endereco() {
        $this->form_validation->set_rules('tipo_endereco','Comprovante de Endereço','required');
        $dados=$this->_upload_endereco(usuario_sessao_cliente()['id_cliente']);
        if(isset($dados['error']) || $this->form_validation->run()==FALSE){
            $this->adicionar_documentacao($dados['error']);
        }
        else{
            $id_pretendente=$this->cliente->get_id_pretendente(usuario_sessao_cliente()['id_cliente'], $this->input->post('principal'));
            $data['tipo_documento']= $this->input->post('tipo_endereco'); 
            $data['id_cliente']=usuario_sessao_cliente()['id_cliente'];
            $data['id_pretendente']=$id_pretendente['id_pretendente'];
            $data['url_documento']=$dados['comprovante_endereco']['file_name'];
            $this->cliente->inserir_documentos($data);
            $this->_enviar_emails();
            $this->session->set_flashdata('mensagem','Comprovante de Residência Adicionado com Sucesso!!!');
            redirect('cli/adicionar-documentacao/'.$this->input->post('principal'));
        }
    }
    
    /**
     * Faz a validação do Comprovante de estado civil do Cliente
     *
     * @param
     * @return BOOLEAN
     */
    public function validacao_estado_civil() {
        $this->form_validation->set_rules('tipo_estado_civil','Comprovante de Estado Civil','required');
        $dados=$this->_upload_comprovante_civil(usuario_sessao_cliente()['id_cliente']);
        if(isset($dados['error']) || $this->form_validation->run()==FALSE){
            $this->adicionar_documentacao($dados['error']);
        }
        else{
            $id_pretendente=$this->cliente->get_id_pretendente(usuario_sessao_cliente()['id_cliente'], $this->input->post('principal'));
            $data['tipo_documento']=$this->input->post('tipo_estado_civil');
            $data['id_cliente']=usuario_sessao_cliente()['id_cliente'];
            $data['id_pretendente']=$id_pretendente['id_pretendente'];
            $contador=count($dados)-1;
            for($i=0;$i<$contador;$i++){
                $data['url_documento']=$dados[$i]['file_name'];
                $this->cliente->inserir_documentos($data);
            }
            $this->_enviar_emails();
            $this->session->set_flashdata('mensagem','Comprovante de Estado Civil Adicionado com Sucesso!!!');
            redirect('cli/adicionar-documentacao/'.$this->input->post('principal'));
        }
    }
    
    /**
     * Faz a validação da comprovação de renda do Cliente
     *
     * @param
     * @return BOOLEAN
     */
    public function validacao_comprovacao_renda() {
        $this->form_validation->set_rules('categoria','Categoria','required');
        if($this->input->post('categoria')==='Assalariado'){
            $this->form_validation->set_rules('funcionario_publico','Funcionário Público','required');
            $this->form_validation->set_rules('imposto_renda','Imposto de Renda','required');
            $dados=$this->_upload_comprovante_assalariado(usuario_sessao_cliente()['id_cliente']);
        }
        if($this->input->post('categoria')==='Empresário'){
            $dados=$this->_upload_comprovante_empresario(usuario_sessao_cliente()['id_cliente']);
        }
        if($this->input->post('categoria')==='Profissional Autônomo ou Profissional Liberal'){
            $dados=$this->_upload_comprovante_autonomo(usuario_sessao_cliente()['id_cliente']);
        }
        if($this->input->post('categoria')==='Aposentado'){
            $dados=$this->_upload_comprovante_aposentado(usuario_sessao_cliente()['id_cliente']);
        }
        if(isset($dados['error']) || $this->form_validation->run()==FALSE){
            if(!isset($dados['error'])){
                $dados['error']=NULL;
            }
            $this->adicionar_documentacao($dados['error']);
        }
        else{
            $id_pretendente=$this->cliente->get_id_pretendente(usuario_sessao_cliente()['id_cliente'], $this->input->post('principal'));
            $cont=count($dados)-1;
            for($i=0;$i<$cont;$i++){
                $data['tipo_documento']=$dados[$i]['tipo'];
                $data['id_cliente']=usuario_sessao_cliente()['id_cliente'];
                $data['url_documento']=$dados[$i]['file_name'];
                $data['id_pretendente']=$id_pretendente['id_pretendente'];
                $this->cliente->inserir_documentos($data);
            }
            $this->_enviar_emails();
            $this->session->set_flashdata('mensagem','Comprovação de Renda Adicionada com Sucesso!!!');
            redirect('cli/adicionar-documentacao/'.$this->input->post('principal'));
        }
    }
    
    /**
     * Faz a validação dos documentos dos residentes do cliente
     *
     * @param
     * @return BOOLEAN
     */
    public function validacao_documentacao_residente() {
        $dados=$this->_upload_documentacao_residente(usuario_sessao_cliente()['id_cliente']);
        if(isset($dados['error'])){
            $this->adicionar_documentacao($dados['error']);
        }
        else{
            $contador=count($dados)-1;
            for($i=0;$i<$contador;$i++){
                $data['url_documento']=$dados[$i]['file_name'];
                $data['id_cliente']=usuario_sessao_cliente()['id_cliente'];
                $this->cliente->inserir_documentos_residentes($data);
            }
            $this->_enviar_emails();
            $this->session->set_flashdata('mensagem','Documentação dos Residentes Adicionado com Sucesso!!!');
            redirect('cli/adicionar-documentacao/1');
        }
    }
    
    /**
     * Faz a validação de outros documentos do cliente
     *
     * @param
     * @return BOOLEAN
     */
    public function validacao_outros_documentos() {
        $dados=$this->_upload_outros_documentos(usuario_sessao_cliente()['id_cliente']);
        if(isset($dados['error'])){
            $this->adicionar_documentacao($dados['error']);
        }
        else{
            $id_pretendente=$this->cliente->get_id_pretendente(usuario_sessao_cliente()['id_cliente'], $this->input->post('principal'));
            $contador=count($dados)-1;
            for($i=0;$i<$contador;$i++){
                $data['tipo_documento']=$dados[$i]['tipo'];
                $data['id_cliente']=usuario_sessao_cliente()['id_cliente'];
                $data['url_documento']=$dados[$i]['file_name'];
                $data['id_pretendente']=$id_pretendente['id_pretendente'];
                $this->cliente->inserir_documentos($data);
            }
            $this->_enviar_emails();
            $this->session->set_flashdata('mensagem','Outros Documentos Adicionado com Sucesso!!!');
            redirect('cli/adicionar-documentacao/1');
        }
    }
    
    /**
     * Faz a validação do RG e CPF do conjugue do cliente
     *
     * @param
     * @return BOOLEAN
     */
    public function validacao_rg_cpf_conjugue() {
        $dados=$this->_upload_rg_cpf(usuario_sessao_cliente()['id_cliente']);
        if(isset($dados['error'])){
            $this->adicionar_documentacao($dados['error']);
        }
        else{
            $id_pretendente=$this->cliente->get_id_pretendente(usuario_sessao_cliente()['id_cliente'], $this->input->post('principal'));
            $data_rg['tipo_documento']=1;
            $data_rg['id_pretendente']=$id_pretendente['id_pretendente'];
            $data_rg['url_documento']=$dados['rg']['file_name'];
            $data_cpf['tipo_documento']=2;
            $data_cpf['id_pretendente']=$id_pretendente['id_pretendente'];
            $data_cpf['url_documento']=$dados['cpf']['file_name'];
            $this->cliente->inserir_documentos_conjugues($data_rg);
            $this->cliente->inserir_documentos_conjugues($data_cpf);
            $this->session->set_flashdata('mensagem','RG e CPF Cônjugue Adicionados com Sucesso!!!');
            redirect('cli/adicionar-documentacao/'.$this->input->post('principal'));
        }
        $this->_enviar_emails();
    }
    
    /**
     * Faz a validação da CNH do conjugue do cliente
     *
     * @param
     * @return BOOLEAN
     */
    public function validacao_cnh_conjugue() {
        $dados=$this->_upload_cnh(usuario_sessao_cliente()['id_cliente']);
        if(isset($dados['error'])){
            $this->adicionar_documentacao($dados['error']);
        }
        else{
            $id_pretendente=$this->cliente->get_id_pretendente(usuario_sessao_cliente()['id_cliente'], $this->input->post('principal'));
            $data_cnh['tipo_documento']=3;
            $data_cnh['id_pretendente']=$id_pretendente['id_pretendente'];
            $data_cnh['url_documento']=$dados['cnh']['file_name'];
            $this->cliente->inserir_documentos_conjugues($data_cnh);
            $this->_enviar_emails();
            $this->session->set_flashdata('mensagem','CNH do Cônjugue Adicionado com Sucesso!!!');
            redirect('cli/adicionar-documentacao/'.$this->input->post('principal'));
        }
    }
    
    /**
     * Faz a validação do comprovante de renda do conjugue do cliente
     *
     * @param
     * @return BOOLEAN
     */
    public function validacao_renda_conjugue() {
        $this->form_validation->set_rules('conjugue_renda','Cônjugue Possui Renda','required');
        if($this->input->post('conjugue_renda')=='0'){
            $id_pretendente=$this->cliente->get_id_pretendente(usuario_sessao_cliente()['id_cliente'], $this->input->post('principal'));
            $data['tipo_documento']=22;
            $data['url_documento']=NULL;
            $data['id_pretendente']=$id_pretendente['id_pretendente'];
            $this->cliente->inserir_documentos_conjugues($data);
            $this->_enviar_emails();
            $this->session->set_flashdata('mensagem','Compravaçao de Renda Adicionada com Sucesso!!!');
            redirect('cli/adicionar-documentacao/'.$this->input->post('principal'));
        }
        else{
            $this->form_validation->set_rules('categoria','Categoria','required');
            if($this->input->post('categoria')==='Assalariado'){
                $this->form_validation->set_rules('funcionario_publico','Funcionário Público','required');
                $this->form_validation->set_rules('imposto_renda','Imposto de Renda','required');
                $dados=$this->_upload_comprovante_assalariado(usuario_sessao_cliente()['id_cliente']);
            }
            if($this->input->post('categoria')==='Empresário'){
                $dados=$this->_upload_comprovante_empresario(usuario_sessao_cliente()['id_cliente']);
            }
            if($this->input->post('categoria')==='Profissional Autônomo ou Profissional Liberal'){
                $dados=$this->_upload_comprovante_autonomo(usuario_sessao_cliente()['id_cliente']);
            }
            if($this->input->post('categoria')==='Aposentado'){
                $dados=$this->_upload_comprovante_aposentado(usuario_sessao_cliente()['id_cliente']);
            }
            if(isset($dados['error']) || $this->form_validation->run()==FALSE){
                if(!isset($dados['error'])){
                    $dados['error']=NULL;
                }
                $this->adicionar_documentacao($dados['error']);
            }
            else{
                $id_pretendente=$this->cliente->get_id_pretendente(usuario_sessao_cliente()['id_cliente'], $this->input->post('principal'));
                $cont=count($dados)-1;
                for($i=0;$i<$cont;$i++){
                    $data['tipo_documento']=$dados[$i]['tipo'];
                    $data['url_documento']=$dados[$i]['file_name'];
                    $data['id_pretendente']=$id_pretendente['id_pretendente'];
                    $this->cliente->inserir_documentos_conjugues($data);
                }
                $this->_enviar_emails();
                $this->session->set_flashdata('mensagem','Compravaçao de Renda Adicionada com Sucesso!!!');
                redirect('cli/adicionar-documentacao/'.$this->input->post('principal'));
            }
        }
    }
    
    /**
     * Faz a exclusão da documetação do cliente
     *
     * @param int $tipo tipo de documento a ser excluido
     * @param int $principal indentificação do pretendente
     * @return BOOLEAN
     */
    public function excluir_documentacao($tipo,$principal) {
        $id_pretendente=$this->cliente->get_id_pretendente(usuario_sessao_cliente()['id_cliente'], $principal);
        if($tipo==1){
            $this->cliente->excluir_documetacao($id_pretendente['id_pretendente'],1);
            $this->cliente->excluir_documetacao($id_pretendente['id_pretendente'],2);
            $this->cliente->excluir_documetacao($id_pretendente['id_pretendente'],3);
        }
        if($tipo==4){
            $this->cliente->excluir_documetacao($id_pretendente['id_pretendente'],4);
            $this->cliente->excluir_documetacao($id_pretendente['id_pretendente'],5);
            $this->cliente->excluir_documetacao($id_pretendente['id_pretendente'],6);
        }
        if($tipo==7){
            $this->cliente->excluir_documetacao($id_pretendente['id_pretendente'],7);
            $this->cliente->excluir_documetacao($id_pretendente['id_pretendente'],8);
            $this->cliente->excluir_documetacao($id_pretendente['id_pretendente'],9);
            $this->cliente->excluir_documetacao($id_pretendente['id_pretendente'],10);
            $this->cliente->excluir_documetacao($id_pretendente['id_pretendente'],11);
            $this->cliente->excluir_documetacao($id_pretendente['id_pretendente'],12);
        }
        if($tipo==13){
            $this->cliente->excluir_documetacao($id_pretendente['id_pretendente'],13);
            $this->cliente->excluir_documetacao($id_pretendente['id_pretendente'],14);
            $this->cliente->excluir_documetacao($id_pretendente['id_pretendente'],15);
            $this->cliente->excluir_documetacao($id_pretendente['id_pretendente'],16);
            $this->cliente->excluir_documetacao($id_pretendente['id_pretendente'],17);
            $this->cliente->excluir_documetacao($id_pretendente['id_pretendente'],18);
            $this->cliente->excluir_documetacao($id_pretendente['id_pretendente'],19);
            $this->cliente->excluir_documetacao($id_pretendente['id_pretendente'],20);
        }
        if($tipo==21){
            $this->cliente->excluir_documetacao($id_pretendente['id_pretendente'],21);
        }
        if($tipo==0){
            $this->cliente->excluir_documetacao_residentes(usuario_sessao_cliente()['id_cliente']);
        }
        if($tipo==1 && $principal==0){
            $this->cliente->excluir_documetacao_conjugue($id_pretendente['id_pretendente'],1);
            $this->cliente->excluir_documetacao_conjugue($id_pretendente['id_pretendente'],2);
            $this->cliente->excluir_documetacao_conjugue($id_pretendente['id_pretendente'],3);
        }
        if($tipo==13 && $principal==0){
            $this->cliente->excluir_documetacao_conjugue($id_pretendente['id_pretendente'],13);
            $this->cliente->excluir_documetacao_conjugue($id_pretendente['id_pretendente'],14);
            $this->cliente->excluir_documetacao_conjugue($id_pretendente['id_pretendente'],15);
            $this->cliente->excluir_documetacao_conjugue($id_pretendente['id_pretendente'],16);
            $this->cliente->excluir_documetacao_conjugue($id_pretendente['id_pretendente'],17);
            $this->cliente->excluir_documetacao_conjugue($id_pretendente['id_pretendente'],18);
            $this->cliente->excluir_documetacao_conjugue($id_pretendente['id_pretendente'],19);
            $this->cliente->excluir_documetacao_conjugue($id_pretendente['id_pretendente'],20);
        }
        
        $this->session->set_flashdata('mensagem','Documentos Excluidos com Sucesso!!!');
        redirect('cli/adicionar-documentacao/'.$principal);
    }
    
    /**
     * Faz o upload do RG e CPF do cliente
     *
     * @param int $id_cliente ID único do cliente
     * @return ARRAY
     */
    public function _upload_rg_cpf($id_cliente){
        if(!is_dir("publico/uploads/".md5($id_cliente))){
            mkdir("publico/uploads/".md5($id_cliente), 0700);
        }
        $config['upload_path']          = 'publico/uploads/'.md5($id_cliente);
        $config['allowed_types']        = 'jpg|png|pdf';
        $config['max_size']             = 100000;
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('rg')){
            $error = array('error' => $this->upload->display_errors());
            return $error;
        }
        $dados['rg'] = $this->upload->data();
        if ( ! $this->upload->do_upload('cpf')){
            $error = array('error' => $this->upload->display_errors());
            return $error;
        }
        $dados['cpf']=$this->upload->data();
        return $dados;
    }
    
    /**
     * Faz o upload da CNH do cliente
     *
     * @param int $id_cliente ID único do cliente
     * @return ARRAY
     */
    public function _upload_cnh($id_cliente){
        if(!is_dir("publico/uploads/".md5($id_cliente))){
            mkdir("publico/uploads/".md5($id_cliente), 0700);
        }
        $config['upload_path']          = 'publico/uploads/'.md5($id_cliente);
        $config['allowed_types']        = 'jpg|png|pdf';
        $config['max_size']             = 100000;
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('cnh')){
            $error = array('error' => $this->upload->display_errors());
            return $error;
        }
        else{
            $dados['cnh'] = $this->upload->data();
            return $dados;
        }
    }
    
    /**
     * Faz o upload do comprovante de endereço do cliente
     *
     * @param int $id_cliente ID único do cliente
     * @return ARRAY
     */
    public function _upload_endereco($id_cliente){
        if(!is_dir("publico/uploads/".md5($id_cliente))){
            mkdir("publico/uploads/".md5($id_cliente), 0700);
        }
        $config['upload_path']          = 'publico/uploads/'.md5($id_cliente);
        $config['allowed_types']        = 'jpg|png|pdf';
        $config['max_size']             = 100000;
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('comprovante_endereco')){
            $error = array('error' => $this->upload->display_errors());
            return $error;
        }
        else{
            $dados['error']=NULL;
            $dados['comprovante_endereco'] = $this->upload->data();
            return $dados;
        }
    }
    
    /**
     * Faz o upload do comprovante de estado civil do cliente
     *
     * @param int $id_cliente ID único do cliente
     * @return ARRAY
     */
    public function _upload_comprovante_civil($id_cliente){
        $filesCount = count($_FILES['estado_civil']['name']);
        for($i = 0; $i < $filesCount; $i++){
            $_FILES['estado_civil_aux']['name'] = $_FILES['estado_civil']['name'][$i];
            $_FILES['estado_civil_aux']['type'] = $_FILES['estado_civil']['type'][$i];
            $_FILES['estado_civil_aux']['tmp_name'] = $_FILES['estado_civil']['tmp_name'][$i];
            $_FILES['estado_civil_aux']['error'] = $_FILES['estado_civil']['error'][$i];
            $_FILES['estado_civil_aux']['size'] = $_FILES['estado_civil']['size'][$i];

            if(!is_dir("publico/uploads/".md5($id_cliente))){
                mkdir("publico/uploads/".md5($id_cliente), 0700);
            }
            $config['upload_path']          = 'publico/uploads/'.md5($id_cliente);
            $config['allowed_types']        = 'jpg|png|pdf';
            $config['max_size']             = 100000;
            $this->load->library('upload', $config);
            if(! $this->upload->do_upload('estado_civil_aux')){
                 $error = array('error' => $this->upload->display_errors());
                return $error;
            }
            $dados[$i] = $this->upload->data();
        }
        $dados['error']=NULL;
        return $dados;
    }
    
    /**
     * Faz o upload do comprovante de salário do cliente(Contra-Cheque)
     *
     * @param int $id_cliente ID único do cliente
     * @return ARRAY
     */
    public function _upload_comprovante_assalariado($id_cliente){
        if(!is_dir("publico/uploads/".md5($id_cliente))){
                mkdir("publico/uploads/".md5($id_cliente), 0700);
            }
            $config['upload_path']          = 'publico/uploads/'.md5($id_cliente);
            $config['allowed_types']        = 'jpg|png|pdf';
            $config['max_size']             = 100000;
            $this->load->library('upload', $config);
        $aux=0;
        $filesCountHolerite = count($_FILES['holerite']['name']);
        for($i = 0; $i < $filesCountHolerite; $i++){
            $_FILES['holerite_aux']['name'] = $_FILES['holerite']['name'][$i];
            $_FILES['holerite_aux']['type'] = $_FILES['holerite']['type'][$i];
            $_FILES['holerite_aux']['tmp_name'] = $_FILES['holerite']['tmp_name'][$i];
            $_FILES['holerite_aux']['error'] = $_FILES['holerite']['error'][$i];
            $_FILES['holerite_aux']['size'] = $_FILES['holerite']['size'][$i];
            if(! $this->upload->do_upload('holerite_aux')){
                 $error = array('error' => $this->upload->display_errors());
                return $error;
            }
            $dados[$i] = $this->upload->data();
            $dados[$i]['tipo'] = 13;
            $aux++;
        }
        $aux=$aux-1;
        if($this->input->post('funcionario_publico')==0){
            $filesCountCarteira = count($_FILES['carteira_trabalho']['name']);
            for($i = 0; $i < $filesCountCarteira; $i++){
                $_FILES['carteira_trabalho_aux']['name'] = $_FILES['carteira_trabalho']['name'][$i];
                $_FILES['carteira_trabalho_aux']['type'] = $_FILES['carteira_trabalho']['type'][$i];
                $_FILES['carteira_trabalho_aux']['tmp_name'] = $_FILES['carteira_trabalho']['tmp_name'][$i];
                $_FILES['carteira_trabalho_aux']['error'] = $_FILES['carteira_trabalho']['error'][$i];
                $_FILES['carteira_trabalho_aux']['size'] = $_FILES['carteira_trabalho']['size'][$i];
                if(! $this->upload->do_upload('carteira_trabalho_aux')){
                    $error = array('error' => $this->upload->display_errors());
                    return $error;
                }
                $aux++;
                $dados[$aux] = $this->upload->data();
                $dados[$aux]['tipo'] = 14;
            }
        }
        if($this->input->post('imposto_renda')==1){
            $filesCountImposto = count($_FILES['imposto_renda_file']['name']);
            for($i = 0; $i < $filesCountImposto; $i++){
                $_FILES['imposto_renda_file_aux']['name'] = $_FILES['imposto_renda_file']['name'][$i];
                $_FILES['imposto_renda_file_aux']['type'] = $_FILES['imposto_renda_file']['type'][$i];
                $_FILES['imposto_renda_file_aux']['tmp_name'] = $_FILES['imposto_renda_file']['tmp_name'][$i];
                $_FILES['imposto_renda_file_aux']['error'] = $_FILES['imposto_renda_file']['error'][$i];
                $_FILES['imposto_renda_file_aux']['size'] = $_FILES['imposto_renda_file']['size'][$i];
                if(! $this->upload->do_upload('imposto_renda_file_aux')){
                     $error = array('error' => $this->upload->display_errors());
                    return $error;
                }
                $aux++;
                $dados[$aux] = $this->upload->data();
                $dados[$aux]['tipo'] = 15;
            }
        }
        $dados['error']=NULL;
        return $dados;
    }
    
    /**
     * Faz o upload do comprovante de renda do cliente que é empresário
     *
     * @param int $id_cliente ID único do cliente
     * @return ARRAY
     */
    public function _upload_comprovante_empresario($id_cliente){
        if(!is_dir("publico/uploads/".md5($id_cliente))){
                mkdir("publico/uploads/".md5($id_cliente), 0700);
            }
        $config['upload_path']          = 'publico/uploads/'.md5($id_cliente);
        $config['allowed_types']        = 'jpg|png|pdf';
        $config['max_size']             = 100000;
        $this->load->library('upload', $config);
        $aux=0;
        $filesCountContrato = count($_FILES['contrato_social']['name']);
        for($i = 0; $i < $filesCountContrato; $i++){
            $_FILES['contrato_social_aux']['name'] = $_FILES['contrato_social']['name'][$i];
            $_FILES['contrato_social_aux']['type'] = $_FILES['contrato_social']['type'][$i];
            $_FILES['contrato_social_aux']['tmp_name'] = $_FILES['contrato_social']['tmp_name'][$i];
            $_FILES['contrato_social_aux']['error'] = $_FILES['contrato_social']['error'][$i];
            $_FILES['contrato_social_aux']['size'] = $_FILES['contrato_social']['size'][$i];
            if(! $this->upload->do_upload('contrato_social_aux')){
                 $error = array('error' => $this->upload->display_errors());
                return $error;
            }
            $dados[$i] = $this->upload->data();
            $dados[$i]['tipo'] = 16;
            $aux++;
        }
        $aux=$aux-1;
        $filesCountDecore = count($_FILES['decore_empresario']['name']);
        for($i = 0; $i < $filesCountDecore; $i++){
            $_FILES['decore_empresario_aux']['name'] = $_FILES['decore_empresario']['name'][$i];
            $_FILES['decore_empresario_aux']['type'] = $_FILES['decore_empresario']['type'][$i];
            $_FILES['decore_empresario_aux']['tmp_name'] = $_FILES['decore_empresario']['tmp_name'][$i];
            $_FILES['decore_empresario_aux']['error'] = $_FILES['decore_empresario']['error'][$i];
            $_FILES['decore_empresario_aux']['size'] = $_FILES['decore_empresario']['size'][$i];
            if(! $this->upload->do_upload('decore_empresario_aux')){
                $error = array('error' => $this->upload->display_errors());
                return $error;
            }
            $aux++;
            $dados[$aux] = $this->upload->data();
            $dados[$aux]['tipo'] = 17;
            }
        $filesCountImposto = count($_FILES['imposto_renda_empresario']['name']);
        for($i = 0; $i < $filesCountImposto; $i++){
            $_FILES['imposto_renda_empresario_aux']['name'] = $_FILES['imposto_renda_empresario']['name'][$i];
            $_FILES['imposto_renda_empresario_aux']['type'] = $_FILES['imposto_renda_empresario']['type'][$i];
            $_FILES['imposto_renda_empresario_aux']['tmp_name'] = $_FILES['imposto_renda_empresario']['tmp_name'][$i];
            $_FILES['imposto_renda_empresario_aux']['error'] = $_FILES['imposto_renda_empresario']['error'][$i];
            $_FILES['imposto_renda_empresario_aux']['size'] = $_FILES['imposto_renda_empresario']['size'][$i];
            $this->load->library('upload', $config);
            if(! $this->upload->do_upload('imposto_renda_empresario_aux')){
                 $error = array('error' => $this->upload->display_errors());
                return $error;
            }
            $aux++;
            $dados[$aux] = $this->upload->data();
            $dados[$aux]['tipo'] = 15;
        }
        $dados['error']=NULL;
        return $dados;
    }
    
    /**
     * Faz o upload do comprovante de renda do cliente que é autônomo
     *
     * @param int $id_cliente ID único do cliente
     * @return ARRAY
     */
    public function _upload_comprovante_autonomo($id_cliente){
        if(!is_dir("publico/uploads/".md5($id_cliente))){
                mkdir("publico/uploads/".md5($id_cliente), 0700);
            }
        $config['upload_path']          = 'publico/uploads/'.md5($id_cliente);
        $config['allowed_types']        = 'jpg|png|pdf';
        $config['max_size']             = 100000;
        $this->load->library('upload', $config);
        $aux=0;
        $filesCountExtrato = count($_FILES['extrato_bancario']['name']);
        for($i = 0; $i < $filesCountExtrato; $i++){
            $_FILES['extrato_bancario_aux']['name'] = $_FILES['extrato_bancario']['name'][$i];
            $_FILES['extrato_bancario_aux']['type'] = $_FILES['extrato_bancario']['type'][$i];
            $_FILES['extrato_bancario_aux']['tmp_name'] = $_FILES['extrato_bancario']['tmp_name'][$i];
            $_FILES['extrato_bancario_aux']['error'] = $_FILES['extrato_bancario']['error'][$i];
            $_FILES['extrato_bancario_aux']['size'] = $_FILES['extrato_bancario']['size'][$i];
            if(! $this->upload->do_upload('extrato_bancario_aux')){
                 $error = array('error' => $this->upload->display_errors());
                return $error;
            }
            $dados[$i] = $this->upload->data();
            $dados[$i]['tipo'] = 18;
            $aux++;
        }
        $aux=$aux-1;
        $filesCountDecore = count($_FILES['decore_autonomo']['name']);
        for($i = 0; $i < $filesCountDecore; $i++){
            $_FILES['decore_autonomo_aux']['name'] = $_FILES['decore_autonomo']['name'][$i];
            $_FILES['decore_autonomo_aux']['type'] = $_FILES['decore_autonomo']['type'][$i];
            $_FILES['decore_autonomo_aux']['tmp_name'] = $_FILES['decore_autonomo']['tmp_name'][$i];
            $_FILES['decore_autonomo_aux']['error'] = $_FILES['decore_autonomo']['error'][$i];
            $_FILES['decore_autonomo_aux']['size'] = $_FILES['decore_autonomo']['size'][$i];
            if(! $this->upload->do_upload('decore_autonomo_aux')){
                $error = array('error' => $this->upload->display_errors());
                return $error;
            }
            $aux++;
            $dados[$aux] = $this->upload->data();
            $dados[$aux]['tipo'] = 17;
            }
        $filesCountImposto = count($_FILES['imposto_renda_autonomo']['name']);
        for($i = 0; $i < $filesCountImposto; $i++){
            $_FILES['imposto_renda_autonomo_aux']['name'] = $_FILES['imposto_renda_autonomo']['name'][$i];
            $_FILES['imposto_renda_autonomo_aux']['type'] = $_FILES['imposto_renda_autonomo']['type'][$i];
            $_FILES['imposto_renda_autonomo_aux']['tmp_name'] = $_FILES['imposto_renda_autonomo']['tmp_name'][$i];
            $_FILES['imposto_renda_autonomo_aux']['error'] = $_FILES['imposto_renda_autonomo']['error'][$i];
            $_FILES['imposto_renda_autonomo_aux']['size'] = $_FILES['imposto_renda_autonomo']['size'][$i];
            $this->load->library('upload', $config);
            if(! $this->upload->do_upload('imposto_renda_autonomo_aux')){
                 $error = array('error' => $this->upload->display_errors());
                return $error;
            }
            $aux++;
            $dados[$aux] = $this->upload->data();
            $dados[$aux]['tipo'] = 15;
        }
        $dados['error']=NULL;
        return $dados;
    }
    
    /**
     * Faz o upload do comprovante de renda do cliente que é aposentado
     *
     * @param int $id_cliente ID único do cliente
     * @return ARRAY
     */
    public function _upload_comprovante_aposentado($id_cliente){
        if(!is_dir("publico/uploads/".md5($id_cliente))){
                mkdir("publico/uploads/".md5($id_cliente), 0700);
            }
        $config['upload_path']          = 'publico/uploads/'.md5($id_cliente);
        $config['allowed_types']        = 'jpg|png|pdf';
        $config['max_size']             = 100000;
        $this->load->library('upload', $config);
        $aux=0;
        $filesCountExtrato = count($_FILES['extrato_beneficio']['name']);
        for($i = 0; $i < $filesCountExtrato; $i++){
            $_FILES['extrato_beneficio_aux']['name'] = $_FILES['extrato_beneficio']['name'][$i];
            $_FILES['extrato_beneficio_aux']['type'] = $_FILES['extrato_beneficio']['type'][$i];
            $_FILES['extrato_beneficio_aux']['tmp_name'] = $_FILES['extrato_beneficio']['tmp_name'][$i];
            $_FILES['extrato_beneficio_aux']['error'] = $_FILES['extrato_beneficio']['error'][$i];
            $_FILES['extrato_beneficio_aux']['size'] = $_FILES['extrato_beneficio']['size'][$i];
            if(! $this->upload->do_upload('extrato_beneficio_aux')){
                 $error = array('error' => $this->upload->display_errors());
                return $error;
            }
            $dados[$i] = $this->upload->data();
            $dados[$i]['tipo'] = 19;
            $aux++;
        }
        $aux=$aux-1;
        $filesCountCartao = count($_FILES['cartao_beneficio']['name']);
        for($i = 0; $i < $filesCountCartao; $i++){
            $_FILES['cartao_beneficio_aux']['name'] = $_FILES['cartao_beneficio']['name'][$i];
            $_FILES['cartao_beneficio_aux']['type'] = $_FILES['cartao_beneficio']['type'][$i];
            $_FILES['cartao_beneficio_aux']['tmp_name'] = $_FILES['cartao_beneficio']['tmp_name'][$i];
            $_FILES['cartao_beneficio_aux']['error'] = $_FILES['cartao_beneficio']['error'][$i];
            $_FILES['cartao_beneficio_aux']['size'] = $_FILES['cartao_beneficio']['size'][$i];
            if(! $this->upload->do_upload('cartao_beneficio_aux')){
                $error = array('error' => $this->upload->display_errors());
                return $error;
            }
            $aux++;
            $dados[$aux] = $this->upload->data();
            $dados[$aux]['tipo'] = 20;
            }
        $filesCountImposto = count($_FILES['imposto_renda_aposentado']['name']);
        for($i = 0; $i < $filesCountImposto; $i++){
            $_FILES['imposto_renda_aposentado_aux']['name'] = $_FILES['imposto_renda_aposentado']['name'][$i];
            $_FILES['imposto_renda_aposentado_aux']['type'] = $_FILES['imposto_renda_aposentado']['type'][$i];
            $_FILES['imposto_renda_aposentado_aux']['tmp_name'] = $_FILES['imposto_renda_aposentado']['tmp_name'][$i];
            $_FILES['imposto_renda_aposentado_aux']['error'] = $_FILES['imposto_renda_aposentado']['error'][$i];
            $_FILES['imposto_renda_aposentado_aux']['size'] = $_FILES['imposto_renda_aposentado']['size'][$i];
            $this->load->library('upload', $config);
            if(! $this->upload->do_upload('imposto_renda_aposentado_aux')){
                 $error = array('error' => $this->upload->display_errors());
                return $error;
            }
            $aux++;
            $dados[$aux] = $this->upload->data();
            $dados[$aux]['tipo'] = 15;
        }
        $dados['error']=NULL;
        return $dados;
    }
    
    /**
     * Faz o upload dos documentos dos residentes na casa do cliente
     *
     * @param int $id_cliente ID único do cliente
     * @return ARRAY
     */
    public function _upload_documentacao_residente($id_cliente){
        if(!is_dir("publico/uploads/".md5($id_cliente))){
                mkdir("publico/uploads/".md5($id_cliente), 0700);
            }
        $config['upload_path']          = 'publico/uploads/'.md5($id_cliente);
        $config['allowed_types']        = 'jpg|png|pdf';
        $config['max_size']             = 100000;
        $this->load->library('upload', $config);
        $filesCountDocumentacao = count($_FILES['documentacao_residente']['name']);
        for($i = 0; $i < $filesCountDocumentacao; $i++){
            $_FILES['documentacao_residente_aux']['name'] = $_FILES['documentacao_residente']['name'][$i];
            $_FILES['documentacao_residente_aux']['type'] = $_FILES['documentacao_residente']['type'][$i];
            $_FILES['documentacao_residente_aux']['tmp_name'] = $_FILES['documentacao_residente']['tmp_name'][$i];
            $_FILES['documentacao_residente_aux']['error'] = $_FILES['documentacao_residente']['error'][$i];
            $_FILES['documentacao_residente_aux']['size'] = $_FILES['documentacao_residente']['size'][$i];
            if(! $this->upload->do_upload('documentacao_residente_aux')){
                 $error = array('error' => $this->upload->display_errors());
                return $error;
            }
            $dados[$i] = $this->upload->data();
            $dados[$i]['cliente'] = $id_cliente;
        }
        $dados['error']=NULL;
        return $dados;
    }
    
    /**
     * Faz o upload de outros documentos do cliente
     *
     * @param int $id_cliente ID único do cliente
     * @return ARRAY
     */
     public function _upload_outros_documentos($id_cliente){
        if(!is_dir("publico/uploads/".md5($id_cliente))){
                mkdir("publico/uploads/".md5($id_cliente), 0700);
            }
        $config['upload_path']          = 'publico/uploads/'.md5($id_cliente);
        $config['allowed_types']        = 'jpg|png|pdf';
        $config['max_size']             = 100000;
        $this->load->library('upload', $config);
        $filesCountExtrato = count($_FILES['outros_documentos']['name']);
        for($i = 0; $i < $filesCountExtrato; $i++){
            $_FILES['outros_documentos_aux']['name'] = $_FILES['outros_documentos']['name'][$i];
            $_FILES['outros_documentos_aux']['type'] = $_FILES['outros_documentos']['type'][$i];
            $_FILES['outros_documentos_aux']['tmp_name'] = $_FILES['outros_documentos']['tmp_name'][$i];
            $_FILES['outros_documentos_aux']['error'] = $_FILES['outros_documentos']['error'][$i];
            $_FILES['outros_documentos_aux']['size'] = $_FILES['outros_documentos']['size'][$i];
            if(! $this->upload->do_upload('outros_documentos_aux')){
                 $error = array('error' => $this->upload->display_errors());
                return $error;
            }
            $dados[$i] = $this->upload->data();
            $dados[$i]['tipo'] = 21;
        }
        $dados['error']=NULL;
        return $dados;
    }
    
    /**
     * Faz o envio dos e-mail quando um cliente adiciona um novo documentos
     *
     * @param
     * @return BOOLEAN
     */
    public function _enviar_emails() {
        /*===ENVIA EMAILS PARA O CORRETOR AVISANDO DE FICHA ADICIONADA====*/
        $email_corretor=$this->cliente->busca_email_corretor(usuario_sessao_cliente()['id_corretor']);
        $dados_email['nome_cliente']=usuario_sessao_cliente()['nome_cliente'];
        $dados_email['email_cliente']=usuario_sessao_cliente()['email'];
        $dados_email['nome_corretor']=$this->cliente->busca_nome_corretor(usuario_sessao_cliente()['id_corretor']);
        $dados_email['link']=base_url('adm/clientes/visualizar-cliente/'.usuario_sessao_cliente()['id_cliente']);
        $mensagem = $this->load->view('template/emails/documento_pretendente_baggio', $dados_email, TRUE);
        $mensagem_cliente = $this->load->view('template/emails/documento_pretendente_cliente', $dados_email, TRUE);
        $array_emails=array($email_corretor['email'],"juarez@baggioimoveis.com.br","andressa@baggioimoveis.com.br");
        enviar_email_fichas(usuario_sessao_cliente()['nome_cliente']." | Documento Pessoa Física", $array_emails, $mensagem);
        enviar_email_fichas("Baggio Imóveis - ".$dados_email['nome_cliente'].", recebemos seus Documentos Pessoa Física",  array($dados_email['email_cliente']), $mensagem_cliente);
    }
}
