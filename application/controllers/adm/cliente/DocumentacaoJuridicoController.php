<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Controller responsável pelas operações referentes a documetação do cliente 
 * pessoa jurídica
 *
 * PHP version 7
 *
 * @category PHP
 * @author   Kellton Leitão <kelltonl10@gmail.com>
 */
class DocumentacaoJuridicoController extends CI_Controller {

     public function __construct() {
        parent::__construct();
        if(!usuario_sessao_cliente()) {
            $this->session->set_flashdata('param_msg_erro_cabecalho', 'Sem Permissão');
            $this->session->set_flashdata('param_msg_erro', 'Você não tem permissão de acessar esta área!');
            redirect(base_url());
        }
        if(usuario_sessao_cliente()['cliente']!=="Pessoa Jurídica"){
            $this->session->set_flashdata('param_msg_erro_cabecalho', 'Sem Permissão');
            $this->session->set_flashdata('param_msg_erro', 'Você não tem permissão de acessar esta área!');
            redirect(base_url());
        }
        $this->load->model('ClienteModel','cliente');
    }
    
    /**
     * Carrega a página de visualização/adição da documentação
     *
     * @param nenhum
     * @return views
     */
    public function index($valor=NULL) {
        $dados['tem_dados']= $this->cliente->verifica_se_tem_dados_juridico(usuario_sessao_cliente()['id_cliente']);
        if($dados['tem_dados']==NULL){
            $this->_sem_ficha_cadastro();
        }
        else{
            $id_cliente=usuario_sessao_cliente()['id_cliente'];
            $dados['error']=$valor;
            $dados['id_cliente']=$id_cliente;
            $dados['documentos']=$this->cliente->get_documentos_juridico($id_cliente);
            $dados['documentos_socios']=$this->cliente->get_documentos_socios($id_cliente);
            $dados['tem_contrato_social']=$this->cliente->verifica_se_tem_contrato_social($id_cliente);
            $dados['tem_alteracao_contrato_social']=$this->cliente->verifica_se_tem_alteracao_contrato_social($id_cliente);
            $dados['tem_balanco']=$this->cliente->verifica_se_tem_balanco($id_cliente);
            $dados['tem_balancete']=$this->cliente->verifica_se_tem_balancete($id_cliente);
            $dados['tem_cartao']=$this->cliente->verifica_se_tem_cartao_cnpj($id_cliente);
            $dados['tem_imposto_renda']=$this->cliente->verifica_se_tem_imposto_renda($id_cliente);
            $dados['tem_certidao_negativa']=$this->cliente->verifica_se_tem_certidao_negativa($id_cliente);
            $dados['tem_outros_documentos']=$this->cliente->verifica_se_tem_outros_documentos_juridico($id_cliente);
            $dados['tem_rg_cnh']=$this->cliente->verifica_se_tem_rg_cnh($id_cliente);
            $dados['tem_comprovante_residencia']=$this->cliente->verifica_se_tem_comprovante_residencia($id_cliente);
            $header["scripts"] = carregar_js();
            $header["styles"] = carregar_css();

            $this->load->view('template/header', $header);
            $this->load->view('template/menu');
            $this->load->view('adm_cliente/juridico/adicionar_documentacao',$dados);
            $this->load->view('template/footer');
            $this->load->view('adm_cliente/juridico/modais/modal_contrato_social');
            $this->load->view('adm_cliente/juridico/modais/modal_visualizar_contrato_social');
            $this->load->view('adm_cliente/juridico/modais/modal_alteracao_contrato_social');
            $this->load->view('adm_cliente/juridico/modais/modal_visualizar_alteracao_contrato_social');
            $this->load->view('adm_cliente/juridico/modais/modal_balanco');
            $this->load->view('adm_cliente/juridico/modais/modal_visualizar_balanco');
            $this->load->view('adm_cliente/juridico/modais/modal_balancete');
            $this->load->view('adm_cliente/juridico/modais/modal_visualizar_balancete');
            $this->load->view('adm_cliente/juridico/modais/modal_cartao_cnpj');
            $this->load->view('adm_cliente/juridico/modais/modal_visualizar_cartao_cnpj');
            $this->load->view('adm_cliente/juridico/modais/modal_imposto_renda');
            $this->load->view('adm_cliente/juridico/modais/modal_visualizar_imposto_renda');
            $this->load->view('adm_cliente/juridico/modais/modal_certidao_negativa');
            $this->load->view('adm_cliente/juridico/modais/modal_visualizar_certidao_negativa');
            $this->load->view('adm_cliente/juridico/modais/modal_rg_cpf');
            $this->load->view('adm_cliente/juridico/modais/modal_cnh');
            $this->load->view('adm_cliente/juridico/modais/modal_visualizar_rg_cpf_cnh');
            $this->load->view('adm_cliente/juridico/modais/modal_endereco');
            $this->load->view('adm_cliente/juridico/modais/modal_visualizar_endereco');
            $this->load->view('adm_cliente/juridico/modais/modal_outros_documentos');
            $this->load->view('adm_cliente/juridico/modais/modal_visualizar_outros_documentos');
        }
    }
    
    /**
     * Caso o cliente aoinda nao tenha preenchido a ficha de cadastro será 
     * carregada essa página
     *
     * @param 
     * @return views
     */
    public function _sem_ficha_cadastro() {
        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $this->load->view('template/menu');
        $this->load->view('adm_cliente/juridico/sem_ficha_cadastro');
        $this->load->view('template/footer');
    }
    
     /**
     * Faz a validação do contrato social da empresa
     *
     * @param
     * @return BOOLEAN
     */
    public function validacao_contrato_social() {
        $dados=$this->_upload_documentos(usuario_sessao_cliente()['id_cliente']);
        if(isset($dados['error'])){
            $this->index($dados['error']);
        }
        else{
            $id_cliente=usuario_sessao_cliente()['id_cliente'];
            $data['tipo_documento']=$this->input->post('tipo_documento');
            $contador=count($dados)-1;
            for($i=0;$i<$contador;$i++){
                $data['url_documento']=$dados[$i]['file_name'];
                $data['id_cliente']=$id_cliente;
                $this->cliente->inserir_documentos_juridico($data);
            }
            $this->_enviar_emails();
            $this->session->set_flashdata('mensagem','Contrato Social Adicionado com Sucesso!!!');
            redirect('cli-juridico/documentacao');
        }
    }
    
    /**
     * Faz a validação da alteração contrato social da empresa
     *
     * @param
     * @return BOOLEAN
     */    
    public function validacao_alteracao_contrato_social() {
        $dados=$this->_upload_documentos(usuario_sessao_cliente()['id_cliente']);
        if(isset($dados['error'])){
            $this->index($dados['error']);
        }
        else{
            $id_cliente=usuario_sessao_cliente()['id_cliente'];
            $data['tipo_documento']=$this->input->post('tipo_documento');
            $contador=count($dados)-1;
            for($i=0;$i<$contador;$i++){
                $data['url_documento']=$dados[$i]['file_name'];
                $data['id_cliente']=$id_cliente;
                $this->cliente->inserir_documentos_juridico($data);
            }
            $this->_enviar_emails();
            $this->session->set_flashdata('mensagem','Alteração do Contrato Social Adicionado com Sucesso!!!');
            redirect('cli-juridico/documentacao');
        }
    }
    
    /**
     * Faz a validação do balanço da empresa
     *
     * @param
     * @return BOOLEAN
     */
    public function validacao_balanco() {
        $dados=$this->_upload_documentos(usuario_sessao_cliente()['id_cliente']);
        if(isset($dados['error'])){
            $this->index($dados['error']);
        }
        else{
            $id_cliente=usuario_sessao_cliente()['id_cliente'];
            $data['tipo_documento']=$this->input->post('tipo_documento');
            $contador=count($dados)-1;
            for($i=0;$i<$contador;$i++){
                $data['url_documento']=$dados[$i]['file_name'];
                $data['id_cliente']=$id_cliente;
                $this->cliente->inserir_documentos_juridico($data);
            }
            $this->_enviar_emails();
            $this->session->set_flashdata('mensagem','Balanço Adicionado com Sucesso!!!');
            redirect('cli-juridico/documentacao');
        }
    }
    
    /**
     * Faz a validação balancete da empresa
     *
     * @param
     * @return BOOLEAN
     */
    public function validacao_balancete() {
        $dados=$this->_upload_documentos(usuario_sessao_cliente()['id_cliente']);
        if(isset($dados['error'])){
            $this->index($dados['error']);
        }
        else{
            $id_cliente=usuario_sessao_cliente()['id_cliente'];
            $data['tipo_documento']=$this->input->post('tipo_documento');
            $contador=count($dados)-1;
            for($i=0;$i<$contador;$i++){
                $data['url_documento']=$dados[$i]['file_name'];
                $data['id_cliente']=$id_cliente;
                $this->cliente->inserir_documentos_juridico($data);
            }
            $this->_enviar_emails();
            $this->session->set_flashdata('mensagem','Balancete Adicionado com Sucesso!!!');
            redirect('cli-juridico/documentacao');
        }
    }
    
    /**
     * Faz a validação do cartão CNPJ da empresa
     *
     * @param
     * @return BOOLEAN
     */
    public function validacao_cartao_cnpj() {
        $dados=$this->_upload_documentos(usuario_sessao_cliente()['id_cliente']);
        if(isset($dados['error'])){
            $this->index($dados['error']);
        }
        else{
            $id_cliente=usuario_sessao_cliente()['id_cliente'];
            $data['tipo_documento']=$this->input->post('tipo_documento');
            $contador=count($dados)-1;
            for($i=0;$i<$contador;$i++){
                $data['url_documento']=$dados[$i]['file_name'];
                $data['id_cliente']=$id_cliente;
                $this->cliente->inserir_documentos_juridico($data);
            }
            $this->_enviar_emails();
            $this->session->set_flashdata('mensagem','Cartão CNPJ Adicionado com Sucesso!!!');
            redirect('cli-juridico/documentacao');
        }
    }
    
    /**
     * Faz a validação do imposto de renda da empresa
     *
     * @param
     * @return BOOLEAN
     */
    public function validacao_imposto_renda() {
        $dados=$this->_upload_documentos(usuario_sessao_cliente()['id_cliente']);
        if(isset($dados['error'])){
            $this->index($dados['error']);
        }
        else{
            $id_cliente=usuario_sessao_cliente()['id_cliente'];
            $data['tipo_documento']=$this->input->post('tipo_documento');
            $contador=count($dados)-1;
            for($i=0;$i<$contador;$i++){
                $data['url_documento']=$dados[$i]['file_name'];
                $data['id_cliente']=$id_cliente;
                $this->cliente->inserir_documentos_juridico($data);
            }
            $this->_enviar_emails();
            $this->session->set_flashdata('mensagem','Declaração de Imposto de Renda Adicionado com Sucesso!!!');
            redirect('cli-juridico/documentacao');
        }
    }
    
    /**
     * Faz a validação da certidão negativa da empresa
     *
     * @param
     * @return BOOLEAN
     */
    public function validacao_certidao_negativa() {
        $dados=$this->_upload_documentos(usuario_sessao_cliente()['id_cliente']);
        if(isset($dados['error'])){
            $this->index($dados['error']);
        }
        else{
            $id_cliente=usuario_sessao_cliente()['id_cliente'];
            $data['tipo_documento']=$this->input->post('tipo_documento');
            $contador=count($dados)-1;
            for($i=0;$i<$contador;$i++){
                $data['url_documento']=$dados[$i]['file_name'];
                $data['id_cliente']=$id_cliente;
                $this->cliente->inserir_documentos_juridico($data);
            }
            $this->_enviar_emails();
            $this->session->set_flashdata('mensagem','Certidão Negativa Adicionada com Sucesso!!!');
            redirect('cli-juridico/documentacao');
        }
    }
    
    /**
     * Faz a validação de outros documentos da empresa
     *
     * @param
     * @return BOOLEAN
     */
    public function validacao_outros_documentos() {
        $dados=$this->_upload_documentos(usuario_sessao_cliente()['id_cliente']);
        if(isset($dados['error'])){
            $this->index($dados['error']);
        }
        else{
            $id_cliente=usuario_sessao_cliente()['id_cliente'];
            $data['tipo_documento']=$this->input->post('tipo_documento');
            $contador=count($dados)-1;
            for($i=0;$i<$contador;$i++){
                $data['url_documento']=$dados[$i]['file_name'];
                $data['id_cliente']=$id_cliente;
                $this->cliente->inserir_documentos_juridico($data);
            }
            $this->_enviar_emails();
            $this->session->set_flashdata('mensagem','Outros Documentos Adicionados com Sucesso!!!');
            redirect('cli-juridico/documentacao');
        }
    }
    
    /**
     * Faz a validação do RG e CPF do Cliente
     *
     * @param
     * @return BOOLEAN
     */
    public function validacao_rg_cpf() {
        $dados=$this->_upload_rg_cpf(usuario_sessao_cliente()['id_cliente']);
        if(isset($dados['error'])){
            $this->index($dados['error']);
        }
        else{
            $cont=count($dados)-1;
            for($i=0;$i<$cont;$i++){
                $data['tipo_documento']=$dados[$i]['tipo'];
                $data['id_cliente']=usuario_sessao_cliente()['id_cliente'];
                $data['url_documento']=$dados[$i]['file_name'];
                $this->cliente->inserir_documentos_socios($data);
            }
            $this->_enviar_emails();
            $this->session->set_flashdata('mensagem','RG e CPF dos Sócios/Diretores Adicionados com Sucesso!!!');
            redirect('cli-juridico/documentacao');
        }
    }
    
    /**
     * Faz a validação da CNH do Cliente
     *
     * @param
     * @return BOOLEAN
     */
    public function validacao_cnh() {
        $dados=$this->_upload_documentos(usuario_sessao_cliente()['id_cliente']);
        if(isset($dados['error'])){
            $this->index($dados['error']);
        }
        else{
            $id_cliente=usuario_sessao_cliente()['id_cliente'];
            $data['tipo_documento']=$this->input->post('tipo_documento');
            $contador=count($dados)-1;
            for($i=0;$i<$contador;$i++){
                $data['url_documento']=$dados[$i]['file_name'];
                $data['id_cliente']=$id_cliente;
                $this->cliente->inserir_documentos_socios($data);
            }
            $this->_enviar_emails();
            $this->session->set_flashdata('mensagem','CNH Adicionada com Sucesso!!!');
            redirect('cli-juridico/documentacao');
        }
    }
    
    /**
     * Faz a validaçao do Comprovante de endereço do Cliente
     *
     * @param
     * @return BOOLEAN
     */
    public function validacao_comprovante_endereco() {
        $dados=$this->_upload_documentos(usuario_sessao_cliente()['id_cliente']);
        if(isset($dados['error'])){
            $this->index($dados['error']);
        }
        else{
            $id_cliente=usuario_sessao_cliente()['id_cliente'];
            $data['tipo_documento']=$this->input->post('tipo_documento');
            $contador=count($dados)-1;
            for($i=0;$i<$contador;$i++){
                $data['url_documento']=$dados[$i]['file_name'];
                $data['id_cliente']=$id_cliente;
                $this->cliente->inserir_documentos_socios($data);
            }
            $this->_enviar_emails();
            $this->session->set_flashdata('mensagem','Comprovantes de Endereço Adicionados com Sucesso!!!');
            redirect('cli-juridico/documentacao');
        }
    }
    
    /**
     * Faz a exclusão da documetação do cliente
     *
     * @param int $tipo tipo de documento a ser excluido
     * @param int $principal indentificação do pretendente
     * @return BOOLEAN
     */
    public function excluir_documentacao($tipo) {
        $id_cliente=usuario_sessao_cliente()['id_cliente'];
       if($tipo==1){
            $this->cliente->excluir_documetacao_juridico_socios($id_cliente,1);
            $this->cliente->excluir_documetacao_juridico_socios($id_cliente,2);
            $this->cliente->excluir_documetacao_juridico_socios($id_cliente,3);
        }
        else{
            if($tipo==4){
                $this->cliente->excluir_documetacao_juridico_socios($id_cliente,4);
                $this->cliente->excluir_documetacao_juridico_socios($id_cliente,5);
                $this->cliente->excluir_documetacao_juridico_socios($id_cliente,6);
            }
            else{
                $this->cliente->excluir_documetacao_juridico($id_cliente,$tipo); 
            }
        } 
        $this->session->set_flashdata('mensagem','Documentos Excluidos com Sucesso!!!');
         redirect('cli-juridico/documentacao');
    }
    
    /**
     * Faz o upload de todos os documentos do cliente
     *
     * @param int $id_cliente ID único do cliente
     * @return ARRAY
     */
    public function _upload_documentos($id_cliente){
        if(!is_dir("publico/uploads/".md5($id_cliente))){
            mkdir("publico/uploads/".md5($id_cliente), 0700);
        }
        $config['upload_path']          = 'publico/uploads/'.md5($id_cliente);
        $config['allowed_types']        = 'jpg|png|pdf';
        $config['max_size']             = 100000;
        $this->load->library('upload', $config);
        $filesCountDocumentacao = count($_FILES['documentos']['name']);
        for($i = 0; $i < $filesCountDocumentacao; $i++){
            $_FILES['documento']['name'] = $_FILES['documentos']['name'][$i];
            $_FILES['documento']['type'] = $_FILES['documentos']['type'][$i];
            $_FILES['documento']['tmp_name'] = $_FILES['documentos']['tmp_name'][$i];
            $_FILES['documento']['error'] = $_FILES['documentos']['error'][$i];
            $_FILES['documento']['size'] = $_FILES['documentos']['size'][$i];
            if(! $this->upload->do_upload('documento')){
                $error = array('error' => $this->upload->display_errors());
                return $error;
            }
            $dados[$i] = $this->upload->data();
        }
        $dados['error']=NULL;
        return $dados;
    }
    
    /**
     * Faz o upload do RG e CPF do cliente
     *
     * @param int $id_cliente ID único do cliente
     * @return ARRAY
     */
    public function _upload_rg_cpf($id_cliente){
        if(!is_dir("publico/uploads/".md5($id_cliente))){
                mkdir("publico/uploads/".md5($id_cliente), 0700);
            }
        $config['upload_path']          = 'publico/uploads/'.md5($id_cliente);
        $config['allowed_types']        = 'jpg|png|pdf';
        $config['max_size']             = 100000;
        $this->load->library('upload', $config);
        $aux=0;
        $filesCountCPF = count($_FILES['cpf']['name']);
        for($i = 0; $i < $filesCountCPF; $i++){
            $_FILES['cpf_aux']['name'] = $_FILES['cpf']['name'][$i];
            $_FILES['cpf_aux']['type'] = $_FILES['cpf']['type'][$i];
            $_FILES['cpf_aux']['tmp_name'] = $_FILES['cpf']['tmp_name'][$i];
            $_FILES['cpf_aux']['error'] = $_FILES['cpf']['error'][$i];
            $_FILES['cpf_aux']['size'] = $_FILES['cpf']['size'][$i];
            if(! $this->upload->do_upload('cpf_aux')){
                 $error = array('error' => $this->upload->display_errors());
                return $error;
            }
            $dados[$i] = $this->upload->data();
            $dados[$i]['tipo'] = 2;
            $aux++;
        }
        $aux=$aux-1;
        $filesCountRG = count($_FILES['rg']['name']);
        for($i = 0; $i < $filesCountRG; $i++){
            $_FILES['rg_aux']['name'] = $_FILES['rg']['name'][$i];
            $_FILES['rg_aux']['type'] = $_FILES['rg']['type'][$i];
            $_FILES['rg_aux']['tmp_name'] = $_FILES['rg']['tmp_name'][$i];
            $_FILES['rg_aux']['error'] = $_FILES['rg']['error'][$i];
            $_FILES['rg_aux']['size'] = $_FILES['rg']['size'][$i];
            if(! $this->upload->do_upload('rg_aux')){
                $error = array('error' => $this->upload->display_errors());
                return $error;
            }
            $aux++;
            $dados[$aux] = $this->upload->data();
            $dados[$aux]['tipo'] = 1;
            }
        $dados['error']=NULL;
        return $dados;
    }
    
    /**
     * Faz o envio dos e-mail quando um cliente adiciona um novo documentos
     *
     * @param
     * @return BOOLEAN
     */
    public function _enviar_emails() {
        /*===ENVIA EMAILS PARA O CORRETOR AVISANDO DE FICHA ADICIONADA====*/
        $email_corretor=$this->cliente->busca_email_corretor(usuario_sessao_cliente()['id_corretor']);
        $dados_email['nome_cliente']=usuario_sessao_cliente()['nome_cliente'];
        $dados_email['email_cliente']=usuario_sessao_cliente()['email'];
        $dados_email['nome_corretor']=$this->cliente->busca_nome_corretor(usuario_sessao_cliente()['id_corretor']);
        $dados_email['link']=base_url('adm/clientes/visualizar-cliente/'.usuario_sessao_cliente()['id_cliente']);
        $mensagem = $this->load->view('template/emails/documento_juridico_baggio', $dados_email, TRUE);
        $mensagem_cliente = $this->load->view('template/emails/documento_juridico_cliente', $dados_email, TRUE);
        $array_emails=array($email_corretor['email'],"juarez@baggioimoveis.com.br","andressa@baggioimoveis.com.br");
        enviar_email_fichas(usuario_sessao_cliente()['nome_cliente']." | Documento Pessoa Jurídica", $array_emails, $mensagem);
        enviar_email_fichas("Baggio Imóveis - ".$dados_email['nome_cliente'].", recebemos seus Documentos Pessoa Jurídica",  array($dados_email['email_cliente']), $mensagem_cliente);
    }
}
