<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Controller responsável pelas operações do perfil do cliente pessoa física
 *
 * PHP version 7
 *
 * @category PHP
 * @author   Kellton Leitão <kelltonl10@gmail.com>
 */
class AreaClienteController extends CI_Controller {

     public function __construct() {
        parent::__construct();
        if(!usuario_sessao_cliente()) {
            $this->session->set_flashdata('param_msg_erro_cabecalho', 'Sem Permissão');
            $this->session->set_flashdata('param_msg_erro', 'Você não tem permissão de acessar esta área!');
            redirect(base_url());
        }
        if(usuario_sessao_cliente()['cliente']!=="Pessoa Física"){
            $this->session->set_flashdata('param_msg_erro_cabecalho', 'Sem Permissão');
            $this->session->set_flashdata('param_msg_erro', 'Você não tem permissão de acessar esta área!');
            redirect(base_url());
        }
        $this->load->model('ClienteModel','cliente');
    }
    
    /**
     * Carrega a página inicial do cliente
     *
     * @param nenhum
     * @return views
     */
    public function index() {
        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();
        
        $this->load->view('template/header', $header);
        $this->load->view('template/menu');
        $this->load->view('adm_cliente/home');
        $this->load->view('template/footer');
    }
    
    /**
     * Carrega a página de alterar dados do cliente
     *
     * @param nenhum
     * @return views
     */
    public function alterar_dados() {
        $id_cliente=usuario_sessao_cliente()['id_cliente'];
        $dados['query']=$this->cliente->buscar_cliente($id_cliente);
        $dados['corretores']=$this->cliente->listar_corretores();
        $dados['tipo_usuario']=$this->cliente->listar_tipo_usuario();
        $dados['query']['id_cliente']=$id_cliente;

        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $this->load->view('template/menu');
        $this->load->view('adm_cliente/alterar_dados',$dados);
        $this->load->view('template/footer');
    }
    
    /**
    * Carrega a página de visualização das fichas do primeiro pretendente
    *
    * @param
    * @return views
    */
    public function visualizar_ficha_pretendente() {
        $dados['query']=$this->cliente->pega_dados_pretendente(usuario_sessao_cliente()['id_cliente']);
        $dados['enderecos']=$this->cliente->enderecos_pretendentes(usuario_sessao_cliente()['id_cliente'],$dados['query']['pretendente_id']);
        $dados['residentes']=$this->cliente->pegar_residentes(usuario_sessao_cliente()['id_cliente']);
        $dados['propriedades']=$this->cliente->pegar_propriedades(usuario_sessao_cliente()['id_cliente']);
        $dados['referencias']=$this->cliente->pegar_referencias(usuario_sessao_cliente()['id_cliente']);
        $dados['referencias_bancarias']=$this->cliente->pegar_referencias_bancarias(usuario_sessao_cliente()['id_cliente']);
        $dados['pais']=$this->cliente->pegar_pais_pretendentes(usuario_sessao_cliente()['id_cliente']);

        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $this->load->view('template/menu');
        $this->load->view('adm_cliente/visualizar_ficha_pretendente',$dados);
        $this->load->view('template/footer');
    }
    
    /**
    * Carrega a página de visualização das fichas do segundo pretendente
    *
    * @param
    * @return views
    */
    public function visualizar_ficha_segundo_pretendente() {
        $dados['query']=$this->cliente->pega_dados_segundo_pretendente(usuario_sessao_cliente()['id_cliente']);
        
        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $this->load->view('template/menu');
        $this->load->view('adm_cliente/visualizar_ficha_segundo_pretendente',$dados);
        $this->load->view('template/footer');
    }
    
    /**
    * Carrega a página de visualização das fichas do fiador
    *
    * @param
    * @return views
    */
    public function visualizar_ficha_fiador() {
        $dados['query']=$this->cliente->pega_dados_fiador(usuario_sessao_cliente()['id_cliente']);
        $id_fiador=$dados['query']['id_fiador'];
        $dados['propriedades']=$this->cliente->pega_propriedades_fiador($id_fiador);
        $dados['referencias']=$this->cliente->pega_referencias_fiador($id_fiador);
        $dados['referencias_bancarias']=$this->cliente->pega_referencias_bancarias_fiador($id_fiador);
        
        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $this->load->view('template/menu');
        $this->load->view('adm_cliente/visulaizar_ficha_fiador',$dados);
        $this->load->view('template/footer');
    }
    
    /**
    * Faz a validação da ediçao dos dados do cliente
    *
    * @param
    * @return BOOLEAN
    */
    public function validacao_editar_cliente() {
        $this->form_validation->set_rules('nome_cliente','Nome','required');
        $this->form_validation->set_rules('telefone','Telefone','required');
        if($this->form_validation->run()==FALSE) {
            $this->alterar_dados();
        }
        else{
            $id_cliente=usuario_sessao_cliente()['id_cliente'];
            $cliente=elements(array('nome_cliente','telefone'), $this->input->post());
            if($this->input->post('senha')!='') {
                $cliente['senha']=md5($this->input->post('senha'));
            }
            $this->cliente->editar_cliente($cliente,$id_cliente);
            $this->session->set_flashdata('mensagem','Alteração Efetuada com Sucesso!!!');
            redirect('cli');
        }
    }
    
    /**
    * Carrega a página de visualização da documentação do cliente
    *
    * @param
    * @return BOOLEAN
    */
    public function documentacao() {
        $dados['tem_dados']= $this->cliente->verifica_se_tem_dados(usuario_sessao_cliente()['id_cliente']);
        $dados['tem_segundo_pretendente']=$this->cliente->verifica_se_tem_segundo_pretendente(usuario_sessao_cliente()['id_cliente']);
        $dados['verifica_fiador']=$this->cliente->verifica_se_adicionou_fiador(usuario_sessao_cliente()['id_cliente']);
        
        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $this->load->view('template/menu');
        $this->load->view('adm_cliente/documentacao',$dados);
        $this->load->view('template/footer');
    }
}
