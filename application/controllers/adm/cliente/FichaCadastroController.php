<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Controller responsável pelas operações referentes as fichas do cliente 
 * pessoa física
 *
 * PHP version 7
 *
 * @category PHP
 * @author   Kellton Leitão <kelltonl10@gmail.com>
 */
class FichaCadastroController extends CI_Controller {

     public function __construct() {
        parent::__construct();
         if(!usuario_sessao_cliente()) {
            $this->session->set_flashdata('param_msg_erro_cabecalho', 'Sem Permissão');
            $this->session->set_flashdata('param_msg_erro', 'Você não tem permissão de acessar esta área!');
            redirect(base_url());
        }
        if(usuario_sessao_cliente()['cliente']!=="Pessoa Física"){
            $this->session->set_flashdata('param_msg_erro_cabecalho', 'Sem Permissão');
            $this->session->set_flashdata('param_msg_erro', 'Você não tem permissão de acessar esta área!');
            redirect(base_url());
        }
        $this->load->model('ClienteModel','cliente');
    }
    
    /**
     * Carrega a página de acesso as fichas do cliente
     *
     * @param 
     * @return views
     */
    public function index() {
        $dados['segundo_pretendente']=$this->cliente->verifica_se_tem_segundo_pretendente(usuario_sessao_cliente()['id_cliente']);
        $dados['tem_pretendente']=$this->cliente->verifica_se_tem_pretendente(usuario_sessao_cliente()['id_cliente']);
        $dados['adicionou_seggundo_pretendente']=$this->cliente->verifica_se_adicionou_segundo_pretendente(usuario_sessao_cliente()['id_cliente']);
        $dados['tem_fiador']=$this->cliente->verifica_se_tem_fiador(usuario_sessao_cliente()['id_cliente']);
        
        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $this->load->view('template/menu');
        $this->load->view('adm_cliente/ficha_cadastro',$dados);
        $this->load->view('template/footer');
    }
    
    /**
     * Carrega o fomulário de adição das fichas
     *
     * @param 
     * @return views
     */
    public function adicionar_ficha_cadastro() {
        $dados['paises']=$this->cliente->listar_paises();
        
        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();
        
        $this->load->view('template/header', $header);
        $this->load->view('template/menu');

        $this->load->view('adm_cliente/fichas/1_pretendente/adicionar/form_cabecalho',$dados);
        $this->load->view('adm_cliente/fichas/1_pretendente/adicionar/inf_basicas',$dados);
        $this->load->view('adm_cliente/fichas/1_pretendente/adicionar/dados_adicionais',$dados);
        $this->load->view('adm_cliente/fichas/1_pretendente/adicionar/estado_civil',$dados);
        $this->load->view('adm_cliente/fichas/1_pretendente/adicionar/propriedades',$dados);
        $this->load->view('adm_cliente/fichas/1_pretendente/adicionar/referencias',$dados);
        $this->load->view('adm_cliente/fichas/1_pretendente/adicionar/inf_complementares',$dados);
        $this->load->view('adm_cliente/fichas/1_pretendente/adicionar/form_rodape',$dados);

        $this->load->view('template/footer');
    }
    
    /**
     * Carrega o fomulário de edição das fichas do primeiro pretendente
     *
     * @param 
     * @return views
     */
    public function editar_ficha_cadastro() {
        $dados['query']=$this->cliente->pega_dados_pretendente(usuario_sessao_cliente()['id_cliente']);
        $dados['endereco_principal']=$this->cliente->enderecos_pretendente_principal(usuario_sessao_cliente()['id_cliente'],$dados['query']['pretendente_id']);
        $dados['enderecos_alternativo']=$this->cliente->enderecos_pretendente_alternativo(usuario_sessao_cliente()['id_cliente'],$dados['query']['pretendente_id']);
        $dados['residentes']=$this->cliente->pegar_residentes(usuario_sessao_cliente()['id_cliente']);
        $dados['propriedades']=$this->cliente->pegar_propriedades(usuario_sessao_cliente()['id_cliente']);
        $dados['referencias']=$this->cliente->pegar_referencias(usuario_sessao_cliente()['id_cliente']);
        $dados['referencias_bancarias']=$this->cliente->pegar_referencias_bancarias(usuario_sessao_cliente()['id_cliente']);
        $dados['mae']=$this->cliente->pegar_mae_pretendentes(usuario_sessao_cliente()['id_cliente']);
        $dados['pai']=$this->cliente->pegar_pai_pretendentes(usuario_sessao_cliente()['id_cliente']);
        $dados['paises']=$this->cliente->listar_paises();
        
        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $this->load->view('template/menu');
        
        $this->load->view('adm_cliente/fichas/1_pretendente/editar/form_cabecalho',$dados);
        $this->load->view('adm_cliente/fichas/1_pretendente/editar/inf_basicas',$dados);
        $this->load->view('adm_cliente/fichas/1_pretendente/editar/dados_adicionais',$dados);
        $this->load->view('adm_cliente/fichas/1_pretendente/editar/estado_civil',$dados);
        $this->load->view('adm_cliente/fichas/1_pretendente/editar/propriedades',$dados);
        $this->load->view('adm_cliente/fichas/1_pretendente/editar/referencias',$dados);
        $this->load->view('adm_cliente/fichas/1_pretendente/editar/inf_complementares',$dados);
        $this->load->view('adm_cliente/fichas/1_pretendente/editar/form_rodape',$dados);

        $this->load->view('template/footer');
    }
    
    /**
     * Carrega o fomulário de edição das fichas do segundo pretendente
     *
     * @param 
     * @return views
     */
    public function editar_ficha_cadastro_segundo_pretendente() {
        $dados['query']=$this->cliente->pega_dados_segundo_pretendente(usuario_sessao_cliente()['id_cliente']);
        $dados['paises']=$this->cliente->listar_paises();
        
        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $this->load->view('template/menu');
        
        $this->load->view('adm_cliente/fichas/2_pretendente/editar/form_cabecalho',$dados);
        $this->load->view('adm_cliente/fichas/2_pretendente/editar/inf_basicas',$dados);
        $this->load->view('adm_cliente/fichas/2_pretendente/editar/estado_civil',$dados);
        $this->load->view('adm_cliente/fichas/2_pretendente/editar/form_rodape',$dados);

        $this->load->view('template/footer');
    }
    
    /**
     * Carrega o fomulário de adição das fichas do segundo pretendente
     *
     * @param 
     * @return views
     */
    public function adicionar_ficha_cadastro_segundo_pretendente() {
        $dados['paises']=$this->cliente->listar_paises();
        
        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $this->load->view('template/menu');
        
        $this->load->view('adm_cliente/fichas/2_pretendente/adicionar/form_cabecalho',$dados);
        $this->load->view('adm_cliente/fichas/2_pretendente/adicionar/inf_basicas',$dados);
        $this->load->view('adm_cliente/fichas/2_pretendente/adicionar/estado_civil',$dados);
        $this->load->view('adm_cliente/fichas/2_pretendente/adicionar/form_rodape',$dados);

        $this->load->view('template/footer');
    }
    
    /**
     * Carrega o fomulário de adição da ficha do fiador
     *
     * @param 
     * @return views
     */
    public function adicionar_ficha_cadastro_fiador() {
        $dados['paises']=$this->cliente->listar_paises();
        
        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $this->load->view('template/menu');
        
        $this->load->view('adm_cliente/fichas/fiador/adicionar/form_cabecalho',$dados);
        $this->load->view('adm_cliente/fichas/fiador/adicionar/inf_basicas',$dados);
        $this->load->view('adm_cliente/fichas/fiador/adicionar/dados_adicionais',$dados);
        $this->load->view('adm_cliente/fichas/fiador/adicionar/estado_civil',$dados);
        $this->load->view('adm_cliente/fichas/fiador/adicionar/propriedades',$dados);
        $this->load->view('adm_cliente/fichas/fiador/adicionar/referencias',$dados);
        $this->load->view('adm_cliente/fichas/fiador/adicionar/inf_complementares',$dados);
        $this->load->view('adm_cliente/fichas/fiador/adicionar/form_rodape',$dados);

        $this->load->view('template/footer');
    }
    
    /**
     * Carrega o fomulário de edição da ficha do fiador
     *
     * @param 
     * @return views
     */
    public function editar_ficha_cadastro_fiador() {
        $dados['query']=$this->cliente->pega_dados_fiador(usuario_sessao_cliente()['id_cliente']);
        $dados['propriedades']=$this->cliente->pega_propriedades_fiador($dados['query']['id_fiador']);
        $dados['referencias']=$this->cliente->pega_referencias_fiador($dados['query']['id_fiador']);
        $dados['referencias_bancarias']=$this->cliente->pega_referencias_bancarias_fiador($dados['query']['id_fiador']);
        $dados['paises']=$this->cliente->listar_paises();
        //to_debug_r($dados);
        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $this->load->view('template/menu');

        $this->load->view('adm_cliente/fichas/fiador/editar/form_cabecalho',$dados);
        $this->load->view('adm_cliente/fichas/fiador/editar/inf_basicas',$dados);
        $this->load->view('adm_cliente/fichas/fiador/editar/dados_adicionais',$dados);
        $this->load->view('adm_cliente/fichas/fiador/editar/estado_civil',$dados);
        $this->load->view('adm_cliente/fichas/fiador/editar/propriedades',$dados);
        $this->load->view('adm_cliente/fichas/fiador/editar/referencias',$dados);
        $this->load->view('adm_cliente/fichas/fiador/editar/inf_complementares',$dados);
        $this->load->view('adm_cliente/fichas/fiador/editar/form_rodape',$dados);

        $this->load->view('template/footer');
    }
    
    /**
     * Faz a validação da ficha de cadastro
     *
     * @param 
     * @return views
     */
    public function validacao_ficha_cadastro() {
        if($this->input->post('enviar')==="Enviar ficha para Baggio"){
            $this->_regras_validacao();
            if($this->form_validation->run()==FALSE){
                $this->_inserir_dados_pretendente();
                $this->editar_ficha_cadastro();
            }
            else{
                $this->_inserir_dados_pretendente($envio=1);
                $this->session->set_flashdata('mensagem','Ficha do Pretendente Adicionado com Sucesso!!!');
                redirect('cli/ficha-cadastro');
            }
        }
        
        else{
            $this->_inserir_dados_pretendente();
            $this->session->set_flashdata('mensagem','Ficha do Pretendente Salva com Sucesso!!!');
            redirect('cli/ficha-cadastro');
        }
    }
    
    /**
     * Insere os dados do pretendente no banco de dados
     *
     * @param int $envio Define o envio ou não da ficha para a Baggio
     * @return BOOLEAN
     */
    public function _inserir_dados_pretendente($envio=0){
        $id_cliente=usuario_sessao_cliente()['id_cliente'];
        
        $this->db->trans_start();
        
        if($this->cliente->verifica_se_tem_dados(usuario_sessao_cliente()['id_cliente'])){
           $id_pretendente= $this->cliente->get_id_pretendente(usuario_sessao_cliente()['id_cliente'],1);
            /*==========ATUALIZA O PRETENDENTE NA TABELA TB_PRETENDENTES========*/
            $dados=elements(array('nome_completo','telefone','celular','email_pretendente','email_morador',
                'data_nascimento','nacionalidade','cpf','rg','expedicao','data_expedicao','id_estado_civil',
                'profissao','aposentado','salario','outras_rendas','total_renda','tempo_residencia',
                'residencia_propria','aluguel_atual','motivo_mudanca','imobiliaria_atual','telefone_imobiliaria_atual',
                'observacoes_complementares','segundo_pretendente','segundo_pretendente_conjugue'), $this->input->post());
            $dados['id_cliente']=$id_cliente;
            $dados['data_expedicao']=formatar_data_sql($dados['data_expedicao']);
            $dados['data_nascimento']= formatar_data_sql($dados['data_nascimento']);
            $dados['principal']= 1;
            $dados['enviou']=$envio;
            $this->cliente->atualiza_pretendente($id_pretendente['id_pretendente'],$dados);
            /*===============================FIM==============================*/
        }
        else{
            /*==========INSERE O PRETENDENTE NA TABELA TB_PRETENDENTES========*/
            $dados=elements(array('nome_completo','telefone','celular','email_pretendente','email_morador',
                'data_nascimento','nacionalidade','cpf','rg','expedicao','data_expedicao','id_estado_civil',
                'profissao','aposentado','salario','outras_rendas','total_renda','tempo_residencia',
                'residencia_propria','aluguel_atual','motivo_mudanca','imobiliaria_atual','telefone_imobiliaria_atual',
                'observacoes_complementares','segundo_pretendente','segundo_pretendente_conjugue'), $this->input->post());
            $dados['id_cliente']=$id_cliente;
            $dados['data_expedicao']=formatar_data_sql($dados['data_expedicao']);
            $dados['data_nascimento']= formatar_data_sql($dados['data_nascimento']);
            $dados['principal']= 1;
            $dados['enviou']=$envio;
           $id_pretendente['id_pretendente']=$this->cliente->inserir_pretendente($dados);
        }
            
        /*INSERE O DADOS DA EMPRESA DO PRETENDENTE NA TB_DADOS_EMPRESA_CLIENTE*/
        if($dados['aposentado']!='1'){
            $dados_nao_aposentado= elements(array('nome_empresa','telefone_empresa','email_comercial',
                'data_admissao','tempo_empresa','cep','logradouro','numero','complemento','bairro',
                'id_pais'), $this->input->post());
             $dados_nao_aposentado['id_cliente']=$id_cliente;
             $dados_nao_aposentado['data_admissao']=formatar_data_sql($dados_nao_aposentado['data_admissao']);
             $dados_nao_aposentado['id_pretendente']=$id_pretendente['id_pretendente'];

             /*VERIFICA SE JA TEM REGISTRO*/
            if($this->cliente->verifica_se_tem_dados_empresa_cliente(usuario_sessao_cliente()['id_cliente'],$id_pretendente['id_pretendente'])){
                $this->cliente->atualiza_dados_empresa_cliente($this->cliente->verifica_se_tem_dados_empresa_cliente(usuario_sessao_cliente()['id_cliente'],$id_pretendente['id_pretendente'])['id_dados_empresa_cliente'],$dados_nao_aposentado);
            }
            else{
                $this->cliente->inserir_dados_empresa_cliente($dados_nao_aposentado);
            }
        }
        /*===============================FIM==============================*/

        /*======ATUALIZA OS DADOS DE ENDEREÇO NA TB_ENDERECO_PRETENDENTE=====*/
        $dados_endereco_cliente=elements(array('cep_cliente','logradouro_cliente','numero_cliente',
            'complemento_cliente','bairro_cliente','id_pais_cliente'), $this->input->post());
        $dados_endereco_cliente['principal']=1;
        $dados_endereco_cliente['id_cliente']=$id_cliente;
        $dados_endereco_cliente['id_pretendente']=$id_pretendente['id_pretendente'];
        
        /*VERIFICA SE JA TEM REGISTRO*/
        if($this->cliente->enderecos_pretendente_principal(usuario_sessao_cliente()['id_cliente'],$id_pretendente['id_pretendente'])){
            $this->cliente->atualizar_dados_endereco($id_pretendente['id_pretendente'],$principal=1,$dados_endereco_cliente);
        }
        else{
            $this->cliente->inserir_dados_endereco($dados_endereco_cliente);
        }
        /*===============================FIM==============================*/

        /*=====INSERE OS DADOS DOS RESIDENTES NA TB_PESSOAS_RESIDENTES=====*/
        $this->cliente->excluir_dados_residentes($id_cliente);
        $dados_pessoas_residencia=elements(array('nome_completo_residente','parentesco_residente',
            'telefone_residente'), $this->input->post());
        $residentes=array();
        for($i=0;$i<count($dados_pessoas_residencia['nome_completo_residente']);$i++){
            $residentes['id_cliente']=$id_cliente;
            $residentes['nome_completo_residente']=$dados_pessoas_residencia['nome_completo_residente'][$i];
            $residentes['parentesco_residente']=$dados_pessoas_residencia['parentesco_residente'][$i];
            $residentes['telefone_residente']=$dados_pessoas_residencia['telefone_residente'][$i];
            $this->cliente->inserir_dados_residentes($residentes);
        }
        /*===============================FIM==============================*/

        /*INSERE OS DADOS DAS PROPRIEDADES DO CLIENTE NA TB_PROPRIEDADES_CLIENTES*/
        $this->cliente->excluir_dados_propriedades($id_cliente);
        $dados_propriedades= elements(array('matricula_imovel','circunscricao','veiculo_marca',
                'renavam'), $this->input->post());
        $propriedades=array();
        for($i=0;$i<count($dados_propriedades['matricula_imovel']);$i++){
            $propriedades['id_cliente']=$id_cliente;
            $propriedades['matricula_imovel']=$dados_propriedades['matricula_imovel'][$i];
            $propriedades['circunscricao']=$dados_propriedades['circunscricao'][$i];
            $propriedades['veiculo_marca']=$dados_propriedades['veiculo_marca'][$i];
            $propriedades['renavam']=$dados_propriedades['renavam'][$i];
            $this->cliente->inserir_dados_propriedades($propriedades);
        }
        /*===============================FIM==============================*/

        /*INSERE OS DADOS DAS REFERENCIAS DO CLIENTE NA TB_REFERENCIAS_CLIENTES*/
        $this->cliente->excluir_dados_referencias($id_cliente);
        $dados_referencias= elements(array('nome_referencia','endereco_referencia','telefone_referencia'), $this->input->post());
        $referencias=array();
        for($i=0;$i<count($dados_referencias['nome_referencia']);$i++){
            $referencias['id_cliente']=$id_cliente;
            $referencias['nome_referencia']=$dados_referencias['nome_referencia'][$i];
            $referencias['endereco_referencia']=$dados_referencias['endereco_referencia'][$i];
            $referencias['telefone_referencia']=$dados_referencias['telefone_referencia'][$i];
            $this->cliente->inserir_dados_referencias($referencias);
        }
        /*===============================FIM==============================*/

        /*INSERE OS DADOS DAS REFERENCIAS DO BANCO DO CLIENTE NA TB_BANCOS_REFERENCIAS*/
         $this->cliente->excluir_dados_banco($id_cliente);
        $dados_banco= elements(array('banco_referencia','telefone_banco_referencia','nome_agencia_referencia',
                'numero_conta_referencia'), $this->input->post());
        $banco=array();
        for($i=0;$i<count($dados_banco['banco_referencia']);$i++){
            $banco['id_cliente']=$id_cliente;
            $banco['banco_referencia']=$dados_banco['banco_referencia'][$i];
            $banco['telefone_banco_referencia']=$dados_banco['telefone_banco_referencia'][$i];
            $banco['nome_agencia_referencia']=$dados_banco['nome_agencia_referencia'][$i];
            $banco['numero_conta_referencia']=$dados_banco['numero_conta_referencia'][$i];
            $this->cliente->inserir_dados_banco($banco);
        }
        /*===============================FIM==============================*/
        
         /*INSERE OS DADOS DOS PAIS DO CLIENTE NA TB_PAIS_CLIENTES*/
        //INSERE MAE
        if($this->input->post('status_mae')=="0"){
            $this->cliente->excluir_dados_pais($id_cliente,$tipo=1);
            $dados_mae['nome']=$this->input->post('mae_pretendente');
            $dados_mae['telefone']=$this->input->post('telefone_mae_pretendente');
            $dados_mae['cep']=$this->input->post('cep_mae_cliente');
            $dados_mae['logradouro']=$this->input->post('logradouro_mae_cliente');
            $dados_mae['numero']=$this->input->post('numero_mae_cliente');
            $dados_mae['complemento']=$this->input->post('complemento_mae_cliente');
            $dados_mae['bairro']=$this->input->post('bairro_mae_cliente');
            $dados_mae['id_pais']=$this->input->post('id_pais_residencia_mae');
            $dados_mae['mae']=1;
            $dados_mae['id_cliente']=$id_cliente;
            $this->cliente->inserir_dados_pais($dados_mae);
        }

        //INSERE PAI  
        if($this->input->post('status_pai')=="0"){
            $this->cliente->excluir_dados_pais($id_cliente,$tipo=0);
            $dados_pai['nome']=$this->input->post('pai_pretendente');
            $dados_pai['telefone']=$this->input->post('telefone_pai_pretendente');
            $dados_pai['cep']=$this->input->post('cep_pai_cliente');
            $dados_pai['logradouro']=$this->input->post('logradouro_pai_cliente');
            $dados_pai['numero']=$this->input->post('numero_pai_cliente');
            $dados_pai['complemento']=$this->input->post('complemento_pai_cliente');
            $dados_pai['bairro']=$this->input->post('bairro_pai_cliente');
            $dados_pai['id_pais']=$this->input->post('id_pais_residencia_pai');
            $dados_pai['mae']=0;
            $dados_pai['id_cliente']=$id_cliente;
            $this->cliente->inserir_dados_pais($dados_pai);
        }
        /*===============================FIM==============================*/
        
        /*=INSERE OS DADOS DO CONJUGUE DO SEGUNDO TB_CONJUGUE_PRETENDENTE=*/
        if($this->input->post('id_estado_civil')=="2"){
            $this->cliente->excluir_dados_conjugue_pretendente($id_pretendente['id_pretendente']);
            $dados_conjugue= elements(array('nome_completo_conjugue','celular_conjugue','email_conjugue',
                            'data_nascimento_conjugue','nacionalidade_conjugue','cpf_conjugue',
                            'rg_conjugue','expedicao_conjugue','data_expedicao_conjugue',
                            'profissao_conjugue','aposentado_conjugue','nome_empresa_conjugue',
                            'telefone_empresa_conjugue','email_comercial_conjugue','data_admissao_conjugue',
                            'tempo_empresa_conjugue','salario_conjugue','outras_rendas_conjugue',
                            'total_renda_conjugue'), $this->input->post());
            $dados_conjugue['id_cliente']=$id_cliente;
            $dados_conjugue['id_pretendente']=$id_pretendente['id_pretendente'];
            $dados_conjugue['data_expedicao_conjugue']=formatar_data_sql($dados_conjugue['data_expedicao_conjugue']);
            $dados_conjugue['data_admissao_conjugue']=formatar_data_sql($dados_conjugue['data_admissao_conjugue']);
            $dados_conjugue['data_nascimento_conjugue']=formatar_data_sql($dados_conjugue['data_nascimento_conjugue']);
            $this->cliente->inserir_dados_conjugue_pretendente($dados_conjugue);
        }
        /*===============================FIM==============================*/
        
        $this->db->trans_complete();
        if($envio==1){
        /*===ENVIA EMAILS PARA O CORRETOR AVISANDO DE FICHA ADICIONADA====*/
            $email_corretor=$this->cliente->busca_email_corretor(usuario_sessao_cliente()['id_corretor']);
            $dados_email['nome_cliente']=usuario_sessao_cliente()['nome_cliente'];
            $dados_email['email_cliente']=usuario_sessao_cliente()['email'];
            $dados_email['nome_corretor']=$this->cliente->busca_nome_corretor(usuario_sessao_cliente()['id_corretor']);
            $dados_email['link']=base_url('adm/clientes/visualizar-cliente/'.usuario_sessao_cliente()['id_cliente']);
            $mensagem = $this->load->view('template/emails/ficha_pessoa_fisica_baggio', $dados_email, TRUE);
            $mensagem_cliente = $this->load->view('template/emails/ficha_pessoa_fisica_cliente', $dados_email, TRUE);
            $array_emails=array($email_corretor['email'],"juarez@baggioimoveis.com.br","andressa@baggioimoveis.com.br");
            enviar_email_fichas(usuario_sessao_cliente()['nome_cliente']." | Ficha Pessoa Física", $array_emails, $mensagem);
            enviar_email_fichas("Baggio Imóveis - ".$dados_email['nome_cliente'].", recebemos sua Ficha Pessoa Física",  array($dados_email['email_cliente']), $mensagem_cliente);
         /*===============================FIM==============================*/
        }
        
        if ($this->db->trans_status() === FALSE){
            $this->session->set_flashdata('mensagem','Nao Foi Possível Salvar Sua Ficha');
            log_message('error', 'ERRO SGBD - INSERIR FICHA DE CADASTRO PRETENDENTE - ');
            redirect('cli/ficha-cadastro');
        }
    }
    
    /**
     * Faz a validação da ficha de cadastro do segundo pretendente
     *
     * @param 
     * @return views
     */
    public function validacao_ficha_cadastro_segundo_pretendente() {
        if($this->input->post('enviar')==="Enviar ficha para Baggio"){
             $this->_regras_validacao_segundo_pretendente();
            if($this->form_validation->run()==FALSE){
                $this->_inserir_dados_segundo_pretendente();
                $this->editar_ficha_cadastro_segundo_pretendente();
            }
            else{
                $this->_inserir_dados_segundo_pretendente($envio=1);
                $this->session->set_flashdata('mensagem','Ficha do Segundo Pretendente Adicionado com Sucesso!!!');
                redirect('cli/ficha-cadastro');
            }
        }
        else{
            $this->_inserir_dados_segundo_pretendente();
            $this->session->set_flashdata('mensagem','Ficha do Segundo Pretendente Salva com Sucesso!!!');
                redirect('cli/ficha-cadastro');
        }
    }
    
    /**
     * Insere os dados do segundo pretendente no banco de dados
     * @param int $envio Define o envio ou não da ficha para a Baggio
     * @param 
     * @return BOOLEAN
     */
    public function _inserir_dados_segundo_pretendente($envio=0) {
            $id_cliente=usuario_sessao_cliente()['id_cliente'];
            $this->db->trans_start();
              
            if($this->cliente->verifica_se_adicionou_segundo_pretendente(usuario_sessao_cliente()['id_cliente'])){
                /*======ATUALIZA O PRETENDENTE NA TABELA TB_PRETENDENTES=====*/
                $dados=elements(array('nome_completo','telefone','celular','email_pretendente','email_morador',
                    'data_nascimento','nacionalidade','cpf','rg','expedicao','data_expedicao','id_estado_civil',
                    'profissao','aposentado','salario','outras_rendas','total_renda'), $this->input->post());
                $dados['id_cliente']=$id_cliente;
                $dados['data_expedicao']=formatar_data_sql($dados['data_expedicao']);
                $dados['data_nascimento']= formatar_data_sql($dados['data_nascimento']);
                $dados['principal']= 0;
                $dados['enviou']=$envio;
                $id_pretendente=$this->cliente->get_id_pretendente(usuario_sessao_cliente()['id_cliente'],0)['id_pretendente'];
                $this->cliente->atualiza_pretendente($id_pretendente,$dados);
                /*===============================FIM==============================*/ 
            }
            else{
                 
            /*==========INSERE O PRETENDENTE NA TABELA TB_PRETENDENTES========*/
                $dados=elements(array('nome_completo','telefone','celular','email_pretendente','email_morador',
                    'data_nascimento','nacionalidade','cpf','rg','expedicao','data_expedicao','id_estado_civil',
                    'profissao','aposentado','salario','outras_rendas','total_renda'), $this->input->post());
                $dados['id_cliente']=$id_cliente;
                $dados['data_expedicao']=formatar_data_sql($dados['data_expedicao']);
                $dados['data_nascimento']= formatar_data_sql($dados['data_nascimento']);
                $dados['principal']= 0;
                $dados['enviou']=$envio;
                $id_pretendente=$this->cliente->inserir_pretendente($dados);
            /*===============================FIM==============================*/ 
            }
            /*INSERE O DADOS DA EMPRESA DO PRETENDENTE NA TB_DADOS_EMPRESA_CLIENTE*/
            if($dados['aposentado']!='1'){
                $dados_nao_aposentado= elements(array('nome_empresa','telefone_empresa','email_comercial',
                    'data_admissao','tempo_empresa','cep','logradouro','numero','complemento','bairro',
                    'id_pais'), $this->input->post());
                 $dados_nao_aposentado['id_cliente']=$id_cliente;
                 $dados_nao_aposentado['data_admissao']=formatar_data_sql($dados_nao_aposentado['data_admissao']);
                 $dados_nao_aposentado['id_pretendente']=$id_pretendente;

                 /*VERIFICA SE JA TEM REGISTRO*/
                if($this->cliente->verifica_se_tem_dados_empresa_cliente(usuario_sessao_cliente()['id_cliente'],$id_pretendente)){
                    $this->cliente->atualiza_dados_empresa_cliente($this->cliente->verifica_se_tem_dados_empresa_cliente(usuario_sessao_cliente()['id_cliente'],$id_pretendente)['id_dados_empresa_cliente'],$dados_nao_aposentado);
                }
                else{
                    $this->cliente->inserir_dados_empresa_cliente($dados_nao_aposentado);
                }
            }
            /*=INSERE OS DADOS DO CONJUGUE DO SEGUNDO TB_CONJUGUE_PRETENDENTE=*/
            if($this->input->post('id_estado_civil')=="2"){
                $this->cliente->excluir_dados_conjugue_pretendente($id_pretendente);
                $dados_conjugue= elements(array('nome_completo_conjugue','celular_conjugue','email_conjugue',
                                'data_nascimento_conjugue','nacionalidade_conjugue','cpf_conjugue',
                                'rg_conjugue','expedicao_conjugue','data_expedicao_conjugue',
                                'profissao_conjugue','aposentado_conjugue','nome_empresa_conjugue',
                                'telefone_empresa_conjugue','email_comercial_conjugue','data_admissao_conjugue',
                                'tempo_empresa_conjugue','salario_conjugue','outras_rendas_conjugue',
                                'total_renda_conjugue'), $this->input->post());
                $dados_conjugue['id_cliente']=$id_cliente;
                $dados_conjugue['id_pretendente']=$id_pretendente;
                $dados_conjugue['data_expedicao_conjugue']=formatar_data_sql($dados_conjugue['data_expedicao_conjugue']);
                $dados_conjugue['data_admissao_conjugue']=formatar_data_sql($dados_conjugue['data_admissao_conjugue']);
                $dados_conjugue['data_nascimento_conjugue']=formatar_data_sql($dados_conjugue['data_nascimento_conjugue']);
                $this->cliente->inserir_dados_conjugue_pretendente($dados_conjugue);
            }
            /*===============================FIM==============================*/
            
             $this->db->trans_complete();
             if($envio==1){
                  /*===ENVIA EMAILS PARA O CORRETOR AVISANDO DE FICHA ADICIONADA====*/
                $email_corretor=$this->cliente->busca_email_corretor(usuario_sessao_cliente()['id_corretor']);
                $dados_email['nome_cliente']=usuario_sessao_cliente()['nome_cliente'];
                $dados_email['email_cliente']=usuario_sessao_cliente()['email'];
                $dados_email['nome_corretor']=$this->cliente->busca_nome_corretor(usuario_sessao_cliente()['id_corretor']);
                $dados_email['link']=base_url('adm/clientes/visualizar-cliente/'.usuario_sessao_cliente()['id_cliente']);
                $mensagem = $this->load->view('template/emails/ficha_pessoa_fisica_baggio', $dados_email, TRUE);
                $mensagem_cliente = $this->load->view('template/emails/ficha_pessoa_fisica_cliente', $dados_email, TRUE);
                $array_emails=array($email_corretor['email'],"juarez@baggioimoveis.com.br","andressa@baggioimoveis.com.br");
                enviar_email_fichas(usuario_sessao_cliente()['nome_cliente']." | Ficha Pessoa Física", $array_emails, $mensagem);
                enviar_email_fichas("Baggio Imóveis - ".$dados_email['nome_cliente'].", recebemos sua Ficha Pessoa Física",  array($dados_email['email_cliente']), $mensagem_cliente);
            
                /*===============================FIM==============================*/
             }
            
            if ($this->db->trans_status() === FALSE){
                $this->session->set_flashdata('mensagem','Nao Foi Possível Salvar Sua Ficha');
                log_message('error', 'ERRO SGBD - INSERIR FICHA DE CADASTRO SEGUNDO PRETENDENTE');
                redirect('cli/ficha-cadastro');
            }
    }
    
     /**
     * Faz a validação da ficha de cadastro do fiador
     *
     * @param 
     * @return views
     */
    public function validacao_fica_cadastro_fiador() {
        if($this->input->post('enviar')==="Enviar ficha para Baggio"){
             $this->_regras_validacao_fiador();
            if($this->form_validation->run()==FALSE){
                $this->_inserir_dados_fiador();
                $this->editar_ficha_cadastro_fiador();
            }
            else{
                $this->_inserir_dados_fiador($envio=1);
                $this->session->set_flashdata('mensagem','Ficha do Fiador Adicionada com Sucesso!');
                redirect('cli/ficha-cadastro');
            }
        }
        else{
            $this->_inserir_dados_fiador();
            $this->session->set_flashdata('mensagem','Ficha do Fiador Salva com Sucesso!');
            redirect('cli/ficha-cadastro');
        }
    }
    
    /**
     * Insere os dados do fiador no banco de dados
     *
     * @param int $envio Define o envio ou não da ficha para a Baggio
     * @return BOOLEAN
     */
    public function _inserir_dados_fiador($envio=0) {
        $id_cliente=usuario_sessao_cliente()['id_cliente'];
        $this->db->trans_start();
        if($this->cliente->verifica_se_tem_fiador(usuario_sessao_cliente()['id_cliente'])){
            $id_fiador=$this->cliente->verifica_se_tem_fiador(usuario_sessao_cliente()['id_cliente'])['id_fiador'];
            /*======ATUALIZA O FIADOR NA TABELA TB_FIADORES==========*/
             $dados=elements(array('nome_completo','telefone','celular','email_pessoal',
                'data_nascimento','nacionalidade','cpf','rg','expedicao','data_expedicao','id_estado_civil',
                'profissao','aposentado','salario','outras_rendas',
                'total_renda','observacoes_complementares'), $this->input->post());
            $dados['id_cliente']=$id_cliente;
            $dados['data_expedicao']=formatar_data_sql($dados['data_expedicao']);
            $dados['data_nascimento']= formatar_data_sql($dados['data_nascimento']);
            $dados['enviou']= $envio;
            $this->cliente->atualiza_fiador(usuario_sessao_cliente()['id_cliente'],$dados);
            /*===============================FIM==============================*/
        }
        else{
            /*==========INSERE O FIADOR NA TABELA TB_FIADORES========*/
            $dados=elements(array('nome_completo','telefone','celular','email_pessoal',
                'data_nascimento','nacionalidade','cpf','rg','expedicao','data_expedicao','id_estado_civil',
                'profissao','aposentado','salario','outras_rendas',
                'total_renda','observacoes_complementares'), $this->input->post());
            $dados['id_cliente']=$id_cliente;
            $dados['data_expedicao']=formatar_data_sql($dados['data_expedicao']);
            $dados['data_nascimento']= formatar_data_sql($dados['data_nascimento']);
            $dados['enviou']= $envio;
            $id_fiador=$this->cliente->inserir_fiador($dados);
            /*===============================FIM==============================*/
         }
        /*INSERE OS DADOS DA EMPRESA DO FIADOR NA TABELA TB_DADOS_EMPRESA_FIADOR*/
        if($dados['aposentado']!='1'){
            $dados_empresa=elements(array('nome_empresa','telefone_empresa','email_comercial',
                'data_admissao','tempo_empresa','cep','logradouro','numero','bairro',
                'id_pais','complemento'), $this->input->post());
            $dados_empresa['id_fiador']=$id_fiador;
            $dados_empresa['data_admissao']=formatar_data_sql($dados_empresa['data_admissao']);
            if($this->cliente->verifica_se_tem_dados_empresa_fiador($id_fiador)){
                 $this->cliente->atualiza_dados_empresa_fiador($id_fiador,$dados_empresa);
            }
            else{
                $this->cliente->inserir_dados_empresa_fiador($dados_empresa);
            }
        }
        
        /*===============================FIM==============================*/

        /*INSERE OS DADOS DE ENDEREÇO DO FIADOR NA TABELA TB_ENDERECOS_FIADORES*/
        $this->cliente->excluir_endereco_fiador($id_fiador);
        $dados_endereco=elements(array('cep_fiador','logradouro_fiador','complemento_fiador',
                'numero_fiador','id_pais_fiador','id_pais_fiador','tempo_residencia',
            'residencia_propria','bairro_fiador'), $this->input->post());
        $dados_endereco['id_fiador']=$id_fiador;
        $this->cliente->inserir_endereco_fiador($dados_endereco);
        /*===============================FIM==============================*/
        
        /*INSERE OS DADOS DAS PROPRIEDADES DO FIADOR NA TB_PROPRIEDADES_FIADORES*/
        $this->cliente->excluir_dados_propriedades_fiador($id_fiador);
        $dados_propriedades= elements(array('matricula_imovel','circunscricao','veiculo_marca',
                'renavam'), $this->input->post());
        $propriedades=array();
        for($i=0;$i<count($dados_propriedades['matricula_imovel']);$i++){
            $propriedades['id_fiador']=$id_fiador;
            $propriedades['matricula_imovel']=$dados_propriedades['matricula_imovel'][$i];
            $propriedades['circunscricao']=$dados_propriedades['circunscricao'][$i];
            $propriedades['veiculo_marca']=$dados_propriedades['veiculo_marca'][$i];
            $propriedades['renavam']=$dados_propriedades['renavam'][$i];
            $this->cliente->inserir_dados_propriedades_fiador($propriedades);
        }
        /*===============================FIM==============================*/
            
        /*INSERE OS DADOS DAS REFERENCIAS DO FIADOR NA TB_REFERENCIAS_FIADORES*/
        $this->cliente->excluir_dados_referencias_fiador($id_fiador);
        $dados_referencias= elements(array('nome_referencia','endereco_referencia','telefone_referencia','tipo_referencia'), $this->input->post());
        $referencias=array();
        for($i=0;$i<count($dados_referencias['nome_referencia']);$i++){
            $referencias['id_fiador']=$id_fiador;
            $referencias['nome_referencia']=$dados_referencias['nome_referencia'][$i];
            $referencias['endereco_referencia']=$dados_referencias['endereco_referencia'][$i];
            $referencias['telefone_referencia']=$dados_referencias['telefone_referencia'][$i];
            $referencias['tipo_referencia']=$dados_referencias['tipo_referencia'][$i];
            $this->cliente->inserir_dados_referencias_fiador($referencias);
        }
        /*===============================FIM==============================*/

        /*INSERE OS DADOS DAS REFERENCIAS DO BANCO DO FIADOR NA TB_BANCOS_REFERENCIAS_FIADORES*/
        $this->cliente->excluir_dados_banco_fiador($id_fiador);
        $dados_banco= elements(array('banco_referencia','telefone_banco_referencia','nome_agencia_referencia',
                'numero_conta_referencia'), $this->input->post());
        $banco=array();
        for($i=0;$i<count($dados_banco['banco_referencia']);$i++){
            $banco['id_fiador']=$id_fiador;
            $banco['banco_referencia']=$dados_banco['banco_referencia'][$i];
            $banco['telefone_banco_referencia']=$dados_banco['telefone_banco_referencia'][$i];
            $banco['nome_agencia_referencia']=$dados_banco['nome_agencia_referencia'][$i];
            $banco['numero_conta_referencia']=$dados_banco['numero_conta_referencia'][$i];
            $this->cliente->inserir_dados_banco_fiador($banco);
        }
        /*===============================FIM==============================*/
        
        /*INSERE OS DADOS DO CONJUGUE DO FIADOR NA TB_CONJUGUE_FIADORES*/
        if($dados['id_estado_civil']==2){
            $this->cliente->excluir_dados_conjugue_fiador($id_fiador);
            $dados_conjugue_fiador= elements(array('nome_completo_conjugue','celular_conjugue','email_conjugue',
                    'data_nascimento_conjugue','nacionalidade_conjugue','cpf_conjugue','rg_conjugue',
                    'expedicao_conjugue','data_expedicao_conjugue','profissao_conjugue','aposentado_conjugue',
                    'nome_empresa_conjugue','telefone_empresa_conjugue','email_comercial_conjugue',
                    'email_comercial_conjugue','data_admissao_conjugue','tempo_empresa_conjugue','salario_conjugue',
                    'outras_rendas_conjugue','total_renda_conjugue'), $this->input->post());
                $dados_conjugue_fiador['id_fiador']=$id_fiador;
                $dados_conjugue_fiador['data_nascimento_conjugue']= formatar_data_sql($dados_conjugue_fiador['data_nascimento_conjugue']);
                $dados_conjugue_fiador['data_expedicao_conjugue']= formatar_data_sql($dados_conjugue_fiador['data_expedicao_conjugue']);
                $dados_conjugue_fiador['data_admissao_conjugue']= formatar_data_sql($dados_conjugue_fiador['data_admissao_conjugue']);
                $this->cliente->inserir_dados_conjugue_fiador($dados_conjugue_fiador);
    }
        /*===============================FIM==============================*/
    
        $this->db->trans_complete();
        
        if($envio==1){
            /*===ENVIA EMAILS PARA O CORRETOR AVISANDO DE FICHA ADICIONADA====*/
            $email_corretor=$this->cliente->busca_email_corretor(usuario_sessao_cliente()['id_corretor']);
            $dados_email['nome_cliente']=usuario_sessao_cliente()['nome_cliente'];
            $dados_email['email_cliente']=usuario_sessao_cliente()['email'];
            $dados_email['nome_corretor']=$this->cliente->busca_nome_corretor(usuario_sessao_cliente()['id_corretor']);
            $dados_email['link']=base_url('adm/clientes/visualizar-cliente/'.usuario_sessao_cliente()['id_cliente']);
            $mensagem = $this->load->view('template/emails/ficha_fiador_baggio', $dados_email, TRUE);
            $mensagem_cliente = $this->load->view('template/emails/ficha_fiador_cliente', $dados_email, TRUE);
            $array_emails=array($email_corretor['email'],"juarez@baggioimoveis.com.br","andressa@baggioimoveis.com.br");
            enviar_email_fichas(usuario_sessao_cliente()['nome_cliente']." | Ficha Pessoa Física Fiador", $array_emails, $mensagem);
            enviar_email_fichas("Baggio Imóveis - ".$dados_email['nome_cliente'].", recebemos sua Ficha Pessoa Física Fiador",  array($dados_email['email_cliente']), $mensagem_cliente);  
        
            /*===============================FIM==============================*/
        }
            
        if ($this->db->trans_status() === FALSE){
            $this->session->set_flashdata('mensagem','Nao Foi Possível Salvar Sua Ficha');
            log_message('error', 'ERRO SGBD - INSERIR FICHA DE CADASTRO FIADOR - ');
            redirect('cli/ficha-cadastro');
        }
    }
    
    /**
     * Regras de Validação das fichas de cadastros
     *
     * @param 
     * @return BOOLEAN
     */
    public function _regras_validacao() {
        $this->form_validation->set_rules('nome_completo','Nome Completo','required');
        $this->form_validation->set_rules('telefone','Telefone','required');
        $this->form_validation->set_rules('celular','Celular','required');
        $this->form_validation->set_rules('email_pretendente','E-mail Pretendente','required');
        $this->form_validation->set_rules('email_morador','Email Morador','required');
        $this->form_validation->set_rules('data_nascimento','Data de Nascimento','required');
        $this->form_validation->set_rules('nacionalidade','Nacionalidade','required');
        $this->form_validation->set_rules('cpf','CPF','required');
        $this->form_validation->set_rules('rg','RG','required');
        $this->form_validation->set_rules('expedicao','Local de Expedição','required');
        $this->form_validation->set_rules('data_expedicao','Data da Expedição','required');
        $this->form_validation->set_rules('id_estado_civil','Estado Civil','required');
        $this->form_validation->set_rules('profissao','Profissão','required');
        $this->form_validation->set_rules('aposentado','Aposentado?','required');
        if($this->input->post('aposentado')==0 && $this->input->post('aposentado')!=NULL){
            $this->form_validation->set_rules('nome_empresa','Nome da Empresa','required');
            $this->form_validation->set_rules('telefone_empresa','Telefone da Empresa','required');
            $this->form_validation->set_rules('email_comercial','Email Comercial','required');
            $this->form_validation->set_rules('data_admissao','Data de Admissão','required');
            $this->form_validation->set_rules('tempo_empresa','Tempo de Empresa','required');
            $this->form_validation->set_rules('cep','Cep da Empresa','required');
            $this->form_validation->set_rules('logradouro','Logradouro da Empresa','required');
            $this->form_validation->set_rules('numero','Número da Empresa','required');
            $this->form_validation->set_rules('bairro','Bairro da Empresa','required');
            $this->form_validation->set_rules('id_pais','País da Empresa','required');
        }
        $this->form_validation->set_rules('salario','Salário','required');
        $this->form_validation->set_rules('outras_rendas','Outras Rendas','required');
        $this->form_validation->set_rules('total_renda','Total da Renda','required');
        $this->form_validation->set_rules('cep_cliente','CEP Residência','required');
        $this->form_validation->set_rules('logradouro_cliente','Logradouro da Residência','required');
        $this->form_validation->set_rules('numero_cliente','Número da Residência','required');
        $this->form_validation->set_rules('bairro_cliente','Bairro de Residência','required');
        $this->form_validation->set_rules('id_pais_cliente','País Residência','required');
        $this->form_validation->set_rules('tempo_residencia','Tempo de Residência','required');
        $this->form_validation->set_rules('residencia_propria','Residencia Própria','required');
        if($this->input->post('residencia_propria')==0 && $this->input->post('residencia_propria')!=NULL){
            $this->form_validation->set_rules('aluguel_atual','Aluguel Atual','required');
        }
        $this->form_validation->set_rules('motivo_mudanca','Motivo da Mudança','required');
        $this->form_validation->set_rules('imobiliaria_atual','Nome do Locador ou Imobiliária Atual','required');
        $this->form_validation->set_rules('telefone_imobiliaria_atual','Telefone do Locador ou Imobiliária Atual','required');
        $this->form_validation->set_rules('nome_completo_residente[]','Nome Completo','required');
        $this->form_validation->set_rules('parentesco_residente[]','Parentesco','required');
        $this->form_validation->set_rules('telefone_residente[]','Telefone do Residente','required');
        $this->form_validation->set_rules('matricula_imovel[]','Matrícula do Imóvel','required');
        $this->form_validation->set_rules('circunscricao[]','Circunscrição','required');
        $this->form_validation->set_rules('veiculo_marca[]','Marca do Veículo','required');
        $this->form_validation->set_rules('renavam[]','Renavam','required');
        $this->form_validation->set_rules('nome_referencia[]','Nome Completo','required');
        $this->form_validation->set_rules('endereco_referencia[]','Endereço','required');
        $this->form_validation->set_rules('telefone_referencia[]','Telefone','required');
        $this->form_validation->set_rules('banco_referencia[]','Banco','required');
        $this->form_validation->set_rules('telefone_banco_referencia[]','Telefone','required');
        $this->form_validation->set_rules('nome_agencia_referencia[]','Nome da Agência','required');
        $this->form_validation->set_rules('numero_conta_referencia[]','Número da Conta','required');
        $this->form_validation->set_rules('status_mae','Sua Mãe é Falecida','required');
        $this->form_validation->set_rules('status_pai','Seu Pai é Falecido','required');
        if($this->input->post('status_mae')==0 && $this->input->post('status_mae')!=NULL){
            $this->form_validation->set_rules('mae_pretendente','Nome da Mãe','required');
            $this->form_validation->set_rules('telefone_mae_pretendente','Telefone da Mãe','required');
            $this->form_validation->set_rules('cep_mae_cliente','Cep da Mãe','required');
            $this->form_validation->set_rules('logradouro_mae_cliente','Logradouro da Mãe','required');
            $this->form_validation->set_rules('numero_mae_cliente','Número da Mãe','required');
            $this->form_validation->set_rules('bairro_mae_cliente','Bairro da Mãe','required');
            $this->form_validation->set_rules('id_pais_residencia_mae','País da Mãe','required');
        }
        if($this->input->post('status_pai')==0 && $this->input->post('status_pai')!=NULL){
            $this->form_validation->set_rules('pai_pretendente','Nome do Pai','required');
            $this->form_validation->set_rules('telefone_pai_pretendente','Telefone do Pai','required');
            $this->form_validation->set_rules('cep_pai_cliente','Cep do Pai','required');
            $this->form_validation->set_rules('logradouro_pai_cliente','Logradouro do Pai','required');
            $this->form_validation->set_rules('numero_pai_cliente','Número do Pai','required');
            $this->form_validation->set_rules('bairro_pai_cliente','Bairro do Pai','required');
            $this->form_validation->set_rules('id_pais_residencia_pai','País do Pai','required');
        }
        $this->form_validation->set_rules('concordo','Marque a Caixa de Observação','required');
        if($this->input->post('id_estado_civil')==2){
            $this->form_validation->set_rules('nome_completo_conjugue','Nome Completo do Conjugue','required');
            $this->form_validation->set_rules('celular_conjugue','Celular do Conjugue','required');
            $this->form_validation->set_rules('email_conjugue','E-mail do Conjugue','required');
            $this->form_validation->set_rules('data_nascimento_conjugue','Data de Nascimento do Conjugue','required');
            $this->form_validation->set_rules('nacionalidade_conjugue','Nacionalidade do Conjugue','required');
            $this->form_validation->set_rules('cpf_conjugue','CPF do Conjugue','required');
            $this->form_validation->set_rules('rg_conjugue','RG do Conjugue','required');
            $this->form_validation->set_rules('expedicao_conjugue','Local de Expedição do RG do Conjugue','required');
            $this->form_validation->set_rules('data_expedicao_conjugue','Data de Expedição do RG do Conjugue','required');
            $this->form_validation->set_rules('profissao_conjugue','Profissão do Conjugue','required');
            $this->form_validation->set_rules('aposentado_conjugue','Aposentado ou do Lar','required');
            if($this->input->post('aposentado_conjugue')==0 && $this->input->post('aposentado')!=NULL){
                $this->form_validation->set_rules('nome_empresa_conjugue','Nome da Empresa do Conjugue','required');
                $this->form_validation->set_rules('telefone_empresa_conjugue','Telefone da Empresa do Conjugue','required');
                $this->form_validation->set_rules('email_comercial_conjugue','E-mail da Empresa do Conjugue','required');
                $this->form_validation->set_rules('data_admissao_conjugue','Data da Admissão do Conjugue','required');
                $this->form_validation->set_rules('tempo_empresa_conjugue','Tempo de Empresa do Conjugue','required');
                $this->form_validation->set_rules('salario_conjugue','Salário do Conjugue','required');
                $this->form_validation->set_rules('outras_rendas_conjugue','Outras Rendas do Conjugue','required');
                $this->form_validation->set_rules('total_renda_conjugue','Total da Renda do Conjugue','required');
            }
        }
        $this->form_validation->set_rules('segundo_pretendente','Possui 2º Pretendente','required');
        if($this->input->post('segundo_pretendente')==1 && $this->input->post('segundo_pretendente')!=NULL){
            $this->form_validation->set_rules('segundo_pretendente_conjugue','O 2º pretendete é seu cônjuge','required');
        }
    }
    
    /**
     * Regras de Validação da ficha de cadastro do segundo pretendente
     *
     * @param 
     * @return BOOLEAN
     */
    public function _regras_validacao_segundo_pretendente() {
        $this->form_validation->set_rules('nome_completo','Nome Completo','required');
        $this->form_validation->set_rules('telefone','Telefone','required');
        $this->form_validation->set_rules('celular','Celular','required');
        $this->form_validation->set_rules('email_pretendente','E-mail Pretendente','required');
        $this->form_validation->set_rules('email_morador','Email Morador','required');
        $this->form_validation->set_rules('data_nascimento','Data de Nascimento','required');
        $this->form_validation->set_rules('nacionalidade','Nacionalidade','required');
        $this->form_validation->set_rules('cpf','CPF','required');
        $this->form_validation->set_rules('rg','RG','required');
        $this->form_validation->set_rules('expedicao','Local de Expedição','required');
        $this->form_validation->set_rules('data_expedicao','Data da Expedição','required');
        $this->form_validation->set_rules('id_estado_civil','Estado Civil','required');
        $this->form_validation->set_rules('profissao','Profissão','required');
        $this->form_validation->set_rules('aposentado','Aposentado?','required');
        if($this->input->post('aposentado')==0 && $this->input->post('aposentado')!=NULL){
            $this->form_validation->set_rules('nome_empresa','Nome da Empresa','required');
            $this->form_validation->set_rules('telefone_empresa','Telefone da Empresa','required');
            $this->form_validation->set_rules('email_comercial','Email Comercial','required');
            $this->form_validation->set_rules('data_admissao','Data de Admissão','required');
            $this->form_validation->set_rules('tempo_empresa','Tempo de Empresa','required');
            $this->form_validation->set_rules('cep','Cep da Empresa','required');
            $this->form_validation->set_rules('logradouro','Logradouro da Empresa','required');
            $this->form_validation->set_rules('numero','Número da Empresa','required');
            $this->form_validation->set_rules('bairro','Bairro da Empresa','required');
            $this->form_validation->set_rules('id_pais','País da Empresa','required');
        }
        $this->form_validation->set_rules('salario','Salário','required');
        $this->form_validation->set_rules('outras_rendas','Outras Rendas','required');
        $this->form_validation->set_rules('total_renda','Total da Renda','required');
        if($this->input->post('id_estado_civil')==2){
            $this->form_validation->set_rules('nome_completo_conjugue','Nome Completo do Conjugue','required');
            $this->form_validation->set_rules('celular_conjugue','Celular do Conjugue','required');
            $this->form_validation->set_rules('email_conjugue','E-mail do Conjugue','required');
            $this->form_validation->set_rules('data_nascimento_conjugue','Data de Nascimento do Conjugue','required');
            $this->form_validation->set_rules('nacionalidade_conjugue','Nacionalidade do Conjugue','required');
            $this->form_validation->set_rules('cpf_conjugue','CPF do Conjugue','required');
            $this->form_validation->set_rules('rg_conjugue','RG do Conjugue','required');
            $this->form_validation->set_rules('expedicao_conjugue','Local de Expedição do RG do Conjugue','required');
            $this->form_validation->set_rules('data_expedicao_conjugue','Data de Expedição do RG do Conjugue','required');
            $this->form_validation->set_rules('profissao_conjugue','Profissão do Conjugue','required');
            $this->form_validation->set_rules('aposentado_conjugue','Aposentado ou do Lar','required');
            if($this->input->post('aposentado_conjugue')==0 && $this->input->post('aposentado')!=NULL){
                $this->form_validation->set_rules('nome_empresa_conjugue','Nome da Empresa do Conjugue','required');
                $this->form_validation->set_rules('telefone_empresa_conjugue','Telefone da Empresa do Conjugue','required');
                $this->form_validation->set_rules('email_comercial_conjugue','E-mail da Empresa do Conjugue','required');
                $this->form_validation->set_rules('data_admissao_conjugue','Data da Admissão do Conjugue','required');
                $this->form_validation->set_rules('tempo_empresa_conjugue','Tempo de Empresa do Conjugue','required');
                $this->form_validation->set_rules('salario_conjugue','Salário do Conjugue','required');
                $this->form_validation->set_rules('outras_rendas_conjugue','Outras Rendas do Conjugue','required');
                $this->form_validation->set_rules('total_renda_conjugue','Total da Renda do Conjugue','required');
            }
        }
    }
    
    /**
     * Regras de Validação da ficha de cadastro do fiador
     *
     * @param 
     * @return BOOLEAN
     */
    public function _regras_validacao_fiador() {
        $this->form_validation->set_rules('nome_completo','Nome Completo','required');
        $this->form_validation->set_rules('telefone','Telefone','required');
        $this->form_validation->set_rules('celular','Celular','required');
        $this->form_validation->set_rules('email_pessoal','E-mail Pessoal','required');
        $this->form_validation->set_rules('data_nascimento','Data de Nascimento','required');
        $this->form_validation->set_rules('nacionalidade','Nacionalidade','required');
        $this->form_validation->set_rules('cpf','CPF','required');
        $this->form_validation->set_rules('rg','RG','required');
        $this->form_validation->set_rules('expedicao','Local de Expedição','required');
        $this->form_validation->set_rules('data_expedicao','Data da Expedição','required');
        $this->form_validation->set_rules('id_estado_civil','Estado Civil','required');
        $this->form_validation->set_rules('profissao','Profissão','required');
        $this->form_validation->set_rules('aposentado','Aposentado?','required');
        if($this->input->post('aposentado')==0 && $this->input->post('aposentado')!=NULL){
            $this->form_validation->set_rules('nome_empresa','Nome da Empresa','required');
            $this->form_validation->set_rules('telefone_empresa','Telefone da Empresa','required');
            $this->form_validation->set_rules('email_comercial','Email Comercial','required');
            $this->form_validation->set_rules('data_admissao','Data de Admissão','required');
            $this->form_validation->set_rules('tempo_empresa','Tempo de Empresa','required');
            $this->form_validation->set_rules('cep','Cep da Empresa','required');
            $this->form_validation->set_rules('logradouro','Logradouro da Empresa','required');
            $this->form_validation->set_rules('numero','Número da Empresa','required');
            $this->form_validation->set_rules('bairro','Bairro da Empresa','required');
            $this->form_validation->set_rules('id_pais','País da Empresa','required');
        }
        $this->form_validation->set_rules('salario','Salário','required');
        $this->form_validation->set_rules('outras_rendas','Outras Rendas','required');
        $this->form_validation->set_rules('total_renda','Total da Renda','required');
        $this->form_validation->set_rules('cep_fiador','CEP Residência','required');
        $this->form_validation->set_rules('logradouro_fiador','Logradouro da Residência','required');
        $this->form_validation->set_rules('numero_fiador','Número da Residência','required');
        $this->form_validation->set_rules('bairro_fiador','Bairro de Residência','required');
        $this->form_validation->set_rules('id_pais_fiador','País Residência','required');
        $this->form_validation->set_rules('tempo_residencia','Tempo de Residência','required');
        $this->form_validation->set_rules('residencia_propria','Residencia Própria','required');
        $this->form_validation->set_rules('matricula_imovel[]','Matrícula do Imóvel','required');
        $this->form_validation->set_rules('circunscricao[]','Circunscrição','required');
        $this->form_validation->set_rules('veiculo_marca[]','Marca do Veículo','required');
        $this->form_validation->set_rules('renavam[]','Renavam','required');
        $this->form_validation->set_rules('nome_referencia[]','Nome Completo','required');
        $this->form_validation->set_rules('endereco_referencia[]','Endereço','required');
        $this->form_validation->set_rules('telefone_referencia[]','Telefone','required');
        $this->form_validation->set_rules('banco_referencia[]','Banco','required');
        $this->form_validation->set_rules('telefone_banco_referencia[]','Telefone','required');
        $this->form_validation->set_rules('nome_agencia_referencia[]','Nome da Agência','required');
        $this->form_validation->set_rules('numero_conta_referencia[]','Número da Conta','required');
        if($this->input->post('id_estado_civil')==2){
            $this->form_validation->set_rules('nome_completo_conjugue','Nome Completo do Conjugue','required');
            $this->form_validation->set_rules('celular_conjugue','Celular do Conjugue','required');
            $this->form_validation->set_rules('email_conjugue','E-mail do Conjugue','required');
            $this->form_validation->set_rules('data_nascimento_conjugue','Data de Nascimento do Conjugue','required');
            $this->form_validation->set_rules('nacionalidade_conjugue','Nacionalidade do Conjugue','required');
            $this->form_validation->set_rules('cpf_conjugue','CPF do Conjugue','required');
            $this->form_validation->set_rules('rg_conjugue','RG do Conjugue','required');
            $this->form_validation->set_rules('expedicao_conjugue','Local de Expedição do RG do Conjugue','required');
            $this->form_validation->set_rules('data_expedicao_conjugue','Data de Expedição do RG do Conjugue','required');
            $this->form_validation->set_rules('profissao_conjugue','Profissão do Conjugue','required');
            $this->form_validation->set_rules('aposentado_conjugue','Aposentado ou do Lar','required');
            if($this->input->post('aposentado_conjugue')==0 && $this->input->post('aposentado')!=NULL){
                $this->form_validation->set_rules('nome_empresa_conjugue','Nome da Empresa do Conjugue','required');
                $this->form_validation->set_rules('telefone_empresa_conjugue','Telefone da Empresa do Conjugue','required');
                $this->form_validation->set_rules('email_comercial_conjugue','E-mail da Empresa do Conjugue','required');
                $this->form_validation->set_rules('data_admissao_conjugue','Data da Admissão do Conjugue','required');
                $this->form_validation->set_rules('tempo_empresa_conjugue','Tempo de Empresa do Conjugue','required');
                $this->form_validation->set_rules('salario_conjugue','Salário do Conjugue','required');
                $this->form_validation->set_rules('outras_rendas_conjugue','Outras Rendas do Conjugue','required');
                $this->form_validation->set_rules('total_renda_conjugue','Total da Renda do Conjugue','required');
            }
        }
    }
}
