<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Controller responsável pelas operações do perfil do cliente pessoa jurídica
 *
 * PHP version 7
 *
 * @category PHP
 * @author   Kellton Leitão <kelltonl10@gmail.com>
 */
class AreaClienteJuridicoController extends CI_Controller {

     public function __construct() {
        parent::__construct();
        if(!usuario_sessao_cliente()) {
            $this->session->set_flashdata('param_msg_erro_cabecalho', 'Sem Permissão');
            $this->session->set_flashdata('param_msg_erro', 'Você não tem permissão de acessar esta área!');
            redirect(base_url());
        }
        if(usuario_sessao_cliente()['cliente']!=="Pessoa Jurídica"){
            $this->session->set_flashdata('param_msg_erro_cabecalho', 'Sem Permissão');
            $this->session->set_flashdata('param_msg_erro', 'Você não tem permissão de acessar esta área!');
            redirect(base_url());
        }
        $this->load->model('ClienteModel','cliente');
    }
    
    /**
     * Carrega a página inicial do cliente
     *
     * @param nenhum
     * @return views
     */
    public function index() {
        $dados['tem_dados']=$this->cliente->verifica_se_tem_dados_juridico(usuario_sessao_cliente()['id_cliente']);        
        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $this->load->view('template/menu');
        $this->load->view('adm_cliente/juridico/home',$dados);
        $this->load->view('template/footer');
    }
    
    /**
     * Carrega a página de alterar dados do cliente
     *
     * @param nenhum
     * @return views
     */
    public function alterar_dados() {
        $id_cliente=usuario_sessao_cliente()['id_cliente'];
        $dados['query']=$this->cliente->buscar_cliente($id_cliente);
        $dados['corretores']=$this->cliente->listar_corretores();
        $dados['tipo_usuario']=$this->cliente->listar_tipo_usuario();
        $dados['query']['id_cliente']=$id_cliente;
        
        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $this->load->view('template/menu');
        $this->load->view('adm_cliente/juridico/alterar_dados',$dados);
        $this->load->view('template/footer');
    }
    
    /**
    * Faz a validação da ediçao dos dados do cliente
    *
    * @param
    * @return BOOLEAN
    */
    public function validacao_editar_cliente() {
        $this->form_validation->set_rules('nome_cliente','Nome','required');
        $this->form_validation->set_rules('telefone','Telefone','required');
        if($this->form_validation->run()==FALSE) {
            $this->alterar_dados();
        }
        else{
            $id_cliente=usuario_sessao_cliente()['id_cliente'];
            $cliente=elements(array('nome_cliente','telefone'), $this->input->post());
            if($this->input->post('senha')!='') {
                $cliente['senha']=md5($this->input->post('senha'));
            }
            $this->cliente->editar_cliente($cliente,$id_cliente);
            $this->session->set_flashdata('mensagem','Alteração Efetuada com Sucesso!!!');
            redirect('cli-juridico');
        }
    }
}
