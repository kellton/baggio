<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * IMOVELCONTROLLER
 * -------------------------------------------------------------------
 * DESCRICAO : 
 * -------------------------------------------------------------------
 * CARREGA : 
 * - N/A
 */
class ImovelController extends CI_Controller {

    /*
     * CONSTRUCT
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function __construct() {
        parent::__construct();

        if(!is_usuario_adm()) {
            $this->session->set_flashdata('param_msg_erro_cabecalho', 'Sem Permissão');
            $this->session->set_flashdata('param_msg_erro', 'Você não tem permissão de acessar esta área!');

            redirect(base_url());
        }

        $this->load->model('ImovelModel','m_imoveis');
        $this->load->model('EnderecoModel','m_enderecos');
        $this->load->model('TipoModel','m_tipos');
        $this->load->model('UsuarioModel','m_usuarios');
        $this->load->model('ImagemModel','m_imagens');
        $this->load->model('NegociacaoModel','m_negociacoes');
        $this->load->model('MaterialApoioModel','m_materiais');
    }
    
    /*
     * INDEX
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function index() {
        $dados = $this->paginacao();

        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $this->load->view('template/mensagens');
        $this->load->view('adm/menu');
        $this->load->view('adm/imovel/listar_imoveis', $dados);
        $this->load->view('template/footer');
    }

    /* ****************************************************** */
    /* ***************** FUNCOES DE PAGINA ****************** */
    /* ****************************************************** */
    /*
     * CARREGAR_FORMULARIO_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function carregar_formulario_imovel() {
        $dados_negociacao = array();
        $dados_informacoes_basicas = array();
        $dados_veiculacao = array();
        $dados_informacoes_complementares = array();
        $dados_gerenciais = array();

        /* VIEW NEGOCIACAO */
        $dados_negociacao['tipo_negociacoes'] = listar_dropdown($this->m_tipos->listar_tipo_negociacoes());
        $dados_negociacao['lojas'] = listar_dropdown($this->m_imoveis->listar_lojas());
        $dados_negociacao['tipo_imoveis'] = $this->m_tipos->listar_tipo_imoveis();
        $dados_negociacao['subtipo_imoveis'] = listar_dropdown($this->m_tipos->listar_dropdown_subtipo_imoveis(), FALSE);
        /* VIEW INFORMACOES BASICAS */
        $dados_informacoes_basicas['cidades'] = listar_dropdown($this->m_enderecos->listar_cidades_estado());
        $dados_informacoes_basicas['comissoes'] = listar_dropdown($this->m_tipos->listar_comissoes(), TRUE, '0%');
        /* VIEW VEICULACAO */
        $dados_veiculacao['privacidades'] = listar_dropdown($this->m_tipos->listar_privacidades(), FALSE);
        $dados_veiculacao['hoje'] = date("Ex: d/m/Y - ") . dia_semana();
        /* VIEW INFORMACOES COMPLEMENTARES */
        // TAB MAIS INFORMACOES
        $dados_mais_informacoes = array();
        $dados_mais_informacoes['tipo_ocupacoes'] = listar_dropdown($this->m_tipos->listar_tipo_ocupacoes());
        $dados_mais_informacoes['tipo_andares'] = listar_dropdown($this->m_tipos->listar_tipo_andares());
        $dados_mais_informacoes['tipo_faces'] = listar_dropdown($this->m_tipos->listar_tipo_faces());
        $dados_informacoes_complementares['tab_mais_informacoes'] = $this->load->view('adm/imovel/cadastro/tabs/mais_informacoes', $dados_mais_informacoes, TRUE);
        // TAB ESPECIFICACOES
        $dados_especificacoes = array();
        $dados_especificacoes['tipo_faces'] = listar_dropdown($this->m_tipos->listar_tipo_faces(), FALSE);
        $dados_especificacoes['tipo_garagens'] = listar_dropdown($this->m_tipos->listar_tipo_garagens(), FALSE);
        $dados_especificacoes['tipo_locais_garagem'] = listar_dropdown($this->m_tipos->listar_tipo_locais_garagem(), FALSE);
        $dados_especificacoes['tipo_situacoes_documental'] = listar_dropdown($this->m_tipos->listar_tipo_situacao_documental(), FALSE);
        $dados_especificacoes['tipo_financiamentos'] = listar_dropdown($this->m_tipos->listar_tipo_financiamentos(), FALSE);
        $dados_informacoes_complementares['tab_especificacoes'] = $this->load->view('adm/imovel/cadastro/tabs/especificacoes', $dados_especificacoes, TRUE);
        // TAB INFRAESTRUTURA
        $dados_infraestrutura = array();
        $dados_infraestrutura['tipo_zoneamentos'] = listar_dropdown($this->m_tipos->listar_tipo_zoneamentos());
        $dados_infraestrutura['tipo_faces'] = listar_dropdown($this->m_tipos->listar_tipo_faces(), FALSE);
        $dados_infraestrutura['tipo_materiais'] = listar_dropdown($this->m_tipos->listar_tipo_materiais(), FALSE);
        $dados_informacoes_complementares['tab_infraestrutura'] = $this->load->view('adm/imovel/cadastro/tabs/infraestrutura', $dados_infraestrutura, TRUE);
        // TAB VALORES VENDA
        $dados_informacoes_complementares['tab_valores_venda'] = $this->load->view('adm/imovel/cadastro/tabs/valores_venda', $dados_infraestrutura, TRUE);
        // TAB VALORES LOCACAO
        $dados_informacoes_complementares['tab_valores_locacao'] = $this->load->view('adm/imovel/cadastro/tabs/valores_locacao', $dados_infraestrutura, TRUE);
        // TAB CHAVES
        $dados_informacoes_complementares['tab_chaves'] = $this->load->view('adm/imovel/cadastro/tabs/chaves', $dados_infraestrutura, TRUE);

        $dados_gerenciais['corretores'] = listar_dropdown($this->m_usuarios->listar_corretores_nome());

        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $this->load->view('template/mensagens');
        $this->load->view('adm/menu');
        $this->load->view('adm/imovel/cadastro/header');
        $this->load->view('adm/imovel/cadastro/negociacao', $dados_negociacao);
        $this->load->view('adm/imovel/cadastro/informacoes_basicas', $dados_informacoes_basicas);
        $this->load->view('adm/imovel/cadastro/veiculacao', $dados_veiculacao);
        $this->load->view('adm/imovel/cadastro/fotos');
        $this->load->view('adm/imovel/cadastro/informacoes_complementares', $dados_informacoes_complementares);
        // $this->load->view('adm/imovel/cadastro/materiais');
        $this->load->view('adm/imovel/cadastro/dados_gerenciais', $dados_gerenciais);
        $this->load->view('adm/imovel/cadastro/footer');
        $this->load->view('template/footer');
    }

    /*
     * CARREGAR_GERENCIAR_IMOVEIS
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function carregar_gerenciar_imoveis() {
        $url_paginacao = "adm/gerenciar/imoveis";
        $pagina = $this->uri->segment(calcular_uri_paginacao($url_paginacao));

        if ($pagina == NULL || empty($pagina)) {
            $pagina = "0";
        }

        $lista_imoveis = $this->_preparar_lista_gerenciar_imoveis($pagina);
        $dados['paginacao']  = paginar($url_paginacao, 10, 
                                       $this->m_imoveis->quantificar_imoveis(), 
                                       $lista_imoveis
        );

        $cabecalho = array(
            "ID","# Baggio","Tipo de Imóvel", 
            "Referência", "Título", "Descrição",
            "Editar", "Excluir");
        $html_sem_resultados = "";
        $html_sem_resultados .= "<div class='row'>";
        $html_sem_resultados .= "<div class='col texto-central'>";
        $html_sem_resultados .= "<h1 class='texto-sem-resultados'>";
        $html_sem_resultados .= "Não há imóveis cadastrados.";
        $html_sem_resultados .= "</h1>";
        $html_sem_resultados .= "</div>";
        $html_sem_resultados .= "</div>";

        $dados['tabela_imoveis'] = tabelar($lista_imoveis, $cabecalho, "table-responsive", "", "", "celula-texto-central", $html_sem_resultados);

        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $this->load->view('template/mensagens');
        $this->load->view('adm/menu');
        $this->load->view('adm/imovel/gerenciar_imoveis', $dados);
        $this->load->view('template/footer');
    }

    /*
     * CARREGAR_EDITAR_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function carregar_editar_imovel() {
        $url_paginacao = "adm/imoveis/editar-imovel";
        $id_imovel = $this->uri->segment(calcular_uri_paginacao($url_paginacao));

        $imovel = $this->m_imoveis->buscar_imovel($id_imovel);
        $imovel['chaves'] = $this->m_imoveis->buscar_chaves($id_imovel);

        if ($imovel == NULL) {
            $this->carregar_gerenciar_imoveis();
        } else {
            // to_debug($this->db->last_query());
            // to_debug($imovel);

            $dados['imovel'] = $imovel;
            $dados_negociacao = array();
            $dados_informacoes_basicas = array();
            $dados_veiculacao = array();
            $dados_informacoes_complementares = array();
            $dados_gerenciais = array();

            /* VIEW NEGOCIACAO */
            $dados_negociacao['imovel'] = $imovel;
            $dados_negociacao['tipo_negociacoes'] = listar_dropdown($this->m_tipos->listar_tipo_negociacoes());
            $dados_negociacao['lojas'] = listar_dropdown($this->m_imoveis->listar_lojas());
            $dados_negociacao['tipo_imoveis'] = $this->m_tipos->listar_tipo_imoveis();
            $dados_negociacao['subtipo_imoveis'] = listar_dropdown($this->m_tipos->listar_dropdown_subtipo_imoveis(), FALSE);
            /* VIEW INFORMACOES BASICAS */
            $dados_informacoes_basicas['imovel'] = $imovel;
            $dados_informacoes_basicas['cidades'] = listar_dropdown($this->m_enderecos->listar_cidades_estado());
            $dados_informacoes_basicas['comissoes'] = listar_dropdown($this->m_tipos->listar_comissoes(), TRUE, '0%');
            /* VIEW VEICULACAO */
            $dados_veiculacao['imovel'] = $imovel;
            $dados_veiculacao['privacidades'] = listar_dropdown($this->m_tipos->listar_privacidades(), FALSE);
            $dados_veiculacao['hoje'] = date("Ex: d/m/Y - ") . dia_semana();
            /* VIEW INFORMACOES COMPLEMENTARES */
            // TAB MAIS INFORMACOES
            $dados_mais_informacoes = array();
            $dados_mais_informacoes['imovel'] = $imovel;
            $dados_mais_informacoes['tipo_ocupacoes'] = listar_dropdown($this->m_tipos->listar_tipo_ocupacoes());
            $dados_mais_informacoes['tipo_andares'] = listar_dropdown($this->m_tipos->listar_tipo_andares());
            $dados_mais_informacoes['tipo_faces'] = listar_dropdown($this->m_tipos->listar_tipo_faces());
            $dados_informacoes_complementares['tab_mais_informacoes'] = $this->load->view('adm/imovel/edicao/tabs/mais_informacoes', $dados_mais_informacoes, TRUE);
            // TAB ESPECIFICACOES
            $dados_especificacoes = array();
            $dados_especificacoes['imovel'] = $imovel;
            $dados_especificacoes['tipo_faces'] = listar_dropdown($this->m_tipos->listar_tipo_faces(), FALSE);
            $dados_especificacoes['tipo_garagens'] = listar_dropdown($this->m_tipos->listar_tipo_garagens(), FALSE);
            $dados_especificacoes['tipo_locais_garagem'] = listar_dropdown($this->m_tipos->listar_tipo_locais_garagem(), FALSE);
            $dados_especificacoes['tipo_situacoes_documental'] = listar_dropdown($this->m_tipos->listar_tipo_situacao_documental(), FALSE);
            $dados_especificacoes['tipo_financiamentos'] = listar_dropdown($this->m_tipos->listar_tipo_financiamentos(), FALSE);
            $dados_informacoes_complementares['tab_especificacoes'] = $this->load->view('adm/imovel/edicao/tabs/especificacoes', $dados_especificacoes, TRUE);
            // TAB INFRAESTRUTURA
            $dados_infraestrutura = array();
            $dados_infraestrutura['imovel'] = $imovel;
            $dados_infraestrutura['tipo_zoneamentos'] = listar_dropdown($this->m_tipos->listar_tipo_zoneamentos());
            $dados_infraestrutura['tipo_faces'] = listar_dropdown($this->m_tipos->listar_tipo_faces(), FALSE);
            $dados_infraestrutura['tipo_materiais'] = listar_dropdown($this->m_tipos->listar_tipo_materiais(), FALSE);
            $dados_informacoes_complementares['tab_infraestrutura'] = $this->load->view('adm/imovel/edicao/tabs/infraestrutura', $dados_infraestrutura, TRUE);
            // TAB VALORES VENDA
            $dados_informacoes_complementares['tab_valores_venda'] = $this->load->view('adm/imovel/edicao/tabs/valores_venda', $dados_infraestrutura, TRUE);
            // TAB VALORES LOCACAO
            $dados_informacoes_complementares['tab_valores_locacao'] = $this->load->view('adm/imovel/edicao/tabs/valores_locacao', $dados_infraestrutura, TRUE);
            // TAB CHAVES
            $dados_informacoes_complementares['tab_chaves'] = $this->load->view('adm/imovel/edicao/tabs/chaves', $dados_infraestrutura, TRUE);
            // DADOS GERENCIAIS
            $dados_gerenciais['imovel'] = $imovel;
            $dados_gerenciais['corretores'] = listar_dropdown($this->m_usuarios->listar_corretores_nome());

            $header['imovel'] = $imovel;
            $header["scripts"] = carregar_js();
            $header["styles"] = carregar_css();

            $this->load->view('template/header', $header);
            $this->load->view('template/mensagens');
            $this->load->view('adm/menu');
            $this->load->view('adm/imovel/edicao/header', $dados);
            $this->load->view('adm/imovel/edicao/negociacao', $dados_negociacao);
            $this->load->view('adm/imovel/edicao/informacoes_basicas', $dados_informacoes_basicas);
            $this->load->view('adm/imovel/edicao/veiculacao', $dados_veiculacao);

            if (!$imovel['xml_atualizavel']) {
                $this->load->view('adm/imovel/edicao/fotos');
            }

            $this->load->view('adm/imovel/edicao/informacoes_complementares', $dados_informacoes_complementares);
            // $this->load->view('adm/imovel/cadastro/materiais');
            $this->load->view('adm/imovel/edicao/dados_gerenciais', $dados_gerenciais);
            $this->load->view('adm/imovel/edicao/footer');
            $this->load->view('template/footer'); 
        }
    }

    /* ****************************************************** */
    /* ******************* FUNCOES GERAIS ******************* */
    /* ****************************************************** */
    /*
     * ADICIONAR_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function adicionar_imovel() {
        if (!$this->_validar_formulario_imovel()) {
            $this->carregar_formulario_imovel();
        } else {
            $dados = elements( array(
                /* NEGOCIACAO */
                "param_id_tipo_negociacao","param_id_loja",
                "param_id_subtipo_imovel",
                /*"param_id_tipo_imovel",*/
                /* INFORMACOES BASICAS */
                "param_titulo","param_id_cidade","param_referencia","param_cep",
                "param_endereco_logradouro","param_endereco_numero",
                "param_endereco_complemento","param_endereco_lat","param_endereco_lng",
                "param_endereco_bairro","param_valor_negociacao","param_id_comissao",
                /* VEICULACAO */
                "param_id_privacidade","param_data_angariacao","param_destaque_viva",
                "param_destaque_imovelweb","param_destaque_zap",
                "param_destaque_zap_super","param_destaque_enkontra",
                "param_nao_imovelweb","param_nao_zap","param_url_youtube",
                /* FOTOS */
                "param_fotos",
                /* INFORMACOES COMPLEMENTARES - MAIS INFORMACOES */
                "param_id_tipo_ocupacao","param_vago_em","param_edificio",
                "param_id_tipo_andar","param_idade_imovel",
                "param_id_tipo_face_apartamento","param_condominio",
                "param_condominio_fechado","param_valor_condominio",
                "param_cobertura","param_entre_ruas","param_informacoes_adicionais",
                "param_detalhes_negociacao",
                /* INFORMACOES COMPLEMENTARES - ESPECIFICACOES */
                "param_qtd_area_servico","param_qtd_banheiros","param_qtd_bwc_empregada",
                "param_qtd_churrasqueiras","param_qtd_closet","param_qtd_copa",
                "param_qtd_cozinhas","param_qtd_dep_empregada","param_qtd_lareira",
                "param_qtd_lavabos","param_qtd_quartos","param_qtd_sacadas",
                "param_qtd_sala_biblioteca","param_qtd_sala_escritorio","param_qtd_sala_jantar",
                "param_qtd_sala_estar_intima","param_qtd_salas","param_qtd_suites",
                "param_qtd_vagas_estacionamento","param_qtd_demi_suite",
                "param_tem_adega","param_tem_alarme","param_tem_aquecimento",
                "param_tem_atico","param_tem_aquecimento_solar","param_tem_aquecimento_eletrico",
                "param_tem_bicicletario","param_tem_brinquedoteca","param_tem_cancha_esportes",
                "param_tem_canil","param_tem_central_gas","param_tem_cerca_eletrica",
                "param_tem_churrasqueira_coletiva","param_tem_churrasqueira_privada",
                "param_tem_cinema","param_tem_cozinha_armarios","param_tem_deposito",
                "param_tem_despensa","param_tem_dormitorios_armarios",
                "param_tem_fitness","param_tem_garden","param_tem_grades",
                "param_tem_hidro","param_tem_interfone","param_tem_jardim_inverno",
                "param_mobiliado","param_tem_piscina","param_tem_piscina_infantil",
                "param_tem_pista_caminhada","param_tem_playground",
                "param_tem_portao_eletronico","param_tem_portaria_24h",
                "param_tem_quiosque","param_tem_salao_festas","param_tem_salao_jogos",
                "param_tem_sauna","param_tem_sistema_ar","param_tem_sistema_cftv",
                "param_tem_sistema_incendio","param_tem_sistema_seguranca",
                "param_tem_spa","param_tem_espaco_gourmet","param_tem_edicula",
                "param_tem_quintal","param_tem_jardim","param_area_privada",
                "param_conservacao","param_area_total","param_revestimento_externo",
                "param_area_averbada","param_esquadrias","param_construtora",
                "param_qtd_vagas_garagem","param_id_tipo_garagem",
                "param_id_tipo_local_garagem","param_horario_visitas",
                "param_id_tipo_face_imovel","param_numero_pavimentos",
                "param_numero_elevadores","param_aptos_andar",
                "param_id_tipo_situacao_documental","param_id_tipo_financiamento",
                "param_tipo_piso","param_area_privada_terreno",
                "param_servicos_opcionais",
                /* INFORMACOES COMPLEMENTARES - INFRAESTRUTURA */
                "param_id_tipo_zoneamento","param_id_tipo_face_terreno",
                "param_area_terreno","param_pavimentacao","param_pe_direito",
                "param_medidas_terreno","param_lote","param_quadra","param_planta",
                "param_indicacao_fiscal","param_topografia","param_registro_imovel",
                "param_circunscricao","param_id_tipo_material",
                "param_tem_agua","param_tem_esgoto","param_tem_luz",
                "param_tem_telefone","param_tem_placa_local","param_tem_forro",
                "param_tem_telhado",
                /* INFORMACOES COMPLEMENTARES - VALORES DE VENDA */
                "param_valor_financiamento","param_valor_poupanca",
                "param_agente_financeiro","param_valor_prestacao","param_saldo_devedor",
                "param_prazo","param_numero_prestacoes","param_numero_prestacoes_pagas",
                /* INFORMACOES COMPLEMENTARES - VALORES DE LOCACAO */
                "param_valor_bruto","param_bonificacao","param_valor_iptu",
                "param_outros_valores","param_prazo_contrato","param_reajuste",
                "param_indice_reajuste","param_seguro_incendio","param_seguro_fianca",
                "param_fci",
                /* INFORMACOES COMPLEMENTARES - CHAVES */
                "param_descricao_chave",
                /* MATERIAIS DE APOIO */
                "param_materiais",
                /* DADOS GERENCIAIS */
                "param_id_corretor_angariador", 
                "param_proprietario", 
                "param_xml_atualizavel"
                ), $this->input->post()
            );

            $id_inserido = $this->_inserir_imovel($dados);

            if ($id_inserido) {
                $this->session->set_flashdata('param_msg', "Seu imóvel de ID $id_inserido foi adicionado com sucesso!");
            } else {
                $this->session->set_flashdata('param_msg_erro', "Ocorreu um erro ao tentar inserir um imóvel.");
            }
            redirect('adm/imoveis');
        }
    }

    /*
     * EDITAR_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function editar_imovel() {
        if (!$this->_validar_formulario_imovel()) {
            $this->carregar_editar_imovel();
        } else {
            $dados = elements( array(
                /* NEGOCIACAO */
                "param_id_tipo_negociacao","param_id_loja",
                "param_id_subtipo_imovel",
                /*"param_id_tipo_imovel",*/
                /* INFORMACOES BASICAS */
                "param_titulo","param_id_cidade","param_referencia","param_cep",
                "param_endereco_logradouro","param_endereco_numero",
                "param_endereco_complemento","param_endereco_lat","param_endereco_lng",
                "param_endereco_bairro","param_valor_negociacao","param_id_comissao",
                /* VEICULACAO */
                "param_id_privacidade","param_data_angariacao","param_destaque_viva",
                "param_destaque_imovelweb","param_destaque_zap",
                "param_destaque_zap_super","param_destaque_enkontra",
                "param_nao_imovelweb","param_nao_zap","param_url_youtube",
                /* FOTOS */
                "param_fotos",
                /* INFORMACOES COMPLEMENTARES - MAIS INFORMACOES */
                "param_id_tipo_ocupacao","param_vago_em","param_edificio",
                "param_id_tipo_andar","param_idade_imovel",
                "param_id_tipo_face_apartamento","param_condominio",
                "param_condominio_fechado","param_valor_condominio",
                "param_cobertura","param_entre_ruas","param_informacoes_adicionais",
                "param_detalhes_negociacao",
                /* INFORMACOES COMPLEMENTARES - ESPECIFICACOES */
                "param_qtd_area_servico","param_qtd_banheiros","param_qtd_bwc_empregada",
                "param_qtd_churrasqueiras","param_qtd_closet","param_qtd_copa",
                "param_qtd_cozinhas","param_qtd_dep_empregada","param_qtd_lareira",
                "param_qtd_lavabos","param_qtd_quartos","param_qtd_sacadas",
                "param_qtd_sala_biblioteca","param_qtd_sala_escritorio","param_qtd_sala_jantar",
                "param_qtd_sala_estar_intima","param_qtd_salas","param_qtd_suites",
                "param_qtd_vagas_estacionamento","param_qtd_demi_suite",
                "param_tem_adega","param_tem_alarme","param_tem_aquecimento",
                "param_tem_atico","param_tem_aquecimento_solar","param_tem_aquecimento_eletrico",
                "param_tem_bicicletario","param_tem_brinquedoteca","param_tem_cancha_esportes",
                "param_tem_canil","param_tem_central_gas","param_tem_cerca_eletrica",
                "param_tem_churrasqueira_coletiva","param_tem_churrasqueira_privada",
                "param_tem_cinema","param_tem_cozinha_armarios","param_tem_deposito",
                "param_tem_despensa","param_tem_dormitorios_armarios",
                "param_tem_fitness","param_tem_garden","param_tem_grades",
                "param_tem_hidro","param_tem_interfone","param_tem_jardim_inverno",
                "param_mobiliado","param_tem_piscina","param_tem_piscina_infantil",
                "param_tem_pista_caminhada","param_tem_playground",
                "param_tem_portao_eletronico","param_tem_portaria_24h",
                "param_tem_quiosque","param_tem_salao_festas","param_tem_salao_jogos",
                "param_tem_sauna","param_tem_sistema_ar","param_tem_sistema_cftv",
                "param_tem_sistema_incendio","param_tem_sistema_seguranca",
                "param_tem_spa","param_tem_espaco_gourmet","param_tem_edicula",
                "param_tem_quintal","param_tem_jardim","param_area_privada",
                "param_conservacao","param_area_total","param_revestimento_externo",
                "param_area_averbada","param_esquadrias","param_construtora",
                "param_qtd_vagas_garagem","param_id_tipo_garagem",
                "param_id_tipo_local_garagem","param_horario_visitas",
                "param_id_tipo_face_imovel","param_numero_pavimentos",
                "param_numero_elevadores","param_aptos_andar",
                "param_id_tipo_situacao_documental","param_id_tipo_financiamento",
                "param_tipo_piso","param_area_privada_terreno",
                "param_servicos_opcionais",
                /* INFORMACOES COMPLEMENTARES - INFRAESTRUTURA */
                "param_id_tipo_zoneamento","param_id_tipo_face_terreno",
                "param_area_terreno","param_pavimentacao","param_pe_direito",
                "param_medidas_terreno","param_lote","param_quadra","param_planta",
                "param_indicacao_fiscal","param_topografia","param_registro_imovel",
                "param_circunscricao","param_id_tipo_material",
                "param_tem_agua","param_tem_esgoto","param_tem_luz",
                "param_tem_telefone","param_tem_placa_local","param_tem_forro",
                "param_tem_telhado",
                /* INFORMACOES COMPLEMENTARES - VALORES DE VENDA */
                "param_valor_financiamento","param_valor_poupanca",
                "param_agente_financeiro","param_valor_prestacao","param_saldo_devedor",
                "param_prazo","param_numero_prestacoes","param_numero_prestacoes_pagas",
                /* INFORMACOES COMPLEMENTARES - VALORES DE LOCACAO */
                "param_valor_bruto","param_bonificacao","param_valor_iptu",
                "param_outros_valores","param_prazo_contrato","param_reajuste",
                "param_indice_reajuste","param_seguro_incendio","param_seguro_fianca",
                "param_fci",
                /* INFORMACOES COMPLEMENTARES - CHAVES */
                "param_descricao_chave",
                /* MATERIAIS DE APOIO */
                "param_materiais",
                /* DADOS GERENCIAIS */
                "param_id_corretor_angariador", 
                "param_proprietario", 
                "param_xml_atualizavel"
                ), $this->input->post()
            );

            $id_modificado = $this->_atualizar_imovel($dados);

            if ($id_modificado) {
                $this->session->set_flashdata('param_msg', "Seu imóvel de ID $id_modificado foi atualizado com sucesso!");
            } else {
                $this->session->set_flashdata('param_msg_erro', "Ocorreu um erro ao tentar inserir um imóvel.");
            }
            redirect('adm/imoveis');
        }
    }

    /* ************************************************** */
    /* ******************* VALIDACOES ******************* */
    /* ************************************************** */
    /*
     * _VALIDAR_FORMULARIO_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    private function _validar_formulario_imovel() {
        // VALIDACAO DE NEGOCIACAO
        $this->form_validation->set_rules("param_id_tipo_negociacao", "Negociação", "required");
        $this->form_validation->set_rules("param_id_loja", "Loja", "required");
        $this->form_validation->set_rules("param_id_subtipo_imovel", "Tipo de Imóvel", "required");
        //$this->form_validation->set_rules("param_id_tipo_imovel", "Subtipo do Imóvel", "required");
        // VALIDACAO DE INFORMACOES BASICAS
        $this->form_validation->set_rules("param_titulo", "Título", "required");
        $this->form_validation->set_rules("param_id_cidade", "Cidade", "required");
        $this->form_validation->set_rules("param_valor_negociacao", "Valor da Negociação", "required");
        // VALIDACAO DE VEICULACAO
        $this->form_validation->set_rules("param_data_angariacao", "Angariação", "required|callback_validacao_data");
        // VALIDACAO DE DADOS GERENCIAIS
        $this->form_validation->set_rules("param_id_corretor_angariador", "Angariador", "required");

        if($this->form_validation->run() == FALSE) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function validacao_data($campo) {
        $data = substr($campo, 0, 10);

        if (!validar_data($data)) {
            $this->form_validation->set_message('validacao_data', 'O campo "%s" deve conter uma data válida');
            return FALSE;
        }
        return TRUE;
    }

    /* ****************************************************** */
    /* ********************** PAGINACAO ********************* */
    /* ****************************************************** */
    /*
     * PAGINACAO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function paginacao() {
        $url_paginacao = "adm/imoveis/paginacao";
        $lista_imoveis = $this->_preparar_lista_imoveis(
            $this->uri->segment(calcular_uri_paginacao($url_paginacao))
        );
        $dados['paginacao']  = paginar($url_paginacao, 10, 
                                       $this->m_imoveis->quantificar_imoveis(), 
                                       $lista_imoveis
        );

        $cabecalho = array(
            "ID","# Baggio","Tipo de Imóvel", 
            "Referência", "Título", "Descrição",
            "Informações Complementares",
            "Infraestrutura",
            "Especificações");
        $html_sem_resultados = "";
        $html_sem_resultados .= "<div class='row'>";
        $html_sem_resultados .= "<div class='col texto-central'>";
        $html_sem_resultados .= "<h1 class='texto-sem-resultados'>";
        $html_sem_resultados .= "Não há imóveis cadastrados.";
        $html_sem_resultados .= "</h1>";
        $html_sem_resultados .= "</div>";
        $html_sem_resultados .= "</div>";

        $dados['tabela_imoveis'] = tabelar($lista_imoveis, $cabecalho, "table-responsive", "", "", "celula-texto-central", $html_sem_resultados);

        return $dados;
    }

    /* ****************************************************** */
    /* **************** FUNCOES ASSINCRONAS ***************** */
    /* ****************************************************** */
    /*
     * BUSCAR_INFORMACOES_COMPLEMENTARES
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function buscar_informacoes_complementares() {
        $url = "adm/imoveis/informacoes-complementares";
        $id_imovel = $this->uri->segment(calcular_uri_paginacao($url));

        $cabecalho_info_comp = array("Informação", "Valor");

        $lista_info_complementar = $this->_preparar_informacoes_complementares($id_imovel);

        echo tabelar($lista_info_complementar, $cabecalho_info_comp);
    }

    /*
     * BUSCAR_INFRAESTRUTURA
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function buscar_infraestrutura() {
        $url = "adm/imoveis/infraestrutura";
        $id_imovel = $this->uri->segment(calcular_uri_paginacao($url));

        $cabecalho_infraestrutura = array("Informação", "Valor");

        $lista_infraestrutura = $this->_preparar_infraestrutura($id_imovel);

        echo tabelar($lista_infraestrutura, $cabecalho_infraestrutura);
    }

    /*
     * BUSCAR_ESPECIFICACOES
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function buscar_especificacoes() {
        $url = "adm/imoveis/especificacoes";
        $id_imovel = $this->uri->segment(calcular_uri_paginacao($url));

        $cabecalho_especificacoes = array("Informação", "Valor");

        $lista_especificacoes = $this->_preparar_especificacoes($id_imovel);

        echo tabelar($lista_especificacoes, $cabecalho_especificacoes);
    }

    /* ****************************************************** */
    /* ****************** FUNCOES INTERNAS ****************** */
    /* ****************************************************** */
    /*
     * PREPARAR_LISTA_IMOVEIS - PRIVATE
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    private function _preparar_lista_imoveis($inicio=0) {
        $lista_view = array();
        $lista_resumida_imoveis = $this->m_imoveis->listar_resumo_imoveis(10, $inicio);

        if (!empty($lista_resumida_imoveis)) {
            foreach ($lista_resumida_imoveis as $indice => $imovel) {
                $imovel["descricao"] = substr($imovel["descricao"], 0, 50) . (strlen($imovel["descricao"]) > 50 ? "..." : "");
                //$imovel["alugado"] = $this->i18n->t_boolean($imovel["alugado"]);
                //$imovel["vendido"] = $this->i18n->t_boolean($imovel["vendido"]);
                $imovel["id_informacoes_complementares"] = $this->_preparar_modal_imovel_informacoes($imovel['id_imovel']);
                $imovel["id_infraestrutura"] = $this->_preparar_modal_imovel_infraestrutura($imovel['id_imovel']);
                $imovel["id_especificacao"] = $this->_preparar_modal_imovel_especs($imovel['id_imovel']);

                array_push($lista_view, $imovel);
            }
        }
        return $lista_view;
    }

    /*
     * PREPARAR_LISTA_GERENCIAR_IMOVEIS - PRIVATE
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    private function _preparar_lista_gerenciar_imoveis($inicio=0) {
        $lista_view = array();
        $lista_resumida_imoveis = $this->m_imoveis->listar_resumo_imoveis(10, $inicio);

        if (!empty($lista_resumida_imoveis)) {
            foreach ($lista_resumida_imoveis as $indice => $imovel) {
                $imovel["descricao"] = substr($imovel["descricao"], 0, 50) . (strlen($imovel["descricao"]) > 50 ? "..." : "");
                $imovel["editar"] = "<a href='" . base_url('adm/imoveis/editar-imovel/' . $imovel["id_imovel"]) . "'><i class='fas fa-edit' aria-hidden='true'></i></a>";
                $imovel["excluir"] = "<a href='#' data-toggle='confirmation' data-title='Deseja excluir este imóvel?'><i class='fas fa-window-close' aria-hidden='true'></i></a>";

                array_push($lista_view, $imovel);
            }
        }
        return $lista_view;
    }

    /*
     * PREPARAR_MODAL_IMOVEL_INFORMACOES - PRIVATE
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    private function _preparar_modal_imovel_informacoes($id_imovel) {
        $atributos = array(
            "data-toggle" => "modal",
            "data-target" => "#modal-informacoes-complementares",
            "class" => "btn btn-primary btn-sm",
            "title" => $this->i18n->t('title-modal-informacoes-complementares'),
            "role" => "button"
        );
        return anchor(
                base_url("/adm/imoveis/informacoes-complementares/$id_imovel"), 
                "<i class='material-icons'>info</i>", 
                $atributos
        );
    }

    /*
     * PREPARAR_MODAL_IMOVEL_INFRAESTRUTURA - PRIVATE
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    private function _preparar_modal_imovel_infraestrutura($id_imovel) {
        $atributos = array(
            "data-toggle" => "modal",
            "data-target" => "#modal-infraestrutura",
            "class" => "btn btn-primary btn-sm",
            "title" => $this->i18n->t('title-modal-infraestrutura'),
            "role" => "button"
        );
        return anchor(
                base_url("/adm/imoveis/infraestrutura/$id_imovel"), 
                "<i class='material-icons'>info</i>", 
                $atributos
        );
    }

    /*
     * PREPARAR_MODAL_IMOVEL_ESPECS - PRIVATE
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    private function _preparar_modal_imovel_especs($id_imovel) {
        $atributos = array(
            "data-toggle" => "modal",
            "data-target" => "#modal-especificacoes",
            "class" => "btn btn-primary btn-sm",
            "title" => $this->i18n->t('title-modal-especificacoes'),
            "role" => "button"
        );
        return anchor(
                base_url("/adm/imoveis/especificacoes/$id_imovel"), 
                "<i class='material-icons'>info</i>", 
                $atributos
        );
    }

    /*
     * PREPARAR_INFORMACOES_COMPLEMENTARES - PRIVATE
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    private function _preparar_informacoes_complementares($id_imovel) {
        $lista_view = array();
        $array_info_complementar = $this->m_imoveis->buscar_informacoes_complementares($id_imovel);

        if (!empty($array_info_complementar)) {
            $info = $array_info_complementar[0];

            $info["condominio_fechado"] = $this->i18n->t_boolean($info["condominio_fechado"]);
            $info["cobertura"] = $this->i18n->t_boolean($info["cobertura"]);
            $info["lancamento"] = $this->i18n->t_boolean($info["lancamento"]);
            $info["reservado"] = $this->i18n->t_boolean($info["reservado"]);

            foreach ($info as $chave_info => $informacao) {
                if ($chave_info == 'vago_em') {
                    if (isset($informacao)) {
                        $data_formatada = formatar_sql_to_data($informacao);
                        array_push($lista_view, $this->_rotular_campo($chave_info, $data_formatada));
                    } else {
                        array_push($lista_view, $this->_rotular_campo($chave_info, $informacao));
                    }
                } else {
                    array_push($lista_view, $this->_rotular_campo($chave_info, $informacao));
                }
            }
        }
        return $lista_view;
    }

    /*
     * PREPARAR_INFRAESTRUTURA - PRIVATE
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    private function _preparar_infraestrutura($id_imovel) {
        $lista_view = array();
        $array_infra = $this->m_imoveis->buscar_infraestrutura($id_imovel);

        if (!empty($array_infra)) {
            $infra = $array_infra[0];

            $infra["tem_agua"] = $this->i18n->t_boolean($infra["tem_agua"]);
            $infra["tem_esgoto"] = $this->i18n->t_boolean($infra["tem_esgoto"]);
            $infra["tem_luz"] = $this->i18n->t_boolean($infra["tem_luz"]);
            $infra["tem_telefone"] = $this->i18n->t_boolean($infra["tem_telefone"]);
            $infra["tem_placa_local"] = $this->i18n->t_boolean($infra["tem_placa_local"]);
            $infra["tem_forro"] = $this->i18n->t_boolean($infra["tem_forro"]);
            $infra["tem_telhado"] = $this->i18n->t_boolean($infra["tem_telhado"]);

            foreach ($infra as $chave_infra => $infraestrutura) {
                array_push($lista_view, $this->_rotular_campo($chave_infra, $infraestrutura));
            }
        }
        return $lista_view;
    }

    /*
     * PREPARAR_ESPECIFICACOES - PRIVATE
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    private function _preparar_especificacoes($id_imovel) {
        $lista_view = array();
        $array_especificacoes = $this->m_imoveis->buscar_especificacoes($id_imovel);

        if (!empty($array_especificacoes)) {
            $especs = $array_especificacoes[0];

            $especs["tem_adega"] = $this->i18n->t_boolean($especs["tem_adega"]);
            $especs["tem_alarme"] = $this->i18n->t_boolean($especs["tem_alarme"]);
            $especs["tem_aquecimento"] = $this->i18n->t_boolean($especs["tem_aquecimento"]);
            $especs["tem_atico"] = $this->i18n->t_boolean($especs["tem_atico"]);
            $especs["tem_aquecimento_solar"] = $this->i18n->t_boolean($especs["tem_aquecimento_solar"]);
            $especs["tem_aquecimento_eletrico"] = $this->i18n->t_boolean($especs["tem_aquecimento_eletrico"]);
            $especs["tem_bicicletario"] = $this->i18n->t_boolean($especs["tem_bicicletario"]);
            $especs["tem_brinquedoteca"] = $this->i18n->t_boolean($especs["tem_brinquedoteca"]);
            $especs["tem_cancha_esportes"] = $this->i18n->t_boolean($especs["tem_cancha_esportes"]);
            $especs["tem_central_gas"] = $this->i18n->t_boolean($especs["tem_central_gas"]);
            $especs["tem_canil"] = $this->i18n->t_boolean($especs["tem_canil"]);
            $especs["tem_cerca_eletrica"] = $this->i18n->t_boolean($especs["tem_cerca_eletrica"]);
            $especs["tem_churrasqueira_privada"] = $this->i18n->t_boolean($especs["tem_churrasqueira_privada"]);
            $especs["tem_churrasqueira_coletiva"] = $this->i18n->t_boolean($especs["tem_churrasqueira_coletiva"]);
            $especs["tem_cozinha_armarios"] = $this->i18n->t_boolean($especs["tem_cozinha_armarios"]);
            $especs["tem_cinema"] = $this->i18n->t_boolean($especs["tem_cinema"]);
            $especs["tem_sistema_ar"] = $this->i18n->t_boolean($especs["tem_sistema_ar"]);
            $especs["tem_sistema_incendio"] = $this->i18n->t_boolean($especs["tem_sistema_incendio"]);
            $especs["tem_sistema_cftv"] = $this->i18n->t_boolean($especs["tem_sistema_cftv"]);
            $especs["tem_sistema_seguranca"] = $this->i18n->t_boolean($especs["tem_sistema_seguranca"]);
            $especs["tem_deposito"] = $this->i18n->t_boolean($especs["tem_deposito"]);
            $especs["tem_despensa"] = $this->i18n->t_boolean($especs["tem_despensa"]);
            $especs["tem_dormitorio_armarios"] = $this->i18n->t_boolean($especs["tem_dormitorio_armarios"]);
            $especs["tem_fitness"] = $this->i18n->t_boolean($especs["tem_fitness"]);
            $especs["tem_garden"] = $this->i18n->t_boolean($especs["tem_garden"]);
            $especs["tem_grades"] = $this->i18n->t_boolean($especs["tem_grades"]);
            $especs["tem_hidro"] = $this->i18n->t_boolean($especs["tem_hidro"]);
            $especs["tem_interfone"] = $this->i18n->t_boolean($especs["tem_interfone"]);
            $especs["tem_jardim_inverno"] = $this->i18n->t_boolean($especs["tem_jardim_inverno"]);
            $especs["tem_piscina"] = $this->i18n->t_boolean($especs["tem_piscina"]);
            $especs["tem_piscina_infantil"] = $this->i18n->t_boolean($especs["tem_piscina_infantil"]);
            $especs["esta_mobiliado"] = $this->i18n->t_boolean($especs["esta_mobiliado"]);
            $especs["tem_pista_caminhada"] = $this->i18n->t_boolean($especs["tem_pista_caminhada"]);
            $especs["tem_playground"] = $this->i18n->t_boolean($especs["tem_playground"]);
            $especs["tem_portao_eletronico"] = $this->i18n->t_boolean($especs["tem_portao_eletronico"]);
            $especs["tem_portaria_vinte_quatro"] = $this->i18n->t_boolean($especs["tem_portaria_vinte_quatro"]);
            $especs["tem_quiosque"] = $this->i18n->t_boolean($especs["tem_quiosque"]);
            $especs["tem_salao_festas"] = $this->i18n->t_boolean($especs["tem_salao_festas"]);
            $especs["tem_salao_jogos"] = $this->i18n->t_boolean($especs["tem_salao_jogos"]);
            $especs["tem_sauna"] = $this->i18n->t_boolean($especs["tem_sauna"]);
            $especs["tem_spa"] = $this->i18n->t_boolean($especs["tem_spa"]);
            $especs["tem_espaco_gourmet"] = $this->i18n->t_boolean($especs["tem_espaco_gourmet"]);
            $especs["tem_edicula"] = $this->i18n->t_boolean($especs["tem_edicula"]);
            $especs["tem_quintal"] = $this->i18n->t_boolean($especs["tem_quintal"]);
            $especs["tem_jardim"] = $this->i18n->t_boolean($especs["tem_jardim"]);

            foreach ($especs as $chave_especs => $especificacoes) {
                array_push($lista_view, $this->_rotular_campo($chave_especs, $especificacoes));
            }
        }
        return $lista_view;
    }

    /*
     * ROTULAR_CAMPO - PRIVATE
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    private function _rotular_campo($campo, $valor) {
        $linha = array();

        array_push($linha, $this->i18n->t('label-' . str_replace("_", "-", $campo), false, true));
        array_push($linha, (isset($valor) && (is_numeric($valor) || !empty($valor)) ? $valor : "N/A"));

        return $linha;
    }


    /* ****************************************************** */
    /* *************** FUNCOES DE PRE-DATABASE ************** */
    /* ****************************************************** */
    /*
     * _MODELAR_IMOVEL
     * ---------------------------------------
     * DESCRICAO : Retorna um array pronto para
     * ser inserido na tabela de imoveis e suas
     * respectivas dependentes.
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - Array do formulario de imovel
    */
    private function _modelar_imovel($formulario=array()) {
        if (!empty($formulario)) {
            $dados = array();

            $dados["id_imobiliaria"] = 1;
            $dados["id_especificacoes"] = $formulario["id_especificacoes"];
            $dados["id_informacoes_complementares"] = $formulario["id_informacoes_complementares"];
            $dados["id_infraestrutura"] = $formulario["id_infraestrutura"];
            $dados["id_veiculacao"] = $formulario["id_veiculacao"];
            $dados["referencia"] = $formulario["param_referencia"];
            $dados["titulo"] = $formulario["param_titulo"];
            /* ATRIBUTO NAO LISTADO NO FORM */
            $dados["descricao"] = NULL;
            /* ATRIBUTO NAO LISTADO NO FORM */
            $dados["observacao"] = NULL;
            $dados["proprietario"] = $formulario["param_proprietario"];
            $dados["alugado"] = FALSE;
            $dados["vendido"] = FALSE;
            $dados["cadastrado_sistema"] = TRUE;
            $dados["xml_atualizavel"] = ($formulario["param_xml_atualizavel"] != NULL);

            return $dados;
        }
        return array();
    }

    /*
     * _MODELAR_ESPECIFICACOES
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - Array do formulario de imovel
    */
    private function _modelar_especificacoes($formulario=array()) {
        if (!empty($formulario)) {
            $dados = array();

            $dados["id_situacao_documental"] = $formulario["param_id_tipo_situacao_documental"];
            $dados["id_tipo_financiamento"] = $formulario["param_id_tipo_financiamento"];
            $dados["id_tipo_garagem"] = $formulario["param_id_tipo_garagem"];
            $dados["id_local_garagem"] = $formulario["param_id_tipo_local_garagem"];
            $dados["id_face_imovel"] = $formulario["param_id_tipo_face_imovel"];
            $dados["qtd_vagas_estacionamento"] = $formulario["param_qtd_vagas_estacionamento"];
            $dados["qtd_vagas_garagem"] = $formulario["param_qtd_vagas_garagem"];
            $dados["qtd_closets"] = $formulario["param_qtd_closet"];
            $dados["qtd_quartos"] = $formulario["param_qtd_quartos"];
            $dados["qtd_suites"] = $formulario["param_qtd_suites"];
            $dados["qtd_demi_suites"] = $formulario["param_qtd_demi_suite"];
            $dados["qtd_churrasqueiras"] = $formulario["param_qtd_churrasqueiras"];
            $dados["qtd_copas"] = $formulario["param_qtd_copa"];
            /* ATRIBUTO NAO LISTADO NO FORM */
            $dados["qtd_despensas"] = NULL;
            $dados["qtd_cozinhas"] = $formulario["param_qtd_cozinhas"];
            $dados["qtd_lareiras"] = $formulario["param_qtd_lareira"];
            $dados["qtd_sacadas"] = $formulario["param_qtd_sacadas"];
            $dados["qtd_salas"] = $formulario["param_qtd_salas"];
            $dados["qtd_salas_jantar"] = $formulario["param_qtd_sala_jantar"];
            /* ATRIBUTO NAO LISTADO NO FORM */
            $dados["qtd_salas_estar"] = NULL;
            $dados["qtd_salas_estar_intima"] = $formulario["param_qtd_sala_estar_intima"];
            $dados["qtd_salas_escritorio"] = $formulario["param_qtd_sala_escritorio"];
            $dados["qtd_salas_biblioteca"] = $formulario["param_qtd_sala_biblioteca"];
            $dados["qtd_lavabos"] = $formulario["param_qtd_lavabos"];
            $dados["qtd_banheiros"] = $formulario["param_qtd_banheiros"];
            $dados["qtd_areas_servico"] = $formulario["param_qtd_area_servico"];
            $dados["qtd_dependencias_empregada"] = $formulario["param_qtd_dep_empregada"];
            $dados["qtd_bwc_empregada"] = $formulario["param_qtd_bwc_empregada"];
            
            $dados["tem_adega"] = ( isset($formulario["param_tem_adega"]) ? TRUE : FALSE );
            $dados["tem_alarme"] = ( isset($formulario["param_tem_alarme"]) ? TRUE : FALSE );
            $dados["tem_aquecimento"] = ( isset($formulario["param_tem_aquecimento"]) ? TRUE : FALSE );
            $dados["tem_atico"] = ( isset($formulario["param_tem_atico"]) ? TRUE : FALSE );
            $dados["tem_aquecimento_solar"] = ( isset($formulario["param_tem_aquecimento_solar"]) ? TRUE : FALSE );
            $dados["tem_aquecimento_eletrico"] = ( isset($formulario["param_tem_aquecimento_eletrico"]) ? TRUE : FALSE );
            $dados["tem_bicicletario"] = ( isset($formulario["param_tem_bicicletario"]) ? TRUE : FALSE );
            $dados["tem_brinquedoteca"] = ( isset($formulario["param_tem_brinquedoteca"]) ? TRUE : FALSE );
            $dados["tem_cancha_esportes"] = ( isset($formulario["param_tem_cancha_esportes"]) ? TRUE : FALSE );
            $dados["tem_canil"] = ( isset($formulario["param_tem_canil"]) ? TRUE : FALSE );
            $dados["tem_central_gas"] = ( isset($formulario["param_tem_central_gas"]) ? TRUE : FALSE );
            $dados["tem_cerca_eletrica"] = ( isset($formulario["param_tem_cerca_eletrica"]) ? TRUE : FALSE );
            $dados["tem_churrasqueira_privada"] = ( isset($formulario["param_tem_churrasqueira_privada"]) ? TRUE : FALSE );
            $dados["tem_churrasqueira_coletiva"] = ( isset($formulario["param_tem_churrasqueira_coletiva"]) ? TRUE : FALSE );
            $dados["tem_cinema"] = ( isset($formulario["param_tem_cinema"]) ? TRUE : FALSE );
            $dados["tem_cozinha_armarios"] = ( isset($formulario["param_tem_cozinha_armarios"]) ? TRUE : FALSE );
            $dados["tem_sistema_ar"] = ( isset($formulario["param_tem_sistema_ar"]) ? TRUE : FALSE );
            $dados["tem_sistema_incendio"] = ( isset($formulario["param_tem_sistema_incendio"]) ? TRUE : FALSE );
            $dados["tem_sistema_cftv"] = ( isset($formulario["param_tem_sistema_cftv"]) ? TRUE : FALSE );
            $dados["tem_sistema_seguranca"] = ( isset($formulario["param_tem_sistema_seguranca"]) ? TRUE : FALSE );
            $dados["tem_deposito"] = ( isset($formulario["param_tem_deposito"]) ? TRUE : FALSE );
            $dados["tem_despensa"] = ( isset($formulario["param_tem_despensa"]) ? TRUE : FALSE );
            $dados["tem_dormitorio_armarios"] = ( isset($formulario["param_tem_dormitorios_armarios"]) ? TRUE : FALSE );
            $dados["tem_fitness"] = ( isset($formulario["param_tem_fitness"]) ? TRUE : FALSE );
            $dados["tem_garden"] = ( isset($formulario["param_tem_garden"]) ? TRUE : FALSE );
            $dados["tem_grades"] = ( isset($formulario["param_tem_grades"]) ? TRUE : FALSE );
            $dados["tem_hidro"] = ( isset($formulario["param_tem_hidro"]) ? TRUE : FALSE );
            $dados["tem_interfone"] = ( isset($formulario["param_tem_interfone"]) ? TRUE : FALSE );
            $dados["tem_jardim_inverno"] = ( isset($formulario["param_tem_jardim_inverno"]) ? TRUE : FALSE );
            $dados["tem_piscina"] = ( isset($formulario["param_tem_piscina"]) ? TRUE : FALSE );
            $dados["tem_piscina_infantil"] = ( isset($formulario["param_tem_piscina_infantil"]) ? TRUE : FALSE );
            $dados["esta_mobiliado"] = ( isset($formulario["param_mobiliado"]) ? TRUE : FALSE );
            $dados["tem_pista_caminhada"] = ( isset($formulario["param_tem_pista_caminhada"]) ? TRUE : FALSE );
            $dados["tem_playground"] = ( isset($formulario["param_tem_playground"]) ? TRUE : FALSE );
            $dados["tem_portao_eletronico"] = ( isset($formulario["param_tem_portao_eletronico"]) ? TRUE : FALSE );
            $dados["tem_portaria_vinte_quatro"] = ( isset($formulario["param_tem_portaria_24h"]) ? TRUE : FALSE );
            $dados["tem_quiosque"] = ( isset($formulario["param_tem_quiosque"]) ? TRUE : FALSE );
            $dados["tem_salao_festas"] = ( isset($formulario["param_tem_salao_festas"]) ? TRUE : FALSE );
            $dados["tem_salao_jogos"] = ( isset($formulario["param_tem_salao_jogos"]) ? TRUE : FALSE );
            $dados["tem_sauna"] = ( isset($formulario["param_tem_sauna"]) ? TRUE : FALSE );
            $dados["tem_spa"] = ( isset($formulario["param_tem_spa"]) ? TRUE : FALSE );
            $dados["tem_espaco_gourmet"] = ( isset($formulario["param_tem_espaco_gourmet"]) ? TRUE : FALSE );
            $dados["tem_edicula"] = ( isset($formulario["param_tem_edicula"]) ? TRUE : FALSE );
            $dados["tem_quintal"] = ( isset($formulario["param_tem_quintal"]) ? TRUE : FALSE );
            $dados["tem_jardim"] = ( isset($formulario["param_tem_jardim"]) ? TRUE : FALSE );

            $dados["conservacao"] = $formulario["param_conservacao"];
            $dados["area_privada"] = $formulario["param_area_privada"];
            $dados["area_total"] = $formulario["param_area_total"];
            $dados["area_averbada"] = $formulario["param_area_averbada"];
            $dados["area_privada_terreno"] = $formulario["param_area_privada_terreno"];
            $dados["esquadrias"] = $formulario["param_esquadrias"];
            $dados["revestimento_externo"] = $formulario["param_revestimento_externo"];
            $dados["construtora"] = $formulario["param_construtora"];
            $dados["horarios_visita"] = $formulario["param_horario_visitas"];
            $dados["qtd_elevadores"] = $formulario["param_numero_elevadores"];
            $dados["qtd_apartamentos_andar"] = $formulario["param_aptos_andar"];
            $dados["numero_pavimentos"] = $formulario["param_numero_pavimentos"];
            $dados["tipo_piso"] = $formulario["param_tipo_piso"];
            /* ATRIBUTO NAO LISTADO NO FORM */
            $dados["condicoes_negocio"] = NULL;
            /* ATRIBUTO NAO LISTADO NO FORM */
            $dados["status_imovel"] = NULL;
            $dados["servicos_opcionais"] = $formulario["param_servicos_opcionais"];

            return $dados;
        }
        return array();
    }

    /*
     * _MODELAR_INFORMACOES_COMPLEMENTARES
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - Array do formulario de imovel
    */
    private function _modelar_informacoes_complementares($formulario=array()) {
        if (!empty($formulario)) {
            $dados = array();

            /* ATRIBUTO NAO LISTADO NO FORM */
            $dados["id_tipo_acao"] = 1;
            $dados["id_tipo_ocupacao"] = (!empty($formulario["param_id_tipo_ocupacao"]) ? $formulario["param_id_tipo_ocupacao"] : 1);
            /* ATRIBUTO NAO LISTADO NO FORM */
            $dados["id_tipo_finalidade"] = 1;
            $dados["id_face_apartamento"] = (!empty($formulario["param_id_tipo_face_apartamento"]) ? $formulario["param_id_tipo_face_apartamento"] : 1);
            $dados["id_tipo_andar"] = (!empty($formulario["param_id_tipo_andar"]) ? $formulario["param_id_tipo_andar"] : 1);
            $dados["idade_imovel"] = $formulario["param_idade_imovel"];
            $dados["condominio_fechado"] = ( isset($formulario["param_condominio_fechado"]) ? TRUE : FALSE );
            $dados["cobertura"] = ( isset($formulario["param_cobertura"]) ? TRUE : FALSE );
            $dados["informacoes_adicionais"] = $formulario["param_informacoes_adicionais"];
            /* ATRIBUTO NAO LISTADO NO FORM */
            $dados["lancamento"] = FALSE;
            /* ATRIBUTO NAO LISTADO NO FORM */
            $dados["uso_tipo"] = NULL;
            /* ATRIBUTO NAO LISTADO NO FORM */
            $dados["reservado"] = FALSE;
            /* ATRIBUTO NAO LISTADO NO FORM */
            $dados["andar_apartamento"] = 0;
            /* ATRIBUTO NAO LISTADO NO FORM */
            $dados["situacao"] = NULL;
            $dados["vago_em"] = formatar_data_sql($formulario["param_vago_em"]);
            /* ATRIBUTO NAO LISTADO NO FORM */
            $dados["acab_benfeito"] = NULL;

            return $dados;
        }
        return array();
    }

    /*
     * _MODELAR_INFRAESTRUTURA
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - Array do formulario de imovel
    */
    private function _modelar_infraestrutura($formulario=array()) {
        if (!empty($formulario)) {
            $dados = array();

            $dados["id_tipo_zoneamento"] = (!empty($formulario["param_id_tipo_zoneamento"]) ? $formulario["param_id_tipo_zoneamento"] : 1);
            $dados["id_face_terreno"] = $formulario["param_id_tipo_face_terreno"];
            $dados["id_tipo_material"] = $formulario["param_id_tipo_material"];
            $dados["area_terreno"] = $formulario["param_area_terreno"];
            $dados["medidas_terreno"] = $formulario["param_medidas_terreno"];
            $dados["pavimentacao"] = $formulario["param_pavimentacao"];
            $dados["pe_direito"] = $formulario["param_pe_direito"];
            $dados["lote"] = $formulario["param_lote"];
            $dados["quadra"] = $formulario["param_quadra"];
            $dados["planta"] = $formulario["param_planta"];
            $dados["indicacao_fiscal"] = $formulario["param_indicacao_fiscal"];
            $dados["topografia"] = $formulario["param_topografia"];
            $dados["registro_imovel"] = $formulario["param_registro_imovel"];
            $dados["circunscricao"] = $formulario["param_circunscricao"];
            $dados["tem_agua"] = ( isset($formulario["param_tem_agua"]) ? TRUE : FALSE );
            $dados["tem_esgoto"] = ( isset($formulario["param_tem_esgoto"]) ? TRUE : FALSE );
            $dados["tem_luz"] = ( isset($formulario["param_tem_luz"]) ? TRUE : FALSE );
            $dados["tem_telefone"] = ( isset($formulario["param_tem_telefone"]) ? TRUE : FALSE );
            $dados["tem_placa_local"] = ( isset($formulario["param_tem_placa_local"]) ? TRUE : FALSE );
            $dados["tem_forro"] = ( isset($formulario["param_tem_forro"]) ? TRUE : FALSE );
            $dados["tem_telhado"] = ( isset($formulario["param_tem_telhado"]) ? TRUE : FALSE );

            return $dados;
        }
        return array();
    }

    /*
     * _MODELAR_VEICULACAO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - Array do formulario de imovel
    */
    private function _modelar_veiculacao($formulario=array()) {
        if (!empty($formulario)) {
            $dados = array();

            $dados["id_corretor"] = $formulario["param_id_corretor_angariador"];
            $dados["id_privacidade"] = $formulario["param_id_privacidade"];
            $dados["data_angariacao"] = formatar_data_sql( substr($formulario["param_data_angariacao"], 0, 10) );
            $dados["viva_real"] = ( isset($formulario["param_destaque_viva"]) ? TRUE : FALSE );
            $dados["imovel_web"] = ( isset($formulario["param_destaque_imovelweb"]) ? TRUE : FALSE );
            $dados["zap"] = ( isset($formulario["param_destaque_zap"]) ? TRUE : FALSE );
            $dados["zap_super"] = ( isset($formulario["param_destaque_zap_super"]) ? TRUE : FALSE );
            $dados["enkontra"] = ( isset($formulario["param_destaque_enkontra"]) ? TRUE : FALSE );
            $dados["nao_imovel_web"] = ( isset($formulario["param_nao_imovelweb"]) ? TRUE : FALSE );
            $dados["nao_zap"] = ( isset($formulario["param_nao_zap"]) ? TRUE : FALSE );
            $dados["video_youtube"] = $formulario["param_url_youtube"];

            return $dados;
        }
        return array();
    }

    /*
     * _MODELAR_SUBTIPO_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - Array do formulario de imovel
    */
    private function _modelar_subtipo_imovel($formulario=array()) {
        if (!empty($formulario)) {
            $dados = array();

            $dados["id_imovel"] = $formulario["id_imovel"];
            $dados["id_sub_tipo"] = $formulario["param_id_subtipo_imovel"];
            $dados["id_tipo"] = $this->m_imoveis->buscar_id_tipo_imovel($formulario["param_id_subtipo_imovel"]);

            return $dados;
        }
        return array();
    }

    /*
     * _MODELAR_BAIRRO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - Array do formulario de imovel
    */
    private function _modelar_bairro($formulario=array()) {
        if (!empty($formulario) && !empty($formulario["param_endereco_bairro"])) {
            $dados = array();

            $dados["nome_bairro"] = $formulario["param_endereco_bairro"];

            return $dados;
        }
        return array();
    }

    /*
     * _MODELAR_COMPLEMENTO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - Array do formulario de imovel
    */
    private function _modelar_complemento($formulario=array()) {
        if (!empty($formulario) && !empty($formulario["param_endereco_complemento"])) {
            $dados = array();

            $dados["descricao"] = $formulario["param_endereco_complemento"];
            $dados["bloco"] = NULL;
            $dados["sala"] = NULL;
            $dados["nome_edificio"] = $formulario["param_edificio"];
            $dados["nome_condominio"] = $formulario["param_condominio"];
            $dados["imediacoes"] = NULL;
            $dados["entre_ruas"] = $formulario["param_entre_ruas"];

            return $dados;
        }
        return array();
    }


    /*
     * _MODELAR_LAT_LONG
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - Array do formulario de imovel
    */
    private function _modelar_lat_long($formulario=array()) {
        if (!empty($formulario) && !empty($formulario["param_endereco_lat"]) && !empty($formulario["param_endereco_lng"])) {
            $dados = array();

            $dados["latitude"] = $formulario["param_endereco_lat"];
            $dados["longitude"] = $formulario["param_endereco_lng"];

            return $dados;
        }
        return array();
    }

    /*
     * _MODELAR_ENDERECO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - Array do formulario de imovel
    */
    private function _modelar_endereco($formulario=array()) {
        if (!empty($formulario)
            &&  $formulario["id_imovel"] != NULL
            &&  $formulario["param_id_cidade"] != NULL) 
        {
            $dados = array();

            $dados["id_imovel"] = $formulario["id_imovel"];
            $dados["id_cidade"] = $formulario["param_id_cidade"];
            $dados["id_bairro"] = $formulario["id_bairro"];
            $dados["id_complemento"] = $formulario["id_complemento"];
            $dados["id_lat_long"] = $formulario["id_lat_long"];
            $dados["cep"] = str_replace("-", "", $formulario["param_cep"]);
            $dados["logradouro"] = $formulario["param_endereco_logradouro"];
            $dados["numero"] = $formulario["param_endereco_numero"];

            return $dados;
        }
        return array();
    }

    /*
     * _INSERIR_FOTOS
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - 
    */
    private function _inserir_fotos($formulario=array()) {
        if (!empty($formulario)) {
            $imagens = array();
            $tamanhos_fotos = $_FILES["param_fotos"]["size"];

            foreach ($tamanhos_fotos as $indice => $tamanho_foto) {
                if ($tamanho_foto > 0) {
                    $imagem = array();
                    $indice_criptografado = md5(strval($indice));
                    $id_criptografado = md5($formulario["id_imovel"]);
                    $foto_tipo = $_FILES["param_fotos"]["type"][$indice];
                    $foto_extensao = explode("/", $foto_tipo)[1];
                    $foto_temp = $_FILES["param_fotos"]["tmp_name"][$indice];
                    $foto_erro = $_FILES["param_fotos"]["error"][$indice];
                    $foto_tamanho = $_FILES["param_fotos"]["size"][$indice];
                    $foto_nome = md5($_FILES["param_fotos"]["name"][$indice]) . ".$foto_extensao";
                    $caminho_imagem = CONS_BAGGIO_FOTOS_CAMINHO . $id_criptografado . DIRECTORY_SEPARATOR . $indice_criptografado;

                    $imagem["id_imovel"] = $formulario["id_imovel"];
                    $imagem["caminho_imagem"] = $caminho_imagem . DIRECTORY_SEPARATOR . $foto_nome;
                    $imagem["caminho_miniatura"] = NULL;
                    $imagem["link_imagem"] = base_url(str_replace(DIRECTORY_SEPARATOR, "/", $caminho_imagem) . "/" . $foto_nome);
                    $imagem["texto_imagem"] = $foto_nome;
                    $imagem["tamanho_largura"] = 0;
                    $imagem["tamanho_altura"] = 0;
                    $imagem["principal"] = $indice == 0;
                    $imagem["ordem"] = $indice;

                    if ( upload_foto_imovel($foto_nome, $foto_tipo, $foto_temp, $foto_erro, $foto_tamanho, $caminho_imagem) ) {
                        array_push($imagens, $imagem);
                    }
                }
            }
            $this->m_imagens->inserir_imagens($imagens);
        }
    }

    /*
     * _MODELAR_VALORES
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - Array do formulario de imovel
    */
    private function _modelar_valores($formulario=array()) {
        if (!empty($formulario) && !empty($formulario["param_valor_negociacao"])) {
            $dados = array();

            $dados["id_imovel"] = $formulario["id_imovel"];
            $dados["preco_venda"] = NULL;
            $dados["preco_locacao"] = NULL;
            $dados["valor_condominio"] = formatar_float_sql($formulario["param_valor_condominio"]);
            $dados["valor_aluguel"] = NULL;
            $dados["valor_liquido"] = NULL;
            $dados["valor_negociacao"] = formatar_float_sql($formulario["param_valor_negociacao"]);
            $dados["valor_financiamento"] = formatar_float_sql($formulario["param_valor_financiamento"]);
            $dados["valor_poupanca"] = formatar_float_sql($formulario["param_valor_poupanca"]);
            $dados["valor_prestacao"] = formatar_float_sql($formulario["param_valor_prestacao"]);
            $dados["agente_financeiro"] = $formulario["param_agente_financeiro"];
            $dados["saldo_devedor"] = $formulario["param_saldo_devedor"];
            $dados["prazo"] = $formulario["param_prazo"];
            $dados["numero_prestacoes_pagas"] = $formulario["param_numero_prestacoes_pagas"];
            $dados["numero_prestacoes"] = $formulario["param_numero_prestacoes"];
            $dados["valor_bruto"] = formatar_float_sql($formulario["param_valor_bruto"]);
            $dados["bonificacao"] = $formulario["param_bonificacao"];
            $dados["valor_iptu"] = formatar_float_sql($formulario["param_valor_iptu"]);
            $dados["outros_valores"] = $formulario["param_outros_valores"];
            $dados["prazo_contrato"] = $formulario["param_prazo_contrato"];
            $dados["reajuste"] = $formulario["param_reajuste"];
            $dados["indice_reajuste"] = $formulario["param_indice_reajuste"];
            $dados["valor_seguro_incendio"] = $formulario["param_seguro_incendio"];
            $dados["fci"] = $formulario["param_fci"];
            $dados["valor_seguro_fianca"] = $formulario["param_seguro_fianca"];

            return $dados;
        }
        return array();
    }

    /*
     * _INSERIR_CHAVES
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - 
    */
    private function _inserir_chaves($formulario=array()) {
        if (!empty($formulario)) {
            $chaves = array();

            foreach ($formulario["param_descricao_chave"] as $indice => $param_chave) {
                if (!empty($param_chave)) {
                    $chave = array();

                    $chave["id_imovel"] = $formulario["id_imovel"];
                    $chave["descricao"] = $param_chave;

                    array_push($chaves, $chave);
                }
            }
            $this->m_imoveis->inserir_chaves($chaves);
        }
    }

    /*
     * _MODELAR_NEGOCIACAO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - Array do formulario de imovel
    */
    private function _modelar_negociacao($formulario=array()) {
        if (!empty($formulario)) {
            $dados = array();

            $dados["id_imovel"] = $formulario["id_imovel"];
            $dados["id_corretor"] = $formulario["param_id_corretor_angariador"];
            $dados["id_proprietario"] = NULL;
            $dados["id_cliente"] = NULL;
            $dados["id_tipo_negociacao"] = $formulario["param_id_tipo_negociacao"];
            $dados["id_loja"] = $formulario["param_id_loja"];
            $dados["id_valores_imovel"] = $formulario["id_valores_imovel"];
            $dados["id_comissao"] = (empty($formulario["param_id_comissao"]) ? NULL : $formulario["param_id_comissao"]);
            $dados["detalhes_negociacao"] = $formulario["param_detalhes_negociacao"];

            return $dados;
        }
        return array();
    }

    /*
     * _INSERIR_MATERIAIS
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - 
    */
    private function _inserir_materiais($formulario=array()) {
        if (!empty($formulario)) {
            $materiais_apoio = array();

            /*foreach ($formulario["param_materiais"] as $indice => $param_material) {
                $material_apoio = array();

                $material_apoio["id_negociacao"] = $formulario["id_negociacao"];
                $material_apoio["descricao"] = $param_material;
                $material_apoio["caminho_arquivo"] = "publico/uploads/materiais-apoio/$param_material";

                array_push($materiais_apoio, $material_apoio);
            }
            $this->m_materiais->inserir_materiais($materiais_apoio);*/
        }
    }

    /*
     * _INSERIR_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - Array do formulario de imovel
    */
    private function _inserir_imovel($dados=array()) {
        if (!empty($dados)) {
            $id_imovel = NULL;

            $this->load->model('EnderecoModel', 'm_enderecos');

            $this->db->trans_start();

            /* INSERCOES ANTES A DE IMOVEL */
            $dados["id_especificacoes"] = $this->m_imoveis->inserir_especificacoes($this->_modelar_especificacoes($dados));
            $dados["id_informacoes_complementares"] = $this->m_imoveis->inserir_informacoes_complementares($this->_modelar_informacoes_complementares($dados));
            $dados["id_infraestrutura"] = $this->m_imoveis->inserir_infraestrutura($this->_modelar_infraestrutura($dados));
            $dados["id_veiculacao"] = $this->m_imoveis->inserir_veiculacao($this->_modelar_veiculacao($dados));

            $id_imovel = $this->m_imoveis->inserir_imovel($this->_modelar_imovel($dados));

            /* INSERCOES APOS A DE IMOVEL */
            $dados["id_imovel"] = $id_imovel;

            $this->m_imoveis->inserir_imovel_sub_tipo($this->_modelar_subtipo_imovel($dados));
            
            $id_bairro = $this->m_enderecos->buscar_id_bairro($dados["param_endereco_bairro"]);
            if ($id_bairro == NULL) {
                $id_bairro = $this->m_enderecos->inserir_bairro($this->_modelar_bairro($dados));
            }
            $dados["id_bairro"] = $id_bairro;

            $dados["id_complemento"] = $this->m_enderecos->inserir_complemento($this->_modelar_complemento($dados));
            $dados["id_lat_long"] = $this->m_enderecos->inserir_lat_long($this->_modelar_lat_long($dados));

            $this->db->insert("tb_enderecos", $this->_modelar_endereco($dados));

            $files_foto_size = $_FILES["param_fotos"]["size"];

            if ($files_foto_size[0] > 0) {
                $this->_inserir_fotos($dados);
            }

            $dados["id_valores_imovel"] = $this->m_negociacoes->inserir_valores($this->_modelar_valores($dados));

            if (count($dados["param_descricao_chave"]) > 0) {
                $this->_inserir_chaves($dados);
            }

            $dados["id_negociacao"] = $this->m_negociacoes->inserir_negociacao($this->_modelar_negociacao($dados));

            if (count($dados["param_materiais"]) > 0) {
                $this->_inserir_materiais($dados);
            }
            
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }
            return $id_imovel;
        }
    }

    /*
     * _ATUALIZAR_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - Array do formulario de imovel
    */
    private function _atualizar_imovel($dados=array()) {
        if (!empty($dados)) {
            //to_debug($dados);
            
            $url_paginacao = "adm/imoveis/atualizar-imovel";
            $id_imovel = $this->uri->segment(calcular_uri_paginacao($url_paginacao));

            if ($id_imovel != NULL) {
                $dados['id_imovel'] = $id_imovel;

                $this->load->model('EnderecoModel', 'm_enderecos');

                $this->db->trans_start();

                $dados['id_informacoes_complementares'] = $this->m_imoveis->atualizar_informacoes_complementares($id_imovel, $this->_modelar_informacoes_complementares($dados));
                $dados['id_especificacoes'] = $this->m_imoveis->atualizar_especificacoes($id_imovel, $this->_modelar_especificacoes($dados));
                $dados['id_infraestrutura'] = $this->m_imoveis->atualizar_infraestrutura($id_imovel, $this->_modelar_infraestrutura($dados));
                $dados['id_veiculacao'] = $this->m_imoveis->atualizar_veiculacao($id_imovel, $this->_modelar_veiculacao($dados));

                $atualizou_imovel = $this->m_imoveis->atualizar_imovel($id_imovel, $this->_modelar_imovel($dados));

                $atualizou_tipo_imovel = $this->m_imoveis->atualizar_imovel_sub_tipo($id_imovel, $this->_modelar_subtipo_imovel($dados));

                // $id_bairro = $this->m_enderecos->buscar_id_bairro($dados["param_endereco_bairro"]);
                // if ($id_bairro == NULL) {
                //     $id_bairro = $this->m_enderecos->inserir_bairro($this->_modelar_bairro($dados));
                // }
                // $dados["id_bairro"] = $id_bairro;
                
                // $dados["id_complemento"] = $this->m_enderecos->inserir_complemento($this->_modelar_complemento($dados));
                // $dados["id_lat_long"] = $this->m_enderecos->inserir_lat_long($this->_modelar_lat_long($dados));

                // $this->db->insert("tb_enderecos", $this->_modelar_endereco($dados));

                // $files_foto_size = $_FILES["param_fotos"]["size"];

                // if ($files_foto_size[0] > 0) {
                //     $this->_inserir_fotos($dados);
                // }

                // $dados["id_valores_imovel"] = $this->m_negociacoes->inserir_valores($this->_modelar_valores($dados));

                // if (count($dados["param_descricao_chave"]) > 0) {
                //     $this->_inserir_chaves($dados);
                // }

                // $dados["id_negociacao"] = $this->m_negociacoes->inserir_negociacao($this->_modelar_negociacao($dados));

                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                } else if ($atualizou_imovel
                        && $atualizou_tipo_imovel) 
                {
                    $this->db->trans_commit();
                }
            }
            return $id_imovel;
        }
    }

}
