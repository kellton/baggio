<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Controller responsável pela operações do perfil do usuario ADM
 *
 * PHP version 7
 *
 * @category PHP
 * @author   Kellton Leitão <kelltonl10@gmail.com>
 */
class UsuarioController extends CI_Controller {

     public function __construct() {
        parent::__construct();
        if(!usuario_sessao_adm() || usuario_sessao_adm()['tipo_usuario']!=1) {
            redirect(base_url());
        }
        $this->load->model('UsuarioModel','usuario');
    }
    
     /**
     * Carrega a lsiatgem de usuarios ADM
     *
     * @param 
     * @return 
     */
    public function index(){
        $dados=$this->_paginacao();
        
        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $this->load->view('adm/menu');
        $this->load->view('adm/usuario/listagem_usuarios',$dados);
        $this->load->view('template/footer');
    }
    
    
    /**
     * Carrega o formulário de adição de um novo usuario ADM
     *
     * @param 
     * @return 
     */
    public function adicionar_usuario() {
        
        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $dados['tipo_usuario']=$this->usuario->listar_tipo_usuario();
        $dados['lojas']=$this->usuario->listar_lojas();
        $this->load->view('adm/menu');
        $this->load->view('adm/usuario/adicionar_usuario',$dados);
        $this->load->view('template/footer');
    }
    
    /**
     * Carrega o formulário de edição de um usuario ADM
     *
     * @param int $id_usuario ID único do usuario do sistema 
     * @return 
     */
    public function editar_usuario($id_usuario) {
        $dados['query']=$this->usuario->buscar_usuario($id_usuario);
         if($dados['query']==NULL){
            redirect('adm/usuarios');
        }
        $dados['tipo_usuario']=$this->usuario->listar_tipo_usuario();
        
        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $this->load->view('adm/menu');
        $this->load->view('adm/usuario/editar_usuario',$dados);
        $this->load->view('template/footer');
    }
    
    /**
     * Carrega o formulário de exclusao de um usuario ADM
     *
     * @param int $id_usuario ID único do usuario do sistema 
     * @return 
     */
    public function excluir_usuario($id_usuario) {
         $dados['query']=$this->usuario->buscar_usuario($id_usuario);
         if($dados['query']==NULL){
            redirect('adm/usuarios');
        }
        $dados['tipo_usuario']=$this->usuario->listar_tipo_usuario();
        
        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $this->load->view('adm/menu');
        $this->load->view('adm/usuario/excluir_usuario',$dados);
        $this->load->view('template/footer');
    }
    
    /**
     * Faz a validação da inserção de um usuario ADM e o insere no Banco de Dados
     *
     * @param 
     * @return 
     */
    public function validacao() {
        $this->form_validation->set_rules('nome_usuario','Nome','required');
        $this->form_validation->set_rules('email','E-mail','required|valid_email|is_unique[tb_clientes.email]');
        $this->form_validation->set_message('is_unique','Esse E-mail Já Está Cadastrado');
        $this->form_validation->set_rules('id_tipo_usuario','Tipo de Cliente','required');
        $this->form_validation->set_rules('senha','Senha','required');
        $this->form_validation->set_rules('r_senha','Repetir Senha','required|matches[senha]');
        if($this->form_validation->run()==FALSE){
            $this->adicionar_usuario();
        }
        else{
            $dados= elements(array('nome_usuario','email','telefone','id_tipo_usuario','senha'), $this->input->post());
            $dados['senha']= md5($dados['senha']);
            $dados['ativo']= 1;
            $this->usuario->adicionar_usuario($dados);
            $this->session->set_flashdata('mensagem','Usuário Adicionado com Sucesso!!!');
            redirect('adm/usuarios');
        }
    }
    
    /**
     * Faz a validação da ediçao de um usuario ADM e o altera no Banco de Dados
     *
     * @param int $id_usuario ID único do usuario do sistema 
     * @return 
     */
    public function validacao_editar_usuario($id_usuario) {
        $this->form_validation->set_rules('nome_usuario','Nome','required');
        $this->form_validation->set_rules('id_tipo_usuario','Tipo de Cliente','required');
        $this->form_validation->set_rules('id_tipo_usuario','Tipo de Cliente','required');
        if($this->form_validation->run()==FALSE){
            $this->editar_usuario($id_usuario);
        }
        else{
            $usuario=elements(array('nome_usuario','id_tipo_usuario','telefone','id_tipo_usuario'), $this->input->post());
            if($this->input->post('senha')!=''){
                $usuario['senha']=md5($this->input->post('senha'));
            }
            $this->usuario->editar_usuario($usuario,$id_usuario);
            $this->session->set_flashdata('mensagem','Alteração Efetuada com Sucesso!!!');
            redirect('adm/usuarios');
        }
    }
    
    /**
     * Faz a exclusão do usuário
     *
     * @param int $id_usuario ID único do usuario do sistema 
     * @return 
     */
    public function validacao_excluir_usuario($id_usuario) {
        $this->usuario->editar_usuario(array('ativo'=>0),$id_usuario);
        $this->session->set_flashdata('mensagem','Usuário Excluído com Sucesso!!!');
        redirect('adm/usuarios');
    }
    
    /**
     * Reativa o usuário
     *
     * @param int $id_usuario ID único do usuario do sistema 
     * @return 
     */
    public function validacao_reativar_usuario($id_usuario) {
        $this->usuario->editar_usuario(array('ativo'=>1),$id_usuario);
        $this->session->set_flashdata('mensagem','Usuário Reativado com Sucesso!!!');
        redirect('adm/usuarios');
    }
    
    public function alterar_dados() {
        $id_usuario= usuario_sessao_adm()['id_usuario'];
        $dados['query']=$this->usuario->buscar_usuario($id_usuario);
        $dados['tipo_usuario']=$this->usuario->listar_tipo_usuario();
        $dados['lojas']=$this->usuario->listar_lojas();
        
        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $this->load->view('adm/menu');
        $this->load->view('adm/alterar_meus_dados',$dados);
        $this->load->view('template/footer');
    }
    
    public function validacao_alterar_meus_dados() {
        $this->form_validation->set_rules('nome_usuario','Nome','required');
        $this->form_validation->set_rules('telefone','Telefone','required');
        if($this->form_validation->run()==FALSE){
            $this->alterar_dados();
        }
        else{
            $usuario=elements(array('nome_usuario','telefone'), $this->input->post());
            if($this->input->post('senha')!=''){
                $usuario['senha']=md5($this->input->post('senha'));
            }
            $this->usuario->editar_usuario($usuario, usuario_sessao_adm()['id_usuario']);
            $this->session->set_flashdata('mensagem','Alteração Efetuada com Sucesso!!!');
            redirect('adm/usuarios');
        }
    }
    
    /**
     * Faz a paginação da listagem de usuários
     *
     * @param 
     * @return 
     */
    public function _paginacao() {
        $config['full_tag_open'] = "<nav><ul class='pagination'>";
        $config['full_tag_close'] ="</ul></nav>";
        $config['num_tag_open'] = "<li class='page-item'>";
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='page-item active'><a class='page-link' href='#'>";
        $config['cur_tag_close'] = "</a></li>";
        $config['next_tag_open'] = "<li class='page-item'>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li class='page-item'>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li class='page-item'>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li class='page-item'>";
        $config['last_tagl_close'] = "</li>";
        $config['next_link'] = "Próximo";
        $config['prev_link'] = "Anterior";
        $config['attributes'] = array('class' => 'page-link');
        $config['base_url'] = base_url() . 'adm/usuarios/paginacao/';
        $config['total_rows'] = $this->usuario->listar_usuarios()->num_rows();
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $qtde = $config['per_page'];
        $inicio = $this->uri->segment(4);
        $this->pagination->initialize($config);
        $dados['paginacao']  = $this->pagination->create_links();
        $dados['lista_usuarios'] = $this->usuario->listar_usuarios($qtde,$inicio)->result();
        return $dados;
    }
}
