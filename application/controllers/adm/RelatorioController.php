<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Controller responsável pela geraçao do PDF das informações dos clientes
 *
 * PHP version 7
 *
 * @category PHP
 * @author   Kellton Leitão <kelltonl10@gmail.com>
 */
class RelatorioController extends CI_Controller {

     public function __construct() {
        parent::__construct();
         if(!usuario_sessao_adm() || (usuario_sessao_adm()['tipo_usuario']!=1 && usuario_sessao_adm()['tipo_usuario']!=4)) {
            redirect(base_url());
        }
        $this->load->model('ClienteModel','cliente');
        $this->load->library('mympdf');
    }
    
    /**
     * Gera o PDF das informações do pretendente pessoa física
     *
     * @param int $id_cliente ID único do cliente 
     * @return 
     */
    public function relatorio_ficha_pretendente_pessoa_fisica($id_cliente){
        $dados=$this->_carregar_dados_pessoa_fisica($id_cliente); 
        $html=$this->load->view('adm/cliente/relatorio/ficha_cadastro_pretendente', $dados, true);
        $this->mympdf->generate($html,'Pretendente Pessoa Física - '.$dados['query2']['nome_completo']);
    }
    
    /**
     * Gera o PDF das informações do segundo pretendente pessoa física
     *
     * @param int $id_cliente ID único do cliente 
     * @return 
     */
    public function relatorio_ficha_segundo_pretendente_pessoa_fisica($id_cliente){
        $dados=$this->_carregar_dados_pessoa_fisica($id_cliente); 
        $html=$this->load->view('adm/cliente/relatorio/ficha_cadastro_segundo_pretendente', $dados, true);
        $this->mympdf->generate($html,'Segundo Pretendente Pessoa Física - '.$dados['query3']['nome_completo']);
    }
    
    /**
     * Gera o PDF das informações do fiador pessoa física
     *
     * @param int $id_cliente ID único do cliente 
     * @return 
     */
    public function relatorio_ficha_fiador($id_cliente){
        $dados=$this->_carregar_dados_pessoa_fisica($id_cliente); 
        $html=$this->load->view('adm/cliente/relatorio/ficha_cadastro_fiador', $dados, true);
        $this->mympdf->generate($html,'Fiador - '.$dados['query4']['nome_completo']);
    }
    
    /**
     * Gera o PDF das informações do cliente pessoa jurídica
     *
     * @param int $id_cliente ID único do cliente 
     * @return 
     */
    public function relatorio_pessoa_juridica($id_cliente){
        $dados=$this->_carregar_dados_pessoa_juridica($id_cliente); 
        $html=$this->load->view('adm/cliente/relatorio/ficha_cadastro_juridico', $dados, true);
        $this->mympdf->generate($html,'Pessoa Jurídica - '.$dados['query2']['nome_completo']);
    }
    
    /**
     * Página de Visualização de todas as informações do cliente pessoa fisica
     *
     * @param int $id_cliente ID único do cliente 
     * @return 
     */
    public function _carregar_dados_pessoa_fisica($id_cliente) {
        if(usuario_sessao_adm()['tipo_usuario']==4){
            $dados['query']=$this->cliente->buscar_informacoes($id_cliente, usuario_sessao_adm()['id_usuario']);
        }
        else{
            $dados['query']=$this->cliente->buscar_informacoes($id_cliente);
        }
        if($dados['query']==NULL){
            redirect('adm/clientes');
        }
        $dados['id_cliente']=$id_cliente;
        /*======= BUSCA AS INFORMAÇÕES DAS FICHAS CADASTRAIS DOS CLIENTES =====*/
        $dados['tem_pretendente']=$this->cliente->verifica_se_tem_pretendente($id_cliente);
        $dados['tem_fiador']=$this->cliente->verifica_se_tem_fiador($id_cliente);
        $dados['query2']=$this->cliente->pega_dados_pretendente($id_cliente);
        $dados['enderecos']=$this->cliente->enderecos_pretendentes($id_cliente,$dados['query2']['pretendente_id']);
        $dados['residentes']=$this->cliente->pegar_residentes($id_cliente);
        $dados['propriedades']=$this->cliente->pegar_propriedades($id_cliente);
        $dados['referencias']=$this->cliente->pegar_referencias($id_cliente);
        $dados['referencias_bancarias']=$this->cliente->pegar_referencias_bancarias($id_cliente);
        $dados['pais']=$this->cliente->pegar_pais_pretendentes($id_cliente);
        $dados['query3']=$this->cliente->pega_dados_segundo_pretendente($id_cliente);
        $dados['query4']=$this->cliente->pega_dados_fiador($id_cliente);
        $dados['documentos_residentes']=$this->cliente->get_documentos_residentes($id_cliente);
        $id_fiador=$dados['query4']['id_fiador'];
        $dados['propriedades_fiador']=$this->cliente->pega_propriedades_fiador($id_fiador);
        $dados['referencias_fiador']=$this->cliente->pega_referencias_fiador($id_fiador);
        $dados['referencias_bancarias_fiador']=$this->cliente->pega_referencias_bancarias_fiador($id_fiador);
        $dados['documentos_fiador']=$this->cliente->get_documentos_fiadores($id_cliente);
        $dados['documentos_conjugue_fiador']= $this->cliente->get_documentos_conjugue_fiador($id_fiador);
        return $dados;
    }
    
    /**
     * Página de Visualização de todas as informações do cliente pessoa jurídica
     *
     * @param int $id_cliente ID único do cliente 
     * @return 
     */
    public function _carregar_dados_pessoa_juridica($id_cliente) {
        if(usuario_sessao_adm()['tipo_usuario']==4){
            $dados['query']=$this->cliente->buscar_informacoes($id_cliente, usuario_sessao_adm()['id_usuario']);
        }
        else{
            $dados['query']=$this->cliente->buscar_informacoes($id_cliente);
        }
        if($dados['query']==NULL){
            redirect('adm/clientes');
        }
        $dados['id_cliente']=$id_cliente;
        $dados['query2']= $this->cliente->get_cliente_jurudico($id_cliente);
        $dados['socios']= $this->cliente->get_socios($id_cliente);
        $dados['propriedades']=$this->cliente->pegar_propriedades($id_cliente);
        $dados['referencias']=$this->cliente->pegar_referencias($id_cliente);
        $dados['referencias_bancarias']=$this->cliente->pegar_referencias_bancarias($id_cliente);
        $dados['documentos']=$this->cliente->get_documentos_juridico($id_cliente);
        $dados['documentos_socios']=$this->cliente->get_documentos_socios($id_cliente);     
        return $dados;
    }
    
}
