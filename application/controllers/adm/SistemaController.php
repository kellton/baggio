<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * SISTEMACONTROLLER
 * -------------------------------------------------------------------
 * DESCRICAO : 
 * -------------------------------------------------------------------
 * CARREGA : 
 * - N/A
 */
class SistemaController extends CI_Controller {

    /*
     * CONSTRUCT
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function __construct() {
        parent::__construct();
        if(!usuario_sessao_adm()) {
            $this->session->set_flashdata('param_msg_erro_cabecalho', 'Sem Permissão');
            $this->session->set_flashdata('param_msg_erro', 'Você não tem permissão de acessar esta área!');

            redirect(base_url());
        }

        $this->load->model('EnderecoModel','m_enderecos');
        $this->load->model('ImobiliariaModel','m_imobiliarias');
    }
    
    /*
     * INDEX
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function index() {
        if(!is_usuario_adm()) {
             redirect(base_url());
        }
    }

    
    /*
     * CARREGAR_GERENCIAR_AUX_BAIRROS
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function carregar_gerenciar_aux_bairros() {
        if (is_usuario_adm()) {
            $url_paginacao = "adm/SistemaController/carregar_gerenciar_aux_bairros";
            $pagina = $this->uri->segment(calcular_uri_paginacao($url_paginacao));

            if ($pagina == NULL || empty($pagina)) {
                $pagina = "0";
            }

            $dados = array();
            $dados_bairros = $this->m_enderecos->paginar_aux_bairros(10, $pagina);
            $lista_aux_bairros = array();

            foreach ($dados_bairros as $dados_bairro) {
                $bairro = array();
                $bairro["id"] = $dados_bairro["id_aux_bairro"];
                $bairro["nome_aux"] = "\"" . $dados_bairro["nome_aux"] . "\"";
                $bairro["nome_original"] = $dados_bairro["nome_bairro"];
                $bairro["nome_cidade"] = $dados_bairro["nome_cidade"];
                $bairro["editar"] = "<a type='button' class='btn btn-primary comportamento-botao' href='" . base_url('adm/editar/aux-bairro/' . $dados_bairro['id_aux_bairro']) . "' title='Editar bairro auxiliar'><i class='fas fa-edit' aria-hidden='true'></i></a>";
                $bairro["excluir"] = "<a type='button' class='btn btn-primary comportamento-botao' href='" . base_url('adm/excluir/aux-bairro/' . $dados_bairro['id_aux_bairro']) . "' title='Excluir bairro auxiliar'><i class='fas fa-window-close' aria-hidden='true'></i></a>";
                
                array_push($lista_aux_bairros, $bairro);
            }

            $cabecalho = array(
                "#", 
                "Nome do Bairro Auxiliar", 
                "Nome do Bairro de Referência", 
                "Nome da Cidade", 
                "Editar", 
                "Excluir"
            );
            $html_sem_resultados = "";
            $html_sem_resultados .= "<div class='row'>";
            $html_sem_resultados .= "<div class='col texto-central'>";
            $html_sem_resultados .= "<h1 class='texto-sem-resultados'>";
            $html_sem_resultados .= "Não há bairros auxiliares cadastrados.";
            $html_sem_resultados .= "</h1>";
            $html_sem_resultados .= "</div>";
            $html_sem_resultados .= "</div>";

            $dados['tabela_aux_bairros'] = tabelar($lista_aux_bairros, $cabecalho, "", "celula-texto-central", "", "celula-texto-central", $html_sem_resultados);

            $dados['paginacao_aux_bairros']  = paginar($url_paginacao, 10, 
                                       $this->m_enderecos->quantificar_aux_bairros(), 
                                       $lista_aux_bairros
            );

            $header["scripts"] = carregar_js();
            $header["styles"] = carregar_css();

            $this->load->view('template/header', $header);
            $this->load->view('template/mensagens');
            $this->load->view('adm/menu');
            $this->load->view('adm/sistema/bairros/gerenciar_aux_bairros', $dados);
            $this->load->view('template/footer');
        }
    }

    /*
     * CARREGAR_ADICIONAR_AUX_BAIRRO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function carregar_adicionar_aux_bairro() {
        if (is_usuario_adm()) {
            $dados = array();
            $dados["bairros"] = listar_dropdown($this->m_enderecos->listar_bairros_id());

            if( isset($_SESSION['param_msg']) ) {
                unset($_SESSION['param_msg']);
            }

            $header["scripts"] = carregar_js();
            $header["styles"] = carregar_css();

            $this->load->view('template/header', $header);
            $this->load->view('template/mensagens');
            $this->load->view('adm/menu');
            $this->load->view('adm/sistema/bairros/adicionar_aux_bairro', $dados);
            $this->load->view('template/footer');
        }
    }

    /*
     * CADASTRAR_AUX_BAIRRO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function cadastrar_aux_bairro() {
        if (is_usuario_adm()) {
            $this->form_validation->set_rules("param_nome", "Nome do Bairro", "required");
            $this->form_validation->set_rules("param_id_bairro", "Bairro Referente", "required");

            if($this->form_validation->run() == FALSE) {
                $this->carregar_adicionar_aux_bairro();
            } else {
                $dados = elements( array("param_nome", "param_id_bairro"), $this->input->post());

                $id_cidade = $this->m_enderecos->buscar_id_cidade_bairro($dados["param_id_bairro"]);

                if($id_cidade != NULL) {
                    if( isset($_SESSION['param_msg']) ) {
                        unset($_SESSION['param_msg']);
                    }

                    $dados["param_id_cidade"] = $id_cidade;
                    
                    if ($this->m_enderecos->inserir_aux_bairro($this->_modelar_aux_bairro($dados)) ) {
                        $this->session->set_flashdata('param_msg', 'Bairro auxiliar cadastrado com sucesso.');
                    } else {
                        $this->session->set_flashdata('param_msg', 'Ocorreu um problema ao cadastrar este bairro auxiliar.');
                    }
                    $this->carregar_gerenciar_aux_bairros();
                }
            }
        }
    }

    /*
     * EDITAR_AUX_BAIRRO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function editar_aux_bairro() {
        if (is_usuario_adm()) {
            $url = "adm/editar/aux-bairro";
            $id_aux_bairro = $this->uri->segment(calcular_uri_paginacao($url));

            $bairro_aux = $this->m_enderecos->buscar_nome_aux_bairro($id_aux_bairro);

            $dados = array();
            $dados["param_nome"] = $bairro_aux;
            $dados["bairros"] = listar_dropdown($this->m_enderecos->listar_bairros_id());

            if( isset($_SESSION['param_msg']) ) {
                unset($_SESSION['param_msg']);
            }

            $header["scripts"] = carregar_js();
            $header["styles"] = carregar_css();

            $this->load->view('template/header', $header);
            $this->load->view('template/mensagens');
            $this->load->view('adm/menu');
            $this->load->view('adm/sistema/bairros/adicionar_aux_bairro', $dados);
            $this->load->view('template/footer');
        }
    }

    /*
     * EXCLUIR_AUX_BAIRRO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function excluir_aux_bairro() {
        if (is_usuario_adm()) {
            $url = "adm/excluir/aux-bairro";
            $id_aux_bairro = $this->uri->segment(calcular_uri_paginacao($url));

            if( isset($_SESSION['param_msg']) ) {
                unset($_SESSION['param_msg']);
            }

            if ($this->m_enderecos->excluir_aux_bairro($id_aux_bairro)) {
                $this->session->set_flashdata('param_msg', 'Bairro auxiliar excluído com sucesso.');
            } else {
                $this->session->set_flashdata('param_msg', 'Não foi possível realizar esta ação.');
            }
            $this->carregar_gerenciar_aux_bairros();
        }
    }

    /*
     * CARREGAR_GERENCIAR_BAIRROS
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function carregar_gerenciar_bairros() {
        if (is_usuario_adm()) {
            $dados = array();
            $dados_bairros = $this->m_enderecos->listar_bairros();
            $lista_bairros = array();

            foreach ($dados_bairros as $dados_bairro) {
                $bairro = array();
                $bairro["id"] = $dados_bairro["id_bairro"];
                $bairro["cidade"] = $dados_bairro["nome_cidade"];
                $bairro["nome"] = $dados_bairro["nome_bairro"];
                $bairro["excluir"] = "<a type='button' class='btn btn-primary comportamento-botao' href='" . base_url('adm/excluir/bairro/' . $dados_bairro['id_bairro']) . "' title='Excluir cidade'><i class='fas fa-window-close' aria-hidden='true'></i></a>";
                
                array_push($lista_bairros, $bairro);
            }

            $cabecalho = array("#", "Cidade", "Bairro", "Excluir");
            $html_sem_resultados = "";
            $html_sem_resultados .= "<div class='row'>";
            $html_sem_resultados .= "<div class='col texto-central'>";
            $html_sem_resultados .= "<h1 class='texto-sem-resultados'>";
            $html_sem_resultados .= "Não há bairros cadastrados.";
            $html_sem_resultados .= "</h1>";
            $html_sem_resultados .= "</div>";
            $html_sem_resultados .= "</div>";

            $dados['tabela_bairros'] = tabelar($lista_bairros, $cabecalho, "", "celula-texto-central", "", "celula-texto-central", $html_sem_resultados);

            $header["scripts"] = carregar_js();
            $header["styles"] = carregar_css();

            $this->load->view('template/header', $header);
            $this->load->view('template/mensagens');
            $this->load->view('adm/menu');
            $this->load->view('adm/sistema/bairros/gerenciar_bairros', $dados);
            $this->load->view('template/footer');
        }
    }

    /*
     * CARREGAR_ADICIONAR_BAIRRO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function carregar_adicionar_bairro() {
        if (is_usuario_adm()) {
            $dados = array();
            $dados["cidades"] = listar_dropdown($this->m_enderecos->listar_cidades_estado());

            if( isset($_SESSION['param_msg']) ) {
                unset($_SESSION['param_msg']);
            }

            $header["scripts"] = carregar_js();
            $header["styles"] = carregar_css();

            $this->load->view('template/header', $header);
            $this->load->view('template/mensagens');
            $this->load->view('adm/menu');
            $this->load->view('adm/sistema/bairros/adicionar_bairro', $dados);
            $this->load->view('template/footer');
        }
    }

    /*
     * CADASTRAR_BAIRRO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function cadastrar_bairro() {
        if (is_usuario_adm()) {
            $this->form_validation->set_rules("param_nome", "Nome do Bairro", "required");
            $this->form_validation->set_rules("param_id_cidade", "Cidade", "required");

            if($this->form_validation->run() == FALSE) {
                $this->carregar_adicionar_bairro();
            } else {
                $dados = elements( array("param_nome", "param_id_cidade"), $this->input->post());

                if( isset($_SESSION['param_msg']) ) {
                    unset($_SESSION['param_msg']);
                }

                if ( $this->m_enderecos->inserir_bairro($this->_modelar_bairro($dados)) ) {
                    $this->session->set_flashdata('param_msg', 'Bairro cadastrado com sucesso.');
                } else {
                    $this->session->set_flashdata('param_msg', 'Ocorreu um problema ao cadastrar este bairro.');
                }
                $this->carregar_gerenciar_bairros();
            }
        }
    }

    /*
     * EXCLUIR_BAIRRO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function excluir_bairro() {
        if (is_usuario_adm()) {
            $url = "adm/excluir/bairro";
            $id_bairro = $this->uri->segment(calcular_uri_paginacao($url));

            if( isset($_SESSION['param_msg']) ) {
                unset($_SESSION['param_msg']);
            }

            if ($this->m_enderecos->excluir_bairro($id_bairro)) {
                $this->session->set_flashdata('param_msg', 'Bairro excluído com sucesso.');
            } else {
                $this->session->set_flashdata('param_msg', 'Não foi possível realizar esta ação.');
            }

            $this->carregar_gerenciar_bairros();
        }
    }

    /*
     * CARREGAR_GERENCIAR_CIDADES
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function carregar_gerenciar_cidades() {
        if (is_usuario_adm()) {
            $dados = array();
            $dados_cidades = $this->m_enderecos->listar_cidades();
            $lista_cidades = array();

            foreach ($dados_cidades as $dados_cidade) {
                $cidade = array();
                $cidade["id"] = $dados_cidade["id_cidade"];
                $cidade["nome"] = $dados_cidade["nome_cidade"];
                $cidade["estado"] = $dados_cidade["sigla_estado"];
                $cidade["excluir"] = "<a type='button' class='btn btn-primary comportamento-botao' href='" . base_url('adm/excluir/cidade/' . $dados_cidade['id_cidade']) . "' title='Excluir cidade'><i class='fas fa-window-close' aria-hidden='true'></i></a>";
                
                array_push($lista_cidades, $cidade);
            }

            $cabecalho = array("#","Nome da Cidade","Estado","Excluir");
            $html_sem_resultados = "";
            $html_sem_resultados .= "<div class='row'>";
            $html_sem_resultados .= "<div class='col texto-central'>";
            $html_sem_resultados .= "<h1 class='texto-sem-resultados'>";
            $html_sem_resultados .= "Não há cidades cadastrados.";
            $html_sem_resultados .= "</h1>";
            $html_sem_resultados .= "</div>";
            $html_sem_resultados .= "</div>";

            $dados['tabela_cidades'] = tabelar($lista_cidades, $cabecalho, "", "celula-texto-central", "", "celula-texto-central", $html_sem_resultados);

            $header["scripts"] = carregar_js();
            $header["styles"] = carregar_css();

            $this->load->view('template/header', $header);
            $this->load->view('template/mensagens');
            $this->load->view('adm/menu');
            $this->load->view('adm/sistema/cidades/gerenciar_cidades', $dados);
            $this->load->view('template/footer');
        }
    }

    /*
     * CARREGAR_ADICIONAR_CIDADE
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function carregar_adicionar_cidade() {
        if (is_usuario_adm()) {
            $dados = array();
            $dados["estados"] = listar_dropdown($this->m_enderecos->listar_estados_sigla());

            if( isset($_SESSION['param_msg']) ) {
                unset($_SESSION['param_msg']);
            }

            $header["scripts"] = carregar_js();
            $header["styles"] = carregar_css();

            $this->load->view('template/header', $header);
            $this->load->view('template/mensagens');
            $this->load->view('adm/menu');
            $this->load->view('adm/sistema/cidades/adicionar_cidade', $dados);
            $this->load->view('template/footer');
        }
    }

    /*
     * CADASTRAR_CIDADE
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function cadastrar_cidade() {
        if (is_usuario_adm()) {
            $this->form_validation->set_rules("param_nome", "Nome da Cidade", "required");

            if($this->form_validation->run() == FALSE) {
                $this->carregar_adicionar_cidade();
            } else {
                $dados = elements( array("param_nome", "param_id_estado"), $this->input->post());

                if( isset($_SESSION['param_msg']) ) {
                    unset($_SESSION['param_msg']);
                }

                if ( $this->m_enderecos->inserir_cidade($this->_modelar_cidade($dados)) ) {
                    $this->session->set_flashdata('param_msg', 'Cidade cadastrada com sucesso.');
                } else {
                    $this->session->set_flashdata('param_msg', 'Ocorreu um problema ao cadastrar essa cidade.');
                }

                $this->carregar_gerenciar_cidades();
            }
        }
    }

    /*
     * EXCLUIR_CIDADE
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function excluir_cidade() {
        if (is_usuario_adm()) {
            $url = "adm/excluir/cidade";
            $id_cidade = $this->uri->segment(calcular_uri_paginacao($url));

            if( isset($_SESSION['param_msg']) ) {
                unset($_SESSION['param_msg']);
            }

            if ($this->m_enderecos->excluir_cidade($id_cidade)) {
                $this->session->set_flashdata('param_msg', 'Cidade excluída com sucesso.');
            } else {
                $this->session->set_flashdata('param_msg', 'Não foi possível realizar esta ação.');
            }

            $this->carregar_gerenciar_cidades();
        }
    }

    /*
     * CARREGAR_GERENCIAR_ESTADOS
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function carregar_gerenciar_estados() {
        if (is_usuario_adm()) {
            $dados = array();
            $lista_estados = array();
            $dados_estados = $this->m_enderecos->listar_estados();

            foreach ($dados_estados as $dados_estado) {
                $estado = array();
                $estado["id"] = $dados_estado["id_estado"];
                $estado["nome"] = $dados_estado["nome_estado"];
                $estado["sigla"] = $dados_estado["sigla_estado"];
                $estado["excluir"] = "<a type='button' class='btn btn-primary comportamento-botao' href='" . base_url('adm/excluir/estado/' . $dados_estado['id_estado']) . "' title='Excluir cidade'><i class='fas fa-window-close' aria-hidden='true'></i></a>";

                array_push($lista_estados, $estado);
            }

            $cabecalho = array("#","Nome do Estado","Sigla do Estado","Excluir");
            $html_sem_resultados = "";
            $html_sem_resultados .= "<div class='row'>";
            $html_sem_resultados .= "<div class='col texto-central'>";
            $html_sem_resultados .= "<h1 class='texto-sem-resultados'>";
            $html_sem_resultados .= "Não há Estados cadastrados.";
            $html_sem_resultados .= "</h1>";
            $html_sem_resultados .= "</div>";
            $html_sem_resultados .= "</div>";

            $dados['tabela_estados'] = tabelar($lista_estados, $cabecalho, "", "celula-texto-central", "", "celula-texto-central", $html_sem_resultados);

            $header["scripts"] = carregar_js();
            $header["styles"] = carregar_css();

            $this->load->view('template/header', $header);
            $this->load->view('template/mensagens');
            $this->load->view('adm/menu');
            $this->load->view('adm/sistema/estados/gerenciar_estados', $dados);
            $this->load->view('template/footer');
        }
    }

    /*
     * CARREGAR_ADICIONAR_ESTADO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function carregar_adicionar_estado() {
        if (is_usuario_adm()) {
            $dados = array();

            if( isset($_SESSION['param_msg']) ) {
                unset($_SESSION['param_msg']);
            }

            $header["scripts"] = carregar_js();
            $header["styles"] = carregar_css();

            $this->load->view('template/header', $header);
            $this->load->view('template/mensagens');
            $this->load->view('adm/menu');
            $this->load->view('adm/sistema/estados/adicionar_estado');
            $this->load->view('template/footer');
        }
    }

    /*
     * CADASTRAR_ESTADO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function cadastrar_estado() {
        if (is_usuario_adm()) {
            $this->form_validation->set_rules("param_nome", "Nome do Estado", "required");
            $this->form_validation->set_rules("param_sigla", "Sigla do Estado", "required");

            if($this->form_validation->run() == FALSE) {
                $this->carregar_adicionar_estado();
            } else {
                $dados = elements( array("param_nome", "param_sigla"), $this->input->post());

                if( isset($_SESSION['param_msg']) ) {
                    unset($_SESSION['param_msg']);
                }

                if ( $this->m_enderecos->inserir_estado($this->_modelar_estado($dados)) ) {
                    $this->session->set_flashdata('param_msg', 'Estado cadastrado com sucesso.');
                } else {
                    $this->session->set_flashdata('param_msg', 'Ocorreu um problema ao cadastrar este Estado.');
                }

                $this->carregar_gerenciar_estados();
            }
        }
    }

    /*
     * EXCLUIR_ESTADO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function excluir_estado() {
        if (is_usuario_adm()) {
            $url = "adm/excluir/estado";
            $id_estado = $this->uri->segment(calcular_uri_paginacao($url));

            if( isset($_SESSION['param_msg']) ) {
                unset($_SESSION['param_msg']);
            }

            if ($this->m_enderecos->excluir_estado($id_estado)) {
                $this->session->set_flashdata('param_msg', 'Estado excluído com sucesso.');
            } else {
                $this->session->set_flashdata('param_msg', 'Não foi possível realizar esta ação.');
            }

            $this->carregar_gerenciar_estados();
        }
    }

    /*
     * CARREGAR_GERENCIAR_JOBS
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function carregar_gerenciar_jobs() {
        if (is_usuario_adm()) {
            $dados = array();
            $dados_jobs = $this->m_imobiliarias->listar_jobs();
            $lista_jobs = array();

            foreach ($dados_jobs as $dados_job) {
                $job = array();
                $job["id"] = $dados_job["id_jobs_xml"];
                $job["rodar"] = "<a type='button' class='btn btn-primary comportamento-botao' href='" . base_url('adm/rodar/job/' . $dados_job["codigo"]) . "' title='Rodar Job'><i class='fa fa-play-circle' aria-hidden='true'></i></a>";
                $job["imobiliaria"] = $dados_job["nome_imobiliaria"];
                $job["url"] = $dados_job["url"];
                $job["codigo"] = $dados_job["codigo"];
                $job["horarios"] = $dados_job["horarios_atualizacao"];
                $job["data"] = $dados_job["data_ultimo_job"];
                $job["ativar_desativar"] = ( $dados_job["ativo"] ? "<a type='button' class='btn btn-primary comportamento-botao' href='" . base_url('adm/alternar-ativacao/job/' . $dados_job["id_jobs_xml"]) . "' title='Ativar/Desativar Job'><i class='fas fa-stop-circle' aria-hidden='true'></i></a>" : "<a type='button' class='btn btn-primary comportamento-botao' href='" . base_url('adm/alternar-ativacao/job/' . $dados_job["id_jobs_xml"]) . "' title='Ativar/Desativar Job'><i class='fas fa-check-circle' aria-hidden='true'></i></a>");
                $job["ativo"] = ( $dados_job["ativo"] ? "<i class='fas fa-check-circle cor-verde-botao' aria-hidden='true'></i>" : "<i class='fas fa-window-close cor-vermelha-botao' aria-hidden='true'></i>");

                array_push($lista_jobs, $job);
            }

            $cabecalho = array(
                "#", "Rodar",
                "Imobiliária", "URL", "Código", 
                "Horários de Atualização",
                "Última Atualização", "Ativar/Desativar", "Ativo ?"
            );
            $html_sem_resultados = "";
            $html_sem_resultados .= "<div class='row'>";
            $html_sem_resultados .= "<div class='col texto-central'>";
            $html_sem_resultados .= "<h1 class='texto-sem-resultados'>";
            $html_sem_resultados .= "Não há Jobs cadastrados.";
            $html_sem_resultados .= "</h1>";
            $html_sem_resultados .= "</div>";
            $html_sem_resultados .= "</div>";

            $dados['tabela_jobs'] = tabelar($lista_jobs, $cabecalho, "", "celula-texto-central", "", "celula-texto-central", $html_sem_resultados);

            $header["scripts"] = carregar_js();
            $header["styles"] = carregar_css();

            $this->load->view('template/header', $header);
            $this->load->view('template/mensagens');
            $this->load->view('adm/menu');
            $this->load->view('adm/sistema/jobs/gerenciar_jobs', $dados);
            $this->load->view('template/footer');
        }
    }

    /*
     * CARREGAR_ADICIONAR_JOB
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function carregar_adicionar_job() {
        if (is_usuario_adm()) {
            if( isset($_SESSION['param_msg']) ) {
                unset($_SESSION['param_msg']);
            }

            $dados = array();

            $dados["imobiliarias"] = $this->m_imobiliarias->listar_imobiliarias();

            $header["scripts"] = carregar_js();
            $header["styles"] = carregar_css();

            $this->load->view('template/header', $header);
            $this->load->view('template/mensagens');
            $this->load->view('adm/menu');
            $this->load->view('adm/sistema/jobs/adicionar_job', $dados);
            $this->load->view('template/footer');
        }
    }

    /*
     * CADASTRAR_JOB
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function cadastrar_job() {
        if (is_usuario_adm()) {
            $this->form_validation->set_rules("param_id_imobiliaria", "Imobiliária", "required");
            $this->form_validation->set_rules("param_codigo", "Código", "required");
            $this->form_validation->set_rules("param_horarios", "Horários", "required");

            if($this->form_validation->run() == FALSE) {
                $this->carregar_adicionar_job();
            } else {
                $dados = elements( array(
                    "param_id_imobiliaria",
                    "param_codigo",
                    "param_horarios"
                ), $this->input->post());

                if( isset($_SESSION['param_msg']) ) {
                    unset($_SESSION['param_msg']);
                }

                if ( $this->m_imobiliarias->inserir_job($this->_modelar_job($dados)) ) {
                    $this->session->set_flashdata('param_msg', 'Job cadastrado com sucesso.');
                } else {
                    $this->session->set_flashdata('param_msg', 'Ocorreu um problema ao cadastrar este bairro.');
                }

                $this->carregar_gerenciar_jobs();
            }
        }
    }

    /*
     * ALTERNAR_ATIVACAO_JOB
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function alternar_ativacao_job() {
        if (is_usuario_adm()) {
            $url = "adm/excluir/job";
            $id_job = $this->uri->segment(calcular_uri_paginacao($url));

            if( isset($_SESSION['param_msg']) ) {
                unset($_SESSION['param_msg']);
            }

            if ($this->m_imobiliarias->esta_ativo_job($id_job)) {
                if ($this->m_imobiliarias->desativar_job($id_job)) {
                    $this->session->set_flashdata('param_msg', 'Job desativado com sucesso.');
                } else {
                    $this->session->set_flashdata('param_msg', 'Não foi possível realizar esta ação.');
                }
            } else {
                if ($this->m_imobiliarias->ativar_job($id_job)) {
                    $this->session->set_flashdata('param_msg', 'Job ativado com sucesso.');
                } else {
                    $this->session->set_flashdata('param_msg', 'Não foi possível realizar esta ação.');
                }
            }

            redirect(base_url('adm/gerenciar/jobs'));
        }
    }


    /* ****************************************************** */
    /* ********************* PREPARACAO ********************* */
    /* ****************************************************** */
    /*
     * _MODELAR_AUX_BAIRRO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - 
    */
    private function _modelar_aux_bairro($formulario=array()) {
        if (!empty($formulario)) {
            $dados = array();

            $dados["id_bairro"] = $formulario["param_id_bairro"];
            $dados["id_cidade"] = $formulario["param_id_cidade"];
            $dados["nome_aux"] = $formulario["param_nome"];

            return $dados;
        }
        return array();
    }

    /*
     * _MODELAR_BAIRRO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - 
    */
    private function _modelar_bairro($formulario=array()) {
        if (!empty($formulario)) {
            $dados = array();

            $dados["id_cidade"] = $formulario["param_id_cidade"];
            $dados["nome_bairro"] = $formulario["param_nome"];

            return $dados;
        }
        return array();
    }

    /*
     * _MODELAR_CIDADE
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - 
    */
    private function _modelar_cidade($formulario=array()) {
        if (!empty($formulario)) {
            $dados = array();

            $dados["id_estado"] = $formulario["param_id_estado"];
            $dados["nome_cidade"] = $formulario["param_nome"];

            return $dados;
        }
        return array();
    }

    /*
     * _MODELAR_ESTADO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - 
    */
    private function _modelar_estado($formulario=array()) {
        if (!empty($formulario)) {
            $dados = array();

            $dados["nome_estado"] = $formulario["param_nome"];
            $dados["sigla_estado"] = $formulario["param_sigla"];

            return $dados;
        }
        return array();
    }

    /*
     * _MODELAR_JOB
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - 
    */
    private function _modelar_job($formulario=array()) {
        if (!empty($formulario)) {
            $dados = array();

            $dados["id_imobiliaria"] = $formulario["param_id_imobiliaria"];
            $dados["url"] = CONS_BAGGIO_URL_UPDATE_XML . $formulario["param_codigo"];
            $dados["codigo"] = $formulario["param_codigo"];
            $dados["horarios_atualizacao"] = $formulario["param_horarios"];

            return $dados;
        }
        return array();
    }
    
}
