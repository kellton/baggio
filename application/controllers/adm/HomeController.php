<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * HOMECONTROLLER
 * -------------------------------------------------------------------
 * DESCRICAO : 
 * -------------------------------------------------------------------
 * CARREGA : 
 * - N/A
 */
class HomeController extends CI_Controller {

    /*
     * CONSTRUCT
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function __construct() {
        parent::__construct();
        if(!usuario_sessao_adm() && !usuario_sessao_cliente()) {
            $this->session->set_flashdata('param_msg_erro_cabecalho', 'Sem Permissão');
            $this->session->set_flashdata('param_msg_erro', 'Você não tem permissão de acessar esta área!');

            redirect(base_url());
        }
    }
    
    /*
     * INDEX
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function index() {
        if(is_usuario_adm()) {
            $header["scripts"] = carregar_js();
            $header["styles"] = carregar_css();

            $this->load->view('template/header', $header);
            $this->load->view('template/mensagens');
            $this->load->view('adm/menu');
            $this->load->view('adm/home');
            $this->load->view('template/footer');
        } else {
            $header["scripts"] = carregar_js();
            $header["styles"] = carregar_css();

            $this->load->view('template/header', $header);
            $this->load->view('template/mensagens');
            $this->load->view('adm/menu');
            $this->load->view('adm/home_corretor');
            $this->load->view('template/footer');
        }
    }
    
}
