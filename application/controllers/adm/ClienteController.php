<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Controller responsável pela visualização do cliente pelo usuário ADM
 *
 * PHP version 7
 *
 * @category PHP
 * @author   Kellton Leitão <kelltonl10@gmail.com>
 */
class ClienteController extends CI_Controller {

     public function __construct() {
        parent::__construct();
         if(!usuario_sessao_adm() || (usuario_sessao_adm()['tipo_usuario']!=1 && usuario_sessao_adm()['tipo_usuario']!=4)) {
            redirect(base_url());
        }
        $this->load->model('ClienteModel','cliente');
    }
    
    /**
     * Visualiza todos os clientes cadastrados no sistema
     *
     * @param 
     * @return 
     */
    public function index(){
        $dados=$this->_paginacao();
        
        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $this->load->view('adm/menu');
        $this->load->view('adm/cliente/listagem_clientes',$dados);
        $this->load->view('template/footer');
    }
    
    public function pesquisa_cliente(){
        $dados=$this->_paginacao($this->input->post("busca_cliente"));
        
        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $this->load->view('adm/menu');
        $this->load->view('adm/cliente/listagem_clientes',$dados);
        $this->load->view('template/footer');
    }
    
    /**
     * Adiciona um novo Cliente
     *
     * @param 
     * @return 
     */
    public function adicionar_cliente() {
        
        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $dados['tipo_usuario']=$this->cliente->listar_tipo_usuario();
        $dados['corretores']=$this->cliente->listar_corretores();
        $this->load->view('adm/menu');
        $this->load->view('adm/cliente/adicionar_cliente',$dados);
        $this->load->view('template/footer');
    }
    
    /**
     * Edita um Cliente pessoa física existente
     *
     * @param int $id_cliente ID único do cliente 
     * @return 
     */
    public function editar_cliente($id_cliente) {
        if(usuario_sessao_adm()['tipo_usuario']==4){
            $dados['query']=$this->cliente->buscar_cliente($id_cliente, usuario_sessao_adm()['id_usuario']);
        }
        else{
            $dados['query']=$this->cliente->buscar_cliente($id_cliente);
        }
        if($dados['query']==NULL){
            redirect('adm/clientes');
        }
        if($dados['query']['tipo_cliente']==="Pessoa Jurídica"){
            $this->_editar_cliente_pessoa_juridica($id_cliente);
        }
        else{
            $dados['corretores']=$this->cliente->listar_corretores();
            $dados['tipo_usuario']=$this->cliente->listar_tipo_usuario();
            $dados['query']['id_cliente']=$id_cliente;
            
            $header["scripts"] = carregar_js();
            $header["styles"] = carregar_css();

            $this->load->view('template/header', $header);
            $this->load->view('adm/menu');
            $this->load->view('adm/cliente/editar_cliente',$dados);
            $this->load->view('template/footer');
        }
    }
    
    /**
     * Edita um Cliente pessoa jurídica existente
     *
     * @param int $id_cliente ID único do cliente 
     * @return 
     */
    public function _editar_cliente_pessoa_juridica($id_cliente) {
            $dados['query']=$this->cliente->buscar_cliente($id_cliente);
            $dados['corretores']=$this->cliente->listar_corretores();
            $dados['tipo_usuario']=$this->cliente->listar_tipo_usuario();
            $dados['query']['id_cliente']=$id_cliente;
            
            $header["scripts"] = carregar_js();
            $header["styles"] = carregar_css();

            $this->load->view('template/header', $header);
            $this->load->view('adm/menu');
            $this->load->view('adm/cliente/alterar_dados_juridico',$dados);
            $this->load->view('template/footer');
    }
    
    /**
     * Faz a validação ba ediçao de um cliente
     *
     * @param int $id_cliente ID único do cliente 
     * @return 
     */
    public function validacao_editar_cliente_juridico($id_cliente) {
        $this->form_validation->set_rules('nome_cliente','Nome','required');
        $this->form_validation->set_rules('telefone','Telefone','required');
        if($this->form_validation->run()==FALSE) {
            $this->editar_cliente($id_cliente);
        }
        else{
            $cliente=elements(array('nome_cliente','telefone'), $this->input->post());
            if($this->input->post('senha')!='') {
                $cliente['senha']=md5($this->input->post('senha'));
            }
            $this->cliente->editar_cliente($cliente,$id_cliente);
            $this->session->set_flashdata('mensagem','Alteração Efetuada com Sucesso!!!');
            redirect('adm/clientes');
        }
    }
    
    /**
     * Faz a visualização de todas as informaçoes de um cliente pessoa física
     *
     * @param int $id_cliente ID único do cliente 
     * @return 
     */
    public function visualizar_cliente($id_cliente) {
        if(usuario_sessao_adm()['tipo_usuario']==4){
            $dados['query']=$this->cliente->buscar_informacoes($id_cliente, usuario_sessao_adm()['id_usuario']);
        }
        else{
            $dados['query']=$this->cliente->buscar_informacoes($id_cliente);
        }
        if($dados['query']==NULL){
            redirect('adm/clientes');
        }
        if($dados['query']['tipo_cliente']=="Pessoa Jurídica"){
            $this->_visualizar_cliente_juridico($id_cliente);
        }
        else{
            $dados['id_cliente']=$id_cliente;
            $id_pretendente_principal=$this->cliente->get_id_pretendente($id_cliente,1);
            $id_pretendente_secundario=$this->cliente->get_id_pretendente($id_cliente,0);
            if($id_pretendente_secundario){
               $dados['documentos_secundario']=$this->cliente->get_documentos($id_cliente,$id_pretendente_secundario['id_pretendente']);
               $dados['tem_documentos_secundario']= $this->cliente->verifica_se_tem_documemtos_rg_cnh($id_cliente,$id_pretendente_secundario['id_pretendente']);
               $dados['tem_comprovante_secundario']= $this->cliente->verifica_se_tem_comrprovante_residencia($id_cliente,$id_pretendente_secundario['id_pretendente']);
               $dados['tem_estado_civil_secundario']= $this->cliente->verifica_se_tem_estado_civil($id_cliente,$id_pretendente_secundario['id_pretendente']);
               $dados['tem_renda_secundario']= $this->cliente->verifica_se_tem_renda($id_cliente,$id_pretendente_secundario['id_pretendente']);
               $dados['tem_outros_documentos_secundario']= $this->cliente->verifica_se_tem_outros_documentos($id_cliente,$id_pretendente_secundario['id_pretendente']);
               $dados['tem_conjugue']= $this->cliente->verifica_se_segundo_pretendente_tem_conjugue($id_cliente);
               $dados['tem_documentos_conjugue']= $this->cliente->verifica_se_tem_documemtos_rg_cnh_conjugue($id_pretendente_secundario['id_pretendente']);
               $dados['documentos_conjugue']= $this->cliente->get_documentos_conjugue($id_pretendente_secundario['id_pretendente']);
               $dados['tem_renda_conjugue']= $this->cliente->verifica_se_tem_renda_conjugue($id_pretendente_secundario['id_pretendente']);
            }
            $dados['documentos']=$this->cliente->get_documentos($id_cliente,$id_pretendente_principal['id_pretendente']);
            $dados['tem_documentos']= $this->cliente->verifica_se_tem_documemtos_rg_cnh($id_cliente,$id_pretendente_principal['id_pretendente']);
            $dados['tem_comprovante']= $this->cliente->verifica_se_tem_comrprovante_residencia($id_cliente,$id_pretendente_principal['id_pretendente']);
            $dados['tem_estado_civil']= $this->cliente->verifica_se_tem_estado_civil($id_cliente,$id_pretendente_principal['id_pretendente']);
            $dados['tem_renda']= $this->cliente->verifica_se_tem_renda($id_cliente,$id_pretendente_principal['id_pretendente']);
            $dados['tem_documetacao_residentes']= $this->cliente->verifica_se_tem_documentos_residente($id_cliente);
            $dados['residentes']=$this->cliente->get_residentes($id_cliente);
            $dados['tem_outros_documentos']= $this->cliente->verifica_se_tem_outros_documentos($id_cliente,$id_pretendente_principal['id_pretendente']);

            /*======= BUSCA AS INFORMAÇÕES DAS FICHAS CADASTRAIS DOS CLIENTES =====*/
            $dados['tem_pretendente']=$this->cliente->verifica_se_tem_pretendente($id_cliente);
            $dados['tem_fiador']=$this->cliente->verifica_se_tem_fiador($id_cliente);
            $dados['segundo_pretendente']=$this->cliente->verifica_se_tem_segundo_pretendente($id_cliente);
            $dados['adicionou_seggundo_pretendente']=$this->cliente->verifica_se_adicionou_segundo_pretendente($id_cliente);
            $dados['query2']=$this->cliente->pega_dados_pretendente($id_cliente);
            $dados['enderecos']=$this->cliente->enderecos_pretendentes($id_cliente,$dados['query2']['pretendente_id']);
            $dados['residentes']=$this->cliente->pegar_residentes($id_cliente);
            $dados['propriedades']=$this->cliente->pegar_propriedades($id_cliente);
            $dados['referencias']=$this->cliente->pegar_referencias($id_cliente);
            $dados['referencias_bancarias']=$this->cliente->pegar_referencias_bancarias($id_cliente);
            $dados['pais']=$this->cliente->pegar_pais_pretendentes($id_cliente);
            $dados['query3']=$this->cliente->pega_dados_segundo_pretendente($id_cliente);
            $dados['query4']=$this->cliente->pega_dados_fiador($id_cliente);
            $dados['documentos_residentes']=$this->cliente->get_documentos_residentes($id_cliente);
            $id_fiador=$dados['query4']['id_fiador'];
            $dados['propriedades_fiador']=$this->cliente->pega_propriedades_fiador($id_fiador);
            $dados['referencias_fiador']=$this->cliente->pega_referencias_fiador($id_fiador);
            $dados['referencias_bancarias_fiador']=$this->cliente->pega_referencias_bancarias_fiador($id_fiador);

            $dados['tem_documentos_fiador']= $this->cliente->verifica_se_tem_documemtos_rg_cnh_fiador($id_cliente);
            $dados['documentos_fiador']=$this->cliente->get_documentos_fiadores($id_cliente);
            $dados['tem_comprovante_fiador']= $this->cliente->verifica_se_tem_comrprovante_residencia_fiador($id_cliente);
            $dados['tem_estado_civil_fiador']= $this->cliente->verifica_se_tem_estado_civil_fiador($id_cliente);
            $dados['tem_renda_fiador']= $this->cliente->verifica_se_tem_renda_fiador($id_cliente);
            $dados['tem_conjugue_fiador']= $this->cliente->verifica_se_fiador_tem_conjugue($id_cliente);
            $dados['tem_documentos_conjugue_fiador']= $this->cliente->verifica_se_tem_documemtos_rg_cnh_conjugue_fiador($id_fiador);
            $dados['documentos_conjugue_fiador']= $this->cliente->get_documentos_conjugue_fiador($id_fiador);
            $dados['tem_renda_conjugue_fiador']= $this->cliente->verifica_se_tem_renda_conjugue_fiador($id_fiador);
            /*=================================FIM================================*/

            
            $header["scripts"] = carregar_js();
            $header["styles"] = carregar_css();

            $this->load->view('template/header', $header);
            $this->load->view('adm/menu');
            $this->load->view('adm/cliente/visualizar_cliente',$dados);
            $this->load->view('template/footer');
            $this->load->view('adm_cliente/modais/modal_documentos');
            $this->load->view('adm_cliente/modais/modal_visualizar_endereco');
            $this->load->view('adm_cliente/modais/modal_visualizar_estado_civil');
            $this->load->view('adm/cliente/ficha_cadastro/modal_ficha_cadastro_pretendente');
            $this->load->view('adm/cliente/ficha_cadastro/modal_ficha_cadastro_segundo_pretendente');
            $this->load->view('adm/cliente/ficha_cadastro/modal_ficha_cadastro_fiador');
            $this->load->view('adm/cliente/ficha_cadastro/modal_documentos');
            $this->load->view('adm/cliente/ficha_cadastro/modal_visualizar_comprovante_renda');
            $this->load->view('adm/cliente/ficha_cadastro/modal_visualizar_endereco');
            $this->load->view('adm/cliente/ficha_cadastro/modal_visualizar_estado_civil');
            $this->load->view('adm_cliente/modais/modal_visualizar_comprovante_renda');
            $this->load->view('adm_cliente/modais/modal_visualizar_comprovante_residente');
            $this->load->view('adm_cliente/modais/modal_visualizar_outros_documentos');
            $this->load->view('adm/cliente/ficha_cadastro/modal_visualizar_outros_documentos');
            $this->load->view('adm_cliente/modais/modal_visualizar_documentos_conjugue');
            $this->load->view('adm_cliente/modais/modal_visualizar_comprovante_renda_conjugue');

            $this->load->view('adm/cliente/ficha_cadastro/modal_visualizar_documentos_fiador');
            $this->load->view('adm/cliente/ficha_cadastro/modal_visualizar_endereco_fiador');
            $this->load->view('adm/cliente/ficha_cadastro/modal_visualizar_estado_civil_fiador');
            $this->load->view('adm/cliente/ficha_cadastro/modal_visualizar_comprovante_renda_fiador');
            $this->load->view('adm/cliente/ficha_cadastro/modal_visualizar_documentos_conjugue_fiador');
            $this->load->view('adm/cliente/ficha_cadastro/modal_visualizar_comprovante_renda_conjugue_fiador');
        }
    }
    
    /**
     * Faz a visualização de todas as informaçoes de um cliente pessoa juridica
     *
     * @param int $id_cliente ID único do cliente 
     * @return 
     */
    public function _visualizar_cliente_juridico($id_cliente) {
        if(usuario_sessao_adm()['tipo_usuario']==4){
            $dados['query']=$this->cliente->buscar_informacoes($id_cliente, usuario_sessao_adm()['id_usuario']);
        }
        else{
            $dados['query']=$this->cliente->buscar_informacoes($id_cliente);
        }
        if($dados['query']==NULL){
            redirect('adm/clientes');
        }
        $dados['id_cliente']=$id_cliente;
        $dados['tem_ficha_cadastro']= $this->cliente->verifica_se_tem_dados_juridico($id_cliente);
        $dados['query2']= $this->cliente->get_cliente_jurudico($id_cliente);
        $dados['socios']= $this->cliente->get_socios($id_cliente);
        $dados['propriedades']=$this->cliente->pegar_propriedades($id_cliente);
        $dados['referencias']=$this->cliente->pegar_referencias_comerciais($id_cliente);
        $dados['referencias_bancarias']=$this->cliente->pegar_referencias_bancarias($id_cliente);
        $dados['documentos']=$this->cliente->get_documentos_juridico($id_cliente);
        $dados['documentos_socios']=$this->cliente->get_documentos_socios($id_cliente);
        $dados['tem_contrato_social']=$this->cliente->verifica_se_tem_contrato_social($id_cliente);
        $dados['tem_alteracao_contrato_social']=$this->cliente->verifica_se_tem_alteracao_contrato_social($id_cliente);
        $dados['tem_balanco']=$this->cliente->verifica_se_tem_balanco($id_cliente);
        $dados['tem_balancete']=$this->cliente->verifica_se_tem_balancete($id_cliente);
        $dados['tem_cartao']=$this->cliente->verifica_se_tem_cartao_cnpj($id_cliente);
        $dados['tem_imposto_renda']=$this->cliente->verifica_se_tem_imposto_renda($id_cliente);
        $dados['tem_certidao_negativa']=$this->cliente->verifica_se_tem_certidao_negativa($id_cliente);
        $dados['tem_outros_documentos']=$this->cliente->verifica_se_tem_outros_documentos_juridico($id_cliente);
        $dados['tem_rg_cnh']=$this->cliente->verifica_se_tem_rg_cnh($id_cliente);
        $dados['tem_comprovante_residencia']=$this->cliente->verifica_se_tem_comprovante_residencia($id_cliente);       
        
        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $this->load->view('adm/menu');
        $this->load->view('adm/cliente/visualizar_cliente_juridico',$dados);
        $this->load->view('template/footer');
        $this->load->view('adm/cliente/ficha_cadastro/modal_ficha_cadastro_juridico');
        $this->load->view('adm_cliente/juridico/modais/modal_visualizar_contrato_social');
        $this->load->view('adm_cliente/juridico/modais/modal_visualizar_alteracao_contrato_social');
        $this->load->view('adm_cliente/juridico/modais/modal_visualizar_balanco');
        $this->load->view('adm_cliente/juridico/modais/modal_visualizar_balancete');
        $this->load->view('adm_cliente/juridico/modais/modal_visualizar_cartao_cnpj');
        $this->load->view('adm_cliente/juridico/modais/modal_visualizar_imposto_renda');
        $this->load->view('adm_cliente/juridico/modais/modal_visualizar_certidao_negativa');
        $this->load->view('adm_cliente/juridico/modais/modal_visualizar_rg_cpf_cnh');
        $this->load->view('adm_cliente/juridico/modais/modal_visualizar_endereco');
        $this->load->view('adm_cliente/juridico/modais/modal_visualizar_outros_documentos');
    }
    
    /**
     * Faz a exclusão do cliente
     *
     * @param int $id_cliente ID único do cliente 
     * @return 
     */
    public function excluir_cliente($id_cliente) {
        if(usuario_sessao_adm()['tipo_usuario']==4){
            $dados['query']=$this->cliente->buscar_informacoes($id_cliente, usuario_sessao_adm()['id_usuario']);
        }
        else{
            $dados['query']=$this->cliente->buscar_informacoes($id_cliente);
        }
        if($dados['query']==NULL){
            redirect('adm/clientes');
        }
        $dados['corretores']=$this->cliente->listar_corretores();
        $dados['tipo_usuario']=$this->cliente->listar_tipo_usuario();
        $dados['query']['id_cliente']=$id_cliente;
       
        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $this->load->view('adm/menu');
        $this->load->view('adm/cliente/excluir_cliente',$dados);
        $this->load->view('template/footer');
    }
    
    /**
     * Faz a validaçao da inserção de um cliente
     *
     * @param 
     * @return 
     */
    public function validacao() {
        $this->form_validation->set_rules('nome_cliente','Nome','required');
        $this->form_validation->set_rules('email','E-mail','required|valid_email|is_unique[tb_clientes.email]');
        $this->form_validation->set_message('is_unique','Esse E-mail Já Está Cadastrado');
        $this->form_validation->set_rules('telefone','Telefone','required');
        $this->form_validation->set_rules('tipo_cliente','Tipo de Cliente','required');
        $this->form_validation->set_rules('id_tipo_cliente','Tipo de Usuário','required');
        $this->form_validation->set_rules('id_corretor','Colaborador','required');
        $this->form_validation->set_rules('senha','Senha','required');
        $this->form_validation->set_rules('r_senha','Repetir Senha','required|matches[senha]');
        if($this->form_validation->run()==FALSE){
            $this->adicionar_cliente();
        }
        else{
            $dados= elements(array('nome_cliente','email','id_tipo_cliente','senha','id_corretor','tipo_cliente','telefone'), $this->input->post());
            $dados['senha']= md5($dados['senha']);
            $dados['ativo']=1;
            $this->cliente->adicionar_cliente($dados);
            $this->session->set_flashdata('mensagem','Cliente Adicionado com Sucesso!!!');
            redirect('adm/clientes');
        }
    }
    
    /**
     * Faz a validaçao da edição de um cliente
     *
     * @param int $id_cliente ID único do cliente 
     * @return 
     */
    public function validacao_editar_cliente($id_cliente) {
        $this->form_validation->set_rules('nome_cliente','Nome','required');
        $this->form_validation->set_rules('id_corretor','Colaborador','required');
        $this->form_validation->set_rules('telefone','Telefone','required');
        if($this->form_validation->run()==FALSE){
            $this->editar_cliente($id_cliente);
        }
        else{
            $cliente=elements(array('nome_cliente','id_corretor','telefone'), $this->input->post());
            if($this->input->post('senha')!=''){
                $cliente['senha']=md5($this->input->post('senha'));
            }
            $this->cliente->editar_cliente($cliente,$id_cliente);
            $this->session->set_flashdata('mensagem','Alteração Efetuada com Sucesso!!!');
            redirect('adm/clientes');
        }
    }
    
    /**
     * Muda o status do cliente para inativo
     *
     * @param int $id_cliente ID único do cliente 
     * @return 
     */
    public function validacao_excluir_cliente($id_cliente) {
        $this->cliente->editar_cliente(array('ativo'=>0),$id_cliente);
        $this->session->set_flashdata('mensagem','Cliente Excluido com Sucesso!!!');
        redirect('adm/clientes');
    }
    
    /**
     * Faz a paginação da listagem de clientes
     *
     * @param String $busca valor digitado no campo de busca de usuário 
     * @return 
     */
    public function _paginacao($busca=NULL) {
            $id_corretor=NULL;
            if(usuario_sessao_adm()['tipo_usuario']==4){
                $id_corretor=(usuario_sessao_adm()['id_usuario']);
            }
            $config['full_tag_open'] = "<nav><ul class='pagination'>";
            $config['full_tag_close'] ="</ul></nav>";
            $config['num_tag_open'] = "<li class='page-item'>";
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = "<li class='page-item active'><a class='page-link' href='#'>";
            $config['cur_tag_close'] = "</a></li>";
            $config['next_tag_open'] = "<li class='page-item'>";
            $config['next_tagl_close'] = "</li>";
            $config['prev_tag_open'] = "<li class='page-item'>";
            $config['prev_tagl_close'] = "</li>";
            $config['first_tag_open'] = "<li class='page-item'>";
            $config['first_tagl_close'] = "</li>";
            $config['last_tag_open'] = "<li class='page-item'>";
            $config['last_tagl_close'] = "</li>";
            $config['next_link'] = "Próximo";
            $config['prev_link'] = "Anterior";
            $config['attributes'] = array('class' => 'page-link');
            $config['base_url'] = base_url() . 'adm/clientes/paginacao/';
            $config['total_rows'] = $this->cliente->listar_usuarios(0, 0, NULL, $busca)->num_rows();
            if($busca==NULL) {
                $config['per_page'] = 10;
            } else {
                $config['per_page'] = 100;
            }
            $config['uri_segment'] = 4;
            $qtde = $config['per_page'];
            $inicio = $this->uri->segment(4);
            $this->pagination->initialize($config);
            $dados['paginacao']  = $this->pagination->create_links();
            $dados['lista_usuarios'] = $this->cliente->listar_usuarios($qtde,$inicio,$id_corretor, $busca)->result();
            return $dados;
    }
}
