<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginClienteController extends CI_Controller {

     public function __construct() {
        parent::__construct();
        if(usuario_sessao_cliente()) {
            redirect('gerenciador');
        }
        $this->load->model('LoginModel','login');
    }
    
    public function index(){
        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $this->load->view('template/menu');
        $this->load->view('login/login_cliente');
        $this->load->view('template/footer');
    }
    
    public function validacao() {
        $this->form_validation->set_rules('email','E-mail','required|valid_email');
        $this->form_validation->set_rules('senha','Senha','trim|callback__checar_usuario|required');
        if($this->form_validation->run() == FALSE) {
            $this->index();
        }
        else {
            redirect('gerenciador');
        }
    }
    
    function _checar_usuario($senha) {
        $email = $this->input->post('email');
        $resultado = $this->login->buscar_usuario_cliente($email, md5($senha));
        if($resultado) {
            $array_usuario = array(
                'id_cliente'=>$resultado->id_cliente,
                'nome_cliente'=> $resultado->nome_cliente,
                'email'=> $resultado->email,
                'tipo_cliente'=>$resultado->id_tipo_cliente,
                'cliente'=>$resultado->tipo_cliente,
                'id_corretor'=>$resultado->id_corretor
            );
            $this->session->set_userdata('cliente', $array_usuario);
            return TRUE;
        }
        else{
            $this->form_validation->set_message('_checar_usuario', 'E-mail e/ou Senha inválidos ou Cliente Inativo');
            return FALSE;
        }
    }
}
