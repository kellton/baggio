<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginController extends CI_Controller {

     public function __construct() {
        parent::__construct();
        if(usuario_sessao_adm()) {
            redirect('gerenciador');
        }
        $this->load->model('LoginModel','login');
    }
    
    public function index() {
        $header["scripts"] = carregar_js();
        $header["styles"] = carregar_css();

        $this->load->view('template/header', $header);
        $this->load->view('template/menu');
        $this->load->view('login/login');
        $this->load->view('template/footer');
    }
    
    public function validacao() {
        $this->form_validation->set_rules('email','E-mail','required|valid_email');
        $this->form_validation->set_rules('senha','Senha','callback__checar_usuario|required');
        if($this->form_validation->run() == FALSE) {
            $this->index();
        }
        else {
            redirect('gerenciador');
        }
    }
    
    function _checar_usuario($senha) {
        $email = $this->input->post('email');
        $resultado = $this->login->buscar_usuario($email, md5($senha));
        if($resultado) {
            $array_usuario = array(
                'id_usuario'=>$resultado->id_usuario,
                'nome_usuario'=> $resultado->nome_usuario,
                'email'=> $resultado->email,
                'tipo_usuario'=>$resultado->id_tipo_usuario
            );
            $this->session->set_userdata('usuario', $array_usuario);
            return TRUE;
        }
        else{
            $this->form_validation->set_message('_checar_usuario', 'E-mail e/ou Senha inválidos');
            return FALSE;
        }
    }
}
