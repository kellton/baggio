<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * APIABERTACONTROLLER
 * -------------------------------------------------------------------
 * DESCRICAO : 
 * -------------------------------------------------------------------
 * CARREGA : 
 * - N/A
 */
class ApiAbertaController extends CI_Controller {

    /*
     * CONSTRUCT
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function __construct() {
        parent::__construct();

        $this->load->model('ImovelModel','m_imoveis');
        $this->load->model('EnderecoModel','m_enderecos');
        $this->load->model('TipoModel','m_tipos');
    }
    
    /*
     * INDEX
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function index() {

    }

    /* ************************************************************* */
    /* ******************** CONSULTAS DE IMOVEIS ******************* */
    /* ************************************************************* */
    /*
     * ASYNC_BUSCAR_REFERENCIAS
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function async_buscar_referencias() {
        $dados = array();

        $dados = $this->_preparar_json_referencias( $this->m_imoveis->listar_referencias_com_imovel() );

        header('Content-Type: application/json');

        echo json_encode($dados);
    }

    /*
     * async_buscar_condominios
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function async_buscar_condominios() {
        $dados = array();

        $dados = $this->_preparar_json_condominios( $this->m_imoveis->listar_condominios_com_imovel() );

        header('Content-Type: application/json');

        echo json_encode($dados);
    }

    /*
     * ASYNC_BUSCAR_INFORMACOES
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function async_buscar_informacoes() {
        $id_negociacao = $this->input->post("param_resultado_id_negociacao");
        $id_tipo = $this->input->post("param_resultado_id_tipo");
        $local = $this->input->post("param_resultado_local");
        $inicio = $this->input->post("param_resultado_inicio");

        $dados = array();

        if ($id_negociacao == 1) {
            $areas = $this->m_imoveis->buscar_menor_maior_area_venda();
            $precos = $this->m_imoveis->buscar_menor_maior_preco_venda();
        } else if ($id_negociacao == 2) {
            $areas = $this->m_imoveis->buscar_menor_maior_area_locacao();
            $precos = $this->m_imoveis->buscar_menor_maior_preco_locacao();
        }

        $dados["menor_preco"] = number_format( $precos[0]["menor"], 0, ",", ".");
        $dados["maior_preco"] = number_format( $precos[0]["maior"], 0, ",", ".");
        $dados["menor_area"] = number_format( $areas[0]["menor"], 0, ",", ".");
        $dados["maior_area"] = number_format( $areas[0]["maior"], 0, ",", ".");

        header('Content-Type: application/json');

        echo json_encode($dados);
    }

    /*
     * ASYNC_BUSCAR_IMOVEIS
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function async_buscar_imoveis() {
        /* BUSCA */
        $id_negociacao = 0;
        $id_tipo_busca = $this->input->post("param_tipo_busca");
        $inicio = $this->input->post("param_inicio");
        $tipos_imoveis = array();
        $cidade = NULL;
        $bairros = array();
        $referencia = NULL;
        $condominio = NULL;
        $tipos_condominios = array();
        /* POR BAIRRO */
        if ($id_tipo_busca == 1) {
            $id_negociacao = $this->input->post("param_negociacao");
            $tipos_imoveis = $this->input->post("param_tipos");
            $cidade = $this->input->post("param_cidade");
            $bairros = $this->input->post("param_bairros");

            if ($bairros == NULL || empty($bairros)) {
                $bairros = recuperar_sessao_bairros();
            }
        /* POR REFERENCIA */
        } else if ($id_tipo_busca == 2) {
            $referencia = $this->input->post("param_referencia");
        /* POR CONDOMINIO */
        } else if ($id_tipo_busca == 3) {
            $condominio = $this->input->post("param_condominio");
            $tipos_condominios = $this->input->post("param_tipos_condominios");
        }
        /* FILTROS */
        $menor_preco = $this->input->post("param_menor_preco");
        $maior_preco = $this->input->post("param_maior_preco");
        $qtd_quartos = $this->input->post("param_qtd_quartos");
        $qtd_banheiros = $this->input->post("param_qtd_banheiros");
        $qtd_vagas = $this->input->post("param_qtd_vagas");
        $menor_area = $this->input->post("param_menor_area");
        $maior_area = $this->input->post("param_maior_area");
        $ordenacao = $this->input->post("param_ordenacao");

        if (strpos($maior_preco, '+') !== FALSE) {
            $maior_preco = 0;
        }

        if (strpos($maior_area, '+') !== FALSE) {
            $maior_area = 0;
        }
        /* SALVA OS PARAMETROS NA SESSAO */
        $this->_salvar_sessao_busca_imoveis(
            $id_tipo_busca, $id_negociacao, 
            $tipos_imoveis, $inicio,
            $cidade, $bairros, 
            $referencia, $condominio, $tipos_condominios, 
            $ordenacao,
            $menor_preco, $maior_preco, 
            $qtd_quartos, $qtd_banheiros, $qtd_vagas, 
            $menor_area, $maior_area
        );
        /* BUSCA OS DADOS */
        $dados_resultado = $this->_preparar_imoveis(
             $id_negociacao, $tipos_imoveis, $inicio,
             $cidade, $bairros, 
             $referencia, $condominio, $tipos_condominios, 
             $ordenacao,
             $menor_preco, $maior_preco, 
             $qtd_quartos, $qtd_banheiros, $qtd_vagas, 
             $menor_area, $maior_area
        );

        $resposta = array();
        $resposta["dados"] = array();
        $resposta["qtd_imoveis"] = $dados_resultado["qtd_imoveis"];

        $resposta["dados"] = $this->load->view('api/busca/resultado-imoveis', $dados_resultado, TRUE);

        header('Content-Type: application/json');

        echo json_encode($resposta);
    }

     /*
     * ASYNC_COPIAR_BUSCA
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function async_copiar_busca() {
        $busca = $this->session->userdata("params_busca");
        $busca = base64_encode($busca);
        $busca = str_replace("=", "_", $busca);

        echo base_url("buscar/url/imoveis"."/".$busca);
    }

    /*
     * ASYNC_BUSCAR_BAIRROS
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function async_buscar_bairros() {
        $id_cidade = $this->input->post("param_id_cidade");

        $resposta = array();
        $resposta["bairros"] = array();

        if (isset($id_cidade) && $id_cidade != NULL) {
            $bairros_selecionados = array();
            $ids_bairros_selecionados = recuperar_sessao_bairros();

            $bairros = $this->m_enderecos->buscar_bairros_por_cidade($id_cidade);

            foreach ($bairros as $bairro) {
                $bairro["selecionado"] = in_array($bairro["id_bairro"], $ids_bairros_selecionados);

                array_push($bairros_selecionados, $bairro);
            }

            if (!empty($bairros_selecionados)) {
                $resposta["bairros"] = $bairros_selecionados;
            }
        }

        header('Content-Type: application/json');

        echo json_encode($resposta);
    }

    /* ************************************************************* */
    /* ********************** FUNCOES PRIVADAS ********************* */
    /* ************************************************************* */
    /*
     * _PREPARAR_JSON_BAIRROS
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    private function _preparar_json_bairros($bairros=array()) {
        $json_bairros = array();

        foreach ($bairros as $indice => $bairro) {
            array_push($json_bairros, $bairro["nome_bairro"]);
        }
        return $json_bairros;
    }

    /*
     * _PREPARAR_JSON_CIDADES
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    private function _preparar_json_cidades($cidades=array()) {
        $json_cidades = array();

        foreach ($cidades as $indice => $cidade) {
            array_push($json_cidades, $cidade["nome_cidade"]);
        }
        return $json_cidades;
    }

    /*
     * _PREPARAR_JSON_REFERENCIAS
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    private function _preparar_json_referencias($referencias=array()) {
        $json_referencias = array();

        foreach ($referencias as $indice => $referencia) {
            $partes_referencia = explode("-", $referencia["referencia"]);

            array_push($json_referencias, $partes_referencia[0]);
        }
        return $json_referencias;
    }

    /*
     * _PREPARAR_JSON_CONDOMINIOS
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    private function _preparar_json_condominios($condominios=array()) {
        $json_condominios = array();

        foreach ($condominios as $indice => $condominio) {
            array_push($json_condominios, $condominio["nome_condominio"]);
        }
        return $json_condominios;
    }

    /*
     * _PREPARAR_IMOVEIS
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    private function _preparar_imoveis(
        $id_negociacao=0, $tipos_imoveis=array(), $inicio=0,

        $cidade=1, $bairros=array(),
        $referencia=NULL,
        $condominio=NULL, $tipos_condominios=array(), 

        $ordenacao=NULL,

        $menor_preco=0, $maior_preco=0,
        $qtd_quartos=0, $qtd_banheiros=0, $qtd_vagas=0,
        $menor_area=0, $maior_area=0) 
    {
        $resultado = array();
        $imoveis_completos = array();
        /* NECESSARIO VERFICAR COMO FICARA A QUESTAO DO COOKIE */
        $ids_nao_gostei = values_only_from_resultarray(
            "id_imovel", 
            $this->m_imoveis->busca_ids_imoveis_nao_gostei()
        );

        $imoveis = $this->m_imoveis->buscar_imoveis_filtros(
            $id_negociacao, $tipos_imoveis, CONS_BAGGIO_BUSCA_QTD, $inicio * CONS_BAGGIO_BUSCA_QTD,
            
            $cidade, $bairros, 
            remover_acentos($referencia), remover_acentos($condominio), $tipos_condominios, 

            $ordenacao,

            $menor_preco, $maior_preco, $qtd_quartos, $qtd_banheiros, $qtd_vagas, $menor_area, $maior_area,

            $ids_nao_gostei
        );

        // to_debug($this->db->last_query());

        foreach ($imoveis as $imovel) {
            $imagem = $this->m_imoveis->buscar_imagem_imovel($imovel["id_imovel"]);
            $tipos = $this->m_imoveis->buscar_todos_tipos_imovel($imovel["id_imovel"]);

            if (!empty($imagem)) {
                $imovel["link_imagem"] = $imagem[0]["link_imagem"];
                $imovel["link_miniatura"] = $imagem[0]["link_miniatura"];
            }

            $imovel["tipos_imovel"] = array();

            foreach ($tipos as $indice => $tipo) {
                 array_push($imovel["tipos_imovel"], $tipo);
            }
            array_push($imoveis_completos, $imovel);
        }
        $resultado["imoveis"] = $imoveis_completos;

        $resultado["qtd_imoveis"] = $this->m_imoveis->quantificar_imoveis_busca(
            $id_negociacao, $tipos_imoveis, $cidade, $bairros, 
            remover_acentos($referencia), remover_acentos($condominio), $tipos_condominios, 
            $menor_preco, $maior_preco, $qtd_quartos, $qtd_banheiros, $qtd_vagas, $menor_area, $maior_area
        );

        // to_debug($this->db->last_query());

        return $resultado;
    }

    /*
     * _SALVAR_SESSAO_BUSCA_IMOVEIS
     *
     * @param   
     * 
     * @return  array
    */
    function _salvar_sessao_busca_imoveis(
        $id_tipo_busca=1,
        $id_negociacao=0, 
        $tipos_imoveis=array(), $inicio=0,
        $cidade=1, $bairros=array(),
        $referencia=NULL, 
        $condominio=NULL, $tipos_condominios=array(),
        $ordenacao=NULL,
        $menor_preco=0, $maior_preco=0,
        $qtd_quartos=0, $qtd_banheiros=0, $qtd_vagas=0,
        $menor_area=0, $maior_area=0) 
    {
        $busca = array();
        $busca["id_tipo_busca"] = $id_tipo_busca;
        $busca["id_negociacao"] = $id_negociacao;
        $busca["tipos_imoveis"] = $tipos_imoveis;
        $busca["inicio"] = $inicio;
        $busca["cidade"] = $cidade;
        $busca["bairros"] = $bairros;
        $busca["referencia"] = $referencia;
        $busca["condominio"] = $condominio;
        $busca["tipos_condominios"] = $tipos_condominios;
        $busca["ordenacao"] = $ordenacao;
        $busca["menor_preco"] = $menor_preco;
        $busca["maior_preco"] = $maior_preco;
        $busca["qtd_quartos"] = $qtd_quartos;
        $busca["qtd_banheiros"] = $qtd_banheiros;
        $busca["qtd_vagas"] = $qtd_vagas;
        $busca["menor_area"] = $menor_area;
        $busca["maior_area"] = $maior_area;

        $this->session->set_userdata('params_busca', json_encode($busca));
    }

}
