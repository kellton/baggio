<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * APICONTROLLER
 * -------------------------------------------------------------------
 * DESCRICAO : 
 * -------------------------------------------------------------------
 * CARREGA : 
 * - N/A
 */
class ApiController extends CI_Controller {

    /*
     * CONSTRUCT
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function __construct() {
        parent::__construct();

        if(!usuario_sessao_adm() && !usuario_sessao_cliente()) {
            $this->session->set_flashdata('param_msg_erro_cabecalho', 'Sem Permissão');
            $this->session->set_flashdata('param_msg_erro', 'Você não tem permissão de acessar esta área!');

            redirect(base_url());
        }

        $this->load->model('ImovelModel','m_imoveis');
        $this->load->model('EnderecoModel','m_enderecos');
        $this->load->model('UsuarioModel','m_usuarios');
    }
    
    /*
     * INDEX
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function index() {
        if(!usuario_sessao_adm() && !usuario_sessao_cliente()) {
            $this->session->set_flashdata('param_msg_erro_cabecalho', 'Sem Permissão');
            $this->session->set_flashdata('param_msg_erro', 'Você não tem permissão de acessar esta área!');

            redirect(base_url());
        }
    }

    /* ************************************************************* */
    /* ******************* CONSULTAS DE USUARIOS ******************* */
    /* ************************************************************* */
    /*
     * BUSCAR_PROPRIETARIOS
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function buscar_proprietarios() {
        if(!usuario_sessao_adm()) {
            $this->session->set_flashdata('param_msg_erro_cabecalho', 'Sem Permissão');
            $this->session->set_flashdata('param_msg_erro', 'Você não tem permissão de acessar esta área!');

            redirect(base_url());
        }

        $array_proprietarios = $this->m_usuarios->listar_proprietarios_nome(TRUE);

        header('Content-Type: application/json');

        echo json_encode($array_proprietarios);
    }
    
}
