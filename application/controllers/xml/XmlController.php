<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Carbon\Carbon;

/**
 * XMLCONTROLLER
 * -------------------------------------------------------------------
 * DESCRICAO : 
 * -------------------------------------------------------------------
 * CARREGA : 
 * - N/A
 */
class XmlController extends CI_Controller {

    /*
     * CONSTRUCT
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function __construct() {
        parent::__construct();

        $this->load->model('ImovelModel','m_imoveis');
        $this->load->model('ImobiliariaModel','m_imobiliarias');
        $this->load->model('TipoModel','m_tipos');
        $this->load->model('EnderecoModel','m_enderecos');
        $this->load->model('ImagemModel','m_imagens');
        $this->load->model('NegociacaoModel','m_negociacoes');
        $this->load->model('MaterialApoioModel','m_materiais');
        $this->load->model('XmlModel','m_xmls');
    }
    
    /*
     * INDEX
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function index() {
    }

    /* ************************************************************* */
    /* *************************** FUNCOES ************************* */
    /* ************************************************************* */
    /*
     * TESTE_CRON_JOB
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function teste_cron_job() {
        date_default_timezone_set(CONS_BAGGIO_SYSTEM_TIMEZONE);

        // echo data_hoje_sql() . " >> Testado";

        log_message('info', "Teste Baggio Cron Job - " . data_hoje_sql() . ";");
    }

    /*
     * VERIFICAR_ATUALIZACOES
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function verificar_atualizacoes() {
        $jobs = $this->m_imobiliarias->listar_jobs_ativos();

        if (!empty($jobs)) {
            date_default_timezone_set(CONS_BAGGIO_SYSTEM_TIMEZONE);

            // echo data_hoje(TRUE) . " >> INICIO DO PROCESSO DE ATUALIZACAO;";
            log_message('error', "INICIO DO PROCESSO DE ATUALIZACAO >> " . data_hoje(TRUE) . ";");

            foreach ($jobs as $job) {
                $horarios = explode(";", $job["horarios_atualizacao"]);

                // verificar se a ultima atualizacao que foi feita eh mais nova que o horario do servidor
                // verificar o primeiro horario disponivel e se o tempo do servidor esta nesta hora

                foreach ($horarios as $horario) {
                    $data_sistema = Carbon::now();
                    $data_horario = from_sql_to_carbon( data_hoje_sql(FALSE) . " " . $horario );

                    if ($data_sistema->diffInSeconds($data_horario, false) < 0) {
                        // echo "Comparando " . $data_sistema->toDateTimeString() . " vs " . $data_horario->toDateTimeString() . " = " . $data_sistema->diffInSeconds($data_horario, false);
                        // echo " | ";
                        // echo  $job["codigo"] . " passou do horario." . PHP_EOL;
                        $this->atualizar($job["codigo"]);

                        break;
                    }
                }
            }
            // echo data_hoje(TRUE) . " >> TERMINO DO PROCESSO DE ATUALIZACAO;";
            log_message('error', "TERMINO DO PROCESSO DE ATUALIZACAO >> " . data_hoje(TRUE) . ";");
        }
    }

    /*
     * ATUALIZAR_IMOBILIARIA
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function atualizar_imobiliaria() {
        date_default_timezone_set(CONS_BAGGIO_SYSTEM_TIMEZONE);

        $tempo_inicio = "";
        $tempo_fim = "";
        $url = "adm/rodar/job";
        $codigo = $this->uri->segment(calcular_uri_paginacao($url));

        if(!usuario_sessao_adm()) {
            $this->session->set_flashdata('param_msg_erro_cabecalho', 'Sem Permissão');
            $this->session->set_flashdata('param_msg_erro', 'Você não tem permissão de acessar esta área!');

            redirect(base_url());
        }

        if ($codigo) {
            $tempo_inicio = date('H:i:s');
            $tempo_fim = date("H:i:s");

            if ($this->atualizar($codigo)) {
                $tempo_fim = date("H:i:s");

                $date_inicio = date_create($tempo_inicio);
                $date_fim = date_create($tempo_fim);

                $intervalo = date_diff($date_inicio, $date_fim);

                $horario_mensagem = "";
                $horario_mensagem .= ( $intervalo->h > 0 ? ( $intervalo->h . " horas " ) : "" );
                $horario_mensagem .= ( $intervalo->i > 0 ? ( $intervalo->i . " minutos " ) : "" );
                $horario_mensagem .= ( $intervalo->s > 0 ? ( $intervalo->s . " segundos ") : "" );
                $horario_mensagem .= ".";

                $this->session->set_flashdata('param_msg_sem_tempo', TRUE);
                $this->session->set_flashdata('param_msg_sucesso', "O XML foi atualizado com sucesso. Operação realizada em $horario_mensagem");
            } else {
                $this->session->set_flashdata('param_msg_sem_tempo', TRUE);
                $this->session->set_flashdata('param_msg_erro', 'Ocorreu um erro ao atualizar o XML. Verifique se a imobiliária está ativa na Rede Imóveis ou se o job está ativo no sistema.');
            }
            redirect(base_url('adm/gerenciar/jobs'));
        }
    }

    /*
     * ATUALIZAR
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    public function atualizar($codigo_imobiliaria=19) {
        try {
            date_default_timezone_set(CONS_BAGGIO_SYSTEM_TIMEZONE);

            set_time_limit(CONS_BAGGIO_MAX_TIME_POR_XML);

            $job = $this->m_imobiliarias->buscar_job_imobiliaria($codigo_imobiliaria);

            if (!empty($job)) {
                $id_imobiliaria = $job["id_imobiliaria"];

                if ($job["ativo"] == TRUE) {

                    $xml = simplexml_load_file($job["url"], NULL, LIBXML_NOCDATA );
                    
                    if ($this->_verificar_xml($xml)) {
                        $this->_excluir_imoveis($id_imobiliaria);

                        foreach ($xml->Imovel as $indice => $xml_imovel) {
                            $id_imovel = strval($xml_imovel->id_imovel);
                            $referencia = strval($xml_imovel->referencia);

                            if (!$this->m_imoveis->existe_imovel($id_imovel, $referencia, $id_imobiliaria)) {
                                if (!$this->_inserir_imovel($xml_imovel, $id_imobiliaria)) {
                                    return FALSE;
                                }
                            }
                        }
                        $this->m_imobiliarias->atualizar_job($job["id_jobs_xml"], data_hoje_sql());

                        return TRUE;
                    }
                }
            }
        } catch (Exception $e) {
            log_message('error', 'ERRO XML - ATUALIZAR - ' . $e->getMessage());

            return FALSE;
        }
        return FALSE;
    }

    /* ************************************************************* */
    /* ************************** PRIVATES ************************* */
    /* ************************************************************* */
    /*
     * _EXCLUIR_IMOVEIS
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    private function _excluir_imoveis($id_imobiliaria=0) {
        if ($id_imobiliaria > 0) {
            $this->m_imoveis->excluir_imoveis_imobiliaria($id_imobiliaria);
        }
    }

    /*
     * _EXCLUIR_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    private function _excluir_imovel($id_imovel=0, $referencia=NULL, $id_imobiliaria=0) {
        if ($id_imovel > 0 && $referencia && $id_imobiliaria > 0) {
            $this->db->trans_start();

            /* NEGOCIACAO */
            $id_negociacao = $this->m_negociacoes->excluir_negociacao_imovel($id_imovel);
            /* MATERIAL DE APOIO */
            $this->m_materiais->excluir_material($id_negociacao);
            /* VALORES */
            $id_valores_imovel = $this->m_imoveis->excluir_valores_imovel($id_imovel);
            
            /* LAT LONG */
            $this->m_enderecos->excluir_lat_long_imovel($id_imovel);
            /* COMPLEMENTO */
            $this->m_enderecos->excluir_complemento_imovel($id_imovel);
            /* CIDADE - OUTROS IMOVEIS VAO USAR */
            //$this->m_enderecos->excluir_cidade_imovel($id_imovel);
            /* BAIRRO - OUTROS IMOVEIS VAO USAR */
            //$this->m_enderecos->excluir_bairro_imovel($id_imovel);
            /* ENDERECO */
            $this->m_enderecos->excluir_endereco_imovel($id_imovel);
            
            /* CHAVES */
            $this->m_imoveis->excluir_chaves_imovel($id_imovel);
            /* IMAGENS */
            $this->m_imagens->excluir_imagens_imovel($id_imovel);
            /* ESPECIFICACOES */
            $this->m_imoveis->excluir_especificacoes_imovel($id_imovel);
            /* INFORMACOES_COMPLEMENTARES */
            $this->m_imoveis->excluir_informacoes_complementares_imovel($id_imovel);
            /* INFRAESTRUTURA */
            $this->m_imoveis->excluir_infraestrutura_imovel($id_imovel);
            /* VEICULACAO */
            $this->m_imoveis->excluir_veiculacao_imovel($id_imovel);
            /* INFORMACOES_XML */
            $this->m_imoveis->excluir_informacoes_xml_imovel($id_imovel);

            /* IMOVEL */
            $this->m_imoveis->excluir_imovel($id_imovel, $referencia, $id_imobiliaria);

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }

        }
    }

    /*
     * _INSERIR_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    private function _inserir_imovel($dados_xml=array(), $id_imobiliaria=0) {
        try{
            $dados = array();

            if (!$this->_verificar_xml_imovel($dados_xml)) {
                return FALSE;
            }

            $this->db->trans_start();

            /* INSERCOES ANTES A DE IMOVEL */
            $dados["id_especificacoes"] = $this->m_imoveis->inserir_especificacoes($this->_modelar_especificacoes($dados_xml));
            $dados["id_informacoes_complementares"] = $this->m_imoveis->inserir_informacoes_complementares($this->_modelar_informacoes_complementares($dados_xml));
            $dados["id_infraestrutura"] = $this->m_imoveis->inserir_infraestrutura($this->_modelar_infraestrutura($dados_xml));
            $dados["id_veiculacao"] = $this->m_imoveis->inserir_veiculacao($this->_modelar_veiculacao($dados_xml));
            $dados["id_informacoes_xml"] = $this->m_xmls->inserir_informacoes_xml($this->_modelar_informacoes_xml($dados_xml));

            $this->m_imoveis->inserir_imovel($this->_modelar_imovel($dados_xml, $dados, $id_imobiliaria));

            $this->_preparar_imovel_sub_tipo($dados_xml);

            $dados["id_bairro"] = $this->_preparar_bairro($dados_xml);
            $dados["id_cidade"] = $this->_preparar_cidade($dados_xml);
            $dados["id_complemento"] = $this->m_enderecos->inserir_complemento($this->_modelar_complemento($dados_xml));
            $dados["id_lat_long"] = $this->m_enderecos->inserir_lat_long($this->_modelar_lat_long($dados_xml));

            $this->db->insert("tb_enderecos", $this->_modelar_endereco($dados_xml, $dados));

            $this->_preparar_fotos($dados_xml);

            $dados["id_valores_imovel"] = $this->m_negociacoes->inserir_valores($this->_modelar_valores($dados_xml));

            $this->_preparar_chaves($dados_xml);

            $this->m_negociacoes->inserir_negociacao($this->_modelar_negociacao($dados_xml, $dados));

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();

                return TRUE;
            }
        } catch (Exception $e) {
            $this->db->trans_rollback();
        }
        return FALSE;
    }

    /*
     * _PREPARAR_IMOVEL_SUB_TIPO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - 
    */
    private function _preparar_imovel_sub_tipo($dados_xml=array()) {
        if (!empty($dados_xml) && !empty($dados_xml->tipo_imovel)) {
            $dados = array();
            $sub_tipos = array();
            $id_imovel = strval($dados_xml->id_imovel);
            $xml_sub_tipos = explode("/", remover_acentos( strval($dados_xml->tipo_imovel) ));

            $id_uso_tipo = $this->m_tipos->buscar_id_tipo_imovel(strval($dados_xml->uso_tipo));
            $busca_sub_tipos = $this->m_tipos->buscar_imovel_sub_tipos( $xml_sub_tipos, $id_uso_tipo );

            foreach ($busca_sub_tipos as $busca_sub_tipo) {
                if (!empty($busca_sub_tipo) && $id_uso_tipo > 0) {
                    $sub_tipo = array();

                    $sub_tipo["id_imovel"] = $id_imovel;
                    $sub_tipo["id_sub_tipo"] = $busca_sub_tipo["id_sub_tipo_imovel"];
                    $sub_tipo["id_tipo"] = $busca_sub_tipo["id_tipo_imovel"];

                    array_push($sub_tipos, $sub_tipo);
                }
            }
            $this->m_imoveis->inserir_imovel_sub_tipos($sub_tipos);
        }
    }

    /*
     * _PREPARAR_BAIRRO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - 
    */
    private function _preparar_bairro($dados_xml=array()) {
        $cidade = remover_espacos_externos( remover_acentos(strval($dados_xml->cidade)) );
        $id_estado = $this->m_enderecos->buscar_id_estado( remover_acentos(strval($dados_xml->estado)) );
        $id_cidade = $this->m_enderecos->buscar_id_cidade( $cidade, $id_estado );
        $bairro = strval($dados_xml->bairro);
        $aux_bairro = $this->m_enderecos->buscar_aux_bairro($bairro, $id_cidade);

        if (empty($aux_bairro)) {
            $padronizados = formatar_erro_bairro($bairro);

            $this->m_enderecos->inserir_aux_bairros($this->_modelar_bairros_auxs($padronizados, $id_cidade));
        }
        return $aux_bairro['id_bairro'];
    }

    /*
     * _PREPARAR_CIDADE
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - 
    */
    private function _preparar_cidade($dados_xml=array()) {
        $cidade = remover_espacos_externos( remover_acentos(strval($dados_xml->cidade)) );
        $id_estado = $this->m_enderecos->buscar_id_estado( remover_acentos(strval($dados_xml->estado)) );
        $id_cidade = $this->m_enderecos->buscar_id_cidade( $cidade, $id_estado );

        if ($id_cidade == NULL && $id_estado != NULL) {
            $id_cidade = $this->m_enderecos->inserir_cidade($this->_modelar_cidade($dados_xml, $id_estado));
        }
        return $id_cidade;
    }

    /*
     * _PREPARAR_FOTOS
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - 
    */
    private function _preparar_fotos($dados_xml=array()) {
        $obj_fotos = $dados_xml->fotos;

        if (array_key_exists("foto", $obj_fotos) && count($obj_fotos) > 0) {
            $id_imovel = strval($dados_xml->id_imovel);
            $imagens = array();

            if ($id_imovel != NULL) {
                $fotos = $obj_fotos->foto;

                foreach ($fotos as $indice => $foto) {
                    array_push(
                        $imagens, 
                        $this->_modelar_foto($foto, $id_imovel, $indice)
                    );
                }
            }
            $this->m_imagens->inserir_imagens($imagens);
        }
    }

    /*
     * _PREPARAR_CHAVES
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - 
    */
    private function _preparar_chaves($dados_xml=array()) {
        $this->m_imoveis->inserir_chave(
            $this->_modelar_chave($dados_xml)
        );
    }

    /* ************************************************************* */
    /* ******************** FUNCOES DE MODELAGEM ******************* */
    /* ************************************************************* */
    /*
     * _MODELAR_ESPECIFICACOES
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - 
    */
    private function _modelar_especificacoes($dados_xml=array()) {
        if (!empty($dados_xml) && $this->_verificar_xml_especificacoes($dados_xml)) {
            $dados = array();

            $dados["id_situacao_documental"] = $this->m_tipos->buscar_id_situacao_documental(strval($dados_xml->sit_docmental));
            /* NAO TEM ATRIBUTO NO XML */
            $dados["id_tipo_financiamento"] = 1;
            $dados["id_tipo_garagem"] = $this->m_tipos->buscar_id_tipo_garagem(strval($dados_xml->tipo_garagem));
            $dados["id_local_garagem"] = $this->m_tipos->buscar_id_tipo_local_garagem(strval($dados_xml->local_garagem));
            $dados["id_face_imovel"] = $this->m_tipos->buscar_id_tipo_face(strval($dados_xml->face_imovel));
            /* NAO TEM ATRIBUTO NO XML */
            $dados["qtd_vagas_estacionamento"] = 0;
            $dados["qtd_vagas_garagem"] = intval($dados_xml->vagas_garagem);
            $dados["qtd_closets"] = intval($dados_xml->closet);
            $dados["qtd_quartos"] = intval($dados_xml->quartos);
            $dados["qtd_suites"] = intval($dados_xml->suite);
            /* NAO TEM ATRIBUTO NO XML */
            $dados["qtd_demi_suites"] = 0;
            $dados["qtd_churrasqueiras"] = intval($dados_xml->churrasqueira);
            $dados["qtd_copas"] = intval($dados_xml->copa);
            $dados["qtd_despensas"] = intval($dados_xml->despensa);
            $dados["qtd_cozinhas"] = intval($dados_xml->cozinha);
            $dados["qtd_lareiras"] = intval($dados_xml->lareira);
            $dados["qtd_sacadas"] = intval($dados_xml->sacada);
            $dados["qtd_salas"] = intval($dados_xml->sala_num);
            $dados["qtd_salas_jantar"] = intval($dados_xml->sala_jantar);
            $dados["qtd_salas_estar"] = intval($dados_xml->sala_estar);
            $dados["qtd_salas_estar_intima"] = intval($dados_xml->sala_estar_intima);
            $dados["qtd_salas_escritorio"] = intval($dados_xml->sala_escritorio);
            $dados["qtd_salas_biblioteca"] = intval($dados_xml->sala_biblioteca);
            $dados["qtd_lavabos"] = intval($dados_xml->lavabos);
            $dados["qtd_banheiros"] = intval($dados_xml->banheiros);
            $dados["qtd_areas_servico"] = intval($dados_xml->area_servico);
            $dados["qtd_dependencias_empregada"] = intval($dados_xml->dep_empregada);
            $dados["qtd_bwc_empregada"] = intval($dados_xml->bwc_empregada);
            /* NAO TEM ATRIBUTO NO XML */
            $dados["tem_adega"] = FALSE;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["tem_alarme"] = FALSE;
            $dados["tem_aquecimento"] = ( strval($dados_xml->aquecimento) == "S" ? TRUE : FALSE );
            /* NAO TEM ATRIBUTO NO XML */
            $dados["tem_atico"] = FALSE;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["tem_aquecimento_solar"] = FALSE;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["tem_aquecimento_eletrico"] = FALSE;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["tem_bicicletario"] = FALSE;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["tem_brinquedoteca"] = FALSE;
            $dados["tem_cancha_esportes"] = ( strval($dados_xml->cancha_esportes) == "S" ? TRUE : FALSE );
            $dados["tem_canil"] = ( strval($dados_xml->canil) == "S" ? TRUE : FALSE );
            $dados["tem_central_gas"] = ( strval($dados_xml->central_gas) == "S" ? TRUE : FALSE );
            /* NAO TEM ATRIBUTO NO XML */
            $dados["tem_cerca_eletrica"] = FALSE;
            $dados["tem_churrasqueira_privada"] = ( strval($dados_xml->churrasqueira_priv) == "S" ? TRUE : FALSE );
            $dados["tem_churrasqueira_coletiva"] = ( strval($dados_xml->churrasqueira_col) == "S" ? TRUE : FALSE );
            /* NAO TEM ATRIBUTO NO XML */
            $dados["tem_cinema"] = FALSE;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["tem_cozinha_armarios"] = FALSE;
            $dados["tem_sistema_ar"] = ( strval($dados_xml->sist_ar) == "S" ? TRUE : FALSE );
            $dados["tem_sistema_incendio"] = ( strval($dados_xml->sist_incendio) == "S" ? TRUE : FALSE );
            $dados["tem_sistema_cftv"] = ( strval($dados_xml->sist_cftv) == "S" ? TRUE : FALSE );
            /* NAO TEM ATRIBUTO NO XML */
            $dados["tem_sistema_seguranca"] = FALSE;
            $dados["tem_deposito"] = ( strval($dados_xml->deposito) == "S" ? TRUE : FALSE );
            /* NAO TEM ATRIBUTO NO XML */
            $dados["tem_despensa"] = FALSE;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["tem_dormitorio_armarios"] = FALSE;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["tem_fitness"] = FALSE;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["tem_garden"] = FALSE;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["tem_grades"] = FALSE;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["tem_hidro"] = FALSE;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["tem_interfone"] = FALSE;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["tem_jardim_inverno"] = FALSE;
            $dados["tem_piscina"] = ( strval($dados_xml->piscina) == "S" ? TRUE : FALSE );
            /* NAO TEM ATRIBUTO NO XML */
            $dados["tem_piscina_infantil"] = FALSE;
            $dados["esta_mobiliado"] = ( strval($dados_xml->mobiliado) == "S" ? TRUE : FALSE );
            /* NAO TEM ATRIBUTO NO XML */
            $dados["tem_pista_caminhada"] = FALSE;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["tem_playground"] = FALSE;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["tem_portao_eletronico"] = FALSE;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["tem_portaria_vinte_quatro"] = FALSE;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["tem_quiosque"] = FALSE;
            $dados["tem_salao_festas"] = ( strval($dados_xml->salao_festas) == "S" ? TRUE : FALSE );
            $dados["tem_salao_jogos"] = ( strval($dados_xml->salao_jogos) == "S" ? TRUE : FALSE );
            /* NAO TEM ATRIBUTO NO XML */
            $dados["tem_sauna"] = FALSE;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["tem_spa"] = FALSE;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["tem_espaco_gourmet"] = FALSE;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["tem_edicula"] = FALSE;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["tem_quintal"] = FALSE;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["tem_jardim"] = FALSE;

            $dados["conservacao"] = strval($dados_xml->conservacao);
            /* NAO TEM ATRIBUTO NO XML */
            $dados["area_privada"] = 0;
            $dados["area_total"] = strval($dados_xml->area_total);
            $dados["area_averbada"] = strval($dados_xml->area_averbada);
            $dados["area_util"] = strval($dados_xml->area_util);
            /* NAO TEM ATRIBUTO NO XML */
            $dados["area_privada_terreno"] = 0;
            $dados["esquadrias"] = strval($dados_xml->esquadrias);
            $dados["revestimento_externo"] = strval($dados_xml->revestimento_ext);
            $dados["construtora"] = strval($dados_xml->construtora);
            $dados["horarios_visita"] = strval($dados_xml->horario_visitas);
            $dados["qtd_elevadores"] = intval($dados_xml->elevador);
            /* NAO TEM ATRIBUTO NO XML */
            $dados["qtd_apartamentos_andar"] = 0;
            $dados["numero_pavimentos"] = intval($dados_xml->num_pavimentos);
            /* NAO TEM ATRIBUTO NO XML */
            $dados["tipo_piso"] = NULL;
            $dados["condicoes_negocio"] = strval($dados_xml->condicoes_negocio);
            $dados["status_imovel"] = strval($dados_xml->status_imovel);
            $dados["servicos_opcionais"] = strval($dados_xml->serv_opcionais);

            return $dados;
        }
        return array();
    }

    /*
     * _MODELAR_INFORMACOES_COMPLEMENTARES
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - 
    */
    private function _modelar_informacoes_complementares($dados_xml=array()) {
        if (!empty($dados_xml) && $this->_verificar_xml_informacoes_complementares($dados_xml)) {
            $dados = array();

            $dados["id_tipo_acao"] = $this->m_tipos->buscar_id_tipo_acao(strval($dados_xml->acao));
            $dados["id_tipo_ocupacao"] = $this->m_tipos->buscar_id_tipo_ocupacao(strval($dados_xml->ocupacao));
            $dados["id_tipo_finalidade"] = $this->m_tipos->buscar_id_tipo_finalidade(strval($dados_xml->finalidade));
            $dados["id_face_apartamento"] = $this->m_tipos->buscar_id_tipo_face(strval($dados_xml->face_apto));
            $dados["id_tipo_andar"] = $this->m_tipos->buscar_id_tipo_andar(strval($dados_xml->apt_andar));
            $dados["idade_imovel"] = strval($dados_xml->idade);
            $dados["condominio_fechado"] = ( strval($dados_xml->condominio_fechado) == "S" ? TRUE : FALSE );
            /* NAO TEM ATRIBUTO NO XML */
            $dados["cobertura"] = FALSE;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["informacoes_adicionais"] = NULL;
            $dados["lancamento"] = ( strval($dados_xml->lancamento) == "S" ? TRUE : FALSE );
            /* NAO TEM ATRIBUTO NO XML */
            $dados["uso_tipo"] = NULL;
            $dados["reservado"] = ( strval($dados_xml->reservado) == "S" ? TRUE : FALSE );
            $dados["andar_apartamento"] = intval($dados_xml->apt_andar);
            $dados["situacao"] = strval($dados_xml->situacao);
            /* NAO TEM ATRIBUTO NO XML */
            $dados["vago_em"] = NULL;
            $dados["acab_benfeito"] = strval($dados_xml->acab_benfeito);

            return $dados;
        }
        return array();
    }

    /*
     * _MODELAR_INFRAESTRUTURA
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - Array do dados_xml de imovel
    */
    private function _modelar_infraestrutura($dados_xml=array()) {
        if (!empty($dados_xml)) {
            $dados = array();

            $dados["id_tipo_zoneamento"] = $this->m_tipos->buscar_id_tipo_zoneamento(strval($dados_xml->zoneamento));
            $dados["id_face_terreno"] = $this->m_tipos->buscar_id_tipo_face(strval($dados_xml->face_terreno));
            /* NAO TEM ATRIBUTO NO XML */
            $dados["id_tipo_material"] = NULL;
            $dados["area_terreno"] = strval($dados_xml->area_terreno);
            $dados["medidas_terreno"] = strval($dados_xml->medidas_terreno);
            $dados["pavimentacao"] = strval($dados_xml->pavimentacao);
            $dados["pe_direito"] = strval($dados_xml->pe_direito);
            $dados["lote"] = strval($dados_xml->lote);
            $dados["quadra"] = strval($dados_xml->quadra);
            $dados["planta"] = strval($dados_xml->planta);
            $dados["indicacao_fiscal"] = strval($dados_xml->indicacao_fiscal);
            $dados["topografia"] = strval($dados_xml->topografia);
            /* NAO TEM ATRIBUTO NO XML */
            $dados["registro_imovel"] = NULL;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["circunscricao"] = NULL;
            $dados["tem_agua"] = ( strval($dados_xml->agua) == "S" ? TRUE : FALSE );
            $dados["tem_esgoto"] = ( strval($dados_xml->esgoto) == "S" ? TRUE : FALSE );
            $dados["tem_luz"] = ( strval($dados_xml->luz) == "S" ? TRUE : FALSE );
            $dados["tem_telefone"] = ( strval($dados_xml->telefone) == "S" ? TRUE : FALSE );
            $dados["tem_placa_local"] = ( strval($dados_xml->placa) == "S" ? TRUE : FALSE );
            $dados["tem_forro"] = ( strval($dados_xml->forro) == "S" ? TRUE : FALSE );
            $dados["tem_telhado"] = ( strval($dados_xml->telhado) == "S" ? TRUE : FALSE );

            return $dados;
        }
        return array();
    }

    /*
     * _MODELAR_VEICULACAO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - 
    */
    private function _modelar_veiculacao($dados_xml=array()) {
        if (!empty($dados_xml)) {
            $dados = array();

            /* NAO TEM ATRIBUTO NO XML */
            $dados["id_corretor"] = NULL;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["id_privacidade"] = 1;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["data_angariacao"] = NULL;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["viva_real"] = FALSE;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["imovel_web"] = FALSE;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["zap"] = FALSE;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["zap_super"] = FALSE;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["enkontra"] = FALSE;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["nao_imovel_web"] = FALSE;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["nao_zap"] = FALSE;
            $dados["video_youtube"] = strval($dados_xml->video);

            return $dados;
        }
        return array();
    }

    /*
     * _MODELAR_INFORMACOES_XML
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - 
    */
    private function _modelar_informacoes_xml($dados_xml=array()) {
        if (!empty($dados_xml)) {
            $dados = array();

            /* NAO TEM ATRIBUTO NO XML */
            $dados["id_tipo_xml_status"] = 1;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["data_insercao"] = data_hoje_sql();
            /* NAO TEM ATRIBUTO NO XML */
            $dados["data_atualizacao"] = data_hoje_sql();
            /* NAO TEM ATRIBUTO NO XML */
            $dados["data_edicao"] = NULL;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["ultimas_modificacoes"] = "Todos os campos";

            return $dados;
        }
        return array();
    }

    /*
     * _MODELAR_BAIRRO_AUX
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] -
    */
    private function _modelar_bairro_aux($dados_xml=array(), $id_cidade=NULL) {
        if (!empty($dados_xml)) {
            $dados = array();

            $dados["id_bairro"] = NULL;
            $dados["id_cidade"] = $id_cidade;
            $dados["nome_aux"] = lower_acentuada($dados_xml->bairro);

            return $dados;
        }
        return array();
    }

    /*
     * _MODELAR_BAIRROS_AUXS
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] -
    */
    private function _modelar_bairros_auxs($padronizados=array(), $id_cidade=NULL) {
        if (!empty($padronizados)) {
            $dados = array();

            foreach ($padronizados as $padronizado) {
                $dados_padronizado = array();

                $dados_padronizado["id_bairro"] = NULL;
                $dados_padronizado["id_cidade"] = $id_cidade;
                $dados_padronizado["nome_aux"] = $padronizado;

                array_push($dados, $dados_padronizado);
            }
            return $dados;
        }
        return array();
    }

    /*
     * _MODELAR_CIDADE
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] -
    */
    private function _modelar_cidade($dados_xml=array(), $id_estado=-1) {
        if (!empty($dados_xml) && $id_estado != NULL) {
            $dados = array();

            $dados["id_estado"] = $id_estado;
            $dados["nome_cidade"] = remover_espacos_externos( ucwords_acentuada($dados_xml->cidade) );

            return $dados;
        }
        return array();
    }

    /*
     * _MODELAR_COMPLEMENTO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - 
    */
    private function _modelar_complemento($dados_xml=array()) {
        if (!empty($dados_xml)) {
            $dados = array();

            $dados["descricao"] = strval($dados_xml->complemento);
            /* NAO TEM ATRIBUTO NO XML */
            $dados["bloco"] = NULL;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["sala"] = NULL;
            $dados["nome_edificio"] = strval($dados_xml->inform_edificio);
            $dados["nome_condominio"] = strval($dados_xml->condominio);
            $dados["imediacoes"] = strval($dados_xml->imediacoes);
            /* NAO TEM ATRIBUTO NO XML */
            $dados["entre_ruas"] = NULL;

            return $dados;
        }
        return array();
    }


    /*
     * _MODELAR_LAT_LONG
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - 
    */
    private function _modelar_lat_long($dados_xml=array()) {
        if (!empty($dados_xml)) {
            $dados = array();

            $dados["latitude"] = strval($dados_xml->latitude);
            $dados["longitude"] = strval($dados_xml->longitude);

            return $dados;
        }
        return array();
    }

    /*
     * _MODELAR_ENDERECO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - 
    */
    private function _modelar_endereco($dados_xml=array(), $dados=array()) {
        if (!empty($dados_xml) && !empty($dados)) {
            $endereco = array();

            $endereco["id_imovel"] = strval($dados_xml->id_imovel);
            $endereco["id_cidade"] = $dados["id_cidade"];
            $endereco["id_bairro"] = $dados["id_bairro"];
            $endereco["id_complemento"] = $dados["id_complemento"];
            $endereco["id_lat_long"] = $dados["id_lat_long"];
            $endereco["cep"] = strval($dados_xml->cep);
            $endereco["logradouro"] = strval($dados_xml->endereco);
            $endereco["numero"] = strval($dados_xml->numero);

            return $endereco;
        }
        return array();
    }

    /*
     * _MODELAR_FOTO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - 
    */
    private function _modelar_foto($dados_xml=array(), $id_imovel=0, $ordem=0) {
        if (!empty($dados_xml)) {
            $dados = array();

            $dados["id_imovel"] = $id_imovel;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["caminho_imagem"] = NULL;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["caminho_miniatura"] = NULL;
            $dados["link_imagem"] = strval($dados_xml->foto);
            $dados["link_miniatura"] = strval($dados_xml->miniatura);
            /* NAO TEM ATRIBUTO NO XML */
            $dados["texto_imagem"] = NULL;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["tamanho_largura"] = 0;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["tamanho_altura"] = 0;
            $dados["principal"] = ( intval($dados_xml->principal) ? TRUE : FALSE );
            $dados["ordem"] = $ordem;

            return $dados;
        }
        return array();
    }

    /*
     * _MODELAR_VALORES
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - 
    */
    private function _modelar_valores($dados_xml=array()) {
        if (!empty($dados_xml)) {
            $dados = array();

            $dados["id_imovel"] = strval($dados_xml->id_imovel);
            /* NAO TEM ATRIBUTO NO XML */
            $dados["preco_venda"] = floatval($dados_xml->preco_venda);
            /* NAO TEM ATRIBUTO NO XML */
            $dados["preco_locacao"] = ( strval($dados_xml->finalidade) == "L" ? 0 : floatval($dados_xml->valor_liquido) );
            $dados["valor_condominio"] = floatval($dados_xml->valor_condominio);
            $dados["valor_aluguel"] = floatval($dados_xml->valor_aluguel);
            $dados["valor_liquido"] = floatval($dados_xml->valor_liquido);
            /* NAO TEM ATRIBUTO NO XML */
            $dados["valor_negociacao"] = ( strval($dados_xml->finalidade) == "V" ? floatval($dados_xml->preco_venda) : floatval($dados_xml->valor_liquido) );
            /* NAO TEM ATRIBUTO NO XML */
            $dados["valor_financiamento"] = NULL;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["valor_poupanca"] = NULL;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["valor_prestacao"] = NULL;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["agente_financeiro"] = NULL;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["saldo_devedor"] = NULL;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["prazo"] = NULL;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["numero_prestacoes_pagas"] = NULL;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["numero_prestacoes"] = NULL;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["valor_bruto"] = NULL;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["bonificacao"] = NULL;
            $dados["valor_iptu"] = floatval($dados_xml->iptu);
            /* NAO TEM ATRIBUTO NO XML */
            $dados["outros_valores"] = NULL;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["prazo_contrato"] = NULL;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["reajuste"] = NULL;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["indice_reajuste"] = NULL;
            $dados["valor_seguro_incendio"] = floatval($dados_xml->seguro_incendio);
            /* NAO TEM ATRIBUTO NO XML */
            $dados["fci"] = NULL;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["valor_seguro_fianca"] = NULL;

            return $dados;
        }
        return array();
    }

    /*
     * _MODELAR_CHAVE
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - 
    */
    private function _modelar_chave($dados_xml=array()) {
        if (!empty($dados_xml)) {
            $dados = array();

            $dados["id_imovel"] = strval($dados_xml->id_imovel);
            /* NAO TEM ATRIBUTO NO XML */
            $dados["id_cliente"] = NULL;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["id_loja"] = NULL;
            /* NAO TEM ATRIBUTO NO XML */
            $dados["id_usuario"] = NULL;
            $dados["descricao"] = strval($dados_xml->chaves);

            return $dados;
        }
        return array();
    }

    /*
     * _MODELAR_NEGOCIACAO
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - 
    */
    private function _modelar_negociacao($dados_xml=array(), $dados=array()) {
        if (!empty($dados_xml) && !empty($dados)) {
            $negociacao = array();

            $negociacao["id_imovel"] = strval($dados_xml->id_imovel);
            $negociacao["id_corretor"] = NULL;
            $negociacao["id_proprietario"] = NULL;
            $negociacao["id_cliente"] = NULL;
            $negociacao["id_tipo_negociacao"] = $this->m_tipos->buscar_id_tipo_negociacao(strval($dados_xml->finalidade));
            $negociacao["id_loja"] = $this->m_imoveis->buscar_id_loja(strval($dados_xml->loja));
            $negociacao["id_valores_imovel"] = $dados["id_valores_imovel"];
            $negociacao["id_comissao"] = NULL;
            $negociacao["detalhes_negociacao"] = NULL;

            return $negociacao;
        }
        return array();
    }

    /*
     * _MODELAR_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : N/A
     * ---------------------------------------
     * RETORNO : N/A
    */
    private function _modelar_imovel($dados_xml=array(), $dados=array(), $id_imobiliaria=0) {
        if (!empty($dados_xml) && !empty($dados)) {
            $imovel = array();

            $imovel["id_imovel"] = strval($dados_xml->id_imovel);
            $imovel["id_imobiliaria"] = $id_imobiliaria;
            $imovel["id_especificacoes"] = $dados["id_especificacoes"];
            $imovel["id_informacoes_complementares"] = $dados["id_informacoes_complementares"];
            $imovel["id_infraestrutura"] = $dados["id_infraestrutura"];
            $imovel["id_veiculacao"] = $dados["id_veiculacao"];
            $imovel["id_informacoes_xml"] = $dados["id_informacoes_xml"];
            $imovel["referencia"] = strval($dados_xml->referencia);
            /* NAO TEM ATRIBUTO NO XML */
            $imovel["titulo"] = NULL;
            $imovel["descricao"] = strval($dados_xml->descricao);
            /* NAO TEM ATRIBUTO NO XML */
            $imovel["observacao"] = NULL;
            /* NAO TEM ATRIBUTO NO XML */
            $imovel["alugado"] = FALSE;
            /* NAO TEM ATRIBUTO NO XML */
            $imovel["vendido"] = FALSE;

            $imovel["xml_atualizavel"] = TRUE;

            return $imovel;
        }
        return array();
    }

    /* ************************************************************* */
    /* ******************* FUNCOES DE VERIFICACAO ****************** */
    /* ************************************************************* */
    /*
     * _VERIFICAR_XML
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - 
    */
    private function _verificar_xml($xml=array()) {
        return $xml 
        &&  isset($xml) 
        &&  !empty($xml)
        &&  array_key_exists("Imovel", $xml);
    }

    /*
     * _VERIFICAR_XML_IMOVEL
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - 
    */
    private function _verificar_xml_imovel($xml=array()) {
        return 
            array_key_exists("id_imovel", $xml)
        &&  array_key_exists("referencia", $xml)
        &&  array_key_exists("descricao", $xml);
    }

    /*
     * _VERIFICAR_XML_ESPECIFICACOES
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - 
    */
    private function _verificar_xml_especificacoes($xml=array()) {
        return 
            array_key_exists("sit_docmental", $xml)
        &&  array_key_exists("tipo_garagem", $xml)
        &&  array_key_exists("local_garagem", $xml)
        &&  array_key_exists("face_imovel", $xml)
        &&  array_key_exists("vagas_garagem", $xml)
        &&  array_key_exists("closet", $xml)
        &&  array_key_exists("quartos", $xml)
        &&  array_key_exists("suite", $xml)
        &&  array_key_exists("churrasqueira", $xml)
        &&  array_key_exists("copa", $xml)
        &&  array_key_exists("despensa", $xml)
        &&  array_key_exists("cozinha", $xml)
        &&  array_key_exists("lareira", $xml)
        &&  array_key_exists("sacada", $xml)
        &&  array_key_exists("sala_num", $xml)
        &&  array_key_exists("sala_jantar", $xml)
        &&  array_key_exists("sala_estar", $xml)
        &&  array_key_exists("sala_estar_intima", $xml)
        &&  array_key_exists("sala_escritorio", $xml)
        &&  array_key_exists("sala_biblioteca", $xml)
        &&  array_key_exists("lavabos", $xml)
        &&  array_key_exists("banheiros", $xml)
        &&  array_key_exists("area_servico", $xml)
        &&  array_key_exists("dep_empregada", $xml)
        &&  array_key_exists("bwc_empregada", $xml)
        &&  array_key_exists("aquecimento", $xml)
        &&  array_key_exists("cancha_esportes", $xml)
        &&  array_key_exists("canil", $xml)
        &&  array_key_exists("central_gas", $xml)
        &&  array_key_exists("churrasqueira_priv", $xml)
        &&  array_key_exists("churrasqueira_col", $xml)
        &&  array_key_exists("sist_ar", $xml)
        &&  array_key_exists("sist_incendio", $xml)
        &&  array_key_exists("sist_cftv", $xml)
        &&  array_key_exists("deposito", $xml)
        &&  array_key_exists("piscina", $xml)
        &&  array_key_exists("mobiliado", $xml)
        &&  array_key_exists("salao_festas", $xml)
        &&  array_key_exists("salao_jogos", $xml)
        &&  array_key_exists("conservacao", $xml)
        &&  array_key_exists("area_total", $xml)
        &&  array_key_exists("area_averbada", $xml)
        &&  array_key_exists("area_util", $xml)
        &&  array_key_exists("esquadrias", $xml)
        &&  array_key_exists("revestimento_ext", $xml)
        &&  array_key_exists("construtora", $xml)
        &&  array_key_exists("horario_visitas", $xml)
        &&  array_key_exists("elevador", $xml)
        &&  array_key_exists("num_pavimentos", $xml)
        &&  array_key_exists("condicoes_negocio", $xml)
        &&  array_key_exists("status_imovel", $xml)
        &&  array_key_exists("serv_opcionais", $xml);
    }

    /*
     * _VERIFICAR_XML_INFORMACOES_COMPLEMENTARES
     * ---------------------------------------
     * DESCRICAO : 
     * ---------------------------------------
     * PARAMETROS : --
     * ---------------------------------------
     * RETORNO : [ARRAY] - 
    */
    private function _verificar_xml_informacoes_complementares($xml=array()) {
        return 
            array_key_exists("acao", $xml)
        &&  array_key_exists("ocupacao", $xml)
        &&  array_key_exists("finalidade", $xml)
        &&  array_key_exists("face_apto", $xml)
        &&  array_key_exists("apt_andar", $xml)
        &&  array_key_exists("idade", $xml)
        &&  array_key_exists("condominio_fechado", $xml)
        &&  array_key_exists("lancamento", $xml)
        &&  array_key_exists("reservado", $xml)
        &&  array_key_exists("apt_andar", $xml)
        &&  array_key_exists("situacao", $xml)
        &&  array_key_exists("acab_benfeito", $xml);
    }

}
