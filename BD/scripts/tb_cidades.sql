SET FOREIGN_KEY_CHECKS=0;

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `baggio`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_cidades`
--

DROP TABLE IF EXISTS `tb_cidades`;
CREATE TABLE IF NOT EXISTS `tb_cidades` (
  `id_cidade` int(11) NOT NULL AUTO_INCREMENT,
  `id_estado` int(11) DEFAULT NULL,
  `nome_cidade` varchar(191) NOT NULL,
  PRIMARY KEY (`id_cidade`),
  KEY `index_fk_estado` (`id_estado`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

--
-- Extraindo dados da tabela `tb_cidades`
--

INSERT INTO `tb_cidades` (`id_cidade`, `id_estado`, `nome_cidade`) VALUES
(1, 4, 'Campo Largo'),
(2, 4, 'Curitiba'),
(3, 4, 'Araucária'),
(4, 4, 'São José Dos Pinhais'),
(5, 4, 'Campo Magro'),
(6, 4, 'Palmeira'),
(7, 5, 'Jaraguá Do Sul'),
(8, 4, 'Matinhos'),
(9, 4, 'Piraquara'),
(10, 5, 'São Francisco Do Sul'),
(11, 4, 'Colombo'),
(12, 5, 'Itapema'),
(13, 4, 'Fazenda Rio Grande'),
(14, 4, 'Almirante Tamandaré'),
(15, 4, 'Balsa Nova'),
(16, 5, 'Balneário Camboriú'),
(17, 4, 'Guaratuba'),
(19, 4, 'Pinhais'),
(20, 4, 'Pontal Do Paraná'),
(21, 4, 'Campina Grande Do Sul'),
(22, 4, 'Castro'),
(23, 5, 'Xanxerê'),
(24, 5, 'Itapoá'),
(25, 5, 'Imbituba'),
(26, 4, 'Antonina'),
(27, 4, 'Quatro Barras'),
(28, 5, 'Piçarras'),
(29, 4, 'Bocaiúva Do Sul'),
(30, 4, 'Paranaguá');

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `tb_cidades`
--
ALTER TABLE `tb_cidades`
  ADD CONSTRAINT `fk_tb_cidades_tb_estados` FOREIGN KEY (`id_estado`) REFERENCES `tb_estados` (`id_estado`) ON DELETE SET NULL ON UPDATE SET NULL;

SET FOREIGN_KEY_CHECKS=1;
