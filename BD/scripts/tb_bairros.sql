SET FOREIGN_KEY_CHECKS=0;

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";
--
-- Database: `baggio`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_bairros`
--

DROP TABLE IF EXISTS `tb_bairros`;
CREATE TABLE IF NOT EXISTS `tb_bairros` (
  `id_bairro` int(11) NOT NULL AUTO_INCREMENT,
  `id_cidade` int(11) DEFAULT NULL,
  `nome_bairro` varchar(191) NOT NULL,
  PRIMARY KEY (`id_bairro`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=118 ;

--
-- Extraindo dados da tabela `tb_bairros`
--

INSERT INTO `tb_bairros` (`id_bairro`, `id_cidade`, `nome_bairro`) VALUES
(1, 1, 'Rondinha'),
(2, 2, 'Campo Comprido'),
(3, 2, 'Bigorrilho'),
(4, 2, 'Portão'),
(5, 2, 'Centro'),
(6, 2, 'Cidade Industrial'),
(7, 2, 'São Braz'),
(8, 2, 'Guaíra'),
(9, 2, 'Novo Mundo'),
(10, 2, 'Ecoville'),
(11, 1, 'Ferraria'),
(12, 2, 'Água Verde'),
(13, 2, 'Cabral'),
(14, 2, 'Santa Quitéria'),
(15, 2, 'Santo Inácio'),
(16, 2, 'Santa Cândida'),
(17, 2, 'Batel'),
(18, 1, 'Vila David Antônio'),
(20, 2, 'Campina do Siqueira'),
(21, 2, 'Juvevê'),
(22, 2, 'Vila Izabel'),
(23, 3, 'Araucária - Centro'),
(24, 2, 'Orleans'),
(25, 1, 'Bolinete / Ferraria'),
(26, 2, 'Xaxim'),
(27, 2, 'Jardim Botânico'),
(28, 2, 'Fazendinha'),
(29, NULL, 'Colônia Rebouças'),
(30, 2, 'Augusta'),
(31, 2, 'Santa Felicidade'),
(33, 2, 'Alto da Glória'),
(34, 2, 'Fanny'),
(35, 2, 'Vila Guaíra'),
(36, 2, 'Bacacheri'),
(37, 2, 'Pinheirinho'),
(38, 2, 'Guabirotuba'),
(39, 2, 'Seminário'),
(40, 2, 'Centro Civico'),
(41, 1, 'Vila Gilcy'),
(42, 1, 'Ferraria / Bolinete'),
(43, 2, 'Campo Largo da Roseira'),
(44, 2, 'Jardim das Américas'),
(45, 2, 'Cristo Rei '),
(46, 2, 'Parolin'),
(47, 2, 'Capão Raso'),
(48, 4, 'São José dos Pinhais'),
(49, 5, 'Samambaia'),
(50, 2, 'Riviera'),
(51, 6, 'Centro - Palmeira/PR'),
(52, 2, 'Prado Velho'),
(53, 2, 'São Francisco'),
(54, 7, 'Vila Lalau - Jaraguá Do Sul'),
(55, NULL, 'Cotolengo'),
(56, 8, 'Balneário Inaja'),
(57, 9, 'Jardim Guarani'),
(58, 2, 'Cic'),
(59, 2, 'Butiatuvinha'),
(60, 10, 'Praia de Itaguaçu'),
(62, 11, 'Fazenda Rincão'),
(63, 2, 'Barigui/Santo Inácio'),
(64, 2, 'Alto da Rua Xv'),
(66, 2, 'Vista Alegre'),
(67, 2, 'Lindoia'),
(68, 2, 'Mercês'),
(69, 2, 'Pilarzinho'),
(70, 2, 'Boa Vista'),
(71, 1, 'Vila Torres I'),
(74, 2, 'Umbará'),
(75, 2, 'Uberaba'),
(82, 2, 'Rebouças'),
(83, 13, 'Estados'),
(85, 2, 'Mossunguê'),
(86, 2, 'Tatuquara'),
(87, 14, 'Tanguá'),
(88, 8, 'Balneário de Caioba'),
(89, 6, 'Centro - Palmeira/PR'),
(90, 12, 'Centro'),
(92, 16, 'Centro'),
(93, 15, 'Bugre'),
(94, 2, 'Capão da Imbuia'),
(95, 2, 'Abranches'),
(96, 2, 'Bom Retiro'),
(97, 2, 'Cajuru'),
(98, 2, 'Tarumã'),
(99, 19, 'Jardim Pedro Demeterco'),
(100, 2, 'Hauer'),
(101, 2, 'Cachoeira'),
(102, 2, 'Ahú'),
(103, 2, 'Boqueirão'),
(104, 2, 'Bairro Alto'),
(105, 4, 'Roseira de São Sebastião'),
(106, 2, 'Jardim Social'),
(107, 11, 'Vila Yara'),
(108, 2, 'São João'),
(109, 2, 'Alto Boqueirão'),
(110, 17, 'Piçarras'),
(111, 5, 'Campo Magro'),
(112, 2, 'Barreirinha'),
(113, 9, 'Jardim Primavera'),
(114, 25, 'Praia do Rosa'),
(115, 4, 'Afonso Pena'),
(116, 2, 'Passauna');

SET FOREIGN_KEY_CHECKS=1;
