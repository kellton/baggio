SET FOREIGN_KEY_CHECKS=0;

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `baggio`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_sub_tipo_imovel`
--

DROP TABLE IF EXISTS `tb_sub_tipo_imovel`;
CREATE TABLE IF NOT EXISTS `tb_sub_tipo_imovel` (
  `id_sub_tipo_imovel` int(11) NOT NULL AUTO_INCREMENT,
  `id_tipo_imovel` int(11) NOT NULL,
  `nome_tipo` varchar(191) NOT NULL,
  PRIMARY KEY (`id_sub_tipo_imovel`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Extraindo dados da tabela `tb_sub_tipo_imovel`
--

INSERT INTO `tb_sub_tipo_imovel` (`id_sub_tipo_imovel`, `id_tipo_imovel`, `nome_tipo`) VALUES
(1, 0, 'Tipo Não Informado'),
(2, 2, 'Apartamento'),
(3, 2, 'Casa'),
(4, 2, 'Chácara'),
(5, 2, 'Edifício'),
(6, 2, 'Fazenda'),
(7, 2, 'Flat'),
(8, 2, 'Garagem'),
(9, 2, 'Hotel/Pousada'),
(10, 2, 'Kitnet'),
(11, 2, 'Loft'),
(12, 2, 'Sobrado'),
(13, 2, 'Stúdio'),
(14, 2, 'Terreno/Área'),
(15, 3, 'Barracão/Galpão'),
(16, 3, 'Loja'),
(17, 3, 'Ponto/Negócios'),
(18, 3, 'Sala/Conjunto Comercial'),
(19, 3, 'Imóvel Comercial'),
(20, 3, 'Casa'),
(21, 3, 'Terreno'),
(22, 3, 'Chácara'),
(23, 3, 'Fazenda'),
(24, 2, 'Cobertura');

SET FOREIGN_KEY_CHECKS=1;
