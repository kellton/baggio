Baggio Imóveis
====================

# Primeiros Passos

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

# Pré-Requisitos

Apache >= 2.2

PHP >= 7.0

MySQL >= 5.6

Codeigniter >= 3.0

- - -

# Instalação
Fazer o Clone do projeto no repositório do BitBucket:

```
git clone https://bitbucket.org/kellton/baggio
```

Depois rodar o Composer

```
composer install
```

Adcionar os seguintes arquivos no projeto em seus respectivos diretórios:

* config.php

```
application/config/
```

* database.php

```
application/config/
```

* baggio-config.js

```
publico/js/
```

* baggio-constants.js

```
publico/js/
```

Para inicinar o projeto basta abrir o navegador e digitar

```
http://localhost/baggio
```

##Desenvolvido com

* [Codeigniter](https://codeigniter.com/user_guide/) - Framework PHP
* [MPDF](https://mpdf.github.io/) - Biblioteca de PHP

# Autores

* **Felipe Chagas** - *Desenvolvedor Full Stack* - [Facebook](https://www.facebook.com/felipe.chagas.71)
* **Guilherme Shiki** - *Desenvolvedor Full Stack* - [Facebook](https://www.facebook.com/guilherme.shiki)
* **Kellton Leitão** - *Desenvolvedor Full Stack* - [Facebook](https://www.facebook.com/kellton.leitao)

# Licença

Please see the [license agreement] (https://github.com/bcit-ci/CodeIgniter/blob/develop/user_guide_src/source/license.rst)
