/* CONSTANTES DE TEMPO DE ALERTA */
var CONS_TEMPO_ESCONDER_AUTO = 0.3 * 1000;
var CONS_TEMPO_MOSTRAR_AUTO = 0.3 * 1000;

/* **************************************************************************** */
/* ********************************* CHAMADAS ********************************* */
/* **************************************************************************** */
$(document).ready( function() {
	//inicializarAutocompleteProprietarios();
});

/* **************************************************************************** */
/* ********************************* FUNCOES ********************************** */
/* **************************************************************************** */
/**
 * INCIALIZAR AUTOCOMPLETE PROPRIETARIOS
 * ***********************
 * Funcao para incializacao de 
 * elementos que tem busca com
 * autocomplete para proprietario.
 * ***********************
*/
function inicializarAutocompleteProprietarios() {
	var $elementoSearchProprietario = $("#search-proprietario");

	var atributosAuto = {
		url: CONS_URL_BAGGIO + "api/adm/busca/proprietarios",

		getValue: "nome_cliente",

		list: {	
			match: {
				enabled: true
			},
			maxNumberOfElements: 6,
			showAnimation: {
				type: "slide",
				time: CONS_TEMPO_MOSTRAR_AUTO
			},
			hideAnimation: {
				type: "slide",
				time: CONS_TEMPO_ESCONDER_AUTO
			},
			onChooseEvent: function() {
				var idClienteEscolhido = $("#search-proprietario").getSelectedItemData().id_cliente;

				$("#search-result-proprietario").val(idClienteEscolhido);
			}
		},
		theme: "square"
	};

	if ($elementoSearchProprietario.length) {
		$elementoSearchProprietario.easyAutocomplete(atributosAuto);
	}
}
