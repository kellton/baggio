/* **************************************************************************** */
/* ********************************* CHAMADAS ********************************* */
/* **************************************************************************** */
$(document).ready(function (){
  inicializarMascaras();

  inicializarEnderecoAuto();
});

/* **************************************************************************** */
/* ********************************* FUNCOES ********************************** */
/* **************************************************************************** */
/**
 * INCIIALIZAR MASCARAS
 * ***********************
 * Funcao para incializacao de mascaras
 * dos formularios.
 * ***********************
*/
function inicializarMascaras() {
  //Máscara para CEP
  $('[mascara="cep"]').mask('00000-000',{reverse:true});
  //Máscara para data
  $('[mascara="data"]').mask('00/00/0000',{reverse:true});
  //Máscara para data e hora
  $('[mascara="datahora"]').mask('00/00/0000 00:00',{reverse:true});
  //Máscara para telefone fixo
  $('[mascara="tel"]').mask('(00) 0000-0000');
  //Máscara para celular
  $('[mascara="cel"]').mask('(00) 0 0000-0000');
  //Máscara para CPF
  $('[mascara="cpf"]').mask('000.000.000-00',{reverse:true});
  //Máscara para CNPJ
  $('[mascara="cnpj"]').mask('00.000.000/0000-00',{reverse:true});
  //Máscara para moeda
  $('[mascara="moeda"]').mask('000.000.000.000.000,00',{reverse:true});
  //Máscara para area
  $('[mascara="area"]').mask('000000000000.00',{reverse:true});
}
