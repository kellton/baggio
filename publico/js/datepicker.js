/* **************************************************************************** */
/* ********************************* CHAMADAS ********************************* */
/* **************************************************************************** */
$(document).ready( function () {
  inicializarDatepickers();
});

/* **************************************************************************** */
/* ********************************* FUNCOES ********************************** */
/* **************************************************************************** */
/**
 * INCIALIZAR DATEPICKERS
 * ***********************
 * Funcao para incializacao de mascaras
 * ***********************
*/
function inicializarDatepickers() {
  $("[datepicker]").each( function() {
    $(this).datepicker({
      dayNames: CONS_DIAS_SEMANA,
      dateFormat: "dd/mm/yy - DD",
      dayNamesMin: CONS_DIAS_RESUMO,
      dayNamesShort: CONS_DIAS_RESUMO,
      monthNames: CONS_MESES,
      monthNamesShort: CONS_MESES_RESUMO
    })
  });
}
