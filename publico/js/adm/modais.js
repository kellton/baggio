
/* **************************************************************************** */
/* ********************************* CHAMADAS ********************************* */
/* **************************************************************************** */
$(document).ready( function () {
    inicializarModais();
});

/**
 * INICIALIZAR MODAIS
 * -----------------------------------
 *
 * -----------------------------------
*/
function inicializarModais() {
	$("#modal-informacoes-complementares").on("show.bs.modal", function(e) {
	    carregarInformacoesModal(e, $(this));
	});

	$("#modal-infraestrutura").on("show.bs.modal", function(e) {
	    carregarInformacoesModal(e, $(this));
	});

	$("#modal-especificacoes").on("show.bs.modal", function(e) {
	    carregarInformacoesModal(e, $(this));
	});
}

/* **************************************************************************** */
/* ********************************* FUNCOES ********************************** */
/* **************************************************************************** */
/**
 * CARREGAR INFORMACOES MODAL
 * -----------------------------------
 *
 * -----------------------------------
*/
function carregarInformacoesModal(evento, $elementoModal) {
	var $elementoBotaoInfo = $(evento.relatedTarget);
	var idImovel = -1;
	var linkCargaImovel = $elementoBotaoInfo.attr('href');
	var arrayLinkCargaImovel = linkCargaImovel.split('/');

	if (arrayLinkCargaImovel.length) {
		idImovel = arrayLinkCargaImovel[arrayLinkCargaImovel.length - 1];
	}

	if (idImovel > 0) {
    	$elementoModal.find('.modal-body').load(linkCargaImovel);
	}
}
