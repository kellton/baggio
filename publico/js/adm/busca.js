/* CONSTANTES DE TEMPO DE ALERTA */
var CONS_TEMPO_ESCONDER_AUTOCOMPLETE = 0.3 * 1000;
var CONS_TEMPO_MOSTRAR_AUTOCOMPLETE = 0.3 * 1000;

var CONS_TEMPO_FILTRAR_BUSCA = 0.8 * 1000;

var CONS_MENOR_QUANTIDADE = 0;
var CONS_MAIOR_QUANTIDADE = 30;

/* GLOBAIS */
var timeoutBusca = null;
var idDropdownAtivo = "";
var usuarioSegurandoMouseFiltros = false;

/* **************************************************************************** */
/* ********************************* CHAMADAS ********************************* */
/* **************************************************************************** */
$(document).ready( function() {
	// ELEMENTOS DA BUSCA
	inicializarBuscaFinalidades();
	inicializarBuscaDropdowns();
	inicializarBuscaReferencias();
	inicializarBuscaCondominios();
	inicializarBuscaTipos();
	inicializarBuscaAbaSelecionada();
	inicializarToggleBusca();
	// ELEMENTOS DOS FILTROS
	inicializarFiltros();
	inicializarElementosFiltros();

	// BUSCA
	inicializarBusca();
	inicializarBuscaCopiar();

	inicializarAtualizacaoScroll();
});

/* **************************************************************************** */
/* ************************ FUNCOES DE INICIALIZACAO ************************** */
/* **************************************************************************** */
/* INCIALIZAR BUSCA FINALIDADES
 * *****************************
 * Inicializa os componentes de 
 * filtro de preco para verifi-
 * car qual opcao esta selecio-
 * nada e modificar o filtro de
 * acordo, venda ou aluguel.
 * *****************************
*/
function inicializarBuscaFinalidades() {
	$("[filter-change='verificar-dropdown-precos']").each( function() {
		$(this).change( function() {
			verificarDropdownPrecos();
		});
	});

	verificarDropdownPrecos();
}

/**
 * INCIALIZAR BUSCA DROPDOWNS
 * ***************************
 * Inicializa os dropdowns da 
 * busca para cada aba de tipo
 * de busca que houver.
 * ***************************
*/
function inicializarBuscaDropdowns() {
	// INICIALIZA O DROPDOWN DE TIPOS DE IMOVEL NA ABA POR BAIRRO
	inicializarBuscaDropdown(
		"dropdown-bairro-tipos-imoveis", 
		"Sel. um ou mais tipos", 
		"Não há imóveis deste tipo",
		"Tipo(s)"
	);
	// INICIALIZA O DROPDOWN DE BAIRROS NA ABA POR BAIRRO
	inicializarBuscaDropdown(
		"dropdown-bairro-bairros-cidade", 
		"Sel. um bairro", 
		"Não há bairros com esse nome",
		"Bairro(s)"
	);
	// INICIALIZA O DROPDOWN DE TIPOS DE IMOVEL NA ABA POR CONDOMINIO
	inicializarBuscaDropdown(
		"dropdown-condominio-tipos-imoveis", 
		"Sel. um ou mais tipos", 
		"Não há imóveis deste tipo",
		"Tipo(s)"
	);
	// DROPDOWN DE CIDADES DA ABA POR BAIRRO
	$("#dropdown-bairro-cidades").select2({
		allowClear: false,
		closeOnSelect: false,
		placeholder: "Selecione a cidade",
		width: "100%",
		language: {
			noResults: function() {
				return "Não há cidades com esse nome";
			}
		}
	}).ready( function() {
		// EVENTO PARA ATUALIZAR O DROPDOWN DE BAIRROS
		$("#dropdown-bairro-cidades").on('change', function (e) {
			updateDropdownBairros($(e.target));
		});

		updateDropdownBairros($("#dropdown-bairro-cidades"));
	});
}

function inicializarBuscaDropdown(paramID, paramPlaceholder, paramNoresults, selectedPlaceholder) {
	// CARREGA O DROPDOWN
	$("#" + paramID).select2({
		allowClear: true,
		closeOnSelect: false,
		placeholder: paramPlaceholder,
		width: "100%",
		language: {
			noResults: function() {
				return paramNoresults;
			}
		}
	}).ready( function() {
		// EVENTO DE SELECIONAR TODOS OS ITENS
		$('#' + paramID).on('select2:selecting', function (e) {
			selectAllItems($(e.params.args.data.element));
		});
		// EVENTO DE DESELECIONAR TODOS OS ITENS
		$('#' + paramID).on('select2:unselecting', function (e) {
			unselectAllItems($(e.params.args.data.element));
		});
		// EVENTO PARA CHECKAR QUANDO SELECIONAR TODOS OS ITENS
		$('#' + paramID).on('change', function (e) {
			checkAllSelected($(e.target));

			updateSearchInput($(e.target), selectedPlaceholder);
		});
		// DISPARAD O CHANGE
		setTimeout( function(){ $('#' + paramID).trigger('change') }, 0.5 * 1000 );
	});
}

/**
 * INICIALIZAR BUSCA REFERENCIA
 * ***********************
 *
 * ***********************
*/
function inicializarBuscaReferencias() {
	var $elementoBuscaReferencia = $("#text-busca-referencia");

	var atributosAutoComplete = {
		url: CONS_URL_BAGGIO + "async/buscar/referencias",

		list: {	
			match: {
				enabled: true
			},
			sort: {
				enabled: true
			},
			/*maxNumberOfElements: 10,*/
			showAnimation: {
				type: "slide",
				time: CONS_TEMPO_MOSTRAR_AUTOCOMPLETE
			},
			hideAnimation: {
				type: "slide",
				time: CONS_TEMPO_ESCONDER_AUTOCOMPLETE
			},
			onClickEvent: function() {
				$elementoBuscaReferencia.focus();
			}
		},

		theme: "square"
	};

	if ($elementoBuscaReferencia.length) {
		$elementoBuscaReferencia.easyAutocomplete(atributosAutoComplete);
	}
}

/**
 * INICIALIZAR BUSCA CONDOMINIO
 * ***********************
 *
 * ***********************
*/
function inicializarBuscaCondominios() {
	var $elementoBuscaCondominio = $("#text-busca-condominio");

	var atributosAutoComplete = {
		url: CONS_URL_BAGGIO + "async/buscar/condominios",

		list: {	
			match: {
				enabled: true
			},
			sort: {
				enabled: true
			},
			/*maxNumberOfElements: 10,*/
			showAnimation: {
				type: "slide",
				time: CONS_TEMPO_MOSTRAR_AUTOCOMPLETE
			},
			hideAnimation: {
				type: "slide",
				time: CONS_TEMPO_ESCONDER_AUTOCOMPLETE
			},
			onClickEvent: function() {
				$elementoBuscaCondominio.focus();
			}
		},

		theme: "square"
	};

	if ($elementoBuscaCondominio.length) {
		$elementoBuscaCondominio.easyAutocomplete(atributosAutoComplete);
	}
}

/**
 * INCIALIZAR BUSCA TIPOS
 * ***********************
 *
 * ***********************
*/
function inicializarBuscaTipos() {
	$("[muda-tipo-busca]").each( function() {
		$(this).click( function() {
			var tipo = $(this).attr("muda-tipo-busca");
			var $elementoInputTipoBusca = $("#elemento-busca-tipo");

			if ($elementoInputTipoBusca.length) {
				$elementoInputTipoBusca.val(tipo);
			}
		});
	});
}

/**
 * INCIALIZAR BUSCA
 * ***********************
 * 
 * ***********************
*/
function inicializarBusca() {
	$("[submit-busca]").each( function() {
		$(this).keypress( function(event) {
			dispararBusca(event); 
		})
	});

	$("[loader-content='elemento-loader-imoveis']").each(function() {
		$("#elemento-busca-botao-buscar").click( function() {
			buscar(true, true);
		});

		buscar(true, true);
	});
}

/**
 * INCIALIZAR COPIAR BUSCA
 * ***********************
 * 
 * ***********************
*/
function inicializarBuscaCopiar() {
	$('div.success').hide();

	$("[copiar-busca]").each( function() {
		$(this).click( function(event) {
			$("#temp_textarea").select();
			document.execCommand('copy');
			$('div.success').fadeIn(300).delay(500).fadeOut(400);
		});
	});
}

function prepararUrlDeCopia() {

	$.post(
		CONS_URL_BAGGIO + "async/buscar/copiar", 
		{ 
			
		},
		// SUCCESS
		function(data) {
			console.log(data);
			$("#temp_textarea").val("");
			$("#temp_textarea").val(data);
		}
	);
}

/**
 * INCIALIZAR BUSCA ABAS
 * ***********************
 * 
 * ***********************
*/
function inicializarBuscaAbaSelecionada() {
	$("[muda-tipo-busca]").each( function() {
		var $elementoTipoBusca = $("[name='param_tipo_busca']");
		
		if ($elementoTipoBusca.length) {
			var abaSelecionada = $elementoTipoBusca.val();
			var aba = $(this).attr("muda-tipo-busca");

			if (aba == abaSelecionada) {
				$(this).tab("show");
			}
		}
	});
}

/**
 * INCIALIZAR TOGGLE BUSCA
 * ***********************
 *
 * ***********************
*/
function inicializarToggleBusca() {
	var $elementoBotaoBusca = $("#elemento-botao-toggle-busca");
	var $elementoConteudoBusca = $("#elemento-conteudo-toggle-busca");
	
	if ($elementoBotaoBusca.length) {
		$elementoBotaoBusca.click( function() {
			$elementoConteudoBusca.slideToggle("fast", function() {
				var $elementoIconMostrarBusca = $("#elemento-icon-toggle-mostrar-busca");
				var $elementoIconEsconderBusca = $("#elemento-icon-toggle-esconder-busca");
				var $elementoOrdenacao = $(".caixa-busca-ordenacao");

				if ($elementoIconMostrarBusca.length && $elementoIconEsconderBusca.length) {
					$elementoIconMostrarBusca.toggle();
					$elementoIconEsconderBusca.toggle();
				}

				if ($elementoOrdenacao.length) {
					if ($elementoOrdenacao.hasClass("espacada")) {
						$elementoOrdenacao.removeClass("espacada");
					} else {
						$elementoOrdenacao.addClass("espacada");
					}
				}
			});
		});
	}
}


/**
 * INCIALIZAR FILTROS
 * ***********************
 *
 * ***********************
*/
function inicializarFiltros() {
	var $elementoBotaoFiltros = $("#elemento-botao-toggle-filtros");
	var $elementoConteudoFiltros = $("#elemento-conteudo-toggle-filtros");
	
	if ($elementoBotaoFiltros.length) {
		$elementoBotaoFiltros.click( function() {
			$elementoConteudoFiltros.slideToggle("fast", function() {
				var elementoDisplay = $(this).css("display");
				var $elementoIconMostrarFiltros = $("#elemento-icon-toggle-mostrar-filtros");
				var $elementoIconEsconderFiltros = $("#elemento-icon-toggle-esconder-filtros");

				if ($elementoIconMostrarFiltros.length && $elementoIconEsconderFiltros.length) {
					if (elementoDisplay === "none") {
						$elementoIconMostrarFiltros.css({
							"display": "inline-block"
						});
						$elementoIconEsconderFiltros.css({
							"display": "none" 
						});
					} else {
						$elementoIconMostrarFiltros.css({
							"display": "none"
						});
						$elementoIconEsconderFiltros.css({
							"display": "inline-block" 
						});
					}
				}
			});
		});
	}
}

/**
 * INCIALIZAR ELEMENTOS FILTROS
 * ***********************
 * 
 * ***********************
*/
function inicializarElementosFiltros() {
	$("[filter-change]").each( function() {
		$(this).change( function() {
			filtrar($(this), $(this).attr("filter-change"));
		});
	});

	$("[filter-click]").each( function() {
		$(this).click( function() {
			filtrar($(this), $(this).attr("filter-click"));
		});
	});

	$("[filter-order-change]").each( function() {
		$(this).change( function() {
			buscar(true, false);
		});
	});
}

/**
 * INCIALIZAR ATUALIZACAO SCROLL
 * ***********************
 * Inicializa o evento de scroll 
 * para busca de mais imoveis quando 
 * o scroll da caixa de resultados
 * chega ao fim.
 * ***********************
*/
function inicializarAtualizacaoScroll() {
	$(window).scroll( function() {
		atualizarConteudoScroll();
	});
}

/* **************************************************************************** */
/* ************************** FUNCOES DO SELECT2 ****************************** */
/* **************************************************************************** */
/**
 * CHECK SELECT ALL ITEMS
 * ***********************
 *
 * ***********************
*/
function selectAllItems($item) {
	if ($item.length && $item.text().trim() == 'Selecionar Todos') {
 		var $elementParent = $item.closest('select');
 
 		if ($elementParent.length) {
 			$elementParent.find('option').prop('selected', true);
 
 			// $elementParent.trigger('change');
 		}
 	}
}

/**
 * CHECK UNSELECT ALL ITEMS
 * ***********************
 *
 * ***********************
*/
function unselectAllItems($item) {
	if ($item.length && $item.text().trim() == 'Selecionar Todos') {
		var $elementParent = $item.closest('select');

		if ($elementParent.length) {
			$elementParent.find('option').prop('selected', false);

			// $elementParent.trigger('change');
		}
	}
}

/**
 * CHECK ALL SELECTED
 * ***********************
 *
 * ***********************
*/
function checkAllSelected($select) {
	if ($select.length) {
		var totalItems = $select.find('option:gt(0)').length;
		var totalSelected = totalItems;

		$select.find('option:gt(0)').each( function() {
			if (!$(this).prop('selected')) {
				totalSelected--;

				return;
			}
		});

		if (totalItems > 0 && totalItems == totalSelected) {
			$select.find('option').first().prop('selected', true);
		} else {
			$select.find('option').first().prop('selected', false);
		}
	}
}

/**
 * UPDATE DROPDOWN BAIRROS
 * ***********************
 *
 * ***********************
*/
function updateDropdownBairros($selectCidades) {
	if ($selectCidades.length) {
		var postingBairros = $.post(CONS_URL_BAGGIO + "async/buscar/bairros", 
			{ 
				"param_id_cidade": $selectCidades.val(),
			},
			// SUCCESS
			function(data) {
				if (data.bairros) {
					var bairros = data.bairros;
					var $selectBairros = $("#dropdown-bairro-bairros-cidade");

					if ($selectBairros.length && bairros.length) {
						var $optionsBairros = $selectBairros.find('option:gt(0)');

						if ($optionsBairros.length) {
							$optionsBairros.remove();
						}

						$.each( bairros, function( index, bairro ) {
							var optionBairro = new Option(
								bairro.nome_bairro, 
								bairro.id_bairro, bairro.selecionado, bairro.selecionado);

    						$selectBairros.append(optionBairro);
						});

						setTimeout( function(){ $selectBairros.trigger('change'); }, 0.5 * 1000 );
					}
				}
			}
		);

		postingBairros.fail( function() {
			console.log("Ocorreu um erro ao tentar recuperar os bairros da cidade de ID = " + $selectCidades.val() + ".");
		});
	}
}

/**
 * UPDATE SEARCH INPUT
 * ***********************
 *
 * ***********************
*/
function updateSearchInput($select, text) {
	if ($select.length) {
		var $elementSelectParent = $select.parent();

		if ($elementSelectParent.length) {
			var $selectInput = $elementSelectParent.find("input.select2-search__field");
			
			if ($selectInput.length) {
				var totalSelectedItems = $select.find('option:gt(0)').filter(":selected").length;
				
				if (totalSelectedItems > 0) {
					$selectInput.attr(
						"placeholder", 
						"( " + totalSelectedItems + " ) " + text);
					$selectInput.css({
						"width": "auto"
					});
				}
			}
		}
	}
}

/* **************************************************************************** */
/* **************************** FUNCOES DE ACAO ******************************* */
/* **************************************************************************** */
/**
 * DISPARAR BUSCA
 * ***********************
 *
 * ***********************
*/
function dispararBusca(e) {
	if (e.which == 13) {
		e.preventDefault();

		buscar(true, false);
	}
}

/**
 * VERIFICAR MODIFICACAO DROPDOWN PRECOS
 * ***********************
 *
 * ***********************
*/
function verificarDropdownPrecos() {
	var $elementoRadioPrecoSelecionado = $("[name='param_negociacao']:checked");

	if ($elementoRadioPrecoSelecionado.length) {
		var valorEscolhido = $elementoRadioPrecoSelecionado.val();
		var $elementoSelectMenorPreco = $("[name='param_menor_preco']");
		var $elementoSelectMaiorPreco = $("[name='param_maior_preco']");

		if ($elementoSelectMenorPreco.length) {
			$elementoSelectMenorPreco.children("option").remove();
		}

		if ($elementoSelectMaiorPreco.length) {
			$elementoSelectMaiorPreco.children("option").remove();
		}
		
		if (valorEscolhido == 1) {
			// VENDA
			if ($elementoSelectMenorPreco.length) {
				adicionarValoresDropdownPrecos(
					$elementoSelectMenorPreco, 
					CONS_OPCOES_COMPRA_MENOR_PRECO, 
					0);
			}
			if ($elementoSelectMaiorPreco.length) {
				adicionarValoresDropdownPrecos(
					$elementoSelectMaiorPreco, 
					CONS_OPCOES_COMPRA_MAIOR_PRECO, 
					CONS_OPCOES_COMPRA_MAIOR_PRECO.length - 1);
			}
		} else if (valorEscolhido == 2) {
			// ALUGUEL
			if ($elementoSelectMenorPreco.length) {
				adicionarValoresDropdownPrecos(
					$elementoSelectMenorPreco, 
					CONS_OPCOES_ALUGUEL_MENOR_PRECO, 
					0);
			}
			if ($elementoSelectMaiorPreco.length) {
				adicionarValoresDropdownPrecos(
					$elementoSelectMaiorPreco, 
					CONS_OPCOES_ALUGUEL_MAIOR_PRECO, 
					CONS_OPCOES_ALUGUEL_MAIOR_PRECO.length - 1);
			}
		}
	}
}

/**
 * ADICIONAR VALORES DROPDOWNPRECOS
 * ***********************
 *
 * ***********************
*/
function adicionarValoresDropdownPrecos($elemento, array, indiceSelecionado) {
	$.each(array, function(indice, valor) {
		if (indice == indiceSelecionado) {
			$elemento.append($("<option></option>")
 				.attr("value", valor)
 				.attr("selected", "selected")
 				.text(valor));
		} else {
			$elemento.append($("<option></option>")
 				.attr("value", valor)
 				.text(valor));
		}
	});
}

/**
 * ATUALIZAR CONTEUDO SCROLL
 * ***********************
 *
 * ***********************
*/
function atualizarConteudoScroll() {
	var $elementoFooter = $(".caixa-footer");

	if ($elementoFooter.length) {
		var alturaFooter = $elementoFooter.height();
		var alturaDocumento = $(document).height();
		var alturaWindow = $(window).height();
		var topWindow = $(window).scrollTop();

		if( topWindow + alturaWindow >= alturaDocumento - ( alturaFooter / 2 ) ) {
			var $elementoInicio = $("[name='param_inicio']");

			if ($elementoInicio.length) {
				var inicioAtual = parseInt($elementoInicio.val());

				$elementoInicio.val(inicioAtual + 1);

				/*$(window).scrollTop(topWindow - 20);*/

				buscar(false, false);
			}
		}
	}
}

/**
 * FILTRO VERIFICAR MENOR PRECO
 * ***********************
 *
 * ***********************
*/
function filtroVerificarMenorPreco($elementoDisparador) {
	var valorMaiorSelecionado = parseInt($elementoDisparador.val().replace(/\./g, "").replace(/,/g, "."));
	var $elementoMenorPreco = $("[name='param_menor_preco']");

	if ($elementoMenorPreco.length) {
		var $opcoesMenorPreco = $elementoMenorPreco.find("option");

		for (var i = $opcoesMenorPreco.length - 1; i >= 0; i--) {
			var $opcaoMenorPreco = $($opcoesMenorPreco[i]);
			var valorMenorPreco = parseInt($opcaoMenorPreco.val().replace(/\./g, "").replace(/,/g, "."));

			if (valorMenorPreco <= valorMaiorSelecionado) {
				$opcaoMenorPreco.prop("disabled", false);
			} else {
				if (valorMenorPreco == 300000) {
					$opcaoMenorPreco.prop("disabled", true);
				}
				$opcaoMenorPreco.prop("disabled", true);
			}
		};
	}
}

/**
 * FILTRO VERIFICAR MAIOR PRECO
 * ***********************
 *
 * ***********************
*/
function filtroVerificarMaiorPreco($elementoDisparador) {
	var valorMenorSelecionado = parseInt($elementoDisparador.val().replace(/\./g, "").replace(/,/g, "."));
	var $elementoMaiorPreco = $("[name='param_maior_preco']");

	if ($elementoMaiorPreco.length) {
		var $opcoesMaiorPreco = $elementoMaiorPreco.find("option");

		for (var i = $opcoesMaiorPreco.length - 1; i >= 0; i--) {
			var $opcaoMaiorPreco = $($opcoesMaiorPreco[i]);
			var valorMaiorPreco = parseInt($opcaoMaiorPreco.val().replace(/\+|\./g, "").replace(/,/g, "."));

			if (valorMaiorPreco < valorMenorSelecionado) {
				$opcaoMaiorPreco.prop("disabled", true);
			} else {
				$opcaoMaiorPreco.prop("disabled", false);

				$elementoMaiorPreco.val($opcaoMaiorPreco.val());
			}
		};
	}
}

/**
 * FILTRO DIMINUIR QUANTIDADE QUARTOS
 * ***********************
 *
 * ***********************
*/
function filtroDiminuirQuantidadeQuartos($elementoDisparador) {
	var $elementoNumberQuartos = $("[name='param_qtd_quartos']");

	if ($elementoNumberQuartos.length) {
		var valorAtual = parseInt($elementoNumberQuartos.val());

		$elementoNumberQuartos.val( Math.max(CONS_MENOR_QUANTIDADE, valorAtual - 1) );
	}
}

/**
 * FILTRO AUMENTAR QUANTIDADE QUARTOS
 * ***********************
 *
 * ***********************
*/
function filtroAumentarQuantidadeQuartos($elementoDisparador) {
	var $elementoNumberQuartos = $("[name='param_qtd_quartos']");

	if ($elementoNumberQuartos.length) {
		var valorAtual = parseInt($elementoNumberQuartos.val());

		$elementoNumberQuartos.val( Math.min(CONS_MAIOR_QUANTIDADE, valorAtual + 1) );
	}
}

/**
 * FILTRO DIMINUIR QUANTIDADE SUITES
 * ***********************
 *
 * ***********************
*/
function filtroDiminuirQuantidadeBanheiros($elementoDisparador) {
	var $elementoNumberBanheiros = $("[name='param_qtd_banheiros']");

	if ($elementoNumberBanheiros.length) {
		var valorAtual = parseInt($elementoNumberBanheiros.val());

		$elementoNumberBanheiros.val( Math.max(CONS_MENOR_QUANTIDADE, valorAtual - 1) );
	}
}

/**
 * FILTRO AUMENTAR QUANTIDADE BANHEIROS
 * ***********************
 *
 * ***********************
*/
function filtroAumentarQuantidadeBanheiros($elementoDisparador) {
	var $elementoNumberBanheiros = $("[name='param_qtd_banheiros']");

	if ($elementoNumberBanheiros.length) {
		var valorAtual = parseInt($elementoNumberBanheiros.val());

		$elementoNumberBanheiros.val( Math.min(CONS_MAIOR_QUANTIDADE, valorAtual + 1) );
	}
}

/**
 * FILTRO DIMINUIR QUANTIDADE VAGAS
 * ***********************
 *
 * ***********************
*/
function filtroDiminuirQuantidadeVagas($elementoDisparador) {
	var $elementoNumberVagas = $("[name='param_qtd_vagas']");

	if ($elementoNumberVagas.length) {
		var valorAtual = parseInt($elementoNumberVagas.val());

		$elementoNumberVagas.val( Math.max(CONS_MENOR_QUANTIDADE, valorAtual - 1) );
	}
}

/**
 * FILTRO AUMENTAR QUANTIDADE VAGAS
 * ***********************
 *
 * ***********************
*/
function filtroAumentarQuantidadeVagas($elementoDisparador) {
	var $elementoNumberVagas = $("[name='param_qtd_vagas']");

	if ($elementoNumberVagas.length) {
		var valorAtual = parseInt($elementoNumberVagas.val());

		$elementoNumberVagas.val( Math.min(CONS_MAIOR_QUANTIDADE, valorAtual + 1) );
	}
}

/**
 * FILTRO VERIFICAR MENOR AREA
 * ***********************
 *
 * ***********************
*/
function filtroVerificarMenorArea($elementoDisparador) {
	var valorMaiorSelecionado = parseInt($elementoDisparador.val());
	var $elementoMenorArea = $("[name='param_menor_area']");

	if ($elementoMenorArea.length) {
		var $opcoesMenorArea = $elementoMenorArea.find("option");

		for (var i = $opcoesMenorArea.length - 1; i >= 0; i--) {
			var $opcaoMenorArea = $($opcoesMenorArea[i]);
			var valorMenorArea = parseInt($opcaoMenorArea.val().replace("+", ""));

			if (valorMenorArea <= valorMaiorSelecionado) {
				$opcaoMenorArea.prop("disabled", false);
			} else {
				$opcaoMenorArea.prop("disabled", true);
			}
		};
	}
}

/**
 * FILTRO VERIFICAR MAIOR AREA
 * ***********************
 *
 * ***********************
*/
function filtroVerificarMaiorArea($elementoDisparador) {
	var valorMenorSelecionado = parseInt($elementoDisparador.val());
	var $elementoMaiorArea = $("[name='param_maior_area']");

	if ($elementoMaiorArea.length) {
		var $opcoesMaiorArea = $elementoMaiorArea.find("option");

		for (var i = $opcoesMaiorArea.length - 1; i >= 0; i--) {
			var $opcaoMaiorArea = $($opcoesMaiorArea[i]);
			var valorMaiorArea = parseInt($opcaoMaiorArea.val());

			if (valorMaiorArea < valorMenorSelecionado) {
				$opcaoMaiorArea.prop("disabled", true);
			} else {
				$opcaoMaiorArea.prop("disabled", false);

				$elementoMaiorArea.val($opcaoMaiorArea.val());
			}
		};
	}
}


/**
 * FILTRAR
 * ***********************
 *
 * ***********************
*/
function filtrar($elementoDisparador, funcao) {
	limparTimeoutsFiltro();

	/* MENOR PRECO */
	if (funcao == "verificar-menor-preco") {
		filtroVerificarMenorPreco($elementoDisparador);
	}
	/* MAIOR PRECO */
	if (funcao == "verificar-maior-preco") {
		filtroVerificarMaiorPreco($elementoDisparador);
	}
	/* DIMINUIR QUARTOS */
	if (funcao == "diminuir-quartos") {
		filtroDiminuirQuantidadeQuartos($elementoDisparador);
	}
	/* AUMENTAR QUARTOS */
	if (funcao == "aumentar-quartos") {
		filtroAumentarQuantidadeQuartos($elementoDisparador);
	}
	/* DIMINUIR BANHEIROS */
	if (funcao == "diminuir-banheiros") {
		filtroDiminuirQuantidadeBanheiros($elementoDisparador);
	}
	/* AUMENTAR BANHEIROS */
	if (funcao == "aumentar-banheiros") {
		filtroAumentarQuantidadeBanheiros($elementoDisparador);
	}
	/* DIMINUIR VAGAS */
	if (funcao == "diminuir-vagas") {
		filtroDiminuirQuantidadeVagas($elementoDisparador);
	}
	/* AUMENTAR VAGAS */
	if (funcao == "aumentar-vagas") {
		filtroAumentarQuantidadeVagas($elementoDisparador);
	}
	/* MENOR AREA */
	if (funcao == "verificar-menor-area") {
		filtroVerificarMenorArea($elementoDisparador);
	}
	/* MAIOR AREA */
	if (funcao == "verificar-maior-area") {
		filtroVerificarMaiorArea($elementoDisparador);
	}

	timeoutBusca = setTimeout( function(){ buscar(true, false) }, CONS_TEMPO_FILTRAR_BUSCA );
}

/**
 * BUSCAR
 * ***********************
 *
 * ***********************
*/
function buscar(limparConteudo, zerarFiltros) {
	/* view_busca */
	var $elementoNegociacao = $("[name='param_negociacao']:checked");
	var $elementoTipoBusca = $("[name='param_tipo_busca']");
	var $elementoInicio = $("[name='param_inicio']");
	/* busca_bairro */
	var $elementoIdsTiposImoveis = $("[name='param_tipos[]']");
	var $elementoIdCidade = $("[name='param_cidade']");
	var $elementoIdsBairros = $("[name='param_bairros[]']");
	/* busca_referencia */
	var $elementoReferencia = $("[name='param_referencia']");
	/* busca_condominio */
	var $elementoCondominio = $("[name='param_condominio']");
	var $elementoCondominios = $("[name='param_tipos_condominios[]']");
	/* filtros */
	var $elementoMenorPreco = $("[name='param_menor_preco']");
	var $elementoMaiorPreco = $("[name='param_maior_preco']");
	var $elementoQtdQuartos = $("[name='param_qtd_quartos']");
	var $elementoQtdBanheiros = $("[name='param_qtd_banheiros']");
	var $elementoQtdVagas = $("[name='param_qtd_vagas']");
	var $elementoMenorArea = $("[name='param_menor_area']");
	var $elementoMaiorArea = $("[name='param_maior_area']");
	/* ordenacao */
	var $elementoOrdenacao = $("[name='param_ordenacao']");

	var $elementoConteudo = $("[loader-content='elemento-loader-imoveis']");
	var $elementoLoaderGIF = $("[loader-container='elemento-loader-imoveis']");
	var $elementoSemResultados = $("#elemento-sem-resultados");

	if ($elementoInicio.length && limparConteudo) {
		$elementoInicio.val(0);
	}

	if ($elementoLoaderGIF.length && limparConteudo) {
		$elementoLoaderGIF.show();
	}

	if (zerarFiltros) {
		//ZERAR OS FILTROS
	}

	var postingFiltro = $.post(CONS_URL_BAGGIO + "async/buscar/imoveis", 
		{ 
			"param_negociacao": $elementoNegociacao.val(),
			"param_tipo_busca": $elementoTipoBusca.val(),
			"param_inicio": $elementoInicio.val(),
			"param_tipos[]": $elementoIdsTiposImoveis.val(),
			"param_cidade": $elementoIdCidade.val(),
			"param_bairros[]": $elementoIdsBairros.val(),
			"param_referencia": $elementoReferencia.val(),
			"param_condominio": $elementoCondominio.val(),
			"param_tipos_condominios[]": $elementoCondominios.val(),
			"param_menor_preco": $elementoMenorPreco.val(),
			"param_maior_preco": $elementoMaiorPreco.val(),
			"param_qtd_quartos": $elementoQtdQuartos.val(),
			"param_qtd_banheiros": $elementoQtdBanheiros.val(),
			"param_qtd_vagas": $elementoQtdVagas.val(),
			"param_menor_area": $elementoMenorArea.val(),
			"param_maior_area": $elementoMaiorArea.val(),
			"param_ordenacao": $elementoOrdenacao.val()
		},
		//SUCCESS
		function(data) {
			//console.log(data);
			//$elementoConteudo.html(data);
			//$elementoLoaderGIF.hide();

			if ($elementoConteudo.length && limparConteudo) {
				$elementoConteudo.empty();
				$elementoConteudo.html(data.dados);
			} else if ($elementoConteudo.length) {
				$elementoConteudo.append(data.dados);
			}

			if ($elementoLoaderGIF.length) {
				$elementoLoaderGIF.hide();
			}

			if ($elementoSemResultados.length) {
				if (data.qtd_imoveis > 0) {
					$elementoSemResultados.hide();

					atualizarLabelQuantificador(data.qtd_imoveis);

					inicializarTooltips();
					inicializarVerificacaoFavoritos();
				} else if (limparConteudo || $elementoConteudo.html().length == 0) {
					$elementoSemResultados.show();

					atualizarLabelQuantificador(0);
				}
			}
		}
	);

	postingFiltro.fail( function() {
		console.log("Ocorreu um erro ao tentar recuperar as informações.");

		if ($elementoConteudo.length && limparConteudo) {
			$elementoConteudo.empty();
		}

		if ($elementoLoaderGIF.length) {
			$elementoLoaderGIF.hide();
		}

		if ($elementoSemResultados.length && limparConteudo) {
			$elementoSemResultados.show();

			atualizarLabelQuantificador(0);
		}
	});

	prepararUrlDeCopia();
}

/**
 * ATUALIZAR LABEL QUANTIFICADOR
 * ***********************
 *
 * ***********************
*/
function atualizarLabelQuantificador(quantidade) {
	var $labelQuantificador = $("#search-quantidade-resultado-imoveis");

	if ($labelQuantificador.length) {
		if (quantidade > 1) {
			$labelQuantificador.html(quantidade + " imóveis encontrados.");
		} else {
			$labelQuantificador.html(quantidade + " imóvel encontrado.");
		}
	}
}

/**
 * LIMPAR TIMEOUTS FILTRO
 * ***********************
 *
 * ***********************
*/
function limparTimeoutsFiltro() {
	if (timeoutBusca != null) { 
		window.clearTimeout(timeoutBusca); 
	}
}


