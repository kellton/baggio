$(document).ready(function(){
	inicializarEventosMenu();
	inicializarToogleAvisos();
	inicializarBotaoEnvio();

	inicializarBuscaCep();

	
	//inicializarEdicaoTogglerSelect();
});

/**
 * INCIALIZAR ELEMENTOS AVISOS
 * ***********************
 * Funcao para incializacao elementos 
 * que farão o efeito de mostrar o aviso.
 * ***********************
*/
function inicializarEventosMenu() {
	$(".menu-fichas .nav-link").click(function(){
        inicializarToogleAvisos();
        //inicializarBuscaCep();
        
    });

    $("[clonar-elemento]").click(function(){
        inicializarToogleAvisos();
    });
}

function inicializarToogleAvisos() {
	$("[togvis-estado=hide]").show();

	$("[aviso-texto]").hide();
	//$("[aviso-texto]").css({visibility: 'hidden'});

	$("[aviso-bloco]").each( function() {
		$bloco = $(this);

		$bloco.find('[aviso-campo]').focus(function(){
			$(this).siblings("[aviso-texto]").slideDown('fast');
	        //$(this).siblings("[aviso-texto]").fadeIn('fast');
	        //$(this).siblings("[aviso-texto]").css('visibility', 'visible');
	    });
	    $bloco.find('[aviso-campo]').blur(function(){
	    	$(this).siblings("[aviso-texto]").slideUp('fast');
	        //$(this).siblings("[aviso-texto]").fadeOut('fast');
	        //$(this).siblings("[aviso-texto]").css('visibility', 'hidden');
	    });
	});

	$("[togvis-estado=hide]").hide();
	inicializarEdicaoTogglerSelect();
}

/**
 * INCIALIZAR ELEMENTOS DA EDIÇÃO
 * ***********************
 * Funcao para preparar elementos
 * escondidos para edição.
 * ***********************
*/

function inicializarEdicaoTogglerSelect() {
    
    //for($i=0; $i<3; $i++){
        $("[togvis-button]").each( function() {
            $(this).trigger("change");
        });

        $("[togvis-select]").each( function() {
            $(this).trigger("change");
        });
    //}
}

/**
 * INCIALIZAR BOTÕES
 * ***********************
 * Funcao para inicializar os
 * botões usados para envio e
 * salvamento das fichas.
 * ***********************
*/

function inicializarBotaoEnvio() {
	$("[numero-aba]").click(function(){
        $('[proxima-etapa]').hide();

        $('[proxima-etapa="'+$(this).attr("numero-aba")+'"]').show();
    });

	$("[proxima-etapa]").click(function(){
		proxima = parseInt($(this).attr('proxima-etapa')) + 1;

		console.log(proxima);

        $('[numero-aba="'+ proxima +'"]').trigger("click");
    });

    $('[proxima-etapa]').hide();
    $('[numero-aba="0"]').trigger("click");
	$('[proxima-etapa="0"]').show();
}

//----------------------------------------
//----------------------------------------
//----------------------------------------
//----------------------------------------
//----------------------------------------

/**
 * FUNÇÕES RELATIVAS AO TRATAMENTO DE CEP
 * ***********************
 * Funcõs utilizando o web service
 * fornecido pelos correios.
 * ***********************
*/

function limpaFormularioCep() {
    // Limpa valores do formulário de cep.
    $("#rua").val("");
    $("#bairro").val("");
    $("#cidade").val("");
    $("#uf").val("");
    $("#ibge").val("");
}

function inicializarBuscaCep() {
	$("[togvis-estado=hide]").show();

	//Quando o campo cep perde o foco.
    $("[cep-bloco]").each(function() {
    	$(this).blur(function() {

	        //Nova variável "cep" somente com dígitos.
	        var cep = $(this).val().replace(/\D/g, '');
	        var bloco = $(this).attr("cep-bloco");

	        //Verifica se campo cep possui valor informado.
	        if (cep != "") {

	            //Expressão regular para validar o CEP.
	            var validacep = /^[0-9]{8}$/;

	            //Valida o formato do CEP.
	            if(validacep.test(cep)) {

	                //Preenche os campos com "..." enquanto consulta webservice.
	                $("[cep-rua='"+bloco+"']").val("...");
	                $("[cep-bairro='"+bloco+"']").val("...");
	                $("[cep-cidade='"+bloco+"']").val("...");
	                $("[cep-uf='"+bloco+"']").val("...");
	                //$("#ibge").val("...");
	                
	                //Consulta o webservice viacep.com.br/
	                $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

	                    if (!("erro" in dados)) {
	                        //Atualiza os campos com os valores da consulta.
	                        $("[cep-rua='"+bloco+"']").val(dados.logradouro);
	                        $("[cep-bairro='"+bloco+"']").val(dados.bairro);
	                        $("[cep-cidade='"+bloco+"']").val(dados.localidade);
	                        $("[cep-uf='"+bloco+"']").val(dados.uf);
	                        //$("#ibge").val(dados.ibge);
	                    } //end if.
	                    else {
	                        //CEP pesquisado não foi encontrado.
	                        limpaFormularioCep();
	                        console.log("CEP não encontrado.");
	                    }
	                });
	            } //end if.
	            else {
	                //cep é inválido.
	                limpaFormularioCep();
	                console.log("Formato de CEP inválido.");
	            }
	        } //end if.
	        else {
	            //cep sem valor, limpa formulário.
	            limpaFormularioCep();
	        }
	    });
	});

	$("[togvis-estado=hide]").hide();
}