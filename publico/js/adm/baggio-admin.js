$(document).ready( function() {
	inicializarDropdownBairrosAuxiliares();
});

function inicializarDropdownBairrosAuxiliares() {
	$("#dropdown-aux-bairros").select2({
		allowClear: true,
		closeOnSelect: false,
		placeholder: "Selecione o bairro de referência",
		width: "100%",
		language: {
			noResults: function() {
				return "Não houve resultados para esta busca...";
			}
		}
	});
}
