/* **************************************************************************** */
/* ********************************* CHAMADAS ********************************* */
/* **************************************************************************** */
$(document).ready( function () {
    inicializarTogglers();
    inicializarTogglersSelect();

    inicializarTogglerElements();


    //inicializarEdicaoTogglerSelect();
});

/* **************************************************************************** */
/* ********************************* FUNCOES ********************************** */
/* **************************************************************************** */
/**
 * INICIALIZAR TOGGLERS
 * ***********************
 * Funcao para iniciliazacao
 * dos elementos que farao a
 * funcao de modificar o "estado"
 * de seus repectivos "filhos".
 * ***********************
 */
 function inicializarTogglers() {
    $("[togvis-button]").each( function() {

        $(this).change( function() {
            var elementoID = $(this).attr("togvis-button");
            if (elementoID) {
                var $elementoToggle = $("[togvis-id='" + elementoID + "']");

                if ($elementoToggle.length) {
                    var esconderOutros = $elementoToggle.attr("togvis-outros") == "esconder";

                    if ($(this).is(":checked")) {
                        var grupoToggle = $elementoToggle.attr("togvis-grupo");

                        if (esconderOutros) {
                            $("[togvis-grupo='" + grupoToggle + "']").slideUp('fast');
                        }
                        $elementoToggle.slideDown('fast');
                    }
                }
            }
        });
    });
}

/**
 * INICIALIZAR TOGGLERS SELECT
 * ***********************
 * Funcao para iniciliazacao
 * dos elementos select que farao a
 * funcao de modificar o "estado"
 * de visao de outros elementos a
 * partir de um option.
 * ***********************
 */
 function inicializarTogglersSelect() {
    $("select[togvis-select]").each( function() {
        $(this).change( function() {
            var $opcaoSelecionada = $("option:selected", this);
            //console.log($opcaoSelecionada);
            if ($opcaoSelecionada.length) {
                var id = $opcaoSelecionada.attr("togvis-option");

                if (id) {
                    //console.log(id);
                    var $elementoToggle = $("[togvis-id='" + id + "']");

                    if ($elementoToggle.length) {
                        var esconderOutros = $opcaoSelecionada.attr("togvis-outros") == "esconder";
                        var grupoToggle = $elementoToggle.attr("togvis-grupo");

                        if (esconderOutros) {
                            $("[togvis-grupo='" + grupoToggle + "']").slideUp('fast');
                        }
                        $elementoToggle.slideDown('fast');
                    }
                } else {
                    var esconderGrupo = $opcaoSelecionada.attr("togvis-esconder-grupo");

                    if (esconderGrupo) {
                        $("[togvis-grupo='" + esconderGrupo + "']").slideUp('fast');
                    }
                }
            }
        });
    });
}

/**
 * INCIALIZAR TOGGLER ELEMENTS
 * ***********************
 * Funcao para incializacao dos
 * elementos.
 * ***********************
 */
 function inicializarTogglerElements() {
    esconderTogglerElements();
}

/**
 * ESCONDER TOGGLER ELEMENTS
 * ***********************
 * Funcao para esconder os
 * elementos de visualizacao
 * que contem o atributo hide.
 * ***********************
 */
 function esconderTogglerElements() {
    $("[togvis-id][togvis-estado='hide']").hide();
}

/**
 * EDIÇÃO TOGGLER ELEMENTS
 * ***********************
 * Chama as funções para
 * Atualizar elementos na
 * edicao.
 * ***********************
 */


/*function inicializarEdicaoTogglerSelect() {
    console.log("teste");
    for($i=0; $i<3; $i++){
        $("[togvis-button]").each( function() {
            $(this).trigger("change");
        });

        $("select[togvis-select]").each( function() {
            $(this).trigger("change");
        });
    }
}*/

