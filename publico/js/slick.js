$(document).ready(function (){
    inicializarCarouselCardboard();
    inicializarCarouselImovel();
});

function inicializarCarouselCardboard() {
	$('.carousel-imoveis').slick({
		dots: true,
		infinite: true,
		speed: 300,
		autoplay: false,
  		//autoplaySpeed: 3000,
  		arrows: false,
		slidesToShow: 2,
		slidesToScroll: 2,
		responsive: [
		    {
		      	breakpoint: 995,
		      	settings: {
			        slidesToShow: 1,
			        slidesToScroll: 1
		      	}
		    }
		  ]
	});
}

function inicializarCarouselImovel() {
	$('.carousel-imovel').slick({
		centerMode: true,
		centerPadding: '60px',
		slidesToShow: 1,
		focusOnSelect: true,
		dots: true
	});
}