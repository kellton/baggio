/* **************************************************************************** */
/* ********************************* CHAMADAS ********************************* */
/* **************************************************************************** */
$(document).ready( function() {
	fecharAlertas();

	/*inicializarAlturaBuscaInicial();*/
	inicializarModificacoesAltura();
	inicializarElementosClonar();
	inicializarElementosRolagem();

	inicializarSubmitEnter();

	inicializarSlideImoveis();

	inicializarTooltips();
	inicializarPopovers();
	
	inicializarVerificacaoFavoritos();
	inicializarElementosConfirmacao();
});

/* **************************************************************************** */
/* ********************************* FUNCOES ********************************** */
/* **************************************************************************** */


/**
 * INCIALIZAR MODIFICACOES ALTURA
 * ***********************
 * Funcao para incializacao elementos que mudam altura
 * ***********************
*/
function inicializarModificacoesAltura() {
	$("[modifica-altura]").each( function() {
		inicializarAlturaContainer($(this));
	});
}

/**
 * INCIALIZAR ELEMENTOS CLONAR
 * ***********************
 * Funcao para incializacao elementos 
 * que farão o efeito de clone.
 * ***********************
*/
function inicializarElementosClonar() {
	$("[clonar-elemento]").each( function() {
		inicializarBotoesClonar($(this));
	});
}

/**
 * INCIALIZAR ELEMENTOS ROLAGEM
 * ***********************
 * Funcao para incializacao elementos 
 * que farão o efeito de rolagem suave.
 * ***********************
*/
function inicializarElementosRolagem() {
	$("[rolagem=cadastro-imovel-negociacao]").click( function(e) {
		rolagemSuave(e, $(this));
	});
	$("[rolagem=cadastro-imovel-informacoes-basicas]").click( function(e) {
		rolagemSuave(e, $(this));
	});
	$("[rolagem=cadastro-imovel-veiculacao]").click( function(e) {
		rolagemSuave(e, $(this));
	});
	$("[rolagem=cadastro-imovel-fotos]").click( function(e) {
		rolagemSuave(e, $(this));
	});
	$("[rolagem=cadastro-imovel-informacoes-complementares]").click( function(e) {
		rolagemSuave(e, $(this));
	});
	$("[rolagem=cadastro-imovel-material]").click( function(e) {
		rolagemSuave(e, $(this));
	});
	$("[rolagem=cadastro-imovel-dados-gerenciamento]").click( function(e) {
		rolagemSuave(e, $(this));
	});
}

/**
 * INCIALIZAR ENDERECOS AUTOMATICAMENTE
 * ***********************
 * Funcao para incializacao funcionalidade
 * de busca automatica de endereco a partir
 * do CEP.
 * ***********************
*/
function inicializarEnderecoAuto() {
	inicializarEnderecoAutoClick();
	inicializarEnderecoAutoKeyup();
}

/**
 * INICIALIZAR ENDERECO AUTO CLICK
 * *****************************
 * DESCRICAO: 
 * *****************************
*/
function inicializarEnderecoAutoClick() {
	$("[endereco-auto=click]").click( function (e) {
		e.preventDefault();

		if ($("[mascara='cep']").val().length) {
			atualizarEnderecoComCEP($("[mascara='cep']").val());
		}
	});
}

/**
 * INICIALIZAR ENDERECO AUTO KEYUP
 * *****************************
 * DESCRICAO: 
 * *****************************
*/
function inicializarEnderecoAutoKeyup() {
	$("[endereco-auto=keyup]").each( function() {
		$(this).keyup( function (e) {
			if ($("[mascara='cep']").val().length == 9) {
				atualizarEnderecoComCEP($("[mascara='cep']").val());
			}
		})
	});
}

/**
 * INICIALIZAR ALTURA CONTAINERS
 * *****************************
 * DESCRICAO: Funcao que calcula
 * a altura de containers com
 * atributos configurados.
 * *****************************
*/
function inicializarAlturaContainer($elementoContainer) {
 	if ($elementoContainer.length) {
 		var windowHeight = $(window).outerHeight(true);
 		var windowPercentage = parseInt($elementoContainer.attr("modifica-altura"));

 		if ($elementoContainer.length) {
 			$elementoContainer.height( windowHeight * ( windowPercentage / 100 ) );
 		}
 	}
 }

/**
 * INICIALIZAR ALTURA BUSCA INICIAL
 * *****************************
 * DESCRICAO: 
 * *****************************
 */
 function inicializarAlturaBuscaInicial() {
 	var $elementoContainerBusca = $("#elemento-container-busca-inicial");

 	if ($elementoContainerBusca.length) {
 		var windowHeight = $(window).outerHeight(true);
 		var $elementoMenuTopo = $("#elemento-menu-topo");
 		var $elementoMenu = $("#elemento-menu");

 		if ($elementoContainerBusca.length && $elementoMenu.length && $elementoMenuTopo.length) {
 			var menuTopoHeight = $elementoMenuTopo.outerHeight(true);
 			var menuHeight = $elementoMenu.outerHeight(true);

 			$elementoContainerBusca.height( ( windowHeight * 0.60 ) - menuHeight - menuTopoHeight);
 		}
 	}
 }

/**
 * INICIALIZAR BOTOES CLONAR
 * ***********************
 * 
 * ***********************
 */
 function inicializarBotoesClonar($elementoBotao) {
 	$elementoBotao.click( function() {
 		clonarElemento($(this));
 	});
 }

/**
 * INCIALIZAR SUBMIT ENTER
 * ***********************
 *
 * ***********************
*/
function inicializarSubmitEnter() {
	$("[submit-enter]").each( function() {
		$(this).keypress( function(event) {
			dispararSubmit(event); 
		})
	});
}

/**
 * INCIALIZAR SLIDE IMOVEIS
 * ***********************
 *
 * ***********************
*/
function inicializarSlideImoveis() {
	$('#gallery-fotos-imovel').lightSlider({
		autoWidth: false,
	    gallery: true,
	    item: 1,
	    loop: true,
	    slideMargin: 2,
	    thumbItem: 9
	});
}

/**
 * INCIALIZAR TOOLTIPS
 * ***********************
 *
 * ***********************
*/
function inicializarTooltips() {
	$( function() {
		$('[data-toggle="tooltip"]').tooltip();
	});
}

/**
 * INCIALIZAR POPOVERS
 * ***********************
 *
 * ***********************
*/
function inicializarPopovers() {
	$(function () {
		$('[data-toggle="popover"]').popover()
	});
}

/**
 * INCIALIZAR VERIFICACAO FAVORITOS
 * ***********************
 *
 * ***********************
*/
function inicializarVerificacaoFavoritos() {
	var baggioCK = buscarBaggioCookie();

	if (baggioCK.length) {
		var idsImoveisFavoritos = baggioCK.split(CONS_COOKIE_SEPARADOR);

		for (var i = idsImoveisFavoritos.length - 1; i >= 0; i--) {
			var $elementoLinkFavorito = $("a[id-imovel*='" + idsImoveisFavoritos[i] + "']");

			if ($elementoLinkFavorito.length) {
				$elementoLinkFavorito.css({
					"color": "#ff0000"
				});
			}
		}
	}
}

function favoritar(dom_element) {
	jquery_element = $(dom_element);
	id_imovel = jquery_element.attr("id-imovel");
	var idsImoveisFavoritos = "";
	var baggioCK = buscarBaggioCookie();

	if (baggioCK.length) {
		idsImoveisFavoritos = baggioCK.split(CONS_COOKIE_SEPARADOR);
	} else {
		var postingFavorito = $.post(CONS_URL_BAGGIO + "imovel/favoritar", 
			{ 
				"id_imovel": id_imovel
			},
			// SUCCESS
			function(data) {
				inicializarVerificacaoFavoritos();
			}
		);

		postingFavorito.fail( function() {
			console.log("Ocorreu um erro ao tentar favoritar.");
		});
	}

	if( idsImoveisFavoritos.indexOf(id_imovel) =="-1") {
		var postingFavorito = $.post(CONS_URL_BAGGIO + "imovel/favoritar", 
			{ 
				"id_imovel": id_imovel
			},
			// SUCCESS
			function(data) {
				inicializarVerificacaoFavoritos();
			}
		);

		postingFavorito.fail( function() {
			console.log("Ocorreu um erro ao tentar favoritar.");
		});
	} else {
		var postingFavorito = $.post(CONS_URL_BAGGIO + "imovel/remover-favorito", 
			{ 
				"id_imovel": id_imovel
			},
			// SUCCESS
			function(data) {
				$("a[id-imovel*='" + id_imovel + "']").css({
					"color": "#7b7b7b"
				});
			}
		);

		postingFavorito.fail( function() {
			console.log("Ocorreu um erro ao tentar remover favorito.");
		});
	}
}

function desgostar(linkElement) {
	var $elementLink = $(linkElement);

	if ($elementLink.length) {
		var imovelID = $elementLink.attr("id-imovel");
		
		var postingDesgosto = $.post(CONS_URL_BAGGIO + "imovel/desgostar", 
			{ 
				"id_imovel": imovelID
			},
			// SUCCESS
			function(data) {
				alert('desgostou');
			}
		);

		postingDesgosto.fail( function() {
			console.log("Ocorreu um erro ao tentar desgostar o imovel " + imovelID + ".");
		});
	}
}

/**
 * INCIALIZAR ELEMENTOS COM CONFIRMACAO
 * ***********************
 *
 * ***********************
*/
function inicializarElementosConfirmacao() {
	$('[data-toggle="confirmation"]').click( function(event) {
		if (!confirm($(this).attr("data-title")) ){
			event.preventDefault();
		}
	});
}

/* **************************************************************************** */
/* ********************************* FUNCOES ********************************** */
/* **************************************************************************** */
/**
 * FECHAR ALERTAS
 * ***********************
 * Funcao para fechar alertas
 * com o atributo "alert-autoclose"
 * em um certo periodo de tempo.
 * ***********************
 */
 function fecharAlertas() {
	$("[alert-autoclose]").each( function() {
		fecharAlerta($(this));
	});
 }

 /**
 * FECHAR ALERTA
 * ***********************
 * 
 * ***********************
 */
 function fecharAlerta($elementoAlerta) {
 	if ($elementoAlerta.length) {
 		var tempoEsconder = parseFloat($elementoAlerta.attr("alert-autoclose")) * 1000;

 		$elementoAlerta
 			.delay(tempoEsconder)
 			.fadeTo(CONS_VELOCIDADE_ESCONDER, 0)
 			.slideUp(CONS_VELOCIDADE_SUMIR, function() {
 				//$(this).remove(); 
 			});
 	}
 }


/**
 * CLONAR ELEMENTO
 * ***********************
 * 
 * ***********************
 */
 function clonarElemento($elementoBotao) {
 	var $elementoClonar = $("#" + $elementoBotao.attr("clonar-elemento"));

 	if ($elementoClonar.length) {
 		var $parentClonar = $elementoClonar.parent();

 		if ($parentClonar.length) {
 			$elementoClonar.clone(true).appendTo($parentClonar);
 		}
 	}
 }


/**
 * ROLAGEM SUAVE
 * ***********************
 * 
 * ***********************
 */
 function rolagemSuave(evento, $elementoClick) {
 	evento.preventDefault();

 	var $elementoAncora = $("[name='" + $elementoClick.attr('rolagem') + "']");

 	if ($elementoAncora.length) {
 		$('html').animate({
 			scrollTop: $elementoAncora.offset().top
 		});
 	}
 }

/**
 * DISPARAR SUBMIT
 * ***********************
 *
 * ***********************
*/
function dispararSubmit(e) {
	var $elementoForm = $(e.target).closest("form");

	if (e.which == 13 && $elementoForm.length) {
		e.preventDefault();

		$elementoForm.submit();
	}
}

/**
 * BUSCAR BAGGIO COOKIE
 * ***********************
 *
 * ***********************
*/
function buscarBaggioCookie() {
	var baggioCookie = "";
	var documentCookies = document.cookie;

	if (documentCookies.length) {
		var cookies = documentCookies.split(";");

		for (var i = cookies.length - 1; i >= 0; i--) {
			var ck = cookies[i];

			if (ck.indexOf(CONS_COOKIE_BAGGIO) > 0) {
				baggioCookie = ck.replace(" " + CONS_COOKIE_BAGGIO +  "=", "");
			}
		}
	}
	return baggioCookie;
}

/**
 * ATUALIZAR ENDERECO COM CEP
 * ***********************
 *
 * ***********************
 *  {
	  "cep": "01001-000",
	  "logradouro": "Praça da Sé",
	  "complemento": "lado ímpar",
	  "bairro": "Sé",
	  "localidade": "São Paulo",
	  "uf": "SP",
	  "unidade": "",
	  "ibge": "3550308",
	  "gia": "1004"
	}
*/
function atualizarEnderecoComCEP(cep) {
    $.getJSON("https://viacep.com.br/ws/"+ cep +"/json",
		function (dados) {
			if(!("erro" in dados)) {
				var $elementoTextLogradouro = $("[endereco-auto='logradouro']");
				var $elementoTextComplemento = $("[endereco-auto='complemento']");
				var $elementoTextBairro = $("[endereco-auto='bairro']");
				var $elementoTextCidadeOptions = $("[endereco-auto='cidade'] option");
				var $elementoTextNumero = $("[name=param_endereco_numero]");
				var numero = null;
				// LOGRADOURO
				if ($elementoTextLogradouro.length) {
					$elementoTextLogradouro.val(dados.logradouro);
				}
				// COMPLEMENTO
				if ($elementoTextComplemento.length) {
					$elementoTextComplemento.val(dados.complemento);
				}
				// BAIRRO
				if ($elementoTextBairro.length) {
					$elementoTextBairro.val(dados.bairro);
				}
				// CIDADE
				if ($elementoTextCidadeOptions.length) {
					$elementoTextCidadeOptions.filter( function() { 
						return ( $(this).text() == dados.localidade + " - " + dados.uf);
					}).prop('selected', true);
				}
				// NUMERO
				if ($elementoTextNumero.length) {
					numero = $elementoTextNumero.val();
				}
				buscarGeorreferencia(numero, dados.logradouro, dados.localidade, dados.uf);
			} else {
				alert("CEP não encontrado");
			}
		}
	);
}

/**
 * BUSCAR GEORREFERENCIA
 * ***********************
 *
 * ***********************
*/
function buscarGeorreferencia(numero, rua, cidade, estado) {
	var ruaFormatada = "";
	var cidadeFormatada = "";
	var estadoFormatado = "";
	var numeroFormatado = "";
	var enderecoFormatado = "";

	if (numero) {
		numeroFormatado = numero.toString().trim();
	}
	if (rua) {
		ruaFormatada = rua.replace(new RegExp(" ", "g"), "+");
	}
	if (cidade) {
		cidadeFormatada = cidade.replace(new RegExp(" ", "g"), "+");
	}
	if (estado) {
		estadoFormatado = estado.replace(new RegExp(" ", "g"), "+");
	}

	if (ruaFormatada.length || cidadeFormatada.length || estadoFormatado.length) {
		enderecoFormatado += numeroFormatado;
		// CONCATENA RUA
		if (ruaFormatada.length) {
			enderecoFormatado += (enderecoFormatado.length ? ",+" : "");
			enderecoFormatado += ruaFormatada;
		}
		// CONCATENA CIDADE
		if (cidadeFormatada.length) {
			enderecoFormatado += (enderecoFormatado.length ? ",+" : "");
			enderecoFormatado += cidadeFormatada;
		}
		// CONCATENA ESTADO
		if (estadoFormatado.length) {
			enderecoFormatado += (enderecoFormatado.length ? ",+" : "");
			enderecoFormatado += estadoFormatado;
		}
	}

	if (enderecoFormatado.length) {
		$.getJSON("https://maps.googleapis.com/maps/api/geocode/json?address=" + enderecoFormatado + "&key=" + CONS_GOOGLE_GEO_API_KEY, 
			function(dados) {
				if (dados) {
					var resposta = dados.status;

					if (resposta == "OK") {
						var resultados = dados.results;

						for (var i = resultados.length - 1; i >= 0; i--) {
							var objetoEndereco = resultados[i];
							var placeID = objetoEndereco.place_id;
							var $elementoTextLatitude = $("#text-latitude-auto");
							var $elementoTextLongitude = $("#text-longitude-auto");

							if (placeID.length) {
								var geometria = objetoEndereco.geometry;

								if (geometria) {
									var localizacao = geometria.location;

									if (localizacao) {
										var latitude = localizacao.lat;
										var longitude = localizacao.lng;

										if ($elementoTextLatitude.length) {
											$elementoTextLatitude.val(latitude);
										}

										if ($elementoTextLongitude.length) {
											$elementoTextLongitude.val(longitude);
										}
									}
								}
								break;
							}
						};
					} else if (resposta == "ZERO_RESULTS") {
						alert("Não foi possível achar o georreferenciamento deste endereço.");
					} else if (resposta == "OVER_QUERY_LIMIT") {
						alert("Ops! O sistema atingiu seu limite de consultas de georreferenciamento da Google.");
					}
				}

			}
		);
	}

}

/**
 * IMPRIMIR PAGINA
 * ***********************
 *
 * ***********************
*/
function imprimirPagina() {
    window.print();
}
